import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";
import { Platform } from "react-native";
import { vanillaAuthState } from "src/services/auth_store";
import { get_is_on_service } from "src/api/delivery/profile";

const BACKGROUND_CHECK_SERVICE_TASK = "check-whether-on-service";

if (Platform.OS !== "web") {
  TaskManager.defineTask(BACKGROUND_CHECK_SERVICE_TASK, async () => {
    console.log(
      `task ${BACKGROUND_CHECK_SERVICE_TASK} at ${new Date(Date.now())}`,
    );
    //return BackgroundFetch.BackgroundFetchResult.NewData;
    const user_id = vanillaAuthState.getState().base_user.id;
    try {
      get_is_on_service();
      console.log(
        `Got service status ${user_id} at date: ${new Date(
          Date.now(),
        ).toISOString()}`,
      );
      return BackgroundFetch.BackgroundFetchResult.NewData;
    } catch {
      console.log(
        `Error fetching service status at date: ${new Date(
          Date.now(),
        ).toISOString()}`,
      );
      return BackgroundFetch.BackgroundFetchResult.Failed;
    }
  });
}

export async function registerForServiceStatusCheck() {
  // if (Platform.OS === "android") {
  //   return Promise.reject("disabled on android");
  // }
  return BackgroundFetch.registerTaskAsync(BACKGROUND_CHECK_SERVICE_TASK, {
    minimumInterval: 5, //60 * 5, // 5 minutes
    stopOnTerminate: true, // android only,
    startOnBoot: true, // android only
  });
}
export async function unregisterForServiceStatusCheck() {
  // if (Platform.OS === "android") {
  //   return Promise.reject("disabled on android");
  // }
  try {
    BackgroundFetch.unregisterTaskAsync(BACKGROUND_CHECK_SERVICE_TASK);
  } catch (e) {
    //
  }
}
