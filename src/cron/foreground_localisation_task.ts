import * as BackgroundFetch from "expo-background-fetch";
import * as TaskManager from "expo-task-manager";
import * as Location from "expo-location";

import { auth, db } from "firebase";
import { update, ref } from "firebase/database";
import Constants from "expo-constants";
import { Platform } from "react-native";
import {
  signInWithEmailAndPassword,
  getAuth as getFirebaseAuth,
} from "firebase/auth";
import { LocationObject, LocationObjectCoords } from "expo-location";
import * as Device from "expo-device";
import { IProfileInformation } from "src/interfaces/Entities";
import { http } from "src/services/http";
import { vanillaAuthState } from "src/services/auth_store";
import { vanillaProfileStore } from "src/zustore/profilestore";

const { setState: setProfile, getState: getProfile } = vanillaProfileStore;

const BACKGROUND_FETCH_TASK = "localisation-to-firebase";

export async function recover_delivery_information_for_profile(
  id: number,
): Promise<IProfileInformation> {
  const url = vanillaAuthState.getState().base_url;

  return http.get(`${url}deliverymen/${id}/informations`).then((response) => {
    if (response && response.status === 200) {
      const informations: IProfileInformation = response.data;
      setProfile({ profile: informations });
      return Promise.resolve(informations);
    }
    if (response) {
      console.log(response.status);
    }
    return Promise.reject();
  });
}

signInWithEmailAndPassword(auth, "cedric@sacrearmand.com", "wq6JrGgXhjhV5Lu");

// 1. Define the task by providing a name and the function that should be executed
// Note: This needs to be called in the global scope (e.g outside of your React components)
TaskManager.defineTask(BACKGROUND_FETCH_TASK, async ({ data, error }) => {
  //console.log("calling service");
  const current_user = getFirebaseAuth().currentUser;
  //console.log("user: ", current_user);
  if (current_user == null || current_user == undefined) {
    console.log("no user");
    try {
      signInWithEmailAndPassword(
        auth,
        "cedric@sacrearmand.com",
        "wq6JrGgXhjhV5Lu",
      );
    } catch (error) {
      //console.error(error);
      return BackgroundFetch.BackgroundFetchResult.Failed;
    }
  }

  // if (getProfile().isOnService != true) {
  //   // try {
  //   //   //const in_on_service = fet
  //   // } catch (error) {

  //   // }
  //   console.log(`Not in service hours`, isOnService);
  //   return BackgroundFetch.BackgroundFetchResult.NewData;
  // }
  const { locations } = data;
  if (error) {
    console.error(error);
    return BackgroundFetch.BackgroundFetchResult.Failed;
  }
  if (locations && locations?.length > 0) {
    const location: LocationObjectCoords = (locations[0] as LocationObject)
      .coords;
    const { latitude, longitude } = location;

    const auth = await vanillaAuthState.getState().auth;

    if (!auth?.access_token) {
      console.log("token not found: ", auth.access_token);

      return BackgroundFetch.BackgroundFetchResult.Failed;
    }

    let { profile } = getProfile();
    const extraData = {
      devicebrand: Device.brand ? Device.brand : "",
      deviceModelName: Device.modelName ? Device.modelName : "",
      deviceOS: Device.osName ? Device.osName : "",
      deviceOsVersion: Device.osVersion ? Device.osVersion : "",
      deviceYear: Device.deviceYearClass ? Device.deviceYearClass : "",
      platform: Platform.OS === "android" ? "android" : "ios",
      version: Constants.expoConfig?.version,
    } as any;

    // console.log("running service");
    const base_user = vanillaAuthState.getState().base_user;
    const user_id = base_user.id;

    let first_name;
    let idCity;
    first_name = getProfile().profile.first_name;
    idCity = getProfile().profile.idCity;
    try {
      if (!first_name || !idCity) {
        profile = await recover_delivery_information_for_profile(user_id);
        first_name = profile.first_name;
        idCity = profile.idCity;
      }
    } catch (error) {
      console.error(error);
    }
    //console.log("sending loc", latitude, longitude, user.id);
    const db_ref = ref(db, `localisations/${user_id}/`);
    update(db_ref, {
      ...extraData,
      longitude,
      latitude,
      idVille: idCity ? idCity : -1,
      prenom: first_name ? first_name : "",
      id: user_id,
      latest_update: new Date(),
    });
    //console.log(`Sent data to firebase at date: ${new Date().toISOString()}`);

    return BackgroundFetch.BackgroundFetchResult.NewData;
  } else {
    console.log("no data received");
    return BackgroundFetch.BackgroundFetchResult.NoData;
  }
});

// 2. Register the task at some point in your app by providing the same name, and some configuration options for how the background fetch should behave
// Note: This does NOT need to be in the global scope and CAN be used in your React components!

export async function registerBackgroundFetchAsync() {
  // if (Platform.OS === "android") {
  //   return Promise.reject("disabled on android");
  // }
  const auth = vanillaAuthState.getState().auth;
  // if (getBaseUser(auth).roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
  //   return;
  // }
  await BackgroundFetch.registerTaskAsync(BACKGROUND_FETCH_TASK, {
    minimumInterval: 10, // 10 minutes
    stopOnTerminate: false, // android only,
    startOnBoot: true, // android only
  });
  const { granted: foreground_granted } =
    await Location.requestForegroundPermissionsAsync();
  if (!foreground_granted) {
    console.log("foreground location tracking denied");
    return;
  }
  const { granted } = await Location.requestBackgroundPermissionsAsync();
  if (!granted) {
    console.log("background location tracking denied");
    return;
  }

  console.log("task starting point");

  // Make sure the task is defined otherwise do not start tracking
  const isTaskDefined = TaskManager.isTaskDefined(BACKGROUND_FETCH_TASK);
  if (!isTaskDefined) {
    console.log("Task is not defined");
    return;
  }

  // Don't track if it is already running in background
  const hasStarted = await Location.hasStartedLocationUpdatesAsync(
    BACKGROUND_FETCH_TASK,
  );

  if (hasStarted) {
    console.log("Already started");
    return;
  }

  await Location.startLocationUpdatesAsync(BACKGROUND_FETCH_TASK, {
    // For better logs, we set the accuracy to the most sensitive option
    accuracy: Location.Accuracy.Highest,
    // Make sure to enable this notification if you want to consistently track in the background
    showsBackgroundLocationIndicator: true,
    deferredUpdatesInterval: 3000,
    timeInterval: 1,
    distanceInterval: 0,
    foregroundService: {
      notificationTitle: "Sacré Armand Pro",
      notificationBody: "Positionnement activé pendant vos heures de service",
      notificationColor: "#fff",
    },
  });
}

export async function unregisterBackgroundFetchAsync() {
  // if (Platform.OS === "android") {
  //   return Promise.reject("disabled on android");
  // }
  const isRegistered = await TaskManager.isTaskRegisteredAsync(
    BACKGROUND_FETCH_TASK,
  );
  //const hasStarted = await Location.hasStartedLocationUpdatesAsync(BACKGROUND_FETCH_TASK)
  if (isRegistered === true) {
    try {
      // try {
      //   Location.stopLocationUpdatesAsync(BACKGROUND_FETCH_TASK);
      // } catch (e) {
      //   console.error(e);
      // }

      Location.stopLocationUpdatesAsync(BACKGROUND_FETCH_TASK);
    } catch (e) {
      //console.error(e);
    }
  }
}

export async function isTrackingTaskRegistered() {
  return TaskManager.isTaskRegisteredAsync(BACKGROUND_FETCH_TASK);
}

export async function getTrackingTaskStatus() {
  return BackgroundFetch.getStatusAsync();
}
