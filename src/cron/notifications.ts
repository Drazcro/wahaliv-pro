import * as TaskManager from "expo-task-manager";
import * as Notifications from "expo-notifications";
import { Linking } from "react-native";

const BACKGROUND_NOTIFICATION_TASK = "BACKGROUND-NOTIFICATION-TASK";

TaskManager.defineTask(
  BACKGROUND_NOTIFICATION_TASK,
  ({ data, error, executionInfo }) => {
    console.log("Received a notification in the background!");
    console.log(data);
    console.log(error);
    console.log(executionInfo);

    // Do something with the notification data
  },
);

export function registerForBGNotifications() {
  TaskManager.isTaskRegisteredAsync(BACKGROUND_NOTIFICATION_TASK)
    .then((isRegistered) => {
      if (!isRegistered) {
        console.log("Registering background notification task");
        Notifications.registerTaskAsync(BACKGROUND_NOTIFICATION_TASK);
      }
    })
    .catch((error) => {
      console.log(
        "Error registering background notification task for the 1st time",
      );
      console.log(error);

      try {
        Notifications.registerTaskAsync(BACKGROUND_NOTIFICATION_TASK);
      } catch (error) {
        console.error("IMPOSSIBLE TO REGISTER NOTIFICATION TASK");
      }
    });
}
