import { API_BASE_URL_DEV, API_BASE_URL_PROD } from "src/constants/urls";
import { Platform } from "react-native";
import Constants from "expo-constants";

import { vanillaAuthState } from "src/services/auth_store";
import { getBaseUser } from "src/services";
import { IAuth, ROLE_TYPE } from "src/services/types";
import { no_auth_http } from "src/services/http";

enum ACCOUNT_TYPE {
  "PARTNER",
  "DELIVERY",
  "UNKNOWN",
}

export const TEST_USERS = [];

export async function login(
  username: string,
  password: string,
  expo_token: string,
  device_brand: string | null,
  device_model_name: string | null,
  device_os_version: string | null,
  device_year: number | null,
  marketplace: number | null,
): Promise<IAuth> {
  // Monkey patch
  // if (username !== "demo@partenaire.test"){
  // }
  setApiEnv(username);

  const { setState } = vanillaAuthState;

  const url = `${vanillaAuthState.getState().base_url}login`;

  const app_pro_os = Platform.OS === "android" ? "android" : "ios";
  const app_pro_version = Constants.expoConfig?.version;

  const formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);
  formData.append("expo_token", expo_token);
  formData.append("device_brand", device_brand);
  formData.append("device_model_name", device_model_name);
  formData.append("device_os_version", device_os_version);
  formData.append("device_year", device_year);
  formData.append("app_pro_os", app_pro_os);
  formData.append("app_pro_version", app_pro_version);
  formData.append("marketplace", marketplace);

  return no_auth_http
    .post_form({ url, form: formData })
    .then((response) => {
      console.log(response.status);
      if (response && response.status === 200) {
        const auth_data: IAuth = response.data;
        const user = getBaseUser(auth_data);
        if (user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
          setState({ account_type: ACCOUNT_TYPE.DELIVERY });
        } else if (user.roles.includes(ROLE_TYPE.ROLE_PARTENAIRE)) {
          setState({ account_type: ACCOUNT_TYPE.PARTNER });
        } else {
          return Promise.reject();
        }

        auth_data.is_loading = false;
        vanillaAuthState.getState().setAuth(auth_data);
        auth_data.access_token = response.data.token;
        //setApiEnv(username)
        return Promise.resolve(auth_data);
      }
      return Promise.reject();
    })
    .catch((err) => {
      console.error(err, url);
      return Promise.reject();
    });
}

export function setApiEnv(username: string) {
  if (
    TEST_USERS.includes(username) ||
    (username.includes("demo") && username.includes("@wahaliv.com"))
  ) {
    vanillaAuthState.getState().base_url = API_BASE_URL_DEV;
  } else {
    vanillaAuthState.getState().base_url = API_BASE_URL_PROD;
  }
}
