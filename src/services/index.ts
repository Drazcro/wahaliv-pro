import jwtDecode from "jwt-decode";
import { remove_auth, vanillaAuthState } from "src/services/auth_store";
import { IAuth, IBaseUser } from "src/services/types";
import Constants from "expo-constants";
import * as Device from "expo-device";

import { Platform } from "react-native";

export function getBaseUser(auth: IAuth): IBaseUser {
  const auth_ = auth ?? vanillaAuthState.getState().auth;
  try {
    const user = jwtDecode(auth_.access_token) as IBaseUser;
    return user;
  } catch (e) {
    logout();
    return { id: -1 } as IBaseUser;
  }
}

export const logout = async () => {
  remove_auth();
};

export function authToUserId(auth?: IAuth): number {
  const auth_ = auth ?? vanillaAuthState.getState().auth;
  try {
    const user = jwtDecode(auth_.access_token) as IBaseUser;
    return user.id;
  } catch (e) {
    logout();
    return -1;
  }
}

export const get_device_info = () => {
  const app_pro_os = Platform.OS === "android" ? "android" : "ios";
  const app_pro_version = Constants.expoConfig?.version;
  const device_brand = Device.brand;
  const device_model_name = Device.modelName;
  const device_os_version = Device.osVersion;
  const device_year = Device.deviceYearClass;

  const expo_token = `ExponentPushToken[${
    vanillaAuthState.getState().push_token
  }]`;
  const base_user = getBaseUser(vanillaAuthState.getState().auth);

  console.log("expo token test: ", expo_token);
  const response: any = {
    app_pro_os,
    app_pro_version,
    device_brand,
    device_model_name,
    device_os_version,
    device_year,
    base_user,
  };
  if (expo_token !== "ExponentPushToken[]") {
    response["expo_token"] = expo_token;
  }
  return response;
};
