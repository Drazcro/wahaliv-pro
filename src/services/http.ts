import axios from "axios";
import {
  remove_auth,
  retrieve_auth,
  vanillaAuthState,
} from "src/services/auth_store";
import { IAuth } from "./types";

export type KeyValue = Record<
  string,
  string | number | boolean | unknown | unknown
>;

const get_new_access_token = () => {
  const URL = vanillaAuthState.getState().base_url;

  axios
    .get(`${URL}token/refresh`, {
      params: {
        refresh_token: vanillaAuthState.getState().auth.refresh_token,
      },
    })
    .then(function (response) {
      const auth_data: IAuth = response.data;
      auth_data.is_loading = false;
      vanillaAuthState.getState().setAuth(auth_data);
    })
    .catch(function (error) {
      console.log(
        "No refresh token : " + vanillaAuthState.getState().auth.refresh_token,
      );
      console.log(error);
      remove_auth();
    });
};

const instance = axios.create({
  withCredentials: false,
  headers: {
    Accept: "application/json",
  },
});

const no_auth_instance = axios.create({
  withCredentials: false,
  headers: {
    Accept: "application/json",
  },
});

instance.interceptors.request.use(
  function (config) {
    const access = retrieve_auth().auth.access_token;
    config.headers.setAuthorization(`Bearer ${access}`);
    return config;
  },
  function (error) {
    return Promise.reject(error);
  },
);

instance.interceptors.response.use(
  function (response) {
    // Triggered by 2xx status
    console.log(`http ${response.status} | ${response.config.url}`);
    return response;
  },
  function (error) {
    // Triggered by anything other than 2xx

    if (error.response.status == 401) {
      //remove_auth();
      get_new_access_token();
    }
    console.log(`http ${error.response.status} | ${error.config.url} `);
    return Promise.reject(error);
  },
);

const get = (url: string) => {
  return instance(url, { method: "get" });
};
const post = ({ url, data }: { url: string; data?: KeyValue }) => {
  return instance(url, { method: "post", data: data });
};

const no_auth_post = ({ url, data }: { url: string; data?: KeyValue }) => {
  return no_auth_instance(url, { method: "post", data: data });
};

const post_form = ({ url, form }: { url: string; form: FormData }) => {
  return instance(url, {
    method: "post",
    data: form,
    headers: { "Content-Type": "multipart/form-data" },
  });
};

const no_auth_post_form = ({ url, form }: { url: string; form: FormData }) => {
  return no_auth_instance(url, {
    method: "post",
    data: form,
    headers: { "Content-Type": "multipart/form-data" },
  });
};

const put = ({ url, data }: { url: string; data?: KeyValue }) => {
  return instance(url, { method: "put", data: data });
};

const del = ({ url, data }: { url: string; data?: KeyValue }) => {
  return instance(url, { method: "delete", data: data });
};

const http = {
  get: get,
  post: post,
  put: put,
  del: del,
  post_form: post_form,
};

const no_auth_http = {
  post: no_auth_post,
  post_form: no_auth_post_form,
};

export { http, no_auth_http };
