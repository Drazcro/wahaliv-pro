export interface IBaseUser {
  email: string;
  id: number;
  iat: number;
  exp: number;
  isSalarie: boolean;
  is_latest_version: boolean;
  roles: string[];
}

export enum ROLE_TYPE {
  ROLE_PARTENAIRE = "ROLE_PARTENAIRE",
  ROLE_COURSIER = "ROLE_COURSIER",
  ROLE_PARTICULIER = "ROLE_PARTICULIER",
}

export interface IUser {
  name: string;
  first_name: string;
  last_name: string;
  civility: string;
  email: string;
  dob: string;
  siret: string;
  telephone: string;
  address: string;
  company_name: string;
  social_form: string;
}

export interface IAuth {
  access_token: string;
  refresh_token: string;
  is_loading?: boolean;
}
