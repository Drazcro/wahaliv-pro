/* eslint-disable */

import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_BASE_URL_DEV, API_NOTIF_BASE_URL } from "src/constants/urls";

import { IAuth, IBaseUser, ROLE_TYPE } from "src/services/types";
import { ref, set } from "firebase/database";
import { db } from "firebase";
import * as Notifications from "expo-notifications";
//import * as Sentry from "@sentry/react-native";
import * as Device from "expo-device";
import { Platform } from "react-native";
import Constants from "expo-constants";
import jwtDecode from "jwt-decode";

export enum ACCOUNT_TYPE {
  "PARTNER",
  "DELIVERY",
  "UNKNOWN",
}

interface State<T> {
  push_token: string;
  auth: IAuth;
  account_type: ACCOUNT_TYPE;
  base_url: string;
  notif_url: string;
  base_user: IBaseUser;
  setPushToken: (payload: string) => void;
  setAuth: (payload: IAuth) => void;
  setIsLoading: (payload: boolean) => void;
  setAccountTypePartner: () => void;
  setAccountTypeDelivery: () => void;
  removeAuth: () => void;
  setBaseUrl: (value: string) => void;
  setNotifUrl: (value: string) => void;
}

const vanillaAuthState = create<State<{}>>()(
  persist(
    (set) => ({
      push_token: "",
      auth: { is_loading: true } as IAuth,
      account_type: ACCOUNT_TYPE.UNKNOWN,
      base_url: API_BASE_URL_DEV,
      notif_url: API_NOTIF_BASE_URL,
      base_user: { id: -1 } as IBaseUser,
      setNotifUrl: (url) => set(() => ({ notif_url: url })),
      setBaseUrl: (url) => set(() => ({ base_url: url })),
      setPushToken: (token) =>
        set((state) => ({
          ...state,
          push_token: token.replace("ExponentPushToken[", "").replace("]", ""),
        })),
      setAuth: (payload) => {
        set((state) => {
          if (payload.access_token) {
            saveTokenToFirebase(payload);
          }
          return { auth: payload, base_user: getBaseUser(payload) };
        });
      },

      setIsLoading: (is_loading) =>
        set((state) => ({ auth: { ...state.auth, is_loading } })),
      setAccountTypePartner: () =>
        set((state) => ({ account_type: ACCOUNT_TYPE.PARTNER })),
      setAccountTypeDelivery: () =>
        set((state) => ({ account_type: ACCOUNT_TYPE.DELIVERY })),
      removeAuth: () => {
        set({
          auth: { is_loading: false } as IAuth,
          account_type: ACCOUNT_TYPE.UNKNOWN,
          base_url: API_BASE_URL_DEV,
          notif_url: API_NOTIF_BASE_URL,
        });
      },
    }),
    {
      name: "wahaliv-auth-persistance",
      storage: createJSONStorage(() => AsyncStorage),
    },
  ),
);

const authStore = vanillaAuthState as {
  <T>(): State<T>;
  <T, U>(selector: (s: State<T>) => U): U;
};

function retrieve_auth() {
  return vanillaAuthState.getState();
}

function remove_auth() {
  vanillaAuthState.getState().removeAuth();
}

function getBaseUser(auth: IAuth): IBaseUser {
  const auth_ = auth ?? vanillaAuthState.getState().auth;
  try {
    const user = jwtDecode(auth_.access_token) as IBaseUser;
    return user;
  } catch (e) {
    logout();
    return { id: -1 } as IBaseUser;
  }
}

const logout = async () => {
  remove_auth();
};

function authToUserId(auth?: IAuth): number {
  const auth_ = auth ?? vanillaAuthState.getState().auth;
  try {
    const user = jwtDecode(auth_.access_token) as IBaseUser;
    return user.id;
  } catch (e) {
    logout();
    return -1;
  }
}
export { vanillaAuthState, authStore, remove_auth, retrieve_auth };

function saveTokenToFirebase(auth: IAuth) {
  const base_user = getBaseUser(auth);
  if (auth.access_token && auth.access_token.length > 0) {
    for (const role of base_user.roles) {
      if (role === ROLE_TYPE.ROLE_PARTICULIER) {
        continue;
      }

      const db_ref = ref(db, `access_tokens/${role}/${authToUserId(auth)}/`);
      const device_brand = Device.brand;
      const device_model_name = Device.modelName;
      const device_os_version = Device.osVersion;
      const device_year = Device.deviceYearClass;
      const app_pro_os = Platform.OS === "android" ? "android" : "ios";
      const app_pro_version = Constants.expoConfig?.version;
      Notifications.getExpoPushTokenAsync({
        //applicationId: "@helodev/sacrearmand_pro_app",
        projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
      })
        .then((response) => {
          console.log("response:", response.data);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            push_token: response.data,
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
        })
        .catch((e) => {
          console.error(e);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
          //Sentry.captureException(e);
        });
    }
  }
}
