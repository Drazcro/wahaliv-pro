/* eslint-disable @typescript-eslint/no-explicit-any */
("use strict");
import { IAuth, ROLE_TYPE } from "src/services/types";
import { authToUserId, getBaseUser } from "src/services";
import { ref, set } from "firebase/database";
import { db } from "firebase";
import * as Notifications from "expo-notifications";
//import * as Sentry from "@sentry/react-native";
import * as Device from "expo-device";
import { Platform } from "react-native";
import Constants from "expo-constants";

import I18n from "i18n-js";
import moment from "moment";
import "moment/locale/fr";
import { Alert } from "react-native";
import * as Linking from "expo-linking";
import * as Clipboard from "expo-clipboard";
import { vanillaAuthState } from "src/services/auth_store";

const TEST_USERS = [
  "demo@sacrefernand.com",
  "dev1livrer@gmail.com",
  "demotest@sacrearmand.com",
  "sushi.boat.carcassonne@gmail.com",
  "demoLiv2@sacrearmand.com",
  "burgertest@sacrearmand.com",
  "demo2@sacrearmand.com",
];

export function saveTokenToFirebase(auth: IAuth) {
  const base_user = getBaseUser(auth);
  if (auth.access_token && auth.access_token.length > 0) {
    for (const role of base_user.roles) {
      if (role === ROLE_TYPE.ROLE_PARTICULIER) {
        continue;
      }

      const db_ref = ref(db, `access_tokens/${role}/${authToUserId(auth)}/`);
      const device_brand = Device.brand;
      const device_model_name = Device.modelName;
      const device_os_version = Device.osVersion;
      const device_year = Device.deviceYearClass;
      const app_pro_os = Platform.OS === "android" ? "android" : "ios";
      const app_pro_version = Constants.expoConfig?.version;
      Notifications.getExpoPushTokenAsync({
        //applicationId: "@helodev/sacrearmand_pro_app",
        projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
      })
        .then((response) => {
          console.log("response for token :", response.data);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            push_token: response.data,
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
        })
        .catch((e) => {
          console.error(e);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
          //Sentry.captureException(e);
        });
    }
  }
}

moment.locale("fr");

export interface IHourDate {
  hour: number;
  min: number;
  sec: number;
  day: number;
}

export const dateDiff = (
  first_date: moment.Moment,
  second_date: moment.Moment,
): IHourDate => {
  const date_diff = {} as IHourDate;
  let tmp = first_date.diff(second_date);
  tmp = Math.floor(tmp / 1000);
  date_diff.sec = tmp % 60;

  tmp = Math.floor((tmp - date_diff.sec) / 60);
  date_diff.min = tmp % 60;

  tmp = Math.floor((tmp - date_diff.min) / 60);
  date_diff.hour = tmp % 24;

  tmp = Math.floor((tmp - date_diff.hour) / 24);
  date_diff.day = tmp;

  return date_diff;
};

export function isToday(date1: Date, date2: Date) {
  return (
    date1.getDate() == date2.getDate() &&
    date1.getMonth() == date2.getMonth() &&
    date1.getFullYear() == date2.getFullYear()
  );
}

export const dateToTime = (date: string) => {
  const heure = moment(date).format("HH");
  const minute = moment(date).format("mm");
  return `${heure}h${minute}`;
};

export const dateToDay = (date: string) => {
  const toDay = moment().day();
  const value = moment(date).day();
  if (toDay === value) {
    return "aujourd'hui";
  }
  return moment(date).format("dddd");
};

export const validate_email = (email: string) => {
  //@ts-ignore
  // eslint-disable-next-line no-useless-escape
  if (!email || !/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {
    return "Email non valide";
  }
  return "";
};

export const validate_phone_number = (num: string) => {
  if (!num || !/^(0|\+33)[1-9]([-.]?[0-9]{2}){3}([-.]?[0-9]{2})$/.test(num)) {
    return I18n.t("profile.information.phone");
  }
  return "";
};

export const validate_siret = (siret: string) => {
  const pattern = /^([0-9]{14})$/;
  const validate = pattern.test(siret);
  if (siret === null || siret === "" || !validate) {
    return I18n.t("profile.information.siret");
  }
  return "";
};

export const validate_dob = (dob: string) => {
  const pattern = /^([0-9]{2})\/([0-9]{2})\/([0-9]{4})$/;
  if (dob === null || !pattern.test(dob)) {
    return I18n.t("profile.information.dob");
  }
  return "";
};

export const convert_point_to_comma = (m: unknown) => {
  const parseNumber: number = parseInt(m as string, 10);
  const parseSymbol = parseFloat(String(parseNumber / 1000)).toFixed(1);
  return parseSymbol.replace(".", ",");
};

export const point_and_comma = (val: unknown) => {
  const parseVal = parseFloat(val as string).toFixed(2);
  return parseVal.replace(".", ",");
};

export const convert_number = (price: string) => {
  const parseVal = parseFloat(price).toFixed(2);
  return parseVal.replace(".", ",");
};

export const validate_iban = (iban: string) => {
  const pattern = /^FR\d{12}[0-9a-zA-Z]{11}\d{2}$/;
  if (iban === null || !pattern.test(iban)) {
    return I18n.t("profile.coordonnerBancaire.error");
  }
  return "";
};

export const validate_swift = (swift: string) => {
  const pattern = /^([a-zA-Z0-9]{8}([a-zA-Z0-9]{3})?)$/;
  if (swift === null || !pattern.test(swift)) {
    return I18n.t("profile.coordonnerBancaire.error");
  }
  return "";
};

export const password_validate = (password: string) => {
  if (!password || password.length < 8) {
    return I18n.t("profile.updatePassword.limitPassword");
  }
  return "";
};

export const confirm_password_validate = (
  confirm: string,
  password: string,
) => {
  if (confirm && confirm !== password) {
    return I18n.t("profile.updatePassword.confirmPasswordValid");
  }
  return "";
};

export const parse_to_float_number = (_n: string | number | undefined) => {
  return _n !== undefined ? _n.toString().replace(".", ",") : "0";
};

export const capitalize_first_letter = (string: string) => {
  return string.replace(/^./, string[0].toUpperCase());
};

export const to_formatted_date = (date: string) => {
  return moment(date).format("DD MMMM YYYY");
};

export const french_to_api_date = (date: string) => {
  const parts = date.split("/");
  const day = parts[0];
  const month = parts[1];
  const year = parts[2];

  const formattedDate = `${year}-${month}-${day}`;
  return formattedDate;
};

export const to_formatted_time = (date: string) => {
  return moment(date).format("HH") + "h" + moment(date).format("mm");
};

export const hour_to_string = (hour: string) => hour.replace(":", "h");

export function should_update_from_store(
  username: string,
  remote_version: string,
) {
  if (
    TEST_USERS.includes(username) ||
    (username.includes("demo") && username.includes("@sacrearmand.com"))
  ) {
    console.log("is test account, no update should be done");
    return false;
  }

  const local_version = Constants.expoConfig?.version;
  if (!local_version || local_version == remote_version) {
    return false;
  }

  const splitted_local = local_version.split(".");
  const splitted_remote = remote_version.split(".");
  //console.log(splitted_local, splitted_remote, "versions");
  if (splitted_local[0] < splitted_remote[0]) {
    return true;
  } else if (splitted_local[0] == splitted_remote[0]) {
    if (splitted_local[1] < splitted_remote[1]) {
      return true;
    } else if (splitted_local[1] == splitted_remote[1]) {
      if (splitted_local[2] < splitted_remote[2]) {
        return true;
      }
    }
  }
  return false;
}

export function shouldUpdateFromStore(remote_version: string) {
  const local_version = Constants.expoConfig?.version;
  if (!local_version || local_version == remote_version) {
    return false;
  }

  const splitted_local = local_version.split(".");
  const splitted_remote = remote_version.split(".");
  console.log(splitted_local, splitted_remote, "versions");
  if (splitted_local[0] < splitted_remote[0]) {
    return true;
  } else if (splitted_local[0] == splitted_remote[0]) {
    if (splitted_local[1] < splitted_remote[1]) {
      return true;
    } else if (splitted_local[1] == splitted_remote[1]) {
      if (splitted_local[2] < splitted_remote[2]) {
        return true;
      }
    }
  }
  return false;
}

export async function call_hidden_number(number: string) {
  try {
    // console.log("--------+-------");
    const badPattern = "+0033";
    const phoneNumber = number.replace("+", "");
    const iosNumber = "#31#00" + phoneNumber;
    const androidNumber = "%2331%2300" + phoneNumber;
    if (Platform.OS === "web") {
      if (iosNumber.includes(badPattern)) {
        iosNumber.replace(badPattern, "0033");
      }

      await Linking.openURL(`tel:${androidNumber}`);
    } else {
      if (androidNumber.includes(badPattern)) {
        androidNumber.replace(badPattern, "0033");
      }
      await Linking.openURL(`tel:${androidNumber}`);
    }
  } catch (error) {
    console.error(error);
    Alert.alert(
      "Appel",
      number,
      [
        {
          text: "Copier le numéro",
          isPreferred: true,
          onPress: () => {
            Clipboard.setStringAsync(number);
          },
        },
        {
          text: "Non merci",
        },
      ],
      { cancelable: true },
    );
  }
}

export const get_device_info = () => {
  const app_pro_os = Platform.OS === "android" ? "android" : "ios";
  const app_pro_version = Constants.expoConfig?.version;
  const device_brand = Device.brand;
  const device_model_name = Device.modelName;
  const device_os_version = Device.osVersion;
  const device_year = Device.deviceYearClass;

  const expo_token = `ExponentPushToken[${
    vanillaAuthState.getState().push_token
  }]`;
  const base_user = getBaseUser(vanillaAuthState.getState().auth);

  console.log("expo token test: ", expo_token);
  const response: any = {
    app_pro_os,
    app_pro_version,
    device_brand,
    device_model_name,
    device_os_version,
    device_year,
    base_user,
  };
  if (expo_token !== "ExponentPushToken[]") {
    response["expo_token"] = expo_token;
  }
  return response;
};
