export { default as Month } from "./components/Month";

export { MonthProps, ThemeType, LocaleType, MarkedDays } from "./types";
