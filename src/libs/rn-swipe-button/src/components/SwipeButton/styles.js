import { StyleSheet } from "react-native";

const Styles = StyleSheet.create({
  container: {
    borderRadius: 100 / 2,
    borderWidth: 1,
    justifyContent: "center",
    margin: 5,
    height: 58,
  },
  title: {
    alignSelf: "center",
    position: "absolute",
  },
});

export default Styles;
