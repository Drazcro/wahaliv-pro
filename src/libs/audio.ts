import Sound from "react-native-sound";

Sound.setCategory("Playback");

const alert = new Sound("notification.mp3", Sound.MAIN_BUNDLE, (error) => {
  if (error) {
    return;
  } else {
    alert.setNumberOfLoops(-1);
  }
});

const playAlert = () => alert.play();
const stopAlert = () => alert.stop();

export { playAlert, stopAlert };
