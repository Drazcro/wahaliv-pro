import { COURSE_STATUS } from "src/constants/course";
import { Float } from "react-native/Libraries/Types/CodegenTypes";

export interface ILoginData {
  username: string;
  password: string;
  firebaseToken?: string;
}

export interface ILoginUser {
  username: string;
  password: string;
  userFirstName: string;
  userLastName: string;
  email: string;
  roles: [];
  plainPassword: string;
}

export interface IMGEntry {
  amount: number;
  days: string;
  hours: {
    from: string;
    to: string;
  };
  nb_services: number;
  nb_services_done: number;
  specific_day_nb_services: number;
}

export interface IMinimumGaranti {
  soir?: IMGEntry;
  midi?: IMGEntry;
}

export interface IProfile {
  autoValid: boolean;
  averageDelay: number;
  bonus: number;
  coursesCount: number;
  earnings: number;
  email: string;
  firstName: string;
  first_name: string;
  isSalarie: boolean;
  lastName: string;
  last_name: string;
  minimum_garanti: IMinimumGaranti;
  name: string;
  nbTransactions: number;
  nb_services: number | null;
  nb_services_done: number;
  phoneNumber: string;
  profilPicturePath: string;
  rateCount: number;
  ratePercentage: number;
  tipEarning: number;
}

export interface IProfileInformation {
  id: number;
  address: string;
  civility: string;
  company_name?: string | undefined;
  dob: string;
  email: string;
  firstName: string;
  first_name: string;
  idCity: number;
  id_city: number;
  isSalarie: boolean;
  lastName?: string | undefined;
  last_name?: string | undefined;
  name?: string | undefined;
  siret: string;
  telephone: string;
}

export interface IInformation {
  civility: string;
  name?: string | undefined;
  company_name?: string | undefined;
  email: string;
  dob: string;
  siret: string;
  telephone: string;
  address: string;
}

export interface ICoordonnateBank {
  banque: string;
  frequence: string;
  iban: string;
  swift: string;
  titulaireCompte: string;
}

export interface IResetField {
  field: string;
  setField: () => void;
}

export interface ICourse {
  // customer
  customer: string;
  customer_name?: string;
  customer_lat: number;
  customer_lng: number;
  customer_telephone: string;
  // delivery
  delivery_address: IAddresse;
  delivery_amount: number;
  delivery_distance: number;
  delivery_date: IDateHour;
  delivery_status: COURSE_STATUS;
  details: Array<ICourseDetails>;
  // commande
  id_commande: number;
  id_delivery: number;
  // partner
  partner: string;
  partner_name?: string;
  partner_lat: number;
  partner_lng: number;
  partner_telephone: string;
  // pickup
  pickup_address: string;
  pickup_date: IDateHour;
  isRetrievable: boolean;
  isDisplayed: boolean;
  // new
  real_delivery_date: IDateHour;
  commande_at: IDateHour;
  isRefund: boolean;
  isGrouped: boolean;
  tip: number;
  comment: string;
  groupedCommandIds: Array<number>;
  need_real_price: boolean;
  newPrice: string;
}

export interface IAddresse {
  address: string;
  complementadresse: string;
  numerorue: number;
  postalCode: string;
}

export interface ICourseWithStatus {
  id: number;
  delivery_status: number;
}

export interface ICourseSearchFinished {
  search: string;
  sort: string;
}

export interface ICourseDetails {
  name: string;
  price: number;
  qte: number;
}

interface IDateHour {
  date: string;
  timezone: string;
  timezone_type: number;
}

export interface IUpdatePassword {
  newPassword: string;
  oldPassword: string;
}

export interface IDocumentJustificatif {
  contract: boolean;
  identityRecto: boolean;
  identityVerso: boolean;
  registration_proof: boolean;
  rib: boolean;
}

export type Case = {
  id: number;
  title: string;
  fileTypes: string;
  docType: string;
  uploaded: boolean;
};

export interface IReverseEarnings {
  canPayback: boolean;
  isSalarie: boolean;
  paybackAmount: number;
}

//////////////////// INVOICE //////////////////////

export interface IInvoiceAPIResponse {
  total_tip: number;
  total_bonus: number;
  global_data: IDayInvoice[];
}

export interface IDeliveryDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IInvoiceResume {
  distance: number;
  total_customer: number;
  total_mp: number;
  total: number;
  bonus: number;
  tip: number;
}

export interface IInvoiceDataCourse {
  id_delivery: number;
  id_commande: number;
  distance: number;
  delivery_date: IDeliveryDate;
  total: number;
  total_customer: number;
  total_mp: number;
  bonus: number;
  tip: number;
}

export interface IDayInvoice {
  date: IDeliveryDate;
  global: IInvoiceResume;
  courses: IInvoiceDataCourse[];
}

////////////////////// Finished Invoice /////////////////////////

export interface IBillsDetails {
  begin_date: IDeliveryDate;
  end_date: IDeliveryDate;
  deliveryman_bill_id: number;
  edition_date: IDeliveryDate;
  paiement_date: IDeliveryDate;
  deliveryman_bill_amount: number;
}

//////////////////// Planning /////////////////////////////////

export interface IPlanningSummaryData {
  start_time: string;
  end_time: string;
  hour: number;
  id: number;
}

export interface IPlanningSummary {
  id: number;
  day_str: string;
  day: string;
  total_hour: number;
  data: Array<IPlanningSummaryData>;
}

export type IPlanningsSummary = Array<IPlanningSummary>;

export interface IPlanning {
  id: number;
  time: string;
  state: string;
  id_planning?: number;
  id_creneaux?: number;
}

export type IPlannings = Array<IPlanning>;

export interface IPlanningDisengageStatus {
  is_course_in_progress: boolean;
  is_able_to_disengage: boolean;
}

// ----------------------------------------------- //
// -------------- PARTNER PROFILE ---------------- //
// ----------------------------------------------- //

interface IPartner {
  name: string;
  telephone: string;
  address: string;
  is_open: boolean;
  can_print: boolean;
}
interface IState {
  rate_client: string;
  rate_count: number;
  order_count: string;
  ca: string;
  average_delay: string;
  nb_transactions: number;
}

export interface IPartnerProfile {
  partner: IPartner;
  stats: IState;
}

export interface IPartnerProfilePassword {
  oldPassword: string;
  newPassword: string;
}

export interface IDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IPartnerCommande {
  id: number;
  collecte_date: IDate;
  status: number;
  is_checked: boolean;
}

export interface IPartnerCustomer {
  name: string;
}

export interface PromoCode {
  amount: string;
  by: string;
}

export interface IPartnerOrderDetails {
  nb_products: number;
  delivery_fees: number;
  total_amount: string;
  promo_code: PromoCode;
}

///////////////////////GET_PARTNER_DETAILS_COURSE//////////////////////////////

export interface IPickupDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IDeliveryDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface ICommande {
  id: number;
  pickup_date: IPickupDate;
  delivery_date: IDeliveryDate;
  deliveryman: string;
  comment: string;
  status: number;
  service_type: string;
  is_checked: boolean;
}

export interface ICustomer {
  name: string;
}

export interface ISubProduct {
  name: string;
  qte: number;
  price: string;
  is_refund: boolean;
}

export interface IProductDetail {
  name: string;
  qte: number;
  price: string;
  is_refund: boolean;
  sub_products: ISubProduct[];
}

export interface IPayementDetail {
  type: string;
  amount: string;
  frais: string;
}

export interface IPartnerOrderDetails {
  commande: ICommande;
  customer: ICustomer;
  product_details: IProductDetail[];
  total_amount: string;
  // promo_code: string;
  // delivery_fees: string;
  payement_details: IPayementDetail[];
}

/////////////////////////PARTNER_ORDERS_DETAILS//////////////////////////
export interface IPickupDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IDeliveryDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface ICommande {
  id: number;
  pickup_date: IPickupDate;
  delivery_date: IDeliveryDate;
  deliveryman: string;
  comment: string;
  status: number;
  service_type: string;
  is_checked: boolean;
  need_real_price: boolean;
}

export interface ICustomer {
  name: string;
}

export interface ISubProduct {
  name: string;
  qte: number;
  price: string;
  is_refund: boolean;
}

export interface IProductDetail {
  name: string;
  qte: number;
  price: string;
  is_refund: boolean;
  sub_products: ISubProduct[];
}

export interface IPromoCode {
  amount: string;
  by: string;
  promo_code: string;
}

export interface IPayementDetails {
  type: string;
  amount: string;
  frais: string;
}

export interface IOrder {
  commande: ICommande;
  customer: ICustomer;
  product_details: Array<IProductDetails>;
  total_amount: string;
  promo_code: IPromoCode;
  delivery_fees: string;
  paiement_details: Array<IPayementDetails>;
  newPrice: string;
}

export interface IPartnerOrderData {
  total_data: number;
  total: string;
  data: IOrder[];
}

export enum ORDER_STATUS {
  NEW = 5,
  ACCEPTED = 4,
  DELIVERED = 1,
  CANCELLED = 2,
  ALL = 0,
  IN_DELIVERY = "Livraison",
  IN_TAKE_OUT = "A emporter",
}

interface IOrder {
  id: string;
}

export interface IProductDetails {
  id: number;
  name: string;
  qte: number;
  quantity: number;
  price: string;
  is_refund: boolean;
  isRefund: boolean;
  sub_products?: Array<IProductDetails>;
}

export interface IOrderProducts {
  commande: Array<IOrder>;
  product_details: Array<IProductDetails>;
}

export interface IOrderRefund {
  products: Array<number>;
  supplements: Array<number>;
}

export interface IOrderTradeDiscount {
  amount: number;
  comment: string;
}
/////////////////////////////TIMER//////////////////////////////////

export interface IPartnerProfileEarlyClosing {
  id: string;
  begin_date: IDate;
  end_date: IDate;
}

export interface IPartnerProfileUpdateDate {
  begin_date: string;
  end_date: string;
  id?: number | null;
}

export type IPartnerProfileEarlyClosings = Array<IPartnerProfileEarlyClosing>;

// ---------------------------------------- //
// ------------  TOURNEES ----------------- //
// ---------------------------------------- //

export interface ITournee {
  id: number;
  begin_hour: string;
  end_hour: string;
  status: number;
  orders_count: number;
  earnings: Float;
  validation_hour_limit: string;
  begin_date: IDate;
}

export interface ITourneeAPIResponse {
  total_data: number;
  total: string;
  data: Array<ITournee>;
}

interface IOrderDetailSubProduct {
  id: number;
  name: string;
  price: string;
  quantity: number;
}

export interface IOrderDetail {
  id: number;
  qte: number;
  name: string;
  price: string;
  sub_products: Array<IOrderDetailSubProduct>;
}

interface IStepDeliveryAddress {
  address: string;
  postalCode: string;
  numerorue: string;
  // complementadresse: string;
  complementadresse: string;
  appartement: string;
  etage: string;
  batiment: string;
  code_porte: string;
}
interface IStepTarget {
  name: string;
  address: string;
  lat: Float;
  lng: Float;
  telephone: string;
  delivery_adress: IStepDeliveryAddress | null;
  adress: string;
}

export interface IStep {
  step_id: number;
  order_id: number;
  course_id: number;
  order_comment: string;
  position: number;
  order_is_last_moment?: boolean;
  hour: string;
  type: string;
  step_status: number;
  state: string;
  target: IStepTarget;
  tip: string;
  order_details: Array<IOrderDetail>;
  pickup_indication: string;
}

export interface ITourneeDetail {
  id: number;
  begin_hour: string;
  end_hour: string;
  status: number;
  orders_count: number;
  steps_count: number;
  earnings: Float;
  tips: string;
  distance: string;
  steps: Array<IStep>;
}

// ---------------------------------------- //
// ------------ COURSES V2 ---------------- //
// ---------------------------------------- //

export interface ICourseV2Data {
  total_data: number;
  total: number;
  data: ICourseV2[];
  hasTournee: boolean;
  tournee_begin_at: string;
}

export interface ICourseV2 {
  id_delivery: number;
  id_commande: number;
  status_delivery: number;
  delivery_info: IDeliveryInfo;
  alert_date: IDate;
  pickup_info: IPickupInfo;
  earnings: string;
  tips: string;
  distance: string;
}

export interface IDeliveryInfo {
  customer: string;
  delivery_address: string;
  delivery_date: IDate;
  delivery_hour: string;
}

export interface IPickupInfo {
  partner: string;
  pickup_address: string;
  pickup_date: IDate;
  pickup_hour: string;
}

export interface ICollectHour {
  day: IDate;
  planning: ICollectPlanning[];
}

export interface ICollectPlanning {
  begin: IDate;
  end: IDate;
  collect: IDate;
  delivery: IDate;
  collect_distance: number;
  delivery_distance: number;
  deliveryman: number;
}

///////////////////////////////////////////////////////////////////////////////

export interface IDay {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IPlanningRecoverHour {
  collecte: string;
  theoric_delivery: string;
  interval_delivery: string;
  begin_interval_delivery: string;
  last_interval_delivery: string;
  is_grouped: boolean;
  ouverture: boolean;
}

export interface IHour {
  day: IDay;
  planning: IPlanningRecoverHour[];
}

//////////////////////////////////// Bill ///////////////////////////////////////

export interface Date {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IPartnerBillData {
  id: number;
  reference: string;
  date: Date;
  type: string;
  amount: number;
  order_id: number;
  interval: IDateBillInterval;
  is_paied: boolean;
  action: string;
  action_at: IDate;
}

export interface IPartnerBill {
  total_data: string;
  total: number;
  data: IPartnerBillData[];
}

export interface IPartnerBillCustomerData {
  id: number;
  reference: string;
  date: IDate;
  type: string;
  amount: number;
  order_id: number;
}

export interface ICustomerBillData {
  total_data: string;
  total: number;
  data: IPartnerBillCustomerData[];
}

export interface IDateBillInterval {
  begin: IDate;
  last: IDate;
}

////////////////////////// Partner Customer ///////////////////////////////

export interface ILastOrderAt {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IPartnerCustomerOrder {
  nb_orders: number;
  nb_cancelled_orders: number;
  ca: number;
  last_order_at: ILastOrderAt;
}

export interface IPartnerCustomerData {
  id: number;
  first_name: string;
  last_name: string;
  city: string;
  average_cart: number;
  order: IPartnerCustomerOrder;
}

export interface IPartnerCustomer {
  total_data: number;
  total: number;
  data: IPartnerCustomerData[];
}

//////////// Financial Historique /////////////////

export interface IHistory {
  date: string;
  hour: string;
  description: string;
  commande_id: string;
  datetime: {
    date: string;
    timezone_type: number;
    timezone: string;
  };
  penalty: number;
}

export interface IFinancialImpact {
  base_bonus: number;
  month_penalty: number;
  total_bonus: number;
  historique: IHistory[];
}

export interface IContact {
  first_name: string;
  email: string;
  mobile: string;
}

export interface ClientDiscussionCard {
  id_command: number;
  nom_client: string;
  last_message_at: string;
}

// Profile Info
export interface ProfileLegalInfo {
  homepage_title: string;
  business_name: string;
  company_name: string;
  siret: string;
  address: string;
  telephone: string;
  capital: string;
  rcs: string;
  intra_community_vat: string;
}

export interface ProfilePersonalInfo {
  mobile: string;
  civility: string;
  last_name: string;
  first_name: string;
  email: string;
  dob: string;
  dob_date: string;
}

export interface ProfileInformations {
  legal_informations: ProfileLegalInfo;
  personal_informations: ProfilePersonalInfo;
}

// Admin and justif
export interface BankingInformations {
  iban: string;
  swift: string;
  bank_name: string;
  frequency: string;
}

export interface DocumentInformations {
  front_identity_card: boolean;
  back_identity_card: boolean;
  rib: false;
  kbis: false;
  contract: false;
}

export interface AdminJusti {
  banking_informations: BankingInformations;
  document_informations: DocumentInformations;
}

export interface UniversType {
  id: number;
  name: string;
  is_activated: boolean;
}

export interface GeneralInfo {
  account_id: number;
  print_reference: string;
  is_assujetti: boolean;
  is_order_auto_valid: boolean;
  print_only_daily_order: boolean;
  has_order_supervision_page: boolean;
}

export interface TvaInfo {
  tva_rate_1: number;
  tva_rate_2: number;
  tva_rate_3: number;
}

export interface Collaborator {
  id: number;
  name: string;
  email: string;
  options: string[];
}

export interface ParametersDetails {
  general_informations: GeneralInfo;
  tva_informations: TvaInfo;
  collab_informations: Collaborator;
  order_quota: [];
}

export interface NewCollaborator {
  first_name: string;
  last_name: string;
  email: string;
  telephone: string;
}

export interface WeekSchedule {
  midi(midi: any): unknown;
  soir(soir: any): unknown;
  day: {
    midi: string[] | string;
    soir: string[] | string;
  };
}
