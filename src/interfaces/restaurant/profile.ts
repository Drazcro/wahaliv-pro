import { IDate } from "src/interfaces/common";

interface IPartner {
  name: string;
  telephone: string;
  address: string;
  is_open: boolean;
  can_print: boolean;
}
interface IState {
  rate_client: string;
  rate_count: number;
  order_count: string;
  ca: string;
  average_delay: string;
  nb_transactions: number;
}

export interface IPartnerProfile {
  partner: IPartner;
  stats: IState;
}

export interface ProfileLegalInfo {
  homepage_title: string;
  business_name: string;
  company_name: string;
  siret: string;
  address: string;
  telephone: string;
  capital: string;
  rcs: string;
  intra_community_vat: string;
}

export interface ProfilePersonalInfo {
  mobile: string;
  civility: string;
  last_name: string;
  first_name: string;
  email: string;
  dob: string;
  dob_date: string;
}

export interface ProfileInformations {
  legal_informations: ProfileLegalInfo;
  personal_informations: ProfilePersonalInfo;
}

// Admin and justif
export interface BankingInformations {
  iban: string;
  swift: string;
  bank_name: string;
  frequency: string;
}

export interface DocumentInformations {
  recto: boolean;
  verso: boolean;
  rib: false;
  kbis: false;
  contract: false;
}

export interface AdminJusti {
  banking_informations: BankingInformations;
  document_informations: DocumentInformations;
}

export interface UniversType {
  id: number;
  name: string;
  is_activated: boolean;
}

export interface GeneralInfo {
  account_id: number;
  print_reference: string;
  is_assujetti: boolean;
  is_order_auto_valid: boolean;
  print_only_daily_order: boolean;
  has_order_supervision_page: boolean;
}

export interface TvaInfo {
  tva_rate_1: number;
  tva_rate_2: number;
  tva_rate_3: number;
}

export interface Collaborator {
  id: number;
  name: string;
  email: string;
  options: string[];
}

export interface ParametersDetails {
  general_informations: GeneralInfo;
  tva_informations: TvaInfo;
  collab_informations: Collaborator;
  order_quota: [];
}

export interface NewCollaborator {
  first_name: string;
  last_name: string;
  email: string;
  telephone: string;
}

export interface WeekSchedule {
  midi(midi: any): unknown;
  soir(soir: any): unknown;
  day: {
    midi: string[] | string;
    soir: string[] | string;
  };
}

export interface IPartnerProfileEarlyClosing {
  id: string;
  begin_date: IDate;
  end_date: IDate;
}

export interface IPartnerProfileUpdateDate {
  begin_date: string;
  end_date: string;
  id?: number | null;
}

export type IPartnerProfileEarlyClosings = Array<IPartnerProfileEarlyClosing>;
