import { IDate } from "src/interfaces/common";

export interface IPayementDetails {
  type: string;
  amount: string;
  frais: string;
}

export interface IPromoCode {
  amount: string;
  by: string;
  promo_code: string;
}

export interface IOrderProducts {
  commande: Array<IOrder>;
  product_details: Array<IProductDetails>;
}

export interface IProductDetails {
  id: number;
  name: string;
  qte: number;
  quantity: number;
  price: string;
  is_refund: boolean;
  isRefund: boolean;
  sub_products?: IProductDetails[];
}

export interface ICommande {
  id: number;
  pickup_date: IDate;
  delivery_date: IDate;
  deliveryman: string;
  comment: string;
  status: number;
  service_type: string;
  is_checked: boolean;
}

export interface IOrder {
  commande: ICommande;
  customer: { name: string };
  product_details: Array<IProductDetails>;
  total_amount: string;
  promo_code: IPromoCode;
  delivery_fees: string;
  paiement_details: Array<IPayementDetails>;
  newPrice: string;
}

export interface IPartnerOrderData {
  total_data: number;
  total: string;
  data: IOrder[];
}

export enum ORDER_STATUS {
  NEW = 5,
  ACCEPTED = 4,
  DELIVERED = 1,
  CANCELLED = 2,
  ALL = 0,
  IN_DELIVERY = "Livraison",
  IN_TAKE_OUT = "A emporter",
}

export interface IOrderRefund {
  products: Array<number>;
  supplements: Array<number>;
}

export interface IOrderTradeDiscount {
  amount: number;
  comment: string;
}

export interface IDeliveryInfo {
  customer: string;
  delivery_address: string;
  delivery_date: IDate;
  delivery_hour: string;
}

export interface IPickupInfo {
  partner: string;
  pickup_address: string;
  pickup_date: IDate;
  pickup_hour: string;
}

export interface ICollectHour {
  day: IDate;
  planning: ICollectPlanning[];
}

export interface ICollectPlanning {
  begin: IDate;
  end: IDate;
  collect: IDate;
  delivery: IDate;
  collect_distance: number;
  delivery_distance: number;
  deliveryman: number;
}

export interface IHour {
  day: IDate;
  planning: IPlanningRecoverHour[];
}

export interface IPlanningRecoverHour {
  collecte: string;
  theoric_delivery: string;
  interval_delivery: string;
  begin_interval_delivery: string;
  last_interval_delivery: string;
  is_grouped: boolean;
  ouverture: boolean;
}

export interface IPartnerCustomerOrder {
  nb_orders: number;
  nb_cancelled_orders: number;
  ca: number;
  last_order_at: IDate;
}

export interface IPartnerCustomerData {
  id: number;
  first_name: string;
  last_name: string;
  city: string;
  average_cart: number;
  order: IPartnerCustomerOrder;
}

export interface IPartnerCustomer {
  total_data: number;
  total: number;
  data: IPartnerCustomerData[];
}
