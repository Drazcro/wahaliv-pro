export type ClientFilterType =
  | "city"
  | "avg_kart"
  | "nb_command"
  | "nb_cancel"
  | "last_command"
  | "turnover";
