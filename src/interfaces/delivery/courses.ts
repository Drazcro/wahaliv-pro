import { IDeliveryInfo } from "src/interfaces/delivery/profile";
import { IAddress, IDate } from "src/interfaces/common";
import { COURSE_STATUS } from "src/constants/course";
import { IInvoiceResume } from "./finance";

export interface ICourseData {
  id_delivery: number;
  id_commande: number;
  status_delivery: number;
  delivery_info: IDeliveryInfo;
  alert_date: IDate;
  pickup_info: IPickupInfo;
  earnings: string;
  tips: string;
  distance: string;
}

export interface ICourse {
  total_data: number;
  total: number;
  data: ICourseData[];
  hasTournee: boolean;
  tournee_begin_at: string;
}

export interface IPickupInfo {
  partner: string;
  pickup_address: string;
  pickup_date: IDate;
  pickup_hour: string;
}

export interface ICollectInfo {
  day: IDate;
  planning: ICollectPlanning[];
}

export interface ICollectPlanning {
  begin: IDate;
  end: IDate;
  collect: IDate;
  delivery: IDate;
  collect_distance: number;
  delivery_distance: number;
  deliveryman: number;
}

export interface IOldCourse {
  // customer
  customer: string;
  customer_name?: string;
  customer_lat: number;
  customer_lng: number;
  customer_telephone: string;
  // delivery
  delivery_address: IAddress;
  delivery_amount: number;
  delivery_distance: number;
  delivery_date: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  delivery_status: COURSE_STATUS;
  details: {
    name: string;
    price: number;
    qte: number;
  }[];
  // commande
  id_commande: number;
  id_delivery: number;
  // partner
  partner: string;
  partner_name?: string;
  partner_lat: number;
  partner_lng: number;
  partner_telephone: string;
  // pickup
  pickup_address: string;
  pickup_date: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  isRetrievable: boolean;
  isDisplayed: boolean;
  // new
  real_delivery_date: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  commande_at: {
    date: string;
    timezone: string;
    timezone_type: number;
  };
  isRefund: boolean;
  isGrouped: boolean;
  tip: number;
  comment: string;
  groupedCommandIds: Array<number>;
  need_real_price: boolean;
  newPrice: string;
}

export interface IOldCourseDetails {
  id_delivery: number;
  id_commande: number;
  distance: number;
  delivery_date: IDate;
  total: number;
  total_customer: number;
  total_mp: number;
  bonus: number;
  tip: number;
}

export interface IOldCourseAPIResponse {
  date: IDate;
  global: IInvoiceResume;
  courses: IOldCourseDetails[];
}
