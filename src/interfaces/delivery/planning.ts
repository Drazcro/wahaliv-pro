export interface IPlanningRecoverHour {
  collecte: string;
  theoric_delivery: string;
  interval_delivery: string;
  begin_interval_delivery: string;
  last_interval_delivery: string;
  is_grouped: boolean;
  ouverture: boolean;
}

export interface IPlanningSummaryData {
  start_time: string;
  end_time: string;
  hour: number;
  id: number;
}

export interface IPlanningSummary {
  id: number;
  day_str: string;
  day: string;
  total_hour: number;
  data: Array<IPlanningSummaryData>;
}

export type IPlanningsSummary = Array<IPlanningSummary>;

export interface IPlanning {
  id: number;
  time: string;
  state: string;
  id_planning?: number;
  id_creneaux?: number;
}

export type IPlannings = Array<IPlanning>;

export interface IPlanningDisengageStatus {
  is_course_in_progress: boolean;
  is_able_to_disengage: boolean;
}
