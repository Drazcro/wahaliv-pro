// ---------------------------------------- //
// ------------  TOURNEES ----------------- //
// ---------------------------------------- //

import { Float } from "react-native/Libraries/Types/CodegenTypes";
import { IDate } from "src/interfaces/common";

export interface ITournee {
  id: number;
  begin_hour: string;
  end_hour: string;
  status: number;
  orders_count: number;
  earnings: Float;
  validation_hour_limit: string;
  begin_date: IDate;
}

export interface ITourneeAPIResponse {
  total_data: number;
  total: string;
  data: ITournee[];
}

interface IOrderDetailSubProduct {
  id: number;
  name: string;
  price: string;
  quantity: number;
}

export interface IOrderDetail {
  id: number;
  qte: number;
  name: string;
  price: string;
  sub_products: IOrderDetailSubProduct[];
}

interface IStepDeliveryAddress {
  address: string;
  postalCode: string;
  numerorue: string;
  complementadresse: string;
  appartement: string;
  etage: string;
  batiment: string;
  code_porte: string;
}
interface IStepTarget {
  name: string;
  address: string;
  lat: Float;
  lng: Float;
  telephone: string;
  delivery_adress?: IStepDeliveryAddress;
  adress: string;
}

export interface IStep {
  step_id: number;
  order_id: number;
  course_id: number;
  order_comment: string;
  position: number;
  order_is_last_moment?: boolean;
  hour: string;
  type: string;
  step_status: number;
  state: string;
  target: IStepTarget;
  tip: string;
  order_details: IOrderDetail[];
  pickup_indication: string;
}

export interface ITourneeDetail {
  id: number;
  begin_hour: string;
  end_hour: string;
  status: number;
  orders_count: number;
  steps_count: number;
  earnings: Float;
  tips: string;
  distance: string;
  steps: IStep[];
}
