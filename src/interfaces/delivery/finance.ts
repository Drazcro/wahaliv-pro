import { IDate } from "src/interfaces/common";

export interface IInvoiceDataCourse {
  id_delivery: number;
  id_commande: number;
  distance: number;
  delivery_date: IDate;
  total: number;
  total_customer: number;
  total_mp: number;
  bonus: number;
  tip: number;
}
export interface IDayInvoice {
  date: IDate;
  global: IInvoiceResume;
  courses: IInvoiceDataCourse[];
}

export interface IInvoiceAPIResponse {
  total_tip: number;
  total_bonus: number;
  global_data: IDayInvoice[];
}
export interface IInvoiceResume {
  distance: number;
  total_customer: number;
  total_mp: number;
  total: number;
  bonus: number;
  tip: number;
}

export interface IBankInfo {
  banque: string;
  frequence: string;
  iban: string;
  swift: string;
  titulaireCompte: string;
}

export interface IReverseEarnings {
  canPayback: boolean;
  isSalarie: boolean;
  paybackAmount: number;
}

export interface IBillsDetails {
  begin_date: IDate;
  end_date: IDate;
  deliveryman_bill_id: number;
  edition_date: IDate;
  paiement_date: IDate;
  deliveryman_bill_amount: number;
}
