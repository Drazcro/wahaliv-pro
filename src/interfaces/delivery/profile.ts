import { IDate } from "src/interfaces/common";

export interface IDeliveryInfo {
  customer: string;
  delivery_address: string;
  delivery_date: IDate;
  delivery_hour: string;
}

export interface IMGEntry {
  amount: number;
  days: string;
  hours: {
    from: string;
    to: string;
  };
  nb_services: number;
  nb_services_done: number;
  specific_day_nb_services: number;
}
export interface IMinimumGaranti {
  soir?: IMGEntry;
  midi?: IMGEntry;
}

export interface IDeliveryProfile {
  autoValid: boolean;
  averageDelay: number;
  bonus: number;
  coursesCount: number;
  earnings: number;
  email: string;
  firstName: string;
  first_name: string;
  isSalarie: boolean;
  lastName: string;
  last_name: string;
  minimum_garanti: IMinimumGaranti;
  name: string;
  nbTransactions: number;
  nb_services: number | null;
  nb_services_done: number;
  phoneNumber: string;
  profilPicturePath: string;
  rateCount: number;
  ratePercentage: number;
  tipEarning: number;
}

export interface IDeliveryProfileInformation {
  id: number;
  address: string;
  civility: string;
  company_name?: string | undefined;
  dob: string;
  email: string;
  firstName: string;
  first_name: string;
  idCity: number;
  id_city: number;
  isSalarie: boolean;
  lastName?: string | undefined;
  last_name?: string | undefined;
  name?: string | undefined;
  siret: string;
  telephone: string;
}

export interface ProfileNavigationProps {
  title: string;
  iconLeft: JSX.Element;
  iconRight?: JSX.Element;
  onPress: () => void;
  isSalarie: boolean;
  index: number;
}

export interface IDocumentJustificatif {
  contract: boolean;
  identityRecto: boolean;
  identityVerso: boolean;
  registration_proof: boolean;
  rib: boolean;
}
