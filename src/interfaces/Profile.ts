export interface ProfileNavigationProps {
  title: string;
  iconLeft: JSX.Element;
  iconRight?: JSX.Element;
  onPress: () => void;
  isSalarie: boolean;
  index: number;
}
