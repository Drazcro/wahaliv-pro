export interface IDate {
  date: string;
  timezone_type: number;
  timezone: string;
}

export interface IAddress {
  address: string;
  complementadresse: string;
  numerorue: number;
  postalCode: string;
}

export interface IMessage {
  content: string;
  sent_at: string;
  sent_by_user: boolean;
  read: boolean;
  timestamp: string;
  sender: string;
  sent_by_client: boolean;
}

export interface IContact {
  first_name: string;
  email: string;
  mobile: string;
}
