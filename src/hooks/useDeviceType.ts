import * as React from "react";
import * as Device from "expo-device";

export default function useDeviceType() {
  const [isTablet, setIsTablet] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function getDeviceType() {
      try {
        const deviceType = await Device.getDeviceTypeAsync();
        // Tablet ou TV ou Desktop
        setIsTablet(deviceType != Device.DeviceType.PHONE);
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      }
    }

    getDeviceType();
  }, []);

  return isTablet;
}
