import { BackgroundFetchStatus } from "expo-background-fetch";
import React from "react";
import { Platform } from "react-native";
import {
  getTrackingTaskStatus,
  isTrackingTaskRegistered,
  registerBackgroundFetchAsync,
  unregisterBackgroundFetchAsync,
} from "src/cron/foreground_localisation_task";
import { authStore } from "src/services/auth_store";
import { ROLE_TYPE } from "src/services/types";

export function useTrackLocation() {
  const [isRegistered, setIsRegistered] = React.useState(false);
  const [status, setStatus] = React.useState<null | BackgroundFetchStatus>(
    null,
  );
  //console.log("background status ",status);
  const { base_user } = authStore();

  const checkStatusAsync = async () => {
    const status = await getTrackingTaskStatus();
    //console.log("Tracking task status",status);
    const registered = await isTrackingTaskRegistered();
    //console.log("isTrackingTaskRegistered",registered);
    setStatus(status);
    setIsRegistered(registered);
    return registered;
  };

  const toggleFetchTask = async () => {
    if (Platform.OS !== "web") {
      if (isRegistered) {
        console.log("is registered");

        await unregisterBackgroundFetchAsync();
      } else {
        console.log("is not registered");

        if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
          await registerBackgroundFetchAsync();
        }
      }
      checkStatusAsync();
    }
  };

  return {
    checkStatusAsync,
    toggleFetchTask,
    isRegistered,
  };
}
