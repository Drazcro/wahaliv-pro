import React from "react";
import { PermissionsAndroid } from "react-native";
import RNCallKeep from "react-native-callkeep";

const options = {
  ios: {
    appName: "Sacre Armand Pro",
  },
  android: {
    alertTitle: "Permission requise",
    alertDescription:
      "Cette application veut votre permission pour passer des appels",
    cancelButton: "Refuser",
    okButton: "Accepter",
    imageName: "ic_notification48",
    additionalPermissions: [],
  },
};

RNCallKeep.setup(options).then((accepted) => {
  console.log("RN CALL KEEP STATUS", accepted.toString());
});
// export default function useCall(){

// }
