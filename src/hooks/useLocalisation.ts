import * as React from "react";
import * as Location from "expo-location";
import { useFocusEffect } from "expo-router";
//import * as Sentry from "@sentry/react-native";
import { useAppState } from "src/hooks/useAppState";

type BGResponse = Location.PermissionResponse;
type FGResponse = Location.PermissionResponse & {
  ios?: Location.PermissionDetailsLocationIOS | undefined;
  android?: Location.PermissionDetailsLocationAndroid | undefined;
};
const DEFAULT_PERMISSION: Location.PermissionResponse = {
  status: Location.PermissionStatus.UNDETERMINED,
  granted: false,
  canAskAgain: false,
  expires: 0,
};
export function useGPS() {
  const [isLoading, setIsLoading] = React.useState(true);
  const [isGPSOn, setIsGPSOn] = React.useState(false);
  const [BGLocation, setBGLocation] = React.useState<BGResponse>(
    DEFAULT_PERMISSION as BGResponse,
  );
  const [FGLocation, setFGLocation] = React.useState<FGResponse>(
    DEFAULT_PERMISSION as FGResponse,
  );

  const appState = useAppState();

  const check = React.useCallback(() => {
    Promise.all([
      Location.getBackgroundPermissionsAsync(),
      Location.getForegroundPermissionsAsync(),
      Location.hasServicesEnabledAsync(),
    ])
      .then((statuses) => {
        setBGLocation(statuses[0]);
        setFGLocation(statuses[1]);

        setIsGPSOn(statuses[2]);
      })
      .catch((e) => {
        setBGLocation(DEFAULT_PERMISSION);
        setFGLocation(DEFAULT_PERMISSION);
        console.error(e);
        //Sentry.captureException(e);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, []);

  React.useEffect(() => {
    check();
  }, [appState]);

  useFocusEffect(check);

  return {
    isLoading,
    isGPSOn,
    check,
    BGLocation,
    FGLocation,
  };
}
