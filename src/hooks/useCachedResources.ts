import * as SplashScreen from "expo-splash-screen";
import * as React from "react";
import * as Font from "expo-font";

export default function useCachedResources() {
  const [isLoadingComplete, setLoadingComplete] = React.useState(false);

  // Load any resources or data that we need prior to rendering the app
  React.useEffect(() => {
    async function loadResourcesAndDataAsync() {
      try {
        SplashScreen.preventAutoHideAsync();

        // Load fonts
        await Font.loadAsync({
          //...FontAwesome.font,
          "Montserrat-SemiBold": require("assets/fonts/Montserrat-SemiBold.ttf"),
          "Montserrat-SemiBoldItalic": require("assets/fonts/Montserrat-SemiBoldItalic.ttf"),
          "Montserrat-Black": require("assets/fonts/Montserrat-Black.ttf"),
          "Montserrat-BlackItalic": require("assets/fonts/Montserrat-BlackItalic.ttf"),
          "Montserrat-Bold": require("assets/fonts/Montserrat-Bold.ttf"),
          "Montserrat-ExtraBold": require("assets/fonts/Montserrat-ExtraBold.ttf"),
          "Montserrat-ExtraBoldItalic": require("assets/fonts/Montserrat-ExtraBoldItalic.ttf"),
          "Montserrat-ExtraLightItalic": require("assets/fonts/Montserrat-ExtraLightItalic.ttf"),
          "Montserrat-ExtraLight": require("assets/fonts/Montserrat-ExtraLight.ttf"),
          "Montserrat-Light": require("assets/fonts/Montserrat-Light.ttf"),
          "Montserrat-LightItalic": require("assets/fonts/Montserrat-LightItalic.ttf"),
          "Montserrat-Italic": require("assets/fonts/Montserrat-Italic.ttf"),
          "Montserrat-Medium": require("assets/fonts/Montserrat-Medium.ttf"),
          "Montserrat-MediumItalic": require("assets/fonts/Montserrat-MediumItalic.ttf"),
          "Montserrat-Regular": require("assets/fonts/Montserrat-Regular.ttf"),
          "Montserrat-Thin": require("assets/fonts/Montserrat-Thin.ttf"),
          "Montserrat-ThinItalic": require("assets/fonts/Montserrat-ThinItalic.ttf"),
        });
      } catch (e) {
        // We might want to provide this error information to an error reporting service
        console.warn(e);
      } finally {
        setLoadingComplete(true);
        SplashScreen.hideAsync();
      }
    }

    loadResourcesAndDataAsync();
  }, []);

  return isLoadingComplete;
}
