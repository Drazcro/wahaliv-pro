import React from "react";
import * as Notifications from "expo-notifications";
import * as Device from "expo-device";

const NOT_A_DEVICE = "Must use physical device for Push Notifications";

type NotifStatus = Notifications.NotificationPermissionsStatus;
enum PermissionStatus {
  /**
   * User has granted the permission.
   */
  GRANTED = "granted",
  /**
   * User hasn't granted or denied the permission yet.
   */
  UNDETERMINED = "undetermined",
  /**
   * User has denied the permission.
   */
  DENIED = "denied",
}
const DEFAULT: NotifStatus = {
  status: PermissionStatus.UNDETERMINED,
  canAskAgain: true,
  granted: false,
  expires: 0,
};

export default function useNotification() {
  const [push_token, set_push_token] = React.useState("");
  const [status, set_status] = React.useState<NotifStatus>(DEFAULT);

  React.useEffect(() => {
    if (status.status == PermissionStatus.GRANTED) {
      return;
    }

    getPushToken()
      .then((token) => set_push_token(token))
      .catch((e) => {
        if (e != NOT_A_DEVICE) {
          console.error(e);
        }
      });
  }, [status]);

  React.useEffect(() => {
    check();
  }, []);

  const check = React.useCallback(async () => {
    const response = await Notifications.getPermissionsAsync();
    set_status(response);
  }, []);

  const request = React.useCallback(async () => {
    const response = await Notifications.requestPermissionsAsync();
    set_status(response);
    check();
    return Promise.resolve(response.granted);
  }, [check]);

  return {
    push_token,
    notif: status,
    request,
    check,
  };
}

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const getPushToken = () => {
  if (!Device.isDevice) {
    return Promise.reject(NOT_A_DEVICE);
  }

  try {
    return Notifications.getExpoPushTokenAsync({
      //applicationId: "@helodev/sacrearmand_pro_app",
      projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
    }).then((tokenData) => tokenData.data);
  } catch (error) {
    return Promise.reject("Couldn't check notifications permissions");
  }
};
