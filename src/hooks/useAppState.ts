import { useEffect, useRef, useState } from "react";
import { AppState } from "react-native";
import React from "react";
import moment from "moment";
import { ref, update } from "firebase/database";
import { db } from "firebase";

import { ACCOUNT_TYPE, authStore } from "src/services/auth_store";
import { getBaseUser } from "src/services";

export const useAppState = () => {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const { auth, account_type } = authStore();

  useEffect(() => {
    const subscription = AppState.addEventListener("change", (nextAppState) => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === "active"
      ) {
        console.log("App has come to the foreground!");
      } else {
        console.log("App has went to the background!");
      }
      const base_user = getBaseUser(auth);
      if (
        base_user.id == -1 ||
        !auth.access_token ||
        account_type == ACCOUNT_TYPE.UNKNOWN
      ) {
        return;
      }

      const role = base_user.roles.filter(
        (item) => item !== "ROLE_PARTICULIER",
      )[0];
      const dbRef = ref(db, `/status/${role}/` + base_user.id);
      const updates = {
        email: base_user.email,
        last_seen: moment().format(),
        online: false,
        id: base_user.id,
        //full_name: `${base_user}`,
      };
      if (nextAppState === "active") {
        updates.online = true;
      }
      if (nextAppState === "active") {
        updates.online = true;
      }

      update(dbRef, updates);
      appState.current = nextAppState;
      setAppStateVisible(nextAppState);
    });

    return () => {
      subscription.remove();
    };
  }, [auth, account_type]);

  return appStateVisible;
};
