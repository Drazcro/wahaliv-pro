import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";

import { fontFamily } from "src/theme/typography";
import { AdminJusti } from "src/interfaces/restaurant/profile";
import {
  get_profile_admin_and_justification,
  update_profile_admin_and_justification,
} from "src/api/restaurant/profile";
import BandDetails from "src/components/restaurant/sub_menu/administratif/bank_details";
import JustitifcationDocuments from "src/components/restaurant/sub_menu/administratif/doc";
import AdminitratifOverlay from "src/components/restaurant/overlay/admin_overlay";
import { color } from "src/theme";

const Administratif = () => {
  const [open, setOpen] = useState(false);
  const [adminData, setAdminData] = useState({} as AdminJusti);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function updateProfileAdminAndJusti() {
    update_profile_admin_and_justification(adminData.banking_informations)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.error(err));
  }

  useEffect(() => {
    get_profile_admin_and_justification()
      .then((res) => {
        console.log(res);
        setAdminData(res);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <View style={styles.container}>
      <AdminitratifOverlay open={open} closeOverlay={handleClose} />
      <ScrollView showsVerticalScrollIndicator={false}>
        <BandDetails
          bankData={adminData.banking_informations}
          setBankData={setAdminData}
        />
        <JustitifcationDocuments openOverlay={handleOpen} data={adminData} />
        <View
          style={{
            marginTop: 40,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            style={styles.submitBtn}
            onPress={updateProfileAdminAndJusti}
          >
            <Text
              style={{
                color: "#fff",
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
              }}
            >
              Enregistrer les modifications
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Administratif;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "80%",
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    borderRadius: 30,
  },
});
