import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native";
import { ScrollView, StyleSheet, Text, View } from "react-native";

import { fontFamily } from "src/theme/typography";
import { ProfilePicture } from "src/components/restaurant/sub_menu/profile/picture";
import InformationJuridique from "src/components/restaurant/sub_menu/profile/juridique";
import Interlocuteur from "src/components/restaurant/sub_menu/profile/interlocuteur";
import { ProfileInformations } from "src/interfaces/restaurant/profile";
import {
  get_partenaire_profile_information,
  update_partenaire_profile_information,
} from "src/api/restaurant/profile";
import { french_to_api_date } from "src/services/helpers";
import { color } from "src/theme";

export const PartenaireProfile = () => {
  const [profileInfo, setProfileInfo] = useState({} as ProfileInformations);
  const [loading, setLoading] = useState(false);

  function flattenProfileData(profileData: any) {
    const flattenedData = {
      ...profileData.legal_informations,
      ...profileData.personal_informations,
    };
    return flattenedData;
  }

  const load_profile_data = () => {
    try {
      setLoading(true);
      get_partenaire_profile_information()
        .then((res) => {
          console.log(res);
          setProfileInfo(res as ProfileInformations);
        })
        .catch((err) => console.error(err))
        .finally(() => {
          setLoading(false);
        });
    } catch (error) {
      console.error(error);
    }
  };

  const updateProfileInfo = () => {
    const profileBody = flattenProfileData(profileInfo);
    const formatted = french_to_api_date(profileBody.dob);
    profileBody.dob = formatted;
    profileBody.telephone = profileBody.telephone.replaceAll(" ", "");
    profileBody.mobile = profileBody.mobile.replaceAll(" ", "");

    //console.log(profileBody)
    update_partenaire_profile_information(profileBody)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    load_profile_data();
  }, []);

  if (loading || profileInfo === null) {
    return (
      <View
        style={{
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>Chargement des données !</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <ProfilePicture
          pageTitle={profileInfo?.legal_informations?.homepage_title}
        />
        <InformationJuridique
          legalInfo={profileInfo.legal_informations}
          setLegalInfo={setProfileInfo}
        />
        <Interlocuteur
          personalInfo={profileInfo.personal_informations}
          setPersonalInfo={setProfileInfo}
        />
        <View
          style={{
            marginTop: 40,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity
            style={styles.submitBtn}
            onPress={updateProfileInfo}
          >
            <Text
              style={{
                color: "#fff",
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
              }}
            >
              Enregistrer les modifications
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    gap: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "80%",
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    borderRadius: 30,
  },
});
