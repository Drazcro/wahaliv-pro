import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import UsersManager from "src/components/restaurant/sub_menu/parametres/users_manager";
import { ParametersDetails } from "src/interfaces/restaurant/profile";
import { get_parameters_details } from "src/api/restaurant/profile";
import CommandsQuantityController from "src/components/restaurant/sub_menu/parametres/command_qty";
import { Tva } from "src/components/restaurant/sub_menu/parametres/tva";
import GeneralInformation from "src/components/restaurant/sub_menu/parametres/general";

const Parameters = () => {
  const [ParamsData, setParamsData] = useState({} as ParametersDetails);

  useEffect(() => {
    get_parameters_details()
      .then((res) => {
        setParamsData(res as unknown as ParametersDetails);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <GeneralInformation generalInfo={ParamsData.general_informations} />
        <Tva tvaInfo={ParamsData.tva_informations} />
        <CommandsQuantityController />
        <UsersManager collaborators={ParamsData?.collab_informations} />
      </ScrollView>
    </View>
  );
};

export default Parameters;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
