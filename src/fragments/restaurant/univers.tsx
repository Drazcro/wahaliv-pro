import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
// import { cuisineTypes } from "src/constants/DefaultValues";
import { fontFamily } from "src/theme/typography";
import { authStore } from "src/services/auth_store";
import { UniversType } from "src/interfaces/restaurant/profile";
import {
  deleteAll_univers,
  get_profile_univers,
  update_univers,
} from "src/api/restaurant/profile";
import popup from "src/components/toasts/popup";
import { color } from "src/theme";

const Univers = () => {
  const { base_user } = authStore();
  const [foodTypes, setFoodTypes] = useState<UniversType[]>([]);
  const [formData, setFormData] = useState(new FormData());
  const [selected, setSelected] = useState<number[]>([]);

  function updatedSelectedIds(typeId: number) {
    let updatedSelected = [];
    if (selected.includes(typeId)) {
      updatedSelected = selected.filter((i) => i != typeId);
    } else {
      updatedSelected = [...selected, typeId]; // Create a new array with the updated selected IDs
    }
    setSelected(updatedSelected);

    const updatedFormData = new FormData();
    updatedSelected.forEach((id, index) => {
      updatedFormData.append(`cuisine_ids[${index}]`, id); // Append with the specific format
    });

    setFormData(updatedFormData); // Update the FormData state directly
  }

  function updateUnivers() {
    if (selected.length === 0) {
      deleteAll_univers()
        .then((res) => {
          //console.log(res);
          setFoodTypes(res);
          popup.success({ text: "Enregistré" });
        })
        .catch((err) => {
          console.error(err);
          popup.error({ text: "Erreur" });
        });
    } else {
      update_univers(formData)
        .then((res) => {
          //console.log(res);
          setFoodTypes(res);
          popup.success({ text: "Enregistré" });
        })
        .catch((err) => {
          console.error(err);
          popup.error({ text: "Erreur" });
        });
    }
  }

  useEffect(() => {
    if (!base_user) {
      return;
    }

    get_profile_univers()
      .then((data) => {
        setFoodTypes(data as UniversType[]);
        const newSelected: number[] = [];
        data?.forEach((univer) => {
          if (univer.is_activated) newSelected.push(univer.id);
        });
        setSelected([...newSelected]);
      })
      .catch((err) => {
        console.error(err);
      });
  }, [base_user?.id]);

  return (
    <View style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.typesSelectionContainer}>
          <Text style={styles.title}>Type de cuisine</Text>
          <Text style={styles.caption}>
            Choisissez jusqu'à trois options de types de cuisine qui
            représentent le style de cuisine de votre establishment.
          </Text>
          <View
            style={{
              marginTop: 33,
            }}
          >
            {foodTypes.map((type) => (
              <TouchableOpacity
                key={type.id}
                style={styles.cuisineType}
                onPress={() => updatedSelectedIds(type.id)}
              >
                <View
                  style={
                    selected.includes(type.id)
                      ? styles.checkIndicatorActivated
                      : styles.checkIndicatorInactivated
                  }
                ></View>
                <Text style={styles.typeLabel}>{type.name}</Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
      </ScrollView>
      <View
        style={{
          marginTop: 20,
          width: "80%",
        }}
      >
        <TouchableOpacity style={styles.submitBtn} onPress={updateUnivers}>
          <Text
            style={{
              color: "#fff",
              fontFamily: fontFamily.semiBold,
              fontSize: 16,
            }}
          >
            Enregistrer les modifications
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Univers;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  typesSelectionContainer: {
    width: 355,
    backgroundColor: "#fff",
    marginTop: 8,
    paddingTop: 20,
    paddingLeft: 14,
    borderRadius: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    lineHeight: 20,
    color: "#1D2A40",
  },
  caption: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    lineHeight: 20,
    color: "#1D2A40",
    marginTop: 12,
  },
  cuisineType: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginBottom: 33,
  },
  checkIndicatorInactivated: {
    // backgroundColor: ""
    width: 22,
    height: 22,
    borderWidth: 1,
    borderRadius: 22,
    borderColor: "#112842",
    marginRight: 28,
  },

  checkIndicatorActivated: {
    backgroundColor: color.ACCENT,
    width: 22,
    height: 22,
    borderRadius: 22,
    marginRight: 28,
  },

  typeLabel: {
    marginLeft: 19.41,
    fontSize: 16,
    fontFamily: fontFamily.regular,
    lineHeight: 20,
    color: "#030507",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    borderRadius: 30,
  },
});
