import React from "react";
import { StyleSheet, View } from "react-native";
import { Stack } from "expo-router";
import { HeaderBack } from "src/components/router";
import FaqComponent from "src/components/common/faq_component";

export default function FaqScreen() {
  return (
    <View style={styles.container}>
      <Stack.Screen
        options={{
          title: "Aide et contact",
          headerLeft: HeaderBack,
        }}
      />
      <FaqComponent />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
  },
});
