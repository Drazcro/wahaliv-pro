import { Stack, usePathname, useRootNavigation, useRouter } from "expo-router";
import React from "react";
import { HeaderBack } from "src/components/router";
import { authStore, ACCOUNT_TYPE } from "src/services/auth_store";

export default function RootLayout() {
  const { auth, account_type } = authStore();
  const navigation = useRootNavigation();
  const router = useRouter();
  const path = usePathname();

  React.useEffect(() => {
    if (!navigation?.isReady()) return;

    if (account_type === ACCOUNT_TYPE.UNKNOWN) {
      router.replace("/auth");
    }
  }, [auth, account_type, auth.access_token, auth.is_loading, path, router]);

  return (
    <Stack
      screenOptions={{
        headerLeft: HeaderBack,
      }}
    />
  );
}
