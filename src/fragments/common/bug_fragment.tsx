import React from "react";
import { KeyboardAvoidingView, Platform, StyleSheet, View } from "react-native";
import { Stack } from "expo-router";
import { BugComponent } from "src/components/common/bug_component";

export default function BugFragment() {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <Stack.Screen
        options={{
          title: "Informer un bug",
        }}
      />
      <View style={styles.bugComponentPosition}>
        <BugComponent />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  bugComponentPosition: {
    height: "100%",
    backgroundColor: "white",
    marginTop: -10,
  },
});
