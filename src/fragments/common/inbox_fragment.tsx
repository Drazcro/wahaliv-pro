import React, { useMemo, useState } from "react";
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Divider } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import Search from "assets/image_svg/SearchIcon.svg";
import { Input } from "@rneui/themed";
import StartChatIcon from "assets/image_svg/StartChatIcon.svg";
import { Button, Text } from "react-native-elements";
import CaretLeft from "assets/image_svg/CaretLeft.svg";

import ClearSearchIcon from "assets/image_svg/ClearSearchIcon.svg";
import { Stack, router } from "expo-router";
import { onValue, ref } from "firebase/database";
import { db } from "firebase";
import { FlashList } from "@shopify/flash-list";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src/services/auth_store";
import { ROLE_TYPE } from "src/services/types";
import DiscussionSPCard from "src/components/common/chats";
import DiscussionCard from "src/components/common/discussion_card";

interface chat_metadata {
  id_command: number;
  id_client: string;
  id_livreur: number;
  id_partenaire: number;
  email_client: string;
  email_partenaire: string;
  email_livreur: string;
  nom_partenaire: string;
  nom_livreur: string;
  nom_client: string;
  titre: string;
  closed: boolean;
  last_message_at: any;
}

const InboxFragment = () => {
  const { auth, base_user } = authStore();

  const [searchedUser, setSearchedUser] = useState<string>("");
  const [discussions, setDiscussions] = useState<chat_metadata[]>([]);
  const [loading, setLoading] = useState(false);

  const handleSearchChange = (text: string) => {
    setSearchedUser(text);
  };

  React.useEffect(() => {
    const callbackList: (() => void)[] = [];
    setLoading(true);
    try {
      const db_ref = ref(db, `chat_all_My8xMS8yMDIz/${base_user.id}/`);
      const chat_metadata: chat_metadata[] = [];

      const unsubscribe = onValue(db_ref, (snapshot) => {
        if (!snapshot.exists()) {
          setLoading(false);
          return;
        }

        const data = Object.values(snapshot.val());

        data.forEach((chat: any) => {
          if (chat.chat_metadata.closed == false) {
            chat_metadata.push(chat.chat_metadata);
          }
        });

        const sorted_chats = chat_metadata.sort((a, b) => {
          const dateA: any = new Date(a.last_message_at);
          const dateB: any = new Date(b.last_message_at);
          return dateB - dateA;
        });

        setDiscussions(sorted_chats);
        setLoading(false);
      });

      callbackList.push(unsubscribe);
    } catch (error) {
      setLoading(false);
      console.error(error);
      //Sentry.captureException(error);
    } finally {
      setLoading(false);
    }

    return () => {
      for (const callback of callbackList) {
        callback();
      }
    };
  }, [discussions]);

  const filteredDiscussions = useMemo(() => {
    if (searchedUser === "") {
      return discussions;
    }

    const filtered_data = discussions.filter((item) => {
      return item.nom_client.includes(searchedUser);
    });

    return filtered_data ?? [];
  }, [searchedUser, discussions]);

  const onNavigateToChat = React.useCallback(() => {
    // console.log(base_user.roles, ROLE_TYPE.ROLE_COURSIER)
    // return
    if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
      router.push("/delivery/screens/chat_screen");
    } else {
      router.push("/partenaire_chat_screen");
    }
  }, [auth]);

  return (
    <SafeAreaView
      style={{
        backgroundColor: "#fff",
        height: "100%",
        flex: 1,
        //position: "relative",
      }}
    >
      <Stack.Screen
        options={{
          title: "Messagerie",
          headerRight: () => (
            <Button
              icon={<StartChatIcon />}
              type="clear"
              onPress={onNavigateToChat}
            />
          ),
        }}
      />

      {/* <Divider color="rgba(196, 196, 196, 1)" width={0.5} /> */}
      <View>
        <Input
          style={{ borderBottomWidth: 0 }}
          inputContainerStyle={styles.searchInputContainer}
          inputStyle={styles.searchInput}
          placeholder="Rechercher"
          value={searchedUser}
          onChangeText={handleSearchChange}
          leftIcon={<Search />}
          rightIcon={
            searchedUser !== "" ? (
              <TouchableOpacity
                style={{
                  right: 10,
                }}
                onPress={() => setSearchedUser("")}
              >
                <ClearSearchIcon />
              </TouchableOpacity>
            ) : null
          }
        />
      </View>
      {/* <ScrollView style={{ height: useWindowDimensions().height - 180 }}> */}
      {/* {filteredDiscussions.map((discussion, index) => ( */}
      <Divider width={1} />
      {loading === true ? (
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            height: 500,
          }}
        >
          <Text
            style={{
              fontFamily: fontFamily.Medium,
              marginTop: 8,
            }}
          >
            Chargement des discussions
          </Text>
        </View>
      ) : (
        <React.Fragment>
          <DiscussionSPCard />
          <View
            style={{
              height: "100%",
              width: Dimensions.get("screen").width,
            }}
          >
            <FlashList
              data={filteredDiscussions}
              estimatedItemSize={50}
              renderItem={({ item, index }) => (
                <DiscussionCard key={index} discussion={item} />
              )}
            />
          </View>
        </React.Fragment>
      )}
    </SafeAreaView>
  );
};

export default InboxFragment;

const styles = StyleSheet.create({
  searchInputContainer: {
    borderBottomWidth: 0,
    borderRadius: 20,
    paddingLeft: 15,
    marginTop: 20,
    height: 37,
    marginHorizontal: 29,
    backgroundColor: "rgba(231, 231, 231, 1)",
  },

  searchInput: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
    marginLeft: 5,
    color: "rgba(141, 152, 160, 1)",
  },

  screenHeader: {
    paddingTop: 50,
    // paddingBottom: 10,
    paddingHorizontal: 25,

    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  headerTitle: {
    // textAlign: "center",
    fontSize: 20,
    fontFamily: fontFamily.semiBold,
    marginLeft: 25,
  },
});
