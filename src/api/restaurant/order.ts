import {
  ICollectHour,
  IHour,
  IOrder,
  IOrderProducts,
  IOrderRefund,
  IPartnerOrderData,
  ORDER_STATUS,
} from "src/interfaces/restaurant/order";
import { vanillaAuthState } from "src/services/auth_store";
import { http } from "src/services/http";
import { vanillaPaginationStore } from "src/zustore/paginationstore";
import { vanillaPartnerStore } from "src/zustore/partnerstore";
import { vanillaSoundStore } from "src/zustore/sound_store";

const { setState: set_store } = vanillaPartnerStore;
export const INITIAL_PAGE = 1;
export const INTERVAL_PAGE = 15;

export async function get_new_orders_for_partner(
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const id = vanillaAuthState.getState().base_user.id;

  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.NEW}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        set_store({
          newOrders: orders_data.data,
        });

        vanillaPaginationStore
          .getState()
          .setTotalPagesValidated(parseInt(orders_data.total));
        try {
          if (orders_data.data.length > 0) {
            vanillaSoundStore.getState().playSound();
          } else {
            vanillaSoundStore.getState().stopSound();
          }
        } catch (err) {
          console.error(err);
        }
        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function get_accepted_orders_for_partner(
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.ACCEPTED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        set_store({ acceptedOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesAccepted(parseInt(orders_data.total));

        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function get_delivered_orders_for_partner(
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.DELIVERED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        set_store({ deliveredOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesDelivered(parseInt(orders_data.total));
        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function get_refused_orders_for_partner(
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.CANCELLED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        set_store({ cancelledOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesRefused(parseInt(orders_data.total));

        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export const reload_orders = async () => {
  try {
    await Promise.allSettled([
      await get_new_orders_for_partner(1, 100),
      await get_accepted_orders_for_partner(1, 100),
      await get_delivered_orders_for_partner(1, INTERVAL_PAGE),
      await get_refused_orders_for_partner(1, INTERVAL_PAGE),
    ]);
  } catch (e) {
    //console.error(e)
  }
  return Promise.resolve();
};

export async function get_order_details_for_partner(
  id: number,
): Promise<IOrder> {
  const url = `${vanillaAuthState.getState().base_url}commande/detail/${id}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const order: IOrder = response.data;
      return Promise.resolve(order);
    }
    return Promise.reject();
  });
}

export async function get_order_products(id: number): Promise<IOrderProducts> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}commande/products/${id}`).then((response) => {
    if (response && response.status === 200) {
      const products: IOrderProducts = response.data;
      return Promise.resolve(products);
    }
    return Promise.reject();
  });
}

export async function order_refund(id: number, payload: IOrderRefund) {
  const url = `${vanillaAuthState.getState().base_url}commande/refund/${id}`;

  return http.post({ url: url, data: { ...payload } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data.message);
    }
    return Promise.reject();
  });
}

export async function accept_cmd(id: string) {
  const url = `${vanillaAuthState.getState().base_url}commande/accept/${id}`;

  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      const message: string = response.data;
      return Promise.resolve(message);
    }
    return Promise.reject();
  });
}

export async function cancel_or_refuse_cmd(id: string) {
  const url = `${vanillaAuthState.getState().base_url}commande/cancel/${id}`;

  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      const message: string = response.data;
      return Promise.resolve(message);
    }
    return Promise.reject();
  });
}

export async function get_hours_collected(id: number): Promise<IHour> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}commande/hours-collecte/${id}`).then((response) => {
    if (response && response.status === 200) {
      const orders_data: IHour = response.data;
      return Promise.resolve(orders_data);
    }
    return Promise.reject();
  });
}

export async function get_hours_collected_v2(
  id: number,
): Promise<ICollectHour> {
  const url = `${
    vanillaAuthState.getState().base_url
  }v2/commande/hours-collecte/${id}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const res: ICollectHour = response.data;

      return Promise.resolve(res);
    }
    return Promise.reject();
  });
}

export async function update_hours_collected(
  id: number,
  hour: string,
  day: unknown,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }commande/update-collecte-hour/${id}`;

  return http.post({ url: url, data: { day, hour } }).then((response) => {
    if (response && response.status === 200) {
      vanillaPartnerStore.getState().updateCollecteHourById(id, hour);

      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

export async function update_hours_collected_v2(
  id: number,
  hour: string,
  begin: string,
  end: string,
  collecte: string,
  delivery: string,
  collecte_distance: number,
  delivery_distance: number,
  deliveryman: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }v2/commande/update-collecte-hour/${id}`;
  return http
    .post({
      url: url,
      data: {
        begin,
        end,
        collecte,
        delivery,
        collecte_distance,
        delivery_distance,
        deliveryman,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        vanillaPartnerStore.getState().updateCollecteHourById(id, hour);

        return Promise.resolve(true);
      }

      return Promise.reject();
    });
}
