/* eslint-disable @typescript-eslint/no-explicit-any */
import { getExpoPushTokenAsync } from "expo-notifications";
import { IContact } from "src/interfaces/common";
import { IPartnerCustomer } from "src/interfaces/restaurant/order";
import {
  AdminJusti,
  BankingInformations,
  IPartnerProfile,
  IPartnerProfileEarlyClosings,
  IPartnerProfileUpdateDate,
  NewCollaborator,
  ParametersDetails,
  ProfileInformations,
  UniversType,
  WeekSchedule,
} from "src/interfaces/restaurant/profile";
import { get_device_info } from "src/services";
import { vanillaAuthState } from "src/services/auth_store";
import { KeyValue, http } from "src/services/http";
import { ROLE_TYPE } from "src/services/types";
import { vanillaPartnerStore } from "src/zustore/partnerstore";

const { setState: set_store } = vanillaPartnerStore;

export async function update_partner_password(payload: KeyValue) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/password`;

  return http.post({ url: url, data: { ...payload } });
}

export async function get_partenaire_help_information() {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${vanillaAuthState.getState().base_url}partenaire/${id}/help`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const contact_informations: IContact = response.data;
      return Promise.resolve(contact_informations);
    }

    return Promise.reject();
  });
}

export async function get_partner_profile_information_and_stats(): Promise<IPartnerProfile> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}partenaire/${id}/informations-and-stats`)
    .then((response) => {
      if (response && response.status === 200) {
        const profile: IPartnerProfile = response.data;
        set_store({ partnerProfile: profile });
        return Promise.resolve(profile);
      }
      return Promise.reject();
    });
}

export async function save_device_info() {
  let infos = {} as any;
  let token = "";
  try {
    const retrieved_info = get_device_info();
    if (!retrieved_info?.expo_token) {
      try {
        const token_raw = await getExpoPushTokenAsync({
          //applicationId: "@helodev/sacrearmand_pro_app",
          projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
        });
        token = token_raw.data;
        infos = {
          ...infos,
          ...retrieved_info,
          expo_token: token,
        };
      } catch (error) {
        console.error("cant retrieve token");
        infos = {
          ...infos,
          ...retrieved_info,
        };
      }
    } else {
      infos = {
        ...infos,
        ...retrieved_info,
      };
    }
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
  if (!infos.base_user) {
    return Promise.reject();
  }
  const url_suffix = infos.base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)
    ? "deliverymen"
    : "partenaire";
  const url = `${vanillaAuthState.getState().base_url}${url_suffix}/${
    infos.base_user.id
  }/send-user-token-data`;

  if (Object.keys(infos).includes("id")) {
    delete infos.base_user;
  }

  return http
    .post({
      url,
      data: {
        ...infos,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve(response.data);
      }

      return Promise.reject(response);
    })
    .catch((e) => {
      return Promise.reject(e);
    });
}

// Partenaire informations
export async function get_partenaire_profile_information() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/informations`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const profile_informations: ProfileInformations = response.data;
      return Promise.resolve(profile_informations);
    }

    return Promise.reject();
  });
}

export async function update_partenaire_profile_information(
  profileInfo: ProfileInformations,
) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/information-update`;

  console.log("profile data body: ", profileInfo);

  return http.post({ url, data: { ...profileInfo } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data);
    }

    return Promise.reject();
  });
}

export async function get_profile_admin_and_justification() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/admin-and-justif`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const ProfileAdminJusti: AdminJusti = response.data;
      return Promise.resolve(ProfileAdminJusti);
    }

    return Promise.reject();
  });
}

export async function update_profile_admin_and_justification(
  banking_data: BankingInformations,
) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/banking-information-update`;

  console.log("admin data body: ", { ...banking_data });
  return http.post({ url, data: { ...banking_data } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data);
    }

    return Promise.reject();
  });
}

export async function get_profile_univers() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/univers`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const univers: UniversType[] = response.data;
      return Promise.resolve(univers);
    }

    return Promise.reject();
  });
}

export async function update_univers(ids: FormData) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/update-univers`;
  return http
    .post_form({ url, form: ids })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve(response.data);
      }

      return Promise.reject();
    })
    .catch((error) => {
      console.log(error);
    });
}

export async function deleteAll_univers() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/update-univers`;

  return http
    .post({ url })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve(response.data);
      }

      return Promise.reject();
    })
    .catch((error) => {
      console.log(error);
    });
}

// Parameters

export async function get_parameters_details() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/parameters`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const params: ParametersDetails[] = response.data;
      return Promise.resolve(params);
    }

    return Promise.reject();
  });
}

export async function add_new_collaborator(new_collaborator: NewCollaborator) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/add-collab`;

  return http.post({ url, data: new_collaborator }).then((response) => {
    if (response && response.status === 200) {
      // vanillaToastStore
      //   .getState()
      //   .showToast("valide", "Collaborateur bien ajouté", "Collaboration");
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

export async function get_week_planning() {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/planning/resume`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const planning: WeekSchedule[] = response.data;
      return Promise.resolve(planning);
    }

    return Promise.reject();
  });
}

export async function update_planning(planning: FormData) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/planning/update`;

  return http.post_form({ url, form: planning }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

// export async function get_quotas(id_partenaire: number) {}

export async function update_quotas(quota: FormData) {
  const id_partenaire = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/order-quota`;

  return http.post_form({ url, form: quota }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

export async function get_early_closing(): Promise<IPartnerProfileEarlyClosings> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}partenaire/${id}/opening`).then((response) => {
    if (response && response.status === 200) {
      const times: IPartnerProfileEarlyClosings = response.data;
      return Promise.resolve(times);
    }
    return Promise.reject();
  });
}

export async function update_early_closing(payload: IPartnerProfileUpdateDate) {
  const id = vanillaAuthState.getState().base_user.id;

  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/update-opening`;

  return http.post({ url: url, data: { ...payload } });
}

export async function delete_early_closing(dataId?: string) {
  const id = vanillaAuthState.getState().base_user.id;

  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/delete-opening/${dataId}`;

  return http.del({ url: url, data: {} });
}

export async function load_partner_customer(): Promise<IPartnerCustomer> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/customers`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const data: IPartnerCustomer = response.data;
      return Promise.resolve(data);
    }
    return Promise.reject(null);
  });
}

export async function upload_document(form: FormData, type: string) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/profil/upload-document/${type}`;
  return http.post_form({ url, form }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data);
    }
    return Promise.reject();
  });
}
