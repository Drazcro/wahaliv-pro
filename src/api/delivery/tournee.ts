import { TOURNEE_STATUS } from "src/constants/tournee";
import {
  ITourneeAPIResponse,
  ITourneeDetail,
} from "src/interfaces/delivery/tournees";

import { vanillaAuthState } from "src/services/auth_store";
import { http } from "src/services/http";

// ----------------------------------------------- //
// ----------------- TOURNEES -------------------- //
// ----------------------------------------------- //

export async function get_tournees(): Promise<ITourneeAPIResponse> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/tournees`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const tournee: ITourneeAPIResponse = response.data;
      const enabled_tournee = tournee.data.filter(
        (item) => item.status == TOURNEE_STATUS.ACCEPTED,
      );
      tournee.data = enabled_tournee;
      return Promise.resolve(tournee);
    }
    return Promise.reject(null);
  });
}

export async function get_tournee_details(id: number): Promise<ITourneeDetail> {
  const url = `${vanillaAuthState.getState().base_url}tournees/${id}/steps`;
  //console.log("getting details up", url);
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const tourneeDetail: ITourneeDetail = response.data;
      // vanillaTourneeStore.setState({
      //   steps: tourneeDetail.steps,
      // });

      //vanillaTourneeStore.setState({selectedTournee: tourneeDetail})
      return Promise.resolve(tourneeDetail);
    }
    return Promise.reject(null);
  });
}

export async function accept_tournee(id: number) {
  const url = `${vanillaAuthState.getState().base_url}tournee/${id}/accept`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournée acceptée");
      // vanillaToastStore
      //   .getState()
      //   .showToast("valide", "La tournée à été acceptée", "Tournée acceptée");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "error",
    //       "Veuillez vérifier votre connexion internet",
    //       "Une erreur est survenue",
    //     );
    return Promise.reject(null);
  });
}

export async function reject_tournee(id: number) {
  const url = `${vanillaAuthState.getState().base_url}tournee/${id}/cancel`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournée refusée");
      // vanillaToastStore
      //   .getState()
      //   .showToast("error", "Vous avez refusée la tournée", "Tournée Refusée");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "error",
    //       "Veuillez vérifier votre connexion internet",
    //       "Une erreur est survenue",
    //     );
    return Promise.reject(null);
  });
}

export async function complete_step(id: number, msg: string, title: string) {
  const url = `${vanillaAuthState.getState().base_url}step/${id}/retrieve`;
  //console.log("+++++++++retrieving up", url);
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", msg);
      //vanillaToastStore.getState().showToast("valide", msg, title);

      return Promise.resolve(true);
    }
    // showToast("error", "Une erreur est survenue");
    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "error",
    //       "Une erreur est survenue, veuillez vérifier votre connexion internet",
    //       "Une erreur est survenue",
    //     );
    return Promise.reject(null);
  });
}

export async function cancel_step(id: number) {
  const url = `${vanillaAuthState.getState().base_url}step/${id}/cancel`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournee annulée");
      // vanillaToastStore.getState().showToast("valide", "Tournee annulée", "");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "error",
    //       "Veuillez vérifier votre connexion internet",
    //       "Une erreur est survenue",
    //     );
    return Promise.reject(null);
  });
}

export async function ready_to_go_to_pick_up(
  id: number,
  lat: number,
  lng: number,
) {
  const url = `${vanillaAuthState.getState().base_url}course/last-moment/${id}`;
  //console.log("+++++++picking up", url, lat, lng);
  return http.post({ url, data: { lat, lng } }).then((response) => {
    if (response && response.status === 200) {
      //showToast("success", "Départ vers partenaire");
      // vanillaToastStore
      //   .getState()
      //   .showToast(
      //     "valide",
      //     "Bonne route vers le restaurant pour récupérer la commande !",
      //     "Départ enregistré",
      //   );
      //console.log(response);
      return Promise.resolve(true);
    }
    // showToast("error", "Une erreur est survenue");
    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "error",
    //       "Veuillez vérifier votre connexion internet",
    //       "Une erreur est survenue",
    //     );
    return Promise.reject(false);
  });
}

//   export const reload_orders = async () => {
//     const user_id = vanillaAuthState.getState().base_user.id

//     try {
//       await Promise.allSettled([
//         await get_new_orders_for_partner(user_id, 1, INTERVAL_PAGE),
//         await get_accepted_orders_for_partner(user_id, 1, INTERVAL_PAGE),
//         await get_delivered_orders_for_partner(user_id, 1, INTERVAL_PAGE),
//         await get_refused_orders_for_partner(user_id, 1, INTERVAL_PAGE),
//       ]);
//     } catch (e) {
//       //console.error(e)
//     }
//     return Promise.resolve();
// };
