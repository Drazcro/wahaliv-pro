import {
  IPlanningDisengageStatus,
  IPlannings,
  IPlanningsSummary,
} from "src/interfaces/Entities";
import { vanillaAuthState } from "src/services/auth_store";
import { http } from "src/services/http";

export async function recover_delivery_planning(
  marketplace: number,
  date: string,
): Promise<IPlannings> {
  const state = vanillaAuthState.getState();

  const url = `${state.base_url}deliverymen/${state.base_user.id}/planning?date=${date}&marketplace=${marketplace}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const plannings: IPlannings = response.data;
      return Promise.resolve(plannings);
    }
    return Promise.reject();
  });
}

export async function recover_delivery_planning_summary(
  marketplace: number,
): Promise<IPlanningsSummary> {
  const state = vanillaAuthState.getState();

  const url = `${state.base_url}deliverymen/${state.base_user.id}/planning-summary?marketplace=${marketplace}`;
  return http
    .get(url)
    .then((response) => {
      console.log(response);
      if (response && response.status === 200) {
        const summary: IPlanningsSummary = response.data;
        return Promise.resolve(summary);
      }

      return Promise.reject();
    })
    .catch((e) => {
      console.error(e);
      return Promise.reject();
    });
}

export async function recover_delivery_planning_summary_disengage_status(
  creneaux: number | undefined,
): Promise<IPlanningDisengageStatus> {
  const state = vanillaAuthState.getState();

  const url = `${state.base_url}deliverymen/${state.base_user.id}/disengage-status/${creneaux}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const summary: IPlanningDisengageStatus = response.data;
      return Promise.resolve(summary);
    }
    return Promise.reject();
  });
}
