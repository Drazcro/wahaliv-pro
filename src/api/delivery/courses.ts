import { COURSE_STATUS } from "src/constants/course";
import { ICourse, IOldCourse } from "src/interfaces/delivery/courses";
import { vanillaAuthState } from "src/services/auth_store";
import { http } from "src/services/http";
import { vanillaCourseStore } from "src/zustore/coursestore";

export async function get_new_course(): Promise<ICourse> {
  const URL = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;

  return http.get(`${URL}deliverymen/${id}/course/v2/new`).then((response) => {
    if (response && response.status === 200) {
      const res: ICourse = response.data;
      if (res) {
        return Promise.resolve(res);
      }
    }
    return Promise.reject();
  });
}

export async function get_finished_course(): Promise<IOldCourse[]> {
  const url = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;
  const status = COURSE_STATUS.FINISHED;

  return http
    .get(`${url}deliverymen/${id}/course/new/${status}`)
    .then((response) => {
      if (response && response.status === 200) {
        const courses: IOldCourse[] = response.data;
        return Promise.resolve(courses);
      }

      return Promise.reject();
    });
}

export async function accept_course(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/accept/${id}`;

  return http.post({ url: url, data: { id: "" } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }
    return Promise.reject();
  });
}

export async function reject_course(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/cancel/${id}`;
  return http.post({ url: url, data: { id: "" } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

export async function get_course_details(id: number): Promise<IOldCourse> {
  const url = `${vanillaAuthState.getState().base_url}courses/detail/${id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const course: IOldCourse = response.data;
      vanillaCourseStore.getState().set_opened(course);
      return Promise.resolve(course);
    }
    return Promise.reject();
  });
}
