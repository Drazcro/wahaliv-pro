/* eslint-disable @typescript-eslint/no-explicit-any */
import { getExpoPushTokenAsync } from "expo-notifications";
import { IContact, IFinancialImpact } from "src/interfaces/Entities";
import {
  IBankInfo,
  IBillsDetails,
  IInvoiceAPIResponse,
  IReverseEarnings,
} from "src/interfaces/delivery/finance";
import {
  IDeliveryProfile,
  IDeliveryProfileInformation,
  IDocumentJustificatif,
} from "src/interfaces/delivery/profile";
import { get_device_info } from "src/services";
import { vanillaAuthState } from "src/services/auth_store";
import { KeyValue, http } from "src/services/http";
import { ROLE_TYPE } from "src/services/types";

export async function get_is_on_service(): Promise<boolean> {
  const url = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;
  return http.get(`${url}deliverymen/${id}/in-service/5`).then((response) => {
    if (response && response.status === 200) {
      // setProfile({ isOnService: response.data.in_service });
      // if (Platform.OS !== "web") {
      //   if (response.data.in_service === true) {
      //     // registerBackgroundFetchAsync();
      //   } else {
      //     // unregisterBackgroundFetchAsync();
      //   }
      // }
      return Promise.resolve(response.data.in_service);
    }
    return Promise.reject();
  });
}

export async function get_delivery_information_and_state_for_profile(): Promise<IDeliveryProfile> {
  const url = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;
  return http
    .get(`${url}deliverymen/${id}/informations-and-stats`)
    .then((response) => {
      if (response && response.status === 200) {
        const profile: IDeliveryProfile = response.data;
        //vanillaProfileStore.setState({ stats: profile });
        return Promise.resolve(profile);
      }
      return Promise.reject();
    });
}

export async function update_delivery_auto_valid_status(payload: KeyValue) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/is-auto-valid`;

  return http.post({ url: url, data: payload });
}

export async function get_delivery_information_for_profile(): Promise<IDeliveryProfileInformation> {
  const url = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;

  return http.get(`${url}deliverymen/${id}/informations`).then((response) => {
    if (response && response.status === 200) {
      const informations: IDeliveryProfileInformation = response.data;
      //setProfile({ profile: informations });
      return Promise.resolve(informations);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function update_delivery_information(payload: KeyValue) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/information-update`;

  return http.post({ url: url, data: { ...payload } });
}
export async function get_deliveryman_docs() {
  const url = vanillaAuthState.getState().base_url;
  const id = vanillaAuthState.getState().base_user.id;
  return http
    .get(`${url}deliverymen/${id}/document-justificatif`)
    .then((response) => {
      if (response && response.status === 200) {
        const document: IDocumentJustificatif = response.data;
        return Promise.resolve(document);
      }

      return Promise.reject();
    });
}

export async function upload_doc(form: FormData, type: string) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/document/${type}`;
  return http.post_form({ url, form }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve();
    }
    return Promise.reject();
  });
}

export async function get_financial_history(year: string, month: string) {
  const id = vanillaAuthState.getState().base_user.id;

  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/impact-financier?custom_date=${year}-${month}&id=${id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const financials: IFinancialImpact = response.data[0];
      return Promise.resolve(financials);
    }

    return Promise.reject();
  });
}

export async function update_password(payload: KeyValue) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/password`;

  return http.post({ url: url, data: { ...payload } });
}

export async function payback_delivery_and_generate_invoice(
  marketplace: number,
) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/payback`;

  return http.post({ url: url, data: { marketplace } });
}

export async function get_delivery_information_for_payback(
  marketplace: number,
) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}deliverymen/${id}/payback-informations/${marketplace}`)
    .then((response) => {
      if (response && response.status === 200) {
        const receipts: IReverseEarnings = response.data;
        return Promise.resolve(receipts);
      }

      return Promise.reject();
    });
}

export async function get_delivery_bank_information_for_profile(): Promise<IBankInfo> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}deliverymen/${id}/informations-bancaires`)
    .then((response) => {
      if (response && response.status === 200) {
        const banks: IBankInfo = response.data;
        return Promise.resolve(banks);
      }

      return Promise.reject();
    });
}

export async function update_bank_information(payload: IBankInfo) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/information-bancaires-update`;

  return http.post({ url: url, data: { ...payload } });
}

export async function get_daily_bill(
  begin_date: string,
  end_date: string,
  page: number,
  page_size = 80,
): Promise<IInvoiceAPIResponse> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/daily-bill?begin_date=${begin_date}&end_date=${end_date}&page_size=${page_size}&page_number=${page}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const invoice: IInvoiceAPIResponse = response.data;
      //console.log(invoice.global_data.length);
      return Promise.resolve(invoice);
    }
    return Promise.reject();
  });
}

export async function get_finished_daily_bill(
  begin_date: string,
  end_date: string,
  page: number,
  page_size = 40,
): Promise<IBillsDetails[]> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/finished-bill?begin_date=${begin_date}&end_date=${end_date}&page_size=${page_size}&page_number=${page}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const finishedBill = response.data;
      console.log("!!!!!!!!!!");
      //console.log(finishedBill);

      ////console.log(response.request?.);
      //finishedBill.map(e=>//console.error(e.begin_date.date.split(' ')[0], e.deliveryman_bill_amount, e.deliveryman_bill_id))
      return Promise.resolve(finishedBill.data);
    }
    //console.log(response.status)
    return Promise.reject();
  });
}

export async function download_customer_bill(id: number, bill_id: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/download/customer-bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function download_recap_bill(bill_id: number) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/download/recap-bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }

    return Promise.reject();
  });
}

export async function download_bill(bill_id: number) {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/download/bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}
export async function get_invoice_bill(): Promise<IBillsDetails[]> {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/finished-bill`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const finishedBill = response.data;

      return Promise.resolve(finishedBill.data);
    }
    return Promise.reject();
  });
}

export async function get_deliverymen_help_information() {
  const id = vanillaAuthState.getState().base_user.id;
  const url = `${vanillaAuthState.getState().base_url}deliverymen/${id}/help`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const contact_informations: IContact = response.data;
      return Promise.resolve(contact_informations);
    }

    return Promise.reject();
  });
}

export async function save_device_info() {
  let infos = {} as any;
  let token = "";
  try {
    const retrieved_info = get_device_info();
    if (!retrieved_info?.expo_token) {
      try {
        const token_raw = await getExpoPushTokenAsync({
          //applicationId: "@helodev/sacrearmand_pro_app",
          projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
        });
        token = token_raw.data;
        infos = {
          ...infos,
          ...retrieved_info,
          expo_token: token,
        };
      } catch (error) {
        console.error("cant retrieve token");
        infos = {
          ...infos,
          ...retrieved_info,
        };
      }
    } else {
      infos = {
        ...infos,
        ...retrieved_info,
      };
    }
  } catch (error) {
    console.error(error);
    return Promise.reject(error);
  }
  if (!infos.base_user) {
    return Promise.reject();
  }
  const url_suffix = infos.base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)
    ? "deliverymen"
    : "partenaire";
  const url = `${vanillaAuthState.getState().base_url}${url_suffix}/${
    infos.base_user.id
  }/send-user-token-data`;

  if (Object.keys(infos).includes("id")) {
    delete infos.base_user;
  }

  return http
    .post({
      url,
      data: {
        ...infos,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve(response.data);
      }

      return Promise.reject(response);
    })
    .catch((e) => {
      return Promise.reject(e);
    });
}
