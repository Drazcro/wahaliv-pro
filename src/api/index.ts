export type KeyValue = Record<string, string | number | boolean | unknown>;
export interface IHttpData {
  url: string;
  data: KeyValue;
}
