import { STATUS } from "src/constants/status";
import { vanillaAuthState } from "src/services/auth_store";
import { no_auth_http } from "src/services/http";

export async function send_password_reset_email(email: string) {
  const url = `${vanillaAuthState.getState().base_url}users/reset-password`;
  const marketplace = STATUS.MARKETPLACE;
  return no_auth_http
    .post({ url, data: { email, marketplace } })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve();
      }
      console.log(response);
      return Promise.reject();
    });
}
