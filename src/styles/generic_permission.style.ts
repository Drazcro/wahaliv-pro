import { StyleSheet, Platform, Dimensions } from "react-native";
import { color } from "src/theme";
import { fontFamily } from "src/theme/typography";
const DEVICE = Dimensions.get("screen");

console.log(DEVICE.height, DEVICE.scale);
export const styles = StyleSheet.create({
  container: {
    //flexGrow: 1,
    height: 667,
    //backgroundColor: "red",
    maxWidth: "100%",
    width: "100%",
    //flexWrap: 'wrap',
    justifyContent: "center",
    alignItems: "center",
  },
  status: {
    marginTop: Platform.OS === "ios" ? 20 : 0,
  },
  content: {
    // backgroundColor: 'blue',
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    width: "100%",
  },
  spacing: {
    width: "100%",
    height: 30,
  },
  contentFragment: {
    flex: DEVICE.height < 760 ? 15 : 8,
    width: "100%",
    alignItems: "center",
    justifyContent: "space-between",
  },
  contentFragmentSmaller: {
    flex: 3,
    width: "100%",
    backgroundColor: "red",
    alignItems: "center",
  },
  contentFragmentDivider: {
    flex: 1,
    width: "100%",
  },
  permissionTitle: {
    marginTop: 58,
    marginBottom: 31,
    fontSize: 28,
    color: "#273A62",
    textAlign: "center",
    fontFamily: fontFamily.semiBold,
  },
  permissionText: {
    marginBottom: 107,
    paddingHorizontal: 20,
    textAlign: "center",
    fontFamily: fontFamily.Medium,
    color: "#273A62",
    //flexShrink: 4,
    fontSize: 16,
    width: "100%",
  },
  button: {
    alignSelf: "center",
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
    alignContent: "center",
    borderRadius: 50,
    height: 52,
    width: 308,
    backgroundColor: color.ACCENT,
  },
  denyButton: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 16,
    color: color.ACCENT,
    marginTop: 25,
  },
  error: {
    fontFamily: fontFamily.regular,
    color: color.ACCENT,
    fontSize: 12,
    lineHeight: 44,
  },
  buttonText: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
  },

  denyButtonText: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 18,
    lineHeight: 22,
    textAlign: "center",
  },

  /** Tablet screen */
  TabletPermissionText: {
    textAlign: "center",
    fontFamily: fontFamily.Medium,
    color: "#273A62",
    width: 336,
    lineHeight: 26,
    fontSize: 18,
    marginBottom: 107,
    paddingHorizontal: 5,
  },
  TabletDenyButton: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 16,
    color: color.ACCENT,
    marginTop: 51.5,
  },
});
