import { StyleSheet } from "react-native";
import { color } from "src/theme";
import { fontFamily } from "src/theme/typography";

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  text: {
    marginBottom: 56,
  },
  textPass: {
    textAlign: "center",
    color: "#1D2A40",
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.Medium,
  },
  textMail: {
    textAlign: "center",
    color: "#1D2A40",
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.Bold,
  },
  main: {
    flex: 1,
    marginTop: 27,
  },
  content: {
    flex: 4,
  },
  button: {
    flex: 8,
    justifyContent: "flex-end",
    alignItems: "center",
    marginBottom: 60,
  },
  buttonContainer: {
    flex: 5,
    justifyContent: "flex-end",
    alignItems: "center",
    paddingBottom: 84,

    //backgroundColor: "red"
  },
  buttonStyle: {
    height: 52,
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: color.ACCENT,
    color: color.ACCENT,
  },
  buttonTitleStyle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonContainerStyle: {
    backgroundColor: color.ACCENT,
    padding: 0,
    width: 308,
    borderRadius: 34,
  },
  input: {
    flex: 2,
    justifyContent: "center",
    marginHorizontal: -5,
  },
  connexion: {
    flex: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  // text: {
  //   fontFamily: fontFamily.semiBold,
  //   fontWeight: "600",
  //   color: "#1D2A40",
  //   fontSize: 30,
  //   lineHeight: 45,
  // },
  forgotPasswordButtonZone: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 14,
    marginRight: 35,
  },
  forgotPassword: {
    fontFamily: fontFamily.Medium,
    textAlign: "center",
    fontSize: 12,
    lineHeight: 22,
    height: 22,
  },
  forgotPasswordButton: {
    width: 160,
  },
  passForgot: {
    flexDirection: "row",
    justifyContent: "space-around",
  },
  inputContainer: {
    width: "83%",
    // marginLeft: "12.5%",
    marginLeft: 35,
    marginRight: 31,
  },
  /** tablet screen */
  tabletTextPass: {
    textAlign: "center",
    color: "#1D2A40",
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.Medium,
    width: 441,
    alignSelf: "center",
  },
  tabletText: {
    marginBottom: 84,
  },
  tabletContent: {},
  tabletInputContainer: {
    width: 396,
    alignSelf: "center",
  },
  tabletButton: {
    marginTop: 143,
    alignSelf: "center",
    alignItems: "center",
  },
  tabletButtonContainerStyle: {
    backgroundColor: color.ACCENT,
    width: 355,
    borderRadius: 34,
  },
});
