import { ThemeType } from "src/libs/calendario";
import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export const styles = StyleSheet.create({
  picker_input: {
    marginLeft: 23,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    color: "rgba(17, 40, 66, 1)",
  },
  picker_input_container: {
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    width: "100%",
    height: 38,
    borderRadius: 17,
    justifyContent: "center",
  },
  closeBtn: {
    position: "absolute",
    top: 30.38,
    right: 25.38,
  },
  title: {
    position: "relative",
    top: 25,
    marginBottom: 21,
    color: "#112842",
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    lineHeight: 22,
    textAlign: "center",
  },
  selectContainer: {
    marginTop: 30,
    width: "90%",
    alignSelf: "center",
  },
  typeContainer: {
    width: "90%",
    alignSelf: "center",
    marginBottom: "10%",
  },
  text_label: {
    color: "#000000",
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 17.07,
    marginBottom: "10%",
  },
  calendar: {
    marginRight: 10,
  },
  dateContainer: {
    height: "10%",
    width: "90%",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 10,
  },
  dateInpuContainer: {
    width: "40%",
    height: 36,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    borderColor: "#C4C4C4",
    borderWidth: 1,
  },
  dateInput: {
    color: "#000000",
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 17.07,
  },
  separator: {
    marginHorizontal: 5,
  },
  buttonContainer: {
    width: "100%",
    paddingTop: "10%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    borderTopColor: "#E7E7E7",
    borderTopWidth: 0.5,
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 133,
    height: 35,
    backgroundColor: "#fff",
    borderRadius: 30,
    borderWidth: 1,
    borderColor: color.ACCENT,
  },
  text_button: {
    color: color.ACCENT,
    fontSize: 16,
    lineHeight: 17.072,
  },
  btnSeparator: {
    marginHorizontal: 9,
  },
  typeButton: {
    width: "100%",
    height: "10%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf: "flex-start",
  },
  typeButtonStyle: {
    justifyContent: "center",
    alignSelf: "center",
    width: 133,
    height: 35,
    backgroundColor: "#fff",
    borderRadius: 30,
    borderWidth: 1,
    borderColor: "black",
  },
  textButtonStyle: {
    fontFamily: fontFamily.Medium,
    color: "black",
    fontSize: 14,
    marginTop: -2,
  },
  footerBlock: {
    width: "100%",
    position: "absolute",
    bottom: "3%",
    alignSelf: "center",
    backgroundColor: "white",
    paddingTop: "2%",
  },
});

export const theme: ThemeType = {
  monthTitleTextStyle: {
    color: "#1D2A40",
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    alignSelf: "flex-start",
    lineHeight: 17.07,
  },
  weekColumnsContainerStyle: {
    paddingTop: 10,
  },
  weekColumnTextStyle: {
    fontFamily: fontFamily.regular,
    color: "#848689",
    fontSize: 12,
  },
  dayContainerStyle: {
    backgroundColor: "transparent",
  },
  dayTextStyle: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
  },
  activeDayContainerStyle: {
    backgroundColor: "#E8E8E8",
  },
  activeDayTextStyle: {
    color: "black",
  },
};
