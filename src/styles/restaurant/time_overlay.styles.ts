import { PixelRatio, StyleSheet } from "react-native";
import { color } from "src/theme";
import { fontFamily } from "src/theme/typography";

export const styles = StyleSheet.create({
  overlayBody: {
    marginTop: 20,
    alignItems: "center",
  },
  xPosition: {
    alignItems: "flex-end",
    marginTop: 20,
    marginRight: 20,
  },
  header: {
    display: "flex",
    flexDirection: "row",
    marginBottom: 12,
  },
  headerText: {
    marginLeft: 10,
    fontFamily: fontFamily.semiBold,
    fontSize: 18,
    marginTop: -1,
  },
  subTitle: {
    textAlign: "center",
    fontFamily: fontFamily.Medium,
    fontSize: 14,
  },
  subTitleNoDeliveryman: {
    textAlign: "center",
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    marginHorizontal: 25,
  },
  subTitleSemiBold: {
    fontFamily: fontFamily.semiBold,
  },
  buttonPosition: {
    alignItems: "center",
    justifyContent: "center",
  },
  buttonPos: {
    alignItems: "center",
    justifyContent: "center",
  },

  button: {
    backgroundColor: color.ACCENT,
    borderRadius: 30,
    width: 308,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 13,
    marginTop: 19,
    marginLeft: 25,
    marginRight: 25,
  },
  agreeText: {
    color: "white",
    fontFamily: fontFamily.Medium,
    fontSize: 16,
  },
  subTitleBox: {
    backgroundColor: "rgba(233, 200, 82, 0.14)",
    borderRadius: 9,
    marginBottom: 25,
    marginTop: 20,
    padding: 15,
  },
  TimeDetailsBox: {
    backgroundColor: "rgba(196, 196, 196, 0.22)",
    borderRadius: 18,
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 18,
  },
  TimeDetails: {
    display: "flex",
    flexDirection: "row",
    marginLeft: 16,
    marginVertical: 8,
  },
  TimeHeaderText: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    marginBottom: 7,
    marginLeft: 20,
    marginRight: 20,
  },
  TimeText: {
    marginLeft: 8,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
  },
  DisplayTimePossibility: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 25,
  },
  TimeBlock: {
    backgroundColor: "rgba(196, 196, 196, 0.22)",
    paddingHorizontal: 17,
    paddingVertical: 6,
    borderRadius: 18,
  },
  NewTimeText: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
  },
  ///////////////////////////////////////////////////////
  timeCardBody: {
    marginHorizontal: 17,
    borderRadius: 10,
    paddingTop: 12,
    paddingBottom: 12,
    marginBottom: 16,
  },
  cardDisposition: {
    marginLeft: 18,
    flexDirection: "row",
  },
  textStyle: {
    fontFamily: fontFamily.Medium,
    fontSize: 12,
    marginLeft: 10,
  },
  textPosition: {
    alignItems: "center",
    justifyContent: "center",
  },
  boldText: {
    fontFamily: fontFamily.semiBold,
  },
  space: {
    marginTop: 10,
  },
});
