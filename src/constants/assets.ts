import Asiatique from "assets/image_svg/Asiatique.svg";
import Burger from "assets/image_svg/Burger.svg";
import Charcuterie from "assets/image_svg/Charcuterie.svg";
import Indien from "assets/image_svg/Indien.svg";
import International from "assets/image_svg/International.svg";
import Italien from "assets/image_svg/Italien.svg";
import Japonais from "assets/image_svg/Japonais.svg";
import Mediterraneen from "assets/image_svg/Mediterraneen.svg";
import Mexicaine from "assets/image_svg/Mexicaine.svg";
import Pizza from "assets/image_svg/Pizza.svg";
import Sandwish from "assets/image_svg/Sandwish.svg";
import Sucreries from "assets/image_svg/Sucreries.svg";
import Thai from "assets/image_svg/Thai.svg";

export {
  Asiatique,
  Burger,
  Charcuterie,
  Indien,
  International,
  Italien,
  Japonais,
  Mediterraneen,
  Mexicaine,
  Pizza,
  Sandwish,
  Sucreries,
  Thai,
};
