export enum TOURNEE_STATUS {
  TO_SEND = 0,
  SENDED = 1,
  ACCEPTED = 2,
  FINISHED = 3,
  CANCELED = 4,
}

export enum TOURNEE_STEP_STATUS {
  TO_SEND = 0,
  SENDED = 1,
  FINISHED = 2,
  CANCELED = 3,
}

export enum TOURNEE_STEP_STATE {
  ENABLE = "enable",
  DISABLED = "disabled",
  CROSSED = "crossed",
}

export enum TOURNEE_STEP_TYPE {
  PICK = "pickup",
  DELIVERY = "delivery",
}
