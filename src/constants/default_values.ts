// import { ICourse } from "src/interfaces/Entities";
import { Dimensions } from "react-native";
export const DEVICE = Dimensions.get("screen");
import {
  Asiatique,
  Italien,
  Japonais,
  Mexicaine,
  Pizza,
  Burger,
  Thai,
  International,
  Mediterraneen,
  Sandwish,
  Indien,
  Charcuterie,
  Sucreries,
} from "./assets";

export const cuisineTypes = [
  {
    id: 1,
    label: "Burgers",
    icon: Burger,
  },
  {
    id: 2,
    label: "Italien",
    icon: Italien,
  },
  {
    id: 3,
    label: "Asiatique",
    icon: Asiatique,
  },
  {
    id: 4,
    label: "Japonais",
    icon: Japonais,
  },
  {
    id: 5,
    label: "Pizza",
    icon: Pizza,
  },
  {
    id: 6,
    label: "Méxicaine",
    icon: Mexicaine,
  },
  {
    id: 7,
    label: "Thai",
    icon: Thai,
  },
  {
    id: 8,
    label: "International",
    icon: International,
  },
  {
    id: 9,
    label: "Méditerranéen",
    icon: Mediterraneen,
  },
  {
    id: 10,
    label: "Sandwish",
    icon: Sandwish,
  },
  {
    id: 11,
    label: "Indien",
    icon: Indien,
  },
  {
    id: 12,
    label: "Charcuterie",
    icon: Charcuterie,
  },
  {
    id: 13,
    label: "Sucreries",
    icon: Sucreries,
  },
];

// export const initialCourse = {
//   id_delivery: 887844,
//   id_commande: 8878474,
//   delivery_date: {
//     date: "2020-06-12 11:44:00.000000",
//     timezone_type: 3,
//     timezone: "UTC",
//   },
//   delivery_address: {
//     address: "Rue de verdun, 11000 Carcassonne",
//     postalCode: "1100",
//     numerorue: 154,
//     complementadresse: "Jarcaranda Street",
//   },
//   delivery_distance: 5000,
//   delivery_amount: 4.5,
//   delivery_status: 1,
//   pickup_hour: {
//     date: "2020-06-12 11:44:00.000000",
//     timezone_type: 3,
//     timezone: "UTC",
//   },
//   pickup_address: "Rue de verdun, 11000 Carcassonne",
//   pickup_date: {
//     date: "2020-06-12 11:44:00.000000",
//     timezone_type: 3,
//     timezone: "UTC",
//   },
//   partner: "Sushi Boat",
//   partner_lng: 7.25959594,
//   partner_lat: 0.27985959594,
//   partner_telephone: "+33698989878",
//   customer: "Jean Christian",
//   customer_lng: 0.25959594,
//   customer_lat: 9.25959594,
//   customer_telephone: "+33698989878",
//   idDisplayed: true,
//   real_delivery_date: {
//     date: "2020-06-12 11:44:00.000000",
//     timezone_type: 3,
//     timezone: "UTC",
//   },
//   commande_at: {
//     date: "2020-06-12 11:44:00.000000",
//     timezone_type: 3,
//     timezone: "UTC",
//   },
//   details: [
//     {
//       name: "Burger",
//       qte: 2,
//       price: 2000,
//     },
//   ],
//   canRecover: true,
//   comment: "Ajoutez de la sauce ketchup svp !",
//   isRetrievable: true,
//   isDisplayed: true,
//   isRefund: true,
//   isGrouped: true,
//   tip: 2.9,
//   groupedCommandIds: [],
//   status_commande: 4,
// } as ICourse;

// export const DOCUMENT_FILE = {
//   RECTO: "recto",
//   VERSO: "verso",
//   RIB: "rib",
//   KBIS: "kbis",
//   CONTACT: "contact",
// };

export const WEEK_DAYS = [
  {
    key: "lu",
    value: "lundi",
  },
  {
    key: "ma",
    value: "mardi",
  },
  {
    key: "me",
    value: "mercredi",
  },
  {
    key: "je",
    value: "jeudi",
  },
  {
    key: "ve",
    value: "vendredi",
  },
  {
    key: "sa",
    value: "samedi",
  },
  {
    key: "di",
    value: "dimanche",
  },
];

export const WEEK_SCHEDULE = [
  {
    day: "Lundi",
    midi: "10:00- 14:00",
    night: "fermé",
  },
  {
    day: "Mardi",
    midi: "10:00- 14:00",
    night: "18:00- 22:00",
  },
  {
    day: "Mercredi",
    midi: "10:00- 14:00",
    night: "18:00- 22:00",
  },
  {
    day: "Jeudi",
    midi: "10:00- 14:00",
    night: "18:00- 22:00",
  },
  {
    day: "Vendredi",
    midi: "10:00- 14:00",
    night: "18:00- 22:00",
  },
  {
    day: "Samedi",
    midi: "10:00- 14:00",
    night: "18:00- 22:00",
  },
  {
    day: "Dimanche",
    midi: "10:00- 14:00",
    night: "fermé",
  },
];

export const AVAILABLE_HOURS = [
  "10:00",
  "10:30",
  "11:00",
  "11:30",
  "12:00",
  "12:30",
  "13:00",
  "13:30",
  "14:00",
  "14:30",
  "15:00",
  "15:30",
  "16:00",
  "16:30",
  "17:00",
  "17:30",
  "18:00",
  "18:30",
  "19:00",
  "19:30",
  "20:00",
  "20:30",
  "21:00",
  "21:30",
  "22:00",
  "22:30",
  "23:00",
  "23:30",
  "00:00",
];

// export const DATA_CALENDAR = [
//   {
//     id: 0,
//     time: "11:00",
//   },
//   {
//     id: 1,
//     time: "12:00",
//   },
//   {
//     id: 2,
//     time: "13:00",
//   },
//   {
//     id: 3,
//     time: "14:00",
//   },
//   {
//     id: 4,
//     time: "15:00",
//   },
//   {
//     id: 5,
//     time: "16:00",
//   },
//   {
//     id: 6,
//     time: "17:00",
//   },
//   {
//     id: 7,
//     time: "18:00",
//   },
//   {
//     id: 8,
//     time: "19:00",
//   },
//   {
//     id: 9,
//     time: "20:00",
//   },
//   {
//     id: 10,
//     time: "21:00",
//   },
//   {
//     id: 11,
//     time: "22:00",
//   },
//   {
//     id: 12,
//     time: "23:00",
//   },
//   {
//     id: 13,
//     time: "00:00",
//   },
//   {
//     id: 14,
//     time: "01:00",
//   },
//   {
//     id: 15,
//     time: "02:00",
//   },
//   {
//     id: 16,
//     time: "03:00",
//   },
//   {
//     id: 17,
//     time: "04:00",
//   },
//   {
//     id: 18,
//     time: "05:00",
//   },
//   {
//     id: 19,
//     time: "06:00",
//   },
//   {
//     id: 20,
//     time: "07:00",
//   },
//   {
//     id: 21,
//     time: "08:00",
//   },
//   {
//     id: 22,
//     time: "09:00",
//   },
//   {
//     id: 23,
//     time: "10:OO",
//   },
// ];

// interface HistoriqueDataType {
//   title: string;
//   bonus: number;
//   time: string;
//   day: string;
//   commandId: string;
// }

// export const HISTORIQUE_DATA: HistoriqueDataType[] = [
//   {
//     title: "Dépositionnement -6h",
//     bonus: 25,
//     time: "12h53",
//     day: "12/05/23",
//     commandId: "ID5678",
//   },
//   {
//     title: "Dépositionnement -6h",
//     bonus: 25,
//     time: "12h53",
//     day: "11/05/23",
//     commandId: "ID5678",
//   },
//   {
//     title: "Refus commande",
//     bonus: 100,
//     time: "22h00",
//     day: "11/03/23",
//     commandId: "ID5678",
//   },
//   {
//     title: "Non acceptation en 10’",
//     bonus: 100,
//     time: "22h00",
//     day: "11/03/23",
//     commandId: "ID5678",
//   },
// ];

export const INITIAL_PAGE = 1;
export const INTERVAL_PAGE = 20;
