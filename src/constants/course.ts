export enum COURSE_STATUS {
  // Delivery
  ALL_COURSE = 0,
  TO_BE_ACCEPTED = 1, //to be accepted by deliveryman
  ACCEPTED = 2,
  CANCELED_BY_DELIVERY = 3,
  CANCELED_BY_SACRE = 4,
  CANCELED_BY_PARTNER = 5,

  FINISHED = 6,
  RECOVERED = 7,
}
