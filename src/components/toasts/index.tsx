import { JSX } from "react";
import {
  Dimensions,
  Platform,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import Toast, {
  BaseToast,
  BaseToastProps,
  ErrorToast,
  ToastConfig,
  ToastConfigParams,
} from "react-native-expo-popup-notifications";
import { fontFamily } from "src/theme/typography";
import { IToastType } from "./types";

import ValidateImage from "assets/image_svg/validateImg.svg";
import NewTournee from "assets/image_svg/newTournee.svg";
import ErrorIcon from "assets/image_svg/errorIcon.svg";
import CloseBtn from "assets/image_svg/X.svg";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import * as Device from "expo-device";

//import ChatNotification from "assets/image_svg/ChatNotificationIcon.svg";

const device_width = Dimensions.get("window").width;
const is_tablet = Device.deviceType != Device.DeviceType.PHONE;

type Props = {
  text: string;
  text1?: string;
  props?: JSX.IntrinsicAttributes & BaseToastProps;
  type: IToastType;
  onPress?: () => void;
};

const CustomToast = ({ type, text, text1, props }: Props) => {
  const { top } = useSafeAreaInsets();
  return (
    <SafeAreaView
      style={[
        styles.container,
        { top },
        Platform.OS === "android" ? styles.androidShadow : styles.iosShadow,
      ]}
    >
      <ToastIcon type={type} />
      <View style={styles.fontPosition}>
        <Text style={styles.titleStyle}>{text}</Text>
        <Text
          style={[
            styles.textStyle,
            {
              width:
                text ===
                "Bonne route vers le restaurant pour récupérer la commande !"
                  ? "100%"
                  : "100%",
            },
          ]}
        >
          {text1}
        </Text>
      </View>
      <TouchableOpacity
        onPressIn={() => {
          Toast.hide();
          props?.onPress?.();
        }}
        style={{
          position: "absolute",
          right: 10,
          top: 10,
        }}
      >
        <CloseBtn />
      </TouchableOpacity>
    </SafeAreaView>
  );
};
/*
  1. Create the config
*/
const toastConfig: ToastConfig = {
  /*
    Overwrite 'success' type,
    by modifying the existing `BaseToast` component
  */
  success: (props: ToastConfigParams<any>) => (
    <CustomToast
      {...props}
      onPress={props.onPress}
      type="SUCCESS"
      text={props.text1 ?? ""}
      text1={props.text2}
    />
  ),

  error: (props: ToastConfigParams<any>) => (
    <CustomToast
      {...props}
      onPress={props.onPress}
      type="ERROR"
      text={props.text1 ?? ""}
      text1={props.text2}
    />
  ),
  tournee: (props: ToastConfigParams<any>) => (
    <CustomToast
      {...props}
      onPress={props.onPress}
      type="TOURNEE"
      text={props.text1 ?? ""}
      text1={props.text2}
    />
  ),
};

const ToastIcon = ({ type }: { type: IToastType }) => {
  switch (type) {
    case "SUCCESS":
      return (
        <View style={styles.imgPosition}>
          <ValidateImage />
        </View>
      );
    case "TOURNEE":
      return (
        <View style={styles.imgPosition}>
          <NewTournee />
        </View>
      );

    case "ERROR":
      return (
        <View style={styles.imgPosition}>
          <ErrorIcon />
        </View>
      );

    default:
      return <ValidateImage />;
  }
};

export { toastConfig };

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",

    maxWidth: is_tablet ? 350 : device_width - 30,
    height: 105,
    marginBottom: 26,
    marginRight: "10%",
    backgroundColor: "white",
    marginVertical: "auto",

    alignItems: "center",
    alignSelf: "center",
    borderRadius: 15,
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.25,
    position: "absolute",
    //zIndex: 10000,
    borderColor: "white",
    //top: 20,
    // shadowColor: "red",
    borderWidth: 1,
    zIndex: 2 ^ 52,
  },

  androidShadow: {
    shadowColor: "rgba(0, 0, 0, 0.8)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    elevation: 5,
    // shadowOpacity: 0.25,
    // shadowRadius: 7.49,
  },
  iosShadow: {
    shadowColor: "rgba(0, 0, 0, 0.8)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    borderWidth: 1,
    borderColor: "#e7e7e7",
  },

  imgPosition: {
    marginLeft: "5%",
  },

  fontPosition: {
    height: "100%",
    justifyContent: "center",
    marginLeft: "5%",
    marginHorizontal: "10%",
  },

  titleStyle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "rgba(29, 42, 64, 1)",
    lineHeight: 22,
  },

  textStyle: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    // width: text ,
    lineHeight: 20,
    marginTop: 5,
    color: "rgba(29, 42, 64, 1)",
  },
});
