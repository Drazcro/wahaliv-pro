import Toast from "react-native-expo-popup-notifications";

type Params = {
  title?: string;
  text?: string;
  onPress?: () => void;
};
function success(params: Params) {
  Toast.show({
    type: "success",
    text1: params?.title ?? "",
    text2: params?.text ?? "",
    onPress: params.onPress,
  });
}

function error(params: Params) {
  Toast.show({
    type: "error",
    text1: params?.title ?? "",
    text2: params?.text ?? "",
    onPress: params.onPress,
  });
}

function tournee(params: Params) {
  Toast.show({
    type: "tournee",
    text1: params?.title ?? "",
    text2: params?.text ?? "",
    onPress: params.onPress,
  });
}

export default {
  success,
  error,
  tournee,
};
