import React from "react";
import { View, Text } from "react-native";
import moment from "moment";
import "moment/locale/fr";

import { styles } from "../../../../styles/delivery/invoice_card.style";
import Path from "assets/image_svg/Path.svg";
import Euro from "assets/image_svg/Euro.svg";
import Bonus from "assets/image_svg/Rocket.svg";
import Clock from "assets/image_svg/ClockDark.svg";
import { IInvoiceDataCourse } from "src/interfaces/delivery/finance";
import { parse_to_float_number } from "src/services/helpers";

moment.locale("fr");

export function InvoiceCardByID({
  invoice_data_course,
}: {
  invoice_data_course: IInvoiceDataCourse;
}) {
  const newDateFormat = invoice_data_course.delivery_date.date.split(" ");
  const time = newDateFormat[1].split(":");

  return (
    <View style={styles.cardBody}>
      <View style={styles.subCardBody}>
        <View>
          <Text style={styles.courseDelivery}>
            Course {invoice_data_course.id_delivery}
          </Text>
        </View>
        <View style={styles.timePosition}>
          <Clock />
          <Text style={styles.time}>
            {" "}
            {time[0]}h{time[1]}
          </Text>
        </View>
      </View>
      <View style={styles.customerAmount}>
        <Text style={styles.clientAmount}>
          Montant Client :{" "}
          {parse_to_float_number(invoice_data_course.total_customer)} €
        </Text>
        <Text style={styles.MPAmount}>
          Montant MPu : {parse_to_float_number(invoice_data_course.total_mp)} €
        </Text>
      </View>
      <View style={styles.total}>
        <Text style={styles.TotalAmount}>
          Total : {parse_to_float_number(invoice_data_course.total)} €
        </Text>
        <View style={styles.rightAlign}>
          <View style={styles.Distance}>
            <Path />
            <Text style={styles.TotalDistance}>
              {parse_to_float_number(invoice_data_course.distance)} km
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.PBandBonus}>
        <View style={styles.pourboires}>
          <Euro />
          <Text style={styles.pBB}>
            Pourboire {parse_to_float_number(invoice_data_course.tip)} €
          </Text>
        </View>
        <View style={styles.rightAlign}>
          <View style={styles.Bonus}>
            <Bonus />
            <Text style={styles.pBB}>
              Bonus {parse_to_float_number(invoice_data_course.bonus)} €
            </Text>
          </View>
        </View>
      </View>
    </View>
  );
}
