import { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { fontFamily } from "src/theme/typography";
import { Text } from "react-native-paper";
import Emporter from "assets/Emporter.svg";
import ScootBlack from "assets/ScootBlack.svg";
import Path from "assets/image_svg/Path.svg";
import Money from "assets/image_svg/Money.svg";
import Euro from "assets/image_svg/Euro.svg";

import { accept_course, reject_course } from "src/api/delivery/courses";
import { EventRegister } from "react-native-event-listeners";
import { soundStore } from "src/zustore/sound_store";
import moment from "moment";
import React from "react";
import { tourneeStore } from "src/zustore/tournee_store";
import { ICourseData } from "src/interfaces/delivery/courses";
import { ConfirmCourseOverlay } from "src/components/delivery/modal/confirm_course_overlay";
import { RejectCourseOverlay } from "src/components/delivery/modal/reject_course_overlay";
import { IDate } from "src/interfaces/common";
import popup from "src/components/toasts/popup";

export function NewCourseCard({ course }: { course: ICourseData }) {
  const [showAcceptModal, setShowAcceptModal] = useState(false);
  const [showRefuseModal, setShowRefuseModal] = useState(false);
  const [loadingAccept, setLoadingAccept] = useState(false);
  const [loadingRefus, setLoadingRefus] = useState(false);
  const { set_should_reload } = tourneeStore();

  const { stopSound } = soundStore();

  const stopAlertSound = async () => {
    const stop_status = await stopSound();
    console.log("sound unloaded: ", stop_status);
  };

  const on_accept_course = () => {
    accept_course(course.id_delivery)
      .then(() => {
        set_should_reload(true);
        popup.success({
          title: "Course acceptée",
          text: "La course viens d’être ajoutée sur ta tournée",
        });
        EventRegister.emitEvent("COURSE_SHOULD_RELOAD");
      })
      .catch((error) => {
        popup.error({
          text: "Veuillez vérifier votre connexion internet",
          title: "Une erreur est survenue",
        });
      })
      .finally(() => {
        setLoadingAccept(false);
        setShowAcceptModal(false);
        setShowRefuseModal(false);
        stopAlertSound();
      });
  };

  const on_reject_course = () => {
    setLoadingRefus(true);
    reject_course(course.id_delivery)
      .then(() => {
        popup.success({
          title: "Course refusée",
          text: "Le refus de la course va impacter ton bonus mensuel.",
        });
        EventRegister.emitEvent("COURSE_SHOULD_RELOAD");
      })
      .catch((error) => {
        popup.error({
          text: "Veuillez vérifier votre connexion internet",
          title: "Une erreur est survenue",
        });
      })
      .finally(() => {
        setShowRefuseModal(false);
        setLoadingRefus(false);
        stopAlertSound();
      });
  };

  return (
    <View style={styles.main}>
      <Text style={styles.title}>N° {course.id_commande}</Text>
      <AddressBox course={course} />
      <PriceBox course={course} />
      <ButtonsBox
        alert_date={course.alert_date}
        showAcceptModal={() => setShowAcceptModal(true)}
        showRefuseModal={() => setShowRefuseModal(true)}
      />
      <ConfirmCourseOverlay
        onAccept={() => on_accept_course()}
        visible={showAcceptModal}
        onClose={() => setShowAcceptModal(false)}
        loadingAccept={loadingAccept}
      />
      <RejectCourseOverlay
        onAccept={() => on_accept_course()}
        onRefuse={() => on_reject_course()}
        visible={showRefuseModal}
        onClose={() => setShowRefuseModal(false)}
        loadingAccept={loadingAccept}
        loadingRefus={loadingRefus}
      />
    </View>
  );
}

function AddressBox({ course }: { course: ICourseData }) {
  return (
    <View style={{ flexDirection: "column", marginTop: 19, marginLeft: 22 }}>
      <View style={styles.lineDash} />
      <View style={{ flexDirection: "row" }}>
        <View style={styles.icon}>
          <Emporter />
        </View>
        <View style={styles.adress_box}>
          <View style={styles.collecter_box}>
            <Text style={styles.collecter_text_bold}>
              Collecter à {course.pickup_info.pickup_hour}
            </Text>
            <Text
              lineBreakMode="middle"
              style={styles.collecter_text_medium}
              ellipsizeMode="middle"
            >
              {course.pickup_info.partner}
            </Text>
          </View>
          <Text style={styles.collecter_text_regular}>
            {course.pickup_info.pickup_address}
          </Text>
        </View>
      </View>
      <View style={{ flexDirection: "row", marginTop: 13 }}>
        <View style={styles.icon}>
          <ScootBlack />
        </View>
        <View style={styles.adress_box}>
          <View style={styles.collecter_box}>
            <Text style={styles.collecter_text_bold}>
              Livrer à {course.delivery_info.delivery_hour}
            </Text>
            <Text style={styles.collecter_text_medium}>
              {course.delivery_info.customer}
            </Text>
          </View>
          <Text style={styles.collecter_text_regular}>
            {course.delivery_info.delivery_address}
          </Text>
        </View>
      </View>
    </View>
  );
}

function PriceBox({ course }: { course: ICourseData }) {
  return (
    <View style={styles.price_box}>
      <View style={styles.distance}>
        <Path style={{ marginLeft: 4 }} />
        <Text style={{ ...styles.distance_text, marginLeft: 16 }}>
          Distance{" "}
          <Text style={styles.distance_text_bold}>{course.distance} Km</Text>
        </Text>
      </View>
      <View style={styles.distance}>
        <Money />
        <Text style={{ ...styles.distance_text, marginLeft: 15 }}>
          Gains{" "}
          <Text style={styles.distance_text_bold}>{course.earnings} €</Text>
        </Text>
      </View>
      <View style={styles.distance}>
        <Euro style={{ marginLeft: 4 }} />
        <View
          style={{
            backgroundColor: "#ECC53B",
            marginLeft: 18,
            borderRadius: 5,
          }}
        >
          <Text
            style={{ ...styles.distance_text, marginLeft: 5, marginRight: 5 }}
          >
            Pourboires{" "}
            <Text style={styles.distance_text_bold}>{course.tips} €</Text>
          </Text>
        </View>
      </View>
    </View>
  );
}

function ButtonsBox({
  showAcceptModal,
  showRefuseModal,
  alert_date,
}: {
  showAcceptModal: () => void;
  showRefuseModal: () => void;
  alert_date: IDate;
}) {
  //@ts-ignore
  let interval;

  const [duration, setDuration] = React.useState<moment.Duration>();
  const [disabled, setDisabled] = React.useState(false);

  React.useEffect(() => {
    if (disabled === true) {
      setDuration(undefined);
    }
  }, [disabled]);

  const seconds = React.useMemo(() => {
    if (duration) {
      return duration.seconds();
    }
    return 0;
  }, [duration]);

  const minutes = React.useMemo(() => {
    if (duration) {
      return duration.minutes();
    }
    return 0;
  }, [duration]);

  const calculateTimeLeft = () => {
    const eventTime = moment(alert_date.date).unix();
    //const eventTime = (moment(Date.now()).add(30, "seconds")).unix();
    const currentTime = Math.floor(Date.now() / 1000);
    const leftTime = eventTime - currentTime;
    //console.log(leftTime)
    let duration = moment.duration(leftTime, "seconds");
    //const interval = 1000;
    if (duration.asSeconds() <= 0) {
      //@ts-ignore
      clearInterval(interval);
      //window.location.reload(true); //#skip the cache and reload the page from the server
    }
    duration = moment.duration(duration.asSeconds() - 1, "seconds");
    setDuration(duration);
    //return (duration.days()+ ':' + duration.minutes().toString().padStart(2, "0") + ':' + duration.seconds().toString().padStart(2, "0"));
  };

  // const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());

  React.useEffect(() => {
    //console.log(duration?.asSeconds());
    interval = setInterval(calculateTimeLeft, 1000);
    if (duration && duration?.asSeconds() < 0) {
      //clearInterval(interval)
    }
    //console.log(duration?.asSeconds(), 'seconds')
    //@ts-ignore
    return () => clearInterval(interval);
  }, [duration]);

  React.useEffect(() => {
    const eventTime = moment(alert_date.date).unix();
    //const eventTime = (moment(Date.now()).add(30, "seconds")).unix();
    const currentTime = Math.floor(Date.now() / 1000);
    const leftTime = eventTime - currentTime;
    setDuration(moment.duration(leftTime, "seconds"));
  }, []);

  const getCountdown = React.useMemo(() => {
    if ((minutes <= 0 && seconds <= 0) || minutes > 10) {
      return "";
    }
    return `${minutes.toString().padStart(2, "0")}:${seconds
      .toString()
      .padStart(2, "0")}`;
  }, [minutes, seconds]);

  const isDisabled = React.useMemo(() => {
    return minutes <= 0 && seconds <= 0;
  }, [minutes, seconds]);

  return (
    <View style={styles.boutton_box}>
      <TouchableOpacity
        style={{
          ...styles.boutton_green,
          backgroundColor: isDisabled ? "lightgray" : "#40ADA2",
        }}
        onPress={showAcceptModal}
        disabled={isDisabled}
      >
        <Text style={styles.button_text_white}>Accepter {getCountdown}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          ...styles.boutton_red,
          backgroundColor: isDisabled ? "lightgray" : "#FFFFFF",
          borderColor: isDisabled ? "lightgray" : "#F36A72",
        }}
        onPress={showRefuseModal}
        disabled={isDisabled}
      >
        <Text style={styles.button_text_red}>Refuser</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  main: {
    marginTop: 20,
    width: "90%",
    // width: 329,
    height: 406,
    borderColor: "#C4C4C4",
    borderWidth: 0.7,
    borderRadius: 5,
    paddingTop: 20,
    alignSelf: "center",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#1D2A40",
    marginLeft: 25,
  },
  adress_box: {
    flexDirection: "column",
    // width: 245,
    width: "80%",
    height: 59,
    marginLeft: 8,
  },
  collecter_box: {
    flexDirection: "row",
    flexWrap: "wrap",
  },
  collecter_text_bold: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  collecter_text_medium: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
    marginLeft: 5,
    //flex: 1
    maxWidth: 245,
  },
  collecter_text_regular: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 17,
    color: "#1D2A40",
  },
  icon: {
    width: 22,
    height: 22,
    alignItems: "center",
    justifyContent: "center",
  },
  lineDash: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: 25,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 10,
    borderColor: "#000000",
    height: 45,
  },
  price_box: {
    backgroundColor: "#F4F4F4",
    //width: 327,
    width: "100%",
    height: 90,
    justifyContent: "center",
    paddingLeft: 22,
    // marginTop: 10,
  },
  distance: {
    flexDirection: "row",
    marginBottom: 5,
    alignItems: "center",
  },
  distance_text: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 14.63,
    color: "#112842",
    marginLeft: 20,
  },
  distance_text_bold: {
    fontFamily: fontFamily.Bold,
    color: "#112842",
    fontSize: 14,
    lineHeight: 17.07,
    marginLeft: 5,
    marginRight: 5,
  },
  boutton_box: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 12,
    fontFamily: fontFamily.semiBold,
  },
  boutton_green: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#40ADA2",
    width: "90%",
    height: 42,
    borderRadius: 26,
    marginBottom: 19,
    fontFamily: fontFamily.semiBold,
  },
  boutton_red: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#FFFFFF",
    borderColor: "#F36A72",
    borderWidth: 1,
    //width: 290,
    width: "90%",
    height: 42,
    borderRadius: 26,
    fontFamily: fontFamily.semiBold,
  },
  button_text_white: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: "#FFFFFF",
  },
  button_text_red: {
    fontSize: 14,
    lineHeight: 22,
    color: "#F36A72",
    fontFamily: fontFamily.semiBold,
  },
});
