import React from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";

import Clock from "assets/image_svg/ClockBlack.svg";
import CaretRight from "assets/image_svg/CaretRight.svg";
import { ITournee } from "src/interfaces/Entities";
import { dateToDay, hour_to_string } from "src/services/helpers";
import { router } from "expo-router";
import { tourneeStore } from "src/zustore/tournee_store";

const { width } = Dimensions.get("window");

export function CardTournee({ tournee }: { tournee: ITournee }) {
  const { set_opened } = tourneeStore();

  return (
    <View>
      <TouchableOpacity
        style={styles.main}
        onPress={() => {
          set_opened(tournee);
          router.push("/delivery/screens/tournee_detail");
        }}
      >
        <View style={styles.blockTop}>
          <View
            style={{
              ...styles.blockTopLeft,
            }}
          >
            <Text style={styles.blockTopTitle}>
              Tournée #{Number(tournee.id)}
            </Text>
            <CaretRight />
          </View>
        </View>
        <View style={styles.blockBottom}>
          <View style={styles.blockBottomLeft}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Clock style={styles.icon} />
              <Text style={styles.blockBottomText}>Heure de debut</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 2 }}>
              <Text style={styles.blockBottomTextBold}>
                {dateToDay(tournee.begin_date.date)}{" "}
              </Text>
              <Text style={styles.blockBottomTextBold}>
                {hour_to_string(tournee.begin_hour)}
              </Text>
            </View>
          </View>
          <View style={styles.blockBottomRight}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-end",
                alignItems: "center",
                marginRight: 50,
              }}
            >
              <Clock style={styles.icon} />
              <Text style={styles.blockBottomText}>Heure de fin</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 5 }}>
              {/* <Clock style={styles.icon} /> */}
              <Text style={styles.blockBottomTextBold}>
                {dateToDay(tournee.begin_date.date)}{" "}
              </Text>
              <Text style={styles.blockBottomTextBold}>
                {hour_to_string(tournee.end_hour)}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "rgba(231, 231, 231, 0.4)",
    justifyContent: "center",
    alignSelf: "center",
    width: width * 1,
    //height: 120,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "transparent",
    //borderColor: "#C4C4C4",
    flexDirection: "column",
    marginBottom: 34,
  },
  blockTop: {
    height: 30,
    borderBottomWidth: 0.5,
    //borderBottomColor: "#C4C4C4",
    borderColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "rgba(231, 231, 231, 0.4)",
  },
  blockTopLeft: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    paddingHorizontal: 21,
  },
  blockTopRight: {
    width: "35%",
    height: 32,
    backgroundColor: "#ECC53B",
    alignItems: "center",
    borderTopStartRadius: 13.5,
    borderBottomStartRadius: 13.5,
    flexDirection: "row",
    paddingLeft: 11.8,
  },
  blockTopRightGreen: {
    width: "40%",
    height: 32,
    backgroundColor: "#40ADA2",
    alignItems: "center",
    borderTopStartRadius: 13.5,
    borderBottomStartRadius: 13.5,
    flexDirection: "row",
    paddingLeft: 9.5,
  },
  blockTopTimerTextGreen: {
    fontFamily: fontFamily.regular,
    color: "#fff",
    fontSize: 10,
  },
  blockTopTimerSubTextGreen: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 14,
  },
  blockTopTitle: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 16,
  },
  blockTopSubTitle: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 16,
  },
  blockTopSubTitleBold: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 16,
  },
  blockTimer: {
    flexDirection: "column",
    justifyContent: "center",
    height: 32,
    paddingVertical: 8,
  },
  blockTopTimerText: {
    fontFamily: fontFamily.Bold,
    color: "#fff",
    fontSize: 14,
  },
  blockTopTimerSubText: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 10,
  },
  blockBottom: {
    height: 60,
    flexDirection: "row",
    //backgroundColor: "#FCFAFA",
    // backgroundColor: 'red'
  },
  blockBottomLeft: {
    width: "50%",
    //borderRightWidth: 0.5,
    //borderRightColor: "#C4C4C4",
    borderColor: "#transparent",
    justifyContent: "center",
    paddingLeft: 21,
  },
  blockBottomRight: {
    width: "50%",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
    //backgroundColor: 'blue'
  },
  blockBottomText: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 22,
  },
  icon: {
    marginRight: 1,
  },
  blockBottomTextBold: {
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    fontSize: 16,
    lineHeight: 17.07,
  },
  overlayBackground: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
});
