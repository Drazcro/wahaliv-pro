import React, { useEffect, useState } from "react";
import { RefreshControl, Text, TouchableOpacity, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { IOldCourse } from "src/interfaces/delivery/courses";
import { get_finished_course } from "src/api/delivery/courses";
import { useIsFocused } from "@react-navigation/native";
import { EventRegister } from "react-native-event-listeners";
import { Input } from "@rneui/themed";
import Search from "assets/image_svg/SearchIcon.svg";
import { fontFamily } from "src/theme/typography";
import DeliveryCardV2 from "src/components/delivery/cards/delivery_card/delivery_card";
import ClearSearch from "assets/image_svg/ClearCourseSearch.svg";
import { authStore } from "src/services/auth_store";

export function ClosedCoursesTabView({
  isOnTournee,
}: {
  isOnTournee?: boolean;
}) {
  const [courses, setCourses] = useState([] as IOldCourse[]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [searchedValue, setSearchedValue] = useState("");

  const { auth } = authStore();
  const isFocused = useIsFocused();

  const onRefresh = () => {
    setRefreshing(true);
    setIsLoading(true);
    get_finished_course()
      .then((courses_) => {
        setCourses(courses_);
      })
      .finally(() => {
        setRefreshing(false);
        setIsLoading(false);
      });
  };

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "COURSE_SHOULD_RELOAD",
      onRefresh,
    );
    return () => {
      //@ts-ignore
      EventRegister.removeEventListener(listener);
    };
  }, []);

  useEffect(() => {
    onRefresh();
  }, [auth, isFocused]);

  const filteredData = React.useMemo(() => {
    if (searchedValue === "" || !searchedValue) {
      return courses;
    }
    return courses.filter((item) => {
      return (
        item.id_commande.toString().includes(searchedValue) ||
        item.commande_at.date.includes(searchedValue) ||
        item.delivery_date.date.includes(searchedValue) ||
        item.customer.includes(searchedValue) ||
        item.partner_telephone.includes(searchedValue) ||
        item.customer_telephone.includes(searchedValue) ||
        item.delivery_address.address.includes(searchedValue)
      );
    });
  }, [searchedValue, courses]);

  if (error === true) {
    return <Text>erreur de chargement</Text>;
  }

  return (
    <>
      <View>
        <Input
          inputContainerStyle={{
            borderWidth: 1,
            borderRadius: 20,
            paddingLeft: 15,
            marginTop: 20,
            marginBottom: -10,
            height: 37,
            borderColor: "#C4C4C4",
          }}
          inputStyle={{
            fontSize: 14,
            fontFamily: fontFamily.Medium,
            marginLeft: 5,
            color: "rgba(119, 119, 119, 0.79)",
          }}
          placeholder="Rechercher par numéro, nom ou date"
          leftIcon={<Search />}
          rightIcon={
            <TouchableOpacity
              style={{
                marginRight: 15,
              }}
              onPress={() => setSearchedValue("")}
            >
              {searchedValue !== "" && <ClearSearch />}
            </TouchableOpacity>
          }
          defaultValue={searchedValue}
          onChangeText={(newValue) => setSearchedValue(newValue)}
        />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {filteredData.map((item: IOldCourse) => (
          <DeliveryCardV2
            isOnTournee={isOnTournee}
            course={item}
            key={item.id_commande * Math.random() * 10}
            onAcceptPrompt={() => console.log("on accept")}
          />
        ))}
      </ScrollView>
    </>
  );
}
