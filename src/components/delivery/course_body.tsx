import React from "react";
import { View, Text, ScrollView, TouchableOpacity } from "react-native";
import { Divider } from "react-native-elements/dist/divider/Divider";
import { styles } from "src/styles/delivery/courses_en_cours_delivery.styles";

import CaretDowns from "assets/image_svg/CaretDowns.svg";
import CaretUp from "assets/image_svg/CaretUp.svg";
import { ICourseDetails } from "src/interfaces/Entities";
import MapPinRed from "assets/image_svg/MapPinRedFull.svg";
import MapPinGreen from "assets/image_svg/MapPinGreenFull.svg";

import moment from "moment";
import { fontFamily } from "src/theme/typography";
import { courseStore } from "src/zustore/coursestore";

import { authStore } from "src/services/auth_store";
import { ClosedMapPreview } from "src/components/delivery/maps/map_preview";
import { point_and_comma } from "src/services/helpers";
import {
  CheckDeliveryComplementAddress,
  CheckHowToDisplayInfoCourse,
} from "src/components/delivery/courses_components";

export const CoursesBody = () => {
  const { opened } = courseStore();
  const { base_user } = authStore();

  const [drop, setDrop] = React.useState(true);

  return (
    <View style={styles.main}>
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.globalMap}>
          <ClosedMapPreview />
        </View>

        <CheckHowToDisplayInfoCourse course={opened} baseUser={base_user} />
        <Divider style={styles.divider} />
        <View style={styles.section}>
          <View style={styles.wrapperTwo}>
            <View style={styles.lineDash} />
            <View style={styles.wrapperData}>
              <View style={styles.map}>
                <MapPinRed />
              </View>
              <View style={styles.subWrapperOne}>
                <View style={styles.width1}>
                  <Text style={styles.client1}>
                    PARTENAIRE{" "}
                    <Text style={styles.client}>
                      {opened.partner ?? opened.partner_name}
                    </Text>
                  </Text>
                  <Text style={styles.title}>{opened?.pickup_address}</Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: 14,
                      color: "#8D98A0",
                    }}
                  >
                    Commande passée le{" "}
                    {moment(opened.commande_at.date).format("DD/MM")} -{" "}
                    {moment(opened.commande_at.date).format("HH:MM")}h
                  </Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: 14,
                      color: "#8D98A0",
                    }}
                  >
                    Commande récupérée le{" "}
                    {moment(opened.pickup_date.date).format("DD/MM")} -{" "}
                    {moment(opened.pickup_date.date).format("HH:MM")}h
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.wrapperThreeSub}>
            <View style={styles.wrapperData}>
              <View style={styles.map}>
                <MapPinGreen />
              </View>
              <View style={styles.subWrapperOne}>
                <View style={styles.width}>
                  <Text style={styles.client1}>
                    CLIENT{" "}
                    <Text style={styles.client}>
                      {opened.customer ? opened.customer : opened.customer_name}
                    </Text>
                  </Text>
                  <Text style={styles.title}>
                    {opened?.delivery_address?.address}
                  </Text>
                  <CheckDeliveryComplementAddress
                    complementadresse={
                      opened?.delivery_address?.complementadresse
                    }
                  />

                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: 14,
                      color: "#8D98A0",
                    }}
                  >
                    Commande livrée le{" "}
                    {moment(opened.real_delivery_date.date).format("DD/MM")} -{" "}
                    {moment(opened.real_delivery_date.date).format("HH:MM")}h
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => setDrop(drop === true ? false : true)}
          style={styles.dropDownStyle}
        >
          <Text style={styles.textThree}>DÉTAIL DE LA COMMANDE</Text>
          {drop ? <CaretDowns /> : <CaretUp />}
        </TouchableOpacity>
        <Divider style={styles.dividerOne} />
        {drop && (
          <View style={{ ...styles.dropDown, height: drop ? "auto" : 0 }}>
            {opened &&
              opened?.details?.map((item: ICourseDetails, index: number) => (
                <View style={styles.downContent} key={index}>
                  <View
                    style={{
                      width: "10%",
                      alignItems: "center",
                    }}
                  >
                    <Text style={styles.textOrderFinish}>{item.qte}</Text>
                  </View>
                  <View style={{ width: "70%", alignItems: "flex-start" }}>
                    <Text style={styles.textOrderFinish}>{item?.name}</Text>
                  </View>
                  <View
                    style={{
                      width: "20%",
                      justifyContent: "flex-end",
                    }}
                  >
                    <Text style={styles.textOrderFinish}>
                      {point_and_comma(item.price)} €
                    </Text>
                  </View>
                </View>
              ))}
          </View>
        )}
      </ScrollView>
    </View>
  );
};
