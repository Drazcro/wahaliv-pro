import React from "react";
import { View, Text, TouchableOpacity } from "react-native";
import i18n from "i18n-js";

import { ProfileStyle as styles } from "src/styles/delivery/profile_styles";
import { IMGEntry, IMinimumGaranti } from "src/interfaces/Entities";
import { ProgressBar } from "react-native-paper";
import Sun from "assets/Sun.svg";
import Moon from "assets/Moon.svg";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-elements";
import { router } from "expo-router";

export const ServiceThreshold = ({ data }: { data: IMinimumGaranti }) => {
  return (
    <>
      <View style={styles.line}>
        <Divider color="rgba(196, 196, 196, 1)" />
      </View>
      <View style={styles.progress_container}>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <Text style={styles.min_garanti}>{i18n.t("profile.minimun")}</Text>
          <TouchableOpacity
            onPress={() => router.push("/more_information_screen")}
          >
            <Text style={styles.more}>Plus d'info</Text>
          </TouchableOpacity>
        </View>

        {data?.midi &&
        data?.midi.amount !== 0 &&
        data?.midi.nb_services !== 0 ? (
          <Row data={data.midi} type={"midi"} />
        ) : null}
        {data?.soir &&
        data?.soir.amount !== 0 &&
        data?.soir.nb_services !== 0 ? (
          <Row data={data.soir} type={"soir"} />
        ) : null}
      </View>
    </>
  );
};

const Row = ({ data, type }: { data: IMGEntry; type: "midi" | "soir" }) => {
  return (
    <View style={{ width: "100%" }}>
      <View style={styles.groupe_value}>
        {type === "midi" ? <Sun /> : <Moon />}
        <Text style={styles.text_service}>
          {data.nb_services_done ? data.nb_services_done : 0} /{" "}
          {data.nb_services ? data.nb_services : 0}
        </Text>
      </View>
      <ProgressBar
        progress={
          data.nb_services_done && data.nb_services_done !== undefined
            ? data.nb_services_done / data.nb_services
            : 0.5
        }
        style={styles.progress_bar}
        color={"#FF5C85"}
      />

      <View style={{ width: "100%", flexDirection: "row", flexWrap: "wrap" }}>
        <Text style={{ ...styles.text_rappelle }}>
          Reste{" "}
          <Text
            style={{
              ...styles.text_rappelle,
              color: "#FF5C85",
              fontFamily: fontFamily.semiBold,
            }}
          >
            {data.nb_services - data.nb_services_done} services du {type}
          </Text>{" "}
          ({data.hours.from}h à {data.hours.to}h)
          {data.specific_day_nb_services > 0 ? " dont " : ""}
          {data.specific_day_nb_services > 0 ? (
            <Text
              style={{ ...styles.text_rappelle, fontFamily: fontFamily.Bold }}
            >
              {data.specific_day_nb_services} services{" "}
            </Text>
          ) : (
            ""
          )}
          {data.specific_day_nb_services > 0 ? (
            <Text
              style={{
                ...styles.text_rappelle,
                fontFamily: fontFamily.semiBold,
              }}
            >
              les {data.days}{" "}
            </Text>
          ) : (
            " "
          )}
          pour obtenir {data.amount} €
        </Text>
      </View>
    </View>
  );
};
