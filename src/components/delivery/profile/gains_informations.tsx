import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { fontFamily } from "src/theme/typography";
import CaretRight from "assets/image_svg/CaretRightBlue.svg";
import { router } from "expo-router";
import { parse_to_float_number } from "src/services/helpers";

interface IProfileInformationsProps {
  title: string;
  icon?: SVGElement;
  value: string | number;
  unit?: string;
  icon_plus?: HTMLImageElement;
  startText?: string;
  textValue?: string | number;
  endText?: string;
  tipTextStart?: string;
  tipTextValue: string | number;
  tipTextEnd?: string;
}

export const GainsInformations = (props: IProfileInformationsProps) => {
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => router.push("/delivery/screens/financial_impact_screen")}
    >
      <View style={styles.content_item_one}>
        <View
          style={{
            flexDirection: "column",
            alignItems: "center",
            justifyContent: "flex-start",
          }}
        >
          <View style={styles.content_value}>
            <Text style={styles.text_value}>{props.value}</Text>
            <Text style={styles.text_unity}> {props.unit}</Text>
          </View>
          <View style={styles.content_title}>
            <Text style={styles.text_title}>{props.title}</Text>
          </View>
        </View>
        <View style={styles.content_message}>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={styles.text_message}>{props.startText}</Text>
            <Text style={styles.text_message_value}> {props.textValue} </Text>
            <Text style={styles.text_message}>{props.endText}</Text>
          </View>
          {parseInt(props.tipTextValue.toString()) === 0 ? (
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.text_message}>{props.tipTextStart}</Text>
              <Text style={styles.text_message_value}>
                {" "}
                {parse_to_float_number(props.tipTextValue)} €{" "}
              </Text>
              <Text style={styles.text_message}>{props.tipTextEnd}</Text>
            </View>
          ) : null}
        </View>
        <CaretRight />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 10,
    paddingLeft: 18,
    paddingRight: 9,
    backgroundColor: "rgba(217, 217, 217, 0.2)",
    borderRadius: 11,
    flexDirection: "row",
    height: 57,
  },
  content_title: {
    marginBottom: 10,
  },
  text_title: {
    fontFamily: fontFamily.Medium,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 12,
    lineHeight: 14.63,
  },
  content_item_one: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    flex: 1,
  },
  icon: {
    paddingRight: 5,
  },
  content_value: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 10,
  },
  text_value: {
    fontFamily: fontFamily.Bold,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 18,
    lineHeight: 21.94,
  },
  text_unity: {
    fontFamily: fontFamily.Bold,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 18,
  },
  text_message: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 13,
    lineHeight: 14.63,
    fontFamily: fontFamily.Medium,
    marginVertical: 1,
  },
  text_message_value: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 13,
    lineHeight: 14.6,
    fontFamily: fontFamily.Bold,
    marginVertical: 1,
  },
  content_message: {
    alignSelf: "center",
    flexDirection: "column",
  },
});
