import {
  ActivityIndicator,
  RefreshControl,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  ScrollView,
} from "react-native";

import React, { useEffect, useState } from "react";

import { styles } from "src/styles/delivery/invoice.style";

import Euro from "assets/image_svg/Euro.svg";
import Bonus from "assets/image_svg/Rocket.svg";

import { Icon } from "@rneui/themed";
import moment from "moment";
import { Overlay } from "react-native-elements";
import {
  InvoiceCard,
  InvoiceFinishedCard,
} from "src/components/delivery/cards/invoice_card/invoice_card";
//import * as Sentry from "@sentry/react-native";
import { get_daily_bill, get_invoice_bill } from "src/api/delivery/profile";
import { parse_to_float_number } from "src/services/helpers";
import {
  IBillsDetails,
  IDayInvoice,
  IInvoiceAPIResponse,
} from "src/interfaces/delivery/finance";
import FactureFilter from "src/components/delivery/modal/facture_filter_overlay";

export function FuturInvoiceTabView() {
  const [data, setData] = useState<IInvoiceAPIResponse>({
    total_tip: 0,
    total_bonus: 0,
    global_data: [],
  });

  const [startDate, setStartDate] = useState<moment.Moment>(
    moment().subtract(30, "days"),
  );
  const [endDate, setEndDate] = useState<moment.Moment>(moment());

  const [isFilterModal, setIsFilterModal] = useState(false);
  const [page, setPage] = useState(1);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  async function load_or_reload_data() {
    const begin_date = startDate.format("YYYY-MM-DD").toLocaleString();
    const end_date = endDate.format("YYYY-MM-DD").toLocaleString();
    get_daily_bill(begin_date, end_date, page).then((response) => {
      setData(response);
    });
  }

  function onRefresh() {
    setIsRefreshing(true);
    load_or_reload_data().finally(() => {
      setIsRefreshing(false);
    });
  }

  useEffect(() => {
    onRefresh();
  }, []);

  useEffect(() => {
    if (isFilterModal === false) {
      setIsLoading(true);

      load_or_reload_data().finally(() => {
        setIsLoading(false);
      });
    }
  }, [isFilterModal]);

  useEffect(() => {
    data.global_data.forEach((i) => console.log(i));
  }, [data]);

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }

  return (
    <React.Fragment>
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
        }
      >
        <View style={styles.RappelBgColor}>
          <View style={styles.alignement}>
            <Text style={styles.Mediumtxt}>
              <Text style={styles.boldtxt}>Rappel : </Text>
              <Text>Les factures sont payées </Text>
              <Text style={styles.boldtxt}>7 jours ouvrés </Text>
              <Text>après leurs date d'édition.</Text>
            </Text>
          </View>
        </View>
        <View style={styles.BonusAndTips}>
          <Text style={styles.resume}>Résumé des bonus et pourboires</Text>
        </View>
        <View style={styles.tipsandBonusBg}>
          <View style={styles.tipsandBonus}>
            <View style={styles.tips}>
              <Euro />
              <Text style={styles.resume}>
                {" "}
                Pourboires {parse_to_float_number(data?.total_tip)} €
              </Text>
            </View>
            <View style={styles.bonus}>
              <Bonus />
              <Text style={styles.resume}>
                {" "}
                Bonus {parse_to_float_number(data?.total_bonus)} €
              </Text>
            </View>
          </View>
          <Text style={styles.textPosition}>
            Les bonus et pourboires sont payés sur la dernière facture du mois
          </Text>
        </View>
        <View style={styles.gainTextPosition}>
          <Text style={styles.resume}>Gain journaliers</Text>
          <TouchableOpacity
            style={styles.filterContainer}
            onPress={() => setIsFilterModal(true)}
          >
            <Text style={styles.filterText}>Filtrer le résultat</Text>
            <Icon
              name="sliders"
              type="font-awesome"
              iconStyle={{ paddingRight: 10 }}
              size={13}
              color="#112842"
            />
          </TouchableOpacity>
        </View>
        <View style={styles.invoiceCard}>
          {data.global_data.map((item: IDayInvoice, index: number) => (
            <InvoiceCard key={index} {...item} />
          ))}
        </View>
      </ScrollView>
      <Overlay
        isVisible={isFilterModal}
        onBackdropPress={() => setIsFilterModal(false)}
        overlayStyle={{ width: "100%", height: "100%" }}
      >
        <FactureFilter
          startDate={startDate}
          endDate={endDate}
          setStartDate={setStartDate}
          setEndDate={setEndDate}
          onClose={() => setIsFilterModal(false)}
          onApply={() => {
            setIsFilterModal(false);
          }}
        />
      </Overlay>
    </React.Fragment>
  );
}

export function InvoiceTabView() {
  const [data, setData] = useState<IBillsDetails[]>([]);
  const currentMonth = moment().format("MMMM");

  const [isFilterModal, setIsFilterModal] = useState(false);

  const [startDate, setStartDate] = useState<moment.Moment>(
    moment().subtract(30, "days"),
  );
  const [endDate, setEndDate] = useState<moment.Moment>(moment());

  const [page, setPage] = useState<number>(1);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  async function load_or_reload_data() {
    const begin_date =
      startDate && startDate.format("YYYY-MM-DD").toLocaleString();
    const end_date = endDate && endDate.format("YYYY-MM-DD").toLocaleString();

    let finished_bill: IBillsDetails[] = [];
    try {
      finished_bill = await get_invoice_bill();
      setData(finished_bill);
    } catch (e) {
      console.error(e);
      //Sentry.captureException(e);
    }
  }

  function onRefresh() {
    setIsRefreshing(true);
    load_or_reload_data().finally(() => {
      setIsRefreshing(false);
    });
  }

  useEffect(() => {
    setIsLoading(true);
    load_or_reload_data().finally(() => {
      setIsLoading(false);
    });
  }, []);

  const getData = React.useMemo(() => {
    if (data?.length > 0) {
      return data.filter(
        (item) =>
          moment(item.begin_date.date).isSameOrAfter(moment(startDate)) &&
          moment(item.end_date.date).isSameOrBefore(moment(endDate)),
      );
    }
    return [];
  }, [data, isFilterModal]);

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }

  return (
    <ScrollView
      refreshControl={
        <RefreshControl refreshing={isRefreshing} onRefresh={onRefresh} />
      }
    >
      <View style={styles.RappelBgColor}>
        <View style={styles.alignement}>
          <Text style={styles.Mediumtxt}>
            <Text style={styles.boldtxt}>Rappel : </Text>
            <Text>Les factures sont payées </Text>
            <Text style={styles.boldtxt}>7 jours ouvrés </Text>
            <Text>après leurs date d'édition.</Text>
          </Text>
        </View>
      </View>
      <View style={styles.gainTextPosition}>
        <Text style={styles.resume}>Décomptes</Text>

        <TouchableOpacity
          style={styles.filterContainer}
          onPress={() => setIsFilterModal(true)}
        >
          <Text style={styles.filterText}>Filtrer le résultat</Text>
          <Icon
            name="sliders"
            type="font-awesome"
            iconStyle={{ paddingRight: 10 }}
            size={13}
            color="#112842"
          />
        </TouchableOpacity>
      </View>
      <View style={styles.invoiceCard}>
        {getData.length > 0 ? (
          getData?.map((item: IBillsDetails, index: number) => (
            <InvoiceFinishedCard bill={item} key={index} />
          ))
        ) : (
          <View
            style={{
              backgroundColor: "rgba(218, 218, 218, 0.31)",
              marginVertical: 5,
              paddingVertical: 5,
              paddingLeft: 26,
            }}
          >
            <Text style={styles.Mediumtxt}>
              Pas de décompte disponible sur la période (
              <Text style={styles.boldtxt}>{currentMonth}</Text>)
            </Text>
          </View>
        )}
      </View>
      {/* {isLoading ? <ActivityIndicator /> :<LoadMoreData onPress={()=>setPage(v=>v+1)} />} */}

      <Overlay
        isVisible={isFilterModal}
        onBackdropPress={() => setIsFilterModal(false)}
        overlayStyle={{ width: "100%", height: "100%" }}
      >
        <FactureFilter
          startDate={startDate}
          endDate={endDate}
          setStartDate={setStartDate}
          setEndDate={setEndDate}
          onClose={() => setIsFilterModal(false)}
          onApply={() => {
            setIsFilterModal(false);
          }}
        />
      </Overlay>
    </ScrollView>
  );
}
