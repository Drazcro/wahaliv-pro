import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";

import { ProfileStyle as styles } from "src/styles/delivery/profile_styles";
import Note from "assets/image_svg/Note.svg";
import { fontFamily } from "src/theme/typography";
import { router } from "expo-router";
import { get_deliveryman_docs } from "src/api/delivery/profile";

export const MissingDocs = ({ setDoc }: { setDoc: (v: boolean) => void }) => {
  const [is_doc_ok, set_is_doc_ok] = useState(true);

  const load_or_reload_from_backend = async () => {
    try {
      const response = await get_deliveryman_docs();
      const status =
        response.contract === true &&
        response.identityRecto === true &&
        response.identityVerso === true &&
        response.registration_proof === true &&
        response.rib === true;
      set_is_doc_ok(status);
    } catch (e) {
      console.error(e);
    }
  };

  React.useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  React.useEffect(() => {
    setDoc(is_doc_ok);
  }, [is_doc_ok]);

  if (!is_doc_ok) {
    return (
      <View style={styles.missingDocs}>
        <View style={styles.NotePosition}>
          <Note />
          <Text style={styles.missingDocsTitle}>Documents manquants</Text>
        </View>
        <View style={styles.missingDocsText}>
          <Text
            style={{
              fontFamily: fontFamily.regular,
              color: "rgba(29, 42, 64, 1)",
              lineHeight: 20,
              fontSize: 14,
            }}
          >
            Des documents sont manquants
          </Text>

          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <Text
              style={{
                fontFamily: fontFamily.regular,
                color: "rgba(29, 42, 64, 1)",
                lineHeight: 20,
                fontSize: 14,
              }}
            >
              Pour déclencher tes paiement
            </Text>
            <TouchableOpacity
              style={{ marginTop: 3 }}
              onPress={() => {
                router.push("/delivery/screens/supporting_documents_screen");
              }}
            >
              <Text style={styles.missingDocsTextBold}> cliques ici</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  return <></>;
};
