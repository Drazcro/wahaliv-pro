import i18n from "i18n-js";
import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-elements";
import { ProfileNavigationProps } from "src/interfaces/delivery/profile";

export const ProfileNavigation = (props: ProfileNavigationProps) => {
  return (
    <>
      {props.isSalarie === true ? (
        props.title !== i18n.t("profile.userInfo") &&
        props.title !== i18n.t("profile.Receipt") &&
        props.title !== i18n.t("profile.changePassword") &&
        props.title !== i18n.t("profile.justfication") &&
        props.title !== i18n.t("profile.coordonneBank") &&
        props.title !== i18n.t("profile.bill") &&
        props.title !== i18n.t("profile.planning") && (
          <TouchableOpacity onPress={props.onPress} style={styles.container}>
            <View style={styles.textContainer}>
              <View style={styles.left_icon}>{props.iconLeft}</View>
              <Text style={styles.text_title}>{props.title}</Text>
            </View>
            {props.iconRight && (
              <View style={styles.right_icon}>{props.iconRight}</View>
            )}
          </TouchableOpacity>
        )
      ) : (
        <TouchableOpacity onPress={props.onPress} style={styles.container}>
          <View style={styles.textContainer}>
            <View style={styles.left_icon}>{props.iconLeft}</View>
            <Text style={styles.text_title}>{props.title}</Text>
          </View>
          {props.iconRight && (
            <View style={styles.right_icon}>{props.iconRight}</View>
          )}
        </TouchableOpacity>
      )}
      {props.index === 2 && (
        <View style={{ marginBottom: 20 }}>
          <Divider color="rgba(196, 196, 196, 1)" />
        </View>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    alignItems: "center",
    marginHorizontal: 13,
    marginBottom: Platform.OS === "android" ? 26 : 24,
  },
  textContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignContent: "center",
    alignItems: "center",
  },
  left_icon: {
    color: "rgba(17, 40, 66, 1)",
    marginRight: 7,
  },
  right_icon: {
    color: "rgba(17, 40, 66, 1)",
  },
  text_title: {
    fontSize: 16,
    fontFamily: fontFamily.regular,
    color: "rgba(17, 40, 66, 1)",
    left: 10,
  },
});
