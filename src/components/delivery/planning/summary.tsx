import { STATUS } from "src/constants/status";
import React, { useEffect, useState } from "react";
import { FlatList, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Divider } from "react-native-elements";
import { recover_delivery_planning_summary } from "src/api/delivery/planning";
import { IPlanningsSummary } from "src/interfaces/delivery/planning";
import { DaysItems } from "src/components/delivery/days_items";

export function Summary(props: {
  setDate: (arg0: Date) => void;
  setTabIndex: (arg0: number) => void;
  tabIndex: number;
}) {
  const [data, setData] = useState([] as IPlanningsSummary);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if (props.tabIndex === 1) {
      recover_delivery_planning_summary(STATUS.MARKETPLACE)
        .then((summary: IPlanningsSummary) => {
          setData(summary);
        })
        .catch(() => {
          setError(true);
        })
        .finally(() => {
          setIsLoading(false);
          setError(false);
        });
    }
  }, [props.tabIndex]);

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={data}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => {
          return (
            <View style={{ marginTop: 20 }}>
              <DaysItems
                item={item}
                setDate={props.setDate}
                setIndex={props.setTabIndex}
              />
              <Divider
                style={{
                  borderWidth: 1,
                  borderColor: "#C4C4C4",
                  marginVertical: 8,
                }}
              />
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
