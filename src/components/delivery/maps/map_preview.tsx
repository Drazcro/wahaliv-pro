import * as React from "react";
import MapView, { Marker, Region } from "react-native-maps";
import { StyleSheet, View, Dimensions } from "react-native";
import { COURSE_STATUS } from "src/constants/course";
import MapPinRed from "assets/image_svg/MapPinRed.svg";
import MapPinGreen from "assets/image_svg/MapPinGreen.svg";
import { courseStore } from "src/zustore/coursestore";

const { width, height } = Dimensions.get("window");

const LATITUDE_DELTA = 0.0125;
const LONGITUDE_DELTA = 0.0125 * (height / width);

export interface ILatLong {
  latitude: number;
  longitude: number;
}

export default function MapPreview(props: { coords?: ILatLong }) {
  let initialRegion = {} as Region;
  if (props.coords) {
    initialRegion = {
      latitude: props.coords.latitude,
      longitude: props.coords.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    };
  } else {
    const { opened } = courseStore();

    const isRecovered = opened.delivery_status == COURSE_STATUS.RECOVERED;
    initialRegion = {
      latitude: isRecovered ? opened.customer_lat : opened.partner_lat,
      longitude: isRecovered ? opened.customer_lng : opened.partner_lng,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    };
  }

  return (
    <View style={styles.container}>
      <MapView style={styles.map} region={initialRegion}>
        <Marker
          coordinate={{
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude,
          }}
        >
          <MapPinRed width={30} height={30} />
        </Marker>
      </MapView>
    </View>
  );
}

export function ClosedMapPreview() {
  const { opened } = courseStore();

  const partner_coords = React.useMemo(() => {
    return {
      latitude: opened.partner_lat,
      longitude: opened.partner_lng,
    };
  }, [opened]);

  const client_coords = React.useMemo(() => {
    return {
      latitude: opened.customer_lat,
      longitude: opened.customer_lng,
    };
  }, [opened]);

  const region = React.useMemo(() => {
    return {
      latitude: calculate_median(
        partner_coords.latitude,
        client_coords.latitude,
      ),
      longitude: calculate_median(
        partner_coords.longitude,
        client_coords.longitude,
      ),
      latitudeDelta: Math.abs(opened.partner_lat - opened.customer_lat) * 2,
      longitudeDelta: Math.abs(opened.partner_lng - opened.customer_lng) * 2,
    };
  }, [client_coords, partner_coords]);

  return (
    <View style={styles.container}>
      <MapView style={styles.map} region={region}>
        <Marker coordinate={partner_coords}>
          <MapPinRed width={30} height={30} />
        </Marker>
        <Marker coordinate={client_coords}>
          <MapPinGreen width={30} height={30} />
        </Marker>
      </MapView>
    </View>
  );
}

function calculate_median(a: number, b: number): number {
  return Math.min(a, b) + Math.abs(a - b) / 2.0;
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    width: "100%",
    height: 220,
  },
});
