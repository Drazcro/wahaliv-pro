import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import React, { useState } from "react";

import ScooterWaitBlack from "assets/ScooterWaitBlack.svg";

import { ActivityIndicator } from "react-native-paper";
import moment from "moment";

import { EventRegister } from "react-native-event-listeners";
import { NewCourseCard } from "src/components/delivery/cards/courses/new_course";
import { router } from "expo-router";
import { get_new_course } from "src/api/delivery/courses";

import { ICourse, ICourseData } from "src/interfaces/delivery/courses";
import { soundStore } from "src/zustore/sound_store";

const EMPTY_COURSE = {
  total_data: 0,
  total: 0,
  data: [],
  hasTournee: false,
  tournee_begin_at: moment().format(),
} as ICourse;

export function NewCourseDesign({
  newCoursesData,
  refreshing,
  refreshCourses,
  isLoading,
  onTouchStart,
}: {
  newCoursesData: ICourse;
  refreshing: boolean;
  refreshCourses: () => void;
  isLoading: boolean;
  onTouchStart: () => void;
}) {
  const newCourses = newCoursesData.data;

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading} onTouchStart={onTouchStart}>
        <ScrollView contentContainerStyle={styles.loading_scroll}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }
  return (
    <React.Fragment>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refreshCourses} />
        }
        contentContainerStyle={
          newCourses.length == 0
            ? { flexGrow: 1, justifyContent: "center" }
            : {}
        }
      >
        {newCourses.length != 0 ? (
          newCourses.map((course: ICourseData) => (
            <NewCourseCard
              course={course}
              key={course.id_commande + Math.random() * 1000}
            />
          ))
        ) : (
          <NoCourse />
        )}
      </ScrollView>
    </React.Fragment>
  );
}

function NoCourse() {
  return (
    <View style={styles.no_course_main}>
      <ScooterWaitBlack />
      <Text style={styles.no_course_title}>Pas encore de course</Text>
      <Text style={styles.no_course_description}>
        Attend quelques minutes et une
      </Text>
      <Text style={styles.no_course_description}>
        nouvelle course apparaitra
      </Text>
    </View>
  );
}

export const CoursesHome = () => {
  const [refreshing, setRefreshing] = React.useState(true);
  const [new_courses, set_new_courses] = useState(EMPTY_COURSE);

  const { playSound, stopSound } = soundStore();

  const playAlertSound = async () => {
    await playSound();
  };

  const stopAlertSound = async () => {
    await stopSound();
  };

  const updateCourses = async () => {
    setRefreshing(true);

    try {
      const fetched_data = await get_new_course();
      const new_data = fetched_data.data.filter(
        (item) => !new_courses.data.includes(item),
      );
      set_new_courses(fetched_data);
      if (new_data.length > 0) {
        //router.push("/delivery/tab/courses");
        playAlertSound();
      } else {
        console.log("no new courses found");
        stopAlertSound();
      }

      return Promise.resolve(new_data.length > 0);
    } catch (e) {
      console.error(e);
      stopAlertSound();
      return Promise.resolve(false);
    } finally {
      setRefreshing(false);
    }
  };

  const onRefresh = () => {
    updateCourses();
  };

  React.useEffect(() => {
    updateCourses();
  }, []);

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "COURSE_SHOULD_RELOAD",
      onRefresh,
    );

    return () => {
      //@ts-ignore
      EventRegister.removeEventListener(listener);
    };
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      onRefresh();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  return (
    <NewCourseDesign
      onTouchStart={() => stopAlertSound()}
      newCoursesData={new_courses}
      refreshing={refreshing}
      refreshCourses={onRefresh}
      isLoading={refreshing}
    />
  );
};

const styles = StyleSheet.create({
  /** no course styles */
  no_course_main: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  no_course_title: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 20,
    marginTop: 18,
    marginBottom: 11,
    color: "#1D2A40",
  },
  no_course_description: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 17,
    color: "#1D2A40",
  },
  /** message tournee styles */
  message_tournee_main: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  message_tournee_text_regular: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  message_tournee_text_bold: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  message_tournee_text_bold_pink: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: "#FF5C85",
  },
  /** loading render styles */
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loading_scroll: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
