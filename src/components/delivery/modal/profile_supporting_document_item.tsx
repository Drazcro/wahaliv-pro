import React, { useState } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-elements";
import I18n from "i18n-js";
import * as DocumentPicker from "expo-document-picker";
import Toast from "react-native-expo-popup-notifications";

import Check from "assets/image_svg/Check.svg";
import Errors from "assets/image_svg/Errors.svg";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

interface IButtonDownloadProps {
  hasFile: boolean;
  handleUploadFile: () => void;
}

interface IProfileSupportingDocumentItemProps {
  hasFile: boolean;
  fileTitle: string;
  setFile: (arg0: DocumentPicker.DocumentPickerAsset) => void;
  setErrorSize: (arg0: boolean) => void;
  setErrorFile: (arg0: boolean) => void;
  setShowModalError: (arg0: boolean) => void;
}

const ButtonDowload = (props: IButtonDownloadProps) => {
  return (
    <Button
      type="solid"
      title={
        props.hasFile
          ? I18n.t("profile.supportingDocument.buttonUpdate")
          : I18n.t("profile.supportingDocument.buttonDownload")
      }
      titleStyle={{
        ...styles.title,
        color: props.hasFile ? "rgba(29, 42, 64, 1)" : "rgba(255, 255, 255, 1)",
      }}
      buttonStyle={{
        ...styles.button_download,
        backgroundColor: props.hasFile ? "#fff" : color.ACCENT,
        borderColor: color.ACCENT,
        borderWidth: 1,
      }}
      onPress={props.handleUploadFile}
    />
  );
};

export function ProfileSupportingDocumentItem(
  props: IProfileSupportingDocumentItemProps,
) {
  const [hasError, setHasError] = useState(false);
  const [selected, setSelected] = useState(false);

  const isChecked = (props.hasFile || selected) && !hasError;
  const isCheckedOrHasError = !isChecked && !hasError;

  const checkFileValide = (file: DocumentPicker.DocumentPickerResult) => {
    if (!file.assets) {
      return;
    }

    const doc = file.assets[0];

    console.log(doc);

    const size: number = doc.size ?? 0;
    const type: string = doc.mimeType ?? "";
    const limit = 6000000;

    const hadImg = type.includes("image");
    const hadPdf = type.includes("pdf");

    const isFileTooBig = size > limit;
    const isFileValide = !hadImg && !hadPdf;

    if (isFileTooBig) {
      props.setErrorSize(true);
      props.setShowModalError(true);
      setHasError(true);
    }

    if (!isFileTooBig && isFileValide) {
      props.setErrorFile(true);
      props.setShowModalError(true);
      setHasError(true);
    }

    if (!isFileTooBig && !isFileValide) {
      props.setFile(file);
      setHasError(false);
      setSelected(true);
      Toast.show({
        type: "success",
        text1: "Success",
        text2: I18n.t("profile.notification.file.message"),
      });
    }
  };

  const handleUploadFile = () => {
    DocumentPicker.getDocumentAsync()
      .then((file: DocumentPicker.DocumentPickerResult) => {
        if (!file.canceled) {
          checkFileValide(file);
        }
      })
      .catch((err) => {
        console.error(err);
      });
  };

  return (
    <View style={styles.main}>
      <View style={styles.error_icon}>
        {isChecked && <Check />}
        {hasError && <Errors />}
      </View>
      <View
        style={{
          ...styles.text_container,
          marginLeft: isCheckedOrHasError ? 15 : 0,
        }}
      >
        <View style={styles.text_main}>
          <Text style={styles.document_text}>{props.fileTitle}</Text>
          <Text style={styles.document_type_text}>
            {I18n.t("profile.supportingDocument.fileType")}
          </Text>
          {hasError && (
            <Text style={styles.uploadError}>
              {I18n.t("profile.supportingDocument.failedDownload")}
            </Text>
          )}
        </View>
      </View>
      <ButtonDowload
        hasFile={props.hasFile || selected || hasError}
        handleUploadFile={handleUploadFile}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flexDirection: "row",
    paddingHorizontal: 30,
    paddingVertical: Platform.OS === "android" ? 20 : 25,
  },
  error_icon: {
    marginTop: 3,
    marginRight: 10,
  },
  text_container: {
    marginRight: 20,
    flexDirection: "row",
    alignItems: "center",
  },
  text_main: {
    display: "flex",
    flexDirection: "column",
    width: 161,
  },
  button_download: {
    width: 93,
    height: 30,
    borderRadius: 30,
    alignItems: "center",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    fontSize: 12,
    lineHeight: 20,
    height: 30,
    marginTop: 8,
  },
  document_text: {
    color: "rgba(17, 40, 66, 1)",
    fontWeight: "500",
    fontSize: 14,
    lineHeight: 18,
  },
  document_type_text: {
    color: "rgba(17, 40, 66, 1)",
    fontWeight: "500",
    fontSize: 12,
    lineHeight: 18,
  },
  uploadError: {
    color: "#E4434F",
    fontWeight: "500",
    fontSize: 12,
    lineHeight: 18,
    marginTop: 5,
  },
});
