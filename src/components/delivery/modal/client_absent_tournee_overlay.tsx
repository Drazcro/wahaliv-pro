import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets//image_svg/X.svg";
import { styles } from "../../../styles/delivery/client_absent_tournee_overlay.style";

export function ClientAbsentTourneeOverlay(props: {
  visible: boolean;
  loaderConfirm: boolean;
  Resto?: string;
  onClose: () => void;
  onAccept: () => void;
}) {
  return (
    <Overlay isVisible={props.visible} onBackdropPress={props.onClose}>
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onClose()}
          >
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
          <View style={styles.wrapper}>
            <Text style={styles.courseRefus}>Client absent ?</Text>
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                Je confirme l’absence du client et je retournerai la commande
                dans le resto {props.Resto} à la fin de ma tournée.
              </Text>
            </View>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity style={styles.btnAcpt} onPress={props.onAccept}>
              {props.loaderConfirm ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.btnTextAcpt}>Confirmer</Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnRefus} onPress={props.onClose}>
              <Text style={styles.btnTextRefus}>Annuler</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Overlay>
  );
}
