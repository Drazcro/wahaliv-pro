import React from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets/image_svg/X.svg";
import { fontFamily } from "src/theme/typography";
import { DEVICE } from "src/constants/default_values";
import { color } from "src/theme";

const PlanningDesengageOverlay = (props: {
  visible: boolean;
  onClose: () => void;
  onAccept: () => void;
}) => {
  return (
    <Overlay
      isVisible={props.visible === true ? true : false}
      onBackdropPress={props.onClose}
    >
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <View style={styles.wrapper}>
            <Text style={styles.courseRefus}>Retrait tardif</Text>
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                Attention en vous retirant de ce créneau
              </Text>
              <Text style={styles.text}>
                vous perdrez 25% de vos bonus mensuels.
              </Text>
            </View>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity style={styles.btnAcpt} onPress={props.onAccept}>
              <Text style={styles.btnTextAcpt}>Continuer</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onClose()}
          >
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  backgroundModal: {
    width: DEVICE.width,
    height: DEVICE.height,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  modalContainer: {
    width: DEVICE.width - 42,
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  courseRefus: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 18,
    lineHeight: 21.94,
    textAlign: "center",
    marginBottom: 25,
  },
  btnAcpt: {
    backgroundColor: color.ACCENT,
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginBottom: 20,
  },
  btnTextAcpt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
  closeButton: {
    position: "absolute",
    top: 20,
    right: 20,
  },
  text: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "center",
  },
  text1: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "center",
    marginBottom: 20,
  },
  wrapper: {
    marginBottom: 50,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper1: {
    alignItems: "center",
  },
  textContainer: {
    minWidth: 265,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default PlanningDesengageOverlay;
