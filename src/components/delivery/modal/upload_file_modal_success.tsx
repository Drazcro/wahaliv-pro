import i18n from "i18n-js";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import Check from "assets/image_svg/Check.svg";
import Croix from "assets/image_svg/croix.svg";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export function UploadFileModalSuccess(props: {
  onClose: (arg0: boolean) => void;
}) {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.wrapper}>
          <TouchableOpacity
            style={{ position: "absolute", top: 15, right: 15 }}
            onPress={() => props.onClose(false)}
          >
            <Croix width={16} height={16} />
          </TouchableOpacity>
          <Check width={41} height={41} style={{ marginBottom: 13 }} />
          <Text style={styles.text_success}>
            {i18n.t("profile.supportingDocument.successDownload")}
          </Text>
          <Text style={styles.text_document}>
            {i18n.t("profile.supportingDocument.saveDocument")}
          </Text>
          <Button
            type="solid"
            title={i18n.t("profile.supportingDocument.buttonOK")}
            buttonStyle={styles.button}
            titleStyle={styles.text_button}
            onPress={() => props.onClose(false)}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  content: {
    width: 318,
    height: 229,
    backgroundColor: "white",
    borderRadius: 20,
  },
  button: {
    width: 270.81,
    height: 44,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
    marginTop: 37.54,
  },
  wrapper: {
    flex: 1,
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
  },
  text_success: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 16,
    lineHeight: 22,
    marginBottom: 6,
  },
  text_document: {
    fontFamily: fontFamily.regular,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
});
