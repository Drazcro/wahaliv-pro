import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets//image_svg/X.svg";
import { styles } from "./reject_course.styles";

export function RejectCourseOverlay(props: {
  visible: boolean;
  onClose: () => void;
  onAccept: () => void;
  onRefuse: () => void;
  loadingAccept: boolean;
  loadingRefus: boolean;
}) {
  return (
    <Overlay
      isVisible={props.visible === true ? true : false}
      overlayStyle={{
        backgroundColor: "rgba(16, 16, 16, 0.4)",
      }}
      onBackdropPress={props.onClose}
    >
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <View style={styles.wrapper}>
            <Text style={styles.courseRefus}>Course refusée</Text>
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                Le refus de la course va impacter ton
              </Text>
              <Text style={styles.text1}>bonus mensuel.</Text>
            </View>
            <Text style={styles.refus}>
              Confirmes-tu le refus de la course ?
            </Text>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity style={styles.btnAcpt} onPress={props.onRefuse}>
              {props.loadingRefus ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.btnTextAcpt}>Refuser la course</Text>
              )}
            </TouchableOpacity>
          </View>
          <View style={styles.wrapper2}>
            <TouchableOpacity style={styles.btnGreen} onPress={props.onAccept}>
              {props.loadingAccept ? (
                <ActivityIndicator color="#40ADA2" />
              ) : (
                <Text style={styles.textAccept}>Accepter la course</Text>
              )}
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.closeButton} onPress={props.onClose}>
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  );
}
