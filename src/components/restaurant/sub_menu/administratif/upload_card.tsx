import React, { useEffect } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Divider } from "react-native-paper";
import { fontFamily } from "src/theme/typography";
import ErrorIcon from "assets/image_svg/DownalodErrorIcon.svg";
import Validated from "assets/image_svg/validateImg.svg";

import * as DocumentPicker from "expo-document-picker";
import { Case } from "src/interfaces/Entities";
import { upload_document } from "src/api/restaurant/profile";
import Toast from "react-native-toast-message";
import I18n from "i18n-js";
import { color } from "src/theme";

type Props = {
  case: Case;
  openOverlay: () => void;
};

const DocumentDownloadCard = (props: Props) => {
  console.log(props.case.uploaded);
  const [uploaded, setUploaded] = React.useState(props.case.uploaded);

  useEffect(() => {
    setUploaded(props.case.uploaded);
  }, []);

  const checkFileValid = (file: DocumentPicker.DocumentPickerAsset) => {
    if (!file) {
      return false;
    }

    const size = file.size ?? 0;
    const type = file.mimeType ?? "";
    const limit = 6000000;

    const hasImg = type.includes("image");
    const hasPdf = type.includes("pdf");

    const isFileTooBig = size > limit;
    const isFileValid = !hasImg && !hasPdf;

    if (isFileTooBig || isFileValid) {
      return false;
    } else {
      return true;
    }
  };

  async function uploadDocument(type: string) {
    try {
      const docRes = await DocumentPicker.getDocumentAsync({
        type: ["pdf/*", "image/*"],
      });

      const assets = docRes.assets;
      if (!assets) return;

      const file = assets[0];

      const valide = checkFileValid(file);

      if (!valide) {
        Toast.show({
          type: "error",
          text1: "Error",
          text2: I18n.t("profile.supportingDocument.failedDownload"),
        });
        return;
      }

      if (valide) {
        const formData = new FormData();
        formData.append("file", {
          uri: file.uri,
          type: file.mimeType,
          name: file.name,
          size: file.size,
        });

        await upload_document(formData, type)
          .then((res) => {
            console.log(res);
            setUploaded(true);
          })
          .catch((err) => {
            console.error(err);
          });

        Toast.show({
          type: "success",
          text1: "Success",
          text2: I18n.t("profile.notification.file.message"),
        });
      }

      console.log(file, type);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <React.Fragment>
      <View style={styles.cardContainer}>
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              gap: 5,
            }}
          >
            {uploaded ? <Validated /> : <ErrorIcon />}
            <View>
              <Text style={styles.title}>{props.case.title}</Text>
              <Text style={styles.subTitle}>{props.case.fileTypes}</Text>
              {props.case.id === 3 ? (
                <TouchableOpacity
                  style={{
                    borderBottomWidth: 1,
                  }}
                  onPress={props.openOverlay}
                >
                  <Text
                    style={{
                      marginTop: 20,
                      fontSize: 14,
                      fontFamily: fontFamily.regular,
                      lineHeight: 18,
                      fontStyle: "italic",
                      color: "#1D2A40",
                    }}
                  >
                    Commet obtenir ce document ?
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.downloadBtn}
          onPress={() => uploadDocument(props.case.docType)}
        >
          <Text style={styles.downloadBtnLabel}>
            {uploaded ? "Remplacer" : "Télécharger"}
          </Text>
        </TouchableOpacity>
      </View>
      <Divider
        style={{
          borderWidth: 0.2,
          borderColor: "#c4c4c4",
          marginTop: 5,
          marginBottom: 10,
        }}
      />
    </React.Fragment>
  );
};

export default DocumentDownloadCard;

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 10,
    marginBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 14,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    lineHeight: 18,
  },
  subTitle: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    lineHeight: 18,
  },
  downloadBtn: {
    width: 93,
    height: 27,
    backgroundColor: color.ACCENT,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  downloadBtnLabel: {
    color: "#fff",
    fontSize: 12,
    fontFamily: fontFamily.semiBold,
  },
});
