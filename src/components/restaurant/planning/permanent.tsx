import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useEffect, useState } from "react";
import { fontFamily } from "src/theme/typography";
import Edit from "assets/image_svg/EditWhite.svg";
import PlanningTable from "./table";
import ScheduleEditingOverlay from "../overlay/ScheduleEditingOverlay";
import { get_week_planning } from "src/api/restaurant/profile";
import { WeekSchedule } from "src/interfaces/restaurant/profile";
import { color } from "src/theme";

const PermanentPlanning = () => {
  const [open, setOpen] = useState(false);
  const [planning, setPlanning] = useState<WeekSchedule[]>([]);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  useEffect(() => {
    get_week_planning()
      .then((res) => {
        console.log(res);
        setPlanning(res);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <View style={styles.container}>
      <ScheduleEditingOverlay open={open} close={handleClose} />
      <View style={styles.header}>
        <Text style={styles.title}>Planning</Text>
        <TouchableOpacity style={styles.editBtn} onPress={handleOpen}>
          <Text style={styles.editBtnLabel}>Editer</Text>
          <Edit />
        </TouchableOpacity>
      </View>
      <PlanningTable planning={planning} />
    </View>
  );
};

export default PermanentPlanning;

const styles = StyleSheet.create({
  container: {
    marginTop: 24,
    marginLeft: 18,
    marginRight: 24,
  },
  title: {
    color: "#1D2A40",
    fontSize: 20,
    fontFamily: fontFamily.semiBold,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  editBtn: {
    backgroundColor: color.ACCENT,
    borderRadius: 50,
    paddingVertical: 5,
    paddingHorizontal: 11,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    gap: 5,
  },
  editBtnLabel: {
    color: "#fff",
  },
});
