import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-paper";
import { WeekSchedule } from "src/interfaces/restaurant/profile";

const PlanningTable = ({ planning }: { planning: WeekSchedule[] }) => {
  const WEEK_SCHEDULE = Object.keys(planning).map((day) => {
    return {
      day: day,
      midi: Array.isArray(planning[day].midi)
        ? planning[day].midi.join(", ")
        : planning[day].midi,
      night: Array.isArray(planning[day].soir)
        ? planning[day].soir.join(", ")
        : planning[day].soir,
    };
  });

  return (
    <View>
      <View style={styles.tableHeader}>
        <Text style={styles.headerLabel}>JOUR</Text>
        <Text style={styles.headerLabel}>MIDI</Text>
        <Text style={styles.headerLabel}>SOIR</Text>
      </View>
      <View style={styles.table}>
        {WEEK_SCHEDULE.map((schedule, index) => (
          <React.Fragment key={index}>
            <View style={styles.tableRow}>
              <Text style={styles.dayLabel}>{schedule.day}</Text>
              <Text style={styles.scheduleMidi}>{schedule.midi}</Text>
              <Text style={styles.scheduleNight}>{schedule.night}</Text>
            </View>
            <Divider
              style={{
                marginTop: 15,
                marginBottom: 20,
                display: schedule.day === "Dimanche" ? "none" : "flex",
              }}
            />
          </React.Fragment>
        ))}
      </View>
    </View>
  );
};

export default PlanningTable;

const styles = StyleSheet.create({
  tableHeader: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 25,
    paddingLeft: 10,
    marginRight: 80,
  },
  headerLabel: {
    color: "#1D2A40",
    fontSize: 12,
    fontFamily: fontFamily.Medium,
  },
  scheduleContainer: {
    backgroundColor: "#fff",
    width: "100%",
    height: "80%",
    marginTop: 8,
    borderRadius: 8,
    paddingTop: 21,
    paddingLeft: 17,
    paddingRight: 17,
  },
  table: {
    width: "100%",
    borderRadius: 10,
    overflow: "hidden",
    backgroundColor: "#fff",
    marginTop: 8,
    paddingVertical: 21,
  },
  tableRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 8,
  },
  dayLabel: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    flex: 1,
  },
  scheduleMidi: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    flex: 1,
    textAlign: "left",
  },
  scheduleNight: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    flex: 0.9,
    // marginLeft: 30,
    textAlign: "justify",
  },
});
