import I18n from "i18n-js";
import React, { useState } from "react";
import {
  RefreshControl,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";

import Search from "assets/image_svg_partner/Search.svg";
import { Input } from "@rneui/themed";
import { Entypo } from "@expo/vector-icons";

import moment from "moment";
import { INITIAL_PAGE, INTERVAL_PAGE } from "src/constants/default_values";
import { DefaultTheme, List } from "react-native-paper";
import Layout from "src/constants/layout";
import useDeviceType from "src/hooks/useDeviceType";
import { router } from "expo-router";
import Sac from "assets/image_svg_partner/sac.svg";
import {
  get_delivered_orders_for_partner,
  get_refused_orders_for_partner,
} from "src/api/restaurant/order";
import { OrderSkeletonTablette } from "src/components/restaurant/skeleton/order_tablet";
import { OrderSkeleton } from "src/components/restaurant/skeleton/order";
import { paginationStore } from "src/zustore/paginationstore";
import { OrderCard } from "src/components/restaurant/cards/order_card";
import { partnerStore } from "src/zustore/partnerstore";
import { IOrder, ORDER_STATUS } from "src/interfaces/restaurant/order";

export const NumberOfSkeleton = Array.from({ length: 1 }, (v, k) => k + 1);

DefaultTheme.colors.primary = "#1D2A40";

export default function OldCoursesTabView() {
  moment.locale("fr");

  const { setTotalPagesDelivered, setTotalPagesRefused } = paginationStore();
  const { totalPagesDelivered, totalPagesRefused } = paginationStore();

  const isTablette = useDeviceType();

  const setOrderEnCours = partnerStore((state) => state.setOrderEnCours);
  const deliveredOrders = partnerStore((state) => state.deliveredOrders);
  const cancelledOrders = partnerStore((state) => state.cancelledOrders);

  const [filter, setFilter] = useState("");
  const [refreshing, setRefreshing] = React.useState(false);

  const [cardDeliveredLoader, setCardDeliveredLoader] = useState(false);
  const [cardCancelledLoader, setCardCancelledLoader] = useState(false);

  // const [currentPageDelivered, setCurrentPageDelivered] = useState(1);
  // const [currentPageRefused, setCurrentPageRefused] = useState(1);

  const [expandedDone, setExpandedDone] = React.useState(true);
  const [expandedCancelled, setExpandedCancelled] = React.useState(true);

  const [focusSearch, setFocusSearch] = useState(false);

  const hasOrders = React.useMemo(() => {
    return totalPagesDelivered + totalPagesRefused > 0;
  }, [totalPagesDelivered, totalPagesRefused]);

  const filterDeliveredOrders = React.useMemo(() => {
    if (filter === "" || !filter) {
      return deliveredOrders;
    }
    const filtered_data_set = deliveredOrders.filter((order) => {
      const pickupDate = moment(order.commande.pickup_date.date).format("L");
      return (
        order.customer.name.toLowerCase().includes(filter.toLowerCase()) ||
        order.commande.id.toString().includes(filter.toLowerCase()) ||
        pickupDate.includes(filter.toLowerCase())
      );
    });
    return filtered_data_set;
  }, [filter, deliveredOrders]);

  const filterCancelledOrders = React.useMemo(() => {
    if (filter === "" || !filter) {
      return cancelledOrders;
    }
    const filtered_data_set = cancelledOrders.filter((order) => {
      const pickupDate = moment(order.commande.pickup_date.date).format("L");
      return (
        order.customer.name.toLowerCase().includes(filter.toLowerCase()) ||
        order.commande.id.toString().includes(filter.toLowerCase()) ||
        pickupDate.includes(filter.toLowerCase())
      );
    });
    return filtered_data_set;
  }, [filter, cancelledOrders]);

  function getDeliveredAndRefusedCourses() {
    setCardDeliveredLoader(true);
    return get_delivered_orders_for_partner(INITIAL_PAGE, INTERVAL_PAGE)
      .then((res) => {
        if (res) {
          const total = parseInt(res.total);
          setTotalPagesDelivered(total);
        }
      })
      .finally(function () {
        setCardDeliveredLoader(false);
        setCardCancelledLoader(true);
        get_refused_orders_for_partner(INITIAL_PAGE, INTERVAL_PAGE)
          .then((res) => {
            if (res) {
              const total = parseInt(res.total);
              setTotalPagesRefused(total);
            }
          })
          .finally(() => {
            setCardCancelledLoader(false);
          });
      });
  }

  function onRefresh() {
    setRefreshing(true);
    getDeliveredAndRefusedCourses().finally(() => setRefreshing(false));
  }

  function onPressCard(command: IOrder) {
    if (
      [ORDER_STATUS.DELIVERED, ORDER_STATUS.ACCEPTED].includes(
        command.commande.status,
      )
    ) {
      setOrderEnCours(command);
      router.push("/restaurant/screens/order_details_screen");
    }
  }

  React.useEffect(() => {
    getDeliveredAndRefusedCourses();
  }, []);

  return (
    <>
      {hasOrders && !refreshing ? (
        <Input
          value={filter}
          onChangeText={setFilter}
          containerStyle={styles.searchInputMain}
          inputStyle={styles.searchTextInput}
          inputContainerStyle={styles.searchInput}
          placeholder={I18n.t("partner.order.old.search.label")}
          leftIcon={<Search />}
          onFocus={() => setFocusSearch(true)}
          rightIcon={
            focusSearch ? (
              <TouchableOpacity
                style={styles.icon_right}
                onPress={() => setFilter("")}
              >
                <Entypo name="cross" size={12} color="white" />
              </TouchableOpacity>
            ) : undefined
          }
        />
      ) : null}

      <ScrollView
        style={styles.cards}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        <List.Section>
          {totalPagesDelivered > 0 && (
            <List.Accordion
              theme={DefaultTheme}
              style={{ paddingBottom: 0 }}
              title="Livrées"
              titleStyle={styles.cardCategory}
              expanded={expandedDone}
              onPress={() => setExpandedDone(!expandedDone)}
            >
              <View style={styles.spaceBetweenCategory}>
                {refreshing
                  ? NumberOfSkeleton.map((i) => <OrderSkeleton key={i} />)
                  : deliveredOrders &&
                    filterDeliveredOrders.map((item: IOrder) => (
                      <OrderCard
                        course={item}
                        key={item.commande.id}
                        onPress={() => onPressCard(item)}
                      />
                    ))}
              </View>
            </List.Accordion>
          )}

          {totalPagesRefused > 0 && (
            <List.Accordion
              style={{
                paddingBottom: 0,
                paddingTop: 0,
                backgroundColor: "rgba(245, 245, 245, 1)",
                marginTop: 20,
              }}
              title="Refusées"
              titleStyle={styles.cardCategory}
              expanded={expandedCancelled}
              onPress={() => setExpandedCancelled(!expandedCancelled)}
            >
              <View style={styles.spaceBetweenCategory}>
                {refreshing
                  ? NumberOfSkeleton.map((i) =>
                      isTablette ? (
                        <OrderSkeletonTablette key={i} />
                      ) : (
                        <OrderSkeleton key={i} />
                      ),
                    )
                  : cancelledOrders &&
                    filterCancelledOrders.map((item: IOrder) => (
                      <OrderCard
                        course={item}
                        key={item.commande.id}
                        onPress={() => onPressCard(item)}
                      />
                    ))}
              </View>
            </List.Accordion>
          )}
        </List.Section>
        {!hasOrders && (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
              width: "100%",
              minHeight: 565,
            }}
          >
            <Sac />
            <Text
              style={{
                fontFamily: fontFamily.Medium,
                fontSize: 16,
                color: "#B0B0B0",
                marginTop: 13.78,
                width: "60%",
                textAlign: "center",
              }}
            >
              Vous n'avez pas encore de commandes terminées
            </Text>
          </View>
        )}
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 35,
  },
  cards: {
    marginTop: 15,
  },
  searchInputMain: {
    marginTop: 21,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#C4C4C4",
    height: 38,
    width: Layout.window.width - 35,
    alignSelf: "center",
  },
  icon_right: {
    backgroundColor: "#ccc",
    width: 20,
    height: 20,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  searchInput: {
    borderBottomWidth: 0,
    bottom: 5,
  },
  searchTextInput: {
    fontFamily: fontFamily.regular,
    fontWeight: "500",
    fontSize: 14,
    textAlign: "left",
  },
  cardCategory: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
  spaceBetweenCategory: {
    marginBottom: 20,
  },
});
