import { StyleSheet } from "react-native";
import * as Print from "expo-print";
import moment from "moment";
import "moment/locale/fr";
import { IOrder, IProductDetails } from "src/interfaces/restaurant/order";

moment.locale("fr");

export function printFunction(course: IOrder) {
  const total = course.total_amount + " €";
  const liv = course.delivery_fees + " €";
  const date = moment(course.commande.pickup_date.date).calendar();
  const cmdType = course.commande.service_type;
  const clientName = course.customer.name;
  const cmdNumber = course.commande.id;
  const cmdDetails: any = course.product_details.map(
    (item: IProductDetails, index: number) => `${item.qte} ${item.name} <br/>`,
  );
  const cmdDetailsPrice: any = course.product_details.map(
    (item: IProductDetails, index: number) => `${item.price} € <br/>`,
  );
  function generateSubProduct(item: IProductDetails) {
    let item_string = "";
    let item_array: string[] = [];
    if (item.sub_products != undefined && item.sub_products.length > 0) {
      item_array = item.sub_products.map(
        (v) =>
          `<tr >
          <td><div style="margin-left: 10%; margin-right: 10%;"><i>${v.name}</i></div></td>
          <td style="text-align: left;"><nobr>${v.price} €</nobr></td>
        </tr>`,
      );
    } else {
      item_array = [`<tr><td></td><td></td></tr>`];
    }

    for (const str of item_array) {
      item_string += str;
    }
    return item_string;
  }
  const cmdDetailsData = course.product_details.map(
    (item: IProductDetails, index: number) =>
      `<tr>
    <td>${item.qte} ${item.name}</td>
    <td style="text-align: left;"><nobr>${item.price} €</nobr></td>
  </tr>
  ${generateSubProduct(item)}
  `,
  );

  let OrderDetails = "";
  for (const ch of cmdDetailsData.filter((item) => item != "")) {
    console.log("CH : ", ch);
    OrderDetails += ch;
  }

  let OrderPrice = "";
  for (const ch of cmdDetailsPrice) {
    OrderPrice += ch;
  }

  const html = `
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
</head>
<body>
<div style="margin-left: 52%; margin-bottom: 25px;">
<p style="font-size: 10px; background-color: grey; color: white; width: 80%; font-family: Helvetica Neue; font-weight: normal;">
Ticket préparatoire
</p>
      </div>
      <p style="font-size: 10px; text-align: center; line-height: 5px; font-family: Helvetica Neue; font-weight: normal;">
      A préparer pour
      </p>
      <p style="font-size: 14px; text-align: center; line-height: 5px; font-family: Helvetica Neue; font-weight: normal;">
      ${date}
      </p>
      <p style="font-size: 10px; margin-bottom: 25px; line-height: 5px; text-align: center; font-family: Helvetica Neue; font-weight: normal;">
        Commande N° ${cmdNumber}
      </p>
      <p style="font-size: 10px; background-color: grey; color: white; width: 35%; font-family: Helvetica Neue; font-weight: normal;">
      Type : ${cmdType}
      </p>
      <p style="font-size: 12px; font-family: Helvetica Neue; font-weight: normal;">
      Client : ${clientName}  
      </p>
      
      <table style="width: 93%; font-size: 12px;">
        ${OrderDetails}
      </table>
      
      <div style=" display: flex; justify-content: space-between; margin-right: 20px" >
      <p style="font-size: 14px; font-family: Helvetica Neue; font-weight: normal;justify-content: space-between;">Livraison:</p>
      <p>${liv}</p>
      </div>
      <p>~~~~~~~~~~~~~~~~~~~~</p>
      <div style="display: flex; justify-content: space-between; margin-right: 20px; margin-top: -20px; margin-bottom: -20px">
      <p>Total TTC:</p> 
      <p>${total}</p>
      </div>
      <p>~~~~~~~~~~~~~~~~~~~~</p>
      <p style="font-size: 20px; text-align: center; font-family: Helvetica Neue; font-weight: normal;">Sacré Armand</p>
      </body>
      <style>
      @page {
        margin-left: -5px;
        margin-right: -20px;
        margin-top: 60px;
      }
      </style>
      </html>
      `;
  const print = async () => {
    // On iOS/android prints the given html. On web prints the HTML from the current page.
    await Print.printAsync({
      html,
      // printerUrl: selectedPrinter?.url, // iOS only
    });
  };
  print();
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

{
  /* <div style="display: flex; justify-content: space-between;">
<p style="font-size: 12px; font-family: Helvetica Neue; font-weight: normal; flex:70%;">
  ${OrderDetails}
</p>
<p style="flex:30%">
${OrderPrice}
</p>
</div> */
}

{
  /* <div style=" display: inline-grid;
        grid-template-columns: repeat(2, 1fr);
        grid-auto-rows: minmax(150px, auto);
        align-items: center;
      ">
        <div>
          <p style="font-size: 12px; font-family: Helvetica Neue; font-weight: normal;">
            ${OrderDetails}
          </p>
        </div>
        <div style="margin-left: 45%; line-height: 26px;
        ">
          <p>
          ${OrderPrice}
          </p>
        </div>
      </div> */
}
