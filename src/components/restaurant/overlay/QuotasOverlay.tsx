import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { useState } from "react";
import { Overlay } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import X from "assets/image_svg/X.svg";
import { SelectList } from "react-native-dropdown-select-list";
import { AVAILABLE_HOURS, WEEK_DAYS } from "src/constants/default_values";
import { update_quotas } from "src_legacy/services/Loaders";
import { authStore } from "src/services/auth_store";
import { color } from "src/theme";

type Props = {
  open: boolean;
  close: () => void;
};

const QuotasOverlay = ({ open, close }: Props) => {
  const [selectedDay, setSelectedDay] = useState<{
    index: number;
    value: string;
  }>({ index: 0, value: "" });

  const [selectedDate, setSelectedDate] = useState<{
    start: string;
    end: string;
  }>({
    start: "",
    end: "",
  });

  const { base_user } = authStore();

  const createFormData = () => {
    const formData = new FormData();

    formData.append(
      `data[${selectedDay.value}][${selectedDay.index}][debut]`,
      `${selectedDate.start}-${selectedDate.end}`,
    );
    return formData;
  };

  function updateQuotas() {
    if (!base_user) {
      return;
    }

    const quotas = createFormData();

    update_quotas(base_user.id, quotas)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.error(err));
  }

  return (
    <Overlay isVisible={open} overlayStyle={styles.container}>
      <TouchableOpacity style={styles.closeBtn} onPress={close}>
        <X />
      </TouchableOpacity>
      <Text style={styles.title}>Créer des quotas</Text>
      <Text style={styles.caption}>
        Configurez le(s) jour(s) et horaires où vous souhaitez limiter le nombre
        de commandes
      </Text>
      <ScrollView>
        <View
          style={{
            justifyContent: "flex-start",
            alignItems: "flex-start",
            marginTop: 24,
            width: "100%",
          }}
        >
          <View style={styles.formControl}>
            <Text style={styles.inputLabel}>Sélectionnez le(s) jour(s)</Text>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-evenly",
                marginTop: 7,
              }}
            >
              {WEEK_DAYS.map((day, index) => (
                <TouchableOpacity
                  key={index}
                  style={{
                    ...styles.dayBtn,
                    borderRightWidth: index === 6 ? 0.88 : 0,
                    borderTopLeftRadius: index === 0 ? 4 : 0,
                    borderBottomLeftRadius: index === 0 ? 4 : 0,
                    borderTopRightRadius: index === 6 ? 4 : 0,
                    borderBottomRightRadius: index === 6 ? 4 : 0,
                    backgroundColor:
                      selectedDay.value === day.value ? "#FF5C85" : "white",
                  }}
                  onPress={() =>
                    setSelectedDay({ index: index, value: day.value })
                  }
                >
                  <Text
                    style={{
                      ...styles.dayLabel,
                      color:
                        selectedDay.value === day.value ? "#fff" : "#1D2A40",
                    }}
                  >
                    {day.key}
                  </Text>
                </TouchableOpacity>
              ))}
            </View>
            <View style={{ ...styles.formControl, marginTop: 17 }}>
              <Text style={styles.inputLabel}>L’heure de début</Text>
              <SelectList
                setSelected={(val: string) => {
                  console.log(val);
                  setSelectedDate((prev) => ({
                    ...prev,
                    start: val,
                  }));
                }}
                data={AVAILABLE_HOURS}
                save="value"
                defaultOption={{
                  key: -1,
                  value: "-------",
                }}
                search={false}
                boxStyles={{
                  marginTop: 4,
                  borderColor: "1px solid rgba(196, 196, 196, 1)",
                  height: 44,
                }}
                dropdownStyles={{
                  marginVertical: 20,
                  height: 130,
                }}
              />
            </View>
            <View style={{ ...styles.formControl, marginTop: 17 }}>
              <Text style={styles.inputLabel}>L’heure de fin</Text>
              <SelectList
                setSelected={(val: string) => {
                  console.log(val);
                  setSelectedDate((prev) => ({
                    ...prev,
                    end: val,
                  }));
                }}
                data={AVAILABLE_HOURS}
                save="value"
                defaultOption={{
                  key: -1,
                  value: "-------",
                }}
                search={false}
                boxStyles={{
                  marginTop: 4,
                  borderColor: "1px solid rgba(196, 196, 196, 1)",
                  height: 44,
                }}
                dropdownStyles={{
                  marginVertical: 20,
                  height: 130,
                }}
              />
            </View>
            <TouchableOpacity style={styles.submitBtn} onPress={updateQuotas}>
              <Text
                style={{
                  color: "#fff",
                  fontFamily: fontFamily.semiBold,
                  fontSize: 16,
                }}
              >
                Confirmer
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </Overlay>
  );
};

export default QuotasOverlay;

const styles = StyleSheet.create({
  container: {
    width: 350,
    height: 584,
    borderRadius: 15,
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 34,
  },
  title: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "#1D2A40",
    textAlign: "center",
  },
  closeBtn: {
    position: "absolute",
    top: 14,
    right: 15,
  },
  caption: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
    width: 295,
    marginTop: 11,
  },
  formControl: {
    width: "100%",
  },
  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
  msgInput: {
    width: "100%",
    backgroundColor: "#fff",
    fontSize: 14,
  },
  dayBtn: {
    width: 47,
    height: 40,
    borderRightWidth: 0,
    borderWidth: 0.88,
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#C4C4C4",
  },
  dayLabel: {
    fontFamily: fontFamily.Medium,
    fontSize: 12,
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 44,
    marginTop: 50,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
  },
});
