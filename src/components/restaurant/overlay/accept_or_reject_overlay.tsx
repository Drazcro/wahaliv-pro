import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles } from "./PartnerOverlay.style";
import X from "assets/image_svg/X.svg";

import { ActivityIndicator } from "react-native-paper";
import * as Device from "expo-device";
import useDeviceType from "src/hooks/useDeviceType";
import { fontFamily } from "src/theme/typography";
import { IOrder } from "src/interfaces/restaurant/order";
import {
  accept_cmd,
  cancel_or_refuse_cmd,
  reload_orders,
} from "src/api/restaurant/order";
import { printFunction } from "src/components/restaurant/printer/printer";
import popup from "src/components/toasts/popup";

export function DisplayAcceptedOrRefusedOverlay({
  onClose,
  title,
  subTitle,
  acceptedOrRefused,
  ButtonText,
  course,
  toggleOverlay,
}: {
  onClose: () => void;
  title: string;
  subTitle: string;
  acceptedOrRefused: string;
  ButtonText: string;
  course: IOrder;
  toggleOverlay: () => void;
}) {
  const isTablet = useDeviceType();

  const id = course.commande.id.toString();
  const [loading, setLoading] = useState(false);

  const acceptOrder = () => {
    setLoading(true);
    accept_cmd(id)
      .then((message) => {
        popup.success({
          text: message.message,
        });
        if (Device.brand === "SUNMI") {
          printFunction(course);
        }

        onClose();
        toggleOverlay();
        reload_orders();
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const declineOrder = () => {
    setLoading(true);
    cancel_or_refuse_cmd(id)
      .then((message) => {
        popup.success({
          text: message.message,
        });

        onClose();
        toggleOverlay();
        reload_orders();
      })
      .catch((e) => {
        console.error(e);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          onClose();
        }}
      >
        <View style={styles.xPosition}>
          <X onPress={onClose} />
        </View>
      </TouchableOpacity>
      <View style={styles.AcceptedOrRefusedOverlayBox}>
        <Text style={styles.AcceptedOrRefusedOverlayTitle}>{title}</Text>
        <Text
          style={{
            color: "rgba(29, 42, 64, 1)",
            fontFamily: fontFamily.Medium,
          }}
        >
          {subTitle}
        </Text>
        <View style={styles.buttonPositionAcceptedOrRefusedOverlay}>
          {acceptedOrRefused == "Accepted" && (
            <TouchableOpacity
              style={{ ...styles.buttonPosition, marginTop: 0 }}
              onPress={acceptOrder}
            >
              <View style={styles.button}>
                {loading ? (
                  <ActivityIndicator color="#fff" />
                ) : (
                  <Text
                    style={{
                      ...styles.agreeText,
                      fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                    }}
                  >
                    {ButtonText}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          )}
          {acceptedOrRefused == "Refused" && (
            <View>
              <TouchableOpacity
                onPress={declineOrder}
                style={{ ...styles.buttonPosition, marginTop: 0 }}
              >
                <View style={styles.refusedButton}>
                  {loading ? (
                    <ActivityIndicator color="#fff" />
                  ) : (
                    <Text
                      style={{
                        ...styles.agreeText,
                        fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                      }}
                    >
                      {ButtonText}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={acceptOrder}>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {loading ? (
                    <ActivityIndicator color="#fff" />
                  ) : (
                    <Text
                      style={{
                        fontFamily: fontFamily.semiBold,
                        color: "rgba(64, 173, 162, 1)",
                        fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                      }}
                    >
                      Accepter la commande
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </View>
  );
}
