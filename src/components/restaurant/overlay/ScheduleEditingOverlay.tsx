import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import { Overlay } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import X from "assets/image_svg/X.svg";
import { AVAILABLE_HOURS, WEEK_DAYS } from "src/constants/default_values";
import { SelectList } from "react-native-dropdown-select-list";
import { ScrollView } from "react-native";
import Add from "assets/image_svg/add.svg";
import { update_planning } from "src_legacy/services/Loaders";
import { authStore } from "src/services/auth_store";
import { color } from "src/theme";

type Props = {
  open: boolean;
  close: () => void;
};

const ScheduleEditingOverlay = ({ open, close }: Props) => {
  const [selectedDay, setSelectedDay] = useState<string>("");
  const [isStartDate, setIsStartDate] = useState(false);
  const [selectedTime, setSelectedTime] = useState<string>("");

  const { base_user } = authStore();

  function createFormData(day: string, intervale: string) {
    const startDate = isStartDate ? "debut" : "fin";
    const dayIndex = WEEK_DAYS.findIndex((day) => day.value === selectedDay);
    const data = new FormData();

    data.append(
      `planning_to_add[${day}][${dayIndex}][${startDate}]`,
      intervale,
    );

    console.log(data);

    return data;
  }

  function updateSchedule() {
    if (!base_user) {
      return;
    }

    const bodyData = createFormData(selectedDay, selectedTime);

    update_planning(base_user?.id, bodyData)
      .then((res) => {
        console.log(res);
        close();
      })
      .catch((err) => {
        console.error(err);
      });
  }

  return (
    <Overlay isVisible={open} overlayStyle={styles.container}>
      <TouchableOpacity style={styles.closeBtn} onPress={close}>
        <X />
      </TouchableOpacity>
      <Text style={styles.title}>Editer l’horaire </Text>
      <ScrollView>
        <View style={styles.daysContainer}>
          {WEEK_DAYS.map((day, index) => (
            <TouchableOpacity
              key={index}
              style={{
                ...styles.dayBtn,
                backgroundColor:
                  selectedDay == day.value ? "#FF5C85" : "transparent",
              }}
              onPress={() => setSelectedDay(day.value)}
            >
              <Text
                style={{
                  ...styles.dayBtnValue,
                  color: selectedDay === day.value ? "#fff" : "#1D2A40",
                }}
              >
                {day.key}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "flex-start",
            alignItems: "center",
            width: "100%",
            gap: 9.7,
            marginTop: 15,
          }}
        >
          <View style={{ marginTop: 17, width: "48%" }}>
            <Text style={styles.inputLabel}>Ouverture</Text>
            <SelectList
              setSelected={(val: string) => {
                setIsStartDate(true);
                setSelectedTime(val);
              }}
              data={AVAILABLE_HOURS}
              save="value"
              defaultOption={{
                key: -1,
                value: "-------",
              }}
              search={false}
              boxStyles={{
                marginTop: 4,
                borderColor: "1px solid rgba(196, 196, 196, 1)",
                // height: 44,
              }}
              dropdownStyles={{
                marginVertical: 20,
                height: 130,
              }}
            />
          </View>
          <View style={{ marginTop: 17, width: "48%" }}>
            <Text style={styles.inputLabel}>Fermeture</Text>
            <SelectList
              setSelected={(val: string) => {
                setIsStartDate(false);
                setSelectedTime(val);
              }}
              data={AVAILABLE_HOURS}
              save="value"
              defaultOption={{
                key: -1,
                value: "-------",
              }}
              search={false}
              boxStyles={{
                marginTop: 4,
                borderColor: "1px solid rgba(196, 196, 196, 1)",
                // height: 44,
              }}
              dropdownStyles={{
                marginVertical: 20,
                height: 130,
              }}
            />
          </View>
        </View>
        <TouchableOpacity style={styles.addServiceBtn}>
          <Add />
          <Text style={styles.addServiceBtnLabel}>Ajouter un service</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.submitBtn} onPress={updateSchedule}>
          <Text
            style={{
              color: "#fff",
              fontFamily: fontFamily.semiBold,
              fontSize: 16,
            }}
          >
            Enregistrer
          </Text>
        </TouchableOpacity>
      </ScrollView>
    </Overlay>
  );
};

export default ScheduleEditingOverlay;

const styles = StyleSheet.create({
  container: {
    width: 350,
    height: 395,
    borderRadius: 15,
  },
  title: {
    textAlign: "center",
    marginTop: 28,
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
  },
  closeBtn: {
    position: "absolute",
    top: 14,
    right: 15,
  },
  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
  daysContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    marginTop: 31,
    borderWidth: 1,
    borderColor: "#C4C4C4",
    width: 327,
    height: 41,
    borderRadius: 25,
    paddingHorizontal: 11,
  },
  dayBtn: {
    // backgroundColor: "#FF5C85",
    borderRadius: 25,
    width: 56,
    height: 41,
    justifyContent: "center",
    alignItems: "center",
  },
  dayBtnValue: {
    color: "#1D2A40",
    fontFamily: fontFamily.regular,
    fontSize: 11,
  },
  addServiceBtn: {
    marginTop: 31,
    borderWidth: 1,
    borderColor: "#40ADA2",
    width: 164,
    height: 31,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    borderRadius: 15,
    gap: 5.5,
  },
  addServiceBtnLabel: {
    fontSize: 11,
    fontFamily: fontFamily.semiBold,
    color: "#40ADA2",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 44,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
  },
});
