import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import X from "assets/image_svg/X.svg";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export function PartnerCancelOrderOverlay(props: {
  visible: boolean;
  loading: boolean;
  onClose: () => void;
  onCancel: () => void;
}) {
  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={styles.main}
      onBackdropPress={props.onClose}
    >
      <TouchableOpacity onPress={props.onClose}>
        <View style={styles.closeBtn}>
          <X onPress={props.onClose} />
        </View>
      </TouchableOpacity>

      <View style={styles.header}>
        <Text style={styles.headerText}>
          Confirmez-vous l’annulation de la commande ?
        </Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.text}>Si vous confirmez, votre commande</Text>
        <Text style={styles.text}>sera définitivement effacée</Text>
      </View>

      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={props.onCancel}>
          {props.loading ? (
            <ActivityIndicator color={"#fff"} />
          ) : (
            <Text style={styles.btnText}>Confirmer</Text>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{ ...styles.button, backgroundColor: "#fff" }}
          onPress={props.onClose}
        >
          <Text style={{ ...styles.btnText, color: "#1D2A40" }}>Annuler</Text>
        </TouchableOpacity>
      </View>
    </Overlay>
  );
}

export const styles = StyleSheet.create({
  main: {
    height: "45%",
    width: "90%",
    backgroundColor: "#fff",
    borderRadius: 20,
  },
  closeBtn: {
    alignItems: "flex-end",
    marginTop: 18,
    marginRight: 27.5,
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 19.5,
  },
  headerText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    textAlign: "center",
    color: "rgba(29, 42, 64, 1)",
  },
  text: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    color: "rgba(29, 42, 64, 1)",
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: color.ACCENT,
    borderRadius: 30,
    width: 308,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 24,
  },
  btnText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
});
