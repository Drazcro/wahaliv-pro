import { Platform } from "expo-modules-core";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { Text } from "react-native-elements";
import Close from "assets/image_svg_partner/X.svg";
import { fontFamily } from "src/theme/typography";
import I18n from "i18n-js";
import { Input } from "@rneui/themed";
import React, { useState } from "react";
import { Button } from "react-native-elements";
import { PartnerOrderTradeDiscount } from "src_legacy/services/Loaders";
import Toast from "react-native-expo-popup-notifications";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { color } from "src/theme";

export const PartnerOrderTradeDiscountOverlay = (props: {
  visible: boolean;
  onClose: () => void;
}) => {
  const { orderEnCours } = partnerStore();
  const commande_id = orderEnCours.commande.id;

  const [amount, setAmount] = useState("");
  const [msg, setMsg] = useState("");
  const [btnLoading, setBetnLoading] = useState(false);
  const isTablet = useDeviceType();

  const onValidate = () => {
    setBetnLoading(true);
    PartnerOrderTradeDiscount(commande_id, {
      amount: Number(amount.replace(",", ".")),
      comment: msg,
    })
      .then((res) => {
        if (res && res.status === 200) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: I18n.t("profile.notification.success.message"),
          });
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: I18n.t("profile.notification.error.message4"),
          });
        }
      })
      .catch()
      .finally(() => {
        setBetnLoading(false);
        props.onClose();
      });
  };

  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={
        isTablet && Device.brand != "SUNMI" ? styles.mainTablet : styles.main
      }
      onBackdropPress={props.onClose}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_closeBtn
                : styles.closeBtn
            }
          >
            <TouchableOpacity onPress={props.onClose}>
              <Close onPress={props.onClose} />
            </TouchableOpacity>
          </View>
          <View style={styles.header}>
            <Text
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_title
                  : styles.title
              }
            >
              {" "}
              {I18n.t("partner.order.tradeDiscount.header.title")}
            </Text>
            <Text
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_subTitle
                  : styles.subTitle
              }
            >
              {" "}
              {I18n.t("partner.order.tradeDiscount.header.subtitle1")}{" "}
            </Text>
            <Text
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_subTitle
                  : styles.subTitle
              }
            >
              {" "}
              {I18n.t("partner.order.tradeDiscount.header.subtitle2")}{" "}
            </Text>
          </View>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.amountContentTablet
                : styles.amountContent
            }
          >
            <Text style={styles.amountTitle}>
              {I18n.t("partner.order.tradeDiscount.amount.text")}
            </Text>
            <Input
              keyboardType="decimal-pad"
              value={amount}
              onChangeText={(text) => setAmount(text)}
              containerStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.amountInputTablet
                  : styles.amountInput
              }
              inputStyle={[styles.price]}
              inputContainerStyle={[
                styles.input,
                {
                  width: 120,
                  alignSelf: "center",
                },
              ]}
              placeholder={"0,00"}
              placeholderTextColor={"#C4C4C4"}
              rightIcon={
                <Text
                  style={{
                    ...styles.price,
                    color: !amount ? "#C4C4C4" : "#696565",
                    alignSelf: "flex-start",
                  }}
                >
                  €
                </Text>
              }
            />
          </View>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.amountContentTablet
                : styles.amountContent
            }
          >
            <Text style={styles.amountTitle}>
              {I18n.t("partner.order.tradeDiscount.amount.comment")}
            </Text>
            <Input
              value={msg}
              onChangeText={(text) => setMsg(text)}
              containerStyle={{
                ...(isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_amountInput
                  : styles.amountInput),
                height: 107,
              }}
              inputStyle={styles.msg}
              inputContainerStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_input
                  : styles.input
              }
              placeholder={I18n.t("partner.order.tradeDiscount.amount.msg")}
              multiline
              numberOfLines={4}
            />
          </View>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_btnContent
                : styles.btnContent
            }
          >
            {btnLoading ? (
              <View style={styles.button}>
                <ActivityIndicator color="#fff" />
              </View>
            ) : (
              <Button
                onPress={onValidate}
                title={I18n.t("partner.order.tradeDiscount.btn.text")}
                buttonStyle={styles.button}
                titleStyle={
                  isTablet && Device.brand != "SUNMI"
                    ? styles.tablet_text_button
                    : styles.text_button
                }
                disabled={!amount}
                disabledStyle={{
                  backgroundColor: "#C4C4C4",
                }}
                disabledTitleStyle={{
                  color: "#FDFDFD",
                }}
              />
            )}
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  main: {
    width: "95%",
    backgroundColor: "#fff",
    position: "relative",
    borderRadius: 8,
    alignItems: "center",
  },
  closeBtn: {
    position: "absolute",
    top: 20,
    right: 40,
  },
  header: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: Platform.OS === "ios" ? 58 : 58,
  },
  title: {
    marginBottom: 21,
    color: "#112842",
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    lineHeight: 22,
  },
  subTitle: {
    marginHorizontal: 33,
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 17.07,
  },
  amountContent: {
    marginTop: 38,
    marginHorizontal: 31,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  amountContentTablet: {
    marginTop: 38,
    marginHorizontal: 100,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  amountTitle: {
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 20,
    marginBottom: 13,
  },
  amountInput: {
    backgroundColor: "rgba(196, 196, 196, 0.12)",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    width: 314,
    height: 73,
  },
  amountInputTablet: {
    backgroundColor: "rgba(196, 196, 196, 0.12)",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    width: 314,
    height: 73,
    fontSize: 33,
    borderRadius: 5,
  },
  input: {
    borderBottomWidth: 0,
    top: 10,
  },
  price: {
    fontFamily: fontFamily.semiBold,
    fontSize: 33,
    textAlign: "center",
    color: "#696565",
  },
  msg: {
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 14,
    lineHeight: 22,
  },
  btnContent: {
    marginTop: 20,
    marginBottom: 20,
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  /** tablet screen */
  mainTablet: {
    height: 656,
    width: 518,
    backgroundColor: "#fff",
    position: "relative",
    borderRadius: 8,
    alignItems: "center",
  },
  tablet_closeBtn: {
    position: "absolute",
    top: 26.25,
    right: 28.25,
  },
  tablet_title: {
    marginBottom: 13,
    color: "#112842",
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    lineHeight: 22,
  },
  tablet_subTitle: {
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 19.5,
    color: "#1D2A40",
  },
  tablet_amountInput: {
    backgroundColor: "rgba(196, 196, 196, 0.12)",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    fontSize: 33,
    borderRadius: 5,
    width: 314,
  },
  tablet_input: {
    borderBottomWidth: 0,
    margin: 10,
  },
  tablet_text_button: {
    fontFamily: fontFamily.semiBold,
    color: "#FDFDFD",
    fontSize: 18,
    lineHeight: 22,
  },
  tablet_btnContent: {
    position: "absolute",
    bottom: 80,
    alignSelf: "center",
    alignItems: "center",
  },
});
