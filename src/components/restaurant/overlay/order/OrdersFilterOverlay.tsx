import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Overlay, Text } from "react-native-elements";
import Close from "assets/image_svg_partner/X.svg";
import CalendarSVG from "assets/image_svg/Calendar.svg";
import I18n from "i18n-js";
import { fontFamily } from "src/theme/typography";
import moment from "moment";
import { PartnerOrdersSelectSearchFilter } from "../../OrdersSelectSearchFilter";
import { CalendatFilter } from "../../CalendarFilter";
import { color } from "src/theme";

export const PartnerOrdersFilterOverlay = (props: {
  visible: boolean;
  toggleOverlay: () => void;
}) => {
  const [periode, setPeriode] = useState("1");
  const [startDate, setStartDate] = useState<Date | undefined>(new Date());
  const [endDate, setEndDate] = useState<Date | undefined>(new Date());

  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={styles.main}
      onBackdropPress={props.toggleOverlay}
    >
      <Text style={styles.title}>
        {" "}
        {I18n.t("partner.order.old.filter.title")}
      </Text>
      <TouchableOpacity style={styles.closeBtn} onPress={props.toggleOverlay}>
        <Close onPress={props.toggleOverlay} />
      </TouchableOpacity>
      <View style={styles.selectContainer}>
        <Text style={styles.text_label}>
          {I18n.t("partner.order.old.filter.periode")}
        </Text>
        <PartnerOrdersSelectSearchFilter
          periode={periode}
          setPeriode={setPeriode}
        />
      </View>
      <View style={styles.dateContainer}>
        <CalendarSVG style={styles.calendar} />
        <View style={styles.dateInpuContainer}>
          <Text style={styles.dateInput}>
            {moment(startDate).format("DD-MM-YYYY")}
          </Text>
        </View>
        <Text style={styles.separator}>-</Text>
        <View style={styles.dateInpuContainer}>
          <Text style={styles.dateInput}>
            {moment(endDate).format("DD-MM-YYYY")}
          </Text>
        </View>
      </View>
      <View style={styles.calendarContainer}>
        <CalendatFilter
          startDate={startDate}
          setStartDate={setStartDate}
          endDate={endDate}
          setEndDate={setEndDate}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          title={I18n.t("partner.order.old.filter.btn.cancel")}
          buttonStyle={styles.button}
          titleStyle={styles.text_button}
          onPress={props.toggleOverlay}
        />
        <View style={styles.btnSeparator} />
        <Button
          title={I18n.t("partner.order.old.filter.btn.save")}
          buttonStyle={{ ...styles.button, backgroundColor: color.ACCENT }}
          titleStyle={{ ...styles.text_button, color: "#fff" }}
        />
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fff",
    position: "relative",
    width: "95%",
    height: "90%",
    borderRadius: 20,
  },
  closeBtn: {
    position: "absolute",
    top: 30.38,
    right: 25.38,
  },
  title: {
    position: "relative",
    top: 25,
    marginBottom: 21,
    color: "#112842",
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    lineHeight: 22,
    textAlign: "center",
  },
  selectContainer: {
    marginVertical: 30,
    marginLeft: 31,
    marginRight: 36,
  },
  text_label: {
    color: "#000000",
    fontWeight: "600",
    fontSize: 14,
    lineHeight: 17.07,
    marginBottom: 8,
  },
  calendar: {
    marginRight: 10,
  },
  dateContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginBottom: 25,
    marginLeft: 31,
    marginRight: 36,
  },
  dateInpuContainer: {
    width: "40%",
    height: 36,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    borderColor: "#C4C4C4",
    borderWidth: 1,
  },
  dateInput: {
    color: "#000000",
    fontWeight: "500",
    fontSize: 14,
    lineHeight: 17.07,
  },
  separator: {
    marginHorizontal: 5,
  },
  buttonContainer: {
    position: "absolute",
    bottom: 32,
    width: 100,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 133,
    height: 35,
    backgroundColor: "#fff",
    borderRadius: 30,
    borderWidth: 1,
    borderColor: color.ACCENT,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 16,
    lineHeight: 17.072,
  },
  btnSeparator: {
    marginHorizontal: 9,
  },
  calendarContainer: {
    position: "absolute",
    alignSelf: "center",
    bottom: 100,
    height: "50%",
    width: "82%",
    flex: 1,
  },
});
