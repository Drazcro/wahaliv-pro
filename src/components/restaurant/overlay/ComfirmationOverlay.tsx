import { StyleSheet, Text, TouchableOpacity } from "react-native";
import React from "react";
import { Overlay } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import X from "assets/image_svg/X.svg";
import { color } from "src/theme";

type Props = {
  open: boolean;
  close: () => void;
};

const ComfirmationOverlay = ({ open, close }: Props) => {
  return (
    <Overlay isVisible={open} overlayStyle={styles.Container}>
      <TouchableOpacity style={styles.closeBtn} onPress={close}>
        <X />
      </TouchableOpacity>
      <Text style={styles.title}>Supprimer le utilisateur</Text>
      <Text style={styles.text}>
        Confirmez-vous la suppression de cet utilisateur de la liste ?
      </Text>
      <TouchableOpacity style={styles.submitBtn}>
        <Text
          style={{
            color: "#fff",
            fontFamily: fontFamily.semiBold,
            fontSize: 16,
          }}
        >
          Confirmer
        </Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.cancelBtn}>
        <Text
          style={{
            color: color.ACCENT,
            fontFamily: fontFamily.semiBold,
            fontSize: 16,
          }}
        >
          Annuler
        </Text>
      </TouchableOpacity>
    </Overlay>
  );
};

export default ComfirmationOverlay;

const styles = StyleSheet.create({
  Container: {
    width: 343,
    height: 286,
    borderRadius: 10,
    justifyContent: "flex-start",
    alignItems: "center",
    paddingTop: 39,
  },
  title: {
    color: "#1D2A40",
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
  },
  text: {
    color: "#1D2A40",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    textAlign: "center",
    marginTop: 12,
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: 289,
    height: 45,
    marginTop: 32,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
  },
  cancelBtn: {
    borderWidth: 1,
    borderColor: color.ACCENT,
    width: 289,
    height: 45,
    marginTop: 19.95,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
  },
  closeBtn: {
    position: "absolute",
    top: 14,
    right: 15,
  },
});
