import React, { useState } from "react";
import {
  View,
  Text,
  ScrollView,
  StyleProp,
  ViewStyle,
  TouchableOpacity,
} from "react-native";
import { styles } from "./PartnerOverlay.style";
import X from "assets/image_svg/X.svg";
import { Overlay } from "react-native-elements";
import { DisplayAcceptedOrRefusedOverlay } from "./accept_or_reject_overlay";
import { TimeOverlay } from "./time_overlay";
import { ORDER_STATUS } from "src/interfaces/Entities";
import moment from "moment";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import HeaderLogoColor, {
  CollecteInformation,
} from "./PartnerCourseComponents";
import {
  NewCourseHeader,
  ClientInformation,
  DeliveryManInformation,
  OrderDetails,
  ClientComment,
} from "./PartnerCourseComponents";
import * as Device from "expo-device";
import useDeviceType from "src/hooks/useDeviceType";
import CaretLeft from "assets/image_svg/CaretLeft.svg";
import { STATUS } from "src/constants/status";
import { IOrder } from "src/interfaces/restaurant/order";
import { SliderButton } from "src/components/common/slider_button";

moment.locale("fr");

export function PartnerNewCourseOverlay({
  course,
  toggleOverlay,
  visible,
  style,
}: {
  course: IOrder;
  toggleOverlay: () => void;
  visible: boolean;
  style: StyleProp<ViewStyle>;
}) {
  const { getNewOrderById } = partnerStore();
  const [TimeOverlayVisible, setTimeOverlayVisible] = useState(false);
  const [DetailsCourseOverlayVisible, setDetailsCourseOverlay] =
    useState(false);
  const [acceptOverlayVisible, setAcceptOverlayVisible] = useState(false);
  const [refuseOverlayVisible, setRefuseOverlayVisible] = useState(false);
  const newDate = moment(
    getNewOrderById(course.commande.id)?.commande?.pickup_date?.date,
  );
  console.log("Device Name : ", Device.brand);
  const isTablet = useDeviceType();
  return (
    <Overlay
      // overlayStyle={style}
      fullScreen
      isVisible={visible}
      onBackdropPress={toggleOverlay}
    >
      <View>
        {isTablet && Device.brand != "SUNMI" ? null : (
          <TouchableOpacity onPress={toggleOverlay}>
            <View style={styles.xPosition}>
              <X onPress={toggleOverlay} />
            </View>
          </TouchableOpacity>
        )}

        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.headerTablet
                : styles.header
            }
          >
            {isTablet && Device.brand != "SUNMI" ? (
              <TouchableOpacity
                onPress={toggleOverlay}
                style={{ justifyContent: "center", marginRight: 75 }}
              >
                <CaretLeft onPress={toggleOverlay} />
              </TouchableOpacity>
            ) : null}
            <HeaderLogoColor course={course.commande} />
            <NewCourseHeader course={course.commande} />
          </View>

          {/* <View style={styles.horizontalLine} /> */}

          <View
            style={{
              //backgroundColor: "red",
              paddingTop: isTablet && Device.brand != "SUNMI" ? 60 : undefined,
            }}
          >
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 100 : undefined,
              }}
            >
              <ClientInformation name={course.customer.name} />
            </View>
            <View style={styles.horizontalLine} />
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 100 : undefined,
              }}
            >
              <CollecteInformation
                course={course}
                newDate={newDate}
                openOverlay={() => setTimeOverlayVisible(true)}
              />
            </View>

            <View style={styles.horizontalLine} />
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 100 : undefined,
              }}
            >
              {course.commande.service_type ===
              STATUS.PARTNER_SERVICE_TYPE_DELIVERY ? (
                <DeliveryManInformation name={course?.commande?.deliveryman} />
              ) : null}
            </View>
            {course.commande.service_type ===
            STATUS.PARTNER_SERVICE_TYPE_DELIVERY ? (
              <View style={styles.horizontalLine} />
            ) : null}
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 100 : undefined,
              }}
            >
              <OrderDetails course={course} />
            </View>

            <View style={styles.horizontalLine} />
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 100 : undefined,
              }}
            >
              <ClientComment comment={course?.commande?.comment} />
            </View>
            {/* <ClientComment comment={course?.commande?.comment} /> */}
            <View style={styles.buttonPosition}>
              {course.commande.status === ORDER_STATUS.NEW && (
                <TouchableOpacity onPress={() => setAcceptOverlayVisible(true)}>
                  <View
                    style={
                      !isTablet || Device.brand == "SUNMI"
                        ? styles.button
                        : styles.buttonTablet
                    }
                  >
                    <Text
                      style={{
                        ...styles.agreeText,
                        fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                      }}
                    >
                      Accepter
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              {course.commande.status === ORDER_STATUS.ACCEPTED && (
                <View style={styles.marginSliderButton}>
                  <RecoverButton id={course.commande.id} />
                </View>
              )}
            </View>
            <View style={styles.refuseButtonPosition}>
              {course.commande.status === ORDER_STATUS.NEW && (
                <TouchableOpacity
                  onPress={() => {
                    setRefuseOverlayVisible(true);
                  }}
                >
                  <Text
                    style={{
                      ...styles.refuseText,
                      fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                    }}
                  >
                    Refuser la commande
                  </Text>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </ScrollView>
        <TimeOverlay
          style={styles.TimeOverlaySize}
          onClose={() => setTimeOverlayVisible(false)}
          visible={TimeOverlayVisible}
          id={course?.commande?.id}
          toggleOverlay={toggleOverlay}
          orderType={course.commande.service_type}
        />
        {/* <DetailsCourseOverlay
          onClose={() => setDetailsCourseOverlay(false)}
          visible={DetailsCourseOverlayVisible}
        /> */}
        <AcceptOrRefuseContainer
          visible={acceptOverlayVisible}
          onClose={() => setAcceptOverlayVisible(false)}
          style={styles.AcceptedOrRefusedOverlayBox}
          params={{
            title: "Confirmation de la commande",
            subTitle: "Voulez-vous accepter la commande ?",
            ButtonText: "Accepter la commande",
            acceptedOrRefused: "Accepted",
          }}
          course={course}
          toggleOverlay={toggleOverlay}
        />

        <AcceptOrRefuseContainer
          visible={refuseOverlayVisible}
          onClose={() => setRefuseOverlayVisible(false)}
          style={styles.AcceptedOrRefusedOverlayBox}
          params={{
            title: "Commande refusée",
            subTitle: "Voulez-vous refuser la commande ?",
            ButtonText: "Refuser la commande",
            acceptedOrRefused: "Refused",
          }}
          course={course}
          toggleOverlay={toggleOverlay}
        />
      </View>
    </Overlay>
  );
}

function RecoverButton() {
  return (
    <SliderButton
      title="Commande récupérée"
      subTitle="Fais glisser le bouton dès que le livreur a récupéré la commande."
      railBgColor="rgba(64, 173, 162, 0.12)"
      railStylesBgColor="#40ADA2"
      onSuccess={() => null}
      subTitleColor="#000"
    />
  );
}
const AcceptOrRefuseContainer = (props: {
  visible: boolean;
  onClose: () => void;
  style: ViewStyle;
  params: any;
  course: IOrder;
  toggleOverlay: () => void;
}) => {
  return (
    <Overlay
      isVisible={props.visible}
      onBackdropPress={props.onClose}
      overlayStyle={props.style}
    >
      <DisplayAcceptedOrRefusedOverlay
        onClose={props.onClose}
        {...props.params}
        course={props.course}
        toggleOverlay={props.toggleOverlay}
      />
    </Overlay>
  );
};
