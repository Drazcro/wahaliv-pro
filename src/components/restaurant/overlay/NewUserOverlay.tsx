import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import { Overlay } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import X from "assets/image_svg/X.svg";
import { TextInput, useTheme } from "react-native-paper";
import FRFlag from "assets/image_svg/Flag.svg";
import { NewCollaborator } from "src/interfaces/Entities";
import { add_new_collaborator } from "src_legacy/services/Loaders";
import { authStore } from "src/services/auth_store";
import { color } from "src/theme";

type Props = {
  open: boolean;
  close: () => void;
};

const NewUserOverlay = ({ open, close }: Props) => {
  const theme = useTheme();

  const [newUserData, setNewUserData] = useState({} as NewCollaborator);
  const { base_user } = authStore();

  const handleInputChange = (name: string, value: string) => {
    setNewUserData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const addNewCollaborator = () => {
    if (!base_user) {
      return;
    }

    add_new_collaborator(base_user?.id, newUserData)
      .then((res) => {
        console.log(res);
        close();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <Overlay overlayStyle={styles.OverlayContainer} isVisible={open}>
      <TouchableOpacity style={styles.closeBtn} onPress={close}>
        <X />
      </TouchableOpacity>
      <View style={{ marginTop: 34, marginHorizontal: 12, width: "100%" }}>
        <Text style={styles.OverlayTitle}>Ajouter un collaborateur</Text>
        <View
          style={{
            justifyContent: "flex-start",
            alignItems: "flex-start",
            gap: 16,
            marginTop: 37,
          }}
        >
          <View style={styles.formControl}>
            <Text style={styles.inputLabel}>Nom</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text: string) =>
                handleInputChange("first_name", text)
              }
              value={newUserData.first_name}
              maxLength={200}
              placeholder="Nom du commerce"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View style={styles.formControl}>
            <Text style={styles.inputLabel}>Prénom</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text: string) =>
                handleInputChange("last_name", text)
              }
              value={newUserData.last_name}
              maxLength={200}
              placeholder="Prénom du commerce"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View style={styles.formControl}>
            <Text style={styles.inputLabel}>Email</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text: string) => handleInputChange("email", text)}
              value={newUserData.email}
              maxLength={200}
              placeholder="Email du commerce"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View style={styles.formControl}>
            <Text style={styles.inputLabel}>Téléphone</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text: string) =>
                handleInputChange("telephone", text)
              }
              value={newUserData.telephone}
              maxLength={200}
              placeholder="+33  00 00 00 00"
              left={<TextInput.Icon icon={() => <FRFlag />} />}
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
        </View>

        <TouchableOpacity style={styles.submitBtn} onPress={addNewCollaborator}>
          <Text
            style={{
              color: "#fff",
              fontFamily: fontFamily.semiBold,
              fontSize: 16,
            }}
          >
            Enregistrer les modifications
          </Text>
        </TouchableOpacity>
      </View>
    </Overlay>
  );
};

export default NewUserOverlay;

const styles = StyleSheet.create({
  OverlayContainer: {
    width: 340,
    height: 580,
    borderRadius: 15,
    // paddingHorizontal: 12,
    justifyContent: "flex-start",
    alignItems: "center",
    position: "relative",
  },
  OverlayTitle: {
    color: "#1D2A40",
    fontSize: 16,
    textAlign: "center",
    fontFamily: fontFamily.semiBold,
  },
  closeBtn: {
    position: "absolute",
    top: 14,
    right: 15,
  },
  formControl: {
    width: "100%",
  },
  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
  msgInput: {
    width: "100%",
    backgroundColor: "#fff",
    fontSize: 14,
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 52,
    marginTop: 32,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 30,
  },
});
