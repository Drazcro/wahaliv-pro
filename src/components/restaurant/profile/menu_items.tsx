import React from "react";
import i18n from "i18n-js";

import CaretRight from "assets/image_svg/CaretRight.svg";
import Lock from "assets/image_svg/Lock.svg";
import Receipt from "assets/image_svg/Receipt.svg";
import Calendar from "assets/image_svg/Calendar.svg";
import Handshake from "assets/image_svg_partner/Handshake.svg";
import SignOut from "assets/image_svg/SignOut.svg";
import Coffee from "assets/image_svg/coffee.svg";
import User from "assets/image_svg/User.svg";

import Bug from "assets/image_svg/Bug.svg";
import Chat from "assets/image_svg/ChatIcon.svg";
import FAQ from "assets/image_svg/FAQ.svg";
import { Text, View } from "react-native";
import { fontFamily } from "src/theme/typography";
import useChat from "src/hooks/useChat";
import { router } from "expo-router";
import { PartnerProfileMenu } from "src/components/restaurant/profile/menu";
import { logout } from "src/services";

export const PartnerProfileNavigationItems = () => {
  function ChatIcon() {
    const { unreadCount } = useChat();

    return (
      <View style={{ position: "relative" }}>
        <Chat />
        {unreadCount > 0 ? (
          <View
            style={{
              position: "absolute",
              right: 0,
              top: 0,
              backgroundColor: "rgba(255, 92, 133, 1)",
              width: 14,
              height: 14,
              borderRadius: 20,
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#fff",
                fontSize: 10,
                fontFamily: fontFamily.Medium,
              }}
            >
              {unreadCount}
            </Text>
          </View>
        ) : null}
      </View>
    );
  }

  const options = [
    {
      iconLeft: <User />,
      title: i18n.t("profile.userInfo"),
      onPress: () => {
        router.push("/restaurant/screens/partenaire_information");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <FAQ />,
      title: i18n.t("profile.Help"),
      onPress: () => {
        router.push("/restaurant/screens/faq_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <ChatIcon />,
      title: "Chat service client",
      onPress: () => {
        router.push("/restaurant/screens/chat_screen/");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <Bug />,
      title: i18n.t("partner.profile.navigation.bug"),
      iconRight: <CaretRight />,
      onPress: () => {
        router.push("/restaurant/screens/bug_screen");
      },
    },
    {
      iconLeft: <Coffee />,
      title: i18n.t("partner.profile.navigation.produits"),
      onPress: () => {
        router.push("/restaurant/screens/partner_my_product/");
      },
      iconRight: <CaretRight />,
    },

    {
      iconLeft: <Lock />,
      title: i18n.t("partner.profile.navigation.mdp"),
      onPress: () => {
        router.push(
          "/restaurant/screens/partner_profile_update_password_screen/",
        );
      },
      iconRight: <CaretRight />,
    },
    // {
    //   iconLeft: <Calendar />,
    //   title: i18n.t("partner.profile.navigation.planning"),
    //   iconRight: <CaretRight />,
    //   onPress: () => {
    //     router.push("/restaurant/screens/partenaire_planning_screen");
    //   },
    // },
    {
      iconLeft: <Receipt />,
      title: i18n.t("partner.profile.navigation.facture"),
      iconRight: <CaretRight />,
      onPress: () => {
        router.push("/restaurant/screens/partenaire_invoice_screen/");
      },
    },
    // {
    //   iconLeft: <Handshake />,
    //   title: i18n.t("partner.profile.navigation.client"),
    //   iconRight: <CaretRight />,
    //   onPress: () => {
    //     router.push("/restaurant/screens/customer_screen/");
    //   },
    // },
    {
      iconLeft: <SignOut />,
      title: i18n.t("profile.logout"),
      onPress: logout,
    },
  ];

  return (
    <>
      {options.map((item, index) => (
        <PartnerProfileMenu
          key={index}
          id={index}
          iconLeft={item.iconLeft}
          iconRight={item.iconRight}
          title={item.title}
          onPress={() => item.onPress()}
        />
      ))}
    </>
  );
};
