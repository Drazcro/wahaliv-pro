import useDeviceType from "src/hooks/useDeviceType";
import React, { ReactNode } from "react";
import { StyleSheet, Text, View } from "react-native";
import { fontFamily } from "src/theme/typography";
import * as Device from "expo-device";

interface IPartnerProfileInformationsProps {
  title: string;
  icon?: ReactNode | undefined;
  value: string | undefined;
  unit?: string;
  icon_plus?: HTMLImageElement;
  startText?: string;
  textValue?: string | number;
  endText?: string;
  tipTextStart?: string;
  tipTextValue: number;
  tipTextEnd?: string;
}

export const PartnerProfileInformations = (
  props: IPartnerProfileInformationsProps,
) => {
  const isTablet = useDeviceType();

  return (
    <View
      style={
        isTablet && Device.brand != "SUNMI"
          ? styles.tablet_container
          : styles.container
      }
    >
      <View style={styles.content_title}>
        <Text style={styles.text_title}>{props.title}</Text>
      </View>
      <View style={styles.content_item_one}>
        <View style={styles.content_value}>
          {props.icon ? <View style={styles.icon}>{props.icon}</View> : null}
          <Text
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_text_value
                : styles.text_value
            }
          >
            {props.value}
          </Text>
          <Text
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_text_unity
                : styles.text_unity
            }
          >
            {" "}
            {props.unit}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            alignSelf: "center",
            marginTop: 7,
          }}
        >
          <Text style={styles.text_message}>{props.startText}</Text>
          {Number(props.textValue) >= 0 ? (
            <Text style={styles.text_message_value}> {props.textValue} </Text>
          ) : null}
          <Text style={styles.text_message}>{props.endText}</Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 141,
    marginBottom: 26,
    marginLeft: 10,
    backgroundColor: "rgba(246, 246, 246, 1)",
    justifyContent: "center",
    alignItems: "center",
    gap: 10,
    borderRadius: 15,
    height: 120,
  },
  content_title: {
    textAlign: "center",
  },
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(140, 140, 140, 1)",
    fontSize: 12,
    lineHeight: 14.6,
  },
  content_item_one: {
    flexDirection: "column",
    justifyContent: "center",
  },
  icon: {
    paddingRight: 5,
    alignSelf: "center",
  },
  content_value: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  text_value: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 18,
    fontFamily: fontFamily.semiBold,
  },
  text_unity: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 18,
    fontFamily: fontFamily.semiBold,
  },
  text_message: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 12,
    fontFamily: fontFamily.Medium,
    lineHeight: 14.6,
    textAlign: "center",
    alignSelf: "center",
  },
  text_message_value: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 12,
    fontWeight: "700",
    lineHeight: 14.6,
    textAlign: "center",
  },
  content_message: {
    alignSelf: "flex-start",
    flexDirection: "column",
  },
  /** tablet screen */
  tablet_container: {
    width: 125,
    height: 85,
    marginBottom: 43,
    marginTop: 34,
    marginLeft: 20,
  },
  tablet_text_value: {
    color: "#1D2A40",
    fontSize: 20,
    fontFamily: fontFamily.Bold,
  },
  tablet_text_unity: {
    color: "#1D2A40",
    fontSize: 20,
    fontFamily: fontFamily.Bold,
  },
});
