import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import Filter from "assets/image_svg/Sliders.svg";
import SearchIcon from "assets/image_svg/SearchIcon.svg";
import { fontFamily } from "src/theme/typography";
import { IPartnerBillCustomerData } from "src/interfaces/Entities";
import moment from "moment";
import { Overlay } from "react-native-elements";
import NoInvoice from "assets/image_svg/Recurso.svg";
import { PartnerCustomerInvoiceCards } from "src_legacy/components/partenaire/Cards/PartnerInvoiceCard";
import { load_customer_bill } from "src_legacy/services/Loaders";
import InvoiceFilter from "./invoice_filter";

export function ClientInvoiceTabView(props: any) {
  const [customerBill, setCustomerBill] = useState<IPartnerBillCustomerData[]>(
    [],
  );
  const [inputData, setInputData] = useState<any>("");
  const [isLoading, setIsLoading] = useState(true);
  const [isFilterModal, setIsFilterModal] = useState(false);
  const [type, setType] = useState<string>("");
  const [action, setAction] = useState<string>();

  const [startDate, setStartDate] = useState<moment.Moment>(
    moment().subtract(30, "days"),
  );
  const [endDate, setEndDate] = useState<moment.Moment>(moment());

  const data = React.useMemo(() => {
    if (inputData !== "") {
      return (
        customerBill?.filter((item) =>
          Object.values(item).toString().includes(inputData),
        ) || []
      );
    }
    if (type != null) {
      return (
        customerBill?.filter((item) =>
          Object.values(item).toString().includes(type),
        ) || []
      );
    }
    return customerBill;
  }, [inputData, customerBill, type]);

  useEffect(() => {
    load_customer_bill(props.userId).then((data) => {
      setCustomerBill(data.data);
    });
  }, []);

  async function load_or_reload_data() {
    const begin_date =
      startDate && startDate.format("YYYY-MM-DD").toLocaleString();
    const end_date = endDate && endDate.format("YYYY-MM-DD").toLocaleString();
    console.log(begin_date, end_date);
  }

  useEffect(() => {
    setIsLoading(true);
    load_or_reload_data().finally(() => {
      setIsLoading(false);
    });
  }, []);

  const getData = React.useMemo(() => {
    if (data?.length > 0) {
      return data.filter(
        (item) =>
          moment(item.date.date).isSameOrAfter(moment(startDate)) &&
          moment(item.date.date).isSameOrBefore(moment(endDate)),
      );
    }
    return [];
  }, [data, isFilterModal, endDate, startDate, type]);

  const openModal = () => {
    setType("");
    setIsFilterModal(true);
    console.log("newType : ", type);
  };

  return (
    <View style={styles.invoiceBodyPosition}>
      <View style={styles.filterPosition}>
        <View style={styles.textInputPosition}>
          <SearchIcon
            style={{
              marginTop: 5,
            }}
          />
          <TextInput
            style={styles.textInput}
            onChangeText={(text) => setInputData(text)}
            placeholderTextColor="#8D98A0"
            value={inputData}
            placeholder="Recherche"
          />
        </View>
        <TouchableOpacity onPress={() => openModal()}>
          <Filter />
        </TouchableOpacity>
      </View>
      <ScrollView>
        {getData.length > 0 ? (
          <View style={styles.invoiceBody}>
            {getData.map((item) => (
              <PartnerCustomerInvoiceCards data={item} key={item.order_id} />
            ))}
          </View>
        ) : (
          <View style={styles.noSearchResultBloc}>
            {data?.length === 0 && type !== "Avoir" ? (
              <View style={styles.noSearchResultBloc}>
                <NoInvoice />
                <Text style={styles.noSearchResultBlocText}>
                  Vous n'avez pas encore de facture
                </Text>
              </View>
            ) : (
              <View style={styles.noSearchResultBloc}>
                <NoInvoice />
                <Text style={styles.noSearchResultBlocText}>
                  Aucune facture ne correspond à votre recherche
                </Text>
              </View>
            )}
          </View>
        )}
      </ScrollView>
      <Overlay
        isVisible={isFilterModal}
        onBackdropPress={() => setIsFilterModal(false)}
        overlayStyle={{ width: "100%", height: "100%" }}
      >
        <InvoiceFilter
          startDate={startDate}
          endDate={endDate}
          setStartDate={setStartDate}
          setEndDate={setEndDate}
          setType={setType}
          setAction={setAction}
          SaOrClient="client"
          onClose={() => setIsFilterModal(false)}
          onApply={() => {
            setIsFilterModal(false);
          }}
        />
      </Overlay>
    </View>
  );
}

const styles = StyleSheet.create({
  invoiceBody: {
    flex: 1,
    alignItems: "center",
  },
  invoiceBodyPosition: {
    marginBottom: 40,
  },
  textInput: {
    marginLeft: 5,
    fontFamily: fontFamily.Medium,
    width: "100%",
  },
  textInputPosition: {
    flexDirection: "row",
    width: "80%",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    marginRight: 10,
  },
  filterPosition: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 10,
  },
  noSearchResultBloc: {
    justifyContent: "center", //Centered horizontally
    alignItems: "center", //Centered vertically
    flex: 1,
    height: 500,
  },

  noSearchResultBlocText: {
    fontFamily: fontFamily.semiBold,
    color: "#C4C4C4",
  },
});
