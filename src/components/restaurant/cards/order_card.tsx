import React from "react";
import {
  View,
  Text,
  TouchableOpacity,
  PixelRatio,
  StyleSheet,
} from "react-native";
import moment from "moment";
import "moment/locale/fr";

// import { COURSE_STATUS } from "src/constants/Course";
import DarkClock from "assets/image_svg/ClockDark.svg";
import Clock from "assets/image_svg/Clock.svg";

import WhiteScoot from "assets/image_svg/ScooterWhite.svg";
import Sac from "assets/image_svg_partner/Emporter.svg";
import ThumbsUpGreen from "assets/image_svg_partner/ThumbsUpGreen.svg";
import ScootBlack from "assets/image_svg_partner/ScootBlack.svg";
import EmporterBlack from "assets/image_svg_partner/EmporterBlack.svg";
import UserCanceled from "assets/image_svg_partner/UserCanceled.svg";
import CanceledClock from "assets/image_svg_partner/CanceledClock.svg";
import User from "assets/image_svg/User.svg";

import { fontFamily } from "src/theme/typography";
import { STATUS } from "src/constants/status";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { IOrder, ORDER_STATUS } from "src/interfaces/restaurant/order";
import { dateDiff, isToday, parse_to_float_number } from "src/services/helpers";

moment.locale("fr");

interface Props {
  course: IOrder;
  onPress: () => void;
  //onAcceptPrompt: () => void;
}

export function OrderCard(props: Props) {
  const isTablet = useDeviceType();

  const isDelivery =
    STATUS.PARTNER_SERVICE_TYPE_DELIVERY === props.course.commande.service_type;
  const isNew = props?.course?.commande?.status === ORDER_STATUS.NEW;
  const isCanceled = props?.course?.commande?.status === ORDER_STATUS.CANCELLED;
  const isAccepted = props?.course?.commande?.status === ORDER_STATUS.ACCEPTED;
  const isDelivered =
    props?.course?.commande?.status === ORDER_STATUS.DELIVERED;

  const canceledColorText = isCanceled ? "#C4C4C4" : "#000000";

  const cardState = React.useMemo(() => {
    const dateNow = moment(new Date());
    const dateLiv = moment(props?.course?.commande?.delivery_date?.date);
    const datePicks = moment(props?.course?.commande?.pickup_date?.date);

    const PICK = dateDiff(datePicks, dateNow);
    const LIV = dateDiff(dateLiv, dateNow);

    const dayPick = PICK.day;
    const minPick = PICK.min;
    const hourPick = PICK.hour;

    let label = "À Valider";
    let color = "#E9C852";
    const delivery_status = props?.course?.commande?.status;

    ///////// Accepted /////////
    if (delivery_status === ORDER_STATUS.ACCEPTED) {
      color = "rgba(64, 173, 162, 1)";
      if (hourPick < 0 || dayPick < 0) {
        label = "Plus d'1 heure de retard";
      }
      if (hourPick > 0 || dayPick >= 0) {
        label = "Dans plus d'1 heure";
      } else if (hourPick === 0) {
        if (minPick >= 0) {
          label = `${minPick} min`;
          color = "rgba(64, 173, 162, 1)";
        } else {
          label = `En retard de ${minPick * -1} min`;
          color = "rgba(233, 96, 104, 1)";
        }
      }
    } else if (delivery_status === ORDER_STATUS.DELIVERED) {
      label = "Livrée";
      color = "#40ADA2";
    }
    return {
      label,
      color,
      datePick: datePicks,
      dateLiv: dateLiv,
      datePickDiff: PICK,
      dateLivDiff: LIV,
    };
  }, []);

  const LabelAndColor = React.useCallback(() => {
    return (
      <View style={{ width: "100%" }}>
        {props?.course?.commande?.status === ORDER_STATUS.NEW && (
          <View
            style={[
              isTablet && Device.brand != "SUNMI"
                ? cardStyles.tabletWrapper
                : cardStyles.wrapper,
              { backgroundColor: "rgba(233, 200, 82, 0.84)" },
            ]}
          >
            <Text style={[cardStyles.labelStyle, { paddingHorizontal: 10 }]}>
              À Valider
            </Text>
          </View>
        )}

        {props?.course?.commande?.status === ORDER_STATUS.ACCEPTED && (
          <View
            style={[
              isTablet && Device.brand != "SUNMI"
                ? cardStyles.tabletWrapper
                : cardStyles.wrapper,
              {
                backgroundColor: cardState.color,
                width: 156,
              },
            ]}
          >
            <Clock />
            <Text style={cardStyles.labelStyle}>{cardState.label}</Text>
          </View>
        )}

        {props?.course?.commande?.status === ORDER_STATUS.CANCELLED && (
          <View
            style={[
              isTablet && Device.brand != "SUNMI"
                ? cardStyles.tabletWrapper
                : cardStyles.wrapper,
              { backgroundColor: "rgba(196, 196, 196, 1)" },
            ]}
          >
            <Text style={[cardStyles.labelStyle, { paddingHorizontal: 10 }]}>
              Refusée
            </Text>
          </View>
        )}
        {props?.course?.commande?.status === ORDER_STATUS.DELIVERED && (
          <View
            style={[
              isTablet && Device.brand != "SUNMI"
                ? cardStyles.tabletWrapper
                : cardStyles.wrapper,
              { backgroundColor: "rgba(64, 173, 162, 1)" },
            ]}
          >
            <Text style={[cardStyles.labelStyle, { paddingHorizontal: 10 }]}>
              {isDelivery ? "Livrée" : "Retirée"}
            </Text>
          </View>
        )}
      </View>
    );
  }, [isTablet]);

  const HeaderLogoColor = React.useCallback(() => {
    let bgColor = "";
    switch (props.course.commande.status) {
      case ORDER_STATUS.NEW: {
        bgColor = "rgba(236, 208, 108, 1)";
        break;
      }
      case ORDER_STATUS.ACCEPTED: {
        bgColor = cardState.color;
        break;
      }
      case ORDER_STATUS.DELIVERED: {
        bgColor = "rgba(64, 173, 162, 1)";
        break;
      }
      case ORDER_STATUS.CANCELLED: {
        bgColor = "rgba(196, 196, 196, 1)";
        break;
      }
    }
    return (
      <View>
        <View style={[cardStyles.circle, { backgroundColor: bgColor }]}>
          {!isDelivery ? <Sac /> : <WhiteScoot />}
        </View>
      </View>
    );
  }, []);

  const newDate = moment(props.course?.commande?.pickup_date?.date);
  const delivery_date = moment(props.course?.commande?.delivery_date?.date);

  const getDeliveryDate = React.useMemo(() => {
    try {
      return delivery_date
        .toLocaleString()
        .split(" ")[4]
        .slice(0, 5)
        .replace(":", "h");
    } catch (e) {
      return "";
    }
  }, [delivery_date]);
  const getNewDate = React.useMemo(() => {
    try {
      return newDate
        .toLocaleString()
        .split(" ")[4]
        .slice(0, 5)
        .replace(":", "h");
    } catch (e) {
      return "";
    }
  }, [newDate]);

  return (
    <View style={cardStyles.main}>
      {isTablet && Device.brand != "SUNMI" ? (
        <TouchableOpacity
          onPress={props.onPress}
          style={{
            ...cardStyles.tabletBox,
            backgroundColor:
              props.course?.commande?.status === 5
                ? "rgba(254, 251, 242, 0.78)"
                : "white",
          }}
        >
          {(isNew || isCanceled || isAccepted) && (
            <>
              <View
                style={{
                  flexDirection: "column",
                  paddingTop: 14,
                  paddingLeft: 17,
                  width: "65%",
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <HeaderLogoColor />
                  <Text
                    style={{
                      ...cardStyles.count,
                      marginLeft: 10,
                      color: canceledColorText,
                    }}
                  >
                    {" "}
                    N° {props?.course?.commande?.id}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginVertical: 5,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      width: 240,
                      alignItems: "center",
                    }}
                  >
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        width: 30,
                        height: 30,
                      }}
                    >
                      {isCanceled ? (
                        <UserCanceled width={19} height={19} />
                      ) : (
                        <User width={19} height={19} />
                      )}
                    </View>
                    <Text
                      style={{
                        fontFamily: fontFamily.Medium,
                        fontSize: 14,
                        lineHeight: 22,
                        marginLeft: 15,
                        color: canceledColorText,
                      }}
                    >
                      {props?.course?.customer?.name}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    {isCanceled ? <CanceledClock /> : <DarkClock />}
                    <Text style={{ color: canceledColorText }}>
                      {" "}
                      Collecte{" "}
                      {isToday(newDate.toDate(), new Date())
                        ? "Aujourd'hui "
                        : newDate.calendar().split(" à")[0]}
                    </Text>
                    {newDate && (
                      <Text style={{ color: canceledColorText }}>
                        à{" "}
                        <Text
                          style={{
                            ...cardStyles.boldText,
                            color: canceledColorText,
                          }}
                        >{` ${getNewDate}`}</Text>
                      </Text>
                    )}
                  </View>
                </View>
                <View style={{ flexDirection: "row" }}>
                  {props.course?.product_details.length === 1 && (
                    <Text
                      style={{ ...cardStyles.text, color: canceledColorText }}
                    >
                      {props.course?.product_details.length} produit
                    </Text>
                  )}
                  {props.course?.product_details.length > 1 && (
                    <Text
                      style={{ ...cardStyles.text, color: canceledColorText }}
                    >
                      {props.course?.product_details.length} produits
                    </Text>
                  )}
                </View>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  paddingTop: 14,
                  paddingRight: -3,
                  width: "35%",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "flex-end",
                    width: isAccepted ? 156 : 107,
                    marginRight: 30,
                  }}
                >
                  <LabelAndColor />
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "flex-end",
                    marginRight: 30,
                    marginTop: 45,
                  }}
                >
                  <Text
                    style={{
                      ...cardStyles.tabletBoldText,
                      color: isCanceled ? "#C4C4C4" : "#1D2A40",
                    }}
                  >
                    {parse_to_float_number(props.course?.total_amount)} €
                  </Text>
                </View>
              </View>
            </>
          )}

          {isDelivered && (
            <>
              <View
                style={{
                  flexDirection: "column",
                  paddingTop: 14,
                  paddingLeft: 17,
                  width: "60%",
                }}
              >
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <HeaderLogoColor />
                  <Text style={{ ...cardStyles.count, marginLeft: 5 }}>
                    {" "}
                    N° {props?.course?.commande?.id}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    marginTop: 5,
                  }}
                >
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      width: 210,
                    }}
                  >
                    <View
                      style={{
                        justifyContent: "center",
                        alignItems: "center",
                        width: 30,
                        height: 30,
                      }}
                    >
                      <User width={19} height={19} />
                    </View>
                    <Text
                      style={{
                        fontFamily: fontFamily.Medium,
                        fontSize: 14,
                        lineHeight: 19.5,
                        color: "#1D2A40",
                        marginLeft: 10,
                      }}
                    >
                      {props?.course?.customer?.name}
                    </Text>
                  </View>
                  <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text
                      style={{
                        marginRight: 10,
                        fontFamily: fontFamily.Medium,
                        fontSize: 14,
                        lineHeight: 22,
                        color: "#000000",
                      }}
                    >
                      Note
                    </Text>
                    {<ThumbsUpGreen /> /*<ThumbsDownRed />*/}
                  </View>
                </View>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                  <View
                    style={{
                      justifyContent: "center",
                      alignItems: "center",
                      width: 30,
                      height: 30,
                    }}
                  >
                    {isDelivery ? <ScootBlack /> : <EmporterBlack />}
                  </View>
                  <Text
                    style={{
                      marginLeft: 10,
                      fontFamily: fontFamily.Medium,
                      fontSize: 14,
                      lineHeight: 22,
                      color: "#000000",
                    }}
                  >
                    {isDelivery ? "Livrée " : "Retirée "}
                    {isToday(delivery_date.toDate(), new Date())
                      ? " Aujourd'hui "
                      : " le " + delivery_date.calendar().split("à")[0]}{" "}
                    à {getDeliveryDate}{" "}
                    {isDelivery
                      ? "par " + props.course.commande.deliveryman
                      : ""}
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: "column",
                  paddingTop: 14,
                  paddingRight: -3,
                  width: "40%",
                }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "flex-end",
                    marginRight: 30,
                    width: 107,
                  }}
                >
                  <LabelAndColor />
                </View>

                <View
                  style={{
                    flexDirection: "row",
                    alignSelf: "flex-end",
                    marginRight: 30,
                    marginTop: 45,
                  }}
                >
                  <View style={{ flexDirection: "row" }}>
                    {props.course?.product_details.length === 1 && (
                      <Text style={cardStyles.text}>
                        {props.course?.product_details.length} produit
                      </Text>
                    )}
                    {props.course?.product_details.length > 1 && (
                      <Text style={cardStyles.text}>
                        {props.course?.product_details.length} produits
                      </Text>
                    )}
                  </View>
                  <Text
                    style={{ ...cardStyles.tabletBoldText, marginLeft: 20 }}
                  >
                    {parse_to_float_number(props.course?.total_amount)} €
                  </Text>
                </View>
              </View>
            </>
          )}
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          onPress={props.onPress}
          style={{
            ...cardStyles.box,
            backgroundColor:
              props.course?.commande?.status === 5
                ? "rgba(254, 251, 242, 0.78)"
                : "white",
          }}
        >
          <View style={cardStyles.header}>
            <View
              style={{ flexDirection: "row", alignItems: "center", gap: 8 }}
            >
              <HeaderLogoColor />
              <Text style={cardStyles.count}>
                {" "}
                N° {props?.course?.commande?.id}
              </Text>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignSelf: "flex-end",
                width: isAccepted ? 156 : 107,
                //marginRight: 30,
              }}
            >
              <LabelAndColor />
            </View>
          </View>
          <View style={cardStyles.user}>
            <User />
            <Text style={cardStyles.text}>{props?.course?.customer?.name}</Text>
          </View>

          <View style={cardStyles.nameAndClock}>
            <DarkClock />
            <Text style={cardStyles.text}>
              {(props?.course?.commande?.status === ORDER_STATUS.NEW ||
                props?.course?.commande?.status === ORDER_STATUS.CANCELLED ||
                props?.course?.commande?.status === ORDER_STATUS.ACCEPTED) && (
                <React.Fragment>
                  <Text>
                    Collecté
                    {isToday(newDate.toDate(), new Date())
                      ? " Aujourd'hui "
                      : newDate.calendar().split(" à")[0]}
                  </Text>
                  {newDate && (
                    <Text>
                      {" "}
                      à
                      <Text
                        style={cardStyles.boldText}
                      >{` ${getNewDate}`}</Text>
                    </Text>
                  )}
                </React.Fragment>
              )}

              {props?.course?.commande?.status === ORDER_STATUS.DELIVERED && (
                <React.Fragment>
                  <Text>
                    {"Livrée "}
                    {isToday(delivery_date.toDate(), new Date())
                      ? " Aujourd'hui "
                      : delivery_date.calendar().split("à")[0]}
                  </Text>
                  {getDeliveryDate && (
                    <Text>
                      à{" "}
                      <Text style={cardStyles.boldText}>{getDeliveryDate}</Text>
                    </Text>
                  )}
                </React.Fragment>
              )}
            </Text>
          </View>

          <View style={cardStyles.tabletProduct}>
            {props.course?.product_details.length === 1 && (
              <Text style={cardStyles.text}>
                {props.course?.product_details.length} produit
              </Text>
            )}
            {props.course?.product_details.length > 1 && (
              <Text style={cardStyles.text}>
                {props.course?.product_details.length} produits
              </Text>
            )}
            <Text style={{ ...cardStyles.boldText, fontSize: 16 }}>
              {parse_to_float_number(props.course?.total_amount)} €
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
}

const cardStyles = StyleSheet.create({
  main: {
    width: "100%",
    marginVertical: 10,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  card: {
    position: "relative",
    flexDirection: "column",
  },
  cardImg: {
    position: "absolute",
    top: 35,
    left: 25,
  },
  subText: {
    fontFamily: fontFamily.regular,
    flexDirection: "row",
    marginLeft: 20,
    alignItems: "center",
  },
  textSub: {
    fontFamily: fontFamily.regular,
    color: "#FF5C85",
    fontSize: 12,
    lineHeight: 22,
    marginLeft: 5,
  },
  text: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 19.5,
    color: "#1D2A40",
    paddingBottom: 20,
    marginLeft: 5,
  },
  tabletText: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 19.5,
    color: "#1D2A40",
    paddingBottom: 20,
    marginLeft: "25%",
    backgroundColor: "green",
    //textAlign: "center",
    //alignSelf: "center"
  },
  boldText: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 19.5,
    color: "#1D2A40",
    paddingBottom: 20,
    marginLeft: 5,
  },
  tabletBoldText: {
    fontFamily: fontFamily.Bold,
    fontSize: 16,
    lineHeight: 19.5,
    color: "#1D2A40",
    paddingBottom: 20,
    marginLeft: 5,
  },

  header: {
    width: "95%",
    flexDirection: "row",
    marginTop: 20,
    marginLeft: "5%",
    justifyContent: "space-between",
  },
  user: {
    flexDirection: "row",
    marginLeft: 20,
    marginTop: 10,
  },
  nameAndClock: {
    flexDirection: "row",
    marginLeft: 20,
  },
  tabletNameAndClock: {
    flexDirection: "row",
    marginTop: 10,
    marginLeft: 14,
    //backgroundColor: "red"
  },
  product: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginRight: 20,
    marginTop: 10,
    marginLeft: 20,
  },
  tabletProduct: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginRight: 20,
    marginTop: 0,
    marginLeft: 18,
  },
  count: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#1D2A40",
  },
  customerName: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
    marginTop: 2,
    marginLeft: -4,
  },
  box: {
    position: "relative",
    borderWidth: 1,
    borderColor: "rgba(233, 200, 82, 0.95)",
    borderRadius: 5,
    width: "90%",
    minHeight: 158,
  },
  tabletBox: {
    height: 115,
    width: 730,
    borderWidth: 1,
    borderColor: "#DADADA",
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  circle: {
    alignItems: "center",
    justifyContent: "center",
    height: 30,
    width: 30,
    borderRadius: 45 / PixelRatio.get(),
  },
  state: {
    fontFamily: fontFamily.semiBold,
    fontSize: 12,
    color: "#fff",
    lineHeight: 22,
    marginHorizontal: 10,
    letterSpacing: -0.5,
  },
  name: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 19,
  },
  title: {
    fontFamily: fontFamily.Medium,
    fontSize: 12,
    lineHeight: 22,
    color: "#000",
  },
  mapLineImg: {
    top: 5,
    left: -4,
  },
  map: {
    top: 4,
  },
  wrapper: {
    paddingHorizontal: 5,
    height: 27,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderBottomLeftRadius: 13.5,
    borderTopLeftRadius: 13.5,
  },
  tabletWrapper: {
    paddingHorizontal: 5,
    height: 27,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    borderRadius: 13.5,
  },
  containerLine: {
    bottom: 0,
    height: "100%",
    borderWidth: 1,
  },
  line: {
    position: "absolute",
    top: 0,
    left: 26,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 5,
    height: "35%",
    borderColor: "#1D2A40",
  },
  group: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 20,
  },
  groupText: {
    color: "#FF5C85",
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.Medium,
    marginLeft: 10,
    width: "100%",
    flexWrap: "wrap",
  },
  labelStyle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 12,
    color: "#fff",
    lineHeight: 22,
    marginStart: 5,
    letterSpacing: -0.5,
  },
});
