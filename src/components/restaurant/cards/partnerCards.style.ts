import { Fontisto } from "@expo/vector-icons";
import { COURSE_STATUS } from "src/constants/course";
import { Dimensions, PixelRatio, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

const { width } = Dimensions.get("window");

interface IDeliveryCardStyleProps {
  color: string;
  status: typeof COURSE_STATUS[keyof typeof COURSE_STATUS];
  minPick: number;
  minLiv: number;
}

export const styles = (props: IDeliveryCardStyleProps) => {
  const color = props.color;
  const status = props.status;
  const minPick = props.minPick;
  const minLiv = props.minLiv;

  const setValues = () => {
    // A accepter
    if (status === COURSE_STATUS.ACCEPTED && minPick < 0) {
      return { color, _width: "53%" };
    }

    // En cours
    if (status === COURSE_STATUS.RECOVERED && minLiv < 0) {
      return { color, _width: "53%" };
    }
    // Livrée
    if (status === COURSE_STATUS.FINISHED && (minLiv < 0 || minPick < 0)) {
      return { color, _width: "35%" };
    }

    return { color, _width: "35%" };
  };
};
