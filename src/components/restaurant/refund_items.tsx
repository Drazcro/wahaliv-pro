import React, { FC } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import EllipseB from "assets/image_svg_partner/EllipseB.svg";
import EllipseG from "assets/image_svg_partner/EllipseG.svg";
import { IProductDetails } from "src/interfaces/restaurant/order";
import { parse_to_float_number } from "src/services/helpers";

interface RefundsItemProps {
  product: IProductDetails;
  selectedProducts: IProductDetails[];
  onSelectedProducts: (product: IProductDetails) => void;
}

export const RefundsItems: FC<RefundsItemProps> = React.memo(
  ({ product, onSelectedProducts, selectedProducts }) => {
    const setSelectedProducts = () => {
      onSelectedProducts(product);
    };

    return (
      <TouchableOpacity
        onPress={() => setSelectedProducts()}
        style={styles.refundsContent}
      >
        <View style={styles.refundItems}>
          <View style={{ margin: 2 }}>
            {selectedProducts.length > 0 &&
            selectedProducts.find((value) => value.id === product.id) ? (
              <EllipseG />
            ) : (
              <EllipseB />
            )}
          </View>
          <Text style={styles.refundItemText}>
            {product.qte} {product.name}
          </Text>
        </View>
        <Text style={styles.refundItemPrice}>
          {parse_to_float_number(product.price)}€
        </Text>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  refundsContent: {
    marginHorizontal: 30,
    marginBottom: 16,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  refundItems: {
    width: "65%",
    flexDirection: "row",
  },
  refundItemText: {
    marginLeft: 13,
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 16,
    lineHeight: 22,
  },
  refundItemPrice: {
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 16,
    lineHeight: 22,
  },
});
