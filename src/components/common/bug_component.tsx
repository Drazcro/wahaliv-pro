import React, { useState } from "react";
import {
  Keyboard,
  Linking,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Text } from "react-native-elements";
import { color } from "src/theme";
import { fontFamily } from "src/theme/typography";

export function BugComponent() {
  const [message, setMessage] = useState<string>("");

  function handleChange(value: string) {
    setMessage(value);
    console.log("message : ", message);
  }

  return (
    <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={styles.body}>
        <View style={styles.textPosition}>
          <Text style={styles.text}>
            Expliquez en quelques mots votre problème
          </Text>
        </View>
        <TextInput
          underlineColorAndroid="transparent"
          placeholder="Votre texte ..."
          placeholderTextColor="grey"
          numberOfLines={10}
          multiline={true}
          style={styles.textArea}
          onChangeText={handleChange}
        />
        <View style={styles.buttonPosition}>
          {message.length === 0 && (
            <TouchableOpacity style={styles.button}>
              <Text style={styles.buttonText}>Envoyer</Text>
            </TouchableOpacity>
          )}
          {message.length != 0 && (
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(
                  `mailto:prod@sacrearmand.com?subject=Bug&body=${message}`,
                )
              }
              style={styles.buttonValidate}
            >
              <Text style={styles.buttonText}>Envoyer</Text>
            </TouchableOpacity>
          )}
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
}

const styles = StyleSheet.create({
  body: {
    height: "100%",
    marginTop: -23,
  },
  container: {
    marginTop: "15%",
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 25,
  },
  icon: {
    paddingRight: 5,
  },
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    lineHeight: 24.38,
    marginRight: 20,
    textAlign: "center",
  },
  textPosition: {
    textAlign: "left",
    marginHorizontal: 30,
    marginTop: "10%",
  },
  text: {
    fontFamily: fontFamily.regular,
    fontSize: 16,
    fontWeight: "500",
  },
  textArea: {
    marginTop: 17,
    paddingHorizontal: 20,
    alignItems: "center",
    marginHorizontal: 20,
    borderWidth: 1,
    borderRadius: 15,
    borderColor: "#E7E7E7",
    height: "40%",
    fontFamily: fontFamily.regular,
    fontSize: 16,
    fontWeight: "500",
    paddingTop: 30,
    textAlignVertical: "top",
  },
  button: {
    backgroundColor: "#C4C4C4",
    width: "80%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  buttonValidate: {
    backgroundColor: color.ACCENT,
    width: "80%",
    height: 50,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  buttonPosition: {
    alignItems: "center",
    position: "absolute",
    alignSelf: "center",
    bottom: "15%",
    width: "100%",
  },
  buttonText: {
    color: "white",
    fontFamily: fontFamily.Bold,
    fontSize: 16,
    fontWeight: "600",
  },
});
