import React, { useEffect } from "react";
import { SafeAreaView, ScrollView, StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import { IMessage } from "src/interfaces/common";
import Bubbles from "src/components/common/bubbles";

interface Props {
  messages: IMessage[];
}

export default function MessageBone({ messages }: Props) {
  // const flashListRef = React.useRef<FlashList<IMessage>>(null);
  const messagesEndRef = React.useRef<any>(null);

  useEffect(() => {
    const lastMessage = messagesEndRef.current?.lastElementChild;

    if (lastMessage) {
      lastMessage.scrollIntoView({ behavior: "instant" });
    }
  }, [messages]);

  return (
    <View
      style={{
        ...styles.containerHome,
      }}
    >
      {messages.length === 0 ? (
        <Text style={styles.NoMessagesText}>
          Écris un message pour qu'un de nos opérateurs puisse t'aider
        </Text>
      ) : (
        <SafeAreaView>
          <ScrollView
            ref={messagesEndRef}
            onContentSizeChange={() =>
              messagesEndRef.current?.scrollToEnd({ animated: true })
            }
            onLayout={() => {
              messagesEndRef.current?.scrollToEnd({ animated: true });
            }}
          >
            {messages.map((message, index) => (
              <Bubbles msg={message} key={index} />
            ))}
          </ScrollView>
        </SafeAreaView>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  containerHome: {
    width: "100%",
    flex: 1,

    paddingTop: 10,
  },
  centered: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  textCenter: {
    textAlign: "center",
    marginTop: 20,
  },
  NoMessagesText: {
    textAlign: "center",
    flex: 5,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 50,
    marginTop: 167,
    fontFamily: fontFamily.Medium,
    opacity: 0.4,
    fontSize: 16,
  },
});
