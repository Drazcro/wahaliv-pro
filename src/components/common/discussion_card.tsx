import React, { useEffect, useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Divider } from "react-native-elements";
import { fontFamily } from "src/theme/typography";

import Avatar from "assets/image_svg/DefaultAvatar.svg";
import moment from "moment";
import { off, onValue, ref } from "firebase/database";
import { db } from "firebase";
import { router } from "expo-router";
import { ClientDiscussionCard } from "src/interfaces/Entities";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src/services/auth_store";

type Props = {
  discussion: ClientDiscussionCard;
};

const DiscussionCard = ({ discussion }: Props) => {
  const { base_user: user } = authStore();
  const [counter, setCounter] = useState<number>(0);

  useEffect(() => {
    const counter_ref = ref(
      db,
      `chat_all_My8xMS8yMDIz/${user?.id}/${discussion.id_command}/unread_for/livreur`,
    );
    try {
      onValue(counter_ref, (snapshot) => {
        if (!snapshot.exists()) {
          return;
        }

        setCounter(snapshot.val());
      });
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error);
    }

    return () => {
      off(counter_ref);
    };
  }, [discussion.id_command]);

  return (
    <View>
      <TouchableOpacity
        style={styles.conversationContainer}
        onPress={() => {
          router.push({
            pathname: "/delivery/screens/chat_client_screen",
            params: { command_id: discussion.id_command },
          });
        }}
      >
        <View style={{ position: "relative" }}>
          <Avatar />
          <View
            style={{
              width: 15,
              height: 15,
              backgroundColor: "rgba(245, 160, 82, 1)",
              borderRadius: 50,
              position: "absolute",
              right: 0,
              bottom: 0,
            }}
          ></View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "80%",
          }}
        >
          <View style={{ marginLeft: 20 }}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: fontFamily.semiBold,
                lineHeight: 19.5,
                color: "rgba(29, 42, 64, 1)",
              }}
            >
              Client: {discussion.nom_client}
            </Text>
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 14,
                lineHeight: 18,
                fontStyle: "italic",
                color: "rgba(141, 152, 160, 1)",
                marginTop: 3,
              }}
            >
              {moment(discussion.last_message_at).calendar()}
            </Text>
          </View>
        </View>
        {counter > 0 ? (
          <View
            style={{
              position: "absolute",
              backgroundColor: "rgba(255, 92, 133, 1)",
              width: 23,
              height: 23,
              borderRadius: 50,
              right: 20,
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "#fff",
                fontSize: 16.43,
                fontFamily: fontFamily.Medium,
              }}
            >
              {counter}
            </Text>
          </View>
        ) : null}
      </TouchableOpacity>
      <Divider width={1} />
    </View>
  );
};

export default DiscussionCard;

const styles = StyleSheet.create({
  conversationContainer: {
    paddingVertical: 10,
    marginLeft: 22,
    flexDirection: "row",
    alignItems: "center",
  },
  counter: {
    backgroundColor: "rgba(255, 92, 133, 1)",
    width: 23,
    height: 23,
    borderRadius: 50,
  },
});
