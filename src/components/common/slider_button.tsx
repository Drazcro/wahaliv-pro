import React from "react";
import { StyleSheet, Text, View } from "react-native";
import SwipeButton from "src/libs/rn-swipe-button";

import { images } from "src/theme/images";
import { fontFamily } from "src/theme/typography";

export const SliderButton = (props: {
  onSuccess: any;
  title: string;
  name?: string;
  subTitle: any;
  railBgColor: any;
  railStylesBgColor: any;
  subTitleColor: any;
  isDisabled?: boolean;
}) => {
  const onSuccess = props.onSuccess;
  const title = props.title;
  const name = props.name;
  const subTitle = props.subTitle;
  const railBgColor = props.railBgColor;
  const railStylesBgColor = props.railStylesBgColor;
  const subTitleColor = props.subTitleColor;
  const isDisabled = props.isDisabled;

  return (
    <View style={styles.main}>
      <SwipeButton
        disabled={false}
        swipeSuccessThreshold={95}
        height={52}
        width={308}
        title={title}
        name={name}
        titleColor="rgba(29, 42, 64, 1)"
        titleNameColor="#112842"
        shouldResetAfterSuccess
        onSwipeSuccess={onSuccess}
        railFillBackgroundColor={railStylesBgColor}
        railFillBorderColor={railStylesBgColor}
        thumbIconBackgroundColor={railStylesBgColor}
        thumbIconBorderColor={railStylesBgColor}
        railBackgroundColor={railBgColor}
        railBorderColor={railBgColor}
        thumbIconImageSource={images.Icone.CaretLeftOne}
        thumbIconWidth={76}
        titleStyles={{
          justifyContent: "center",
          alignSelf: "center",
          paddingLeft: "20%",
          fontFamily: fontFamily.semiBold,
          top: name ? 10 : 17,
        }}
        titleFontSize={16}
        titleNameStyles={{
          bottom: 6,
          justifyContent: "center",
          alignSelf: "center",
          paddingLeft: "20%",
          fontFamily: fontFamily.Medium,
          lineHeight: 18,
        }}
        titleNameFontSize={13}
        titleMaxFontScale={25}
      />
      <Text
        style={{
          ...styles.textBtn,
          fontSize: isDisabled ? 14 : 12,
          fontWeight: isDisabled ? "400" : "600",
          color: subTitleColor,
          fontFamily: fontFamily.Medium,
          width: "100%",
          justifyContent: "center",
          textAlign: "center",
        }}
      >
        {subTitle}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    alignItems: "center",
    justifyContent: "center",
  },
  textBtn: {
    fontFamily: fontFamily.regular,
    lineHeight: 14,
    color: "#000",
    marginTop: 10,
    marginBottom: 50,
  },
});
