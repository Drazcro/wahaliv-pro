import CaretLeft from "assets/image_svg/CaretLeft.svg";
import { useRouter } from "expo-router";
import { IconButton } from "react-native-paper";

export const HeaderBack = (props: any) => {
  const router = useRouter();
  return (
    <IconButton onPress={() => router.back()} icon={() => <CaretLeft />} />
  );
};
