import React from "react";
import { StyleSheet, Text } from "react-native";
import { Portal, Modal } from "react-native-paper";
import { useGPS } from "src/hooks/useLocalisation";
import { fontFamily } from "src/theme/typography";
import { LocationPermission } from "src/components/modals/permissions/location_permission";

export default function LocationServiceAlert() {
  const { check, isGPSOn, FGLocation, BGLocation } = useGPS();

  const containerStyle = { backgroundColor: "white", padding: 20 };

  React.useEffect(() => {
    let interval: string | number | NodeJS.Timeout | undefined;
    if (!isGPSOn) {
      interval = setInterval(check, 2000);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [isGPSOn, check]);

  const notGranted = React.useMemo(() => {
    return [FGLocation.granted, BGLocation.granted].some((item) => !item);
  }, [FGLocation, BGLocation]);

  // return (
  //   <Portal>
  //     <Modal
  //       style={styles.container}
  //       dismissable={false}
  //       visible={!isGPSOn}
  //       contentContainerStyle={containerStyle}
  //     >
  //       <Text style={styles.text}>
  //         Vous devez activer le service de localisation pour utiliser cette
  //         application
  //       </Text>
  //       <Text style={styles.text}>
  //         Assurez vous d'avoir activé le GPS sur votre smartphone
  //       </Text>
  //     </Modal>
  //     {isGPSOn ? (
  //       <Modal
  //         style={styles.container}
  //         dismissable={false}
  //         visible={notGranted}
  //         contentContainerStyle={containerStyle}
  //       >
  //         <LocationPermission />
  //       </Modal>
  //     ) : null}
  //   </Portal>
  // );

  return (
    <Portal>
      <Modal
        style={styles.container}
        dismissable={false}
        visible={notGranted}
        contentContainerStyle={containerStyle}
      >
        <LocationPermission />
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
    borderRadius: 5,
  },
  text: {
    textAlign: "center",
    fontFamily: fontFamily.semiBold,
  },
});
