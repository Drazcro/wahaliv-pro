import React from "react";
import { Linking } from "react-native";
import * as Location from "expo-location";

import { useGPS } from "src/hooks/useLocalisation";
import {
  GenericPermission,
  PermissionType,
} from "src/components/modals/permissions/generic_permission";

export function LocationPermission() {
  const [asked_fg, set_asked_fg] = React.useState(false);
  const [asked_bg, set_asked_bg] = React.useState(false);

  async function onGrantCallback() {
    console.log("Grant called");
    if (asked_bg && asked_fg) {
      await Linking.openSettings();
      return;
    }
    try {
      const { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== "granted") {
        console.log("Permission to access location was denied");
        onGrantCallback();
      } else {
        console.log("Foreground Permission to access location was granted");
        try {
          const { status: back_status } =
            await Location.requestBackgroundPermissionsAsync();

          if (back_status !== "granted") {
            console.log("Background Permission to access location was denied");
            onGrantCallback();
          }
        } catch (error) {
          await Linking.openSettings();
        } finally {
          set_asked_bg(true);
        }
      }
    } catch (error) {
      await Linking.openSettings();
    } finally {
      set_asked_fg(true);
    }
  }

  return (
    <GenericPermission
      permissionType={PermissionType.LOCATION}
      onGrantCallback={() => onGrantCallback()}
    />
  );
}
