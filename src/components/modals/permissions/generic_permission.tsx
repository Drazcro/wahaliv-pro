import React from "react";
import { View, Text, Image, TouchableOpacity, ScrollView } from "react-native";

import { styles } from "src/styles/generic_permission.style";
import { images } from "src/theme";
import * as Device from "expo-device";

export enum PermissionType {
  NOTIFICATION = "notifications",
  LOCATION = "location",
}

interface IPermissionProps {
  permissionType: PermissionType;
  onGrantCallback: () => void;
  onDenyCallback?: () => void;
}

import i18n from "i18n-js";

import useDeviceType from "src/hooks/useDeviceType";

export const getSVGByPermissionType = (type: PermissionType) => {
  const DIMENSIONS = 186;
  switch (type) {
    case PermissionType.LOCATION:
      return (
        <Image
          source={images.Landing.Location}
          style={{ width: 181, height: DIMENSIONS, resizeMode: "contain" }}
        />
      );
    case PermissionType.NOTIFICATION:
      return (
        <Image
          source={images.Landing.Notification}
          style={{ width: 181, height: DIMENSIONS, resizeMode: "contain" }}
        />
      );
  }
};

export function GenericPermission(props: IPermissionProps) {
  const { permissionType, onGrantCallback, onDenyCallback } = props;
  const title = i18n.t(`permissions.${permissionType}.title`);
  const newText = i18n.t(`permissions.${permissionType}.newText`);
  const acceptButton = i18n.t(`permissions.${permissionType}.acceptButton`);
  const denyButton = i18n.t(`permissions.${permissionType}.denyButton`);
  const isTablet = useDeviceType();

  return (
    <View style={styles.container}>
      {permissionType == PermissionType.NOTIFICATION ? (
        <View style={styles.content}>
          {getSVGByPermissionType(permissionType)}
          <Text style={styles.permissionTitle}>{title}</Text>
          <Text
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.TabletPermissionText
                : styles.permissionText
            }
          >
            {newText}
          </Text>
          <TouchableOpacity
            onPress={() => {
              onGrantCallback();
            }}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{acceptButton}</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              if (props.onDenyCallback) {
                props.onDenyCallback();
              }
            }}
            style={styles.denyButton}
          >
            <Text style={styles.denyButtonText}>{denyButton}</Text>
          </TouchableOpacity>
        </View>
      ) : null}
      {permissionType !== PermissionType.NOTIFICATION && (
        <View style={styles.content}>
          <View style={styles.contentFragmentDivider} />
          {getSVGByPermissionType(permissionType)}
          <ScrollView
            showsVerticalScrollIndicator={false}
            horizontal={false}
            contentContainerStyle={{
              maxWidth: "100%",
            }}
          >
            <View
              style={{
                flex: 1,
                justifyContent: "space-between",
                flexDirection: "column",
                // flexWrap: 'wrap'
                width: "100%",
              }}
            >
              <Text style={styles.permissionTitle}>{title}</Text>
              <Text
                style={{
                  ...styles.permissionText,
                  marginBottom: isTablet && Device.brand != "SUNMI" ? 40 : 20,
                }}
              >
                {i18n.t(`permissions.${permissionType}.newText`)}
              </Text>
            </View>
          </ScrollView>
          <TouchableOpacity
            onPress={() => {
              onGrantCallback();
            }}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{acceptButton}</Text>
          </TouchableOpacity>
          <View style={styles.contentFragmentDivider} />
        </View>
      )}
    </View>
  );
}
