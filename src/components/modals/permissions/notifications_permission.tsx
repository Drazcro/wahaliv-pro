import React from "react";
import {
  GenericPermission,
  PermissionType,
} from "src/components/modals/permissions/generic_permission";

import useNotification from "src/hooks/useNotification";
import { Linking } from "react-native";
import notifCheckStore from "src/zustore/notifCheckStore";

export function NotificationsPermission() {
  const { notif, push_token, request, check } = useNotification();
  const [asked, set_asked] = React.useState(false);
  const setIsChecked = notifCheckStore((state) => state.setIsChecked);

  const onGrantCallback = async () => {
    if (asked) {
      Linking.openSettings();
      return;
    }
    try {
      const response = await request();
      if (!response) {
        Linking.openSettings();
      }
    } catch (error) {
      onGrantCallback();
    } finally {
      set_asked(true);
      setIsChecked(true);
    }
  };

  const onDenyCallback = () => {
    setIsChecked(true);
  };

  React.useEffect(() => {
    let interval: string | number | NodeJS.Timeout | undefined;
    if (!notif.granted) {
      interval = setInterval(check, 2000);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [notif, check]);

  return (
    <GenericPermission
      permissionType={PermissionType.NOTIFICATION}
      onGrantCallback={onGrantCallback}
      onDenyCallback={() => onDenyCallback()}
    />
  );
}
