import React from "react";
import { StyleSheet, Text } from "react-native";
import { Portal, Modal } from "react-native-paper";
import useNotification from "src/hooks/useNotification";
import { fontFamily } from "src/theme/typography";
import { NotificationsPermission } from "src/components/modals/permissions/notifications_permission";

export default function NotificationServiceAlert() {
  const { check, push_token, notif } = useNotification();

  const containerStyle = { backgroundColor: "white", padding: 20 };

  React.useEffect(() => {
    let interval: string | number | NodeJS.Timeout | undefined;
    if (!notif.granted) {
      interval = setInterval(check, 2000);
    } else {
      clearInterval(interval);
    }
    return () => clearInterval(interval);
  }, [notif, check]);

  return (
    <Portal>
      <Modal
        style={styles.container}
        dismissable={false}
        visible={!notif.granted}
        contentContainerStyle={containerStyle}
      >
        <NotificationsPermission />
      </Modal>
    </Portal>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
    flex: 1,
  },
  text: {
    textAlign: "center",
    fontFamily: fontFamily.semiBold,
  },
});
