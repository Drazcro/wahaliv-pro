import { Overlay } from "react-native-elements";
import { ActivityIndicator } from "react-native-paper";
import { busyStore } from "src/zustore/busy_store";

export function IsBusyOverlay() {
  const isBusy = busyStore((value) => value.isBusy);

  return (
    <Overlay
      style={{ backgroundColor: "red" }}
      overlayStyle={{
        backgroundColor: "transparent",
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center",
        borderWidth: 0,
      }}
      isVisible={isBusy}
    >
      <ActivityIndicator size={"large"} />
    </Overlay>
  );
}
