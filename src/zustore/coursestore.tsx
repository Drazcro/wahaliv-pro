/* eslint-disable */

import { create } from "zustand";
// import { createStore } from "zustand/vanilla";

import { IInvoiceDataCourse } from "src/interfaces/Entities";
import { COURSE_STATUS } from "src/constants/course";
import { IOldCourse } from "src/interfaces/delivery/courses";

interface ICourseStore<T> {
  opened: IOldCourse;
  set_opened: (payload: IOldCourse) => void;
  clean_opened: () => void;
  courses: IInvoiceDataCourse[];
  setCourses: (value: IInvoiceDataCourse[]) => void;
}

const vanillaCourseStore = create<ICourseStore<{}>>((set) => ({
  opened: {} as IOldCourse,
  set_opened: (course) => set((payload) => ({ opened: course })),

  clean_opened: () => set({ opened: {} as IOldCourse }),
  courses: [],
  setCourses: (value: IInvoiceDataCourse[]) => set(() => ({ courses: value })),
}));

const courseStore = vanillaCourseStore as {
  <T>(): ICourseStore<T>;
  <T, U>(selector: (s: ICourseStore<T>) => U): U;
};

export { vanillaCourseStore, courseStore };
