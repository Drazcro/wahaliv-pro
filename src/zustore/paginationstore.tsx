/* eslint-disable */

import { create } from "zustand";

interface IPaginationStore<T> {
  totalPagesValidated: number;
  totalPagesAccepted: number;
  totalPagesDelivered: number;
  totalPagesRefused: number;
  setTotalPagesValidated: (payload: number) => void;
  setTotalPagesAccepted: (payload: number) => void;
  setTotalPagesDelivered: (payload: number) => void;
  setTotalPagesRefused: (payload: number) => void;
}

const vanillaPaginationStore = create<IPaginationStore<{}>>((set) => ({
  totalPagesValidated: 0,
  totalPagesAccepted: 0,
  totalPagesDelivered: 0,
  totalPagesRefused: 0,
  setTotalPagesValidated: (value) =>
    set((payload) => ({ totalPagesValidated: value })),
  setTotalPagesAccepted: (value) =>
    set((payload) => ({ totalPagesAccepted: value })),
  setTotalPagesDelivered: (value) =>
    set((payload) => ({ totalPagesDelivered: value })),
  setTotalPagesRefused: (value) =>
    set((payload) => ({ totalPagesRefused: value })),
}));

const paginationStore = vanillaPaginationStore as unknown as {
  <T>(): IPaginationStore<T>;
  <T, U>(selector: (s: IPaginationStore<T>) => U): U;
};

export { vanillaPaginationStore, paginationStore };
