/* eslint-disable */

import { IStep, ITournee } from "src/interfaces/delivery/tournees";
import { create } from "zustand";

interface ITourneeStore<T> {
  opened: ITournee;
  set_opened: (payload: ITournee) => void;
  should_reload: boolean;
  set_should_reload: (payload: boolean) => void;
  steps: IStep[];
  set_steps: (steps: IStep[]) => void;
}

const vanillaTourneeStore = create<ITourneeStore<{}>>((set, get) => ({
  opened: {} as ITournee,
  should_reload: false,
  steps: [],
  set_steps: (steps: IStep[]) => set(() => ({ steps })),
  set_should_reload: (payload) => set(() => ({ should_reload: payload })),
  set_opened: (payload) => set((state) => ({ opened: payload })),
}));

const tourneeStore = vanillaTourneeStore as {
  <T>(): ITourneeStore<T>;
  <T, U>(selector: (s: ITourneeStore<T>) => U): U;
};

export { vanillaTourneeStore, tourneeStore };
