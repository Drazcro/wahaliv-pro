/* eslint-disable */

import { create } from "zustand";

interface IBusyStore<T> {
  isBusy: boolean;
  setBusyState: (payload: boolean) => void;
}

const vanillaBusyStore = create<IBusyStore<{}>>((set) => ({
  isBusy: false,
  setBusyState: (newState) => set((payload) => ({ isBusy: newState })),
}));

const busyStore = vanillaBusyStore as {
  <T>(): IBusyStore<T>;
  <T, U>(selector: (s: IBusyStore<T>) => U): U;
};

export { vanillaBusyStore, busyStore };
