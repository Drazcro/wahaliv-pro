/* eslint-disable */

import { Audio, AVPlaybackStatus } from "expo-av";
import { create } from "zustand";

interface ISoundStore<T> {
  playSound: () => Promise<boolean>;
  stopSound: () => Promise<boolean>;
  sound: Audio.SoundObject | undefined;
}

const vanillaSoundStore = create<ISoundStore<{}>>((set, get) => ({
  sound: undefined,

  playSound: async () => {
    const sound: Audio.SoundObject | undefined = get().sound;
    // console.log("play sound");
    if (sound == undefined) {
      return Promise.resolve(true);
    }
    try {
      if (!sound.status.isLoaded) {
        await sound.sound.loadAsync(
          // eslint-disable-next-line @typescript-eslint/no-var-requires
          require("../../assets/notification.mp3"),
          { shouldPlay: true, isLooping: true },
        );
      }
      const playback_status = await sound.sound.playAsync();
      return Promise.resolve(playback_status.isLoaded);
    } catch (error) {
      console.error(error);
      return Promise.resolve(false);
    }
  },
  stopSound: async () => {
    const sound: Audio.SoundObject | undefined = get().sound;
    if (sound == undefined) {
      // console.log("sound is undefined");
      return Promise.resolve(true);
    }
    if (!sound.status.isLoaded) {
      return Promise.resolve(true);
    } else {
      try {
        const playback_status: AVPlaybackStatus = await sound.sound.stopAsync();
        return Promise.resolve(!playback_status.isLoaded);
      } catch (error) {
        console.error(error);
        return Promise.resolve(false);
      }
    }
  },
}));

Audio.setAudioModeAsync({
  staysActiveInBackground: true,
  shouldDuckAndroid: false,
  playThroughEarpieceAndroid: false,
  allowsRecordingIOS: false,
  playsInSilentModeIOS: true,
});
Audio.Sound.createAsync(
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  require("../../assets/notification.mp3"),
  { shouldPlay: false, isLooping: true },
  //{ shouldPlay: autoPlay === true, isLooping: true },
).then((res) => vanillaSoundStore.setState({ sound: res }));

const soundStore = vanillaSoundStore as unknown as {
  <T>(): ISoundStore<T>;
  <T, U>(selector: (s: ISoundStore<T>) => U): U;
};

export { vanillaSoundStore, soundStore };
