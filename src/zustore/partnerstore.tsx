/* eslint-disable */

import { create } from "zustand";

import { IOrder } from "src/interfaces/restaurant/order";
import { IPartnerProfile } from "src/interfaces/restaurant/profile";

interface IPartnerStore<T> {
  partnerProfile: IPartnerProfile;
  orderEnCours: IOrder;
  newOrders: IOrder[];
  acceptedOrders: IOrder[];
  deliveredOrders: IOrder[];
  cancelledOrders: IOrder[];
  orderInModal: IOrder;

  setPartnerProfile: (payload: IPartnerProfile) => void;
  setNewOrders: (payload: IOrder[]) => void;
  addOrderToNewOrders: (payload: IOrder) => void;
  resetNewOrders: () => void;
  setOrderEnCours: (payload: IOrder) => void;
  removeOrderEnCours: () => void;
  getNewOrderById: (id: number) => IOrder | null;
  updateCollecteHourByIndex: (index: number, newHour: string) => void;
  updateCollecteHourById: (id: number, newHour: string) => void;
  setOrderInModal: (payload: IOrder) => void;
}

const vanillaPartnerStore = create<IPartnerStore<{}>>((set, get) => ({
  partnerProfile: {
    partner: { name: "" },
  } as IPartnerProfile,
  orderEnCours: {} as IOrder,
  newOrders: [] as IOrder[],
  acceptedOrders: [] as IOrder[],
  cancelledOrders: [] as IOrder[],
  deliveredOrders: [] as IOrder[],
  orderInModal: {} as IOrder,

  setOrderInModal: (order) => set((state) => ({ orderInModal: order })),
  setPartnerProfile: (infos) => set((state) => ({ partnerProfile: infos })),
  setOrderEnCours: (order) => set((state) => ({ orderEnCours: order })),
  setNewOrders: (orders) => set((state) => ({ newOrders: orders })),
  resetNewOrders: () => set((state) => ({ newOrders: [] as IOrder[] })),
  addOrderToNewOrders: (order) =>
    set((state) => ({ newOrders: [...state.newOrders, order] })),
  removeOrderEnCours: () => set({ orderEnCours: {} as IOrder }),
  getNewOrderById: (id) => {
    const index = get().newOrders.findIndex((item) => item.commande.id === id);
    return index !== -1 ? get().newOrders[index] : null;
  },
  updateCollecteHourByIndex: (index, newHour) =>
    set((state) => {
      const orders = state.newOrders;
      if (index != undefined && orders[index] != undefined) {
        orders[index].commande.pickup_date.date = newHour;
      }

      return {
        newOrders: orders,
      };
    }),
  updateCollecteHourById: (id, newHour) =>
    set((state) => {
      const index = state.newOrders.findIndex(
        (item) => item.commande.id === id,
      );

      const orders = state.newOrders;
      if (index != undefined && orders[index] != undefined) {
        orders[index].commande.pickup_date.date = newHour;
      }

      return {
        newOrders: orders,
      };
    }),
}));

const partnerStore = vanillaPartnerStore as unknown as {
  <T>(): IPartnerStore<T>;
  <T, U>(selector: (s: IPartnerStore<T>) => U): U;
};

export { vanillaPartnerStore, partnerStore };
