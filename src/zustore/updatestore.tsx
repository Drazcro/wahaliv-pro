import { create } from "zustand";

type UpdateStore = {
  isChecked: boolean;
  setIsChecked: (value: boolean) => void;
};

const updateStore = create<UpdateStore>()((set) => ({
  isChecked: false,
  setIsChecked: (value) => set(() => ({ isChecked: value })),
}));

export default updateStore;