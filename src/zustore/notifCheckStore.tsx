import { create } from "zustand";

type notifCheckStore = {
  isChecked: boolean;
  setIsChecked: (value: boolean) => void;
};

const notifCheckStore = create<notifCheckStore>()((set) => ({
  isChecked: false,
  setIsChecked: (value) => set(() => ({ isChecked: value })),
}));

export default notifCheckStore;
