const imageSvg = {
  information: {
    ThumbsUp: require("assets//image_svg/ThumbsUp.svg"),
    Scooter: require("assets//image_svg/Scooter.svg"),
    Timer: require("assets//image_svg/Timer.svg"),
    Question: require("assets//image_svg/Question.svg"),
    Info: require("assets//image_svg/Info.svg"),
    User: require("assets//image_svg/User.svg"),
    Lock: require("assets//image_svg/Lock.svg"),
    SignOut: require("assets//image_svg/SignOut.svg"),
    CaretRight: require(" assets//image_svg/CaretRight.svg"),
  },
};

export default imageSvg;
