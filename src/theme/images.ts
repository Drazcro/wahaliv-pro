export const calendar = `<svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M21.9375 4.21875H5.0625C4.59651 4.21875 4.21875 4.59651 4.21875 5.0625V21.9375C4.21875 22.4035 4.59651 22.7812 5.0625 22.7812H21.9375C22.4035 22.7812 22.7812 22.4035 22.7812 21.9375V5.0625C22.7812 4.59651 22.4035 4.21875 21.9375 4.21875Z" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M18.5625 2.53125V5.90625" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M8.4375 2.53125V5.90625" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M4.21875 9.28125H22.7812" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M9.70312 13.4996H12.6563L10.9688 15.609C11.2462 15.609 11.5194 15.6774 11.7641 15.8082C12.0088 15.939 12.2175 16.1281 12.3717 16.3588C12.5259 16.5895 12.6208 16.8546 12.6481 17.1307C12.6753 17.4068 12.6341 17.6854 12.528 17.9418C12.4219 18.1982 12.2542 18.4245 12.0398 18.6006C11.8254 18.7767 11.5708 18.8972 11.2987 18.9514C11.0266 19.0057 10.7453 18.992 10.4798 18.9116C10.2142 18.8312 9.97255 18.6866 9.77624 18.4905" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M15.1875 14.7653L16.875 13.4997V18.9841" stroke="black" stroke-linecap="round" stroke-linejoin="round"/>
</svg>`;

export const check = `
<svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
<rect width="15" height="15" fill="#E5E5E5"/>
<g clip-path="url(#clip0_0:1)">
<rect width="375" height="812" transform="translate(-35 -482)" fill="white"/>
<path d="M10.7656 5.71875L6.41143 9.875L4.23438 7.79688" stroke="#40ADA2" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
<path d="M13.4375 0.96875H1.5625C1.23458 0.96875 0.96875 1.23458 0.96875 1.5625V13.4375C0.96875 13.7654 1.23458 14.0312 1.5625 14.0312H13.4375C13.7654 14.0312 14.0312 13.7654 14.0312 13.4375V1.5625C14.0312 1.23458 13.7654 0.96875 13.4375 0.96875Z" stroke="#777777" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
</g>
<defs>
<clipPath id="clip0_0:1">
<rect width="375" height="812" fill="white" transform="translate(-35 -482)"/>
</clipPath>
</defs>
</svg>`;

export const images = {
  Landing: {
    Background: require("assets/images/splash.png"),
    Cadre: require("assets//images/Rectangle.png"),
    Logo: require("assets//images/logo.png"),
    Ellipse: require("assets//images/header.png"),
    PicOne: require("assets//images/picOne.png"),
    Location: require("assets//images/LocationPermission.png"),
    Notification: require("assets//images/NotificationPermission.png"),
    HalfBlue: require("assets//images/half_blue.svg"),
  },
  Image: {
    Scooter: require("assets//images/scooter.png"),
    Saquette: require("assets//images/saquette.png"),
    Map: require("assets//images/Map.png"),
    MapGPS: require("assets//images/MapGPS.png"),
    MapIn: require("assets//images/MapPin.png"),
    MapOut: require("assets//images/MapBlack.png"),
    MapRedPin: require("assets//red_pin.svg"),
  },
  Icone: {
    //Carre: require('assets//icons/CaretRight.png'),
    EnvelopeSimple: require("assets//icons/EnvelopeSimple.png"),
    Lock: require("assets//icons/Lock.png"),
    User: require("assets//icons/User.png"),
    Flag: require("assets//icons/flag.png"),
    Check: require("assets//icons/check.png"),
    Uncheck: require("assets//icons/uncheck.png"),
    EyeClosed: require("assets//icons/EyeClosed.png"),
    Profil: require("assets//icons/User.png"),
    Calendar: require("assets//icons/Calendar.png"),
    Courses: require("assets//icons/course.png"),
    Order: require("assets//icons/order.png"),
    Home: require("assets//icons/House.png"),
    Like: require("assets//icons/ThumbsUp.png"),
    LikeOne: require("assets//icons/likeOne.png"),
    Timer: require("assets//icons/Timer.png"),
    CurrencyEur: require("assets//icons/CurrencyEur.png"),
    Truck: require("assets//icons/Truck.png"),
    MapPinLine: require("assets//icons/MapPinLine.png"),
    Path: require("assets//icons/Path.png"),
    Money: require("assets//icons/Money.png"),
    CaretRight: require("assets//icons/CaretRightList.png"),
    CaretLeft: require("assets//icons/CaretLeft.png"),
    SignOut: require("assets//icons/SignOut.png"),
    Storefront: require("assets//icons/Storefront.png"),
    Info: require("assets//icons/Info.png"),
    ClipboardText: require("assets//icons/ClipboardText.png"),
    CreditCard: require("assets//icons/CreditCard.png"),
    Receipt: require("assets//icons/Receipt.png"),
    Car: require("assets//icons/Car.png"),
    Bicycle: require("assets//icons/Bicycle.png"),
    Plus: require("assets//icons/Plus.png"),
    Download: require("assets//icons/Download.png"),
    Clock: require("assets//icons/clock.png"),
    Close: require("assets//icons/close_offer.png"),
    MapinRed: require("assets//icons/MapPinRed.png"),
    MapinGreen: require("assets//icons/MapPinGreen.png"),
    Line: require("assets//icons/line.png"),
    Phone: require("assets//icons/phone.png"),
    //One: require('assets//icons/lineOne.png'),
    //LineTwo: require('assets//icons/lineTwo.png'),
    CaretLeftOne: require("assets//icons/CaretLeftOne.png"),
    Scooters: require("assets//icons/scooter.png"),
    DotsThreeOutline: require("assets//icons/DotsThreeOutline.png"),
    Link: require("assets//icons/link.png"),
    Question: require("assets//icons/question.png"),
    user1: require("assets//icons/groom.png"),
    user2: require("assets//icons/man.png"),
    user3: require("assets//icons/man2.png"),
  },
  Coursier: {
    Home: {
      Cadre: require("assets//images/Rectangle527.png"),
    },
    profile: require("assets//images/profile.png"),
  },
  partner: {
    Commande: require("assets//iconsPartenaire/commande.png"),
    Current: require("assets//iconsPartenaire/current.png"),
    ThumbUp: require("assets//iconsPartenaire/thumbUp.png"),
    Timer: require("assets//iconsPartenaire/timer.png"),
  },
};
