import { StyleSheet } from "react-native";

/*
 * generated with https://ethercreative.github.io/react-native-shadow-generator/
 * to get the same shadow on both platforms
 */
export const shadow = StyleSheet.create({
  primary: {
    elevation: 5,
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowRadius: 4,
    // shadowOpacity: 0.25,
    shadowOffset: {
      width: 0,
      height: 4,
    },
  },
});
