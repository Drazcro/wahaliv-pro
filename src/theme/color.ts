export const color = {
  BLACK: {
    BG: "#252525",
    BUTTON: "#393939",
  },
  WHITE: "#FFFFFF",
  RED: "#000000",

  GREY: {
    BG: "#F7F8FC",
    BUTTON: "#C4C4C4",
    TEXT: "#696D75",
    LINE: "#E5E3E3",
    CHIP: "#F0F0F0",
    PRICE: "#8D8F92",
  },
  YELLOW: "#E9C852",
  GREEN: {
    CHECKBOX: "#40ADA2",
    CARDHEADER: "#40ADA2D1",
  },
  TAB: {
    ACTIVE: "#000000",
    INACTIVE: "#C4C4C4",
  },
  ACCENT: "#000000",
  SECOND: "#FFD801",
  TEXT: "#1D2A40",
};
