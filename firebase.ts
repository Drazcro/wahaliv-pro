// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import "firebase/auth";
import { getDatabase } from "firebase/database";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyDXtHdSG_izrIS-OLpHlXCqLqjz1kQUjwo",
  authDomain: "sacre-fernand-app.firebaseio.com",
  projectId: "sacre-fernand-app",
  storageBucket: "sacre-fernand-app.appspot.com",
  messagingSenderId: "685013361807",
  //databaseURL: "https://sacre-fernand-app.europe-west1.firebasedatabase.app/",

  databaseURL: "https://sacre-fernand-app-default-rtdb.firebaseio.com",
  appId: "1:685013361807:web:8c683fe5a9ddf716893cfa",
};

// Initialize Firebase
// console.log(firebaseConfig);
const app = initializeApp(firebaseConfig);
//const analytics = getAnalytics(app);
const db = getDatabase(app);

const auth = getAuth();

export { db, app, auth };
