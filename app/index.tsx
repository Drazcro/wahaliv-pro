import { usePathname, useRootNavigation, useRouter } from "expo-router";
import React from "react";
import { Image, Platform, StyleSheet, View } from "react-native";
import useCachedResources from "src/hooks/useCachedResources";
import { ACCOUNT_TYPE, authStore } from "src/services/auth_store";
import Constants from "expo-constants";

import { color, images } from "src/theme";
import {
  Unsubscribe,
  DatabaseReference,
  onValue,
  ref,
} from "firebase/database";
import { db } from "firebase";
import updateStore from "src/zustore/updatestore";
import { shouldUpdateFromStore } from "src/services/helpers";

export default function LoadingScreen() {
  const { auth, account_type } = authStore();
  const navigation = useRootNavigation();
  const router = useRouter();
  const path = usePathname();
  const { isChecked } = updateStore();

  const isLoadingComplete = useCachedResources();

  React.useEffect(() => {
    let unsubscribe: Unsubscribe;
    let db_ref: DatabaseReference;
    try {
      const OS = Platform.OS;

      db_ref = ref(db, `wahaliv/proAppVersions/${OS}`);

      unsubscribe = onValue(db_ref, async (snapshot) => {
        if (snapshot.exists() && snapshot.val()) {
          try {
            const updateVersion = snapshot.val();
            //const updateVersion = "1.3.92";
            const actualVersion = Constants.expoConfig?.version;
            console.log("#############");
            console.log(`${actualVersion} VS ${updateVersion}`);
            console.log("#############");
            if (shouldUpdateFromStore(updateVersion)) {
              if (!isChecked) {
                //console.log("DO the redirection");
                router.replace("/utils/version");
              }
            }
          } catch (error) {
            console.error(error);
          }
        }
      });
    } catch (error) {
      console.error(error);
    }

    if (!(navigation?.isReady() && isLoadingComplete)) return;

    if (!auth.access_token || account_type === ACCOUNT_TYPE.UNKNOWN) {
      router.replace("/auth/");
    } else if (account_type === ACCOUNT_TYPE.DELIVERY) {
      router.replace("/delivery");
    } else if (account_type === ACCOUNT_TYPE.PARTNER) {
      router.replace("/restaurant");
    } else {
      router.replace("/auth/");
    }

    return () => {
      console.log("Destroy layout");
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, [auth, account_type, path, router, isLoadingComplete, navigation]);

  return (
    <View style={style.main}>
      <Image
        source={images.Landing.Background}
        resizeMode="cover"
        style={style.container}
      />
    </View>
  );
}

const style = StyleSheet.create({
  main: {
    height: "100%",
    backgroundColor: color.ACCENT,
  },
  container: {
    width: "100%",
    height: "100%",
    marginTop: Platform.OS === "ios" ? 5.2 : 0,
  },
});
