import {
    Stack,
    Tabs
} from "expo-router";
import { fontFamily } from "src/theme/typography";


import React from "react";
import useNotification from "src/hooks/useNotification";

import CommandeIcon from "assets/image_svg/commandGrey.svg";
import CommandeRed from "assets/image_svg/commandRose.svg";
import UserIcon from "assets/image_svg/UserLite.svg";
import UserRed from "assets/image_svg/UserRed.svg";
import { color } from "src/theme";




export default function Layout() {


    const {push_token, notif} = useNotification()

    React.useEffect(()=>{
        console.log(push_token)
        console.log("PERMISSION NOTIF: ",notif.granted)
    }, [push_token])


    



    

    return (
        <React.Fragment>
            <Stack.Screen />
            
            <Tabs
        screenOptions={{
          headerShown: false,
          
        }}
        initialRouteName="home"
      >
        <Tabs.Screen
          name="home"
          options={{
            tabBarLabel: "Commandes",
            title: "Commandes",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              lineHeight: 20,
              marginTop: -6,
            },
            tabBarActiveTintColor: color.ACCENT,
            tabBarIcon: ({ focused }) => {
              if (focused) {
                return <CommandeRed />;
              } else {
                return <CommandeIcon />;
              }
            },
          }}
        />
        <Tabs.Screen
          name="profile"
          options={{
            tabBarLabel: "Details",
            title: "Details",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              marginTop: -6,
              lineHeight: 20,
            },
            tabBarActiveTintColor: color.ACCENT,

            tabBarIcon: ({ focused }) => {
              if (focused) {
                return <UserRed />;
              } else {
                return <UserIcon />;
              }
            },
          }}
        />
      </Tabs>
            
        </React.Fragment>
    );
}


