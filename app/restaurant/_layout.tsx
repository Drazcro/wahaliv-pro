import { Stack, usePathname, useRootNavigation, useRouter } from "expo-router"
import {
  Query,
  Unsubscribe,
  off,
  onValue,
  ref,
  serverTimestamp,
  set,
} from "firebase/database";
import React from "react"
import { Platform } from "react-native";
import { get_partner_profile_information_and_stats, save_device_info } from "src/api/restaurant/profile";
import { authStore, ACCOUNT_TYPE } from "src/services/auth_store";
import { db } from "firebase";
import { ROLE_TYPE } from "src/services/types";
import { partnerStore } from "src/zustore/partnerstore";
import { encode } from "base-64";
import useNotification from "src/hooks/useNotification";
import * as Device from "expo-device";
import moment from "moment";
import { PaperProvider } from "react-native-paper";
import { HeaderBack } from "src/components/router";
import Toast from "react-native-expo-popup-notifications";
import { toastConfig } from "src/components/toasts";
import NotificationServiceAlert from "src/components/modals/permissions/notification_service";
import * as Localization from "expo-localization";

import i18n from "i18n-js"
import {fr} from "src/localization/fr"
import { STATUS } from "src/constants/status";

i18n.translations = {
  fr: fr,
  en: fr,
};
// Set the locale once at the beginning of your app.
i18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
i18n.fallbacks = true;


export default function RootLayout(){
    const { auth, base_user, account_type } = authStore();
    const navigation = useRootNavigation();
    const router = useRouter();
    const path = usePathname();
    const partnerProfile = partnerStore((v) => v.partnerProfile);

    const { push_token, notif } = useNotification() 
  
    React.useEffect(() => {
      if (!(navigation?.isReady())) return;
      
      if (account_type === ACCOUNT_TYPE.UNKNOWN) {
        router.replace("/auth");
      } else if (account_type === ACCOUNT_TYPE.DELIVERY) {
        router.replace("/delivery");
      }
    }, [account_type, auth.access_token, auth.is_loading, path, router]);



    React.useEffect(() => {
      get_partner_profile_information_and_stats();
    }, [])

    React.useEffect(() => {
      save_device_info()
        .catch((error) => {
          console.error(error);
         
        });
    }, []);

    React.useEffect(() => {
      const OS = Platform.OS;
      const reference: Query = ref(db, `proAppVersions/${OS}`);
      onValue(reference, async (snapshot) => {
        
        if (snapshot.exists() && base_user) {
          //const value: string = snapshot.val();
          //const should_update = shouldUpdateFromStore(user.email, value);
          //set_out_of_date(should_update || base_user.is_latest_version === false);
        }
      });
  
      return () => {
        off(reference);
      };
    }, []);

    const getChatPath = React.useMemo(() => {
      if (!base_user.roles) {
        return "";
      }
  
      const formattedString = `${base_user.id}|-|;${base_user.email}|-|;${
        base_user.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0]
      }`;
  
      return encode(formattedString);
    }, [base_user]);


    const getFullName = React.useMemo(() => {
      const account = partnerProfile;
        return `${account?.partner?.name ?? ""}`;
    }, [partnerProfile, base_user]);
  


    React.useEffect(() => {
      
  
      //get_partner_profile_information_and_stats();
  
      const chat_ref = ref(db, "chat/" + getChatPath);
  
      onValue(chat_ref, async (snapshot) => {
        if (!snapshot.exists()) {
          console.log('Try to init it');
          const metaDataMessage = {
            email: base_user.email,
            full_name: `${getFullName}`,
            role: base_user.roles.filter(
              (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
            )[0],
            id: base_user.id,
            sent_by_user: false,
            content: "",
            sent_at: moment().format(),
            timestamp: serverTimestamp(),
            meta: "meta",
          };
          set(chat_ref, {
            initial: {
              content: "Bonjour",
              read: false,
              sent_by_user: false,
              sent_at: moment().format(),
              timestamp: serverTimestamp(),
            },
            chat_metadata: {
              email: base_user.email,
              full_name: `${getFullName}`,
              role: base_user.roles.filter(
                (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
              )[0],
              id: base_user.id,              
              marketplace : STATUS.MARKETPLACE,
              lastMessage: metaDataMessage,
              unread_for: {
                supervision: 0,
                user: 0,
              },
            },
          });
        }
      });
  
      return () => {
        off(chat_ref);
      };
    }, [base_user, partnerProfile]);
  
    React.useEffect(() => {
      let unsubscribe: Unsubscribe;
      if (!base_user || !base_user.id) {
        console.log("BASE USER MISSING");
      }
  
   
      try {
        const role =
          base_user.roles.filter(
            (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
          )[0] ?? "NO_ROLE";
        const decoded_token = (
          Device.isDevice || push_token.length > 1
            ? push_token
            : Device.designName ?? Device.deviceName ?? Device.osName ?? "noname"
        ).replace(" ", "");
  
        const token = encode(decoded_token);
  
        console.log("TOKEN: ", token);
  
     
  
    
  
        const db_ref = ref(db, `multi_device/${role}/${base_user.id}/${token}`);
  
        unsubscribe = onValue(db_ref, async (result) => {
          if (!result.exists()) {
            try {
              save_device_info().then((v) => console.log(v));
            } catch (error) {
              console.error(error);
            }
  
            console.log("Device case 1");
            set(db_ref, decoded_token);
          }
          
        });
      } catch (error) {
        console.error(error)
      }
  
      return () => {
        if (unsubscribe) {
          unsubscribe();
        }
      };
    }, [push_token, base_user]);
  
    
    
    
    return (
      <PaperProvider>
        <Stack  screenOptions={({route, navigation})=>{
          return {
            headerShown: false,
            statusBarStyle: Platform.OS =='android' ? 'dark' : undefined,
            headerLeft: HeaderBack
          }
        }}>
            <Stack.Screen name="tab" />
        </Stack>
        <Toast  autoHide config={toastConfig} onPress={()=>Toast.hide()} />
        {!notif.granted ? <NotificationServiceAlert />: null}
      </PaperProvider>
  )
}