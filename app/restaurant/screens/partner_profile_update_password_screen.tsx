import React, { useState } from "react";
import {
  ActivityIndicator,
  Keyboard,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Button } from "react-native-elements";
import { Input } from "@rneui/themed";
import { fontFamily } from "src/theme/typography";
import Lock from "assets/image_svg/Lock.svg";
import EyeClosed from "assets/image_svg/EyeClosed.svg";
import Eye from "assets/image_svg/Eye.svg";

import I18n from "i18n-js";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { Stack, router } from "expo-router";
import { confirm_password_validate, password_validate } from "src/services/helpers";
import { update_partner_password } from "src/api/restaurant/profile";
import popup from "src/components/toasts/popup";
import { color } from "src/theme";

export default function PartnerProfileUpdatePasswordScreen() {
  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showCurrentPassword, setCurrentShowPassword] = useState(false);
  const [showNewPassword, setNewShowPassword] = useState(false);
  const [showConfirmPassword, setConfirmShowPassword] = useState(false);
  const [error, setError] = useState(false);
  const [passError, setPassError] = useState("");
  const [confirmPassError, setConfirmPassError] = useState("");


  const isTablet = useDeviceType();

  const validPassword = () => {
    const err: string = password_validate(newPassword);
    if (err !== "") {
      setPassError(err);
    } else {
      setPassError("");
    }
    validConfirmPassword();
  };

  const validConfirmPassword = () => {
    const err: string = confirm_password_validate(newPassword, confirmPassword);
    if (err !== "") {
      setConfirmPassError(err);
    } else {
      setConfirmPassError("");
    }
  };

  const handleSubmit = () => {
    setShowLoadingButton(true);
    update_partner_password( {
      oldPassword,
      newPassword,
    })
      .then((response) => {
        setShowLoadingButton(true);
        if (response !== undefined) {
          popup.success({
            text: I18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          popup.error({
            
            text: I18n.t("profile.notification.error.message"),
          });
        }
      })
      .catch(() => {
        setError(true);
        setShowLoadingButton(false);
        popup.error({
          title: "Erreur",
          text: I18n.t("profile.notification.error.message"),
        });
      })
      .finally(() => setShowLoadingButton(false));
  };

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <Stack.Screen options={{
        title: I18n.t("profile.updatePassword.title")
      }} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <View style={styles.field_input}>
          <View style={styles.space_input}>
            <Input
              value={oldPassword}
              containerStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_input_border
                  : styles.input_border
              }
              inputContainerStyle={styles.input_style}
              placeholder={I18n.t("profile.updatePassword.enterPassword")}
              label={I18n.t("profile.updatePassword.currentPassword")}
              labelStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_label
                  : styles.text_label
              }
              leftIcon={<Lock />}
              placeholderTextColor="rgba(119, 119, 119, 1)"
              leftIconContainerStyle={{
                paddingLeft: isTablet && Device.brand != "SUNMI" ? 19.37 : 10,
                paddingRight: isTablet && Device.brand != "SUNMI" ? 15.38 : 10,
              }}
              rightIconContainerStyle={{
                paddingRight: isTablet && Device.brand != "SUNMI" ? 22.13 : 10,
              }}
              onChangeText={(text) => setOldPassword(text)}
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_value
                  : styles.text_value
              }
              secureTextEntry={showCurrentPassword === false}
              rightIcon={
                oldPassword ? (
                  showCurrentPassword === true ? (
                    <Eye onPress={() => setCurrentShowPassword(false)} />
                  ) : (
                    <EyeClosed onPress={() => setCurrentShowPassword(true)} />
                  )
                ) : undefined
              }
            />
          </View>
          <View style={styles.space_input}>
            <Input
              value={newPassword}
              containerStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_input_border
                  : styles.input_border
              }
              inputContainerStyle={styles.input_style}
              placeholder={I18n.t("profile.updatePassword.limitPassword")}
              label={I18n.t("profile.updatePassword.newPassword")}
              labelStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_label
                  : styles.text_label
              }
              leftIcon={<Lock />}
              leftIconContainerStyle={{
                paddingLeft: isTablet && Device.brand != "SUNMI" ? 19.37 : 10,
                paddingRight: isTablet && Device.brand != "SUNMI" ? 15.38 : 10,
              }}
              rightIconContainerStyle={{
                paddingRight: isTablet && Device.brand != "SUNMI" ? 17.13 : 10,
              }}
              onChangeText={(text) => setNewPassword(text)}
              onBlur={validPassword}
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_value
                  : styles.text_value
              }
              errorMessage={passError}
              secureTextEntry={showNewPassword === false}
              rightIcon={
                newPassword ? (
                  showNewPassword === true ? (
                    <Eye onPress={() => setNewShowPassword(false)} />
                  ) : (
                    <EyeClosed onPress={() => setNewShowPassword(true)} />
                  )
                ) : undefined
              }
            />
          </View>
          <View style={styles.space_input}>
            <Input
              value={confirmPassword}
              containerStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_input_border
                  : styles.input_border
              }
              inputContainerStyle={styles.input_style}
              placeholder={I18n.t("profile.updatePassword.limitPassword")}
              label={I18n.t("profile.updatePassword.confirmPassword")}
              labelStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_label
                  : styles.text_label
              }
              leftIcon={<Lock />}
              leftIconContainerStyle={{
                paddingLeft: isTablet && Device.brand != "SUNMI" ? 19.37 : 10,
                paddingRight: isTablet && Device.brand != "SUNMI" ? 15.38 : 10,
              }}
              rightIconContainerStyle={{
                paddingRight: isTablet && Device.brand != "SUNMI" ? 17.13 : 10,
              }}
              onChangeText={(text) => setConfirmPassword(text)}
              onBlur={validConfirmPassword}
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_text_value
                  : styles.text_value
              }
              secureTextEntry={showConfirmPassword === false}
              errorMessage={confirmPassError}
              rightIcon={
                confirmPassword ? (
                  showConfirmPassword === true ? (
                    <Eye onPress={() => setConfirmShowPassword(false)} />
                  ) : (
                    <EyeClosed onPress={() => setConfirmShowPassword(true)} />
                  )
                ) : undefined
              }
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? styles.tablet_container_button
            : styles.container_button
        }
      >
        {showLoadingButton ? (
          <View style={styles.loader_button}>
            <ActivityIndicator style={{ alignSelf: "center" }} color="white" />
          </View>
        ) : (
          <Button
            type="solid"
            title={I18n.t("form.button.validate")}
            titleStyle={styles.buttonTitle}
            buttonStyle={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_button
                : styles.button
            }
            onPress={handleSubmit}
            disabled={
              passError !== "" ||
              confirmPassError !== "" ||
              oldPassword === "" ||
              newPassword === "" ||
              confirmPassword === ""
            }
            disabledStyle={{
              backgroundColor: "#C4C4C4",
            }}
            disabledTitleStyle={{
              color: "#FDFDFD",
            }}
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  field_input: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
    paddingTop: 60,
    paddingBottom: 20,
  },
  space_input: {
    marginBottom: 38,
  },
  text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 9,
    lineHeight: 22,
    left: 10,
  },
  input_border: {
    minWidth: 324,
    height: 38,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  input_style: {
    borderRadius: 17,
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    marginBottom: 0,
    borderBottomWidth: undefined,
    height: 38,
  },
  container_button: {
    position: "absolute",
    alignSelf: "center",
    alignItems: "center",
    bottom: 20,
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  text_value: {
    fontFamily: fontFamily.regular,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 15,
    lineHeight: 22,
  },
  buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
  /** table screen */
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    lineHeight: 24.38,
    textAlign: "center",
  },
  tablet_input_border: {
    width: 670,
    height: 50,
  },
  tablet_button: {
    width: 355,
    height: 55,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  tablet_container_button: {
    alignSelf: "center",
    paddingBottom: 167,
  },
  tablet_text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
    left: 10,
    marginBottom: 14,
  },
  tablet_text_value: {
    fontFamily: fontFamily.regular,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
});
