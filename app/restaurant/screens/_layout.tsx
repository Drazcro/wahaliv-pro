import {  Stack } from "expo-router";
import { HeaderBack } from "src/components/router";

export default function Layout(){

    return (
        
        <Stack screenOptions={{
            headerShown: true,
            headerLeft: HeaderBack
        }} />
    )
}