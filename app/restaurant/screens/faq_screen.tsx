import { Stack } from "expo-router";
import React from "react";
import { StyleSheet, View } from "react-native";
import FaqComponent from "src/components/common/faq_component";
import { HeaderBack } from "src/components/router";

export default function Page() {
  
  return (
    <View style={styles.container}>
      <Stack.Screen options={{
        title: "Aide et contact",
        headerLeft: HeaderBack
      }} />
      <FaqComponent />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
  },
});
