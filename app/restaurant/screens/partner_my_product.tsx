import React, { useEffect, useState } from "react";
import { WebView } from "react-native-webview";
import { ActivityIndicator, Text, View } from "react-native";
import { DEVICE } from "src/constants/default_values";
import { Stack, router } from "expo-router";
import { http } from "src/services/http";
import { authStore, vanillaAuthState } from "src/services/auth_store";

export default function PartnerMyProductScreen() {
  const { base_user } = authStore();
  const URL = vanillaAuthState.getState().base_url;

  const [loading, setLoading] = useState(true);
  const [loader, setLoader] = useState(true);
  const [url, setUrl] = useState();

  useEffect(() => {
    const _url = `${URL}partenaire/${base_user.id}/link/produit`;
    http.get(_url).then((r) => {
      if (r?.status !== 200) {
        router.back();
      } else {
        setUrl(r.data.url);
        setLoading(false);
      }
    });
  }, [router]);

  if (loading || url == undefined) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>Chargement en cours</Text>
      </View>
    );
  }

  return (
    <React.Fragment>
      <Stack.Screen options={{
        title: "Mes produits"
      }} />
      <WebView he source={{ uri: url }} onLoad={() => setLoader(false)} />
      {loader && (
        <ActivityIndicator
          color="#1D2A40"
          style={{
            position: "absolute",
            top: DEVICE.height / 2,
            left: DEVICE.width / 2,
          }}
        />
      )}
    </React.Fragment>
  );
}
