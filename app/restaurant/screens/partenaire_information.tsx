import React from "react";
import {
  Platform,
  SafeAreaView,
  StyleSheet,
  View,
  useWindowDimensions,
} from "react-native";
import { Text } from "react-native-paper";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { fontFamily } from "src/theme/typography";

import Administratif from "src/fragments/restaurant/administratif";
import { PartenaireProfile } from "src/fragments/restaurant/profile";
import Univers from "src/fragments/restaurant/univers";
import Parameters from "src/fragments/restaurant/parametres";
import { Stack } from "expo-router";
import { color } from "src/theme";



const TOP_TABS = [
  { key: "first", title: "Profile" },
  { key: "second", title: "Administratif & justificatifs" },
  { key: "third", title: "Univers" },
  { key: "fourth", title: "Paramètres" },
];

const FirstRoute = React.memo(() => {
  return <PartenaireProfile />;
});

const SecondRoute = React.memo(() => {
  return <Administratif />;
});

const ThirdRoute = React.memo(() => {
  return <Univers />;
});

const FourthRoute = React.memo(() => {
  return <Parameters />;
});

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
  third: ThirdRoute,
  fourth: FourthRoute,
});

const partenaire_information = () => {
  const [tabIndex, setTabIndex] = React.useState(0);
  const layout = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <Stack.Screen options={{
        title: "Mes informations"
      }} />
      <TabView
        style={styles.tabView}
        navigationState={{ index: tabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <View>
              <TabBar
                {...props}
                indicatorStyle={styles.tabBarIndicatorStyle}
                style={styles.tabBarStyle}
                renderLabel={({ route, focused }) => (
                  <Text
                    style={
                      focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                    }
                  >
                    {route.title}
                  </Text>
                )}
              />
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
    </SafeAreaView>
  );
};

export default partenaire_information;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FCFAFA",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 50 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 12,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 12,
    textAlign: "center",
    marginBottom: -10,
  },
  reminderContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    width: 318,
    height: 341,
  },
});
