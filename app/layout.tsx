import { Slot} from "expo-router";
import React from "react";
import I18n from "i18n-js";
import { fr } from "src/localization/fr";
import * as Localization from "expo-localization";

import { SafeAreaProvider } from "react-native-safe-area-context";
import { registerForBGNotifications } from "src/cron/notifications";
//import { register_toast } from "react-native-expo-popup-notifications";



registerForBGNotifications()
//register_toast()






I18n.translations = {
  fr: fr,
  en: fr,
};
// Set the locale once at the beginning of your app.
I18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
I18n.fallbacks = true;


export default function RootLayout() {


  return (
    <React.Fragment>
        <SafeAreaProvider>

        <Slot />
        </SafeAreaProvider>
    </React.Fragment>
  )
}
