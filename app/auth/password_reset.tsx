import React, { useState } from "react";
import { View, Text, ActivityIndicator } from "react-native";
import { styles } from "../../src/styles/auth/password_reset.styles";
import EnvelopeSimple from "assets//image_svg/EnvelopeSimple.svg";
import { Input } from "@rneui/themed";

import i18n from "i18n-js";
import { color } from "src/theme/color";
import { Button } from "react-native-elements";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { Stack, router } from "expo-router";
import { send_password_reset_email } from "src/api/auth";
import { StatusBar } from "expo-status-bar";
import Toast from "react-native-expo-popup-notifications";
import { toastConfig } from "src/components/toasts";
import popup from "src/components/toasts/popup";

export default function PasswordForgot() {
  // STATE
  const [email, setEmail] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const isTablet = useDeviceType();

  // Actions
  const sendEmail = () => {
   
   
    setIsLoading(true);
    
    send_password_reset_email(email)
      .then(() => {
        router.push({ pathname: "/email_sent", params: { email } });
      })
      .catch(e => {
        popup.error({text:"veuillez réessayer plus tard", title: "Erreur"})
      })
      .finally(() => { 
        setIsLoading(false);
      })
  };

  return (
    <View style={styles.container}>
       

      <View style={styles.main}>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tabletContent
              : styles.content
          }
        >
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tabletText
                : styles.text
            }
          >
            {isTablet && Device.brand != "SUNMI" ? (
              <Text style={styles.tabletTextPass}>
                Saisis ton adresse email et nous t'aiderons à réinitialiser ton
                mot de passe
              </Text>
            ) : (
              <Text style={styles.textPass}>
                Saisis ton adresse email et nous{"\n"}t'aiderons à réinitialiser
                ton mot{"\n"}de passe{" "}
              </Text>
            )}
          </View>
        </View>
        <Input
          containerStyle={
            isTablet && Device.brand != "SUNMI"
              ? styles.tabletInputContainer
              : styles.inputContainer
          }
          inputContainerStyle={{
            borderBottomColor: "rgba(29, 42, 64, 0.2)",
          }}
          textContentType="emailAddress"
          placeholder={i18n.t("signIn.email")}
          value={email}
          onChangeText={(text) => setEmail(text.toLocaleLowerCase())}
          leftIcon={<EnvelopeSimple />}
          returnKeyLabel="terminé"
        />

        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tabletButton
              : styles.button
          }
        >
          <Button
            onPress={sendEmail}
            type="solid"
            title={
              isLoading === true ? (
                <ActivityIndicator color={color.RED} />
              ) : (
                "Envoyer le lien"
              )
            }
            disabled={isLoading || !email}
            disabledStyle={{ backgroundColor: color.GREY.BUTTON }}
            disabledTitleStyle={{ color: color.WHITE }}
            containerStyle={
              isTablet && Device.brand != "SUNMI"
                ? styles.tabletButtonContainerStyle
                : styles.buttonContainerStyle
            }
            titleStyle={styles.buttonTitleStyle}
            buttonStyle={styles.buttonStyle}
          />
        </View>
      </View>
    </View>
  );
}
