import { Stack, usePathname, useRootNavigation, useRouter} from "expo-router";
import React from "react";
import { Platform, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";
import i18n from "i18n-js";
import { HeaderBack } from "src/components/router";
import Toast from "react-native-expo-popup-notifications";
import { toastConfig } from "src/components/toasts";
import { ACCOUNT_TYPE, authStore } from "src/services/auth_store";

export default function AuthLayout() {
  
  const { auth, account_type } = authStore();
  const navigation = useRootNavigation();
  const router = useRouter();
  const path = usePathname();

  React.useEffect(() => {
    if (!(navigation?.isReady())) return;
    
    if (account_type === ACCOUNT_TYPE.DELIVERY) {
      router.replace("/delivery");
    } else if (account_type === ACCOUNT_TYPE.PARTNER) {
      router.replace("/restaurant");
    }
  }, [auth, account_type, auth.access_token, auth.is_loading, path, router]);

  return (
    <>
      <Stack screenOptions={{ 
        headerTitleStyle: styles.text_title,
        // statusBarAnimation: 'none',
        // statusBarTranslucent: true,
        // statusBarColor: 'transparent',
        statusBarStyle: Platform.OS =='android' ? 'dark' : undefined,
        headerLeft: ()=><HeaderBack />
        }} >
        
        <Stack.Screen name="index" options={{
          headerShown: false,
          title: "Connexion"
        }} />
        <Stack.Screen name="password_reset" options={{
          headerShown: true,
          title: i18n.t("signIn.forgetPasswordTitle"),
          //presentation: Platform.OS == 'ios' ? 'modal' : 'card'
        }} />
        <Stack.Screen 
          name="email_sent" 
          options={{
            headerLeft: undefined,
            title: i18n.t("signIn.consulte"),
            //presentation: Platform.OS == 'ios' ? 'modal' : 'card'
          }} 
        
          />
      </Stack>
      <Toast autoHide config={toastConfig} />
    </>
  );
}


const styles = StyleSheet.create({
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    // lineHeight: 24.38,
    // marginRight: 20,
    //textAlign: "center",
  },
})