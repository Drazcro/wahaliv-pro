import React, { useState } from "react";
import { View, Text, ActivityIndicator, TouchableOpacity } from "react-native";

import { styles } from "src/styles/auth/password_reset.styles";

import { color } from "src/theme/color";
import { Button } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import { router, useLocalSearchParams } from "expo-router";
import { send_password_reset_email } from "src/api/auth";
import popup from "src/components/toasts/popup";

export default function EmailSentScreen() {
  const { email } = useLocalSearchParams<{ email: string }>();

  // STATE
  // const [email] = useState(emailParam);
  const [isLoading, setIsLoading] = useState(false);

  // Actions
  const sendEmail = () => {
    setIsLoading(true);
    send_password_reset_email(email ?? "")
      .then(() => {
        setIsLoading(false);
        router.push("/auth/index");
      })
      .catch(e => {
        popup.error({text:"veuillez réessayer plus tard", title: "Erreur"})
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  React.useEffect(()=>{
    popup.success({
      text: "Un e-mail vous a été envoyé"
    })
  }, [])
  return (
    <View style={styles.container}>
      
      
      <View style={styles.main}>
        <View style={styles.content}>
          <View style={styles.text}>
            <Text style={styles.textPass}>
              Nous avons envoyé les instrutions de {"\n"}
              réinitialisation du mot de passe à
            </Text>

            <Text style={styles.textMail}>{email}</Text>

            <Text style={styles.textPass}>
              Si tu ne les trouves pas, vérifie dans ton {"\n"}
              courrier indésirable.
            </Text>
          </View>
        </View>

        <View style={styles.button}>
          <Button
            onPress={() => router.push("/sign_in")}
            type="solid"
            title="D’accord"
            containerStyle={styles.buttonContainerStyle}
            titleStyle={styles.buttonTitleStyle}
            buttonStyle={styles.buttonStyle}
          />
          <View
            style={{
              marginTop: 26,
              marginBottom: 45,
              height: 56,
              justifyContent: "space-between",
            }}
          >
            <Text style={styles.textPass}>Tu n’as pas reçu d’e-mail ?</Text>
            {!isLoading ? (
              <TouchableOpacity onPress={sendEmail}>
                <Text
                  style={{
                    ...styles.textPass,
                    color: color.RED,
                    fontFamily: fontFamily.Bold,
                  }}
                >
                  Réessayer
                </Text>
              </TouchableOpacity>
            ) : (
              <ActivityIndicator color={color.RED} />
            )}
          </View>
        </View>
      </View>
    </View>
  );
}
