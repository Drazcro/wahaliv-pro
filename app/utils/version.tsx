import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import UpdateShip from "assets/image_svg/updateShip.svg";
import { color } from "src/theme/color";
import { APP_STORE_APP_URL, PLAY_STORE_APP_BASE_URL } from "src/constants";
import Constants from "expo-constants";
import * as Linking from "expo-linking";
import updateStore from "src/zustore/updatestore";
import { useRouter } from "expo-router";

export default function VersionMessage() {
  const { setIsChecked } = updateStore();
  const router = useRouter();

  const makeUpdate = () => {
    try {
      setIsChecked(true);
      const appStoreLink: string =
        Platform.OS === "ios" ? APP_STORE_APP_URL : PLAY_STORE_APP_BASE_URL;

      console.log(appStoreLink);
      Linking.openURL(appStoreLink);
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error);
    }
  };

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <View style={styles.imgContainer}>
          <UpdateShip />
        </View>
        <View>
          <Text style={styles.title}>Nouvelle version disponible !</Text>
        </View>
        <View style={styles.descriptionContainer}>
          <Text style={styles.description}>
            Pour continuer à profiter de notre application, veuillez effectuer
            la mise à jour dès maintenant afin de profiter de nos nouvelles
            fonctionnalités
          </Text>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.buttonPosition} onPress={makeUpdate}>
          <View style={[styles.button, { backgroundColor: color.ACCENT }]}>
            <Text style={[styles.textButton, { color: "white" }]}>
              Mettre à jour maintenant
            </Text>
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.buttonPosition}
          onPress={() => {
            setIsChecked(true);
            router.replace('/');
          }}
        >
          <View
            style={[
              styles.button,
              { backgroundColor: "rgba(255, 255, 255, 1)" },
            ]}
          >
            <Text style={[styles.textButton, { color: color.ACCENT }]}>
              Mettre à jour plus tard
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  textContainer: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
  },
  buttonContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: color.TEXT,
  },
  descriptionContainer: {
    marginTop: 20,
  },
  description: {
    marginHorizontal: 30,
    textAlign: "center",
    fontFamily: fontFamily.Medium,
    lineHeight: 24,
    fontSize: 16,
    color: color.TEXT,
  },
  imgContainer: {
    marginBottom: 40,
    marginTop: "20%",
  },
  buttonPosition: {
    alignItems: "center",
    marginTop: 15,
  },
  button: {
    borderRadius: 30,
    width: 308,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
  },
  textButton: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
  },
});
