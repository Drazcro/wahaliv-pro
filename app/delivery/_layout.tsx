import {
  Stack,
  useFocusEffect,
  usePathname,
  useRootNavigation,
  useRouter,
} from "expo-router";
import {
  DatabaseReference,
  Unsubscribe,
  off,
  onValue,
  ref,
  remove,
  set,
} from "firebase/database";
import React from "react";
import { Platform } from "react-native";
import { PaperProvider } from "react-native-paper";
import Toast from "react-native-expo-popup-notifications";
import { HeaderBack } from "src/components/router";
import { toastConfig } from "src/components/toasts";
import { authStore, ACCOUNT_TYPE } from "src/services/auth_store";
import { ROLE_TYPE } from "src/services/types";
import * as Device from "expo-device";
import useNotification from "src/hooks/useNotification";
import { encode } from "base-64";
import { db } from "firebase";
import { save_device_info } from "src/api/delivery/profile";
import { EventRegister } from "react-native-event-listeners";
// const NO_HEADER_PATHS = [
//   "delivery/tournee",
//   "delivery/courses",
//   "delivery/courses",
// ]
export default function RootLayout() {
  const { auth, account_type, base_user } = authStore();
  const { push_token } = useNotification();
  const navigation = useRootNavigation();
  const router = useRouter();
  const path = usePathname();

  React.useEffect(() => {
    if (!navigation?.isReady()) return;

    if (account_type === ACCOUNT_TYPE.UNKNOWN) {
      router.replace("/auth");
    } else if (account_type === ACCOUNT_TYPE.PARTNER) {
      router.replace("/restaurant");
    }
  }, [auth, account_type, auth.access_token, auth.is_loading, path, router]);

  React.useEffect(() => {
    let unsubscribe: Unsubscribe;
    if (!base_user || !base_user.id) {
      console.log("BASE USER MISSING");
    }

    try {
      const role =
        base_user.roles.filter(
          (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
        )[0] ?? "NO_ROLE";
      const decoded_token = (
        Device.isDevice || push_token.length > 1
          ? push_token
          : Device.designName ?? Device.deviceName ?? Device.osName ?? "noname"
      ).replace(" ", "");

      const token = encode(decoded_token);

      console.log("TOKEN: ", token);

      const db_ref = ref(db, `multi_device/${role}/${base_user.id}/${token}`);

      unsubscribe = onValue(db_ref, async (result) => {
        if (!result.exists()) {
          try {
            save_device_info().then((v) => console.log(v));
          } catch (error) {
            console.error(error);
          }

          set(db_ref, decoded_token);
        }
      });
    } catch (error) {
      console.error(error);
    }

    return () => {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, [push_token, base_user]);

  const moveFocus = () => {
    console.log(path,router && path != "/delivery/tab/courses");
    if (router && path != "/delivery/tab/courses") {
      router.push("/delivery/tab/courses");
      console.log('move the tab');
    }
  };

  useFocusEffect(() => {
    const idUser = base_user.id;
    if (idUser == undefined) return;
    const token = push_token.replace("ExponentPushToken[", "").replace("]", "");
    const reference = ref(db, `actualisations/${idUser}/${token}`);
    onValue(reference, async (snapshot) => {
      console.log("firebase course update.",snapshot.exists() && snapshot.val(), snapshot.val());
      if (snapshot.exists() && snapshot.val() ) {
        try {
          remove(reference as DatabaseReference);
          moveFocus();
          EventRegister.emitEvent("COURSE_SHOULD_RELOAD", { firebase: true });
          //playAlertSound();
        } catch (e) {
          console.error(e);
        }
      }
    });
    return () => {
      off(reference);
    } 
  });

  return (
    <PaperProvider>
      <Stack
        screenOptions={({ route, navigation }) => {
          return {
            headerShown: false,
            statusBarStyle: Platform.OS == "android" ? "dark" : undefined,
            headerLeft: HeaderBack,
          };
        }}
      >
        <Stack.Screen name="tab" />
      </Stack>
      <Toast autoHide config={toastConfig} onPress={() => Toast.hide()} />
    </PaperProvider>
  );
}
