import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  Platform,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import { Input } from "@rneui/themed";
import { Button } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import RNPickerSelect from "react-native-picker-select";
import MaskInput from "react-native-mask-input";
import PhoneInput from "react-native-phone-number-input";
import { Entypo } from "@expo/vector-icons";
import i18n from "i18n-js";

import { birthdayMask, siretMask } from "src/constants/input_validation";
import Toast from "react-native-expo-popup-notifications";
import { fontFamily } from "src/theme/typography";
import CaretDown from "assets/image_svg/CaretDown.svg";
import { Stack, router } from "expo-router";
import {
  validate_dob,
  validate_email,
  validate_phone_number,
  validate_siret,
  french_to_api_date,
} from "src/services/helpers";
import { IDeliveryProfileInformation } from "src/interfaces/delivery/profile";
import {
  get_delivery_information_for_profile,
  update_delivery_information,
} from "src/api/delivery/profile";
import { color } from "src/theme";

interface IResetField {
  field: string;
  setField: () => void;
}

const ResetField = (props: IResetField) => {
  return (
    <>
      {props?.field?.length > 0 && (
        <View style={styles.reset_field}>
          <TouchableOpacity style={styles.icon_right} onPress={props.setField}>
            <Entypo name="cross" size={12} color="white" />
          </TouchableOpacity>
        </View>
      )}
    </>
  );
};

export default function InformationProfilScreen() {
  const [civility, setCivility] = useState("");
  const [name, setName] = useState<string | undefined>("");
  const [socialReason, setSocialReason] = useState<string | undefined>("");
  const [email, setEmail] = useState("");
  const [birthday, setBirthday] = useState("");
  const [siret, setSiret] = useState("");
  const [telephone, setTelephone] = useState("");
  const [address, setAddress] = useState("");

  const [resultAddress, setResultAddress] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [showLoadingButton, setShowLoadingButton] = useState(false);

  const [errorEmail, setErrorEmail] = useState<string>();
  const [errorPhone, setErrorPhone] = useState<string>();
  const [errorSiret, setErrorSiret] = useState<string>();
  const [errorDob, setErrorDob] = useState<string>();

  const [focusName, setFocusName] = useState(false);
  const [focusSocial, setFocusSocial] = useState(false);
  const [focusSiret, setFocusSiret] = useState(false);
  const [focusEmail, setFocusEmail] = useState(false);
  const [focusBirthday, setFocusBirthday] = useState(false);
  const [focusAddress, setFocusAddress] = useState(false);

  const [error, setError] = useState(false);

  const disabledButton =
    (errorEmail !== undefined && errorEmail !== "") ||
    (errorPhone !== undefined && errorPhone !== "") ||
    (errorSiret !== undefined && errorSiret !== "") ||
    (errorDob !== undefined && errorDob !== "");

  const handleSubmit = () => {
    setShowLoadingButton(true);
    
    let formatted = french_to_api_date(birthday);
    
    update_delivery_information({
      civility,
      name,
      company_name: socialReason,
      email,
      dob: formatted,
      siret,
      telephone: "+33" + telephone,
      address,
    })
      .then((response) => {
        get_delivery_information_for_profile();
        setShowLoadingButton(false);
        if (response !== undefined) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: i18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: i18n.t("profile.notification.error.message"),
          });
        }
      })
      .catch((e) => {
        //setError(true);
        setShowLoadingButton(false);
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: i18n.t("profile.notification.error.message"),
        });
      })
      .finally(() => {
        setIsLoading(false);
        setShowLoadingButton(false);
      });
  };

  const getAddressApi = (text: string) => {
    setAddress(text);
    fetch(
      "https://wxs.ign.fr/essentiels/ols/apis/completion?text=" +
        text +
        "&type=StreetAddress&maximumResponses=10",
    )
      .then((datas) => datas.json())
      .then((items) => setResultAddress(items.results));
  };

  const factoringPhoneNumber = (num: string) => {
    const numberDeletedSpace = num.replace(/\s/g, "");
    return numberDeletedSpace.replace("+33", "");
  };

  const onValidateEmail = (email: string) => {
    setEmail(email);
    const err = validate_email(email);
    if (err !== "") {
      setErrorEmail(err);
    } else {
      setErrorEmail("");
    }
  };

  const onValidatePhone = (num: string) => {
    setTelephone(factoringPhoneNumber(num));
    const err = validate_phone_number(num);
    if (err !== "") {
      setErrorPhone(err);
    } else {
      setErrorPhone("");
    }
  };

  const validateActionSiret = () => {
    const err: string = validate_siret(siret);
    if (err !== "") {
      setErrorSiret(err);
    } else {
      setErrorSiret("");
    }
  };

  const onValidateBirthday = () => {
    const err = validate_dob(birthday);
    if (err !== "") {
      setErrorDob(err);
    } else {
      setErrorDob("");
    }
  };

  const setAdressValue = (adress: string) => {
    const arrayOfAdress = adress.split(",", 2);
    return {
      first: arrayOfAdress[0],
      second: arrayOfAdress[1],
    };
  };

  useEffect(() => {
    get_delivery_information_for_profile()
      .then((items: IDeliveryProfileInformation) => {
        //console.log(items);
        setCivility(items.civility);
        setName(items.name);
        setSocialReason(items.company_name);
        setEmail(items.email);
        setBirthday(items.dob);
        setSiret(items.siret);
        setTelephone(factoringPhoneNumber(items.telephone));
        setAddress(items.address);
      })
      .catch(() => setError(true))
      .finally(() => setIsLoading(false));
  }, []);

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  if (error === true) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <Stack.Screen
        options={{
          title: i18n.t("profile.userInfo"),
        }}
      />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.field_input}>
            <Text style={styles.civility_label}>
              {i18n.t("profile.information.civility")}
            </Text>
            <View>
              <RNPickerSelect
                useNativeAndroidPickerStyle={false}
                placeholder={{}}
                onValueChange={(value) => setCivility(value)}
                value={civility}
                Icon={() => (
                  <View style={{ right: 15 }}>
                    <CaretDown />
                  </View>
                )}
                style={{
                  inputAndroidContainer: styles.picker_input_container,
                  inputIOSContainer: styles.picker_input_container,
                  inputIOS: styles.picker_input,
                  inputAndroid: styles.picker_input,
                }}
                items={[
                  {
                    label: i18n.t("form.select.civilities.men"),
                    value: i18n.t("form.select.civilities.men"),
                  },
                  {
                    label: i18n.t("form.select.civilities.women"),
                    value: i18n.t("form.select.civilities.women"),
                  },
                ]}
              />
            </View>
            <Text style={styles.text_label}>
              {i18n.t("profile.information.name")}
            </Text>
            <View>
              <Input
                autoCapitalize={"none"}
                value={name}
                onChangeText={(text) => setName(text)}
                inputContainerStyle={styles.input_style}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                  paddingLeft: 12,
                }}
                onFocus={() => {
                  setFocusName(true);
                  setFocusSocial(false);
                  setFocusSiret(false);
                  setFocusEmail(false);
                  setFocusBirthday(false);
                  setFocusAddress(false);
                }}
                rightIcon={
                  focusName ? (
                    <TouchableOpacity
                      style={styles.icon_right}
                      onPress={() => setName("")}
                    >
                      <Entypo name="cross" size={12} color="white" />
                    </TouchableOpacity>
                  ) : undefined
                }
                rightIconContainerStyle={{ paddingRight: 12 }}
              />
            </View>

            <Text style={[styles.text_label, { marginTop: -22 }]}>
              {i18n.t("profile.information.socialReason")}
            </Text>
            <View>
              <Input
                autoCapitalize={"none"}
                value={socialReason}
                onChangeText={(text) => setSocialReason(text)}
                inputContainerStyle={styles.input_style}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                  paddingLeft: 12,
                }}
                onFocus={() => {
                  setFocusName(false);
                  setFocusSocial(true);
                  setFocusSiret(false);
                  setFocusEmail(false);
                  setFocusBirthday(false);
                  setFocusAddress(false);
                }}
                rightIcon={
                  focusSocial ? (
                    <TouchableOpacity
                      style={styles.icon_right}
                      onPress={() => setSocialReason("")}
                    >
                      <Entypo name="cross" size={12} color="white" />
                    </TouchableOpacity>
                  ) : undefined
                }
                rightIconContainerStyle={{ paddingRight: 10 }}
              />
            </View>
            <Text style={[styles.text_label, { marginTop: -22 }]}>
              {i18n.t("profile.information.email")}
            </Text>
            <View>
              <Input
                autoCapitalize={"none"}
                value={email}
                keyboardType="numeric"
                onChangeText={(text) => onValidateEmail(text)}
                inputContainerStyle={styles.input_style}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                  paddingLeft: 12,
                }}
                onFocus={() => {
                  setFocusName(false);
                  setFocusSocial(false);
                  setFocusSiret(false);
                  setFocusEmail(true);
                  setFocusBirthday(false);
                  setFocusAddress(false);
                }}
                rightIcon={
                  focusEmail ? (
                    <TouchableOpacity
                      style={styles.icon_right}
                      onPress={() => onValidateEmail("")}
                    >
                      <Entypo name="cross" size={12} color="white" />
                    </TouchableOpacity>
                  ) : undefined
                }
                rightIconContainerStyle={{ paddingRight: 10 }}
                errorMessage={
                  errorEmail !== undefined ||
                  errorEmail !== null ||
                  errorEmail !== ""
                    ? errorEmail
                    : ""
                }
                errorStyle={{
                  color: "red",
                  fontSize: 9,
                  paddingLeft: 5,
                  marginTop: 0,
                  marginBottom: 20,
                }}
              />
            </View>
            <Text style={[styles.text_label, { marginTop: -22 }]}>
              {i18n.t("profile.information.birthday")}
            </Text>
            <View>
              <MaskInput
                mask={birthdayMask}
                keyboardType="numeric"
                value={birthday}
                onFocus={() => {
                  setFocusName(false);
                  setFocusSocial(false);
                  setFocusSiret(false);
                  setFocusEmail(false);
                  setFocusBirthday(true);
                  setFocusAddress(false);
                }}
                onBlur={() => onValidateBirthday()}
                onChangeText={(text) => {
                  setBirthday(text);
                }}
                style={styles.input_border}
              />
              {focusBirthday ? (
                <ResetField field={birthday} setField={() => setBirthday("")} />
              ) : undefined}
              {(errorDob !== undefined ||
                errorDob !== null ||
                errorDob !== "") && (
                <Text style={{ color: "red", fontSize: 9, paddingLeft: 12 }}>
                  {errorDob}
                </Text>
              )}
            </View>
            <Text style={styles.text_label}>
              {i18n.t("profile.information.siren")}
            </Text>
            <View>
              <MaskInput
                maxLength={14}
                mask={siretMask}
                placeholderFillCharacter={"x"}
                keyboardType="numeric"
                value={siret}
                onFocus={() => {
                  setFocusName(false);
                  setFocusSocial(false);
                  setFocusSiret(true);
                  setFocusEmail(false);
                  setFocusBirthday(false);
                  setFocusAddress(false);
                }}
                onBlur={() => validateActionSiret()}
                onChangeText={(text) => {
                  setSiret(text);
                }}
                style={styles.input_border}
              />
              {focusSiret ? (
                <ResetField field={siret} setField={() => setSiret("")} />
              ) : undefined}
              {(errorSiret !== undefined ||
                errorSiret !== null ||
                errorSiret !== "") && (
                <Text style={{ color: "red", fontSize: 9, paddingLeft: 12 }}>
                  {errorSiret}
                </Text>
              )}
            </View>
            <Text style={styles.text_label}>
              {i18n.t("profile.information.phoneNumber")}
            </Text>
            <View>
              <PhoneInput
                value={telephone}
                defaultCode="FR"
                layout="first"
                onChangeFormattedText={(text: string) => onValidatePhone(text)}
                placeholder={" "}
                flagButtonStyle={{
                  marginLeft: 8,
                }}
                containerStyle={{
                  width: 310,
                  height: 38,
                  backgroundColor: "#fff",
                  borderWidth: 0.5,
                  borderColor: "rgba(196, 196, 196, 1)",
                  borderRadius: 17,
                  shadowColor: "fff",
                  shadowOffset: {
                    width: 0,
                    height: 0,
                  },
                  shadowOpacity: 0,
                  shadowRadius: 0,
                  elevation: 0,
                }}
                textContainerStyle={{
                  backgroundColor: "fff",
                  paddingHorizontal: 0,
                  paddingVertical: 0,
                }}
                textInputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                }}
                codeTextStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                  marginRight: 0,
                }}
                withDarkTheme
                withShadow
              />
              {(errorPhone !== undefined ||
                errorPhone !== null ||
                errorPhone !== "") && (
                <Text style={{ color: "red", fontSize: 9, paddingLeft: 12 }}>
                  {errorPhone}
                </Text>
              )}
            </View>
            <Text style={styles.text_label}>
              {i18n.t("profile.information.postalCode")}
            </Text>
            <View>
              <Input
                value={address}
                onChangeText={(text) => getAddressApi(text)}
                inputContainerStyle={styles.input_style}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                  paddingLeft: 12,
                }}
                onFocus={() => {
                  setFocusName(false);
                  setFocusSocial(false);
                  setFocusSiret(false);
                  setFocusEmail(false);
                  setFocusBirthday(false);
                  setFocusAddress(true);
                }}
                rightIcon={
                  focusAddress ? (
                    <TouchableOpacity
                      style={styles.icon_right}
                      onPress={() => setAddress("")}
                    >
                      <Entypo name="cross" size={12} color="white" />
                    </TouchableOpacity>
                  ) : undefined
                }
                rightIconContainerStyle={{ paddingRight: 10 }}
              />
              {resultAddress.length > 0 && (
                <View style={styles.list_address}>
                  {resultAddress?.map(
                    (item: { fulltext: string }, index: number) => (
                      <TouchableOpacity
                        onPress={() => {
                          setResultAddress([]);
                          setAddress(item.fulltext);
                        }}
                        key={index}
                        style={{
                          borderBottomColor: "rgba(218, 218, 218, 0.46)",
                          borderBottomWidth: 1,
                        }}
                      >
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center",
                            padding: 10,
                          }}
                        >
                          <Text style={styles.text_list}>
                            {setAdressValue(item.fulltext).first},
                          </Text>
                          <Text style={styles.text_list_bold}>
                            {setAdressValue(item.fulltext).second}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    ),
                  )}
                </View>
              )}
            </View>
          </View>
          <View style={styles.container_button}>
            {showLoadingButton ? (
              <View style={styles.loader_button}>
                <ActivityIndicator
                  style={{ alignSelf: "center" }}
                  color="white"
                />
              </View>
            ) : (
              <Button
                type="solid"
                title={i18n.t("form.button.saveEdit")}
                titleStyle={styles.text_button}
                buttonStyle={styles.button}
                onPress={handleSubmit}
                disabled={disabledButton}
              />
            )}
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  input_border: {
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    borderRadius: 17,
    paddingLeft: 23,
    width: 310,
    height: 38,
    fontFamily: fontFamily.regular,
    fontSize: 14,
  },
  input_style: {
    borderWidth: 0.5,
    borderBottomWidth: undefined,
    borderColor: "rgba(196, 196, 196, 1)",
    borderRadius: 17,
    paddingLeft: 10,
    width: 310,
    height: 38,
  },
  field_input: {
    flex: 8,
    alignItems: "center",
  },
  text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 11,
    lineHeight: 22,
    left: 10,
    width: 310,
    paddingTop: 5,
  },
  civility_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 11,
    lineHeight: 22,
    left: 10,
    width: 310,
    marginTop: 15
  },
  space_input: {
    //marginBottom: 5,
  },
  container_button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    paddingBottom: 20,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  list_address: {
    backgroundColor: "#fff",
    alignSelf: "center",
    width: 308,
    borderRadius: 8,
    height: "auto",
    shadowColor: "rgba(0, 0, 0, 0.25)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 5,
    marginTop: -10,
  },
  text_list: {
    color: "#1D2A40",
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 17,
  },
  reset_field: {
    position: "absolute",
    top: 9,
    right: 10,
  },
  icon_right: {
    backgroundColor: "#ccc",
    width: 20,
    height: 20,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
  text_list_bold: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 17,
  },
  picker_input_container: {
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    width: 310,
    height: 38,
    borderRadius: 17,
    justifyContent: "center",
  },
  picker_input: {
    marginLeft: 23,
    fontFamily: fontFamily.regular,
    fontSize: 14,
    color: "rgba(17, 40, 66, 1)",
  },
});
