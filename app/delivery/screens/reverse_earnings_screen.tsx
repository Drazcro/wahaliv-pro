import React, { useEffect, useState } from "react";
import { View, Text, StyleSheet, ActivityIndicator, Modal } from "react-native";
import { Button } from "react-native-elements";
import i18n from "i18n-js";

import { fontFamily } from "src/theme/typography";

import { STATUS } from "src/constants/status";

import { Stack } from "expo-router";
import { get_delivery_information_for_payback, payback_delivery_and_generate_invoice } from "src/api/delivery/profile";
import { convert_number, parse_to_float_number } from "src/services/helpers";
import { ReverseEarningsModal } from "src/components/delivery/modal/reverse_earnings_modal";
import { color } from "src/theme";

export default function ReverseEarningsScreen() {
  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [canPayback, setCanPayback] = useState(false);
  const [isSalarie, setIsSalarie] = useState(false);
  const [paybackAmount, setPaybackAmount] = useState(0);
  const [price, setPrice] = useState("");

  const [requestError, setRequestError] = useState(false);
  const [error, setError] = useState(false);


  const setButtonDisable =
    (isSalarie === true && canPayback === false) ||
    (isSalarie === false && canPayback === false) ||
    (isSalarie === true && canPayback === true)
      ? true
      : false;

  const setButtonColor =
    (isSalarie === true && canPayback === false) ||
    (isSalarie === false && canPayback === false) ||
    (isSalarie === true && canPayback === true)
      ? "rgba(176, 173, 173, 1)"
      : color.ACCENT;

  useEffect(() => {
    get_delivery_information_for_payback(STATUS.MARKETPLACE).then(
      (response) => {
        setPaybackAmount(response.paybackAmount);
        setCanPayback(response.canPayback);
        setIsSalarie(response.isSalarie);
        setPrice(
          response.paybackAmount === 0
            ? "0"
            : convert_number(response.paybackAmount.toString()),
        );
        setError(response.paybackAmount === 0);
      },
    );
  }, []);

  const handleSubmit = () => {
    setShowLoadingButton(true);
    payback_delivery_and_generate_invoice(STATUS.MARKETPLACE)
      .then((res) => {
        const status = res?.data?.code;
        if (status === 200) {
          setShowModal(true);
        } else {
          setRequestError(true);
        }
        setShowLoadingButton(false);
      })
      .finally(() => setShowLoadingButton(false));
  };

  if (requestError) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <View
      
     
      style={styles.container}
    >
      <Stack.Screen options={{
        title: i18n.t("profile.Receipt")
      }} />
      
      {!error ? (
        <>
          <View style={styles.text_container}>
            <Text style={styles.text_label}>
              {i18n.t("profile.reversEarnings.label")}
            </Text>
          </View>
          <View style={styles.reverse_container}>
            <Text
              style={{
                ...styles.reverse_label,
                color:
                  paybackAmount !== 0
                    ? "rgba(29, 42, 64, 1)"
                    : "rgba(176, 173, 173, 1)",
              }}
            >
              {i18n.t("profile.reversEarnings.reverseLabel")}
            </Text>
            <Text
              style={{
                ...styles.reverse_price,
                color:
                  paybackAmount !== 0
                    ? "rgba(29, 42, 64, 1)"
                    : "rgba(176, 173, 173, 1)",
              }}
            >
              {parse_to_float_number(price)} €
            </Text>
          </View>
        </>
      ) : (
        <View style={styles.error_container}>
          <Text style={styles.error_title}>
            {i18n.t("profile.reversEarnings.errorMsgLabel")}
          </Text>
          <Text style={styles.error_msg}>
            {i18n.t("profile.reversEarnings.errorMsgStart")}
          </Text>
          <Text style={styles.error_msg}>
            {i18n.t("profile.reversEarnings.errorMsgEnd")}
          </Text>
        </View>
      )}
      <View style={styles.button_container}>
        {showLoadingButton ? (
          <View style={styles.button_loading}>
            <ActivityIndicator style={{ alignSelf: "center" }} color="white" />
          </View>
        ) : (
          <Button
            type="solid"
            title={i18n.t("profile.reversEarnings.buttonReverse")}
            buttonStyle={{
              ...styles.button,
              backgroundColor:
                paybackAmount === 0 ? "rgba(176, 173, 173, 1)" : setButtonColor,
            }}
            titleStyle={styles.text_button}
            onPress={handleSubmit}
            disabled={paybackAmount === 0 ? true : setButtonDisable}
            disabledStyle={{ backgroundColor: "rgba(231, 231, 231, 1)" }}
            disabledTitleStyle={{ color: "#fff" }}
          />
        )}
      </View>
      <Modal visible={showModal} transparent={true}>
        <ReverseEarningsModal
          price={paybackAmount}
          onClose={() => setShowModal(false)}
          onAccept={() => {
            setPaybackAmount(0);
            setError(true);
            setShowModal(false);
          }}
        />
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
    backgroundColor: "#fff",
  },
  text_container: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 17,
  },
  text_label: {
    color: "#1D2A40",
    fontWeight: "400",
    fontSize: 14,
    lineHeight: 22,
    textAlign: "center",
    fontFamily: fontFamily.Medium,
  },
  reverse_container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "rgba(218, 218, 218, 0.4)",
    width: 318,
    height: 40,
    borderRadius: 20,
    marginTop: 40,
  },
  reverse_label: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    marginLeft: 20,
  },
  reverse_price: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    marginRight: 20,
  },
  button_container: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    bottom: 70,
  },
  button_loading: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  error_container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    marginTop: -80,
  },
  error_title: {
    color: "rgba(29, 42, 64, 1)",
    fontSize: 18,
    lineHeight: 22,
    fontFamily: fontFamily.semiBold,
    marginBottom: 13,
  },
  error_msg: {
    color: "rgba(29, 42, 64, 1)",
    // fontWeight: "400",
    fontFamily: fontFamily.regular,
    fontSize: 15,
    lineHeight: 22,
    textAlign: "center",
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
});
