import { Stack } from "expo-router";
import React from "react";
import { View } from "react-native";
import FinancialImpact from "src/components/delivery/profile/financial_impact";

export default function FinancialImpactScreen() {
  return (
    <View style={{ backgroundColor: "white", flex: 1 }}>
      {/* <ProfileToolbar title="Impact financier" /> */}
      <Stack.Screen
        options={{
          title: "Impact financier",
        }}
      />
      <FinancialImpact />
    </View>
  );
}
