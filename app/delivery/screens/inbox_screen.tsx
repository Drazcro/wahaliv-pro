import React from "react";
import InboxFragment from "src/fragments/common/inbox_fragment";

export default function Page(){

    return(
        <React.Fragment>
            <InboxFragment />
        </React.Fragment>
    )
}