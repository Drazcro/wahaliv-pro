import React, { useEffect, useRef, useState } from "react";
import {
  Dimensions,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { StyleSheet, TouchableOpacity } from "react-native";
import { fontFamily } from "src/theme/typography";
import { showLocation } from "react-native-map-link";

import Euro from "assets/image_svg/Euro.svg";
import EuroGris from "assets/image_svg/EuroGris.svg";
import StorefrontRed from "assets/image_svg/StorefrontRed.svg";
import UserCircle from "assets/image_svg/UserCircle.svg";
import StorefrontGrey from "assets/image_svg/StorefrontGrey.svg";
import UserCircleGrey from "assets/image_svg/UserCircleGrey.svg";
import CaretDowns from "assets/image_svg/CaretDowns.svg";
import CaretUp from "assets/image_svg/CaretUp.svg";
import Phone from "assets/image_svg/Phone.svg";
import EmporterSmall from "assets/image_svg/EmporterSmall.svg";
import Phone_Disabled from "assets/image_svg/Phone_Disabled.svg";
import * as Location from "expo-location";
import { useFocusEffect } from "@react-navigation/native";

import {
  TOURNEE_STEP_STATE,
  TOURNEE_STEP_STATUS,
  TOURNEE_STEP_TYPE,
} from "src/constants/tournee";

import {
  DatabaseReference,
  off,
  onValue,
  ref,
  remove,
  update,
} from "firebase/database";
import { db } from "firebase";
import { busyStore } from "src/zustore/busy_store";

import { EventRegister } from "react-native-event-listeners";
import ScooterGrey from "assets/image_svg/ScooterGrey.svg";
import CarryGrey from "assets/image_svg/CarryGrey.svg";

import PAST_2 from "assets/image_svg/PAST_2.svg";
import PAST_3 from "assets/image_svg/PAST_3.svg";
import PAST_4 from "assets/image_svg/PAST_4.svg";
import PAST_5 from "assets/image_svg/PAST_5.svg";
import PAST_6 from "assets/image_svg/PAST_6.svg";

import ACTUAL_2 from "assets/image_svg/ACTUAL_2.svg";
import ACTUAL_3 from "assets/image_svg/ACTUAL_3.svg";
import ACTUAL_4 from "assets/image_svg/ACTUAL_4.svg";
import ACTUAL_5 from "assets/image_svg/ACTUAL_5.svg";
import ACTUAL_6 from "assets/image_svg/ACTUAL_6.svg";

import FUTURE_2 from "assets/image_svg/FUTURE_2.svg";
import FUTURE_3 from "assets/image_svg/FUTURE_3.svg";
import FUTURE_4 from "assets/image_svg/FUTURE_4.svg";
import FUTURE_5 from "assets/image_svg/FUTURE_5.svg";
import FUTURE_6 from "assets/image_svg/FUTURE_6.svg";

import { Stack, useRouter } from "expo-router";

import { tourneeStore } from "src/zustore/tournee_store";
import { authStore } from "src/services/auth_store";
import { IStep, ITourneeDetail } from "src/interfaces/delivery/tournees";
import {
  complete_step,
  get_tournee_details,
  ready_to_go_to_pick_up,
} from "src/api/delivery/tournee";
import MapPreviewTournee from "src/components/delivery/maps/map_preview_tournee";
import { call_hidden_number, hour_to_string } from "src/services/helpers";
import { SliderButton } from "src/components/common/slider_button";
import { IsBusyOverlay } from "src/components/modals/is_busy_overlay";
import { ClientAbsentTourneeOverlay } from "src/components/delivery/modal/client_absent_tournee_overlay";
import { PartnerLateTourneeOverlay } from "src/components/delivery/modal/partner_late_tournee_overlay";
import { FinishedTourneeOverlay } from "src/components/delivery/modal/finished_tournee_overlay";

const { width: screenWidth } = Dimensions.get("window");

const ICONS = {
  PAST: {
    2: PAST_2,
    3: PAST_3,
    4: PAST_4,
    5: PAST_5,
    6: PAST_6,
  },
  ACTUAL: {
    2: ACTUAL_2,
    3: ACTUAL_3,
    4: ACTUAL_4,
    5: ACTUAL_5,
    6: ACTUAL_6,
  },
  FUTURE: {
    2: FUTURE_2,
    3: FUTURE_3,
    4: FUTURE_4,
    5: FUTURE_5,
    6: FUTURE_6,
  },
};

export default function Page() {
  const { auth, push_token, base_user } = authStore();
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [showFinishedPopup, setShowFinishedPopup] = useState(false);

  const { opened, set_should_reload, set_steps } = tourneeStore();

  const [details, set_details] = React.useState<ITourneeDetail>();
  const [showLatePopup, setShowLatePopup] = useState(false);
  const [loaderConfirm, setLoaderConfirm] = useState(false);
  const router = useRouter();
  const scroll_ref = useRef<ScrollView>(null);

  const setIsBusy = busyStore((v) => v.setBusyState);

  const steps = React.useMemo(() => {
    if (details === undefined) {
      return [];
    }
    return details?.steps || [];
  }, [details, details?.steps]);

  const next_steps = React.useMemo(() => {
    return steps.filter(
      (s) =>
        s.state === TOURNEE_STEP_STATE.ENABLE &&
        s.step_status === TOURNEE_STEP_STATUS.SENDED,
    );
  }, [steps, steps.length]);

  const current_step = React.useMemo(() => {
    if (next_steps.length > 0) {
      return next_steps[0];
    }
    return undefined;
  }, [next_steps]);

  useEffect(() => {
    if (steps.length > 0 && !current_step) {
      setShowFinishedPopup(true);
      setTimeout(() => {
        goBackToCourse();
      }, 2000);
    }
  }, [current_step]);

  useEffect(() => {
    setIsBusy(true);
    load_or_reload_from_backend();
  }, [opened]);

  async function oncomplete_step() {
    if (!current_step || steps.length == 0) {
      goBackToCourse();
      return;
    }
    const step_id = current_step.step_id;
    if (
      current_step.order_is_last_moment &&
      current_step.order_is_last_moment === true
    ) {
      try {
        const { latitude, longitude } = (
          await Location.getCurrentPositionAsync()
        ).coords;

        setIsBusy(true);
        await ready_to_go_to_pick_up(
          current_step.course_id,
          latitude,
          longitude,
        );
        load_or_reload_from_backend();
      } catch (e) {
        console.error(e);
      } finally {
        setIsBusy(false);
      }
    } else {
      const msg =
        current_step?.type === TOURNEE_STEP_TYPE.PICK
          ? "Bravo ! Tu as bien récupéré la commande ! "
          : "Excellent travail! Tu as livré la commande avec succès!";

      const title =
        current_step?.type === TOURNEE_STEP_TYPE.PICK
          ? "Commande récupérée !"
          : "Client livré !";

      setIsBusy(true);
      complete_step(step_id, msg, title).finally(() => {
        try {
          if (current_step.type === TOURNEE_STEP_TYPE.DELIVERY) {
            const closed_ref = ref(
              db,
              `chat_all_My8xMS8yMDIz/${base_user.id}/${current_step.order_id}/chat_metadata/`,
            );

            update(closed_ref, {
              closed: true,
            });
          }
        } catch (error) {
          console.error(error);
        }

        load_or_reload_from_backend();
      });
    }
  }

  const goBackToCourse = () => {
    setShowFinishedPopup(false);
    set_should_reload(true);
    if (router.canGoBack()) {
      router.back();
    } else {
      router.replace("/delivery/tab/tournee");
    }
  };

  const load_or_reload_from_backend = () => {
    get_tournee_details(opened.id)
      .then((t: ITourneeDetail) => {
        if (t && t.id) {
          set_details(t);
        } else {
          setError(true);
        }
      })
      .catch((e) => {
        setError(true);
        console.log("error", e);
      })
      .finally(() => {
        //setError(false);
        setIsLoading(false);
        setIsBusy(false);
      });
  };

  useFocusEffect(() => {
    if (!base_user.id) return;
    const reference = ref(
      db,
      `actualisationTournee/${base_user.id}/${push_token}`,
    );
    onValue(reference, (snapshot) => {
      if (snapshot.exists()) {
        setIsBusy(true);
        load_or_reload_from_backend();
        remove(reference as DatabaseReference);
        EventRegister.emitEvent("COURSE_SHOULD_RELOAD");
      }
    });
    return () => off(reference);
  });

  const openMap = (destLat: number, destLong: number) => {
    return showLocation({
      latitude: destLat,
      longitude: destLong,
      directionsMode: "car",
      alwaysIncludeGoogle: true,
      dialogTitle: "Ouvrir dans Maps",
      dialogMessage: "Quelle application GPS voudriez-vous utiliser ?",
      cancelText: "Annuler",
      appsWhiteList:
        Platform.OS === "android" ? ["google-maps", "waze"] : undefined,
    });
  };

  function onMapButtonClick() {
    const latitude = current_step ? current_step.target.lat : 0;
    const longitude = current_step ? current_step.target.lng : 0;
    openMap(latitude, longitude);
  }

  useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      load_or_reload_from_backend();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  const current_step_index = React.useMemo(() => {
    return steps.findIndex((item) => item.step_id === current_step?.step_id);
  }, [current_step]);

  const is_two_steps = React.useMemo(() => {
    if (current_step && current_step_index + 1 < steps.length) {
      return (
        current_step?.order_is_last_moment ||
        steps[current_step_index + 1].order_is_last_moment
      );
    }
    return current_step?.order_is_last_moment ?? false;
  }, [current_step_index]);

  React.useEffect(() => {
    if (error) {
      if (router.canGoBack()) {
        router.back();
      } else {
        router.replace("/delivery/tab/tournee");
      }
    }
  }, [error]);

  React.useEffect(() => {
    if (scroll_ref?.current) {
      scroll_ref.current.scrollTo({
        y: current_step_index * 80,
        animated: true,
      });
    }
  }, [current_step_index, scroll_ref]);

  if (isLoading === true || !details || !current_step) {
    return (
      <View style={styles.loadingContainer}>
        <View style={styles.loading}>
          <Text>Chargement en cours ...</Text>
        </View>
      </View>
    );
  }

  return (
    <SafeAreaView style={{ height: "100%" }}>
      <Stack.Screen
        options={{
          headerShown: true,
          //headerLeft: ()=><HeaderBack />,
          headerTitle: "Détail de la tournée " + details?.id ?? "",
        }}
      />
      <View style={styles.map}>
        <View style={styles.mapGps}>
          <TouchableOpacity
            style={styles.mapButton}
            onPress={() => {
              // if (Platform.OS === "android") {
              //   OpenAndroidOptionsModal();
              // } else {
              onMapButtonClick();
            }}
          >
            <Text
              style={{
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
                lineHeight: 22,
                color: "#fff",
              }}
            >
              GPS
            </Text>
          </TouchableOpacity>
        </View>
        <MapPreviewTournee tournee={details} />
      </View>
      <View style={styles.main}>
        <View style={styles.container}>
          <ScrollView
            style={{ flex: 1 }}
            ref={scroll_ref}
            showsVerticalScrollIndicator={false}
          >
            <View style={{ flex: 1, paddingBottom: 200 }}>
              {(details.steps ?? []).map((step, index) => (
                <StepLine
                  key={index}
                  number={step.position + 1}
                  step={step}
                  price={step.tip}
                  isLast={details.steps.length === index + 1 ? true : false}
                  currentIndex={index}
                  activeIndex={current_step_index}
                />
              ))}
            </View>
          </ScrollView>
        </View>

        <View style={styles.footer}>
          <DetailCommande step={current_step} />

          <ButtonTournee step={current_step} doAction={oncomplete_step} />
        </View>

        {/* <View style={styles.container}> */}

        {/* <View style={styles.tourneContainer}>
                  <ScrollView>
                  {(details.steps ?? []).map((step, index) => (
                      <StepLine
                          key={index}
                          number={step.position + 1}
                          step={step}
                          price={step.tip}
                          isLast={
                          details.steps.length === index + 1 ? true : false
                          }
                          currentIndex={index}
                          activeIndex={current_step_index}
                      />
                ))}
                  </ScrollView>
              
                </View> */}

        {/* </View> */}
      </View>

      {current_step?.type === TOURNEE_STEP_TYPE.PICK ? (
        <PartnerLateTourneeOverlay
          visible={showLatePopup}
          loaderConfirm={loaderConfirm}
          onClose={() => setShowLatePopup(false)}
          onAccept={() => {
            setShowLatePopup(false);
          }}
        />
      ) : (
        <ClientAbsentTourneeOverlay
          visible={showLatePopup}
          loaderConfirm={loaderConfirm}
          Resto={current_step?.target.name}
          onClose={() => setShowLatePopup(false)}
          onAccept={() => {
            //
          }}
        />
      )}

      <FinishedTourneeOverlay
        visible={showFinishedPopup}
        onClose={() => goBackToCourse()}
        onPress={() => goBackToCourse()}
      />

      <IsBusyOverlay />

      {is_two_steps ? (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-evenly",
            marginHorizontal: 30,
            alignItems: "center",
            marginTop: -15,
            position: "relative",
            top: -15,
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 12,
              marginLeft: 39,
            }}
          >
            <ScooterGrey />
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 10,
                lineHeight: 12,
                marginTop: 1,
                color: "rgba(196, 196, 196, 1)",
              }}
            >
              Départ vers restaurant
            </Text>
          </View>
          <View
            style={{
              backgroundColor: "#E7E7E7",
              height: 5,
              width: "40%",
              borderRadius: 5,
            }}
          >
            <View style={{}}></View>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginRight: 30,
            }}
          >
            <CarryGrey />
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 10,
                lineHeight: 12,
                color: "rgba(196, 196, 196, 1)",
                marginTop: 1,
              }}
            >
              Recupérer la commande
            </Text>
          </View>
        </View>
      ) : null}
    </SafeAreaView>
  );
}

interface ITourneeProps {
  number: number;
  isLast: boolean;
  step: IStep;
  price: string;
  currentIndex: number;
  activeIndex: number;
}
const ADRESS_DEFAULT = {
  address: "",
  appartement: "",
  batiment: "",
  code_porte: "",
  complementadresse: "",
  etage: "",
  numerorue: "",
  postalCode: "",
};
const StepLine = ({
  number,
  isLast,
  step,
  price,
  currentIndex,
  activeIndex,
}: ITourneeProps) => {
  const active = step.state === TOURNEE_STEP_STATE.ENABLE;
  const isPreview = step.step_status === TOURNEE_STEP_STATUS.FINISHED;
  const isCrossed = step.state === TOURNEE_STEP_STATE.CROSSED;
  const isLivrer = step.type === TOURNEE_STEP_TYPE.DELIVERY;
  const newPrice = parseFloat(price);

  const NBIcon = React.useCallback(() => {
    const total = step.order_details.length;
    if (total === 1 || total > 6) {
      return <EmporterSmall />;
    }
    const key1 = active
      ? "ACTUAL"
      : currentIndex < activeIndex
      ? "PAST"
      : "FUTURE";
    const COMPONENT = ICONS[key1][total];
    return <COMPONENT />;
  }, [activeIndex, currentIndex, active, step.order_details.length]);

  const getAdress = React.useMemo(() => {
    if (step.target.delivery_adress === null) return ADRESS_DEFAULT;
    else {
      return step.target.delivery_adress;
    }
  }, [step.target.delivery_adress]);

  useEffect(() => {
    console.log("step: ", step);
  }, []);

  return (
    <View style={styles.tourneeMain}>
      {!isLast ? <View style={styles.lineDash} /> : <View />}
      <View
        style={{
          ...styles.indexContent,
          backgroundColor: active
            ? "rgba(255, 92, 133, 1)"
            : isPreview || isCrossed
            ? "rgba(141, 152, 160, 1)"
            : "rgba(196, 196, 196, 1)",
        }}
      >
        <Text style={{ color: "#fff" }}>{number}</Text>
      </View>
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{ flexDirection: "column", width: active ? "80%" : "80%" }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              flexWrap: "wrap",
            }}
          >
            <Text
              style={{
                ...styles.tourneeHourBold,
                marginRight: active ? 10 : 5,
                color: active ? "#FF5C85" : "rgba(141, 152, 160, 1)",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {hour_to_string(step.hour)}
            </Text>
            <View
              style={{
                ...styles.tourneeIconName,
                paddingHorizontal: active ? 10 : 0,
                backgroundColor: active ? "rgba(255, 92, 133, 0.1)" : "#fff",
              }}
            >
              {isLivrer ? (
                active ? (
                  <UserCircle />
                ) : (
                  <UserCircleGrey />
                )
              ) : active ? (
                <StorefrontRed />
              ) : (
                <StorefrontGrey />
              )}
              <Text
                style={{
                  ...styles.tourneeName,
                  color: active ? "#1D2A40" : "rgba(141, 152, 160, 1)",
                  textDecorationLine: isCrossed ? "line-through" : "none",
                }}
              >
                {step.target.name}
              </Text>
            </View>
            {step.order_details.length > 1 ? (
              <View
                style={{
                  alignItems: "flex-end",
                  justifyContent: "space-evenly",
                  borderRadius: 10,
                  flexDirection: "row",
                  paddingBottom: 4,
                  paddingHorizontal: 5,
                  marginHorizontal: 5,
                }}
              >
                {<NBIcon />}
              </View>
            ) : null}
          </View>
          <Text
            style={{
              ...styles.tourneeAdress,
              color: active ? "#1D2A40" : "#C4C4C4",
              textDecorationLine: isCrossed ? "line-through" : "none",
            }}
          >
            {step.target.adress}
          </Text>
          {getAdress?.numerorue && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"N° " + getAdress.numerorue + ", "}
            </Text>
          )}

          {getAdress?.code_porte && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"porte " + getAdress.code_porte + ", "}
            </Text>
          )}

          {getAdress?.appartement && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"appartement " + getAdress.appartement + ", "}
            </Text>
          )}
          {getAdress?.etage && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"étage " + getAdress.etage + ", "}
            </Text>
          )}
          {getAdress?.batiment && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"batiment " + getAdress?.batiment}
            </Text>
          )}
          {getAdress?.complementadresse ? (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {getAdress.complementadresse}
            </Text>
          ) : null}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View
              style={{
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
              }}
            >
              <Text
                style={{
                  ...styles.tourneeAdress,
                  color: active ? "#1D2A40" : "#C4C4C4",
                  textDecorationLine: isCrossed ? "line-through" : "none",
                  marginBottom: 5,
                }}
              >
                Commande #{step.order_id}
              </Text>
            </View>
            {newPrice > 0 && isLivrer && (
              <View
                style={{
                  backgroundColor: active ? "#ECC53B" : "#fff",
                  borderRadius: 5,
                  marginLeft: 5,
                  paddingHorizontal: 5,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {active ? <Euro /> : <EuroGris />}
                <Text
                  style={{
                    fontFamily: fontFamily.semiBold,
                    fontSize: 12,
                    lineHeight: 18,
                    color: active ? "#1D2A40" : "#C4C4C4",
                    marginLeft: 3,
                    textDecorationLine: isCrossed ? "line-through" : "none",
                  }}
                >
                  {price} €
                </Text>
              </View>
            )}
          </View>
          {step.pickup_indication && (
            <View style={styles.recuperationBox}>
              <Text style={styles.recuperationTextBold}>
                Récuperation de la commande :{" "}
              </Text>
              <Text style={styles.recuperationTextLite}>
                {step.pickup_indication}
              </Text>
            </View>
          )}
        </View>

        <View
          style={{
            flexDirection: "row",
          }}
        >
          <TouchableOpacity
            onPress={() => {
              try {
                call_hidden_number(step.target.telephone);
              } catch (error) {
                console.error(error);
              }
            }}
          >
            {active ? <Phone /> : <Phone_Disabled />}
          </TouchableOpacity>
          {/* {isLivrer ? (
              <TouchableOpacity
                onPress={() => {
                  router.push({
                    pathname: "/chat_client_screen",
                    params: { command_id: step.order_id },
                  });
                }}
              >
                {active ? <ActiveChatBtn /> : <InactiveChatBtn />}
              </TouchableOpacity> 
            ) : null} */}
        </View>
      </View>
    </View>
  );
};

const DetailCommande = ({ step }: { step: IStep }) => {
  const [toggle, setToggle] = useState(false);
  return (
    <View>
      <View style={styles.clientMain}>
        <TouchableOpacity
          onPress={() => setToggle(!toggle)}
          style={{
            flexDirection: "row",
            justifyContent: "space-between",

            width: "100%",
            height: 30,
          }}
        >
          <Text style={styles.itemTitle}>COMMANDE #{step.order_id}</Text>
          {toggle ? <CaretUp /> : <CaretDowns />}
        </TouchableOpacity>
        <View style={styles.horizontalLine} />
        {toggle ? (
          <ScrollView style={{ marginBottom: 40 }}>
            <View style={{ ...styles.clientContainer, marginHorizontal: 15 }}>
              <View style={{ flexDirection: "column", width: "100%" }}>
                {step.order_details.map((o, index) => (
                  <React.Fragment key={index}>
                    <View style={styles.products} key={index}>
                      <Text style={styles.productTitle}>
                        {o.qte} {o.name}
                      </Text>
                      <Text style={styles.productPrice}>{o.price} €</Text>
                    </View>
                    {o?.sub_products?.map((p, index) => (
                      <React.Fragment key={index}>
                        <View style={styles.products} key={index}>
                          <Text style={styles.productTitle}>
                            {p.quantity} {p.name}
                          </Text>
                          <Text style={styles.productPrice}>{p.price} €</Text>
                        </View>
                      </React.Fragment>
                    ))}
                  </React.Fragment>
                ))}
              </View>
            </View>
          </ScrollView>
        ) : null}
      </View>
      {toggle && step.order_comment ? (
        <View style={styles.clientMain}>
          <View style={styles.comment}>
            <Text style={styles.subTitles}>COMMENTAIRE CLIENT</Text>
            <Text style={styles.commentText}>{step.order_comment}</Text>
          </View>
        </View>
      ) : null}
    </View>
  );
};

const ButtonTournee = ({
  step,
  doAction,
}: {
  step: IStep;
  doAction: () => void;
}) => {
  const subTitle = React.useMemo(() => {
    if (step.order_is_last_moment && step.order_is_last_moment === true) {
      return "Fais glisser le bouton dès que tu pars vers le restaurant";
    }
    if (step.type === TOURNEE_STEP_TYPE.PICK) {
      return "Fais glisser le bouton dès que tu as récupéré la commande";
    } else {
      return "Fais glisser le bouton dès que tu as livré la commande";
    }
  }, [step]);

  const title = React.useMemo(() => {
    if (step.order_is_last_moment && step.order_is_last_moment === true) {
      return "Départ vers restaurant";
    }
    if (step.type === TOURNEE_STEP_TYPE.PICK) {
      return "Commande récupérée";
    } else {
      return "Client livré !";
    }
  }, [step]);
  console.log("updating button");

  return (
    <View
      style={{
        backgroundColor: "#fff",
      }}
    >
      <SliderButton
        title={title}
        name={step.target.name}
        subTitle={subTitle}
        railBgColor="rgba(255, 92, 133, 0.1);"
        railStylesBgColor="#FF5C85"
        onSuccess={doAction}
        subTitleColor={"#000"}
        isDisabled={false}
      />
    </View>
  );
};

export const styles = StyleSheet.create({
  footer: {
    //flex: 1,
    width: "100%",
    //backgroundColor: 'green'
  },
  main: {
    // flex: 5,
    flex: 1,
    flexDirection: "column",
    backgroundColor: "#fff",
    // paddingTop: 40,
    marginTop: -10,
  },
  container: {
    padding: 10,
    //backgroundColor: "blue",
    paddingHorizontal: 10,
    flex: 1,
    alignItems: "center",
  },
  header: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "75%",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 18,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
    marginTop: 15,
  },
  subTitle: {
    fontFamily: fontFamily.regular,
    textAlign: "center",
    fontSize: 14,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
  },
  map: {
    //position: "relative",
    width: "100%",
    height: 220,
  },
  mapGps: {
    position: "absolute",
    top: 10,
    right: 20,
    zIndex: 1,
    overflow: "hidden",
  },
  hour: {
    flexDirection: "row",
    justifyContent: "space-around",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    height: 68,
    marginBottom: 20,
  },
  timeContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    paddingVertical: 10,
  },
  timeTitle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  timeText: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
  timeHour: {
    fontFamily: fontFamily.Bold,
    color: "#1D2A40",
    fontSize: 18,
    lineHeight: 21.97,
  },
  courses: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 40,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
  },
  distance: {
    flexDirection: "row",
    marginBottom: 5,
    alignItems: "center",
  },
  distanceText: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 14.63,
    color: "#112842",
    marginLeft: 20,
  },
  distanceTextBold: {
    fontFamily: fontFamily.Bold,
    color: "#112842",
    fontSize: 12,
    lineHeight: 17.07,
    marginLeft: 5,
    marginRight: 5,
  },
  tourneContainer: {
    marginTop: 10,
    marginBottom: 15,
    paddingLeft: 26,
  },
  tourneeMain: {
    //backgroundColor: 'red',
    flexDirection: "row",
    marginBottom: 10,
    position: "relative",
  },
  mapButton: {
    backgroundColor: "#40ADA2",
    width: 98,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginRight: 15,
    shadowColor: "rgba(0, 0, 0, 1)",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 5,
    elevation: 5,
  },
  lineDash: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 10,
    borderColor: "#C4C4C4",
    height: "100%",
  },
  indexContent: {
    width: 22,
    height: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginRight: 10,
    marginTop: 5,
  },
  tourneeHourBold: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 18,
  },
  tourneeIconName: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    paddingVertical: 3,
  },
  tourneeName: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 18,
    marginLeft: 5,
  },
  tourneeAdress: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 18,
  },
  clientMain: {
    marginHorizontal: 23,
    marginVertical: 5,
    //height: "50%",
  },
  subTitles: {
    position: "absolute",
    top: 9,
    left: 13,
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
  comment: {
    position: "relative",
    marginTop: 10,
    marginBottom: 20,
    width: "100%",
    height: 87,
    borderRadius: 5,
    justifyContent: "center",
    backgroundColor: "rgba(252, 247, 231, 1)",
    alignSelf: "center",
  },
  commentText: {
    fontFamily: fontFamily.regular,
    fontSize: 13,
    lineHeight: 18,
    color: "#5C5B5B",
    marginLeft: 13,
    marginTop: 20,
  },
  clientContainer: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  editText: {
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.semiBold,
    color: "#FF5C85",
  },
  products: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 15,
  },
  productTitle: {
    flex: 9,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    paddingRight: 8,
  },
  productPrice: {
    flex: 3,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    textAlign: "right",
    paddingLeft: 5,
  },
  itemTitle: {
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
  horizontalLine: {
    height: 1,
    backgroundColor: "rgba(196, 196, 196, 0.31)",
    marginVertical: 5,
    marginHorizontal: -20,
  },
  loadingContainer: {
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
    paddingTop: 41,
    marginTop: 30,
    marginBottom: 20,
    width: "100%",
    height: "100%",
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: screenWidth,
    height: 100,
    backgroundColor: "white",
  },
  recuperationBox: {
    width: 270,
    height: 47,
    backgroundColor: "#FCF7E7",
    borderRadius: 5,
    flexDirection: "column",
    justifyContent: "center",
    paddingLeft: 9,
  },
  recuperationTextLite: {
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 13,
    lineHeight: 18,
  },
  recuperationTextBold: {
    fontFamily: fontFamily.semiBold,
    color: "#000000",
    fontSize: 13,
    lineHeight: 18,
  },

  navigationOptions: {
    textAlign: "center",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    marginTop: 17,
    marginBottom: 22,
  },

  cancelBtn: {
    width: "90%",
    height: 57,
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: "#fff",
    marginBottom: 12,
    justifyContent: "center",
  },
  cancelBtnText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "rgba(243, 106, 114, 1)",
    textAlign: "center",
  },
});
