import { Stack } from "expo-router";
import React, { useEffect } from "react";

import { CoursesBody } from "src/components/delivery/course_body";
import { courseStore } from "src/zustore/coursestore";


export default function CoursesEncoursDelivery() {
  const {opened} = courseStore()
  return (
    <>
    <Stack.Screen
        options={{
          headerShown: true,
          headerTitle: "Course n°" + opened.id_commande,
        }}
      />
    <CoursesBody />
    </>
  );
}
