
import { Stack, useLocalSearchParams } from "expo-router";
import React from "react";
import { StyleSheet, View } from "react-native";
import { FAQBonus, FAQKilometrage, FAQLocation, FAQWire } from "src/components/delivery/help_component";
import { profileStore } from "src/zustore/profilestore";

export default function HelpScreen() {
  const { id } = useLocalSearchParams();

  const profile = profileStore((v) => v.profile);

  const ComponentByID = React.useCallback(() => {
    switch (id) {
      case "BONUS":
        return (
          <FAQBonus
            title={"Je ne vois plus mes bonus "}
            object={"Je ne vois plus mes bonus"}
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
            `}
          />
        );
      case "WIRE":
        return (
          <FAQWire
            title={"Je n’ai pas reçu mon/mes virement(s)"}
            object={"Je n’ai pas reçu mon/mes virement(s) "}
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
              Paiement montant attendu: 
            `}
          />
        );
      case "KM":
        return (
          <FAQKilometrage
            title={
              "J’ai un problème avec le nombre de kilomètres et la rémunération."
            }
            object={
              "Il y a un problème avec le nombre de kilomètres et la rémunération"
            }
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
              Km réel (Google Maps): 
              N° de la commande concernée: 
            `}
          />
        );
      case "LOCATION":
        return (
          <FAQLocation title="Je n’arrive pas à me positionner, je ne sais pas comment faire." />
        );
      default:
        return;
    }
  }, [id]);

  return (
    <>
     <Stack.Screen options={{
      title: "Aide"
     }} />
      
      <View style={styles.helpComponentPosition} >
        <ComponentByID />
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  helpComponentPosition: {
    height: "100%",
    backgroundColor: "white",
  },
});
