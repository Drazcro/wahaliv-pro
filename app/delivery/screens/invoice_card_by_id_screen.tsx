import { View } from "react-native";

import React from "react";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import { InvoiceCardByID } from "src_legacy/components/delivery/cards/invoice_card/invoice_card_by_id";
import { Stack, useLocalSearchParams } from "expo-router";
import { styles } from "src/styles/delivery/invoice.style";

export default function InvoiceCardByIDScreen() {
  
  const { date, courses } = useLocalSearchParams<{ date: string, courses?: string }>(); // Note the optional chaining here

  // Check if courses is defined before parsing it
  const parsedCourses = courses ? JSON.parse(courses) : [];

  const charAtOne = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  return (
    <View style={styles.bgColor}>
      <Stack.Screen
        options={{
          title: charAtOne(moment(date).format("dddd D MMMM YYYY")),
        }}
      />
      <ScrollView style={styles.bgColor}>
        <View style={styles.invoiceCard}>
          {parsedCourses?.map((item: any, index: number) => (
            <InvoiceCardByID key={index} invoice_data_course={item} />
          ))}
        </View>
      </ScrollView>
    </View>
  );
}
