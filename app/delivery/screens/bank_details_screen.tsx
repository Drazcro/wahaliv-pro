import React, { useEffect, useState } from "react";
import {
  View,
  StyleSheet,
  Text,
  ActivityIndicator,
  Platform,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
} from "react-native";
import { Input } from "@rneui/themed";
import { Button } from "react-native-elements";
import RNPickerSelect from "react-native-picker-select";
import i18n from "i18n-js";

import Toast from "react-native-expo-popup-notifications";
import MaskInput from "react-native-mask-input";
import { ibanMask, swiftMask } from "src/constants/input_validation";
import CaretDown from "assets/image_svg/CaretDown.svg";
import { fontFamily } from "src/theme/typography";
import { Stack, router } from "expo-router";
import { IBankInfo } from "src/interfaces/delivery/finance";
import { get_delivery_bank_information_for_profile, update_bank_information } from "src/api/delivery/profile";
import { validate_iban, validate_swift } from "src/services/helpers";
import { color } from "src/theme";

enum EPaiementFrequency {
  DAILY = "daily",
  WEEKLY = "weekly",
  MONTHLY = "monthly",
}
export default function BankDetailsScreen() {
  const [titulaireCompte, setTitulaireCompte] = useState("");
  const [iban, setIban] = useState("");
  const [swift, setSwift] = useState("");
  const [banque, setBanque] = useState("");
  const [frequence, setFrequence] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const [errorIban, setErrorIban] = useState<string>();
  const [errorSwift, setErrorSwift] = useState<string>();
  const [error, setError] = useState(false);



  const disabledButton =
    (errorIban !== undefined && errorIban !== "") ||
    (errorSwift !== undefined && errorSwift !== "");

  const handleSubmit = () => {
    setShowLoadingButton(true);
    update_bank_information({
      banque,
      frequence,
      iban,
      swift,
      titulaireCompte,
    })
      .then((response) => {
        setShowLoadingButton(false);
        if (response !== undefined) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: i18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: i18n.t("profile.notification.error.message"),
          });
        }
      })
      .catch(() => {
        setError(true);
        setShowLoadingButton(false);
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: i18n.t("profile.notification.error.message"),
        });
      })
      .finally(() => {
        setShowLoadingButton(false);
      });
  };

  useEffect(() => {
    get_delivery_bank_information_for_profile()
      .then((items: IBankInfo) => {
        console.log(frequence);
        setBanque(items.banque);
        setFrequence(items.frequence);
        setIban(items.iban);
        setSwift(items.swift);
        setTitulaireCompte(items.titulaireCompte);
      })
      .catch(() => setError(true))
      .finally(() => setIsLoading(false));
  }, []);

  const getInitialPlaceholderForFrequency = () => {
    switch (frequence.toLowerCase()) {
      case EPaiementFrequency.DAILY:
        {
          return {
            label: i18n.t("form.select.repaymentFrequency.daily"),
            value: i18n.t("form.select.repaymentFrequency.valueDaily"),
          };
        }
        break;
      case EPaiementFrequency.WEEKLY:
        {
          return {
            label: i18n.t("form.select.repaymentFrequency.hebdo"),
            value: i18n.t("form.select.repaymentFrequency.valueHebdo"),
          };
        }
        break;
      case EPaiementFrequency.MONTHLY: {
        return {
          label: i18n.t("form.select.repaymentFrequency.monthly"),
          value: i18n.t("form.select.repaymentFrequency.valueMonthly"),
        };
      }
      default: {
        return { label: "Sélectionner une option" };
      }
    }
  };

  const onvalidate_swift = (text: string) => {
    setSwift((v) => text);
    const err = validate_swift(text);
    console.log(text);
    setErrorSwift(err);
  };

  const onvalidate_iban = (text: string) => {
    setIban(text);
    const err = validate_iban(text);
    console.log(text);
    setErrorIban(err);
  };

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  if (error === true) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <Stack.Screen options={{
        title: i18n.t("profile.coordonnerBancaire.MyBankDetails")
      }} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.field_input}>
            <View style={styles.space_input}>
              <Text style={styles.text_label}>
                {i18n.t("profile.coordonnerBancaire.accountOwner")}
              </Text>
              <Input
                value={titulaireCompte}
                onChangeText={(text) => setTitulaireCompte(text)}
                containerStyle={styles.input_border}
                inputContainerStyle={styles.input_style}
                placeholder={i18n.t("profile.coordonnerBancaire.firstname")}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                }}
              />
            </View>
            <View style={styles.space_input}>
              <Text style={styles.text_label}>
                {i18n.t("profile.coordonnerBancaire.iban")}
              </Text>
              <MaskInput
                mask={ibanMask}
                placeholderFillCharacter={"x"}
                keyboardType="default"
                value={iban}
                onChangeText={(text) => {
                  onvalidate_iban(text.split("-").join("").toUpperCase());
                }}
                style={styles.input_border}
              />
              {errorIban !== undefined && (
                <Text style={{ color: "red", fontSize: 9, left: 7 }}>
                  {errorIban}
                </Text>
              )}
            </View>
            <View style={styles.space_input}>
              <Text style={styles.text_label}>
                {i18n.t("profile.coordonnerBancaire.swift")}
              </Text>
              <MaskInput
                mask={swiftMask}
                placeholderFillCharacter={"x"}
                keyboardType="default"
                value={swift}
                onChangeText={(text) => {
                  onvalidate_swift(text.toUpperCase());
                }}
                style={styles.input_border}
              />
              {errorSwift !== undefined && (
                <Text style={{ color: "red", fontSize: 9, left: 7 }}>
                  {errorSwift}
                </Text>
              )}
            </View>
            <View style={styles.space_input}>
              <Text style={styles.text_label}>
                {i18n.t("profile.coordonnerBancaire.bank")}
              </Text>
              <Input
                value={banque}
                onChangeText={(text) => setBanque(text)}
                containerStyle={styles.input_border}
                inputContainerStyle={styles.input_style}
                placeholder={i18n.t("profile.coordonnerBancaire.textBank")}
                inputStyle={{
                  fontFamily: fontFamily.regular,
                  fontSize: 14,
                }}
              />
            </View>
            <View style={styles.space_input}>
              <Text style={styles.text_label}>
                {i18n.t("profile.coordonnerBancaire.paymentFrequency")}
              </Text>
              <RNPickerSelect
                placeholder={getInitialPlaceholderForFrequency()}
                onValueChange={(value) => setFrequence(value)}
                value={frequence}
                Icon={() => {
                  if (Platform.OS === "ios") {
                    return (
                      <View style={{ right: 15 }}>
                        <CaretDown />
                      </View>
                    );
                  } else {
                    return null;
                  }
                }}
                pickerProps={{
                  accessibilityLabel: "test",
                  style: {
                    color: "#1D2A40",
                  },
                }}
                style={{
                  viewContainer: {
                    borderWidth: 0.5,
                    borderColor: "rgba(196, 196, 196, 1)",
                    minWidth: 310,
                    height: 38,
                    borderRadius: 17,
                    justifyContent: "center",
                    paddingLeft: 3,
                  },

                  inputIOS: {
                    marginLeft: 23,
                    fontFamily: fontFamily.regular,
                    fontSize: 14,
                    color: "#1D2A40",
                  },
                  inputAndroid: {
                    marginLeft: 9,
                    fontFamily: fontFamily.regular,
                    fontSize: 14,
                    color: "#1D2A40",
                  },
                }}
                items={[
                  {
                    label: i18n.t("form.select.repaymentFrequency.daily"),
                    value: i18n.t("form.select.repaymentFrequency.valueDaily"),
                    color: "#1D2A40",
                  },
                  {
                    label: i18n.t("form.select.repaymentFrequency.hebdo"),
                    value: i18n.t("form.select.repaymentFrequency.valueHebdo"),
                    color: "#1D2A40",
                  },
                  {
                    label: i18n.t("form.select.repaymentFrequency.monthly"),
                    value: i18n.t(
                      "form.select.repaymentFrequency.valueMonthly",
                    ),
                    color: "#1D2A40",
                  },
                ]}
              />
            </View>
          </View>
          <View style={styles.container_button}>
            {showLoadingButton ? (
              <View style={styles.loader_button}>
                <ActivityIndicator
                  style={{ alignSelf: "center" }}
                  color="white"
                />
              </View>
            ) : (
              <Button
                type="solid"
                title={i18n.t("form.button.saveEdit")}
                titleStyle={styles.buttonText}
                buttonStyle={styles.button}
                onPress={handleSubmit}
                disabled={disabledButton}
              />
            )}
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  input_border: {
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    borderRadius: 17,
    paddingLeft: 23,
    minWidth: 310,
    height: 38,
    fontFamily: fontFamily.regular,
    fontSize: 14,
  },
  input_style: {
    borderBottomWidth: undefined,
  },
  field_input: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: 30,
    paddingBottom: 20,
  },
  text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 12,
    lineHeight: 22,
    left: 10,
  },
  space_input: {
    marginBottom: Platform.OS === "ios" ? 16 : 12,
  },
  container_button: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  select: {
    width: 308,
    borderWidth: 1,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  buttonText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
});
