import React from 'react';
import { View } from 'react-native';
import { Button, Text } from 'react-native-paper';
import { useGPS } from 'src/hooks/useLocalisation';
import { logout } from 'src/services';


export default function Index(){
    const { BGLocation, isLoading, FGLocation, isGPSOn } = useGPS()
    return (
        <View style={{flex:1, justifyContent:'center', alignItems: 'center'}}>
            
            <Text>
                BG : {BGLocation.status}, can ask again {BGLocation.canAskAgain.toString()}
            </Text>
            <Text>
                FG : {FGLocation.status}, can ask again {FGLocation.canAskAgain.toString()}
            </Text>
            <Text>
                is gps loading: {isLoading.toString()}
            </Text>
            <Text>  
                is gps on: {isGPSOn.toString()}
            </Text>
            <Button  onPress={logout}>
                    logout
            </Button>
        </View>
    )
}