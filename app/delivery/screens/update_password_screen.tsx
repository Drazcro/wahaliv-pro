import i18n from "i18n-js";
import React, { useState } from "react";
import {
  ActivityIndicator,
  StyleSheet,
  Text,
  View,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Platform,
} from "react-native";
import { Button } from "react-native-elements";
import { Input } from "@rneui/themed";
import { fontFamily } from "src/theme/typography";
import Lock from "assets/image_svg/Lock.svg";
import EyeClosed from "assets/image_svg/EyeClosed.svg";
import Eye from "assets/image_svg/Eye.svg";

import Toast from "react-native-expo-popup-notifications";
import { Stack, router } from "expo-router";
import { confirm_password_validate, password_validate } from "src/services/helpers";
import { update_password } from "src/api/delivery/profile";
import { color } from "src/theme";

export default function UpdatePasswordScreen() {
  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const [oldPassword, setOldPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [showCurrentPassword, setCurrentShowPassword] = useState(false);
  const [showNewPassword, setNewShowPassword] = useState(false);
  const [showConfirmPassword, setConfirmShowPassword] = useState(false);
  const [error, setError] = useState(false);
  const [passError, setPassError] = useState("");
  const [confirmPassError, setConfirmPassError] = useState("");


  const validPassword = () => {
    const err: string = password_validate(newPassword);
    if (err !== "") {
      setPassError(err);
    } else {
      setPassError("");
    }
    validConfirmPassword();
  };

  const validConfirmPassword = () => {
    const err: string = confirm_password_validate(newPassword, confirmPassword);
    if (err !== "") {
      setConfirmPassError(err);
    } else {
      setConfirmPassError("");
    }
  };

  const handleSubmit = () => {
    setShowLoadingButton(true);
    update_password({
      oldPassword,
      newPassword,
    })
      .then((response) => {
        setShowLoadingButton(true);
        if (response !== undefined) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: i18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: i18n.t("profile.notification.error.message"),
          });
        }
      })
      .catch(() => {
        setError(true);
        setShowLoadingButton(false);
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: i18n.t("profile.notification.error.message"),
        });
      })
      .finally(() => setShowLoadingButton(false));
  };

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <Stack.Screen options={{
        title: i18n.t("profile.updatePassword.title")
      }} />
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>

    
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.field_input}>
              <View style={styles.space_input}>
                <Input
                  value={oldPassword}
                  containerStyle={styles.input_border}
                  inputContainerStyle={styles.input_style}
                  placeholder={i18n.t("profile.updatePassword.enterPassword")}
                  label={i18n.t("profile.updatePassword.currentPassword")}
                  labelStyle={styles.text_label}
                  leftIcon={<Lock />}
                  placeholderTextColor="rgba(119, 119, 119, 1)"
                  leftIconContainerStyle={{ paddingLeft: 10 }}
                  rightIconContainerStyle={{ paddingRight: 10 }}
                  onChangeText={(text) => setOldPassword(text)}
                  style={styles.text_value}
                  secureTextEntry={showCurrentPassword === false}
                  rightIcon={
                    showCurrentPassword === true ? (
                      <Eye onPress={() => setCurrentShowPassword(false)} />
                    ) : (
                      <EyeClosed onPress={() => setCurrentShowPassword(true)} />
                    )
                  }
                />
              </View>
              <View style={styles.space_input}>
                <Input
                  value={newPassword}
                  containerStyle={styles.input_border}
                  inputContainerStyle={styles.input_style}
                  placeholder={i18n.t("profile.updatePassword.limitPassword")}
                  label={i18n.t("profile.updatePassword.newPassword")}
                  labelStyle={styles.text_label}
                  leftIcon={<Lock />}
                  leftIconContainerStyle={{ paddingLeft: 10 }}
                  rightIconContainerStyle={{ paddingRight: 10 }}
                  onChangeText={(text) => setNewPassword(text)}
                  onBlur={validPassword}
                  style={styles.text_value}
                  errorMessage={passError}
                  secureTextEntry={showNewPassword === false}
                  rightIcon={
                    showNewPassword === true ? (
                      <Eye onPress={() => setNewShowPassword(false)} />
                    ) : (
                      <EyeClosed onPress={() => setNewShowPassword(true)} />
                    )
                  }
                />
              </View>
              <View style={styles.space_input}>
                <Input
                  value={confirmPassword}
                  containerStyle={styles.input_border}
                  inputContainerStyle={styles.input_style}
                  placeholder={i18n.t("profile.updatePassword.limitPassword")}
                  label={i18n.t("profile.updatePassword.confirmPassword")}
                  labelStyle={styles.text_label}
                  leftIcon={<Lock />}
                  leftIconContainerStyle={{ paddingLeft: 10 }}
                  rightIconContainerStyle={{ paddingRight: 10 }}
                  onChangeText={(text) => setConfirmPassword(text)}
                  onBlur={validConfirmPassword}
                  style={styles.text_value}
                  secureTextEntry={showConfirmPassword === false}
                  errorMessage={confirmPassError}
                  rightIcon={
                    showConfirmPassword === true ? (
                      <Eye onPress={() => setConfirmShowPassword(false)} />
                    ) : (
                      <EyeClosed onPress={() => setConfirmShowPassword(true)} />
                    )
                  }
                />
              </View>
            </View>
            <View style={styles.container_button}>
              {showLoadingButton ? (
                <View style={styles.loader_button}>
                  <ActivityIndicator
                    style={{ alignSelf: "center" }}
                    color="white"
                  />
                </View>
              ) : (
                <Button
                  type="solid"
                  title={i18n.t("form.button.validate")}
                  titleStyle={styles.buttonTitle}
                  buttonStyle={styles.button}
                  onPress={handleSubmit}
                  disabled={passError !== "" || confirmPassError !== ""}
                />
              )}
            </View>
          </ScrollView>

      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  field_input: {
    flex: 2,
    flexDirection: "column",
    alignItems: "center",
    paddingTop: 60,
    paddingBottom: 20,
  },
  space_input: {
    marginBottom: 38,
  },
  text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 12,
    lineHeight: 22,
    left: 10,
  },
  input_border: {
    minWidth: 324,
    height: 38,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
  },
  input_style: {
    borderRadius: 17,
    borderWidth: 0.5,
    borderColor: "rgba(196, 196, 196, 1)",
    marginBottom: 0,
    borderBottomWidth: undefined,
    height: 38,
  },
  container_button: {
    paddingTop: "38%",
    alignItems: "center",
    justifyContent: "flex-end",
    alignSelf: "center",
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  text_value: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 13,
    lineHeight: 22,
    fontFamily: fontFamily.Medium,
  },
  buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
});
