import React, { useEffect, useState } from "react";

import { ITournee, ITourneeAPIResponse } from "src/interfaces/Entities";

import {
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import { ScrollView } from "react-native-gesture-handler";

import ScooterWait from "assets/ScooterWait.svg";

import { ActivityIndicator } from "react-native-paper";
import { useFocusEffect } from "@react-navigation/native";
import {
  DatabaseReference,
  off,
  onValue,
  ref,
  remove,
} from "firebase/database";
import { db } from "firebase";
import { CardTournee } from "src/components/delivery/cards/tournee";

import { get_tournees } from "src/api/delivery/tournee";
import { authStore } from "src/services/auth_store";
import { tourneeStore } from "src/zustore/tournee_store";
import * as Location from 'expo-location';
import { useTrackLocation } from "src/hooks/useTrackLocation";

export default function Page(){
  const { base_user, push_token } = authStore();
  const [tournees, set_tournees] = useState<ITourneeAPIResponse>({
    total_data: 0,
    total: "0",
    data: [],
  });
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  

  const { should_reload, set_should_reload, set_opened } = tourneeStore();

  const { checkStatusAsync, toggleFetchTask } = useTrackLocation();

  const [bgLocation] = Location.useBackgroundPermissions();
  const [fgLocation] = Location.useForegroundPermissions();

  useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  useEffect(() => {
    if (should_reload === true) {
      load_or_reload_from_backend();
    }
  }, [should_reload]);

  const load_or_reload_from_backend = async () => {
    try {
        setIsLoading(true)
      const tournee_data_from_backend = await get_tournees();

      set_tournees(tournee_data_from_backend);
      
      set_tournees(tournee_data_from_backend);
      if (tournee_data_from_backend.data.length > 0) {
        set_opened(tournee_data_from_backend.data[0]);
      }
      return Promise.resolve(true);

    } catch (error) {     
      setError(true);
      return Promise.resolve(false);
    } finally {
    set_should_reload(false);
      setError(false);
      setIsLoading(false);
    }
  };





  const onRefresh = () => {
    load_or_reload_from_backend()
  };

  useFocusEffect(() => {
    
    if (!base_user?.id) return;
    const reference = ref(db, `actualisationTournee/${base_user.id}/${push_token}`);
    onValue(reference, async (snapshot) => {
      if (snapshot.exists()) {
        try {
          await load_or_reload_from_backend();
          remove(reference as DatabaseReference);
        } catch (e) {
          console.error(e);
        }
      }
    });
    return () => off(reference);
  });

  useFocusEffect(() => {
    const checkStatus = async () => {
      console.log('CHECKING The background tracking');
      
      let isRegistred = await checkStatusAsync();
      //console.log("Course : tracking Status ", isRegistred);
      if (!isRegistred && fgLocation !== null && fgLocation.granted && bgLocation !== null && bgLocation.granted ) 
      {
        toggleFetchTask();
      }
    };

    checkStatus();
  });



  const DataBlock = React.useCallback(() => {
    if (tournees.data.length === 0) {
        return (
          <View style={styles.emptyListContainer}>
            <ScooterWait />
            <Text style={styles.titleText}>Pas encore de tournée</Text>
            <Text style={styles.descriptionText}>
              Attend quelques minutes et une nouvelle tournée apparaîtra
            </Text>
          </View>
        );
      } else {
        return (
            <View style={{ marginTop: 30 }}>
              <TourneeItem
                title="Acceptées"
                show={true}
                items={tournees.data}
              
              />
            </View>
          );
        }
      
    
  }, [
    isLoading,
    tournees,
  ]);

  // ----------------  Render ---------------- //
  if (error) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView
          contentContainerStyle={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
          }
        >
          <View>
            <Text>Erreur de chargement ... </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: 'white' }}>
      <ScrollView
        contentContainerStyle={
          tournees.total_data === 0 ? styles.scrollView : undefined
        }
        onTouchStart={() => {
          //stopAlertSound();
        }}
        refreshControl={
          <RefreshControl refreshing={isLoading} onRefresh={onRefresh} />
        }
      >
        {<DataBlock />}
      </ScrollView>

     
    </SafeAreaView>
  );
};

const TourneeItem = ({
  show,
  items,
}: {
  show: boolean;
  items: Array<ITournee>;
  title: string;
}) => {
  return (
    <View>
      {show &&
        items.map((t: ITournee, index) => (
          <View key={index}>
            <CardTournee tournee={t}  />
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  scrollView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  emptyListContainer: {
    marginTop: 200,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  titleText: {
    paddingTop: 25,
    paddingBottom: 5,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "#8D98A0",
  },
  descriptionText: {
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 12,
    paddingHorizontal: 60,
    color: "#8D98A0",
  },
  typeText: {
    fontSize: 16,
    marginLeft: 29,
    marginBottom: 11,
    fontFamily: fontFamily.semiBold,
  },
  overlayBackground: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
});
