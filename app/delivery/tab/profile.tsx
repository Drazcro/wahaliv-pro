import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  Platform,
  ScrollView,
  Text,
  View,
} from "react-native";
import i18n from "i18n-js";
import { Button, Divider, Switch } from "react-native-elements";

import { images } from "src/theme";
import Constants from "expo-constants";

import ThumbsUp from "assets/image_svg/ThumbsUpRed.svg";
import ScooterArmand from "assets/image_svg/ScooterArmand.svg";
import TimerRed from "assets/image_svg/TimerRed.svg";

import Toast from "react-native-expo-popup-notifications";

import * as Location from "expo-location";

import { ProfileStyle as styles } from "src/styles/delivery/profile_styles";
import { ProfileNavigationItems } from "src/components/delivery/profile/profile_navigation_items";
import { GainsInformations } from "src/components/delivery/profile/gains_informations";
import { ServiceThreshold } from "src/components/delivery/profile/service_threshold";
import { MissingDocs } from "src/components/delivery/profile/missing_docs";
import { authStore } from "src/services/auth_store";
import { logout } from "src/services";
import {
  parse_to_float_number,
  saveTokenToFirebase,
} from "src/services/helpers";
import { StatusBar } from "expo-status-bar";
import {
  get_delivery_information_and_state_for_profile,
  get_delivery_information_for_profile,
  get_is_on_service,
  update_delivery_auto_valid_status,
} from "src/api/delivery/profile";
import { IDeliveryProfile } from "src/interfaces/delivery/profile";
import { ProfileInformations } from "src/components/delivery/profile/profile_informations";
import { tourneeStore } from "src/zustore/tournee_store";
import { useTrackLocation } from "src/hooks/useTrackLocation";
import { useFocusEffect } from "expo-router";

function compareProfile(obj1 : any, obj2 : any) {
  // Get the keys of the objects
  const keys1 = Object.keys(obj1);
  const keys2 = Object.keys(obj2);

  // Check if the number of keys is the same
  if (keys1.length !== keys2.length) {
    console.log('Key not the same');
    return false;
  }

  // Check if the values of each key are the same
  for (let key of keys1) {
    if(key !== "minimum_garanti")
    {
      if (obj1[key] !== obj2[key]) {
        console.log('value not the same : '+key);
        console.log(obj1[key],obj2[key]);
        return false;
      }
    }
    
  }

  // If all keys and values are the same, return true
  return true;
}

export default function ProfileScreen() {
  const [isEnable, setIsEnabled] = useState<boolean>();
  const [profile, setProfil] = useState<IDeliveryProfile>(
    {} as IDeliveryProfile,
  );
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [is_doc_ok, set_is_doc_ok] = useState(true);

  const { auth, base_user } = authStore();
  const steps = tourneeStore((v) => v.steps);
  const { isRegistered,checkStatusAsync, toggleFetchTask } = useTrackLocation();
  

  React.useEffect(() => {
    const checkStatus = async () => {
      let trackingStatus = await checkStatusAsync();
      console.log('trackingStatus',trackingStatus);
    } 
    
    checkStatus();
  }, []);

  useEffect(() => {
    if (auth.access_token && auth.access_token !== "") {
      saveTokenToFirebase(auth);
      get_delivery_information_for_profile();
    }
  }, [auth]);

  const onChangeSwitch = () => {
    setIsEnabled((previousState) => !previousState);

    update_delivery_auto_valid_status({ "is-auto-valid": isEnable })
      .then(() => {
        Toast.show({
          type: "success",
          text1: "Success",
          text2: i18n.t("profile.notification.success.message"),
        });
      })
      .catch(() => setError(true))
      .finally(() => {
        setIsLoading(false);
        setError(false);
      });
  };

  useEffect(() => {
    if (!auth.access_token) {
      return;
    }
    get_delivery_information_and_state_for_profile()
      .then((items: IDeliveryProfile) => {
        //console.log(items);
        if (items !== undefined) {
          setIsEnabled(items.autoValid);
          setProfil(items);
          //console.log(items);
        } else {
          setError(true);
        }
      })
      .catch(() => setError(true))
      .finally(() => {
        setError(false);
        setIsLoading(false);
      });
    Location.getCurrentPositionAsync().catch((e) => {
      console.warn(e);
    });
    get_is_on_service();
  }, [auth.access_token]);

  useFocusEffect(() => {
    if (!auth.access_token) {
      return;
    }
    console.log("Focus fetch");
    get_delivery_information_and_state_for_profile()
      .then((items: IDeliveryProfile) => {
        if (items !== undefined) {
          if (!compareProfile(profile, items)) {
            setProfil(items);
          }
        } else {
          //setError(true);
        }
      });
  });

  if (!auth.access_token) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
        <Button title={"logout"} onPress={logout}>
          Logout
        </Button>
      </View>
    );
  }
  //   unregisterForServiceStatusCheck();
  //   unregisterBackgroundFetchAsync();

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        automaticallyAdjustContentInsets={false}
        overScrollMode={"never"}
      >
        <View style={styles.header}>
          {/* <ImageBackground
            source={images.Landing.Ellipse}
            style={styles.background_image}
            resizeMode="cover"
          ></ImageBackground> */}
          <View style={styles.text_group}>
            <Text style={styles.text_name}>
              {i18n.t("profile.welcome")} {profile?.firstName || profile?.name}
            </Text>
            <Text style={styles.text_name}>{i18n.t("profile.espace")}</Text>
            <Text style={styles.text_email}>{profile?.email}</Text>
          </View>
        </View>
        <View style={styles.group_content}>
          <Text style={styles.informationsTitle}>MES INFORMATIONS DU MOIS</Text>
          <GainsInformations
            unit="€"
            title={i18n.t("profile.Earnings")}
            value={parse_to_float_number(profile.earnings)}
            startText="Dont"
            textValue={`${parse_to_float_number(profile.bonus)} €`}
            endText=" de bonus"
            tipTextStart="Dont"
            tipTextValue={parse_to_float_number(profile?.tipEarning)}
            tipTextEnd="de pourboires"
          />
          <View style={styles.wrapper}>
            <ProfileInformations
              unit="min"
              title={i18n.t("profile.averageDelay")}
              value={parse_to_float_number(profile.averageDelay)}
              icon={<TimerRed />}
              startText="Sur"
              textValue={parse_to_float_number(profile.nbTransactions)}
              endText="transactions"
              tipTextValue={0}
            />
            <ProfileInformations
              unit="%"
              title={i18n.t("profile.deliveryNote")}
              value={parse_to_float_number(profile.ratePercentage)}
              icon={<ThumbsUp />}
              startText="Sur"
              textValue={parse_to_float_number(profile.rateCount)}
              endText="notations"
              tipTextValue={0}
            />
            <ProfileInformations
              title={i18n.t("profile.deliveries")}
              value={Number(profile.coursesCount)}
              icon={<ScooterArmand />}
              tipTextValue={0}
            />
          </View>
          <ServiceThreshold data={profile.minimum_garanti} />

          <View style={styles.line}>
            <Divider color="rgba(196, 196, 196, 1)" />
          </View>
          <View style={{ flex: 1, marginHorizontal: 5 }}>
            <View style={styles.group_accept_auto}>
              <Text style={styles.text_auto}>
                {i18n.t("profile.positionSharingLabel")}
              </Text>
            </View>
            <View style={styles.accept_button}>
              <Text style={styles.text_accept}>
                {i18n.t("profile.positionSharingText")}
              </Text>
              <Text></Text>

              <Switch
                ios_backgroundColor="red"
                trackColor={{
                  true: "rgba(64, 173, 162, 1)",
                  false: "rgba(243, 106, 114, 1)", // Green color for false
                }}
                color={
                  isRegistered === true
                    ? "rgba(64, 173, 162, 1)" // Green color for false
                    : "rgba(243, 106, 114, 1)"
                }
                onValueChange={toggleFetchTask}
                value={isRegistered}
              />
            </View>
          </View>

          <View style={styles.line}>
            <Divider color="rgba(196, 196, 196, 1)" />
          </View>
          {profile.isSalarie !== true ? (
            <React.Fragment>
              <View style={{ flex: 1, marginHorizontal: 5 }}>
                <View style={styles.group_accept_auto}>
                  <Text style={styles.text_auto}>
                    {i18n.t("profile.automaticAssignment")} {profile.isSalarie}
                  </Text>
                </View>
                <View style={styles.accept_button}>
                  <Text style={styles.text_accept}>
                    {i18n.t("profile.automaticAcceptance")}
                  </Text>
                  <Text></Text>
                  <Switch
                    ios_backgroundColor="red"
                    trackColor={{
                      true: "rgba(64, 173, 162, 1)",
                      false: "rgba(243, 106, 114, 1)",
                    }}
                    color={
                      isEnable == true
                        ? "rgba(64, 173, 162, 1)"
                        : "rgba(243, 106, 114, 1)"
                    }
                    onValueChange={onChangeSwitch}
                    value={isEnable}
                  />
                </View>
              </View>

              <View style={styles.line}>
                <Divider color="rgba(196, 196, 196, 1)" />
              </View>
            </React.Fragment>
          ) : null}

          <MissingDocs setDoc={(v) => set_is_doc_ok(v)} />
          <View style={{ marginTop: 10 }}>
            <ProfileNavigationItems
              isSalarie={profile.isSalarie}
              docIsOk={is_doc_ok}
            />
          </View>
        </View>
        {Constants.expoConfig?.version && (
          <Text style={styles.appVersionLabel}>
            V{Constants.expoConfig?.version}
          </Text>
        )}
      </ScrollView>
    </View>
  );
}
