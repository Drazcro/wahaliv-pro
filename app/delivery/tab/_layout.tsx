import {
    Stack,
    Tabs,
    usePathname
} from "expo-router";
import { fontFamily } from "src/theme/typography";
import ScooterIcon from "assets/image_svg/ScooterGrey.svg";
import ScooterRed from "assets/image_svg/ScooterArmand.svg";

import TourneeIcon from "assets/icons/TourneesGray.svg";
import TourneeIconRed from "assets/icons/Tournees.svg";

import CalendarIcon from "assets/image_svg/CalendarTab.svg";
import CalendarRed from "assets/image_svg/CalendarTabRose.svg";

import UserIcon from "assets/image_svg/UserLite.svg";
import UserRed from "assets/image_svg/UserRed.svg";
import React, { useState } from "react";
import { StyleSheet } from "react-native";
import useNotification from "src/hooks/useNotification";
import { useGPS } from "src/hooks/useLocalisation";
import LocationServiceAlert from "src/components/modals/permissions/location_service";
import NotificationServiceAlert from "src/components/modals/permissions/notification_service";
import notifCheckStore from "src/zustore/notifCheckStore";
import * as Location from 'expo-location';





const PATHS = ["/delivery/screens/details", "/tournee_detail"];

export default function DeliveryLayout() {

    const path = usePathname()

    const {push_token, notif} = useNotification()
    //const {FGLocation, BGLocation, isGPSOn} = useGPS()
    const [hasPermission,setHasPermission] = useState(true);
    const [bgLocation] = Location.useBackgroundPermissions();
    const [fgLocation] = Location.useForegroundPermissions();

    const isChecked = notifCheckStore((state) => state.isChecked);

    React.useEffect(()=>{
        console.log(push_token)
        console.log("PERMISSION NOTIF: ",notif.granted)
        //console.log("PERMISSION BG: ",FGLocation.granted)
        //console.log("PERMISSION FG: ",BGLocation.granted)
    }, [push_token])

    React.useEffect( () => {
        console.log('Background status');
        console.log(bgLocation);
        console.log('Fourgournd status');
        console.log(fgLocation);
        if(bgLocation == null || fgLocation == null || !bgLocation?.granted  || !fgLocation?.granted )
            setHasPermission(false);
    },[bgLocation,]); 

    const tabDisplay = React.useMemo(() => {
        if (PATHS.includes(path)) {
            return "none";
        } else {
            return "flex";
        }
    }, [path]);

    // const blockGPS = React.useMemo(()=>{
    //     return [FGLocation.granted, BGLocation.granted].includes(false)
    // }, [FGLocation.granted, BGLocation.granted])

    

    return (
        <React.Fragment>
            <Stack.Screen />
            
            <Tabs
                screenOptions={{
                    headerShown: false,
                    tabBarStyle: {
                        display: tabDisplay,
                    },
                }}
            >
                <Tabs.Screen
                    name="courses"

                    options={{
                        tabBarLabel: "Courses",
                        title: "Courses",
                        tabBarLabelStyle: styles.tabBarLabel,
                        tabBarActiveTintColor: "#000000",
                        headerShown: false,
                        tabBarIcon: ({ focused }) => {
                            if (focused) {
                                return <ScooterRed />;
                            } else {
                                return <ScooterIcon />;
                            }
                        },
                    }}
                />

                <Tabs.Screen
                    name="tournee"
                    options={{
                        tabBarLabel: "Tournée",
                        title: "Tournée",
                        tabBarLabelStyle: styles.tabBarLabel,
                        tabBarActiveTintColor: "#000000",

                        headerShown: false,
                        tabBarIcon: ({ focused }) => {
                            if (focused) {
                                return <TourneeIconRed />;
                            } else {
                                return <TourneeIcon />;
                            }
                        },
                    }}
                />

                <Tabs.Screen
                    name="planning"
                    options={{
                        tabBarLabel: "Planning",
                        title: "Planning",
                        tabBarLabelStyle: styles.tabBarLabel,
                        tabBarActiveTintColor: "#000000",

                        headerShown: false,
                        tabBarIcon: ({ focused }) => {
                            if (focused) {
                                return <CalendarRed />;
                            } else {
                                return <CalendarIcon />;
                            }
                        },
                    }}
                />
                <Tabs.Screen
                    name="profile"
                    options={{
                        tabBarLabel: "Profil",
                        title: "Profil",
                        tabBarLabelStyle: styles.tabBarLabel,
                        tabBarActiveTintColor: "#000000",

                        headerShown: false,
                        tabBarIcon: ({ focused }) => {
                            if (focused) {
                                return <UserRed />;
                            } else {
                                return <UserIcon />;
                            }
                        },
                    }}
                />

            </Tabs>

            {!hasPermission ? <LocationServiceAlert />: null}
            {!notif.granted && !isChecked ? <NotificationServiceAlert />: null}
            
        </React.Fragment>
    );
}

const styles = StyleSheet.create({
    tabBarLabel: {
        fontSize: 11,
        fontFamily: fontFamily.Medium,
        marginTop: -6,
        lineHeight: 20
    }
})

