import React from "react";
import { View, Text, StyleSheet, useWindowDimensions } from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { ClosedCoursesTabView } from "src/components/delivery/closed_courses";
import { CoursesHome } from "src/components/delivery/new_courses";
import { useTrackLocation } from "src/hooks/useTrackLocation";
import { color } from "src/theme";
import { fontFamily } from "src/theme/typography";
import * as Location from 'expo-location';
import { useFocusEffect } from "expo-router";

const TOP_TABS = [
  { key: "first", title: "Nouvelles" },
  { key: "second", title: "Toutes" },
];

const FirstRoute = React.memo(() => {
  return <CoursesHome />;
});

const SecondRoute = React.memo(() => {
  return <ClosedCoursesTabView isOnTournee={false} />;
});
const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

export default function Page() {
  const [tabIndex, setTabIndex] = React.useState(0);
  const layout = useWindowDimensions();
  

  const { checkStatusAsync, toggleFetchTask } = useTrackLocation();

  const [bgLocation] = Location.useBackgroundPermissions();
  const [fgLocation] = Location.useForegroundPermissions();

  useFocusEffect(() => {
    const checkStatus = async () => {
      console.log('CHECKING The background tracking');
      
      let isRegistred = await checkStatusAsync();
      //console.log("Course : tracking Status ", isRegistred);
      if (!isRegistred && fgLocation !== null && fgLocation.granted && bgLocation !== null && bgLocation.granted ) 
      {
        toggleFetchTask();
      }
    };

    checkStatus();
  });

  return (
    <TabView
      style={styles.tabView}
      navigationState={{ index: tabIndex, routes: TOP_TABS }}
      renderTabBar={(props) => (
        <View>
          <View style={styles.tabViewHeader}>
            <Text style={styles.tabViewHeaderLabel}>Mes Courses</Text>
          </View>
          <View>
            <TabBar
              {...props}
              indicatorStyle={styles.tabBarIndicatorStyle}
              style={styles.tabBarStyle}
              renderLabel={({ route, focused }) => (
                <Text
                  style={
                    focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                  }
                >
                  {route.title}
                </Text>
              )}
            />
          </View>
        </View>
      )}
      renderScene={renderScene}
      onIndexChange={setTabIndex}
      initialLayout={{ width: layout.width }}
      keyboardDismissMode="none"
    />
  );
}

const styles = StyleSheet.create({
  tabView: {
    width: "100%",
    backgroundColor: "#fff",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
});
