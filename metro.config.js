// eslint-disable-next-line @typescript-eslint/no-var-requires
const { getDefaultConfig } = require("@expo/metro-config");

/** @type {import('expo/metro-config').MetroConfig} */
module.exports = (async () => {
  const { resolver, transformer } = await getDefaultConfig(__dirname);
  return {
    transformer: {
      ...transformer,
      babelTransformerPath: require.resolve("react-native-svg-transformer"),
    },
    resolver: {
      ...resolver,
      assetExts: resolver.assetExts.filter((ext) => ext !== "svg"),
      sourceExts: [
        ...resolver.sourceExts,
        "jsx",
        "js",
        "ts",
        "tsx",
        "cjs",
        "svg",
      ],
    },
  };
})();
