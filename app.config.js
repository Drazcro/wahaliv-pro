import "dotenv/config";

export default {
  expo: {
    name: "WAHALIV Pro",
    slug: "wahaliv_pro_app",
    version: "1.0.7",
    orientation: "default",
    owner: "sacre-armand-sas",
    scheme: "wahalivpro",
    icon: "./assets/images/icon.png",
    userInterfaceStyle: "automatic",
    jsEngine: "hermes",
    splash: {
      image: "./assets/images/splash.png",
      resizeMode: "contain",
      backgroundColor: "#000000",
    },
    runtimeVersion: {
      policy: "sdkVersion",
    },
    assetBundlePatterns: ["**/*"],
    ios: {
      bundleIdentifier: "com.drcv.wahalivpro",
      supportsTablet: true,
      buildNumber: "1.0.7",
      infoPlist: {
        UIViewControllerBasedStatusBarAppearance: true,
        UIBackgroundModes: ["location", "fetch", "remote-notification"],
        LSApplicationQueriesSchemes: [
          "comgooglemaps",
          "citymapper",
          "waze",
          "tel",
        ],
        NSLocationAlwaysAndWhenInUseUsageDescription:
          "We need access to your location in order to track the delivery remaining time and provide real time information to the client.In other word, the moment the deliverymen start using the app, we need to know where they are in order to prompt them with delivery services from restaurants and clients within certain distance range. and the moment the deliverymen picks up the food, we need to provide realtime ETA to the client and restaurant, thus we need to keep tracking the position of the deliverymen during all the process. No data is saved and the app will stop collecting the moment the deliverymen logs out",
        NSLocationAlwaysUsageDescription:
          "We need access to your location in order to track the delivery remaining time and provide real time information to the client.In other word, the moment the deliverymen start using the app, we need to know where they are in order to prompt them with delivery services from restaurants and clients within certain distance range. and the moment the deliverymen picks up the food, we need to provide realtime ETA to the client and restaurant, thus we need to keep tracking the position of the deliverymen during all the process. No data is saved and the app will stop collecting the moment the deliverymen logs out",
        NSLocationWhenInUseUsageDescription:
          "We need access to your location in order to track the delivery remaining time and provide real time information to the client.In other word, the moment the deliverymen start using the app, we need to know where they are in order to prompt them with delivery services from restaurants and clients within certain distance range. and the moment the deliverymen picks up the food, we need to provide realtime ETA to the client and restaurant, thus we need to keep tracking the position of the deliverymen during all the process. No data is saved and the app will stop collecting the moment the deliverymen logs out",
        NSPhotoLibraryUsageDescription:
          "We must have access to your administrative documents necessary for your remuneration.",
      },
    },
    android: {
      package: "com.drcv.wahalivpro",
      versionCode: 107,
      googleServicesFile: "./google-services.json",
      config: {
        googleMaps: {
          apiKey: "AIzaSyCzDzujJAg4QvtU_F-Cy76BcwkQPkD7TX0",
        },
      },
      adaptiveIcon: {
        foregroundImage: "./assets/images/adaptive-icon.png",
        backgroundColor: "#000000",
      },
      permissions: [
        "ACCESS_FINE_LOCATION",
        "ACCESS_COARSE_LOCATION",
        "ACCESS_BACKGROUND_LOCATION",
        "NOTIFICATIONS",
      ],
    },
    web: {
      favicon: "./assets/favicon.png",
    },
    extra: {
      environment: "dev",
      version: "partner",
      eas: {
        "projectId": "b0904124-db05-4663-91ba-a8a38929ec06"
      }
    },
    plugins: [
      [
        "expo-notifications",
        {
          icon: "./assets/store-icons/notification.png",
          color: "#000000",
          sounds: ["./assets/notification.mp3"],
        },
      ],
      "expo-localization",
      "expo-router",
      "expo-document-picker"
    ],
  },
};
