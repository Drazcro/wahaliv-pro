module.exports = function (api) {
  api.cache(true);
  return {
    presets: ["babel-preset-expo"],
    plugins: [
      [
        "module-resolver",
        {
          root: ["./", "./src", "./assets", "./app"],
          extensions: [
            ".js",
            ".jsx",
            ".ts",
            ".tsx",
            ".android.js",
            ".android.tsx",
            ".ios.js",
            ".ios.tsx",
            ".App.tsx",
            ".svg",
          ],
          alias: {
            underscore: "lodash",
            theme: "./src/theme",
            navigation: "./src/navigation",
            screens: "./src/screens",
            $constants: "./src/constants",
            services: "./src/services",
            session: "./src/session",
            delivery: "./src/delivery",
            partner: "./src/partner",
            assets: "./assets",
            src: "./src",
            app: "./app",
          },
        },
      ],
      "react-native-reanimated/plugin",
      "expo-router/babel",
    ],
  };
};
