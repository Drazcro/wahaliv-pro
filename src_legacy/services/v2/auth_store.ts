/* eslint-disable */

import { IAuth } from "src_legacy/session/types";
import { create } from "zustand";
import { createJSONStorage, persist } from "zustand/middleware";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { API_BASE_URL_DEV, API_NOTIF_BASE_URL } from "src/constants/urls";
import { saveSession } from "src_legacy/session/store";
import { saveTokenToFirebase } from "../index";

export enum ACCOUNT_TYPE {
  "PARTNER",
  "DELIVERY",
  "UNKNOWN",
}

interface State<T> {
  push_token: string;
  auth: IAuth;
  account_type: ACCOUNT_TYPE;
  base_url: string;
  notif_url: string;
  setPushToken: (payload: string) => void;
  setAuth: (payload: IAuth) => void;
  setIsLoading: (payload: boolean) => void;
  setAccountTypePartner: () => void;
  setAccountTypeDelivery: () => void;
  removeAuth: () => void;
  setBaseUrl: (value: string) => void;
  setNotifUrl: (value: string) => void;
}

const vanillaAuthState = create<State<{}>>()(
  persist(
    (set) => ({
      push_token: "",
      auth: { is_loading: true } as IAuth,
      account_type: ACCOUNT_TYPE.UNKNOWN,
      base_url: API_BASE_URL_DEV,
      notif_url: API_NOTIF_BASE_URL,
      setNotifUrl: (url) => set(() => ({ notif_url: url })),
      setBaseUrl: (url) => set(() => ({ base_url: url })),
      setPushToken: (token) =>
        set((state) => ({
          ...state,
          push_token: token.replace("ExponentPushToken[", "").replace("]", ""),
        })),
      setAuth: (payload) => {
        set((state) => ({ auth: payload }));
        if (payload.access_token) {
          saveTokenToFirebase(payload);
          saveSession(payload);
        }
      },

      setIsLoading: (is_loading) =>
        set((state) => ({ auth: { ...state.auth, is_loading } })),
      setAccountTypePartner: () =>
        set((state) => ({ account_type: ACCOUNT_TYPE.PARTNER })),
      setAccountTypeDelivery: () =>
        set((state) => ({ account_type: ACCOUNT_TYPE.DELIVERY })),
      removeAuth: () => {
        set({
          auth: { is_loading: false } as IAuth,
          account_type: ACCOUNT_TYPE.UNKNOWN,
        });
      },
    }),
    {
      name: "auth-persistance",
      storage: createJSONStorage(() => AsyncStorage),
    },
  ),
);

const authStore = vanillaAuthState as {
  <T>(): State<T>;
  <T, U>(selector: (s: State<T>) => U): U;
};

export { vanillaAuthState, authStore };
