import { API_BASE_URL_DEV, API_BASE_URL_PROD } from "../../src/constants/urls";
import { IAuth } from "src_legacy/session/types";
import { authToUserId, getBaseUser, ROLE_TYPE } from "src_legacy/services/Utils";
import { Platform } from "react-native";
import Constants from "expo-constants";
import { vanillaAuthState } from "./v2/auth_store";
import axios from "axios";

enum ACCOUNT_TYPE {
  "PARTNER",
  "DELIVERY",
  "UNKNOWN",
}

export const TEST_USERS = [
  "demo@sacrefernand.com",
  "dev1livrer@gmail.com",
  "demotest@sacrearmand.com",
  "sushi.boat.carcassonne@gmail.com",
  "demoLiv2@sacrearmand.com",
  "burgertest@sacrearmand.com",
  "demo2@sacrearmand.com",
];

export async function login(
  username: string,
  password: string,
  expo_token: string,
  device_brand: string | null,
  device_model_name: string | null,
  device_os_version: string | null,
  device_year: number | null,
  account_type?: string,
): Promise<IAuth> {
  setApiEnv(username);

  const { setState } = vanillaAuthState;

  const url = `${vanillaAuthState.getState().base_url}login`;
  // HttpClient.clearAuthorization();
  const http = axios.create({
    headers: { "Content-Type": "multipart/form-data" },
  });

  console.log("expo token", expo_token);

  const app_pro_os = Platform.OS === "android" ? "android" : "ios";
  const app_pro_version = Constants.expoConfig?.version;

  const formData = new FormData();
  formData.append("username", username);
  formData.append("password", password);
  formData.append("expo_token", expo_token);
  formData.append("device_brand", device_brand);
  formData.append("device_model_name", device_model_name);
  formData.append("device_os_version", device_os_version);
  formData.append("device_year", device_year);
  formData.append("app_pro_os", app_pro_os);
  formData.append("app_pro_version", app_pro_version);

  return http
    .post(url, formData)
    .then((response) => {
      console.log("form data: ", formData);
      if (response && response.status === 200) {
        console.log(response.status);
        const auth_data: IAuth = response.data;
        const user = getBaseUser(auth_data);
        console.log("user", user);
        console.log("auth_data", auth_data);

        console.log(
          "check: ",
          user.roles,
          ROLE_TYPE.ROLE_COURSIER,
          ACCOUNT_TYPE.DELIVERY,
        );
        if (user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
          setState({ account_type: ACCOUNT_TYPE.DELIVERY });
        } else if (user.roles.includes(ROLE_TYPE.ROLE_PARTENAIRE)) {
          setState({ account_type: ACCOUNT_TYPE.PARTNER });
        } else {
          return Promise.reject();
        }

        auth_data.is_loading = false;
        vanillaAuthState.getState().setAuth(auth_data);
        // HttpClient.setAuthorization(auth_data.access_token);
        auth_data.access_token = response.data.token;

        console.log("************************");
        return Promise.resolve(auth_data);
      }
      return Promise.reject();
    })
    .catch((err) => {
      console.log("login error:; ", err);
      
      return Promise.reject();
    });
}

export function setApiEnv(username: string) {
  if (
    TEST_USERS.includes(username) ||
    (username.includes("demo") && username.includes("@sacrearmand.com"))
  ) {
    vanillaAuthState.getState().base_url = API_BASE_URL_DEV;
  } else {
    vanillaAuthState.getState().base_url = API_BASE_URL_PROD;
  }
}

