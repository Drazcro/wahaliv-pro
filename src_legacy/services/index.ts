import jwtDecode from "jwt-decode";
import { IAuth } from "src_legacy/session/types";
import { Platform } from "react-native";
import { clearAuth } from "src_legacy/session/store";
import Constants from "expo-constants";
import * as Device from "expo-device";
import { ref, set } from "firebase/database";
import { db } from "firebase";
import * as Notifications from "expo-notifications";
//import * as Sentry from "@sentry/react-native";
import { vanillaAuthState } from "./v2/auth_store";

export interface IBaseUser {
  email: string;
  id: number;
  iat: number;
  exp: number;
  isSalarie: boolean;
  is_latest_version: boolean;
  roles: string[];
}

export enum ROLE_TYPE {
  ROLE_PARTENAIRE = "ROLE_PARTENAIRE",
  ROLE_COURSIER = "ROLE_COURSIER",
  ROLE_PARTICULIER = "ROLE_PARTICULIER",
}

export function getBaseUser(auth: IAuth): IBaseUser {
  try {
    const user = jwtDecode(auth.access_token) as IBaseUser;
    return user;
  } catch (e) {
    console.error(e, "61", auth);
    logout();
    return {} as IBaseUser;
  }
}

export const logout = async () => {
  clearAuth();
  vanillaAuthState.getState().removeAuth();
};

export function authToUserId(auth: IAuth): number {
  try {
    const user = jwtDecode(auth.access_token) as IBaseUser;
    return user.id;
  } catch (e) {
    console.error(e, "49", auth);
    logout();
    return -1;
  }
}

export const get_device_info = () => {
  const app_pro_os = Platform.OS === "android" ? "android" : "ios";
  const app_pro_version = Constants.expoConfig?.version;
  const device_brand = Device.brand;
  const device_model_name = Device.modelName;
  const device_os_version = Device.osVersion;
  const device_year = Device.deviceYearClass;

  const expo_token = `ExponentPushToken[${
    vanillaAuthState.getState().push_token
  }]`;
  const base_user = getBaseUser(vanillaAuthState.getState().auth);

  console.log("expo token test: ", expo_token);
  const response: any = {
    app_pro_os,
    app_pro_version,
    device_brand,
    device_model_name,
    device_os_version,
    device_year,
    base_user,
  };
  if (expo_token !== "ExponentPushToken[]") {
    response["expo_token"] = expo_token;
  }
  return response;
};

export function saveTokenToFirebase(auth: IAuth) {
  const base_user = getBaseUser(auth);
  if (auth.access_token && auth.access_token.length > 0) {
    for (const role of base_user.roles) {
      if (role === ROLE_TYPE.ROLE_PARTICULIER) {
        continue;
      }

      const db_ref = ref(db, `access_tokens/${role}/${authToUserId(auth)}/`);
      const device_brand = Device.brand;
      const device_model_name = Device.modelName;
      const device_os_version = Device.osVersion;
      const device_year = Device.deviceYearClass;
      const app_pro_os = Platform.OS === "android" ? "android" : "ios";
      const app_pro_version = Constants.expoConfig?.version;
      Notifications.getExpoPushTokenAsync({
        //applicationId: "@helodev/sacrearmand_pro_app",
        projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
      })
        .then((response) => {
          console.log("response:", response.data);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            push_token: response.data,
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
        })
        .catch((e) => {
          console.error(e);
          set(db_ref, {
            access_token: auth.access_token,
            refresh_token: auth.refresh_token,
            latest_update: new Date(),
            device_brand,
            device_model_name,
            device_os_version,
            device_year,
            app_pro_os,
            app_pro_version,
            role: base_user.roles,
          });
          //Sentry.captureException(e);
        });
    }
  }
}
