import { COURSE_STATUS } from "src/constants/course";
import {
  ICoordonnateBank,
  ICourse,
  IProfileInformation,
  IProfile,
  IInformation,
  IUpdatePassword,
  IDocumentJustificatif,
  IReverseEarnings,
  IPlannings,
  IPlanningsSummary,
  IPlanningDisengageStatus,
  IPartnerProfile,
  IPartnerProfilePassword,
  IPartnerOrderData,
  IPartnerOrderDetails,
  ORDER_STATUS,
  IHour,
  IPartnerProfileEarlyClosings,
  IPartnerProfileUpdateDate,
  IOrderProducts,
  IOrderRefund,
  IOrderTradeDiscount,
  IBillsDetails,
  ITourneeAPIResponse,
  ITourneeDetail,
  IPartnerData,
  IInvoiceAPIResponse,
  ICourseV2Data,
  ICollectHour,
  IPartnerBill,
  ICustomerBillData,
  IPartnerCustomer,
  IFinancialImpact,
  IContact,
  ProfileInformations,
  AdminJusti,
  BankingInformations,
  UniversType,
  NewCollaborator,
  WeekSchedule,
  ParametersDetails,
} from "src/interfaces/Entities";
import Toast from "react-native-expo-popup-notifications";
import { http, no_auth_http } from "src/services/http";
import { IAuth } from "src_legacy/session/types";
import { vanillaProfileStore } from "src_legacy/zustore/profilestore";
import { vanillaCourseStore } from "src_legacy/zustore/coursestore";
import { vanillaPartnerStore } from "src_legacy/zustore/partnerstore";
import { vanillaPaginationStore } from "src_legacy/zustore/paginationstore";
import { INTERVAL_PAGE } from "src/constants/default_values";
import { ROLE_TYPE, authToUserId, get_device_info } from "./Utils";
import { TOURNEE_STATUS } from "src/constants/tournee";
import { vanillaSoundStore } from "src_legacy/zustore/sound_store";
import { vanillaTourneeStore } from "src_legacy/zustore/tourneestore";
import { getExpoPushTokenAsync } from "expo-notifications";

const { setState: setPartnerProfile } = vanillaPartnerStore;
const { setState: setProfile } = vanillaProfileStore;
const { setState: setCourse, getState: actualCourse } = vanillaCourseStore;

export type KeyValue = Record<string, string | number | boolean | unknown>;
export interface IHttpData {
  url: string;
  data: KeyValue;
}
//import * as Sentry from "@sentry/react-native";
import { vanillaAuthState } from "./v2/auth_store";

//////////////////// Commande Request ////////////////////

export async function accept_cmd(id: string) {
  const url = `${vanillaAuthState.getState().base_url}commande/accept/${id}`;

  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      const message: string = response.data;
      return Promise.resolve(message);
    }
    return Promise.reject();
  });
}

export async function cancel_or_refuse_cmd(id: string) {
  const url = `${vanillaAuthState.getState().base_url}commande/cancel/${id}`;

  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      const message: string = response.data;
      return Promise.resolve(message);
    }
    return Promise.reject();
  });
}

export async function get_order_details(id: number): Promise<ICourse> {
  const url = `${vanillaAuthState.getState().base_url}commande/detail/${id}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const courses: ICourse = response.data;
      return Promise.resolve(courses);
    }
    return Promise.reject();
  });
}

export async function get_order_details_for_partner(
  id: number,
): Promise<IPartnerData> {
  const url = `${vanillaAuthState.getState().base_url}commande/detail/${id}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const order: IPartnerData = response.data;
      return Promise.resolve(order);
    }
    return Promise.reject();
  });
}

export async function finished_cmd(id: number) {
  const url = `${vanillaAuthState.getState().base_url}commande/finished/${id}`;

  return http.post({ url: url, data: { "": "" } });
}

export async function get_hours_collected(id: number): Promise<IHour> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}commande/hours-collecte/${id}`).then((response) => {
    if (response && response.status === 200) {
      const orders_data: IHour = response.data;
      return Promise.resolve(orders_data);
    }
    return Promise.reject();
  });
}

export async function get_hours_collected_v2(
  id: number,
): Promise<ICollectHour> {
  const url = `${
    vanillaAuthState.getState().base_url
  }v2/commande/hours-collecte/${id}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const res: ICollectHour = response.data;
      //console.info("URL: ", url);
      //console.log("response", res);
      return Promise.resolve(res);
    }
    return Promise.reject();
  });
}

export async function update_hours_collected(
  id: number,
  hour: string,
  day: unknown,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }commande/update-collecte-hour/${id}`;
  const newHourAccepted = "L'heure de collecte a bien été modifiée";
  //const errorMessage = "Erreur de changement d'heure";

  return http.post({ url: url, data: { day, hour } }).then((response) => {
    if (response && response.status === 200) {
      vanillaPartnerStore.getState().updateCollecteHourById(id, hour);
      // showToast("success", newHourAccepted);
      //vanillaToastStore.getState().showToast("valide", newHourAccepted, "");
      return Promise.resolve(true);
    }
    // showToast("error", errorMessage);
    // vanillaToastStore
    //   .getState()
    //   .showToast(
    //     "error",
    //     "Veuillez vérifier votre connexion internet",
    //     "Une erreur est survenue",
    //   );
    return Promise.reject();
  });
}

export async function update_hours_collected_v2(
  id: number,
  hour: string,
  begin: string,
  end: string,
  collecte: string,
  delivery: string,
  collecte_distance: number,
  delivery_distance: number,
  deliveryman: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }v2/commande/update-collecte-hour/${id}`;
  const newHourAccepted = "L’heure de collecte a bien été modifié.";
  return http
    .post({
      url: url,
      data: {
        begin,
        end,
        collecte,
        delivery,
        collecte_distance,
        delivery_distance,
        deliveryman,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        vanillaPartnerStore.getState().updateCollecteHourById(id, hour);
        // vanillaToastStore
        //   .getState()
        //   .showToast("valide", newHourAccepted, "Horaire modifié");
        return Promise.resolve(true);
      }
      // vanillaToastStore
      //   .getState()
      //   .showToast(
      //     "error",
      //     "Veuillez vérifier votre connexion internet",
      //     "Une erreur est survenue",
      //   );
      return Promise.reject();
    });
}

export async function get_financial_history(
  id: number,
  year: string,
  month: string,
) {

  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/impact-financier?custom_date=${year}-${month}&id=${id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const financials: IFinancialImpact = response.data[0];
      return Promise.resolve(financials);
    }

    return Promise.reject();
  });
}

//////////////////// Course Request ////////////////////

export async function get_new_course(id: number): Promise<ICourse[]> {
  const URL = vanillaAuthState.getState().base_url;
  const status = COURSE_STATUS.TO_BE_ACCEPTED;

  return http
    .get(`${URL}deliverymen/${id}/course/new/${status}`)
    .then((response) => {
      if (response && response.status === 200) {
        const courses: ICourse[] = response.data;
        return Promise.resolve(courses);
      }
      return Promise.reject();
    });
}

export async function get_new_course_v2(id: number): Promise<ICourseV2Data> {
  const URL = vanillaAuthState.getState().base_url;
  return http.get(`${URL}deliverymen/${id}/course/v2/new`).then((response) => {
    if (response && response.status === 200) {
      const res: ICourseV2Data = response.data;
      if (res) {
        return Promise.resolve(res);
      }
    }
    return Promise.reject();
  });
}

export async function get_accepted_course(id: number): Promise<ICourse[]> {
  const URL = vanillaAuthState.getState().base_url;
  const status = COURSE_STATUS.ACCEPTED;

  return http
    .get(`${URL}deliverymen/${id}/course/new/${status}`)
    .then((response) => {
      if (response && response.status === 200) {
        const courses: ICourse[] = response.data;
        return Promise.resolve(courses);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

export async function get_recovered_course(id: number): Promise<ICourse[]> {
  const URL = vanillaAuthState.getState().base_url;
  const status = COURSE_STATUS.RECOVERED;

  return http
    .get(`${URL}deliverymen/${id}/course/new/${status}`)
    .then((response) => {
      if (response && response.status === 200) {
        const courses: ICourse[] = response.data;
        return Promise.resolve(courses);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

export async function get_finished_course(id: number): Promise<ICourse[]> {
  const URL = vanillaAuthState.getState().base_url;
  const status = COURSE_STATUS.FINISHED;

  return http
    .get(`${URL}deliverymen/${id}/course/new/${status}`)
    .then((response) => {
      if (response && response.status === 200) {
        const courses: ICourse[] = response.data;
        return Promise.resolve(courses);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

export async function accept_course(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/accept/${id}`;
  const acceptedCourse = "Course acceptée";
  const message = "La course viens d’être ajoutée sur ta tournée";
  //const errorMessage = "Une erreur est survenue";

  return http.post({ url: url, data: { id: "" } }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", acceptedCourse);
      vanillaToastStore.getState().showToast("valide", message, acceptedCourse);
      return Promise.resolve(true);
    }
    // showToast("error", errorMessage);
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject();
  });
}

export async function cancel_course(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/cancel/${id}`;
  const refusedToast = "Course refusée";
  //const errorMessage = "Une erreur est survenue";
  return http.post({ url: url, data: { id: "" } }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", refusedToast);
      vanillaToastStore
        .getState()
        .showToast(
          "error",
          "Le refus de la course va impacter ton bonus mensuel.",
          refusedToast,
        );
      return Promise.resolve(true);
    }
    // showToast("error", errorMessage);
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );

    return Promise.reject();
  });
}

export async function get_course_details(id: number): Promise<ICourse> {
  const url = `${vanillaAuthState.getState().base_url}courses/detail/${id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const course: ICourse = response.data;
      setCourse({ courseEnCours: course });
      return Promise.resolve(course);
    }
    return Promise.reject();
  });
}

export async function deliver_course(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/finished/${id}`;
  const deliveredCourse = "Course Livrée";

  return http.post({ url: url, data: { id: "" } }).then((response) => {
    if (response && response.status === 200) {
      //setCourse({courseEnCours: {...actualCourse().courseEnCours, delivery_status: COURSE_STATUS.FINISHED}})
      // showToast("success", deliveredCourse);
      vanillaToastStore.getState().showToast("valide", deliveredCourse, "");
      return Promise.resolve();
    }
    return Promise.reject();
  });
}

export async function retrieved_cmd(id: number) {
  const url = `${vanillaAuthState.getState().base_url}courses/retrieve/${id}`;
  const recoveredCourse = "course récupérée";
  const { need_real_price, newPrice } = actualCourse().courseEnCours;

  const data = {};
  const convertPrice = newPrice && newPrice.replace(",", ".");
  const newData = {
    real_price: convertPrice && parseFloat(convertPrice) * 100,
  };

  if (need_real_price && newPrice === undefined) {
    return;
  }

  return http
    .post({
      url: url,
      data: need_real_price ? newData : data,
    })
    .then((response) => {
      if (response && response.status === 200) {
        setCourse({
          courseEnCours: {
            ...actualCourse().courseEnCours,
            delivery_status: COURSE_STATUS.RECOVERED,
          },
        });
        // showToast("success", recoveredCourse);
        vanillaToastStore.getState().showToast("valide", recoveredCourse, "");
        return Promise.resolve();
      }
      return Promise.reject();
    });
}

//////////////////// DeliveryMan Request ////////////////////

export async function recover_new_course(id: string, status: string) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/course/new/${status}`;

  return http.get(url);
}

export async function recover_daily_bill(
  id: number,
  begin_date: string,
  end_date: string,
  page: number,
  page_size = 80,
): Promise<IInvoiceAPIResponse> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/daily-bill?begin_date=${begin_date}&end_date=${end_date}&page_size=${page_size}&page_number=${page}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const invoice: IInvoiceAPIResponse = response.data;
      //console.log(invoice.global_data.length);
      return Promise.resolve(invoice);
    }
    return Promise.reject();
  });
}

export async function finished_daily_bill(
  id: number,
  begin_date: string,
  end_date: string,
  page: number,
  page_size = 40,
): Promise<IBillsDetails[]> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/finished-bill?begin_date=${begin_date}&end_date=${end_date}&page_size=${page_size}&page_number=${page}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const finishedBill = response.data;
      console.log("!!!!!!!!!!");
      //console.log(finishedBill);

      ////console.log(response.request?.);
      //finishedBill.map(e=>//console.error(e.begin_date.date.split(' ')[0], e.deliveryman_bill_amount, e.deliveryman_bill_id))
      return Promise.resolve(finishedBill.data);
    }
    //console.log(response.status)
    return Promise.reject();
  });
}

export async function get_deliveryman_docs() {
  const URL = vanillaAuthState.getState().base_url;
  const id = authToUserId(vanillaAuthState.getState().auth);
  return http
    .get(`${URL}deliverymen/${id}/document-justificatif`)
    .then((response) => {
      if (response && response.status === 200) {
        const document: IDocumentJustificatif = response.data;
        return Promise.resolve(document);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

export async function upload_document(id: string, type: IDocumentJustificatif) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/document/${type}`;

  return http.post({ url: url, data: { ...type } });
}

export async function download_bill(id: number, bill_id: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/download/bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}
export async function download_partner_bill(id: number, bill_id: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/download/bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function download_customer_bill(id: number, bill_id: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/download/customer-bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function download_recap_bill(id: number, bill_id: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/download/recap-bill/${bill_id}`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const downloadUrl = response.data.url;
      return Promise.resolve(downloadUrl);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function recover_invoice_bill(
  id: number,
): Promise<IBillsDetails[]> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/finished-bill`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const finishedBill = response.data;
      //console.log(finishedBill);
      ////console.log(response.request?.);
      //finishedBill.map(e=>//console.error(e.begin_date.date.split(' ')[0], e.deliveryman_bill_amount, e.deliveryman_bill_id))
      return Promise.resolve(finishedBill.data);
    }
    return Promise.reject();
  });
}

export async function update_bank_information(
  id: number,
  payload: ICoordonnateBank,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/information-bancaires-update`;

  return http.post({ url: url, data: { ...payload } });
}

export async function update_delivery_information(
  id: number,
  payload: IInformation,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/information-update`;
  console.log("payload: ", payload);
  return http.post({ url: url, data: { ...payload } });
}

export async function recover_delivery_information_for_profile(
  id: number,
): Promise<IProfileInformation> {
  const url = vanillaAuthState.getState().base_url;

  return http.get(`${url}deliverymen/${id}/informations`).then((response) => {
    if (response && response.status === 200) {
      const informations: IProfileInformation = response.data;
      setProfile({ profile: informations });
      return Promise.resolve(informations);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function get_is_on_service(id: number): Promise<boolean> {
  const url = vanillaAuthState.getState().base_url;

  return http.get(`${url}deliverymen/${id}/in-service/5`).then((response) => {
    if (response && response.status === 200) {
      setProfile({ isOnService: response.data.in_service });
      // if (Platform.OS !== "web") {
      //   if (response.data.in_service === true) {
      //     // registerBackgroundFetchAsync();
      //   } else {
      //     // unregisterBackgroundFetchAsync();
      //   }
      // }
      return Promise.resolve(response.data.in_service);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export async function fetch_is_on_service(id: number): Promise<boolean> {
  const url = vanillaAuthState.getState().base_url;

  return http.get(`${url}deliverymen/${id}/in-service/5`).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data.in_service);
    }
    return Promise.reject();
  });
}

export async function recover_delivery_information_and_state_for_profile(
  id: number,
): Promise<IProfile> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}deliverymen/${id}/informations-and-stats`)
    .then((response) => {
      if (response && response.status === 200) {
        const profile: IProfile = response.data;
        vanillaProfileStore.setState({ stats: profile });
        return Promise.resolve(profile);
      }
      return Promise.reject();
    });
}

export async function recover_delivery_bank_information_for_profile(
  id: number,
): Promise<ICoordonnateBank> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}deliverymen/${id}/informations-bancaires`)
    .then((response) => {
      if (response && response.status === 200) {
        const banks: ICoordonnateBank = response.data;
        return Promise.resolve(banks);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

export async function update_delivery_auto_valid_status(
  id: number,
  payload: KeyValue,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/is-auto-valid`;

  return http.post({ url: url, data: payload });
}

export async function update_deliver_password(
  id: number,
  payload: IUpdatePassword,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/password`;

  return http.post({ url: url, data: { ...payload } });
}

export async function payback_deliver_and_generate_invoice(
  id: number,
  marketplace: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/payback`;

  return http.post({ url: url, data: { marketplace } });
}

export async function recover_delivery_information_for_payback(
  id: number,
  marketplace: number,
) {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}deliverymen/${id}/payback-informations/${marketplace}`)
    .then((response) => {
      if (response && response.status === 200) {
        const receipts: IReverseEarnings = response.data;
        return Promise.resolve(receipts);
      }
      if (response) {
        //console.log(response.status);
      }
      return Promise.reject();
    });
}

// ------- Planning -------- //
export async function recover_delivery_planning(
  id: number,
  marketplace: number,
  date: string,
): Promise<IPlannings> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/planning?date=${date}&marketplace=${marketplace}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const plannings: IPlannings = response.data;
      return Promise.resolve(plannings);
    }
    // showToast("error", "Une erreur est survenue");
    // vanillaToastStore
    //   .getState()
    //   .showToast(
    //     "error",
    //     "Veuillez vérifier votre connexion internet",
    //     "Une erreur est survenue",
    //   );
    return Promise.reject();
  });
}

export async function recover_delivery_planning_summary(
  id: number,
  marketplace: number,
): Promise<IPlanningsSummary> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/planning-summary?marketplace=${marketplace}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const summary: IPlanningsSummary = response.data;
      console.log("summary data", response.data);
      return Promise.resolve(summary);
    }
    // showToast("error", "Une erreur est survenue");
    // vanillaToastStore
    //   .getState()
    //   .showToast(
    //     "error",
    //     "Veuillez vérifier votre connexion internet",
    //     "Une erreur est survenue",
    //   );
    return Promise.reject();
  });
}

export async function recover_delivery_planning_summary_disengage_status(
  userId: number,
  idCreneaux: number | undefined,
): Promise<IPlanningDisengageStatus> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${userId}/disengage-status/${idCreneaux}`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const summary: IPlanningDisengageStatus = response.data;
      return Promise.resolve(summary);
    }
    // showToast("error", "Une erreur est survenue");
    // vanillaToastStore
    //   .getState()
    //   .showToast(
    //     "error",
    //     "Veuillez vérifier votre connexion internet",
    //     "Une erreur est survenue",
    //   );
    return Promise.reject();
  });
}

// ------- Course -------- //
export async function search_course(id: string) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/search`;

  return http.post({ url: url, data: { "": "" } });
}

export async function sort_course(id: string) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/sort`;

  return http.post({ url: url, data: { "": "" } });
}

export async function update_delivery_firebase_token(id: string) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/update-firebase-token`;

  return http.post({ url: url, data: { "": "" } });
}

export async function generate_token_for_back_office_redirection(
  id: string,
  type: string,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }api/deliverymen/${id}/${type}/token`;

  return http.get(url);
}

export async function send_password_reset_email(email: string) {
  const url = `${vanillaAuthState.getState().base_url}users/reset-password`;
  const marketplace = 5;
  return http.post({ url, data: { email, marketplace } }).then((response) => {
    if (response && response.status === 200) {
      showToast("success", "Email Envoyé");
      //vanillaToastStore.getState().showToast("valide", "Email Envoyé", "");
      return Promise.resolve();
    }
    // showToast("error", "Une erreur est survenue");
    // vanillaToastStore
    //   .getState()
    //   .showToast(
    //     "error",
    //     "Veuillez vérifier votre connexion internet",
    //     "Une erreur est survenue",
    //   );
    return Promise.reject();
  });
}

export async function get_new_access_token_with_refresh(refresh_token: string) {
  const url = `${vanillaAuthState.getState().base_url}token/refresh`;
  const data = { refresh_token };
  return no_auth_http.post({ url, data }).then((response) => {
    if (response && response.status === 200) {
      const { token, refresh_token: new_refresh_token } = response.data;
      const iauth: IAuth = {
        access_token: token,
        refresh_token: new_refresh_token,
        is_loading: false,
      };
      vanillaAuthState.getState().setAuth(iauth);
      return Promise.resolve(iauth);
    }
    if (response) {
      //console.log(response.status);
    }
    return Promise.reject();
  });
}

export const showToast = (type: string, text: string) => {
  let title = "";
  switch (type) {
    case "success":
      title = "Success";
      break;
    case "error":
      title = "Erreur";
      break;
    default:
      title = "Information";
  }
  return Toast.show({
    type: type,
    text1: title,
    text2: text,
  });
};

// ----------------------------------------------- //
// -------------- PARTNER ORDERS ----------------- //
// ----------------------------------------------- //

const { setState: setPartnerStore } = vanillaPartnerStore;

export async function PartnerRecoveredAllCourse(
  id: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}partenaire/${id}/last/commandes/0`)
    .then((response) => {
      if (response && response.status === 200) {
        const course: IPartnerOrderData = response.data;

        return Promise.resolve(course);
      }
      return Promise.reject();
    });
}

export async function get_new_orders_for_partner(
  id: number,
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.NEW}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        //console.log(orders_data)
        setPartnerStore({
          newOrders: orders_data.data,
        });

        vanillaPaginationStore
          .getState()
          .setTotalPagesValidated(parseInt(orders_data.total));
        //setPlaySound({ playSound: orders_data.total > 0 });
        try {
          if (orders_data.data.length > 0) {
            console.log("+++++++++++++++++++++");
            vanillaSoundStore.getState().playSound();
          } else {
            console.log("--------------------");
            vanillaSoundStore.getState().stopSound();
          }
        } catch (err) {
          console.error(err);
        }
        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function PartnerRecoveredNewCourse(
  id: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}partenaire/${id}/last/commandes/5`)
    .then((response) => {
      if (response && response.status === 200) {
        const course: IPartnerOrderData = response.data;
        return Promise.resolve(course);
      }
      return Promise.reject();
    });
}

export async function PartnerRecoveredCourseAccepted(
  id: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}partenaire/${id}/last/commandes/4`)
    .then((response) => {
      if (response && response.status === 200) {
        const course: IPartnerOrderData = response.data;
        return Promise.resolve(course);
      }
      return Promise.reject();
    });
}

export async function get_accepted_orders_for_partner(
  id: number,
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.ACCEPTED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        setPartnerStore({ acceptedOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesAccepted(parseInt(orders_data.total));

        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function get_delivered_orders_for_partner(
  id: number,
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.DELIVERED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        setPartnerStore({ deliveredOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesDelivered(parseInt(orders_data.total));
        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function get_refused_orders_for_partner(
  id: number,
  currentPage: number,
  pageLimit: number,
): Promise<IPartnerOrderData> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(
      `${url}partenaire/${id}/new/commandes/${ORDER_STATUS.CANCELLED}?page_number=${currentPage}&page_size=${pageLimit}`,
    )
    .then((response) => {
      if (response && response.status === 200) {
        const orders_data: IPartnerOrderData = response.data;
        setPartnerStore({ cancelledOrders: orders_data.data });
        vanillaPaginationStore
          .getState()
          .setTotalPagesRefused(parseInt(orders_data.total));

        return Promise.resolve(orders_data);
      }
      return Promise.reject(null);
    });
}

export async function GetPartnerOrderDetails(
  id: number,
): Promise<IPartnerOrderDetails> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}commande/detail/${id}`).then((response) => {
    if (response && response.status === 200) {
      const course: IPartnerOrderDetails = response.data;
      return Promise.resolve(course);
    }
    return Promise.reject();
  });
}

export async function PartnerOrderProducts(
  id: number,
): Promise<IOrderProducts> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}commande/products/${id}`).then((response) => {
    if (response && response.status === 200) {
      const products: IOrderProducts = response.data;
      return Promise.resolve(products);
    }
    return Promise.reject();
  });
}

export async function PartnerOrderRefund(id: number, payload: IOrderRefund) {
  const url = `${vanillaAuthState.getState().base_url}commande/refund/${id}`;

  return http.post({ url: url, data: { ...payload } }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data.message);
    }
    return Promise.reject();
  });
}

export async function PartnerOrderFinished(order: IPartnerData) {
  const id = order.commande.id;
  const needRealPrice = order.commande.need_real_price;
  const real_price = order.newPrice;
  const convertPrice = real_price && real_price.replace(",", ".");
  const data = needRealPrice
    ? { real_price: convertPrice && parseFloat(convertPrice) * 100 }
    : {};
  const url = `${vanillaAuthState.getState().base_url}commande/finished/${id}`;
  return http.post({ url: url, data });
}

export async function PartnerOrderPrint(id: number) {
  const url = `${vanillaAuthState.getState().base_url}commande/print/${id}`;

  return http.post({ url: url, data: {} });
}

export async function PartnerOrderCanceled(id: number) {
  const url = `${vanillaAuthState.getState().base_url}commande/cancel/${id}`;

  return http.post({ url: url, data: {} });
}

export async function PartnerOrderTradeDiscount(
  id: number,
  payload: IOrderTradeDiscount,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }commande/geste-commercial/${id}`;

  return http.post({ url: url, data: { ...payload } });
}

// ----------------------------------------------- //
// -------------- PARTNER PROFILE ---------------- //
// ----------------------------------------------- //

export async function PartnerProfileEarlyClosing(
  id: number,
): Promise<IPartnerProfileEarlyClosings> {
  const url = vanillaAuthState.getState().base_url;
  return http.get(`${url}partenaire/${id}/opening`).then((response) => {
    if (response && response.status === 200) {
      const times: IPartnerProfileEarlyClosings = response.data;
      return Promise.resolve(times);
    }
    return Promise.reject();
  });
}

export async function PartnerProfileEarlyClosingUpdate(
  id: number,
  payload: IPartnerProfileUpdateDate,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/update-opening`;

  return http.post({ url: url, data: { ...payload } });
}

export async function PartnerProfileEarlyClosingDelete(
  id: number,
  dataId: string | undefined,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/delete-opening/${dataId}`;

  return http.del({ url: url, data: {} });
}

export async function partner_profile_information_and_state(
  id: number,
): Promise<IPartnerProfile> {
  const url = vanillaAuthState.getState().base_url;
  return http
    .get(`${url}partenaire/${id}/informations-and-stats`)
    .then((response) => {
      if (response && response.status === 200) {
        const profile: IPartnerProfile = response.data;
        setPartnerProfile({ partnerProfile: profile });
        return Promise.resolve(profile);
      }
      return Promise.reject();
    });
}

export async function PartnerProfileUpdatePassword(
  id: number,
  payload: IPartnerProfilePassword,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/password`;

  return http.post({ url: url, data: { ...payload } });
}

// ----------------------------------------------- //
// ----------------- TOURNEES -------------------- //
// ----------------------------------------------- //

export async function load_tournee_data(
  id: number,
): Promise<ITourneeAPIResponse> {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id}/tournees`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const tournee: ITourneeAPIResponse = response.data;
      const enabled_tournee = tournee.data.filter(
        (item) => item.status == TOURNEE_STATUS.ACCEPTED,
      );
      tournee.data = enabled_tournee;
      return Promise.resolve(tournee);
    }
    return Promise.reject(null);
  });
}

export async function GetDetailTournee(id: number): Promise<ITourneeDetail> {
  const url = `${vanillaAuthState.getState().base_url}tournees/${id}/steps`;
  //console.log("getting details up", url);
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const tourneeDetail: ITourneeDetail = response.data;
      vanillaTourneeStore.setState({
        steps: tourneeDetail.steps,
      });

      //vanillaTourneeStore.setState({selectedTournee: tourneeDetail})
      return Promise.resolve(tourneeDetail);
    }
    return Promise.reject(null);
  });
}

export async function AccepteTournee(id: number) {
  const url = `${vanillaAuthState.getState().base_url}tournee/${id}/accept`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournée acceptée");
      vanillaToastStore
        .getState()
        .showToast("valide", "La tournée à été acceptée", "Tournée acceptée");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject(null);
  });
}

export async function RefuseTournee(id: number) {
  const url = `${vanillaAuthState.getState().base_url}tournee/${id}/cancel`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournée refusée");
      vanillaToastStore
        .getState()
        .showToast("error", "Vous avez refusée la tournée", "Tournée Refusée");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject(null);
  });
}

export async function complete_step(id: number, msg: string, title: string) {
  const url = `${vanillaAuthState.getState().base_url}step/${id}/retrieve`;
  //console.log("+++++++++retrieving up", url);
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", msg);
      vanillaToastStore.getState().showToast("valide", msg, title);

      return Promise.resolve(true);
    }
    // showToast("error", "Une erreur est survenue");
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Une erreur est survenue, veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject(null);
  });
}

export async function CanncelTourneeStep(id: number) {
  const url = `${vanillaAuthState.getState().base_url}step/${id}/cancel`;
  return http.post({ url: url, data: {} }).then((response) => {
    if (response && response.status === 200) {
      // showToast("success", "Tournee annulée");
      vanillaToastStore.getState().showToast("valide", "Tournee annulée", "");
      return Promise.resolve(response);
    }
    // showToast("error", "Une erreur est survenue");
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject(null);
  });
}

export async function ready_to_go_to_pick_up(
  id: number,
  lat: number,
  lng: number,
) {
  const url = `${vanillaAuthState.getState().base_url}course/last-moment/${id}`;
  //console.log("+++++++picking up", url, lat, lng);
  return http.post({ url, data: { lat, lng } }).then((response) => {
    if (response && response.status === 200) {
      //showToast("success", "Départ vers partenaire");
      vanillaToastStore
        .getState()
        .showToast(
          "valide",
          "Bonne route vers le restaurant pour récupérer la commande !",
          "Départ enregistré",
        );
      //console.log(response);
      return Promise.resolve(true);
    }
    // showToast("error", "Une erreur est survenue");
    vanillaToastStore
      .getState()
      .showToast(
        "error",
        "Veuillez vérifier votre connexion internet",
        "Une erreur est survenue",
      );
    return Promise.reject(false);
  });
}

export const reloadOrders = async (user_id?: number) => {
  let id_user: number;
  if (user_id == undefined) {
    id_user = authToUserId(vanillaAuthState.getState().auth);
  } else {
    id_user = user_id;
  }

  try {
    await Promise.allSettled([
      await get_new_orders_for_partner(id_user, 1, INTERVAL_PAGE),
      await get_accepted_orders_for_partner(id_user, 1, INTERVAL_PAGE),
      await get_delivered_orders_for_partner(id_user, 1, INTERVAL_PAGE),
      await get_refused_orders_for_partner(id_user, 1, INTERVAL_PAGE),
    ]);
  } catch (e) {
    //console.error(e)
  }
  return Promise.resolve();
};

export async function load_marketPlace_bill(id: number): Promise<IPartnerBill> {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/marketplace-bill`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const data: IPartnerBill = response.data;
      return Promise.resolve(data);
    }
    return Promise.reject(null);
  });
}

export async function load_customer_bill(
  id: number,
): Promise<ICustomerBillData> {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/customer-bill`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const data: ICustomerBillData = response.data;
      //"data", data);
      return Promise.resolve(data);
    }
    return Promise.reject(null);
  });
}

export async function load_partner_customer(
  id: number,
): Promise<IPartnerCustomer> {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id}/customers`;
  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const data: IPartnerCustomer = response.data;
      return Promise.resolve(data);
    }
    return Promise.reject(null);
  });
}

export async function set_notification_as_read(id: string): Promise<boolean> {
  //const url = `${vanillaAuthState.getState().base_url}partenaire/${id}/customers`;
  const url = `${vanillaAuthState.getState().notif_url}notification/seen/`;
  return http.post({ url, data: { notification_id: id } }).then((response) => {
    if (response && response.status === 200) {
      const data: boolean = response.data.ok;
      console.log(response.data);
      return Promise.resolve(data);
    }
    return Promise.reject(false);
  });
}

// ------- HELP LOADERS ------//

// DelliveryMen
export async function get_deliverymen_help_information(id_livreur: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }deliverymen/${id_livreur}/help`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const contact_informations: IContact = response.data;
      return Promise.resolve(contact_informations);
    }

    return Promise.reject();
  });
}

// Partenaire
export async function get_partenaire_help_information(id_partenaire: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/help`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const contact_informations: IContact = response.data;
      return Promise.resolve(contact_informations);
    }

    return Promise.reject();
  });
}

// Partenaire informations
export async function get_partenaire_profile_information(
  id_partenaire: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/informations`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const profile_informations: ProfileInformations = response.data;
      return Promise.resolve(profile_informations);
    }

    return Promise.reject();
  });
}

export async function update_partenaire_profile_information(
  id_partenaire: number,
  profileInfo: ProfileInformations,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/information-update`;

  console.log("profile data body: ", profileInfo);

  return http.post({ url, data: { ...profileInfo } }).then((response) => {
    if (response && response.status === 200) {
      // vanillaToastStore
      //   .getState()
      //   .showToast(
      //     "valide",
      //     "Ton Profil à été bien mis à jour",
      //     "Mise à jour du profil",
      //   );
      return Promise.resolve(response.data);
    }

    return Promise.reject();
  });
}

export async function get_profile_admin_and_justification(
  id_partenaire: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/admin-and-justif`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const ProfileAdminJusti: AdminJusti = response.data;
      return Promise.resolve(ProfileAdminJusti);
    }

    return Promise.reject();
  });
}

export async function update_profile_admin_and_justification(
  banking_data: BankingInformations,
  id_partenaire: number,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/banking-information-update`;

  console.log("admin data body: ", { ...banking_data });
  return http.post({ url, data: { ...banking_data } }).then((response) => {
    if (response && response.status === 200) {
      // vanillaToastStore
      //   .getState()
      //   .showToast(
      //     "valide",
      //     "Ton Profil à été bien mis à jour",
      //     "Mise à jour du profil",
      //   );
      return Promise.resolve(response.data);
    }

    return Promise.reject();
  });
}

// Univer
export async function get_profile_univers(id_partenaire: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/univers`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const univers: UniversType[] = response.data;
      return Promise.resolve(univers);
    }

    return Promise.reject();
  });
}

export async function update_univers(id_partenaire: number, ids: FormData) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/update-univers`;

  return http.post_form({ url, form: ids }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(response.data);
    }

    return Promise.reject();
  });
}

// Parameters

export async function get_parameters_details(id_partenaire: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/parameters`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const params: ParametersDetails[] = response.data;
      return Promise.resolve(params);
    }

    return Promise.reject();
  });
}

export async function add_new_collaborator(
  id_partenaire: number,
  newCollaborator: NewCollaborator,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/add-collab`;

  console.log(newCollaborator);

  return http.post({ url, data: newCollaborator }).then((response) => {
    if (response && response.status === 200) {
      // vanillaToastStore
      //   .getState()
      //   .showToast("valide", "Collaborateur bien ajouté", "Collaboration");
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

export async function get_week_planning(id_partenaire: number) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/planning/resume`;

  return http.get(url).then((response) => {
    if (response && response.status === 200) {
      const planning: WeekSchedule[] = response.data;
      return Promise.resolve(planning);
    }

    return Promise.reject();
  });
}

export async function update_planning(
  id_partenaire: number,
  planning: FormData,
) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/planning/update`;

  return http.post_form({ url, form: planning }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

// export async function get_quotas(id_partenaire: number) {}

export async function update_quotas(id_partenaire: number, quota: FormData) {
  const url = `${
    vanillaAuthState.getState().base_url
  }partenaire/${id_partenaire}/profil/order-quota`;

  return http.post_form({ url, form: quota }).then((response) => {
    if (response && response.status === 200) {
      return Promise.resolve(true);
    }

    return Promise.reject();
  });
}

// ----------------- SAVE USER DATA ---------------- //

// DeliveryMen
export async function save_device_info() {
  let infos = {} as any;
  let token = "";
  try {
    const retrieved_info = get_device_info();
    if (!retrieved_info?.expo_token) {
      try {
        const token_raw = await getExpoPushTokenAsync({
          //applicationId: "@helodev/sacrearmand_pro_app",
          projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
        });
        token = token_raw.data;
        infos = {
          ...infos,
          ...retrieved_info,
          expo_token: token,
        };
      } catch (error) {
        console.error("cant retrieve token");
        infos = {
          ...infos,
          ...retrieved_info,
        };
        //Sentry.captureException(error);
      }
    } else {
      infos = {
        ...infos,
        ...retrieved_info,
      };
    }
  } catch (error) {
    console.error(error);
    //Sentry.captureException(error);
    return Promise.reject(error);
  }
  if (!infos.base_user) {
    return Promise.reject();
  }
  const url_suffix = infos.base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)
    ? "deliverymen"
    : "partenaire";
  const url = `${vanillaAuthState.getState().base_url}${url_suffix}/${
    infos.base_user.id
  }/send-user-token-data`;

  if (Object.keys(infos).includes("id")) {
    delete infos.base_user;
  }

  return http
    .post({
      url,
      data: {
        ...infos,
      },
    })
    .then((response) => {
      if (response && response.status === 200) {
        return Promise.resolve(response.data);
      }

      return Promise.reject(response);
    })
    .catch((e) => {
      return Promise.reject(e);
    });
}
