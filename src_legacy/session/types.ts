import { ICourse } from "src/interfaces/Entities";
import * as React from "react";
import { Platform } from "react-native";
import {
  AUTH_ACTION_TYPES,
  COURSE_ACTION_TYPES,
  PERMISSION_ACTION_TYPES,
  UserActionTypes,
} from "./actions";

export enum PERMISSION_LEVEL {
  // To be determined
  UNKNOWN = -1,
  // explicit denial from user
  DENIED = 0,
  // user clicked on accept button on app level
  ACCEPTED = 1,
  // user has granted the app the required permission
  GRANTED = 2,
}
export interface IUser {
  name: string;
  first_name: string;
  last_name: string;
  civility: string;
  email: string;
  dob: string;
  siret: string;
  telephone: string;
  address: string;
  company_name: string;
  social_form: string;
}

export interface IAuth {
  access_token: string;
  refresh_token: string;
  is_loading?: boolean;
}

export interface IPermission {
  notifications: PERMISSION_LEVEL;
  localisation: PERMISSION_LEVEL;
}

export interface IAuthActions {
  type: AUTH_ACTION_TYPES;
  payload: {
    access_token?: string;
    refresh_token?: string;
    is_loading?: boolean;
  };
}

export interface IUserActions {
  type: UserActionTypes;
  payload: IUser;
}

export interface ICourseActions {
  type: COURSE_ACTION_TYPES;
  payload: ICourse;
}

export interface IPermissionActions {
  type: PERMISSION_ACTION_TYPES;
  payload: IPermission;
}

export type AuthDispatch = (action: IAuthActions) => void;
export type PermissionDispatch = (action: IPermissionActions) => void;
export type UserDispatch = (action: IUserActions) => void;
export type CourseDispatch = (action: ICourseActions) => void;

export type AuthProviderProps = { children: React.ReactNode };
export type UserProviderProps = { children: React.ReactNode };
export type PermissionProviderProps = { children: React.ReactNode };
export type CourseProviderProps = {
  defaultCourse: ICourse;
  children: React.ReactNode;
};

export function defaultUser() {
  return {} as IUser;
}

export function defaultPermission() {
  return {
    notifications:
      Platform.OS === "android"
        ? PERMISSION_LEVEL.GRANTED
        : PERMISSION_LEVEL.UNKNOWN,
    localisation: PERMISSION_LEVEL.UNKNOWN,
  } as IPermission;
}

export interface IMessage {
  content: string;
  sent_at: string;
  sent_by_user: boolean;
  read: boolean;
  timestamp: string;
  sender: string;
  sent_by_client: boolean;
}
