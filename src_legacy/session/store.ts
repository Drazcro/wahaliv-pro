import AsyncStorage from "@react-native-async-storage/async-storage";
import { IAuth, IPermission, IUser } from "./types";

export function saveSession(sessionData: IAuth) {
  const data = JSON.stringify(sessionData);
  AsyncStorage.setItem("auth", data);
  AsyncStorage.getItem("auth").then((v) => {
    console.log("--session", JSON.parse(v));
  });
}

export function saveUser(user: IUser) {
  const userData = JSON.stringify(user);
  return AsyncStorage.setItem("user", userData);
}

export function savePermissions(permissions: IPermission) {
  const permissionData = JSON.stringify(permissions);
  return AsyncStorage.setItem("permissions", permissionData);
}

export async function getAuth(): Promise<IAuth> {
  const value = await AsyncStorage.getItem("auth");
  // console.log(value, "loaded_session");
  return value ? JSON.parse(value) : {};
}

export async function getPermissions(): Promise<IPermission> {
  const value = await AsyncStorage.getItem("permissions");
  return value ? JSON.parse(value) : {};
}

export async function getUser() {
  const value = await AsyncStorage.getItem("user");
  return value ? JSON.parse(value) : {};
}

export function clearSession() {
  return AsyncStorage.multiRemove(["auth"]);
}

export function clearUser() {
  return AsyncStorage.removeItem("user");
}

export function clearAuth() {
  return AsyncStorage.multiRemove(["auth", "permissions"]);
}
