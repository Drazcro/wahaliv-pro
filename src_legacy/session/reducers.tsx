import { ICourse } from "src/interfaces/Entities";
import {
  AUTH_ACTION_TYPES,
  COURSE_ACTION_TYPES,
  PERMISSION_ACTION_TYPES,
  UserActionTypes,
} from "./actions";
import {
  saveSession,
  clearUser,
  saveUser,
  clearSession,
  savePermissions,
} from "./store";
import {
  IAuth,
  IAuthActions,
  ICourseActions,
  IPermission,
  IPermissionActions,
  IUser,
  IUserActions,
} from "./types";

// Reducers
export function authReducer(state: IAuth, action: IAuthActions) {
  switch (action.type) {
    case AUTH_ACTION_TYPES.SET_ACCESS_TOKEN: {
      return {
        ...state,
        access_token: action.payload.access_token,
      } as IAuth;
    }
    case AUTH_ACTION_TYPES.SET_REFRESH_TOKEN: {
      return {
        ...state,
        refresh_token: action.payload.refresh_token,
      } as IAuth;
    }
    case AUTH_ACTION_TYPES.SET_IS_LOADING: {
      return {
        ...state,
        is_loading: action.payload.is_loading,
      } as IAuth;
    }
    case AUTH_ACTION_TYPES.UPDATE: {
      const newState = {
        access_token: action.payload.access_token,
        refresh_token: action.payload.refresh_token,
        is_loading: action.payload.is_loading,
      } as IAuth;
      // console.log("new state", newState);
      saveSession(newState).then((value: any) => console.log(value));
      return newState;
    }
    case AUTH_ACTION_TYPES.CLEAR: {
      clearSession().then((value) => console.log(value));
      const newState = {
        access_token: undefined,
        refresh_token: undefined,
        is_loading: false,
      };
      // console.log("Clearing authdata data");

      return newState;
    }
    default:
      return state;
  }
}

export function userReducer(state: IUser, action: IUserActions) {
  switch (action.type) {
    case UserActionTypes.UPDATE: {
      const newState = {
        ...state,
        ...action.payload,
      } as IUser;
      saveUser(newState);
      return newState;
    }
    case UserActionTypes.CLEAR: {
      // console.log("Clearing user data");
      clearUser().then((value) => console.log(value));
      return {} as IUser;
    }
    default:
      return state;
  }
}

export function courseReducer(state: ICourse, action: ICourseActions) {
  switch (action.type) {
    case COURSE_ACTION_TYPES.UPDATE: {
      const newState = {
        ...state,
        ...action.payload,
      } as ICourse;
      return newState;
    }
    default:
      return {} as ICourse;
  }
}

export function permissionReducer(
  state: IPermission,
  action: IPermissionActions,
) {
  switch (action.type) {
    case PERMISSION_ACTION_TYPES.SET_LOCALISATION: {
      const newState = {
        ...state,
        ...action.payload,
      } as IPermission;
      savePermissions(newState);
      return newState;
    }
    case PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS: {
      const newState = {
        ...state,
        ...action.payload,
      } as IPermission;
      savePermissions(newState);
      return newState;
    }
    case PERMISSION_ACTION_TYPES.CLEAR: {
      savePermissions({} as IPermission);
      return {} as IPermission;
    }
    case PERMISSION_ACTION_TYPES.UPDATE: {
      const newState = {
        ...state,
        ...action.payload,
      } as IPermission;
      savePermissions(newState);
      return newState;
    }
    default:
      return state;
  }
}
