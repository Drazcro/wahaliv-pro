export enum AUTH_ACTION_TYPES {
  SET_REFRESH_TOKEN = "SET_REFRESH_TOKEN",
  SET_ACCESS_TOKEN = "SET_ACCESS_TOKEN",
  SET_IS_LOADING = "SET_IS_LOADING",
  UPDATE = "UPDATE",
  CLEAR = "CLEAR",
}

export enum UserActionTypes {
  UPDATE = "UPDATE",
  CLEAR = "CLEAR",
}

export enum PERMISSION_ACTION_TYPES {
  SET_NOTIFICATIONS = "SET_NOTIFICATIONS",
  SET_LOCALISATION = "SET_LOCALISATION",
  UPDATE = "UPDATE",
  CLEAR = "CLEAR",
}

export enum COURSE_ACTION_TYPES {
  UPDATE = "UPDATE",
  CLEAR = "CLEAR",
}
