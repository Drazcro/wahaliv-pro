import { ICourse } from "src/interfaces/Entities";
import * as React from "react";
import { courseReducer, permissionReducer, userReducer } from "./reducers";
import {
  CourseDispatch,
  CourseProviderProps,
  defaultPermission,
  defaultUser,
  IPermission,
  IUser,
  PermissionDispatch,
  PermissionProviderProps,
  UserDispatch,
  UserProviderProps,
} from "./types";

const UserStateContext = React.createContext<
  | {
      userState: IUser;
      userDispatch: UserDispatch;
    }
  | undefined
>(undefined);

export const PermissionContext = React.createContext<
  | { permissions: IPermission; permissionDispatch: PermissionDispatch }
  | undefined
>(undefined);

const CourseContext = React.createContext<
  | {
      courseEnCours: ICourse;
      courseDispatch: CourseDispatch;
    }
  | undefined
>(undefined);

// Providers
function UserProvider({ children }: UserProviderProps) {
  const [userState, userDispatch] = React.useReducer(
    userReducer,
    defaultUser(),
  );
  const value = { userState, userDispatch };
  return (
    <UserStateContext.Provider value={value}>
      {children}
    </UserStateContext.Provider>
  );
}

function PermissionProvider({ children }: PermissionProviderProps) {
  const [permissions, permissionDispatch] = React.useReducer(
    permissionReducer,
    defaultPermission(),
  );
  const value = { permissions, permissionDispatch };
  return (
    <PermissionContext.Provider value={value}>
      {children}
    </PermissionContext.Provider>
  );
}

function CourseProvider({ children, defaultCourse }: CourseProviderProps) {
  const [courseEnCours, courseDispatch] = React.useReducer(
    courseReducer,
    defaultCourse,
  );
  const value = { courseEnCours, courseDispatch };
  return (
    <CourseContext.Provider value={value}>{children}</CourseContext.Provider>
  );
}

// Injectors

function useUser() {
  const context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error(
      "useDeliveryUser must be used within a DeliveryUserProvider.",
    );
  }
  return context;
}

function usePermissions() {
  const context = React.useContext(PermissionContext);
  if (context === undefined) {
    throw new Error("usePermission must be used within a PermissionProvider.");
  }
  return context;
}

function useCourse() {
  const context = React.useContext(CourseContext);
  if (context === undefined) {
    throw new Error("useCourse must be used within a CourseBody.");
  }
  return context;
}

export { UserProvider, useUser };
export { PermissionProvider, usePermissions };
export { CourseProvider, useCourse };
