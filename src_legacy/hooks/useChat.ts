import * as React from "react";
import { DatabaseReference, off, onValue, ref } from "firebase/database";
import { ROLE_TYPE, getBaseUser } from "src_legacy/services/Utils";
import { db } from "firebase";
import { encode } from "base-64";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { authStore } from "src_legacy/services/v2/auth_store";

interface IMessage {
  content: string;
  sent_at: string;
  sent_by_user: boolean;
  read: boolean;
  timestamp: string;
  sender: string;
  ref: DatabaseReference;
}

export default function useChat() {
  const { auth } = authStore();
  const [messages, setMessages] = React.useState<IMessage[]>([]);
  const steps = tourneeStore((v) => v.steps);

  const [unreadCount, setUnreadCount] = React.useState(0);
  const [unreadClientCount, setUnreadClientCount] = React.useState<number>(0);

  const [lastMessage, setLastMessage] = React.useState<IMessage | undefined>();

  const [uniqueToken, setUniqueToken] = React.useState("");

  const user = React.useMemo(() => {
    if (auth?.access_token) {
      return getBaseUser(auth);
    } else {
      return undefined;
    }
  }, [auth]);

  const getChatPath = React.useMemo(() => {
    const formattedString = `${user?.id}|-|;${user?.email}|-|;${
      user?.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0]
    }`;

    return encode(formattedString);
  }, [user]);

  React.useEffect(() => {
    const dbRef = ref(
      db,
      "/chat/" + getChatPath + "/chat_metadata/unread_for/user",
    );

    onValue(dbRef, (snapshot) => {
      const count = snapshot.val();
      setUnreadCount(count ?? 0);
    });

    return () => off(dbRef);
  }, [getChatPath]);

  React.useEffect(() => {
    const lastMessageRef = ref(
      db,
      "/chat/" + getChatPath + "/chat_metadata/lastMessage",
    );

    onValue(lastMessageRef, (snapshot) => {
      const lastMessage = snapshot.val();

      setLastMessage(lastMessage);
    });

    return () => off(lastMessageRef);
  }, [getChatPath]);

  React.useEffect(() => {
    if (steps.length === 0) {
      return;
    }

    const callbackList: (() => void)[] = [];

    for (const step of steps) {
      const db_ref = ref(
        db,
        `chat_all_My8xMS8yMDIz/${user?.id}/${step.order_id}/unread_for/livreur`,
      );
      const unsubscribe = onValue(db_ref, (snapshot) => {
        if (!snapshot.exists()) {
          return;
        }
        setUnreadClientCount((prev) => prev + snapshot.val());
      });
      callbackList.push(unsubscribe);
    }

    return () => {
      for (const callback of callbackList) {
        callback();
      }
    };
  }, [steps]);

  return {
    messages,
    unreadCount,
    unreadClientCount,
    setUnreadClientCount: (v: number) => setUnreadClientCount(v),
    setUnreadCount: (v: number) => setUnreadCount(v),
    lastMessage,
    uniqueToken,
    setUniqueToken: (v: string) => setUniqueToken(v),
  };
}
