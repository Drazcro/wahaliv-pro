import * as Location from "expo-location";
import PermissionResponseStatus from "expo-location";
import { Linking, Platform } from "react-native";
import { PERMISSION_ACTION_TYPES } from "src_legacy/session/actions";
import { IPermission, PERMISSION_LEVEL } from "src_legacy/session/types";
import { usePermissions } from "src_legacy/session/context";
import Constants from "expo-constants";
//import * as Sentry from "@sentry/react-native";

const pkg =
  Constants.expoConfig?.extra?.environment === "dev"
    ? "package:host.exp.exponent"
    : "package:com.sacrearmand.expopro";
const { DENIED, GRANTED, UNKNOWN } = PERMISSION_LEVEL;

export default function usePermissionLevel() {
  const { permissions, permissionDispatch } = usePermissions();

  async function enableLocationService() {
    await Linking.openSettings();
  }

  async function onGrantCallback() {
    console.log(
      "Grant called",
      permissions.localisation,
      permissions.notifications,
    );
    console.log(DENIED, UNKNOWN, GRANTED);
    if (permissions.localisation == DENIED) {
      console.log("PERMISSION DENIED");

      Linking.openSettings().catch((e) => console.error(e));
      return;
    }
    if (permissions.localisation < PERMISSION_LEVEL.ACCEPTED) {
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
        payload: { localisation: PERMISSION_LEVEL.ACCEPTED } as IPermission,
      });
    }
    console.log("GRANTING READY TO ASK");
    let status: PermissionResponseStatus.PermissionStatus;
    let canAskAgain: boolean;
    try {
      const response = await Location.requestForegroundPermissionsAsync();
      console.log("response ", response);
      status = response.status;
      canAskAgain = response.canAskAgain;
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error);
      return;
    }
    console.log(status, canAskAgain, "Can ask again ");
    if (status !== "granted") {
      console.log("Permission to access location was denied");
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
        payload: { localisation: PERMISSION_LEVEL.DENIED } as IPermission,
      });
      onGrantCallback();
    } else {
      console.log("Foreground Permission to access location was granted");
      const { status: back_status, canAskAgain: back_can_ask_again } =
        await Location.requestBackgroundPermissionsAsync();

      if (back_status !== "granted" && !back_can_ask_again) {
        console.log("Background Permission to access location was denied");
        permissionDispatch({
          type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
          payload: { localisation: PERMISSION_LEVEL.DENIED } as IPermission,
        });
        onGrantCallback();
      } else {
        console.log("Background Permission to access location was granted");
        permissionDispatch({
          type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
          payload: { localisation: PERMISSION_LEVEL.GRANTED } as IPermission,
        });
      }
    }
  }

  return {
    permissions,
    permissionDispatch,
    onGrantCallback,
    enableLocationService,
  };
}
