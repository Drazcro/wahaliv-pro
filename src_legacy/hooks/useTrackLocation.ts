import { BackgroundFetchStatus } from "expo-background-fetch";
import React from "react";
import { Platform } from "react-native";
import {
  getTrackingTaskStatus,
  isTrackingTaskRegistered,
  registerBackgroundFetchAsync,
  unregisterBackgroundFetchAsync,
} from "src/cron/foreground_localisation_task";
import { ROLE_TYPE, getBaseUser } from "src_legacy/services/Utils";
import { authStore } from "src_legacy/services/v2/auth_store";

export function useTrackLocation() {
  const [isRegistered, setIsRegistered] = React.useState(false);
  const [status, setStatus] = React.useState<null | BackgroundFetchStatus>(
    null,
  );
  const { auth } = authStore();

  const checkStatusAsync = async () => {
    const status = await getTrackingTaskStatus();
    const registered = await isTrackingTaskRegistered();
    setStatus(status);
    setIsRegistered(registered);
  };

  const toggleFetchTask = async () => {
    if (Platform.OS !== "web") {
      if (isRegistered) {
        console.log("is registered");

        await unregisterBackgroundFetchAsync();
      } else {
        console.log("is not registered");

        if (getBaseUser(auth).roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
          await registerBackgroundFetchAsync();
        }
      }
      checkStatusAsync();
    }
  };

  return {
    checkStatusAsync,
    toggleFetchTask,
    isRegistered,
  };
}
