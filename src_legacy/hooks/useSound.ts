import { useCallback, useEffect, useMemo, useRef, useState } from "react";
import { Audio } from "expo-av";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const audioResource = require("../../assets/notification.mp3");

function useAudio(settings = {}) {
  const audioObject = useRef(null);
  const currentAudioResource = useRef(null);

  const [isLoadingAudio, setIsLoadingAudio] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const isMounted = useRef(false);
  const [bugWatchdogCounter, setBugWatchdogCounter] = useState(0);

  // current status of the component
  useEffect(() => {
    isMounted.current = true;
    return () => {
      isMounted.current = false;
    };
  }, []);

  // the expo-av bug watchdog
  // expo-av has a few bugs that cause the audio to not play or pause when requested.
  // this is a workaround that checks the audio status every n ms and force a play/pause action.
  useEffect(() => {
    const timerHandler = setInterval(() => {
      setBugWatchdogCounter((value) => value + 1);
    }, 500);

    return () => clearInterval(timerHandler);
  }, []);

  // merge user settings with default settings
  const componentSettings = useMemo(() => {
    const defaultSettings = {
      autoPlay: false,
      staysActiveInBackground: true,
      shouldDuckAndroid: false,
      playThroughEarpieceAndroid: false,
      allowsRecordingIOS: false,
      playsInSilentModeIOS: true,
    };

    return {
      ...defaultSettings,
      ...settings,
    };
  }, [settings]);

  // check if the user has defined the audio resource
  const isResourceDefined = useMemo(() => {
    // if the audio resource is null or undefined, return false
    if (!audioResource) {
      return false;
    }

    // if the audio resource is not null nor a streaming, it is a local file
    return true;
  }, [audioResource]);

  // unload and free audio resources
  const unload = useCallback(async () => {
    // safe guard to not unload the audio object if not initialized
    if (!audioObject.current) {
      return;
    }

    // only call `stop` if the audio is loaded
    const status = await audioObject.current.getStatusAsync();
    if (status.isLoaded) {
      await audioObject.current.stopAsync();
    }

    // unload the audio object
    await audioObject.current.unloadAsync();

    // check if component is still mounted before changing a state
    if (!isMounted.current) {
      return;
    }

    // clear the audio object
    audioObject.current = null;
    currentAudioResource.current = null;
  }, []);

  // unload audio when unmount the component
  useEffect(() => unload, [unload]);

  // play/pause the audio according to isPlaying status
  useEffect(() => {
    const playOrPause = async () => {
      // safe guard to not call audio object if not initialized
      const isAudioObjectDefined = !!audioObject.current;
      if (!isAudioObjectDefined) {
        return;
      }

      // safe guard to not call if the audio is not loaded
      // due to async, sometimes `isLoadingAudio` is false but the audio is not loaded yet
      const status = await audioObject.current.getStatusAsync();
      if (isLoadingAudio || !status.isLoaded) {
        return;
      }

      // play/pause the audio
      try {
        if (isPlaying) {
          await audioObject.current.playAsync();
        } else {
          await audioObject.current.pauseAsync();
        }
      } catch {
        // bug on expo-av: status not always return correct value for `isLoaded`
        // it's okay to ignore the exception as we would skip the play/pause action anyway
      }
    };
    playOrPause();
  }, [isLoadingAudio, isPlaying, bugWatchdogCounter]);

  // pause the audio
  const pause = useCallback(() => {
    setIsPlaying(false);
  }, []);

  // play the audio
  const play = useCallback(() => {
    setIsPlaying(true);
  }, []);

  return {
    play,
    pause,
    unload,
    isPlaying,
    setIsPlaying,
    isLoadingAudio,
  };
}

export default useAudio;
