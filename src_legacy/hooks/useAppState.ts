import { useEffect, useRef, useState } from "react";
import { AppState, AppStateEvent } from "react-native";

import { ROLE_TYPE, getBaseUser } from "src_legacy/services/Utils";
import React from "react";
import moment from "moment";
import { ref, update } from "firebase/database";
import { db } from "firebase";
import { save_device_info } from "src_legacy/services/Loaders";
import { profileStore } from "src_legacy/zustore/profilestore";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import { authStore } from "src_legacy/services/v2/auth_store";

export const useAppState = () => {
  const appState = useRef(AppState.currentState);
  const [appStateVisible, setAppStateVisible] = useState(appState.current);
  const { auth } = authStore();
  const profile = profileStore((v) => v.profile);
  const partnerProfile = partnerStore((v) => v.partnerProfile);

  const base_user = React.useMemo(() => {
    if (auth?.access_token) {
      return getBaseUser(auth);
    } else {
      return undefined;
    }
  }, [auth]);

  const getFullName = React.useMemo(() => {
    console.log(base_user, "erererere");
    if (!base_user) {
      return "";
    }
    if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
      const account = profile;
      return `${account.firstName ?? ""} ${account.lastName ?? ""}`;
    } else {
      const account = partnerProfile;
      return `${account?.partner?.name ?? ""}`;
    }
  }, [partnerProfile, profile, base_user]);

  useEffect(() => {
    const subscription = AppState.addEventListener("change", (nextAppState) => {
      if (
        appState.current.match(/inactive|background/) &&
        nextAppState === "active"
      ) {
        console.log("App has come to the foreground!");
      } else {
        console.log("App has went to the background!");
      }

      if (
        !base_user ||
        base_user?.id == undefined ||
        !base_user?.id ||
        !base_user.roles ||
        base_user.roles.length < 1
      ) {
        console.log("no user");
      }
      if (base_user?.id == undefined) {
        console.log("no user");
      } else {
        const role = base_user?.roles.filter(
          (item) => item !== "ROLE_PARTICULIER",
        )[0];
        console.log("role ----", role);
        const dbRef = ref(db, `/status/${role}/` + base_user.id);
        const updates = {
          email: base_user.email,
          last_seen: moment().format(),
          online: false,
          full_name: `${getFullName}`,
        };
        if (nextAppState === "active") {
          updates.online = true;
        }
        console.log(AppState, updates);

        update(dbRef, updates);
        appState.current = nextAppState;
        setAppStateVisible(nextAppState);
      }
    });

    return () => {
      subscription.remove();
    };
  }, [base_user, getFullName]);

  // useEffect(() => {
  //   const subscription = AppState.addEventListener("blur", (nextAppState) => {
  //     console.log("AppState----------", nextAppState);
  //     // if (
  //     //   appState.current.match(/inactive|background/) &&
  //     //   nextAppState === "active"
  //     // ) {
  //     //   console.log("App has come to the foreground!");
  //     // }
  //     // else {
  //     //   console.log("App has went to the background!");
  //     // }

  //     if (!base_user?.id){
  //       console.log('no user')
  //     }
  //     const dbRef = ref(db, "/status/" + base_user?.id);
  //     const updates = {
  //       last_seen: moment().format(),
  //       online: false
  //     }

  //     console.log(AppState.currentState, updates)

  //     update(dbRef, updates);
  //     appState.current = nextAppState;
  //     setAppStateVisible(nextAppState);

  //   });

  //   return () => {
  //     subscription.remove();
  //   };
  // }, [base_user]);

  return appStateVisible;
};
