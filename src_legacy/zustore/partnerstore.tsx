/* eslint-disable */

import { create } from "zustand";

import { IPartnerData, IPartnerProfile } from "src/interfaces/Entities";

interface IPartnerStore<T> {
  partnerProfile: IPartnerProfile;
  orderEnCours: IPartnerData;
  newOrders: IPartnerData[];
  acceptedOrders: IPartnerData[];
  deliveredOrders: IPartnerData[];
  cancelledOrders: IPartnerData[];
  orderInModal: IPartnerData;

  setPartnerProfile: (payload: IPartnerProfile) => void;
  setNewOrders: (payload: IPartnerData[]) => void;
  addOrderToNewOrders: (payload: IPartnerData) => void;
  resetNewOrders: () => void;
  setOrderEnCours: (payload: IPartnerData) => void;
  removeOrderEnCours: () => void;
  getNewOrderById: (id: number) => IPartnerData | null;
  updateCollecteHourByIndex: (index: number, newHour: string) => void;
  updateCollecteHourById: (id: number, newHour: string) => void;
  setOrderInModal: (payload: IPartnerData) => void;
}

const vanillaPartnerStore = create<IPartnerStore<{}>>((set, get) => ({
  partnerProfile: {
    partner: { name: "" },
  } as IPartnerProfile,
  orderEnCours: {} as IPartnerData,
  newOrders: [] as IPartnerData[],
  acceptedOrders: [] as IPartnerData[],
  cancelledOrders: [] as IPartnerData[],
  deliveredOrders: [] as IPartnerData[],
  orderInModal: {} as IPartnerData,

  setOrderInModal: (order) => set((state) => ({ orderInModal: order })),
  setPartnerProfile: (infos) => set((state) => ({ partnerProfile: infos })),
  setOrderEnCours: (order) => set((state) => ({ orderEnCours: order })),
  setNewOrders: (orders) => set((state) => ({ newOrders: orders })),
  resetNewOrders: () => set((state) => ({ newOrders: [] as IPartnerData[] })),
  addOrderToNewOrders: (order) =>
    set((state) => ({ newOrders: [...state.newOrders, order] })),
  removeOrderEnCours: () => set({ orderEnCours: {} as IPartnerData }),
  getNewOrderById: (id) => {
    const index = get().newOrders.findIndex((item) => item.commande.id === id);
    return index !== -1 ? get().newOrders[index] : null;
  },
  updateCollecteHourByIndex: (index, newHour) =>
    set((state) => {
      const orders = state.newOrders;
      if (index != undefined && orders[index] != undefined) {
        orders[index].commande.pickup_date.date = newHour;
      }

      return {
        newOrders: orders,
      };
    }),
  updateCollecteHourById: (id, newHour) =>
    set((state) => {
      const index = state.newOrders.findIndex(
        (item) => item.commande.id === id,
      );

      const orders = state.newOrders;
      if (index != undefined && orders[index] != undefined) {
        orders[index].commande.pickup_date.date = newHour;
      }

      return {
        newOrders: orders,
      };
    }),
}));

const partnerStore = vanillaPartnerStore as unknown as {
  <T>(): IPartnerStore<T>;
  <T, U>(selector: (s: IPartnerStore<T>) => U): U;
};

export { vanillaPartnerStore, partnerStore };
