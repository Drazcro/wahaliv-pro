/* eslint-disable */

import { create } from "zustand";
// import { createStore } from "zustand/vanilla";

import { ICourse, IInvoiceDataCourse } from "src/interfaces/Entities";
import { COURSE_STATUS } from "src/constants/course";

interface ICourseStore<T> {
  updateNewCourseSeen: (payload: ICourse) => void;
  newCoursesSeen: ICourse[];
  courseEnCours: ICourse;
  playSound: boolean;
  setCourseEnCours: (payload: ICourse) => void;
  setDeliveryStatus: (payload: COURSE_STATUS) => void;
  setPlaySound: (payload: boolean) => void;
  removeCourseEnCours: () => void;
  courses: IInvoiceDataCourse[];
  setCourses: (value: IInvoiceDataCourse[]) => void;
}

const vanillaCourseStore = create<ICourseStore<{}>>((set) => ({
  newCoursesSeen: [],
  updateNewCourseSeen: (course) =>
    set(({ newCoursesSeen }) => ({
      newCoursesSeen: [
        ...newCoursesSeen.filter(
          (elt) => elt.id_commande != course.id_commande,
        ),
        course,
      ],
    })),
  courseEnCours: {} as ICourse,
  playSound: false,
  setCourseEnCours: (course) => set((payload) => ({ courseEnCours: course })),
  setDeliveryStatus: (new_status) =>
    set((payload) => ({
      courseEnCours: { ...payload.courseEnCours, delivery_status: new_status },
    })),
  setPlaySound: (play) => set(() => ({ playSound: play })),
  removeCourseEnCours: () => set({ courseEnCours: {} as ICourse }),
  courses: [],
  setCourses: (value: IInvoiceDataCourse[]) => set(() => ({ courses: value })),
}));

const courseStore = vanillaCourseStore as {
  <T>(): ICourseStore<T>;
  <T, U>(selector: (s: ICourseStore<T>) => U): U;
};

export { vanillaCourseStore, courseStore };
