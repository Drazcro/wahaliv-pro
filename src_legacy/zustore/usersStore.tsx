/* eslint-disable */

import { create } from "zustand";

export interface IMessage {
  id: string;
  sender: string;
  receiver: string;
  content: string;
  read_status: string;
  sent_at: string;
  read_at: string;
}

export interface IUser {
  email: string;
  messages: IMessage[];
}

interface UserState {
  users: IUser[];
  addUser: (user: IUser) => void;
  addUsers: (users: string[]) => void;
  addMessage: (email: string, message: IMessage) => void;
  getUsers: (myEmail: string) => string[];
  getMessagesForUser: (email: string) => IMessage[];
  openedChat: string;
  setOpenedChat: (email: string) => void;
  addMessages: (email: string, messages: IMessage[]) => void;
  currentChat: IMessage[];
}

const useUserStore = create<UserState>((set, get) => ({
  users: [],
  currentChat: [],
  openedChat: "",
  setOpenedChat: (email) =>
    set((state) => ({
      openedChat: email,
      currentChat:
        state.users.find((item) => item.email === email)?.messages ?? [],
    })),
  addUser: (user) => {
    set((state) => {
      const existing_user = state.users.find(
        (item) => item.email === user.email,
      );
      return {
        ...state,
        users: existing_user ? state.users : [...state.users, user],
      };
    });
  },

  addUsers: (emails) => {
    set((state) => {
      const new_users: IUser[] = [];
      for (const email of emails) {
        const existing_user = state.users.find((item) => item.email === email);

        if (existing_user === undefined) {
          new_users.push({ email, messages: [] });
        }
      }
      // console.log(new_users);

      return {
        ...state,
        users: [...state.users, ...new_users],
      };
    });
  },

  addMessage: (email, message) => {
    set((state) => {
      const existing_index = state.users.findIndex(
        (item) => item.email === email,
      );
      if (existing_index === undefined) {
        const new_user = {
          email,
          messages: [message],
        };
        return {
          ...state,
          users: [...state.users, new_user],
          currentChat:
            get().openedChat === email ? new_user.messages : state.currentChat,
        };
      } else {
        const existing_user = state.users[existing_index];
        const existing_message = existing_user?.messages.find(
          (item) => item.id === message.id,
        );
        if (existing_message === undefined) {
          existing_user?.messages.push(message);

          const new_users = state.users.map((item, index) => {
            if (index === existing_index) {
              return existing_user;
            }
            return item;
          });

          return {
            ...state,
            users: new_users,
            currentChat:
              get().openedChat === email
                ? new_users[existing_index].messages
                : state.currentChat,
          };
        }
        return { ...state, users: state.users };
      }
    });
  },

  addMessages: (email, messages) => {
    set((state) => {
      const existing_index = state.users.findIndex(
        (item) => item.email === email,
      );

      if (existing_index === undefined) {
        const new_user = {
          email,
          messages: [...messages],
        };
        return {
          ...state,
          users: [...state.users, new_user],
          currentChat:
            get().openedChat === email ? new_user.messages : state.currentChat,
        };
      } else {
        const existing_user = state.users[existing_index];
        const new_messages: IMessage[] = [...existing_user.messages];
        for (const msg of messages) {
          const existing_message = existing_user.messages.find(
            (item) => item.id === msg.id,
          );

          if (existing_message === undefined) {
            new_messages.push(msg);
          }
        }
        existing_user.messages = new_messages;

        const new_users = state.users.map((item, index) => {
          if (index === existing_index) {
            return existing_user;
          }
          return item;
        });

        return {
          ...state,
          users: new_users,
          currentChat:
            get().openedChat === email
              ? new_users[existing_index].messages
              : state.currentChat,
        };
      }
    });
  },

  getUsers: (myEmail) => {
    return get()
      .users.map((item) => item.email)
      .filter((item) => item !== myEmail);
  },

  getMessagesForUser: (email) => {
    const existing_user = get().users.find((item) => item.email === email);
    return existing_user?.messages ?? [];
  },
}));

export default useUserStore;
