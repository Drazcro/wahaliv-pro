/* eslint-disable */

import { IMessage } from "src_legacy/session/types";
import { create } from "zustand";

interface IChat {
  countMessages: number;
  setCountMessage: (countMessage: number) => void;
  unreadClientCount: number;
  setUnreadClientCount: (value: number) => void;

  newMessages: IMessage[];
  setNewMessages: (message: IMessage) => void;
  emptyArray: () => void;
}

export const useChatStore = create<IChat>((set) => ({
  countMessages: 0,
  unreadClientCount: 0,
  setUnreadClientCount: (value: number) => {
    set((state) => ({
      ...state, // Spread the existing state to ensure immutability
      unreadClientCount: state.unreadClientCount + value,
    }));
  },

  newMessages: [],
  setCountMessage: (value: number) =>
    set((state) => ({ countMessages: value })),

  setNewMessages: (message: IMessage) =>
    set((state) => ({
      newMessages: [...state.newMessages, message],
    })),

  emptyArray: () => {
    set((state) => ({
      newMessages: [],
    }));
  },
}));
