/* eslint-disable */

import { create } from "zustand";

interface ILoadingStore<T> {
  loadingNewOrder: number;
  setLoadingNewOrder: (payload: number) => void;
}

const vanillaLoadingStore = create<ILoadingStore<{}>>((set) => ({
  loadingNewOrder: 0,
  setLoadingNewOrder: (payload) =>
    set((state) => ({ loadingNewOrder: payload })),
}));

const loadingStore = vanillaLoadingStore as unknown as {
  <T>(): ILoadingStore<T>;
  <T, U>(selector: (s: ILoadingStore<T>) => U): U;
};

export { vanillaLoadingStore, loadingStore };
