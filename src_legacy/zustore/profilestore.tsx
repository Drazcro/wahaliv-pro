/* eslint-disable */

import {
  IPartnerProfileEarlyClosing,
  IProfile,
  IProfileInformation,
} from "src/interfaces/Entities";
import { ILatLong } from "src_legacy/components/delivery/maps/map_preview";
import { create } from "zustand";

interface IProfileStore<T> {
  profile: IProfileInformation;
  stats: IProfile;
  isOnService: boolean;
  position: ILatLong;
  earlyClosing: IPartnerProfileEarlyClosing | null;
  setPosition: (payload: ILatLong) => void;
  setProfile: (payload: IProfileInformation) => void;
  setService: (payload: boolean) => void;
  setEarlyClosing: (payload: IPartnerProfileEarlyClosing | null) => void;
  removeProfile: () => void;
  removePosition: () => void;
}

const vanillaProfileStore = create<IProfileStore<{}>>((set) => ({
  profile: {} as IProfileInformation,
  stats: {} as IProfile,
  position: {} as ILatLong,
  earlyClosing: {} as IPartnerProfileEarlyClosing,
  isOnService: false,
  setPosition: (payload) => set((state) => ({ position: payload })),
  setProfile: (payload) => set((state) => ({ profile: payload })),
  setService: (payload) => set((state) => ({ isOnService: payload })),
  setEarlyClosing: (payload) => set((state) => ({ earlyClosing: payload })),
  removeProfile: () => set({ profile: {} as IProfileInformation }),
  removePosition: () => set({ position: {} as ILatLong }),
}));

const profileStore = vanillaProfileStore as unknown as {
  <T>(): IProfileStore<T>;
  <T, U>(selector: (s: IProfileStore<T>) => U): U;
};

export { vanillaProfileStore, profileStore };
