/* eslint-disable */

import { IStep, ITournee } from "src/interfaces/Entities";
import { create } from "zustand";

interface ITourneeAPIResponseStore<T> {
  selectedTournee: ITournee;
  setSelectedTournee: (payload: ITournee) => void;
  shouldReloadTournee: boolean;
  setShouldReloadTournee: (payload: boolean) => void;
  currentTabIndex: number;
  setCurrentTabIndex: (payload: number) => void;
  steps: IStep[];
  setSteps: (steps: IStep[]) => void;
}

const vanillaTourneeStore = create<ITourneeAPIResponseStore<{}>>(
  (set, get) => ({
    selectedTournee: {} as ITournee,
    shouldReloadTournee: false,
    currentTabIndex: 0,

    steps: [],

    setSteps: (steps: IStep[]) => set(() => ({ steps })),

    setShouldReloadTournee: (payload) =>
      set(() => ({ shouldReloadTournee: payload })),
    setSelectedTournee: (payload) =>
      set((state) => ({ selectedTournee: payload })),
    setCurrentTabIndex: (payload) => set(() => ({ currentTabIndex: payload })),
  }),
);

const tourneeStore = vanillaTourneeStore as {
  <T>(): ITourneeAPIResponseStore<T>;
  <T, U>(selector: (s: ITourneeAPIResponseStore<T>) => U): U;
};

export { vanillaTourneeStore, tourneeStore };
