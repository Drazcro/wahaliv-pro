import React from "react";
import { GenericPermission, PermissionType } from "./generic_permission";
import * as Notifications from "expo-notifications";
import { PERMISSION_ACTION_TYPES } from "src_legacy/session/actions";
import { PERMISSION_LEVEL, IPermission } from "src_legacy/session/types";
import { usePermissions } from "src_legacy/session/context";

export function NotificationsPermission() {
  const { permissions, permissionDispatch } = usePermissions();

  const registerForPushNotificationsAsync = async () => {
    if (permissions.notifications < PERMISSION_LEVEL.ACCEPTED) {
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS,
        payload: { notifications: PERMISSION_LEVEL.ACCEPTED } as IPermission,
      });
    }

    const { status: existingStatus } =
      await Notifications.getPermissionsAsync();
    let finalStatus = existingStatus;
    if (existingStatus == "granted") {
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS,
        payload: { notifications: PERMISSION_LEVEL.GRANTED } as IPermission,
      });
      return;
    } else {
      const { status } = await Notifications.requestPermissionsAsync();
      finalStatus = status;
      if (finalStatus == "granted") {
        permissionDispatch({
          type: PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS,
          payload: { notifications: PERMISSION_LEVEL.GRANTED } as IPermission,
        });
        return;
      }
    }
    permissionDispatch({
      type: PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS,
      payload: { notifications: PERMISSION_LEVEL.DENIED } as IPermission,
    });
  };

  const onGrantCallback = () => {
    registerForPushNotificationsAsync();
  };

  const onDenyCallback = () => {
    permissionDispatch({
      type: PERMISSION_ACTION_TYPES.SET_NOTIFICATIONS,
      payload: { notifications: PERMISSION_LEVEL.DENIED } as IPermission,
    });
  };

  return (
    <GenericPermission
      permissionType={PermissionType.NOTIFICATION}
      onGrantCallback={onGrantCallback}
      onDenyCallback={() => onDenyCallback()}
    />
  );
}
