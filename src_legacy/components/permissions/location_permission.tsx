import React from "react";
import { Linking } from "react-native";
import * as Location from "expo-location";

import { GenericPermission, PermissionType } from "./generic_permission";
import { PERMISSION_ACTION_TYPES } from "src_legacy/session/actions";
import { IPermission, PERMISSION_LEVEL } from "src_legacy/session/types";
import { usePermissions } from "src_legacy/session/context";

const { DENIED } = PERMISSION_LEVEL;

export function LocationPermission() {
  const { permissions, permissionDispatch } = usePermissions();

  async function onGrantCallback() {
    console.log("Grant called");
    if (permissions.localisation == DENIED) {
      await Linking.openSettings();
      return;
    }

    if (permissions.localisation < PERMISSION_LEVEL.ACCEPTED) {
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
        payload: { localisation: PERMISSION_LEVEL.ACCEPTED } as IPermission,
      });
    }

    const { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== "granted") {
      console.log("Permission to access location was denied");
      permissionDispatch({
        type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
        payload: { localisation: PERMISSION_LEVEL.DENIED } as IPermission,
      });
      onGrantCallback();
    } else {
      console.log("Foreground Permission to access location was granted");
      const { status: back_status } =
        await Location.requestBackgroundPermissionsAsync();

      if (back_status !== "granted") {
        console.log("Background Permission to access location was denied");
        permissionDispatch({
          type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
          payload: { localisation: PERMISSION_LEVEL.DENIED } as IPermission,
        });
        onGrantCallback();
      } else {
        console.log("Background Permission to access location was granted");
        permissionDispatch({
          type: PERMISSION_ACTION_TYPES.SET_LOCALISATION,
          payload: { localisation: PERMISSION_LEVEL.GRANTED } as IPermission,
        });
      }
    }
  }

  return (
    <GenericPermission
      permissionType={PermissionType.LOCATION}
      onGrantCallback={() => onGrantCallback()}
    />
  );
}
