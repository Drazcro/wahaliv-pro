import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";

import { styles } from "./generic_permission.style";
import { images } from "src/theme";
import * as Device from "expo-device";

export enum PermissionType {
  NOTIFICATION = "notifications",
  LOCATION = "location",
}

interface IPermissionProps {
  permissionType: PermissionType;
  onGrantCallback: () => void;
  onDenyCallback?: () => void;
}

import i18n from "i18n-js";
import { StatusBar } from "expo-status-bar";
import { ScrollView } from "react-native-gesture-handler";
import useDeviceType from "src/hooks/useDeviceType";

export const getSVGByPermissionType = (type: PermissionType) => {
  const DIMENSIONS = 186;
  switch (type) {
    case PermissionType.LOCATION:
      return (
        <Image
          source={images.Landing.Location}
          style={{ width: 181, height: DIMENSIONS, resizeMode: "contain" }}
        />
      );
    case PermissionType.NOTIFICATION:
      return (
        <Image
          source={images.Landing.Notification}
          style={{ width: 181, height: DIMENSIONS, resizeMode: "contain" }}
        />
      );
  }
};

export function GenericPermission(props: IPermissionProps) {
  const { permissionType, onGrantCallback, onDenyCallback } = props;
  const title = i18n.t(`permissions.${permissionType}.title`);
  const newText = i18n.t(`permissions.${permissionType}.newText`);
  const acceptButton = i18n.t(`permissions.${permissionType}.acceptButton`);
  const denyButton = i18n.t(`permissions.${permissionType}.denyButton`);
  const isTablet = useDeviceType();

  return (
    <View style={styles.container}>
      <View style={styles.status}>
        <StatusBar style="dark" />
      </View>
      {permissionType == PermissionType.NOTIFICATION ? (
        <View style={styles.content}>
          {getSVGByPermissionType(permissionType)}
          <Text style={styles.permissionTitle}>{title}</Text>
          <Text
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.TabletPermissionText
                : styles.permissionText
            }
          >
            {newText}
          </Text>
          <TouchableOpacity
            onPress={() => {
              onGrantCallback();
            }}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{acceptButton}</Text>
          </TouchableOpacity>
          {onDenyCallback ? (
            <>
              <Text
                style={
                  isTablet && Device.brand != "SUNMI"
                    ? styles.TabletDenyButton
                    : styles.denyButton
                }
                onPress={onDenyCallback}
              >
                {denyButton}
              </Text>
            </>
          ) : null}
        </View>
      ) : null}
      {permissionType !== PermissionType.NOTIFICATION && (
        <View style={styles.content}>
          <View style={styles.contentFragmentDivider} />
          {getSVGByPermissionType(permissionType)}
          <ScrollView showsVerticalScrollIndicator={false}>
            <View
              style={{
                flex: 1,
                justifyContent: "space-between",
                flexDirection: "column",
              }}
            >
              <Text style={styles.permissionTitle}>{title}</Text>
              <Text
                style={{
                  ...styles.permissionText,
                  marginBottom: isTablet && Device.brand != "SUNMI" ? 40 : 20,
                }}
              >
                {i18n.t(`permissions.${permissionType}.newText`)}
              </Text>
            </View>
          </ScrollView>
          <TouchableOpacity
            onPress={() => {
              onGrantCallback();
            }}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{acceptButton}</Text>
          </TouchableOpacity>
          <View style={styles.contentFragmentDivider} />
        </View>
      )}
    </View>
  );
}
