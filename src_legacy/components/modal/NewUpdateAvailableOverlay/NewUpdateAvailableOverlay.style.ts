import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

export const styles = StyleSheet.create({
  overlayBox: {
    borderRadius: 15,
    alignItems: "center",
  },
  xPosition: {
    alignItems: "flex-end",
    marginTop: 20,
    marginRight: 20,
  },
  overlayBody: {
    alignItems: "center",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    fontSize: 18,
    marginBottom: 19,
  },
  subTitle: {
    textAlign: "center",
    paddingHorizontal: 10,
  },
  buttonsContainer: {
    marginTop: 36,
    marginBottom: 32,
    marginHorizontal: 27,
  },
  buttonPosition: {
    alignItems: "center",
    marginTop: 15,
  },
  button: {
    borderRadius: 30,
    width: 308,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
  },
  textButton: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
  },
});
