import React from "react";
import { View, Text, TouchableOpacity, Platform } from "react-native";
import Constants from "expo-constants";
import * as Linking from "expo-linking";
import { Overlay } from "react-native-elements";
import { styles } from "./NewUpdateAvailableOverlay.style";
import X from "assets/image_svg/X.svg";
import { APP_STORE_APP_URL, PLAY_STORE_APP_BASE_URL } from "src/constants/urls";
//import * as Sentry from "@sentry/react-native";

const makeUpdate = () => {
  try {
    const appStoreLink: string =
      Platform.OS === "ios"
        ? APP_STORE_APP_URL
        : PLAY_STORE_APP_BASE_URL + Constants.expoConfig?.android?.package;
    Linking.openURL(appStoreLink);
  } catch (error) {
    console.error(error);
    //Sentry.captureException(error);
  }
};

const NewUpdateAvailableOverlay = (props: {
  isVisible: boolean;
  onClose: () => void;
}) => {
  return (
    <Overlay
      isVisible={props.isVisible}
      overlayStyle={styles.overlayBox}
      onDismiss={props.onClose}
    >
      <View>
        <TouchableOpacity
          onPress={() => {
            props.onClose();
          }}
        >
          <View style={styles.xPosition}>
            <X onPress={props.onClose} />
          </View>
        </TouchableOpacity>
        <View style={styles.overlayBody}>
          <Text style={styles.title}>Nouvelle mise à jour disponible</Text>
          <Text style={styles.subTitle}>
            {
              "Une nouvelle version est disponible.\nMettez à jour l'application pour bénéficier des dernières fonctionnalités"
            }
          </Text>
          <View style={styles.buttonsContainer}>
            <TouchableOpacity
              style={styles.buttonPosition}
              onPress={makeUpdate}
            >
              <View
                style={[
                  styles.button,
                  { backgroundColor: "rgba(255, 129, 137, 1)" },
                ]}
              >
                <Text style={[styles.textButton, { color: "white" }]}>
                  Mettre à jour
                </Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.buttonPosition}
              onPress={() => {
                props.onClose();
              }}
            >
              <View
                style={[
                  styles.button,
                  { backgroundColor: "rgba(255, 255, 255, 1)" },
                ]}
              >
                <Text
                  style={[styles.textButton, { color: "rgba(29, 42, 64, 1)" }]}
                >
                  Plus tard
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Overlay>
  );
};

export default NewUpdateAvailableOverlay;
