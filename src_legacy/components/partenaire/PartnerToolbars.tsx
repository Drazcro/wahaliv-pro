import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CaretLeft from "assets/image_svg/CaretLeft.svg";
import DotsThreeOutline from "assets/image_svg/DotsThreeOutline.svg";
import WhiteScoot from "assets/image_svg/ScooterWhite.svg";
import CapaWhite from "assets/image_svg/CapaWhite.svg";
import { fontFamily } from "src/theme/typography";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { router } from "expo-router";

interface PartnerToolbarProps {
  title: string;
  openSetting: () => void;
  isInDelivery: boolean;
  isInTakeOout: boolean;
  isOrderlate: boolean;
}

export function PartnerToolbar(props: PartnerToolbarProps) {
  const isTablet = useDeviceType();

  return (
    <View
      style={
        isTablet && Device.brand != "SUNMI"
          ? styles.containerTablet
          : styles.container
      }
    >
      <TouchableOpacity
        onPress={() => router.back()}
        style={{
          marginLeft: isTablet && Device.brand != "SUNMI" ? 50 : undefined,
          padding: 10,
        }}
      >
        <CaretLeft />
      </TouchableOpacity>
      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? styles.textMainTablet
            : styles.textMain
        }
      >
        <View
          style={{
            ...(isTablet && Device.brand != "SUNMI"
              ? styles.circleTablet
              : styles.circle),
            backgroundColor: props.isOrderlate ? "#ED7178" : "#40ADA2",
          }}
        >
          {props.isInDelivery && <WhiteScoot />}
          {props.isInTakeOout && <CapaWhite />}
        </View>
        <View style={styles.textContainer}>
          <Text
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_textTitle
                : styles.textTitle
            }
          >
            {props.title}
          </Text>
          <Text style={styles.textSubtitle}>
            {props.isInDelivery && "En livraison"}
            {props.isInTakeOout && "À emporter"}
          </Text>
        </View>
      </View>
      <TouchableOpacity
        style={
          isTablet && Device.brand != "SUNMI" ? styles.iconTablet : styles.icon
        }
        onPress={props.openSetting}
      >
        <DotsThreeOutline />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    //paddingTop: 50,
    paddingBottom: 20,
    paddingHorizontal: 20,
  },
  containerTablet: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 50,
    paddingBottom: 20,
    paddingHorizontal: 25,
  },

  textMain: {
    flexDirection: "row",
    alignItems: "center",
  },
  textMainTablet: {
    flexDirection: "row",
    alignItems: "center",
    marginLeft: 45,
  },
  icon: {
    marginTop: -25,
  },
  iconTablet: {
    marginTop: -25,
    flex: 1,
    justifyContent: "flex-end",
    flexDirection: "row",
    paddingRight: 100,
  },
  textContainer: {
    flexDirection: "column",
    justifyContent: "center",
  },
  textTitle: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    lineHeight: 14,
  },
  textSubtitle: {
    fontFamily: fontFamily.regular,
    color: "#112842",
    fontSize: 16,
    lineHeight: 18,
    marginTop: 5,
  },
  circle: {
    alignItems: "center",
    justifyContent: "center",
    height: 35,
    width: 35,
    borderRadius: 50,
    marginRight: 10,
  },
  circleTablet: {
    alignItems: "center",
    justifyContent: "center",
    height: 46,
    width: 46,
    borderRadius: 50,
    marginRight: 10,
  },
  /** tablet screen */
  tablet_textTitle: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 20,
  },
});
