import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { fontFamily } from "src/theme/typography";

const LEFT_PAGE = "LEFT";
const RIGHT_PAGE = "RIGHT";

export const Pagination = ({
  totalRecords,
  pageLimit,
  currentPage,
  setCurrentPage,
  filterByCurrentPage,
}: {
  totalRecords: number;
  pageLimit: number;
  currentPage: number;
  setCurrentPage: (arg0: number) => void;
  filterByCurrentPage: (arg0: number) => void;
}) => {
  let pageNeighbours = 0;

  pageLimit = typeof pageLimit === "number" ? pageLimit : 30;
  totalRecords = typeof totalRecords === "number" ? totalRecords : 0;

  // pageNeighbours can be: 0, 1 or 2
  pageNeighbours =
    typeof pageNeighbours === "number"
      ? Math.max(0, Math.min(pageNeighbours, 2))
      : 0;

  const totalPages = Math.ceil(totalRecords / pageLimit);

  /**
   * Helper method for creating a range of numbers
   * range(1, 5) => [1, 2, 3, 4, 5]
   */
  const range = (from: number, to: number, step = 1) => {
    let i = from;
    const range: Array<string | number> = [];

    while (i <= to) {
      range.push(i);
      i += step;
    }

    return range;
  };

  /**
   * Let's say we have 10 pages and we set pageNeighbours to 2
   * Given that the current page is 6
   * The pagination control will look like the following:
   *
   * (1) < {4 5} [6] {7 8} > (10)
   *
   * (x) => terminal pages: first and last page(always visible)
   * [x] => represents current page
   * {...x} => represents page neighbours
   */
  const fetchPageNumbers = React.useMemo(() => {
    /**
     * totalNumbers: the total page numbers to show on the control
     * totalBlocks: totalNumbers + 2 to cover for the left(<) and right(>) controls
     */
    const totalNumbers = pageNeighbours * 2 + 3;
    const totalBlocks = totalNumbers + 2;

    if (totalPages > totalBlocks) {
      const startPage = Math.max(2, currentPage - pageNeighbours);
      const endPage = Math.min(totalPages - 1, currentPage + pageNeighbours);
      let pages: Array<number | string> = range(startPage, endPage);

      /**
       * hasLeftSpill: has hidden pages to the left
       * hasRightSpill: has hidden pages to the right
       * spillOffset: number of hidden pages either to the left or to the right
       */
      const hasLeftSpill = startPage > 2;
      const hasRightSpill = totalPages - endPage > 1;
      const spillOffset = totalNumbers - (pages.length + 1);

      switch (true) {
        // handle: (1) < {5 6} [7] {8 9} (10)
        case hasLeftSpill && !hasRightSpill: {
          const extraPages = range(startPage - spillOffset, startPage - 1);
          pages = [LEFT_PAGE, ...extraPages, ...pages];
          break;
        }

        // handle: (1) {2 3} [4] {5 6} > (10)
        case !hasLeftSpill && hasRightSpill: {
          const extraPages = range(endPage + 1, endPage + spillOffset);
          pages = [...pages, ...extraPages, RIGHT_PAGE];
          break;
        }

        // handle: (1) < {4 5} [6] {7 8} > (10)
        case hasLeftSpill && hasRightSpill:
        default: {
          pages = [LEFT_PAGE, ...pages, RIGHT_PAGE];
          break;
        }
      }

      return [1, ...pages, totalPages];
    }

    return range(1, totalPages);
  }, [pageNeighbours, currentPage]);

  const gotoPage = (page: number) => {
    const currentPage = Math.max(0, Math.min(page, totalPages));
    setCurrentPage(currentPage);
    filterByCurrentPage(currentPage);
  };

  const handleClick = (page: number) => {
    gotoPage(page);
  };

  const handleMoveLeft = React.useCallback(() => {
    gotoPage(currentPage - pageNeighbours * 2 - 1);
  }, [pageNeighbours, currentPage]);

  const handleMoveRight = React.useCallback(() => {
    gotoPage(currentPage + pageNeighbours * 2 + 1);
  }, [pageNeighbours, currentPage]);

  if (!totalRecords || totalPages === 1) return null;

  return (
    <View style={styles.main}>
      {fetchPageNumbers.map((page, index) => (
        <RenderPage
          key={index}
          page={page}
          handleClick={handleClick}
          handleMoveRight={handleMoveRight}
          currentPage={currentPage}
          handleMoveLeft={handleMoveLeft}
        />
      ))}
    </View>
  );
};

const RenderPage = ({
  page,
  handleMoveLeft,
  handleMoveRight,
  handleClick,
  currentPage,
}: {
  page: number | string;
  handleMoveLeft: () => void;
  handleMoveRight: () => void;
  handleClick: (page: number) => void;
  currentPage: number;
}) => {
  if (page === LEFT_PAGE)
    return (
      <TouchableOpacity
        onPress={handleMoveLeft}
        style={{ ...styles.btnActive, backgroundColor: "#E8E8E8" }}
      >
        <Text style={styles.btnText}>{"<<"}</Text>
      </TouchableOpacity>
    );

  if (page === RIGHT_PAGE)
    return (
      <TouchableOpacity
        onPress={handleMoveRight}
        style={{ ...styles.btnActive, backgroundColor: "#E8E8E8" }}
      >
        <Text style={styles.btnText}>{">>"}</Text>
      </TouchableOpacity>
    );

  return (
    <TouchableOpacity
      onPress={() => handleClick(page as number)}
      style={{
        ...styles.btnActive,
        backgroundColor: currentPage === page ? "#112842" : "#E8E8E8",
      }}
    >
      <Text
        style={{
          ...styles.btnText,
          color: currentPage === page ? "#fff" : "#1D2A40",
        }}
      >
        {page}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  btnActive: {
    height: 25,
    width: 25,
    borderRadius: 25,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 5,
  },
  btnText: {
    color: "#1D2A40",
    fontFamily: fontFamily.regular,
    fontSize: 12,
  },
});
