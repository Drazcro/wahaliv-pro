import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useEffect, useState } from "react";
import { fontFamily } from "src/theme/typography";
import Edit from "assets/image_svg/EditWhite.svg";
import PlanningTable from "./PlanningTable";
import ScheduleEditingOverlay from "../overlay/ScheduleEditingOverlay";
import { WeekSchedule } from "src/interfaces/Entities";
import { authStore } from "src_legacy/services/v2/auth_store";
import { getBaseUser } from "src_legacy/services";
import { get_week_planning } from "src_legacy/services/Loaders";

const PermanentPlanning = () => {
  const [open, setOpen] = useState(false);
  const [planning, setPlanning] = useState<WeekSchedule[]>([]);

  const { auth } = authStore();
  const base_user = getBaseUser(auth);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  useEffect(() => {
    if (!base_user) {
      return;
    }

    get_week_planning(base_user?.id)
      .then((res) => {
        console.log(res);
        setPlanning(res);
      })
      .catch((err) => console.error(err));
  }, [base_user?.id]);

  return (
    <View style={styles.container}>
      <ScheduleEditingOverlay open={open} close={handleClose} />
      <View style={styles.header}>
        <Text style={styles.title}>Planning</Text>
        <TouchableOpacity style={styles.editBtn} onPress={handleOpen}>
          <Text style={styles.editBtnLabel}>Editer</Text>
          <Edit />
        </TouchableOpacity>
      </View>
      <PlanningTable planning={planning} />
    </View>
  );
};

export default PermanentPlanning;

const styles = StyleSheet.create({
  container: {
    marginTop: 24,
    marginLeft: 18,
    marginRight: 24,
  },
  title: {
    color: "#1D2A40",
    fontSize: 20,
    fontFamily: fontFamily.semiBold,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  editBtn: {
    backgroundColor: "#FF5C85",
    borderRadius: 50,
    paddingVertical: 5,
    paddingHorizontal: 11,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    gap: 5,
  },
  editBtnLabel: {
    color: "#fff",
  },
});
