import { StyleSheet, TouchableOpacity } from "react-native";
import { Text } from "react-native-elements";
import StoreFrontW from "assets/image_svg_partner/StorefrontG.svg";
import FlecheW from "assets/image_svg_partner/FlecheG.svg";
import { fontFamily } from "src/theme/typography";
import { router } from "expo-router";

export const PartnerProfileBtnHeader = (props: {
  isOpen: boolean | undefined;
}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.main,
        backgroundColor: props.isOpen ? "rgba(64, 173, 162, 1)" : "#FF451D",
      }}
      onPress={() => router.push("/partner_profile_early_closing_screen")}
    >
      {!props.isOpen ? (
        <>
          <StoreFrontW />
          <Text style={{ ...styles.headerBtnText, color: "#fff" }}>Fermé</Text>
          <FlecheW />
        </>
      ) : (
        <>
          <StoreFrontW />
          <Text
            style={{
              ...styles.headerBtnText,
              color: "#fff",
            }}
          >
            Ouvert
          </Text>
          <FlecheW />
        </>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main: {
    position: "absolute",
    bottom: -17,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: 140,
    height: 39,
    borderRadius: 19,
    backgroundColor: "#fff",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    paddingHorizontal: 15,
  },
  headerBtnText: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 19.5,
  },
});
