import React, { ReactNode } from "react";
import {
  Dimensions,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-elements";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";

interface PartnerProfileNavigationProps {
  title: string;
  iconLeft: ReactNode;
  iconRight?: ReactNode;
  id: number;
  onPress: () => void;
}

export const PartnerProfileMenu = (props: PartnerProfileNavigationProps) => {
  const isTablet = useDeviceType();
  return (
    <React.Fragment>
      <View
        style={isTablet && Device.brand != "SUNMI" ? styles.tablet_main : {}}
      >
        <TouchableOpacity
          onPress={props.onPress}
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_container
              : styles.container
          }
        >
          <View style={styles.textContainer}>
            <View style={styles.left_icon}>{props.iconLeft}</View>
            <Text style={styles.text_title}>{props.title}</Text>
          </View>
          {props.iconRight ? (
            <View style={styles.right_icon}>{props.iconRight}</View>
          ) : null}
        </TouchableOpacity>
        {isTablet && Device.brand != "SUNMI" && (
          <View style={styles.line}>
            <Divider color="rgba(196, 196, 196, 1)" />
          </View>
        )}
      </View>
      {props.id === 2 ? <Divider style={{ marginBottom: 25 }} /> : null}
      {props.id === 4 ? <Divider style={{ marginBottom: 25 }} /> : null}
    </React.Fragment>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    alignItems: "center",
    marginBottom: Platform.OS === "android" ? 26 : 24,
  },
  textContainer: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignContent: "center",
    alignItems: "center",
  },
  left_icon: {
    color: "rgba(17, 40, 66, 1)",
    marginRight: 7,
  },
  right_icon: {
    color: "rgba(17, 40, 66, 1)",
  },
  text_title: {
    fontSize: 16,
    fontFamily: fontFamily.regular,
    color: "rgba(17, 40, 66, 1)",
    left: 10,
  },
  /** tablet screen */
  tablet_main: {
    marginBottom: Platform.OS === "android" ? 26 : 24,
  },
  tablet_container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignContent: "center",
    alignItems: "center",
    marginBottom: 12,
  },
  line: {
    // marginRight: -10,
  },
});
