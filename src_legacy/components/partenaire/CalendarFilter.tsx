import React, { useCallback } from "react";
import { SafeAreaView } from "react-native";
import { Calendar, ThemeType } from "src/libs/calendario";
import moment from "moment";
import { fontFamily } from "src/theme/typography";

const THEME: ThemeType = {
  monthTitleTextStyle: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "left",
  },
  weekColumnsContainerStyle: {},
  weekColumnStyle: {
    paddingVertical: 10,
  },
  weekColumnTextStyle: {
    fontFamily: fontFamily.regular,
    color: "#848689",
    fontSize: 12,
    lineHeight: 14.63,
  },
  nonTouchableDayContainerStyle: {},
  nonTouchableDayTextStyle: {
    fontSize: 15,
  },
  startDateContainerStyle: {
    backgroundColor: "#112842",
  },
  endDateContainerStyle: {
    backgroundColor: "#112842",
  },
  dayContainerStyle: {
    backgroundColor: "transparent",
  },
  dayTextStyle: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 14.63,
  },
  dayOutOfRangeContainerStyle: {},
  dayOutOfRangeTextStyle: {},
  todayContainerStyle: {},
  todayTextStyle: {
    color: "#6d95da",
  },
  activeDayContainerStyle: {
    backgroundColor: "#E8E8E8",
  },
  activeDayTextStyle: {
    color: "#1D2A40",
  },
  nonTouchableLastMonthDayTextStyle: {},
};

export const CalendatFilter = ({
  startDate,
  setStartDate,
  endDate,
  setEndDate,
}: {
  startDate: Date | undefined;
  setStartDate: (arg0: Date | undefined) => void;
  endDate: Date | undefined;
  setEndDate: (arg0: Date | undefined) => void;
}) => {
  const handleChangeDate = useCallback(
    (date) => {
      if (startDate) {
        if (endDate) {
          setStartDate(date);
          setEndDate(undefined);
        } else if (date < startDate) {
          setStartDate(date);
        } else if (date > startDate) {
          setEndDate(date);
        } else {
          setStartDate(date);
          setEndDate(date);
        }
      } else {
        setStartDate(date);
      }
    },
    [startDate, endDate],
  );

  return (
    <SafeAreaView>
      <Calendar
        onPress={handleChangeDate}
        startDate={startDate}
        endDate={endDate}
        startingMonth={moment(new Date()).format("YYYY-MM-DD")}
        monthHeight={310}
        firstDayMonday
        theme={THEME}
        locale="fr"
        dayNames={["L", "M", "M", "J", "V", "S", "D"]}
        numberOfMonths={2}
      />
    </SafeAreaView>
  );
};
