import React, { Dispatch, SetStateAction } from "react";
import { StyleSheet, Text, View } from "react-native";
import { TextInput, useTheme } from "react-native-paper";
import { fontFamily } from "src/theme/typography";
import FRFlag from "assets/image_svg/Flag.svg";
import { ProfileInformations, ProfileLegalInfo } from "src/interfaces/Entities";

type Props = {
  legalInfo: ProfileLegalInfo;
  setLegalInfo: Dispatch<SetStateAction<ProfileInformations>>;
};

const InformationJuridique = ({ legalInfo, setLegalInfo }: Props) => {
  const theme = useTheme();
  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Informations juridiques</Text>
        <View
          style={{
            width: "100%",
            marginTop: 32,
            gap: 23,
          }}
        >
          <View>
            <Text style={styles.inputLabel}>Nom commercial *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    business_name: text,
                  },
                }))
              }
              value={legalInfo?.business_name}
              maxLength={200}
              placeholder="Nom commercial"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Raison sociale *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    // ...theme.fonts.bodyLarge,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 17,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    company_name: text,
                  },
                }))
              }
              value={legalInfo?.company_name}
              maxLength={200}
              placeholder="Raison sociale"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Adresse postale *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    address: text,
                  },
                }))
              }
              value={legalInfo?.address}
              maxLength={200}
              placeholder="Adresse postale"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>RCS *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    rcs: text,
                  },
                }))
              }
              value={legalInfo?.rcs}
              maxLength={200}
              placeholder="RCS"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Téléphone fixe *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    telephone: text.replace(" ", ""),
                  },
                }))
              }
              value={legalInfo?.telephone}
              maxLength={200}
              placeholder="00 00 00 00 00"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
              left={<TextInput.Icon icon={() => <FRFlag />} />}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Siret *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.Medium,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    siret: text,
                  },
                }))
              }
              value={legalInfo?.siret}
              maxLength={200}
              placeholder="0000 0000 0000 00"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>TVA Inta-communautaire *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.Medium,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    intra_community_vat: text,
                  },
                }))
              }
              value={legalInfo?.intra_community_vat}
              maxLength={200}
              placeholder="FR 00 000 000 000"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Capital *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.Medium,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setLegalInfo((prevLegalInfo) => ({
                  ...prevLegalInfo,
                  legal_informations: {
                    ...prevLegalInfo.legal_informations,
                    capital: text,
                  },
                }))
              }
              value={legalInfo?.capital}
              maxLength={200}
              placeholder="0,00 €"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default InformationJuridique;

const styles = StyleSheet.create({
  container: {
    width: 355,
    // height: "100%",
    backgroundColor: "#fff",
    marginTop: 8,
    borderRadius: 10,
    paddingBottom: 10,
    // position: "relative",
  },
  formContainer: {
    marginHorizontal: 15,
    marginTop: 20,
    // height: "100%",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  title: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 13.899,
  },
  msgInput: {
    width: "100%",
    backgroundColor: "#fff",
    fontSize: 14,
  },

  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
});
