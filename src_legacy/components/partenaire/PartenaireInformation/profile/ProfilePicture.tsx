import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import ProfilePic from "assets/images/PartenaireProfile.svg";
import ProfileBackground from "assets/images/ProfileBackground.svg";
import Edit from "assets/image_svg/profile_edit.svg";
import { fontFamily } from "src/theme/typography";

export const ProfilePicture = ({ pageTitle }: { pageTitle: string }) => {
  return (
    <View style={styles.container}>
      <View>
        <ProfileBackground
          style={{
            borderTopLeftRadius: 10,
            borderTopRightRadius: 10,
          }}
        />
      </View>
      <View>
        <View style={styles.avatar}>
          <ProfilePic />
        </View>
        <View style={styles.editing}>
          <Text style={styles.title}>{pageTitle}</Text>
          <View style={styles.rightContainer}>
            <TouchableOpacity style={styles.editBtn}>
              <Edit />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 355,
    height: 180,
    backgroundColor: "#fff",
    marginTop: 8,
    borderRadius: 10,
    position: "relative",
  },
  editing: {
    marginTop: 20,
    marginLeft: 100,
    marginBottom: 21,
    justifyContent: "center",
    flexDirection: "row",
    alignItems: "center",
    // width: "50%",
  },
  editBtn: {
    alignSelf: "flex-end",
    marginRight: 15,
  },
  rightContainer: {
    marginLeft: "auto",
  },
  avatar: {
    position: "absolute",
    top: -40,
    left: 10,
  },
  title: {
    color: "#1D2A40",
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 14.899,
  },
});
