import React, { Dispatch, SetStateAction, useState } from "react";
import { TouchableOpacity } from "react-native";
import { StyleSheet, Text, View } from "react-native";
import { SelectList } from "react-native-dropdown-select-list";
import { TextInput, useTheme } from "react-native-paper";
import useDeviceType from "src/hooks/useDeviceType";
import { fontFamily } from "src/theme/typography";
import * as Device from "expo-device";
import {
  ProfileInformations,
  ProfilePersonalInfo,
} from "src/interfaces/Entities";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";

type Props = {
  personalInfo: ProfilePersonalInfo;
  setPersonalInfo: Dispatch<SetStateAction<ProfileInformations>>;
};

const civilities = [
  {
    key: "1",
    value: "Monsieur",
  },
  {
    key: "2",
    value: "Madame",
  },
];

const Interlocuteur = ({ personalInfo, setPersonalInfo }: Props) => {
  const isTablet = useDeviceType();
  const theme = useTheme();
  // const [disableBtn, setDisableBtn] = React.useState(true);
  const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);

  function hideDatePicker() {
    setIsDatePickerVisible(false);
  }

  function showDatePicker() {
    setIsDatePickerVisible(true);
  }

  const handleDateConfirm = (date: Date) => {
    hideDatePicker();
    setPersonalInfo((prev) => ({
      ...prev,
      personal_informations: {
        ...prev.personal_informations,
        dob: moment(date).format("YYYY-MM-DD"),
      },
    }));
    console.log(personalInfo.dob);
    // setDisableBtn(false);
  };

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Interlocuteur</Text>
        <View
          style={{
            width: "100%",
          }}
        >
          <Text style={styles.inputLabel}>
            Je suis joignable pour la journée au *
          </Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.Medium,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(text) =>
              setPersonalInfo((prevPersonalInfo) => ({
                ...prevPersonalInfo,
                personal_informations: {
                  ...prevPersonalInfo.personal_informations,
                  mobile: text,
                },
              }))
            }
            value={personalInfo?.mobile}
            maxLength={200}
            placeholder="+33 00 00 00 00 00"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View
          style={{
            width: "100%",
          }}
        >
          <Text style={styles.inputLabel}>Civilité *</Text>
          <SelectList
            setSelected={(val: string) => {
              setPersonalInfo((prevPersonalInfo) => ({
                ...prevPersonalInfo,
                personal_informations: {
                  ...prevPersonalInfo.personal_informations,
                  civility: val,
                },
              }));
            }}
            save="value"
            data={civilities}
            search={false}
            boxStyles={{
              marginTop: 10,
              borderColor: "1px solid rgba(196, 196, 196, 1)",
              height: 44,
            }}
            dropdownStyles={{
              marginVertical: 20,
            }}
            defaultOption={{
              key: "1",
              value: personalInfo?.civility,
            }}
            placeholder="Selectioner une option"
            inputStyles={{
              fontFamily: fontFamily.Medium,
              color: "#8D98A0",
              fontSize: 14,
            }}
          />
        </View>
        <View
          style={{
            width: "100%",
          }}
        >
          <Text style={styles.inputLabel}>Nom *</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.Medium,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(text) =>
              setPersonalInfo((prevPersonalInfo) => ({
                ...prevPersonalInfo,
                personal_informations: {
                  ...prevPersonalInfo.personal_informations,
                  last_name: text,
                },
              }))
            }
            value={personalInfo?.last_name}
            maxLength={200}
            placeholder="Nom"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View
          style={{
            width: "100%",
          }}
        >
          <Text style={styles.inputLabel}>Prénom *</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.Medium,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(text) =>
              setPersonalInfo((prevPersonalInfo) => ({
                ...prevPersonalInfo,
                personal_informations: {
                  ...prevPersonalInfo.personal_informations,
                  first_name: text,
                },
              }))
            }
            value={personalInfo?.first_name}
            maxLength={200}
            placeholder="Prénom"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View
          style={{
            width: "100%",
          }}
        >
          <Text style={styles.inputLabel}>Email *</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.Medium,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(text) =>
              setPersonalInfo((prevPersonalInfo) => ({
                ...prevPersonalInfo,
                personal_informations: {
                  ...prevPersonalInfo.personal_informations,
                  email: text,
                },
              }))
            }
            value={personalInfo?.email}
            maxLength={200}
            placeholder="Email"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View
          style={{
            width: "100%",
          }}
        >
          <DateTimePickerModal
            isVisible={isDatePickerVisible}
            display="spinner"
            mode="date"
            onConfirm={handleDateConfirm}
            onCancel={hideDatePicker}
            locale="fr-FR"
            cancelTextIOS="Annuler"
            confirmTextIOS="Valider"
          />
          <Text style={styles.inputLabel}>Date de naissance *</Text>
          <TouchableOpacity
            onPress={() => showDatePicker()}
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tabletInputContent
                : styles.inputContent
            }
          >
            <Text
              style={{
                ...styles.inputDate,
                color: "#1D2A40",
              }}
            >
              {!personalInfo?.dob ? "JJ/MM/AA" : personalInfo?.dob}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Interlocuteur;

const styles = StyleSheet.create({
  container: {
    width: 355,
    // height: "100%",
    marginTop: 10,
    backgroundColor: "#fff",
    borderRadius: 10,
    position: "relative",
    // marginBottom: 30,
  },
  title: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 13.899,
  },
  formContainer: {
    marginHorizontal: 15,
    marginVertical: 20,
    gap: 20,
    // height: "100%",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  msgInput: {
    width: "100%",
    backgroundColor: "#fff",
    fontSize: 14,
    height: 42,
  },

  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
  tabletInput: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderColor: "#6C6C6C",
    borderWidth: 1,
    borderRadius: 30,
    height: 46,
  },
  tabletInputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginRight: 200,
  },
  inputContent: {
    borderWidth: 1,
    borderColor: "#C4C4C4",
    borderRadius: 15,
    height: 42,
    justifyContent: "center",
    // alignItems: "center",
    paddingLeft: 16,
  },
  inputDate: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    color: "#8D98A0",
  },
});
