import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Divider } from "react-native-paper";
import { fontFamily } from "src/theme/typography";
import ErrorIcon from "assets/image_svg/DownalodErrorIcon.svg";
import * as DocumentPicker from "expo-document-picker";

type Props = {
  id: number;
  title: string;
  fileTypes: string;
  openOverlay: () => void;
};

const DocumentDownloadCard = (props: Props) => {
  function uploadDocument() {
    console.log("hit");

    // try {
    //   const docRes = await DocumentPicker.getDocumentAsync({
    //     type: "documents/*",
    //   });

    //   console.log(docRes);
    // } catch (error) {
    //   console.error(error);
    // }
  }

  return (
    <React.Fragment>
      <View style={styles.cardContainer}>
        <View>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "flex-start",
              gap: 5,
            }}
          >
            <ErrorIcon />
            <View>
              <Text style={styles.title}>{props.title}</Text>
              <Text style={styles.subTitle}>{props.fileTypes}</Text>
              {props.id === 3 ? (
                <TouchableOpacity
                  style={{
                    borderBottomWidth: 1,
                  }}
                  onPress={props.openOverlay}
                >
                  <Text
                    style={{
                      marginTop: 20,
                      fontSize: 14,
                      fontFamily: fontFamily.regular,
                      lineHeight: 18,
                      fontStyle: "italic",
                      color: "#1D2A40",
                    }}
                  >
                    Commet obtenir ce document ?
                  </Text>
                </TouchableOpacity>
              ) : null}
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.downloadBtn}
          onPress={() => console.log("hit")}
        >
          <Text style={styles.downloadBtnLabel}>Télécharger</Text>
        </TouchableOpacity>
      </View>
      <Divider
        style={{
          borderWidth: 0.2,
          borderColor: "#c4c4c4",
          marginTop: 5,
          marginBottom: 10,
        }}
      />
    </React.Fragment>
  );
};

export default DocumentDownloadCard;

const styles = StyleSheet.create({
  cardContainer: {
    marginTop: 10,
    marginBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 14,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    lineHeight: 18,
  },
  subTitle: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    lineHeight: 18,
  },
  downloadBtn: {
    width: 93,
    height: 27,
    backgroundColor: "#FF5C85",
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  downloadBtnLabel: {
    color: "#fff",
    fontSize: 12,
    fontFamily: fontFamily.semiBold,
  },
});
