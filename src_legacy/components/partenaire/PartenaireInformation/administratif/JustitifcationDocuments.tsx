import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { fontFamily } from "src/theme/typography";
import DocumentDownloadCard from "./DocumentDownloadCard";
import { Case } from "src/interfaces/Entities";

const cases: Case[] = [
  {
    id: 1,
    title: "Pièce d'identité recto",
    fileTypes: "(JPG, PNG, PDF max 6 Mo)",
    docType: "recto",
  },
  {
    id: 2,
    title: "Pièce d'identité verso",
    fileTypes: "(JPG, PNG, PDF max 6 Mo)",
    docType: "verso",
  },
  {
    id: 3,
    title: "RIB",
    fileTypes: "(JPG, PNG, PDF max 6 Mo)",
    docType: "rib",
  },
  {
    id: 4,
    title: "Kbis",
    fileTypes: "(JPG, PNG, PDF max 6 Mo)",
    docType: "Kbis",
  },
];

const JustitifcationDocuments = ({
  openOverlay,
}: {
  openOverlay: () => void;
}) => {
  return (
    <View style={styles.container}>
      {/* <Text style={styles.title}>Documents justificatifs</Text> */}
      <View
        style={{
          marginTop: 47,
        }}
      >
        {cases.map((caseItem) => (
          <DocumentDownloadCard
            key={caseItem.id}
            case={caseItem}
            openOverlay={openOverlay}
          />
        ))}
      </View>
      <Text
        style={{
          fontFamily: fontFamily.Medium,
          fontSize: 14,
          color: "#1D2A40",
        }}
      >
        *Champs obligatoires
      </Text>
    </View>
  );
};

export default JustitifcationDocuments;

const styles = StyleSheet.create({
  container: {
    width: 355,
    // height: "100%",
    backgroundColor: "#fff",
    marginTop: 8,
    borderRadius: 10,
    paddingBottom: 10,
    paddingVertical: 10,
    paddingHorizontal: 15,
    // position: "relative",
  },
  title: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 13.899,
  },
});
