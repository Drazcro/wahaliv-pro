import React, { Dispatch, SetStateAction } from "react";
import { StyleSheet, Text, View } from "react-native";
import { SelectList } from "react-native-dropdown-select-list";
import { TextInput, useTheme } from "react-native-paper";
import { AdminJusti, BankingInformations } from "src/interfaces/Entities";
import { fontFamily } from "src/theme/typography";

type Props = {
  bankData: BankingInformations;
  setBankData: Dispatch<SetStateAction<AdminJusti>>;
};

const FrequencyOptions = [
  {
    key: "1",
    value: "Weekly",
  },
  {
    key: "2",
    value: "Monthly",
  },
  {
    key: "3",
    value: "Hebdomadaire",
  },
  {
    key: "4",
    value: "Mensuel",
  },
];

const BandDetails = ({ bankData, setBankData }: Props) => {
  const theme = useTheme();

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <Text style={styles.title}>Coordonnées bancaires</Text>
        <View
          style={{
            width: "100%",
            marginTop: 32,
            gap: 23,
          }}
        >
          <View>
            <Text style={styles.inputLabel}>IBAN *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setBankData((prev) => ({
                  ...prev,
                  banking_informations: {
                    ...prev.banking_informations,
                    iban: text,
                  },
                }))
              }
              value={bankData?.iban}
              maxLength={200}
              placeholder="AA 00 0000 0000 0000 0000"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>SWIFT *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    // ...theme.fonts.bodyLarge,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 17,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setBankData((prev) => ({
                  ...prev,
                  banking_informations: {
                    ...prev.banking_informations,
                    swift: text,
                  },
                }))
              }
              value={bankData?.swift}
              maxLength={200}
              placeholder="000000000"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Banque *</Text>
            <TextInput
              theme={{
                fonts: {
                  bodyLarge: {
                    ...theme.fonts.bodySmall,
                    fontFamily: fontFamily.regular,
                  },
                },
              }}
              outlineStyle={{
                borderWidth: 1,
                borderColor: "#C4C4C4",
                borderRadius: 15,
              }}
              mode="outlined"
              style={styles.msgInput}
              onChangeText={(text) =>
                setBankData((prev) => ({
                  ...prev,
                  banking_informations: {
                    ...prev.banking_informations,
                    bank_name: text,
                  },
                }))
              }
              value={bankData?.bank_name}
              maxLength={200}
              placeholder="Banque"
              placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
            />
          </View>
          <View>
            <Text style={styles.inputLabel}>Fréquence de reversement *</Text>
            <SelectList
              setSelected={(val: string) => {
                setBankData((prev) => ({
                  ...prev,
                  banking_informations: {
                    ...prev.banking_informations,
                    frequency: val,
                  },
                }));
              }}
              save="value"
              data={FrequencyOptions}
              search={false}
              boxStyles={{
                marginTop: 10,
                borderColor: "1px solid rgba(196, 196, 196, 1)",
                height: 44,
              }}
              dropdownStyles={{
                marginVertical: 20,
              }}
              defaultOption={{
                key: "-1",
                value: `${bankData?.frequency}`,
              }}
              placeholder="Selectioner une option"
              inputStyles={{
                fontFamily: fontFamily.Medium,
                color: "#8D98A0",
                fontSize: 14,
              }}
            />
          </View>
        </View>
      </View>
    </View>
  );
};

export default BandDetails;

const styles = StyleSheet.create({
  container: {
    width: 355,
    // height: "100%",
    backgroundColor: "#fff",
    marginTop: 8,
    borderRadius: 10,
    paddingBottom: 10,
    // position: "relative",
  },
  formContainer: {
    marginHorizontal: 15,
    marginTop: 20,
    // height: "100%",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  title: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 13.899,
  },
  msgInput: {
    width: "100%",
    backgroundColor: "#fff",
    fontSize: 14,
  },

  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
});
