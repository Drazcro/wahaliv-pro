import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-paper";
import Edit from "assets/image_svg/profile_edit.svg";
import Delete from "assets/image_svg/delete.svg";
import Add from "assets/image_svg/add.svg";
import QuotasOverlay from "../../overlay/QuotasOverlay";

const Limit = () => {
  return (
    <View
      style={{
        marginTop: 20,
        width: "95%",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <View>
        <Text style={styles.date}>Samedi de 20h à 22h</Text>
        <Text style={styles.quantity}>max 30 commandes </Text>
      </View>
      <View style={styles.actions}>
        <TouchableOpacity>
          <Edit />
        </TouchableOpacity>
        <TouchableOpacity>
          <Delete />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const CommandsQuantityController = () => {
  const [open, setOpen] = useState(false);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  return (
    <View style={styles.container}>
      <QuotasOverlay open={open} close={handleClose} />
      <Text style={styles.title}>
        Limiter la quantité des commandes avec quotas
      </Text>
      <Limit />
      <Divider
        style={{ borderColor: "#c4c4c4", borderWidth: 0.2, marginTop: 12 }}
      />
      <Limit />
      <Divider
        style={{ borderColor: "#c4c4c4", borderWidth: 0.2, marginTop: 12 }}
      />
      <TouchableOpacity style={styles.AddBtn} onPress={handleOpen}>
        <Add />
        <Text style={styles.btnLabel}>Ajouter quota</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CommandsQuantityController;

const styles = StyleSheet.create({
  container: {
    width: 355,
    backgroundColor: "#fff",
    marginTop: 8,
    paddingTop: 20,
    paddingLeft: 14,
    borderRadius: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    lineHeight: 18,
    color: "#1D2A40",
    paddingTop: 3,
    width: "95%",
  },
  date: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
  },
  quantity: {
    fontSize: 14,
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
  },
  actions: {
    flexDirection: "row",
    gap: 21,
  },
  AddBtn: {
    borderWidth: 1,
    borderColor: "#40ADA2",
    width: 135,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    gap: 6,
    marginTop: 16,
    borderRadius: 30,
  },
  btnLabel: {
    fontSize: 12,
    fontFamily: fontFamily.semiBold,
    color: "#40ADA2",
  },
});
