import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { Switch } from "react-native-elements";
import { Divider } from "react-native-paper";
import { GeneralInfo } from "src/interfaces/Entities";
import { fontFamily } from "src/theme/typography";

type Props = {
  generalInfo: GeneralInfo;
};

const GeneralInformation = ({ generalInfo }: Props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Information General</Text>
      <View
        style={{
          marginTop: 37,
        }}
      >
        <View style={styles.info}>
          <Text style={styles.label}>Compte client</Text>
          <Text
            style={{
              fontSize: 14,
              color: "#1D2A40",
              fontFamily: fontFamily.Medium,
            }}
          >
            {generalInfo?.account_id}
          </Text>
        </View>
        <Divider style={{ marginBottom: 2, marginTop: 15 }} />
        <View style={styles.info}>
          <Text style={styles.label}>Assujetti la TVA</Text>
          <Switch
            ios_backgroundColor="red"
            trackColor={{
              true: "rgba(64, 173, 162, 1)",
              false: "rgba(243, 106, 114, 1)",
            }}
            color={"rgba(64, 173, 162, 1)"}
            // onValueChange={toggleFetchTask}
            value={generalInfo?.is_assujetti}
          />
        </View>
        <Divider style={{ marginBottom: 2, marginTop: 5 }} />
        <View style={styles.info}>
          <Text style={{ ...styles.label, width: "80%" }}>
            Acceptation automatique de commandes
          </Text>
          <Switch
            ios_backgroundColor="red"
            trackColor={{
              true: "rgba(64, 173, 162, 1)",
              false: "rgba(243, 106, 114, 1)",
            }}
            color={"rgba(243, 106, 114, 1)"}
            // onValueChange={toggleFetchTask}
            value={generalInfo?.is_order_auto_valid}
          />
        </View>
        <Divider style={{ marginBottom: 2, marginTop: 5 }} />
        <View style={styles.info}>
          <Text style={{ ...styles.label, width: "80%" }}>
            Imprimer seulement les commandes du jour
          </Text>
          <Switch
            ios_backgroundColor="red"
            trackColor={{
              true: "rgba(64, 173, 162, 1)",
              false: "rgba(243, 106, 114, 1)",
            }}
            color={"rgba(64, 173, 162, 1)"}
            // onValueChange={toggleFetchTask}
            value={generalInfo?.print_only_daily_order}
          />
        </View>
        <Divider style={{ marginBottom: 2, marginTop: 5 }} />
        <View style={styles.info}>
          <Text style={{ ...styles.label, width: "80%" }}>
            Page supervision de commande
          </Text>
          <Switch
            ios_backgroundColor="red"
            trackColor={{
              true: "rgba(64, 173, 162, 1)",
              false: "rgba(243, 106, 114, 1)",
            }}
            color={"rgba(64, 173, 162, 1)"}
            // onValueChange={toggleFetchTask}
            value={generalInfo?.has_order_supervision_page}
          />
        </View>
      </View>
    </View>
  );
};

export default GeneralInformation;

const styles = StyleSheet.create({
  container: {
    width: 355,
    backgroundColor: "#fff",
    marginTop: 8,
    paddingTop: 20,
    paddingLeft: 14,
    borderRadius: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    lineHeight: 13.446,
    color: "#1D2A40",
    paddingTop: 3,
  },
  label: {
    fontSize: 14,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
  },
  info: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginRight: 27,
  },
});
