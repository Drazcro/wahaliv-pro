import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { TextInput, useTheme } from "react-native-paper";
import { TvaInfo } from "src/interfaces/Entities";
import { fontFamily } from "src/theme/typography";

type Props = {
  tvaInfo: TvaInfo;
};

export const Tva = ({ tvaInfo }: Props) => {
  const theme = useTheme();
  console.log("tva info: ", tvaInfo);
  return (
    <View style={styles.container}>
      <Text style={styles.title}>TVA</Text>
      <View
        style={{
          justifyContent: "flex-start",
          alignItems: "flex-start",
          gap: 16,
          marginTop: 21,
        }}
      >
        <View style={styles.formControl}>
          <Text style={styles.inputLabel}>Taux TVA 1</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.regular,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            // onChangeText={(_text: string) => setText(_text)}
            value={tvaInfo?.tva_rate_1 ? `${tvaInfo?.tva_rate_1}%` : ""}
            maxLength={200}
            placeholder="0,00 %"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View style={styles.formControl}>
          <Text style={styles.inputLabel}>Taux TVA 2</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.regular,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            // onChangeText={(_text: string) => setText(_text)}
            value={tvaInfo?.tva_rate_2 ? `${tvaInfo?.tva_rate_2}%` : ""}
            maxLength={200}
            placeholder="0,00 %"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
        <View style={styles.formControl}>
          <Text style={styles.inputLabel}>Taux TVA 3</Text>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodySmall,
                  fontFamily: fontFamily.regular,
                },
              },
            }}
            outlineStyle={{
              borderWidth: 1,
              borderColor: "#C4C4C4",
              borderRadius: 15,
            }}
            mode="outlined"
            style={styles.msgInput}
            // onChangeText={(_text: string) => setText(_text)}
            value={tvaInfo?.tva_rate_3 ? `${tvaInfo?.tva_rate_3}%` : ""}
            maxLength={200}
            placeholder="0,00 %"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 355,
    backgroundColor: "#fff",
    marginTop: 8,
    paddingTop: 20,
    paddingLeft: 14,
    borderRadius: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    lineHeight: 13.446,
    color: "#1D2A40",
    paddingTop: 3,
  },
  formControl: {
    width: "100%",
  },
  inputLabel: {
    color: "#1D2A40",
    paddingTop: 2,
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 14,
    marginBottom: 6,
  },
  msgInput: {
    width: "95%",
    backgroundColor: "#fff",
    fontSize: 14,
  },
});
