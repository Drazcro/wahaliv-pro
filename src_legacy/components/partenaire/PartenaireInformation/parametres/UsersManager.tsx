import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React, { useState } from "react";
import { fontFamily } from "src/theme/typography";
import Edit from "assets/image_svg/profile_edit.svg";
import Delete from "assets/image_svg/delete.svg";
import { Divider } from "react-native-elements";
import ComfirmationOverlay from "../../overlay/ComfirmationOverlay";
import NewUserOverlay from "../../overlay/NewUserOverlay";
import { Collaborator } from "src/interfaces/Entities";

const UsersList = ({
  openModal,
  collaborator,
}: {
  openModal: () => void;
  collaborator: Collaborator;
}) => {
  return (
    <View style={styles.UserContainer} key={collaborator.id}>
      <View>
        <View style={styles.NameAndRole}>
          <Text style={styles.Name}>{collaborator.name}</Text>
          <Text style={styles.Role}>({collaborator.options[0]})</Text>
        </View>
        <Text style={styles.Email}>{collaborator.email}</Text>
      </View>
      <View style={styles.actions}>
        <TouchableOpacity>
          <Edit />
        </TouchableOpacity>
        <TouchableOpacity onPress={openModal}>
          <Delete />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const UsersManager = ({ collaborators }: { collaborators: Collaborator[] }) => {
  const [open, setOpen] = useState(false);
  const [openNewUserForm, setOpenNewUser] = useState(false);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function handleOpenForm() {
    setOpenNewUser(true);
  }

  function handleCloseForm() {
    setOpenNewUser(false);
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Gestion des utilisateurs</Text>
      {collaborators?.map((collaborator) => (
        <View key={collaborator.id}>
          <UsersList openModal={handleOpen} collaborator={collaborator} />
          <Divider style={{ marginTop: 5 }} />
        </View>
      ))}
      <TouchableOpacity style={styles.addUserBtn} onPress={handleOpenForm}>
        <Text style={styles.addBtnLabel}>Ajouter un collaborateur</Text>
      </TouchableOpacity>
      <ComfirmationOverlay open={open} close={handleClose} />
      <NewUserOverlay open={openNewUserForm} close={handleCloseForm} />
    </View>
  );
};

export default UsersManager;

const styles = StyleSheet.create({
  container: {
    width: 355,
    backgroundColor: "#fff",
    marginTop: 8,
    paddingTop: 20,
    paddingLeft: 14,
    borderRadius: 10,
    paddingBottom: 10,
  },
  title: {
    fontSize: 16,
    fontFamily: fontFamily.semiBold,
    lineHeight: 18,
    color: "#1D2A40",
    paddingTop: 3,
    width: "95%",
  },
  UserContainer: {
    marginTop: 15,
    flexDirection: "row",
    justifyContent: "space-between",
    marginRight: 15,
  },
  actions: {
    flexDirection: "row",
    gap: 21,
  },
  NameAndRole: {
    flexDirection: "row",
    alignItems: "center",
    gap: 2,
  },
  Name: {
    fontSize: 14,
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
  },
  Role: {
    fontSize: 12,
    fontFamily: fontFamily.regular,
    color: "#8D98A0",
    fontStyle: "italic",
  },
  Email: {
    color: "#8D98A0",
    fontSize: 12,
    fontFamily: fontFamily.Medium,
  },
  addUserBtn: {
    backgroundColor: "#FF5C85",
    borderRadius: 30,
    width: 232,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22,
    marginBottom: 40,
  },
  addBtnLabel: {
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
    color: "#fff",
  },
});
