import React from "react";
import { StyleSheet, View } from "react-native";
import ContentLoader, { Rect, Circle } from "react-content-loader/native";
import useDeviceType from "src/hooks/useDeviceType";

const MobileSkeleton = () => {
  return (
    <>
      <Circle x="20" y="0" cx="13" cy="13" r="13" />
      <Rect x="50" y="3" rx="5" ry="5" width="81" height="20" />
      <Rect x="240" y="0" rx="5" ry="5" width="110" height="25" />
      <Circle x="20" y="35" cx="11" cy="11" r="11" />
      <Rect x="50" y="38" rx="5" ry="5" width="102" height="16" />
      <Circle x="20" y="68" cx="11" cy="11" r="11" />
      <Rect x="50" y="70" rx="3" ry="3" width="216" height="12" />
      <Rect x="20" y="108" rx="3" ry="3" width="90" height="12" />
      <Rect x="235" y="108" rx="3" ry="3" width="70" height="12" />
      <Circle x="310" y="100" cx="13" cy="13" r="10" />
    </>
  );
};

const TabletSkeleton = () => {
  return (
    <>
      <Circle x="20" y="10" cx="13" cy="13" r="14" />
      <Rect x="61" y="12" rx="5" ry="5" width="100" height="20" />
      <Rect x="640" y="10" rx="5" ry="5" width="120" height="25" />
      <Circle x="20" y="45" cx="13" cy="13" r="11" />
      <Rect x="60" y="49" rx="5" ry="5" width="125" height="15" />
      <Rect x="245" y="48" rx="5" ry="5" width="50" height="15" />
      <Circle x="300" y="40" cx="13" cy="13" r="11" />
      <Circle x="20" y="75" cx="13" cy="13" r="11" />
      <Rect x="61" y="83" rx="5" ry="5" width="265" height="12" />
      <Rect x="540" y="83" rx="5" ry="5" width="110" height="12" />
      <Rect x="665" y="83" rx="5" ry="5" width="70" height="12" />
      <Circle x="740" y="76" cx="13" cy="13" r="10" />
    </>
  );
};

export const PartnerCardsLoader = () => {
  const isTablet = useDeviceType();
  return (
    <View
      style={{
        ...style.main,
        width: !isTablet ? 368 : 730,
        height: !isTablet ? 175 : 115,
      }}
    >
      <ContentLoader
        height={!isTablet ? 175 : 115}
        width={!isTablet ? 368 : 730}
        speed={1}
        viewBox={isTablet ? "0 0 800 110" : "0 0 350 110"}
        backgroundColor="#DADADA"
      >
        {!isTablet ? <MobileSkeleton /> : <TabletSkeleton />}
      </ContentLoader>
    </View>
  );
};

const style = StyleSheet.create({
  main: {
    borderColor: "#DADADA",
    borderWidth: 1,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 10,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginBottom: 11,
  },
});
