import React from "react";
import { StyleSheet, View } from "react-native";
import ContentLoader, { Rect, Circle } from "react-content-loader/native";

export const PartnerCardsLoaderTablette = () => {
  return (
    <View
      style={{
        ...style.main,
        width: 730,
        height: 115,
      }}
    >
      <ContentLoader
        height={115}
        width={730}
        speed={1}
        viewBox="0 0 700 110"
        backgroundColor="#DADADA"
      >
        <Circle x="20" y="10" cx="13" cy="13" r="14" />
        <Rect x="61" y="12" rx="5" ry="5" width="100" height="20" />
        <Rect x="570" y="12" rx="5" ry="5" width="100" height="25" />
        <Circle x="20" y="45" cx="13" cy="13" r="11" />
        <Rect x="60" y="49" rx="5" ry="5" width="165" height="15" />
        <Circle x="280" y="40" cx="12" cy="12" r="12" />
        <Rect x="310" y="45" rx="5" ry="5" width="220" height="15" />
        <Rect x="25" y="83" rx="5" ry="5" width="85" height="12" />
        <Rect x="510" y="83" rx="5" ry="5" width="90" height="12" />
        <Rect x="620" y="83" rx="5" ry="5" width="50" height="12" />
      </ContentLoader>
    </View>
  );
};
const style = StyleSheet.create({
  main: {
    borderColor: "#DADADA",
    borderWidth: 1,
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: 10,
    backgroundColor: "#fff",
    borderRadius: 5,
    marginBottom: 11,
  },
});
