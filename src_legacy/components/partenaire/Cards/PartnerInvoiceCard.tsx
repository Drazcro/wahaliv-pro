import React from "react";
import { Linking, StyleSheet, Text, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { fontFamily } from "src/theme/typography";
import moment from "moment";
import {
  download_customer_bill,
  download_partner_bill,
} from "src_legacy/services/Loaders";
import Toast from "react-native-expo-popup-notifications";
import I18n from "i18n-js";
import { authToUserId } from "src_legacy/services/Utils";
import { authStore } from "src_legacy/services/v2/auth_store";

moment.locale("fr");

export function PartnerInvoiceCards(props: any) {
  const startDate = moment(props.data.interval.begin.date).format("LL");
  const endDate = moment(props.data.interval.last.date).format("LL");
  const amountDate = moment(props.data.action_at.date).format("LL");
  const { auth } = authStore();
  const user_id = authToUserId(auth);
  let action = "";

  if (props.data.action === "à reverser") action = "A payer";
  else if (props.data.action === "reversé") action = " payé";
  return (
    <View style={styles.invoiceCardBody}>
      <View style={styles.cardHeader}>
        <Text style={styles.invoiceNumber}>{props.data.id}</Text>
        <View style={styles.invoiceStatus}>
          <Text style={styles.invoiceStatusTxt}>{action}</Text>
        </View>
      </View>
      <View style={styles.hr} />
      <View style={styles.datePosition}>
        <Text style={styles.dateTxt}>
          {startDate} - {endDate}
        </Text>
      </View>
      <View style={styles.amountPosition}>
        <Text style={styles.amountTxt}>
          Montant prélevé {props.data.amount}€
        </Text>
      </View>
      <View style={styles.amountDatePosition}>
        <Text style={styles.amountDateTxt}>Prélevé le {amountDate}</Text>
      </View>
      <TouchableOpacity
        style={styles.DlInvoice}
        onPress={() => {
          download_partner_bill(user_id, props.data.id)
            .then((response) => {
              const dowloadBill = response;
              Linking.openURL(dowloadBill);
            })
            .catch(() => {
              Toast.show({
                type: "error",
                text1: "Erreur",
                text2: I18n.t("profile.notification.error.message5"),
              });
            });
        }}
      >
        <Text style={styles.dlInvoiceTxt}>Télécharger la facture</Text>
      </TouchableOpacity>
    </View>
  );
}

export function PartnerCustomerInvoiceCards(props: any) {
  const amount = 50;
  const amountDate = moment(props.data.date.date).format("LL");
  const { auth } = authStore();
  const user_id = authToUserId(auth);
  console.log("props : ", props.data.id);
  return (
    <View style={styles.invoiceCardBody}>
      <View style={styles.cardHeader}>
        <Text style={styles.invoiceNumber}>{props.data.reference}</Text>
        <View style={styles.invoiceAmount}>
          <Text style={styles.amountTxt}>{props.data.amount}€</Text>
          <Text
            style={{
              fontFamily: fontFamily.semiBold,
              fontSize: 14,
              marginLeft: 2,
            }}
          >
            TTC
          </Text>
        </View>
      </View>
      <View style={styles.hr} />
      <View style={styles.amountDatePosition}>
        <Text style={styles.amountDateTxt}>{amountDate}</Text>
        <View style={styles.invoiceType}>
          <Text style={styles.invoiceTypeTxt}>{props.data.type}</Text>
        </View>
      </View>
      <View style={styles.amountDatePosition}>
        <Text style={styles.orderIdPosition}>
          Commande n° {props.data.order_id}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.DlInvoice}
        onPress={() => {
          download_customer_bill(user_id, props.data.id)
            .then((response) => {
              const dowloadBill = response;
              Linking.openURL(dowloadBill);
            })
            .catch(() => {
              Toast.show({
                type: "error",
                text1: "Erreur",
                text2: I18n.t("profile.notification.error.message5"),
              });
            });
        }}
      >
        <Text style={styles.dlInvoiceTxt}>Télécharger la facture</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  invoiceCardBody: {
    borderColor: "#DADADA",
    borderWidth: 1,
    marginTop: 23,
    backgroundColor: "#fff",
    borderRadius: 8,
    marginBottom: 11,
    width: "88%",
  },
  invoiceNumber: {
    marginLeft: "5%",
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
  cardHeader: {
    justifyContent: "space-between",
    width: "100%",
    flexDirection: "row",
    marginTop: 12,
    marginBottom: -10,
  },
  invoiceStatus: {
    position: "absolute",
    right: "10%",
    backgroundColor: "#40ADA2",
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 30,
    marginTop: -5,
  },
  invoiceType: {
    position: "absolute",
    right: "5%",
    backgroundColor: "#E7E7E7",
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 30,
    marginTop: -5,
  },
  invoiceAmount: {
    position: "absolute",
    right: "2%",
    paddingHorizontal: 20,
    paddingVertical: 5,
    borderRadius: 30,
    marginTop: -5,
    flexDirection: "row",
    alignItems: "center",
  },
  invoiceStatusTxt: {
    color: "white",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
  },
  invoiceTypeTxt: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
  },
  hr: {
    borderBottomColor: "#E7E7E7",
    borderBottomWidth: StyleSheet.hairlineWidth,
    marginVertical: "5%",
  },
  datePosition: {
    marginLeft: "5%",
  },
  dateTxt: {
    fontFamily: fontFamily.Medium,
  },
  amountPosition: {
    marginTop: "5%",
    marginLeft: "5%",
  },
  amountTxt: {
    fontFamily: fontFamily.Bold,
    fontSize: 16,
  },
  amountDatePosition: {
    marginTop: 9,
    marginLeft: "5%",
  },
  amountDateTxt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    color: "rgba(29, 42, 64, 1)",
  },
  DlInvoice: {
    marginTop: 18,
    marginLeft: "5%",
    backgroundColor: "#FF5C85",
    width: "60%",
    alignItems: "center",
    borderRadius: 30,
    marginBottom: "10%",
  },
  dlInvoiceTxt: {
    fontFamily: fontFamily.semiBold,
    color: "white",
    marginVertical: "5%",
  },
  orderIdPosition: {
    // marginTop: 10,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    fontFamily: fontFamily.Medium,
  },
});
