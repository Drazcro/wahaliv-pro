import React from "react";
import { Platform, View } from "react-native";
import RNPickerSelect from "react-native-picker-select";
import CaretDown from "assets/image_svg/CaretDown.svg";

export const PartnerOrdersSelectSearchFilter = (props: {
  periode: string;
  setPeriode: (arg0: string) => void;
}) => {
  return (
    <RNPickerSelect
      placeholder={{ label: "Personalisée" }}
      onValueChange={(value) => props.setPeriode(value)}
      value={props.periode}
      Icon={() => {
        if (Platform.OS === "ios") {
          return (
            <View style={{ right: 15 }}>
              <CaretDown />
            </View>
          );
        } else {
          return null;
        }
      }}
      pickerProps={{
        accessibilityLabel: "test",
      }}
      style={{
        viewContainer: {
          borderWidth: 0.5,
          borderColor: "rgba(196, 196, 196, 1)",
          minWidth: 262,
          height: 40,
          borderRadius: 17,
          justifyContent: "center",
          paddingLeft: 3,
        },
        inputIOS: {
          marginLeft: 23,
        },
        inputAndroid: {
          marginLeft: 13,
        },
      }}
      items={Items}
    />
  );
};

const Items = [
  {
    label: "Personalisée",
    value: "1",
  },
  {
    label: "Aujourd’hui",
    value: "2",
  },
  {
    label: "Hier",
    value: "3",
  },
  {
    label: "La semaine dernier",
    value: "4",
  },
  {
    label: "Les 7 dernieres jours",
    value: "5",
  },
  {
    label: "Les 30 dernieres jours",
    value: "6",
  },
];
