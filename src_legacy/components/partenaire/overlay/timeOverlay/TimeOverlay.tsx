import React, { useEffect, useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { styles } from "./TimeOverlay.style";
import X from "assets/image_svg/X.svg";
import DarkClock from "assets/image_svg/ClockDark.svg";
import Bag from "assets/image_svg/Capa.svg";
import Bike from "assets/image_svg/livraison.svg";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import {
  get_hours_collected,
  get_hours_collected_v2,
  update_hours_collected,
  update_hours_collected_v2,
} from "src_legacy/services/Loaders";
import {
  ICollectHour,
  ICollectPlanning,
  IHour,
  IPlanningRecoverHour,
} from "src/interfaces/Entities";
import moment from "moment";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import { ActivityIndicator } from "react-native-paper";
import { loadingStore } from "zustore/loadingstore";
import { EventRegister } from "react-native-event-listeners";
import { STATUS } from "src/constants/status";

moment.locale("fr");

interface TimeOverlayProps {
  id: number;
  visible: boolean;
  style: object;
  onClose: () => void;
  toggleOverlay: () => void;
  orderType: any;
}

export function TimeOverlay(props: TimeOverlayProps) {
  const { getNewOrderById } = partnerStore();
  const { loadingNewOrder, setLoadingNewOrder } = loadingStore();

  const [hour, setHour] = useState<ICollectHour>();
  const [recoverHour, setRecoverHour] = useState<IHour>();
  const [btnLoading, setBtnLoading] = useState(false);
  const [selected, setSelected] = useState(-1);
  const [selectedHour, setSelectedHour] = useState<
    ICollectPlanning | undefined
  >();
  const [recoverSelectetHour, setRecoverSelectethour] = useState<
    IPlanningRecoverHour | undefined
  >();
  const [isLoading, setIsLoading] = useState(true);

  const current_collect_date = getNewOrderById(props.id)?.commande?.pickup_date
    ?.date;
  const current_collect_date_moment = moment(current_collect_date);
  const current_collect_hour = parseInt(
    current_collect_date_moment.format("HH:mm").replace(":", ""),
  );

  function onTimeCardPress(index: number) {
    const selected_hour = hour ? hour.planning[index] : undefined;
    selected_hour && setSelectedHour(selected_hour);
    setSelected(index);
    console.log("newHour: ", selected_hour);
  }
  function recoverOnTimeCardPress(index: number) {
    const recoverSelectet_hour = recoverHour
      ? recoverHour.planning[index]
      : undefined;
    recoverSelectet_hour && setRecoverSelectethour(recoverSelectet_hour);
    setSelected(index);
  }

  function updateCollectHour() {
    if (selectedHour !== undefined && props.orderType === "Livraison") {
      setBtnLoading(true);
      update_hours_collected_v2(
        props.id,
        selectedHour.collect.date, // hour
        moment(selectedHour.begin.date).format("YYYY-MM-DD HH:mm:ss"),
        moment(selectedHour.end.date).format("YYYY-MM-DD HH:mm:ss"),
        moment(selectedHour.collect.date).format("YYYY-MM-DD HH:mm:ss"),
        moment(selectedHour.delivery.date).format("YYYY-MM-DD HH:mm:ss"),
        selectedHour.collect_distance,
        selectedHour.delivery_distance,
        selectedHour.deliveryman,
      ).finally(() => {
        setLoadingNewOrder(loadingNewOrder + 1);
        setBtnLoading(false);
        props.onClose();
        props.toggleOverlay();
      });
    } else {
      setBtnLoading(true);
      update_hours_collected(
        props.id,
        recoverSelectetHour?.collecte,
        moment(recoverHour?.day.date).format("YYYY-MM-DD"),
        // moment(selectedHour.begin.date).format("YYYY-MM-DD HH:mm:ss"),
        // moment(selectedHour.end.date).format("YYYY-MM-DD HH:mm:ss"),
        // moment(selectedHour.collect.date).format("YYYY-MM-DD HH:mm:ss"),
        // moment(selectedHour.delivery.date).format("YYYY-MM-DD HH:mm:ss"),
        // selectedHour.collect_distance,
        // selectedHour.delivery_distance,
        // selectedHour.deliveryman,
      ).finally(() => {
        console.log("RenewHour : ", recoverSelectetHour?.collecte);
        setLoadingNewOrder(loadingNewOrder + 1);
        setBtnLoading(false);
        EventRegister.emit("COMMAND_SHOULD_RELOAD");
        props.onClose();
        props.toggleOverlay();
      });
    }
  }

  useEffect(() => {
    console.log(props.orderType);
    if (props.orderType === STATUS.PARTNER_SERVICE_TYPE_DELIVERY) {
      setIsLoading(true);
      get_hours_collected_v2(props.id)
        .then((hour) => {
          setHour(hour);
          hour &&
            hour.planning.map((p, index) => {
              const _collect_date = moment(p.begin.date);
              const _collect_hour = parseInt(
                _collect_date.format("HH:mm").replace(":", ""),
              );
              if (current_collect_hour === _collect_hour) {
                setSelected(index);
              }
            });
        })
        .finally(() => setIsLoading(false));
    } else {
      setIsLoading(true);
      get_hours_collected(props.id)
        .then((recoverHour) => {
          setRecoverHour(recoverHour);
          recoverHour &&
            recoverHour.planning.map((p, index) => {
              const _collect_date = moment(p.begin_interval_delivery);
              const _collect_hour = parseInt(
                _collect_date.format("HH:mm").replace(":", ""),
              );
              if (current_collect_hour === _collect_hour) {
                setSelected(index);
              }
            });
        })
        .finally(() => setIsLoading(false));
    }
  }, []);

  function RecoverInformation() {
    return (
      <View>
        {recoverHour && recoverHour.planning.length > 0 && (
          <View>
            <View style={styles.subTitleBox}>
              <Text style={styles.subTitle}>
                <Text style={styles.subTitleSemiBold}>Attention : </Text>
                <Text>
                  un changement d'heure trop important peut entrainer
                  l'annulation de la commande par le client
                </Text>
              </Text>
            </View>
            <Text style={styles.TimeHeaderText}>Date actuelle</Text>
            <View style={styles.TimeDetailsBox}>
              <View style={styles.TimeDetails}>
                <Bag />
                <Text style={styles.TimeText}>
                  {current_collect_date_moment.calendar(null, {
                    sameDay: "[Aujourd'hui ]",
                    nextDay: "[Demain ]",
                    nextWeek: "dddd ",
                    lastDay: "[Hier] ",
                  })}
                  {current_collect_date_moment.format("DD MMMM YYYY")}
                </Text>
              </View>
            </View>
            <Text style={styles.subTitle}>
              Sélectionez le nouveau horaire de collecte
            </Text>
            <View>
              {recoverHour &&
                recoverHour.planning.map(
                  (newRecoverHour: IPlanningRecoverHour, index) => (
                    <View key={index}>
                      <RecoverTimeCard
                        courseId={props.id}
                        newRecoverHour={newRecoverHour}
                        onTimeCardPress={() => recoverOnTimeCardPress(index)}
                        isActive={selected === index}
                      />
                    </View>
                  ),
                )}
            </View>
          </View>
        )}
      </View>
    );
  }

  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={{ width: "95%", ...props.style }}
      onBackdropPress={props.onClose}
    >
      <ScrollView showsVerticalScrollIndicator={false}>
        <View>
          <TouchableOpacity
            onPress={() => {
              props.onClose();
            }}
          >
            <View style={styles.xPosition}>
              <X onPress={props.onClose} />
            </View>
          </TouchableOpacity>
          <View style={styles.overlayBody}>
            <View style={styles.header}>
              <DarkClock />
              <Text style={styles.headerText}>Heure de collecte</Text>
            </View>
            {isLoading ? (
              <View
                style={{
                  flex: 1,
                  height: 200,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <ActivityIndicator />
              </View>
            ) : (
              <>
                {hour && hour?.planning?.length == 0 ? (
                  <View>
                    <View>
                      <Text style={styles.subTitleNoDeliveryman}>
                        <Text>Nous sommes désolés, tous nos livreurs </Text>
                        <Text>sont déjà pris, nous ne pouvons pas </Text>
                        <Text>décaler l’horaire de cette livraison.</Text>
                      </Text>
                    </View>
                    <View style={styles.buttonPos}>
                      <TouchableOpacity onPress={props.onClose}>
                        <View
                          style={{
                            ...styles.button,
                            backgroundColor: "#FF5C85",
                          }}
                        >
                          <Text style={styles.agreeText}>D'accord</Text>
                        </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                ) : null}
                {hour && hour?.planning?.length > 0 ? (
                  <View>
                    <View style={styles.subTitleBox}>
                      <Text style={styles.subTitle}>
                        <Text style={styles.subTitleSemiBold}>
                          Attention :{" "}
                        </Text>
                        <Text>
                          un changement d'heure trop important peut entrainer
                          l'annulation de la commande par le client
                        </Text>
                      </Text>
                    </View>
                    <Text style={styles.TimeHeaderText}>Date actuelle</Text>
                    <View style={styles.TimeDetailsBox}>
                      <View style={styles.TimeDetails}>
                        <Bag />
                        <Text style={styles.TimeText}>
                          {current_collect_date_moment.calendar(null, {
                            sameDay: "[Aujourd'hui ]",
                            nextDay: "[Demain ]",
                            nextWeek: "dddd ",
                            lastDay: "[Hier] ",
                          })}
                          {current_collect_date_moment.format("DD MMMM YYYY")}
                        </Text>
                      </View>
                    </View>
                    <Text style={styles.subTitle}>
                      Sélectionez le nouveau horaire de collecte
                    </Text>
                    <View>
                      {hour &&
                        hour.planning.map(
                          (newHour: ICollectPlanning, index) => (
                            <View key={index}>
                              <TimeCard
                                courseId={props.id}
                                newHour={newHour}
                                onTimeCardPress={() => onTimeCardPress(index)}
                                isActive={selected === index}
                              />
                            </View>
                          ),
                        )}
                    </View>
                  </View>
                ) : null}
                <RecoverInformation />
              </>
            )}
          </View>
        </View>
      </ScrollView>
      {hour && hour.planning.length != 0 ? (
        <TouchableOpacity
          style={styles.buttonPosition}
          disabled={selectedHour === undefined ? true : false}
          onPress={() => updateCollectHour()}
        >
          <View style={styles.button}>
            {btnLoading ? (
              <ActivityIndicator color="#fff" />
            ) : (
              <Text style={styles.agreeText}>Confirmer</Text>
            )}
          </View>
        </TouchableOpacity>
      ) : null}
      {recoverHour && recoverHour.planning.length != 0 ? (
        <TouchableOpacity
          style={styles.buttonPosition}
          disabled={recoverSelectetHour === undefined ? true : false}
          onPress={() => updateCollectHour()}
        >
          <View style={styles.button}>
            {btnLoading ? (
              <ActivityIndicator color="#fff" />
            ) : (
              <Text style={styles.agreeText}>Confirmer</Text>
            )}
          </View>
        </TouchableOpacity>
      ) : null}
    </Overlay>
  );
}

interface ITimeCard {
  courseId: number;
  newHour: ICollectPlanning;
  onTimeCardPress: () => void;
  isActive: boolean;
}

const TimeCard = (props: ITimeCard) => {
  const beginDate = moment(props.newHour.begin.date);
  const beginHour = beginDate.format("HH:mm").replace(":", "h");
  const endDate = moment(props.newHour.end.date);
  const endHour = endDate.format("HH:mm").replace(":", "h");

  const collectDate = moment(props.newHour.collect.date);
  const collectHour = collectDate.format("HH:mm").replace(":", "h");

  const deliveryDate = moment(props.newHour.delivery.date);
  const deliveryHour = deliveryDate.format("HH:mm").replace(":", "h");

  return (
    <TouchableOpacity
      onPress={() => {
        console.log("pressed");
        props.onTimeCardPress();
      }}
    >
      <View
        style={[
          styles.timeCardBody,
          {
            backgroundColor: props.isActive
              ? "rgba(255, 239, 243, 1)"
              : "rgba(196, 196, 196, 0.22)",
          },
          props.isActive && {
            borderWidth: 1,
            borderColor: "#FF5C85",
          },
        ]}
      >
        <View style={styles.cardDisposition}>
          <Bag />
          <View style={styles.textPosition}>
            <Text style={styles.textStyle}>
              <Text>Collecte à </Text>
              <Text style={styles.boldText}>{collectHour}</Text>
            </Text>
          </View>
        </View>
        <View style={styles.space}></View>
        <View style={styles.cardDisposition}>
          <Bike />
          <View style={styles.textPosition}>
            <Text style={styles.textStyle}>
              <Text>Livraison à </Text>
              <Text style={styles.boldText}>
                {beginHour}
                {" - "}
                {endHour} ({deliveryHour})
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

interface IRecoverTimeCard {
  courseId: number;
  newRecoverHour: IPlanningRecoverHour;
  onTimeCardPress: () => void;
  isActive: boolean;
}
const RecoverTimeCard = (props: IRecoverTimeCard) => {
  const beginDate = props.newRecoverHour.begin_interval_delivery;
  // const beginHour = beginDate.format("HH:mm").replace(":", "h");
  const endDate = moment(props.newRecoverHour.last_interval_delivery);
  const endHour = endDate.format("HH:mm").replace(":", "h");

  const collectDate = props.newRecoverHour.collecte;
  // const collectHour = collectDate.format("HH:mm").replace(":", "h");

  const deliveryDate = props.newRecoverHour.theoric_delivery;
  // const deliveryHour = deliveryDate.format("HH:mm").replace(":", "h");

  return (
    <TouchableOpacity
      onPress={() => {
        console.log("pressed");
        props.onTimeCardPress();
      }}
    >
      <View
        style={[
          styles.timeCardBody,
          {
            backgroundColor: props.isActive
              ? "rgba(255, 239, 243, 1)"
              : "rgba(196, 196, 196, 0.22)",
          },
          props.isActive && {
            borderWidth: 1,
            borderColor: "#FF5C85",
          },
        ]}
      >
        <View style={styles.cardDisposition}>
          <Bag />
          <View style={styles.textPosition}>
            <Text style={styles.textStyle}>
              <Text>Collecte à </Text>
              <Text style={styles.boldText}>{collectDate}</Text>
            </Text>
          </View>
        </View>
        <View style={styles.space}></View>
        <View style={styles.cardDisposition}>
          <Bike />
          <View style={styles.textPosition}>
            <Text style={styles.textStyle}>
              <Text>Livraison à </Text>
              <Text style={styles.boldText}>
                {beginDate}
                {" - "}
                {collectDate} ({deliveryDate})
              </Text>
            </Text>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};
