import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { fontFamily } from "src/theme/typography";
import { Overlay } from "react-native-elements";
import X from "assets/image_svg/X.svg";

const AdminitratifOverlay = ({
  open,
  closeOverlay,
}: {
  open: boolean;
  closeOverlay: () => void;
}) => {
  return (
    <Overlay isVisible={open} overlayStyle={styles.InstructionModal}>
      <TouchableOpacity style={styles.closeBtn} onPress={closeOverlay}>
        <X />
      </TouchableOpacity>
      <Text
        style={{
          fontSize: 16,
          fontFamily: fontFamily.semiBold,
          textAlign: "center",
          color: "#1D2A40",
        }}
      >
        Obtention du KBI
      </Text>
      <View
        style={{
          marginTop: 24,
        }}
      >
        <Text
          style={{
            color: "#1D2A40",
            fontSize: 14,
            fontFamily: fontFamily.semiBold,
            lineHeight: 22,
          }}
        >
          J’ai déjà le statut d’auto-entrepreneur
        </Text>
        <View
          style={{
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "flex-end",
          }}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontFamily.Medium,
              lineHeight: 22,
              color: "#1D2A40",
              marginTop: 8,
            }}
          >
            Je le connecte sur sirène.fr pour le récupérer et le télécharger
          </Text>
          <TouchableOpacity>
            <Text
              style={{
                fontFamily: fontFamily.semiBold,
                marginRight: 50,
              }}
            >
              içi
            </Text>
          </TouchableOpacity>
        </View>
        <Text
          style={{
            fontSize: 14,
            fontFamily: fontFamily.semiBold,
            lineHeight: 22,
            color: "#1D2A40",
            marginTop: 29,
          }}
        >
          Je n’ai pas encore le statut et je souhaite l’obtenir
        </Text>
        <View
          style={{
            justifyContent: "flex-start",
            flexDirection: "row",
            alignItems: "flex-end",
          }}
        >
          <Text
            style={{
              fontSize: 14,
              fontFamily: fontFamily.Medium,
              lineHeight: 22,
              color: "#1D2A40",
              marginTop: 8,
            }}
          >
            Télécharger le tuto
          </Text>
          <TouchableOpacity
            style={{
              paddingTop: 0,
              height: 21,
              marginLeft: 2,
            }}
          >
            <Text
              style={{
                fontStyle: "italic",
                fontFamily: fontFamily.semiBold,
                textDecorationLine: "underline",
                fontSize: 14,
              }}
            >
              ici
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  );
};

export default AdminitratifOverlay;

const styles = StyleSheet.create({
  InstructionModal: {
    width: 323,
    height: 314,
    borderRadius: 15,
    paddingVertical: 34,
    paddingLeft: 13,
  },
  closeBtn: {
    position: "absolute",
    top: 14,
    right: 15,
  },
});
