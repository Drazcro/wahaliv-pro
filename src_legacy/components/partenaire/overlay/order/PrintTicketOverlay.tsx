import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import X from "assets/image_svg/X.svg";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import { fontFamily } from "src/theme/typography";

export function PartnerOrderPrintTcketOverlay(props: {
  visible: boolean;
  loading: boolean;
  onClose: () => void;
  onPrint: () => void;
}) {
  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={styles.main}
      onBackdropPress={props.onClose}
    >
      <TouchableOpacity onPress={props.onClose}>
        <View style={styles.closeBtn}>
          <X onPress={props.onClose} />
        </View>
      </TouchableOpacity>

      <View style={styles.header}>
        <Text style={styles.headerText}>Voulez-vous réimprimer le ticket?</Text>
      </View>

      <View style={styles.container}>
        <Text style={styles.text}>Un nouveau ticket s’imprimera</Text>
        <Text style={styles.text}>depuis votre imprimante</Text>
      </View>

      <View style={styles.buttonContainer}>
        <TouchableOpacity style={styles.button} onPress={props.onPrint}>
          {props.loading ? (
            <ActivityIndicator color={"#fff"} />
          ) : (
            <Text style={styles.btnText}>Confirmer</Text>
          )}
        </TouchableOpacity>

        <TouchableOpacity
          style={{ ...styles.button, backgroundColor: "#fff" }}
          onPress={props.onClose}
        >
          <Text style={{ ...styles.btnText, color: "#1D2A40" }}>Annuler</Text>
        </TouchableOpacity>
      </View>
    </Overlay>
  );
}

export const styles = StyleSheet.create({
  main: {
    height: "40%",
    width: "95%",
    backgroundColor: "#fff",
    borderRadius: 8,
  },
  closeBtn: {
    alignItems: "flex-end",
    marginTop: 28.5,
    marginRight: 27.5,
  },
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 19.5,
  },
  headerText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
  },
  text: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
  },
  buttonContainer: {
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    backgroundColor: "#FF5C85",
    borderRadius: 30,
    width: 308,
    height: 52,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 24,
  },
  btnText: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
});
