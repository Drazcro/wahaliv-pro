import { Platform } from "expo-modules-core";
import {
  ActivityIndicator,
  StyleSheet,
  View,
  TouchableOpacity,
} from "react-native";
import { Text, Button } from "react-native-elements";
import { ScrollView } from "react-native-gesture-handler";
import Close from "assets/image_svg_partner/X.svg";
import { fontFamily } from "src/theme/typography";
import I18n from "i18n-js";
import React, { useEffect, useState } from "react";
import {
  get_order_details_for_partner,
  PartnerOrderProducts,
  PartnerOrderRefund,
} from "src_legacy/services/Loaders";
import { IOrderProducts, IProductDetails } from "src/interfaces/Entities";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import { partnerStore } from "src/zustore/partnerstore";
import { parse_to_float_number } from "src/services/helpers";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { RefundsItems } from "../../RefundsItems";
import popup from "src/components/toasts/popup";

export const PartnerOrderRefundsOverlay = (props: {
  visible: boolean;
  onClose: () => void;
  commande_id?: number;
}) => {
  const { orderEnCours, setOrderEnCours } = partnerStore();
  const commande_id = props.commande_id
    ? props.commande_id
    : orderEnCours.commande.id;

  const [products, setProducts] = useState<IOrderProducts>();
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [btnLoading, setBetnLoading] = useState(false);
  const [selectedProducts, setSelectedProducts] = useState<IProductDetails[]>(
    [],
  );
  const [selectedSubProducts, setSelectedSubProducts] = useState<
    IProductDetails[]
  >([]);

  const [totalPrice, setTotalPrice] = useState<number>(0);
  const [totalSubPrice, setTotalSubPrice] = useState<number>(0);

  const onProductSelect = (product: IProductDetails) => {
    const index = selectedProducts.indexOf(product);
    if (index === -1) {
      setSelectedProducts([...selectedProducts, product]);
    } else {
      const newTabSelectedProduct = selectedProducts.filter(
        (value, num) => value.id !== product.id && num !== index,
      );
      setSelectedProducts(newTabSelectedProduct);
    }
  };
  const onSupplementSelect = (product: IProductDetails) => {
    const index = selectedSubProducts.indexOf(product);
    if (index === -1) {
      setSelectedSubProducts([...selectedSubProducts, product]);
    } else {
      const newTabSelectedProduct = selectedSubProducts.filter(
        (value, num) => value.id !== product.id && num !== index,
      );
      setSelectedSubProducts(newTabSelectedProduct);
    }
  };

  useEffect(() => {
    setTotalPrice(priceTotal(selectedProducts));
    setTotalSubPrice(priceTotal(selectedSubProducts));
  }, [selectedProducts, selectedSubProducts]);

  const isTablet = useDeviceType();

  const priceTotal = (selectedProducts: Array<IProductDetails>) => {
    const newPrices = selectedProducts
      .map((p: IProductDetails) => Number(p.price.replace(",", ".")))
      .filter((value, index, self) => self.indexOf(value) === index);
    return newPrices.reduce((accumulator: number, a: number) => {
      return accumulator + a;
    }, 0);
  };

  const onValidate = async () => {
    setBetnLoading(true);
    try {
      const response = await PartnerOrderRefund(commande_id, {
        products: selectedProducts.map((item) => item.id),
        supplements: selectedSubProducts.map((item) => item.id),
      });
      popup.success({
        
        text: I18n.t("profile.notification.success.message"),
      });
      const new_order_data = await get_order_details_for_partner(commande_id);
      setOrderEnCours(new_order_data);
      props.onClose();
    } catch (e) {
      popup.error({
        text: I18n.t("profile.notification.error.message4"),
      });
    } finally {
      setBetnLoading(false);
    }
  };

  useEffect(() => {
    PartnerOrderProducts(commande_id)
      .then((items: IOrderProducts) => {
        if (items !== undefined) {
          //const new_data = items.product_details.map()
          setProducts(items);
        } else {
          setError(true);
        }
      })
      .catch(() => setError(true))
      .finally(() => {
        setIsLoading(false);
        setError(false);
      });
  }, []);

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={
        isTablet && Device.brand != "SUNMI" ? styles.mainTablet : styles.main
      }
      onBackdropPress={props.onClose}
    >
      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? styles.tablet_closeBtn
            : styles.closeBtn
        }
      >
        <TouchableOpacity
          style={{ zIndex: 2, padding: 10 }}
          onPress={props.onClose}
        >
          <Close style={{ zIndex: -1 }} />
        </TouchableOpacity>
      </View>
      <View style={styles.header}>
        <Text style={styles.title}>
          {" "}
          {I18n.t("partner.order.refunds.header.title")}
        </Text>
        <Text
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_subTitle
              : styles.subTitle
          }
        >
          {" "}
          {I18n.t("partner.order.refunds.header.subtitle1")}{" "}
        </Text>
        <Text
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_subTitle
              : styles.subTitle
          }
        >
          {" "}
          {I18n.t("partner.order.refunds.header.subtitle2")}{" "}
        </Text>
      </View>
      <View
        style={{
          height: "55%",
          width: "105%",
          justifyContent: "space-between",
        }}
      >
        <ScrollView
          style={styles.refundsMain}
          showsVerticalScrollIndicator={false}
        >
          {products &&
            products.product_details.map(
              (item: IProductDetails) =>
                !item.isRefund &&
                parseFloat(item.price) !== 0 && (
                  <React.Fragment key={item.name + Math.random().toString()}>
                    <RefundsItems
                      product={item}
                      selectedProducts={selectedProducts}
                      onSelectedProducts={onProductSelect}
                    />
                    {item.sub_products?.map(
                      (i: IProductDetails) =>
                        !i.isRefund &&
                        parseFloat(i.price) !== 0 && (
                          <React.Fragment
                            key={i.name + Math.random().toString()}
                          >
                            <RefundsItems
                              product={i}
                              selectedProducts={selectedSubProducts}
                              onSelectedProducts={onSupplementSelect}
                            />
                          </React.Fragment>
                        ),
                    )}
                  </React.Fragment>
                ),
            )}
        </ScrollView>
        {totalPrice + totalSubPrice > 0 && (
          <View style={styles.refundsTotalItems}>
            <Text style={styles.refunsTotal}>
              {I18n.t("partner.order.refunds.items.total")}
            </Text>
            <Text style={styles.refunsTotal}>
              {parse_to_float_number(totalPrice + totalSubPrice)}€
            </Text>
          </View>
        )}
      </View>
      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? styles.tablet_btnContent
            : styles.btnContent
        }
      >
        {btnLoading ? (
          <View style={styles.button}>
            <ActivityIndicator color="#fff" />
          </View>
        ) : (
          <Button
            onPress={onValidate}
            disabled={selectedProducts.length === 0}
            title={I18n.t("partner.order.refunds.btn.text")}
            buttonStyle={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_button
                : styles.button
            }
            titleStyle={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_text_button
                : styles.text_button
            }
            disabledStyle={{
              backgroundColor: "#C4C4C4",
            }}
            disabledTitleStyle={{
              color: "#FDFDFD",
            }}
          />
        )}
      </View>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  main: {
    height: "100%",
    width: "100%",
    backgroundColor: "#fff",
    position: "relative",
    borderRadius: 0,
    alignItems: "center",
  },
  closeBtn: {
    position: "absolute",
    top: 35,
    right: 27,
  },
  header: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    marginTop: Platform.OS === "ios" ? 58 : 58,
  },
  title: {
    marginBottom: 21,
    color: "#112842",
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    lineHeight: 22,
  },
  subTitle: {
    marginHorizontal: 33,
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 24,
  },
  btnContent: {
    position: "absolute",
    bottom: 85,
    alignSelf: "center",
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 308,
    height: 52,
    backgroundColor: "#40ADA2",
    borderRadius: 30,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  refundsMain: {
    marginTop: 39,
  },
  refundsTotalItems: {
    marginTop: 20,
    flexDirection: "row",
    marginHorizontal: 45,
    justifyContent: "space-between",
    alignContent: "center",
    alignItems: "center",
  },
  refunsTotal: {
    fontFamily: fontFamily.semiBold,
    color: "#000000",
    fontSize: 16,
    lineHeight: 22,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  /** tablet screen */
  mainTablet: {
    height: 656,
    width: 518,
    backgroundColor: "white",
    borderRadius: 10,
    alignItems: "center",
  },
  tablet_closeBtn: {
    position: "absolute",
    top: 26.25,
    right: 28.25,
  },
  tablet_subTitle: {
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 20,
  },
  tablet_button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 308,
    height: 52,
    backgroundColor: "#FF5C85",
    borderRadius: 30,
  },
  tablet_text_button: {
    fontFamily: fontFamily.semiBold,
    color: "#FDFDFD",
    fontSize: 18,
    lineHeight: 22,
  },
  tablet_btnContent: {
    position: "absolute",
    bottom: 80,
    alignSelf: "center",
  },
});
