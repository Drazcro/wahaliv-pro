import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";
import { Text } from "react-native-elements";
import { Overlay } from "react-native-elements/dist/overlay/Overlay";
import CaretRight from "assets/image_svg/CaretRight.svg";
import { fontFamily } from "src/theme/typography";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import { ORDER_STATUS } from "src/interfaces/Entities";
import * as Device from "expo-device";
import useDeviceType from "src/hooks/useDeviceType";
import { printFunction } from "../../printer/printer";

export const PartnerOrderDetailsSettingOverlay = (props: {
  visible: boolean;
  canPrint: boolean;
  onClose: () => void;
  onOpenRefunds: () => void;
  onOpenTrade: () => void;
  onOpenprintTicket: () => void;
  onOpencancelOrder: () => void;
}) => {
  const { orderEnCours } = partnerStore();

  const IS_ACCEPTED = orderEnCours.commande.status === ORDER_STATUS.ACCEPTED;
  const IS_DELIVERED = orderEnCours.commande.status === ORDER_STATUS.DELIVERED;
  const CAN_REFUND = IS_ACCEPTED || IS_DELIVERED;
  const CAN_PRINT = IS_DELIVERED && props.canPrint;

  const isTablet = useDeviceType();

  return (
    <Overlay
      isVisible={props.visible}
      overlayStyle={{
        ...styles.main,
        height: IS_ACCEPTED ? 260 : 200,
        ...(isTablet && Device.brand != "SUNMI" ? styles.mainTablet : {}),
      }}
      onBackdropPress={props.onClose}
    >
      {CAN_REFUND && (
        <TouchableOpacity
          style={styles.textContainer}
          onPress={props.onOpenRefunds}
          disabled={!CAN_REFUND}
        >
          <Text style={styles.text}>Remboursement</Text>
          <CaretRight />
        </TouchableOpacity>
      )}

      {CAN_REFUND && <View style={styles.horizontalLine} />}

      {IS_DELIVERED && (
        <TouchableOpacity
          style={styles.textContainer}
          onPress={props.onOpenTrade}
          disabled={!IS_DELIVERED}
        >
          <Text style={styles.text}>Geste commercial</Text>
          <CaretRight />
        </TouchableOpacity>
      )}

      {IS_DELIVERED && <View style={styles.horizontalLine} />}

      {Device.brand === "SUNMI" && (
        <TouchableOpacity
          style={styles.textContainer}
          onPress={() => printFunction(orderEnCours)}
        >
          <Text style={styles.text}>Réimprimer le ticket</Text>
          <CaretRight />
        </TouchableOpacity>
      )}
      {Device.brand === "SUNMI" && <View style={styles.horizontalLine} />}
      {CAN_PRINT && <View style={styles.horizontalLine} />}

      {IS_ACCEPTED && (
        <>
          <TouchableOpacity
            style={styles.textContainer}
            onPress={props.onOpencancelOrder}
          >
            <Text style={styles.text}>Annuler la commande</Text>
          </TouchableOpacity>
        </>
      )}
    </Overlay>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fff",
    position: "absolute",
    bottom: 0,
    width: "100%",
    borderRadius: 8,
  },
  mainTablet: {
    backgroundColor: "white",
    position: "absolute",
    bottom: 0,
    width: "100%",
    borderRadius: 8,
  },
  horizontalLine: {
    height: 1,
    backgroundColor: "rgba(218, 218, 218, 0.46)",
    marginVertical: 5,
  },
  textContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginHorizontal: 20,
    marginVertical: 12,
  },
  text: {
    fontSize: 14,
    lineHeight: 17.07,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
});
