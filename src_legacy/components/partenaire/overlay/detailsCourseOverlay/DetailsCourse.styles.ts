import { PixelRatio, StyleSheet } from "react-native";
import { color } from "theme";
import { fontFamily } from "src/theme/typography";

export const styles = StyleSheet.create({
  overlayBody: {
    marginTop: 20,
    alignItems: "center",
  },
  xPosition: {
    alignItems: "flex-end",
    marginTop: 20,
    marginRight: 20,
  },
});
