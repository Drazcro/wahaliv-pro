import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { styles } from "./PartnerOverlay.style";
import WhiteScoot from "assets/image_svg/ScooterWhite.svg";
import {
  ICommande,
  IPartnerData,
  IProductDetails,
  ORDER_STATUS,
} from "src/interfaces/Entities";
import User from "assets/image_svg/User.svg";
import DarkClock from "assets/image_svg/ClockDark.svg";
import Scooter from "assets/image_svg/ScooterBlack.svg";
import Sac from "assets/image_svg_partner/Emporter.svg";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import { isToday, parse_to_float_number } from "src_legacy/services/Utils";
import { moment } from "moment";
import { PartnerOrderRefundsOverlay } from "./order/RefundsOverlay";
import { STATUS } from "src/constants/status";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { fontFamily } from "src/theme/typography";

export default function HeaderLogoColor(props: { course: ICommande }) {
  const isTablet = useDeviceType();

  const isDelivery =
    STATUS.PARTNER_SERVICE_TYPE_DELIVERY === props.course.service_type;
  return (
    <>
      {props.course.status === 5 && (
        <View
          style={[
            isTablet && Device.brand != "SUNMI"
              ? styles.circleTablet
              : styles.circle,
            { backgroundColor: "rgba(236, 208, 108, 1)" },
          ]}
        >
          {!isDelivery ? <Sac /> : <WhiteScoot />}
        </View>
      )}
      {props.course.status === 4 && (
        <View
          style={[
            isTablet && Device.brand != "SUNMI"
              ? styles.circleTablet
              : styles.circle,
            { backgroundColor: "rgba(64, 173, 162, 1)" },
          ]}
        >
          {!isDelivery ? <Sac /> : <WhiteScoot />}
        </View>
      )}
      {props.course.status === 1 && (
        <View
          style={[
            isTablet && Device.brand != "SUNMI"
              ? styles.circleTablet
              : styles.circle,
            { backgroundColor: "rgba(64, 173, 162, 1)" },
          ]}
        >
          {!isDelivery ? <Sac /> : <WhiteScoot />}
        </View>
      )}
      {props.course.status === 2 && (
        <View
          style={[
            isTablet && Device.brand != "SUNMI"
              ? styles.circleTablet
              : styles.circle,
            { backgroundColor: "rgba(196, 196, 196, 1)" },
          ]}
        >
          {!isDelivery ? <Sac /> : <WhiteScoot />}
        </View>
      )}
    </>
  );
}

export function NewCourseHeader(props: { course: ICommande }) {
  const isTablet = useDeviceType();
  return (
    <>
      {props.course.status === ORDER_STATUS.NEW ? (
        <View
          style={{
            marginTop: isTablet && Device.brand != "SUNMI" ? 5 : undefined,
          }}
        >
          <Text style={styles.title}>Nouvelle commande</Text>
          <Text>
            <Text
              style={{
                fontFamily: fontFamily.Medium,
                fontSize: 14,
                color: "rgba(29, 42, 64, 1)",
              }}
            >
              En {props.course?.service_type}
            </Text>
            <Text style={styles.semiBoldText}> Nº {props.course?.id}</Text>
          </Text>
        </View>
      ) : (
        <View>
          <Text>
            <Text style={styles.title}>commande N°</Text>
            <Text style={styles.semiBoldText}> {props.course?.id}</Text>
          </Text>
          <Text>À {props.course?.service_type}</Text>
        </View>
      )}
    </>
  );
}

export function ClientInformation(props: { name: string }) {
  return (
    <View style={styles.clientInformations}>
      <Text style={styles.subTitles}>CLIENT</Text>
      <View style={styles.client}>
        <User />
        <Text style={styles.clientTextStyle}>{props.name}</Text>
      </View>
    </View>
  );
}

export function CollecteInformation(props: {
  newDate: moment.Moment;
  course: IPartnerData;
  openOverlay: () => void;
}) {
  const { setOrderInModal } = partnerStore();
  const getNewDate = React.useMemo(() => {
    try {
      return props.newDate
        .toLocaleString()
        .split(" ")[4]
        .slice(0, 5)
        .replace(":", "h");
    } catch (e) {
      return "";
    }
  }, [props.newDate]);
  return (
    <>
      <View style={styles.informations}>
        <Text style={styles.subTitles}>COLLECTE</Text>

        <View style={styles.client}>
          <DarkClock />
          <Text style={styles.deliverymanTextStyle}>
            {isToday(props.newDate.toDate(), new Date())
              ? "Aujourd'hui à "
              : props.newDate.calendar().split(" ")[0] + " à "}

            <Text style={styles.semiBoldText}>{getNewDate}</Text>
          </Text>
          <View style={styles.modificationBox}>
            <TouchableOpacity
              onPress={() => {
                setOrderInModal(props.course);
                props.openOverlay();
              }}
            >
              <Text style={styles.modification}>Modifier</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </>
  );
}

export function DeliveryManInformation(props: { name: string }) {
  return (
    <View style={styles.informations}>
      <Text style={styles.subTitles}>LIVREUR</Text>
      <View style={styles.client}>
        <Scooter />
        <Text style={styles.MediumText}>{props.name}</Text>
      </View>
    </View>
  );
}

export function OrderDetails(props: { course: IPartnerData }) {
  const [openRefunds, setOpenRefunds] = React.useState(false);
  //console.log("productdetails: ", props.course);
  const toCrossed = (value: number) => value === 0;

  return (
    <>
      <View style={styles.informations}>
        <View style={styles.client}>
          <Text style={styles.subTitles}>DÉTAIL DE LA COMMANDE</Text>
          {/* <View style={styles.modificationBox}>
            <TouchableOpacity
              onPress={async () => {
                setOpenRefunds(true);
              }}
            >
              <Text style={styles.modification}>Modifier</Text>
            </TouchableOpacity>
          </View> */}
        </View>
        {/* <View style={styles.courseDetails}>
          {props.course?.product_details.map(
            (item: IProductDetails, index: number) => (
              <View style={styles.nameAndAmount} key={index}>
                <Text style={styles.MediumTextAndSpace}>
                  {item.qte} {item?.name}{" "}
                </Text>
                <Text style={styles.amount}>
                  {parse_to_float_number(item.price)} €
                </Text>
              </View>
            ),
          )}
          <View style={styles.nameAndAmount}>
            <Text style={styles.MediumTextAndSpace}>Livraison</Text>
            <Text style={styles.amount}>{props.course?.delivery_fees} €</Text>
          </View>
        </View> */}

        <View style={styles.clientContainer}>
          <View style={{ flexDirection: "column", width: "100%" }}>
            <>
              {props.course?.product_details.map(
                (product: IProductDetails, index: number) => (
                  <React.Fragment key={index}>
                    <View style={styles.products} key={product.id}>
                      <Text
                        style={{
                          ...styles.productTitle,
                          textDecorationLine:
                            product.isRefund || toCrossed(product.qte)
                              ? "line-through"
                              : "none",
                        }}
                      >
                        {product.qte} {product.name}
                      </Text>
                      <Text
                        style={{
                          ...styles.productPrice,
                          textDecorationLine:
                            product.isRefund || toCrossed(product.qte)
                              ? "line-through"
                              : "none",
                        }}
                      >
                        {parse_to_float_number(product.price)}€
                      </Text>
                    </View>
                    {product.sub_products?.map(
                      (subProduct: IProductDetails, index: number) => (
                        <View style={styles.subproducts} key={index}>
                          <Text
                            style={{
                              ...styles.productTitle,
                              textDecorationLine:
                                subProduct.isRefund ||
                                toCrossed(subProduct.quantity)
                                  ? "line-through"
                                  : "none",
                            }}
                          >
                            {subProduct.quantity} {subProduct.name}
                          </Text>
                          <Text
                            style={{
                              ...styles.productPrice,
                              textDecorationLine:
                                subProduct.isRefund ||
                                toCrossed(subProduct.quantity)
                                  ? "line-through"
                                  : "none",
                            }}
                          >
                            {parse_to_float_number(subProduct.price)}€
                          </Text>
                        </View>
                      ),
                    )}
                  </React.Fragment>
                ),
              )}
            </>
          </View>
        </View>
        <View style={styles.nameAndAmount}>
          <Text style={styles.MediumTextAndSpace}>Livraison</Text>
          <Text style={styles.amount}>{props.course?.delivery_fees} €</Text>
        </View>
        {props.course?.promo_code?.amount != null && (
          <View style={styles.nameAndAmount}>
            <View style={styles.promoSpace}>
              <Text style={styles.ChargeBySpace}>Code promo </Text>
              <Text style={styles.ChargeBy}>
                {" "}
                A charge de {props.course?.promo_code?.by}
              </Text>
            </View>
            <Text style={styles.amount}>
              {parse_to_float_number(props.course?.promo_code?.amount)} €
            </Text>
          </View>
        )}
        <View style={styles.nameAndAmount}>
          <Text style={{ ...styles.BoldTextTotal, flex: 10 }}>TOTAL </Text>
          <Text style={styles.BoldAmount}>
            {parse_to_float_number(props.course?.total_amount)} €
          </Text>
        </View>
      </View>
      <PartnerOrderRefundsOverlay
        commande_id={props.course.commande.id}
        visible={openRefunds}
        onClose={() => setOpenRefunds(false)}
      />
    </>
  );
}

export function ClientComment(props: { comment: string }) {
  return (
    <>
      {props.comment && (
        <View style={styles.informations}>
          <Text style={styles.subTitles}>COMMENTAIRE CLIENT</Text>
          <View style={styles.commentPosition}>
            <Text style={styles.comment}>{props.comment}</Text>
          </View>
        </View>
      )}
    </>
  );
}
