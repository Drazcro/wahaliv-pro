import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import { styles } from "./PartnerOverlay.style";
import X from "assets/image_svg/X.svg";
import { IPartnerData } from "src/interfaces/Entities";
import {
  accept_cmd,
  cancel_or_refuse_cmd,
  get_accepted_orders_for_partner,
  get_new_orders_for_partner,
  get_delivered_orders_for_partner,
  get_refused_orders_for_partner,
  reloadOrders,
} from "src_legacy/services/Loaders";
import Toast from "react-native-expo-popup-notifications";
import { ActivityIndicator } from "react-native-paper";
import { authToUserId } from "src_legacy/services/Utils";
import { INTERVAL_PAGE } from "src/constants/default_values";
import { printFunction } from "../printer/printer";
import * as Device from "expo-device";
import useDeviceType from "src/hooks/useDeviceType";
import { fontFamily } from "src/theme/typography";
import { authStore } from "src_legacy/services/v2/auth_store";

export function DisplayAcceptedOrRefusedOverlay({
  onClose,
  title,
  subTitle,
  acceptedOrRefused,
  ButtonText,
  course,
  toggleOverlay,
}: {
  onClose: () => void;
  title: string;
  subTitle: string;
  acceptedOrRefused: string;
  ButtonText: string;
  course: IPartnerData;
  toggleOverlay: () => void;
}) {
  const { auth } = authStore();
  const user_id: number = authToUserId(auth);
  const isTablet = useDeviceType();

  const id = course.commande.id.toString();
  const [laoding, setLoading] = useState(false);

  const acceptOrder = () => {
    setLoading(true);
    accept_cmd(id)
      .then((message) => {
        Toast.show({
          type: "success",
          text1: "Success",
          text2: message,
        });
        if (Device.brand === "SUNMI") {
          printFunction(course);
        }

        onClose();
        toggleOverlay();
        reloadOrders(user_id);
      })
      .catch(() => {
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: "Bad request",
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const declineOrder = () => {
    setLoading(true);
    cancel_or_refuse_cmd(id)
      .then((message) => {
        Toast.show({
          type: "success",
          text1: "Success",
          text2: message,
        });

        onClose();
        toggleOverlay();
        reloadOrders(user_id);
      })
      .catch(() => {
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: "Bad request",
        });
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <View>
      <TouchableOpacity
        onPress={() => {
          onClose();
        }}
      >
        <View style={styles.xPosition}>
          <X onPress={onClose} />
        </View>
      </TouchableOpacity>
      <View style={styles.AcceptedOrRefusedOverlayBox}>
        <Text style={styles.AcceptedOrRefusedOverlayTitle}>{title}</Text>
        <Text
          style={{
            color: "rgba(29, 42, 64, 1)",
            fontFamily: fontFamily.Medium,
          }}
        >
          {subTitle}
        </Text>
        <View style={styles.buttonPositionAcceptedOrRefusedOverlay}>
          {acceptedOrRefused == "Accepted" && (
            <TouchableOpacity
              style={{ ...styles.buttonPosition, marginTop: 0 }}
              onPress={acceptOrder}
            >
              <View style={styles.button}>
                {laoding ? (
                  <ActivityIndicator color="#fff" />
                ) : (
                  <Text
                    style={{
                      ...styles.agreeText,
                      fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                    }}
                  >
                    {ButtonText}
                  </Text>
                )}
              </View>
            </TouchableOpacity>
          )}
          {acceptedOrRefused == "Refused" && (
            <View>
              <TouchableOpacity
                onPress={declineOrder}
                style={{ ...styles.buttonPosition, marginTop: 0 }}
              >
                <View style={styles.refusedButton}>
                  {laoding ? (
                    <ActivityIndicator color="#fff" />
                  ) : (
                    <Text
                      style={{
                        ...styles.agreeText,
                        fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                      }}
                    >
                      {ButtonText}
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={{ marginTop: 30 }} onPress={acceptOrder}>
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                  }}
                >
                  {laoding ? (
                    <ActivityIndicator color="#fff" />
                  ) : (
                    <Text
                      style={{
                        fontFamily: fontFamily.semiBold,
                        color: "rgba(64, 173, 162, 1)",
                        fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                      }}
                    >
                      Accepter la commande
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </View>
    </View>
  );
}
