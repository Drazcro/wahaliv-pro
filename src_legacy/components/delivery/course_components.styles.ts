import { Dimensions, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  wrapperOne: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    paddingVertical: 10,
  },

  textOne: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
  },

  textTwo: {
    fontFamily: fontFamily.Bold,
    fontSize: 18,
    lineHeight: 21.94,
    color: "#1D2A40",
  },

  dateNow: {
    textAlign: "justify",
    fontFamily: fontFamily.Medium,
    color: "#000000",
    fontSize: 13,
    lineHeight: 22,
  },

  divider: {
    borderWidth: 1,
    borderColor: "#C4C4C4",
  },

  pic: {
    flexDirection: "row",
  },

  grid: {
    alignItems: "center",
    lineHeight: 0,
  },

  wrapperRecovered: {
    backgroundColor: "#F4F4F4",
    padding: 15,
    position: "relative",
  },

  subWrapperOne: {
    marginBottom: 35,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    paddingRight: 30,
    left: width * 0.01,
  },

  columnCenter: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },

  title: {
    //textAlign: 'justify',
    fontFamily: fontFamily.Medium,
    color: "#000000",
    fontSize: 14,
    lineHeight: 22,
  },

  tip: {
    backgroundColor: "#E9C852",
    width: 125,
    borderRadius: 5,
    paddingHorizontal: 5,
  },
  tipText: {
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 14.63,
  },

  tipPosition: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 33,
    marginTop: -5,
    marginBottom: 5,
  },
  wrapper: {
    flexDirection: "row",
  },

  subWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5,
    marginLeft: 10,
  },

  distance: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    paddingLeft: 10,
  },

  text1: {
    fontFamily: fontFamily.Bold,
    marginLeft: 20,
    color: "#112842",
    fontSize: 14,
  },

  text2: {
    fontFamily: fontFamily.Bold,
    marginLeft: 10,
    color: "#112842",
    fontSize: 14,
  },
});
