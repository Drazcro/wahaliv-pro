import React, { useState } from "react";
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  Linking,
  Platform,
} from "react-native";
import { Divider } from "react-native-elements/dist/divider/Divider";
import { IBaseUser, point_and_comma } from "src_legacy/services/Utils";
import { styles } from "../../../app/(protected)/(delivery)/courses_en_cours_delivery.styles";
import { COURSE_STATUS } from "src/constants/course";

import Phone from "assets/image_svg/Phone.svg";
import CaretDowns from "assets/image_svg/CaretDowns.svg";
import CaretUp from "assets/image_svg/CaretUp.svg";
import { ICourseDetails } from "src/interfaces/Entities";
import MapPinRed from "assets/image_svg/MapPinRedFull.svg";
import MapPinGreen from "assets/image_svg/MapPinGreenFull.svg";
import { Input } from "@rneui/themed";

import {
  DeliverButton,
  DisabledRetrieveButton,
  RecoverButton,
} from "./slide_button";
import {
  CheckDeliveryComplementAddress,
  CheckHowToDisplayInfoCourse,
} from "./courses_components";
import GPSPartner from "assets//image_svg/GPS_Parteniare.svg";
import GPSClient from "assets//image_svg/GPS_Client.svg";

import { showLocation } from "react-native-map-link";
import { courseStore } from "src_legacy/zustore/coursestore";
import moment from "moment";
import { fontFamily } from "src/theme/typography";
import MapPreview from "src_legacy/components/delivery/maps/map_preview";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";

const openApp = (type: string, number: string) => {
  Linking.openURL(type + number);
};

export const CoursesBody = () => {
  const { courseEnCours, setCourseEnCours } = courseStore();

  const [drop, setDrop] = useState(false);
  const [baseUser] = useState<IBaseUser>();
  const [isLate] = useState(false);
  const [swipeDisable, setSwipeDisable] = useState(true);

  const onChangePrice = (price: string) => {
    setCourseEnCours({ ...courseEnCours, newPrice: price });
    if (price === undefined || price === "") {
      setSwipeDisable(true);
    } else {
      setSwipeDisable(false);
    }
  };

  let PartnerNumber = "";
  let CustomerNumber = "";
  let PartnerAndroidNumber = "";
  let CustomerAndroidNumber = "";

  if (courseEnCours.partner_telephone) {
    const partnerPhoneNumber = courseEnCours.partner_telephone.split("+");
    PartnerNumber = "#31#00" + partnerPhoneNumber[1];
    PartnerAndroidNumber = "%2331%2300" + partnerPhoneNumber[1];
    //PartnerAndroidNumber = "%2331%23"+courseEnCours.partner_telephone;
  } else {
    PartnerNumber = "Inconnu";
  }
  if (courseEnCours.customer_telephone) {
    const customerPhoneNumber = courseEnCours.customer_telephone.split("+");
    CustomerNumber = "#31#00" + customerPhoneNumber[1];
    //PartnerAndroidNumber = "#31#+33" + customerPhoneNumber[1];
    CustomerAndroidNumber = "%2331%2300" + customerPhoneNumber[1];
    //CustomerNumber = courseEnCours.customer_telephone;
  } else {
    CustomerNumber = "Inconnu";
  }
  function onMapButtonClick() {
    const latitude =
      courseEnCours.delivery_status === COURSE_STATUS.RECOVERED
        ? courseEnCours.customer_lat
        : courseEnCours.partner_lat;
    const longitude =
      courseEnCours.delivery_status === COURSE_STATUS.RECOVERED
        ? courseEnCours.customer_lng
        : courseEnCours.partner_lng;

    openMap(latitude, longitude);
  }

  return (
    <View style={styles.main}>
      <View style={styles.header}>
        <Toolbar
          title={"Course n°" + courseEnCours.id_commande}
          state={
            [3, 4, 5].includes(courseEnCours.delivery_status)
              ? "ANNULÉE"
              : "LIVRÉE"
          }
        />
      </View>
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={styles.globalMap}>
          <View style={styles.mapgps}>
            <TouchableOpacity
              onPress={() => {
                onMapButtonClick();
              }}
            >
              {getGPSButton(courseEnCours.delivery_status)}
            </TouchableOpacity>
          </View>
          <MapPreview />
        </View>
        {isLate && (
          <View style={styles.content}>
            <Text style={styles.textOne1}>En retard</Text>
          </View>
        )}
        <CheckHowToDisplayInfoCourse
          course={courseEnCours}
          baseUser={baseUser}
        />
        <Divider style={styles.divider} />
        <View style={styles.section}>
          <View style={styles.wrapperTwo}>
            <View style={styles.lineDash} />
            <View style={styles.wrapperData}>
              <View style={styles.map}>
                <MapPinRed />
              </View>
              <View style={styles.subWrapperOne}>
                <View style={styles.width1}>
                  <Text style={styles.client1}>
                    PARTENAIRE{" "}
                    <Text style={styles.client}>
                      {courseEnCours.partner
                        ? courseEnCours.partner
                        : courseEnCours.partner_name}
                    </Text>
                  </Text>
                  <Text style={styles.title}>
                    {courseEnCours?.pickup_address}
                  </Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: 14,
                      color: "#8D98A0",
                    }}
                  >
                    Commande passée le{" "}
                    {moment(courseEnCours.commande_at.date).format("DD/MM")} -{" "}
                    {moment(courseEnCours.commande_at.date).format("HH:MM")}h
                  </Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.regular,
                      fontSize: 14,
                      color: "#8D98A0",
                    }}
                  >
                    Commande récupérée le{" "}
                    {moment(courseEnCours.pickup_date.date).format("DD/MM")} -{" "}
                    {moment(courseEnCours.pickup_date.date).format("HH:MM")}h
                  </Text>
                </View>
                {courseEnCours.delivery_status != COURSE_STATUS.FINISHED && (
                  <TouchableOpacity
                    onPress={() => {
                      Platform.OS === "ios"
                        ? openApp("tel:", `${PartnerNumber}`)
                        : openApp("tel:", `${PartnerAndroidNumber}`);
                    }}
                  >
                    <Phone />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
          <View style={styles.wrapperThreeSub}>
            <View style={styles.wrapperData}>
              <View style={styles.map}>
                <MapPinGreen />
              </View>
              <View style={styles.subWrapperOne}>
                <View style={styles.width}>
                  <Text style={styles.client1}>
                    CLIENT{" "}
                    <Text style={styles.client}>
                      {courseEnCours.customer
                        ? courseEnCours.customer
                        : courseEnCours.customer_name}
                    </Text>
                  </Text>
                  <Text style={styles.title}>
                    {courseEnCours?.delivery_address?.address}
                  </Text>
                  <CheckDeliveryComplementAddress
                    complementadresse={
                      courseEnCours?.delivery_address?.complementadresse
                    }
                  />
                  {courseEnCours.delivery_status == COURSE_STATUS.FINISHED ? (
                    <Text
                      style={{
                        fontFamily: fontFamily.regular,
                        fontSize: 14,
                        color: "#8D98A0",
                      }}
                    >
                      Commande livrée le{" "}
                      {moment(courseEnCours.real_delivery_date.date).format(
                        "DD/MM",
                      )}{" "}
                      -{" "}
                      {moment(courseEnCours.real_delivery_date.date).format(
                        "HH:MM",
                      )}
                      h
                    </Text>
                  ) : null}
                </View>

                {courseEnCours.delivery_status != COURSE_STATUS.FINISHED && (
                  <TouchableOpacity
                    onPress={() => {
                      Platform.OS === "ios"
                        ? openApp("tel:", `${CustomerNumber}`)
                        : openApp("tel:", `${CustomerAndroidNumber}`);
                    }}
                  >
                    <Phone />
                  </TouchableOpacity>
                )}
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => setDrop(drop === true ? false : true)}
          style={styles.dropDownStyle}
        >
          <Text style={styles.textThree}>DÉTAIL DE LA COMMANDE</Text>
          {drop ? <CaretDowns /> : <CaretUp />}
        </TouchableOpacity>
        <Divider style={styles.dividerOne} />
        {drop && (
          <View style={{ ...styles.dropDown, height: drop ? "auto" : 0 }}>
            {courseEnCours &&
              courseEnCours?.details?.map(
                (item: ICourseDetails, index: number) => (
                  <View style={styles.downContent} key={index}>
                    <View
                      style={{
                        width: "10%",
                        alignItems: "center",
                      }}
                    >
                      <Text style={styles.textOrderFinish}>{item.qte}</Text>
                    </View>
                    <View style={{ width: "70%", alignItems: "flex-start" }}>
                      <Text style={styles.textOrderFinish}>{item?.name}</Text>
                    </View>
                    <View
                      style={{
                        width: "20%",
                        justifyContent: "flex-end",
                      }}
                    >
                      <Text style={styles.textOrderFinish}>
                        {point_and_comma(item.price)} €
                      </Text>
                    </View>
                  </View>
                ),
              )}
          </View>
        )}

        {courseEnCours.delivery_status == COURSE_STATUS.ACCEPTED &&
          courseEnCours.need_real_price && (
            <>
              <Divider style={styles.dividerOne} />
              <View style={styles.realPriceContent}>
                <Text style={styles.textThree}>PRIX AU KILO</Text>
                <Text style={styles.realPriceText}>
                  Veuillez saisir le prix réel au client
                </Text>
                <Text style={styles.realPriceText}>
                  (hors frais de livraison)
                </Text>
              </View>
              <View style={styles.realPriceInputMain}>
                <Input
                  value={courseEnCours.newPrice}
                  onChangeText={(text) => onChangePrice(text)}
                  containerStyle={styles.amountInput}
                  inputStyle={styles.price}
                  inputContainerStyle={styles.input}
                  keyboardType="decimal-pad"
                />
                <Text style={styles.realPriceCurrency}>€</Text>
              </View>
            </>
          )}
      </ScrollView>
      {courseEnCours.delivery_status != COURSE_STATUS.FINISHED && (
        <View style={styles.wrapperFour}>
          {getSliderByCourse(swipeDisable)}
        </View>
      )}
    </View>
  );
};

const openMap = (destLat: number, destLong: number) => {
  return showLocation({
    latitude: destLat,
    longitude: destLong,
    directionsMode: "car",
    alwaysIncludeGoogle: true,
    dialogTitle: "Ouvrir dans Maps",
    dialogMessage: "Quelle application GPS voudriez-vous utiliser ?",
    cancelText: "Annuler",
    appsWhiteList:
      Platform.OS === "android" ? ["google-maps", "waze"] : undefined,
  });
};

function getGPSButton(status: COURSE_STATUS) {
  if ([COURSE_STATUS.ACCEPTED, COURSE_STATUS.TO_BE_ACCEPTED].includes(status)) {
    return <GPSPartner />;
  } else if (status === COURSE_STATUS.RECOVERED) {
    return <GPSClient />;
  }
  return <></>;
}

function getSliderByCourse(swipeDisable?: boolean) {
  const { courseEnCours } = courseStore();
  if (courseEnCours.isRetrievable == false) {
    return <DisabledRetrieveButton isLate={true} />;
  }
  if (courseEnCours.delivery_status == COURSE_STATUS.ACCEPTED) {
    return (
      <RecoverButton
        isLate
        id={courseEnCours.id_delivery}
        swipeDisable={swipeDisable}
      />
    );
  } else if (courseEnCours.delivery_status == COURSE_STATUS.RECOVERED) {
    return <DeliverButton isLate id={courseEnCours.id_delivery} />;
  } else {
    return <></>;
  }
}
