import React from "react";
import {
  retrieved_cmd,
  deliver_course,
  reloadOrders,
} from "src_legacy/services/Loaders";
import { courseStore } from "src_legacy/zustore/coursestore";
import { SliderButton } from "src_legacy/components/slider_button/slider_button";
import { router } from "expo-router";

function DisabledRetrieveButton() {
  return (
    <SliderButton
      title="Commande récupérée"
      subTitle="La commande n'a pas encore été validée par le commerçant."
      railBgColor="#DEDEDE"
      railStylesBgColor="#B9B9B9"
      subTitleColor="#ED7178"
      isDisabled={true}
      onSuccess={() => {
        return;
      }}
    />
  );
}

function RecoverButton({
  isLate,
  id,
  swipeDisable,
}: {
  isLate: boolean;
  id: number;
  swipeDisable?: boolean;
}) {
  const { courseEnCours } = courseStore();
  const hasRealPrice = courseEnCours.need_real_price && swipeDisable;

  function recover() {
    retrieved_cmd(id).then((res) => {
      console.log(res);
      reloadOrders();
    });
  }

  return (
    <SliderButton
      title="Commande récupérée"
      subTitle={
        hasRealPrice
          ? "Veuillez saisir le prix réel facturé au client \n (hors frais de livraison)"
          : "Fais glisser le bouton dès que tu as récupéré la commande"
      }
      railBgColor={hasRealPrice ? "rgba(196, 196, 196, 0.6)" : "#FFECED"}
      railStylesBgColor={hasRealPrice ? "rgba(196, 196, 196, 0.59)" : "#FF5C85"}
      onSuccess={() => recover()}
      subTitleColor={hasRealPrice ? "#FF5C85" : "#000"}
      isDisabled={hasRealPrice}
    />
  );
}

function DeliverButton({ isLate, id }: { isLate: boolean; id: number }) {
  const actionGoBack = () => {
    router.back();
  };

  async function deliver() {
    try {
      await deliver_course(id);
      actionGoBack();
    } catch (e) {
      return Promise.reject();
    }
  }
  return (
    <SliderButton
      title="Commande livrée"
      subTitle="Fais glisser le bouton dès que tu as livré la commande"
      railBgColor="#FFECED"
      railStylesBgColor="#FF5C85"
      onSuccess={() => deliver()}
      subTitleColor="#000"
      isDisabled={false}
    />
  );
}

export { DisabledRetrieveButton, RecoverButton, DeliverButton };
