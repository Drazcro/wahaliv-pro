import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";
import moment from "moment";
import "moment/locale/fr";

import { styles } from "./delivery_card_v2.styles";
import { COURSE_STATUS } from "src/constants/course";
import { dateToDay, dateToTime, IHourDate } from "src_legacy/services/Utils";
import LinkGroup from "assets/image_svg/LinkGroup.svg";
import { ICourse } from "src/interfaces/Entities";

import { courseStore } from "src_legacy/zustore/coursestore";
import CaretRight from "assets/image_svg/CaretRight.svg";

import ScootBlack from "assets/image_svg_partner/ScootBlack.svg";
import EmporterBlack from "assets/image_svg_partner/EmporterBlack.svg";
import { router } from "expo-router";

moment.locale("fr");

interface IDeliveryCardProps {
  course: ICourse;
  onAcceptPrompt: () => void;
  isOnTournee?: boolean;
}

interface IDeliveryCardState {
  label: string;
  color: string;
  datePick: moment.Moment;
  dateLiv: moment.Moment;
  datePickDiff: IHourDate;
  dateLivDiff: IHourDate;
}

export default function DeliveryCardV2(props: IDeliveryCardProps) {
  const [cardState, setCardState] = useState({} as IDeliveryCardState);
  const { course, onAcceptPrompt } = props;
  const { setCourseEnCours } = courseStore();
  // constant
  const is_grouped = course.isGrouped;
  const grouped_command_ids = course.groupedCommandIds;
  const delivery_status = course.delivery_status;

  function SwitchCourseScreen(status: number) {
    if (status != COURSE_STATUS.TO_BE_ACCEPTED) {
      setCourseEnCours(course);
      if (props.isOnTournee) {
        router.push({
          pathname: "/courses_en_cours_delivery",
          params: { course: course },
        });
      } else {
        router.push({
          pathname: "/courses_en_cours_delivery",
          params: { course: course },
        });
      }
    } else if (status == COURSE_STATUS.TO_BE_ACCEPTED) {
      onAcceptPrompt();
    }
  }

  const getLabel = React.useMemo(() => {
    if (
      [
        COURSE_STATUS.CANCELED_BY_DELIVERY,
        COURSE_STATUS.CANCELED_BY_SACRE,
        COURSE_STATUS.CANCELED_BY_PARTNER,
      ].includes(delivery_status)
    ) {
      return "Annulée";
    }
    return "Livrée";
  }, [delivery_status]);

  return (
    <View style={styles.main}>
      <TouchableOpacity
        onPress={() => {
          SwitchCourseScreen(course.delivery_status);
        }}
        style={{
          ...styles.box,
          backgroundColor:
            course.delivery_status === COURSE_STATUS.TO_BE_ACCEPTED
              ? "rgba(254, 251, 242, 0.78)"
              : "white",
        }}
      >
        <View style={styles.header}>
          <View
            style={{
              ...styles.headerLabel,
              backgroundColor: getLabel === "Annulée" ? "#8D98A0" : "#40ADA2",
            }}
          >
            <Text style={styles.state}>{getLabel}</Text>
          </View>

          <View style={styles.wrapper}>
            <Text style={styles.count}>
              {`N° ${props.course?.id_commande}`}
            </Text>
            <CaretRight />
          </View>
        </View>
        <View style={styles.card}>
          <View style={styles.line} />
          {/* <DashedLine axis='vertical' dashLength={10} dashThickness={8} dashGap={5} dashColor='#c32626' dashStyle={{ borderRadius: 5 }} /> */}
          <View>
            <View>
              <View style={styles.datMap}>
                <View style={styles.map}>
                  <EmporterBlack style={{ marginLeft: 3 }} />
                </View>
                <View style={styles.datMapRight}>
                  <Text
                    style={{
                      ...styles.name,
                      textDecorationLine:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "line-through"
                          : "none",
                      color:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "#8D98A0"
                          : "#1D2A40",
                    }}
                  >
                    {delivery_status === COURSE_STATUS.FINISHED
                      ? "Collectée"
                      : "Collecter"}{" "}
                    {dateToDay(cardState.datePick)} à{" "}
                    {dateToTime(cardState.datePick)}
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      textDecorationLine:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "line-through"
                          : "none",
                      color:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "#8D98A0"
                          : "#1D2A40",
                    }}
                  >
                    {props.course?.pickup_address}
                  </Text>
                </View>
              </View>
            </View>
            <View>
              <View style={styles.datMap}>
                <View style={styles.map}>
                  <ScootBlack
                    style={{ marginTop: 10, marginLeft: -5 }}
                    height={15}
                  />
                </View>
                <View style={styles.datMapRight}>
                  <Text
                    style={{
                      ...styles.name,
                      textDecorationLine:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "line-through"
                          : "none",
                      color:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "#8D98A0"
                          : "#1D2A40",
                    }}
                  >
                    {delivery_status === COURSE_STATUS.FINISHED
                      ? "Livrée"
                      : "Livrer"}{" "}
                    {dateToDay(cardState.dateLiv)} à{" "}
                    {dateToTime(cardState.dateLiv)}
                  </Text>
                  <Text
                    style={{
                      ...styles.title,
                      textDecorationLine:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "line-through"
                          : "none",
                      color:
                        delivery_status !== COURSE_STATUS.FINISHED
                          ? "#8D98A0"
                          : "#1D2A40",
                    }}
                  >
                    {props.course?.delivery_address?.address}
                  </Text>
                </View>
              </View>
              {is_grouped && (
                <View style={styles.group}>
                  <LinkGroup width={17} height={17} />
                  <Text style={styles.groupText}>
                    Groupée avec
                    {grouped_command_ids &&
                      grouped_command_ids?.map((item: number, index: number) =>
                        index === 0 ? (
                          <Text key={item}> N° {item} </Text>
                        ) : (
                          <Text key={item}> et N° {item} </Text>
                        ),
                      )}
                  </Text>
                </View>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}
