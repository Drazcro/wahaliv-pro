import React, { useEffect, useState } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Linking,
  ActivityIndicator,
} from "react-native";
import moment from "moment";
import "moment/locale/fr";

import { styles } from "./invoice_card.style";
import Path from "assets//image_svg/Path.svg";
import Euro from "assets/image_svg/Euro.svg";
import Bonus from "assets/image_svg/Rocket.svg";
import Facture from "assets/image_svg/Facture.svg";
import Recap from "assets/image_svg/Recap.svg";
import CaretRight from "assets//image_svg/CaretRight.svg";
import { IBillsDetails, IDayInvoice } from "src/interfaces/Entities";
import { download_bill, download_recap_bill } from "src_legacy/services/Loaders";
import { authToUserId, parse_to_float_number } from "src_legacy/services/Utils";
import Toast from "react-native-expo-popup-notifications";
import I18n from "i18n-js";
import { router } from "expo-router";
import { courseStore } from "src_legacy/zustore/coursestore";
import { authStore } from "src_legacy/services/v2/auth_store";

moment.locale("fr");

export function InvoiceCard({ date, global, courses }: IDayInvoice) {
  const setCourses = courseStore((v) => v.setCourses);

  const charAtOne = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  // useEffect(() => {
  //   setCourses(courses);
  // }, [courses]);

  return (
    <View style={styles.cardBody}>
      <TouchableOpacity
        onPress={() => {
          setCourses(courses);
          router.push({
            pathname: "/invoice_card_by_id_screen",
            params: { date: date.date, courses: courses },
          });
        }}
      >
        <View style={styles.subCardBody}>
          <Text style={styles.date}>
            {charAtOne(moment(date.date).format("dddd D MMMM YYYY"))}
          </Text>
          <Text style={styles.value}>{parse_to_float_number(global.total)} €</Text>
          <CaretRight style={styles.arrowDistance} />
        </View>
        <View style={styles.customerAmount}>
          <Text style={styles.clientAmount}>
            Montant Client : {parse_to_float_number(global.total_customer)} €
          </Text>
          <Text style={styles.MPAmount}>
            Montant MP : {parse_to_float_number(global.total_mp)} €
          </Text>
        </View>
        <View style={styles.total}>
          <Text style={styles.TotalAmount}>
            Total : {parse_to_float_number(global.total)} €
          </Text>
          <View style={styles.rightAlign}>
            <View style={styles.Distance}>
              <Path />
              <Text style={styles.TotalDistance}>
                {parse_to_float_number(global.distance)} km
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.PBandBonus}>
          <View style={styles.pourboires}>
            <Euro />
            <Text style={styles.pBB}>
              Pourboire {parse_to_float_number(global.tip)} €
            </Text>
          </View>
          <View style={styles.rightAlign}>
            <View style={styles.Bonus}>
              <Bonus />
              <Text style={styles.pBB}>
                Bonus {parse_to_float_number(global.bonus)} €
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export function InvoiceFinishedCard({ bill }: { bill: IBillsDetails }) {
  const { auth } = authStore();
  const user_id: number = authToUserId(auth);

  const charAtOne = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const EditionDate = charAtOne(
    moment(bill.edition_date.date).format("dddd D MMMM YYYY"),
  );

  const paiementDate =
    bill.paiement_date !== null
      ? charAtOne(moment(bill.paiement_date.date).format("dddd D MMMM YYYY"))
      : undefined;

  const beginDate = bill.begin_date.date.split(" ")[0].split("-");
  const endDate = bill.end_date.date.split(" ")[0].split("-");
  const endMonth = charAtOne(
    moment(bill.end_date.date).format("dddd D MMMM YYYY").split(" ")[2],
  );
  const beginMonth = charAtOne(
    moment(bill.begin_date.date).format("dddd D MMMM YYYY").split(" ")[2],
  );

  return (
    <View style={styles.cardBody}>
      <View style={styles.subCardBody}>
        {beginMonth == endMonth && (
          <Text style={styles.date}>
            Du {beginDate[2]} au {endDate[2]} {endMonth} {endDate[0]}
          </Text>
        )}
        {beginMonth != endMonth && (
          <Text style={styles.date}>
            Du {beginDate[2]} {beginMonth} au {endDate[2]} {endMonth}{" "}
            {endDate[0]}
          </Text>
        )}
      </View>
      <View style={styles.customerAmount}>
        <Text style={styles.clientAmount}>Date d'édition : {EditionDate}</Text>
        {paiementDate && (
          <Text style={styles.MPAmount}>Date de paiement : {paiementDate}</Text>
        )}
      </View>
      <View style={styles.total}>
        <Text style={styles.TotalAmount}>
          Montant : {parse_to_float_number(bill.deliveryman_bill_amount)} €
        </Text>
      </View>
      <View style={styles.bottomButton}>
        <DownloadButton user_id={user_id} bill_id={bill.deliveryman_bill_id} />
        <RecapButton user_id={user_id} bill_id={bill.deliveryman_bill_id} />
      </View>
    </View>
  );
}

export function DownloadButton(props: any) {
  const [loading, setLoading] = useState(false);

  return (
    <TouchableOpacity
      onPress={() => {
        setLoading(true);
        download_bill(props.user_id, props.bill_id)
          .then((response) => {
            const dowloadBill = response;
            Linking.openURL(dowloadBill);
          })
          .catch(() => {
            Toast.show({
              type: "error",
              text1: "Erreur",
              text2: I18n.t("profile.notification.error.message5"),
            });
          })
          .finally(() => {
            setLoading(false);
          });
      }}
    >
      <View style={{ ...styles.buttonSize, backgroundColor: "#FF5C85" }}>
        {loading ? (
          <ActivityIndicator color="#fff" />
        ) : (
          <>
            <Facture />
            <Text style={{ ...styles.textstyle, color: "#FFFFFF" }}>
              Facture
            </Text>
          </>
        )}
      </View>
    </TouchableOpacity>
  );
}

export const RecapButton = (props: { user_id: number; bill_id: number }) => {
  return (
    <TouchableOpacity
      onPress={() => {
        // console.log('ok')
        download_recap_bill(props.user_id, props.bill_id).then((response) => {
          const dowloadBill = response;
          Linking.openURL(dowloadBill);
        });
      }}
    >
      <View
        style={{
          ...styles.buttonSize,
          backgroundColor: "#FFFFFF",
          borderWidth: 1,
          borderColor: "#FF5C85",
        }}
      >
        <Recap />
        <Text style={{ ...styles.textstyle, color: "#FF5C85" }}>
          Récapitulatif
        </Text>
      </View>
    </TouchableOpacity>
  );
};
