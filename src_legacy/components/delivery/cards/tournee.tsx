import React from "react";
import { Dimensions, StyleSheet, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import { Overlay } from "react-native-elements";

import Clock from "assets/image_svg/ClockBlack.svg";
import CaretRight from "assets/image_svg/CaretRight.svg";
import { TOURNEE_STATUS } from "src/constants/tournee";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { ITournee } from "src/interfaces/Entities";
import { dateToDay, hour_to_string } from "src_legacy/services/Utils";
import { AcceptTourneOverlay } from "../courses/modal/accept_tournee_overlay";
import { TourneeToast } from "../courses/toast/toast";
import { router } from "expo-router";
// import moment from "moment";

const { width } = Dimensions.get("window");

export function CardTournee({
  tournee,
  onClose,
}: {
  tournee: ITournee;
  onClose: () => void;
}) {
  const [showNewTourneeOverlay, setShowNewTourneeOverlay] =
    React.useState(false);
  const [showAcceptedTourneeOverlay, setShowAcceptedTourneeOverlay] =
    React.useState(false);

  // const [intervalTimer, setIntervalTimer] = React.useState(0);


  const { setSelectedTournee } = tourneeStore();

  // React.useEffect(() => {
  //   const interval = setInterval(() => {
  //     setIntervalTimer((t) => t + 1);
  //   }, 10000);
  //   return () => clearInterval(interval);
  // }, []);

  // const momentObj = React.useMemo(() => {
  //   return moment(tournee.begin_date.date);
  //   //return moment("2022-12-15 13:32:54.000000")
  // }, [tournee.begin_date.date, intervalTimer]);

  // const timer = React.useMemo(() => {
  //   return momentObj.fromNow();
  // }, [momentObj]);

  return (
    <View>
      <TouchableOpacity
        style={styles.main}
        onPress={() => {
          setSelectedTournee(tournee);
          if (tournee.status === TOURNEE_STATUS.SENDED) {
            setShowNewTourneeOverlay(true);
          }
          if (tournee.status === TOURNEE_STATUS.ACCEPTED) {
            setShowAcceptedTourneeOverlay(true);
          }
          router.push("/tournee_detail");
        }}
      >
        <View style={styles.blockTop}>
          <View
            style={{
              ...styles.blockTopLeft,
              // width: tournee.status === TOURNEE_STATUS.SENDED ? "65%" : "60%",
            }}
          >
            <Text style={styles.blockTopTitle}>
              Tournée #{Number(tournee.id)}
            </Text>
            <CaretRight />
            {/* <Text style={styles.blockTopSubTitle}>
              {Number(tournee.orders_count)} commandes
              -{" "}
              <Text style={styles.blockTopSubTitleBold}>
                Gains {tournee.earnings} €
              </Text>  */}
          </View>
          {/* {tournee.status === TOURNEE_STATUS.SENDED && (
            <View style={styles.blockTopRight}>
              <TimerWhite style={{ marginRight: 9.8 }} />
              <View style={styles.blockTimer}>
                <Text style={styles.blockTopTimerText}>À valider</Text>
                <Text style={styles.blockTopTimerSubText}>
                  avant {hour_to_string(tournee.validation_hour_limit)}
                </Text>
              </View>
            </View>
          )}
          {tournee.status === TOURNEE_STATUS.ACCEPTED && (
            <View style={styles.blockTopRightGreen}>
              <ClockWhite2 style={{ marginRight: 5.5 }} />
              <View style={styles.blockTimer}>
                <Text style={styles.blockTopTimerTextGreen}>
                  {momentObj.isBefore() ? "A commencé" : "Commence"}
                </Text>
                <Text style={styles.blockTopTimerSubTextGreen}>
                  {momentObj.isSame() ? "Maintenant" : timer}
                </Text>
              </View>
            </View>
          )} */}
        </View>
        <View style={styles.blockBottom}>
          <View style={styles.blockBottomLeft}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center",
              }}
            >
              <Clock style={styles.icon} />
              <Text style={styles.blockBottomText}>Heure de debut</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 2 }}>
              {/* <Clock style={styles.icon} /> */}
              <Text style={styles.blockBottomTextBold}>
                {dateToDay(tournee.begin_date.date)}{" "}
              </Text>
              <Text style={styles.blockBottomTextBold}>
                {hour_to_string(tournee.begin_hour)}
              </Text>
            </View>
          </View>
          <View style={styles.blockBottomRight}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "flex-end",
                alignItems: "center",
                marginRight: 50,
              }}
            >
              <Clock style={styles.icon} />
              <Text style={styles.blockBottomText}>Heure de fin</Text>
            </View>
            <View style={{ flexDirection: "row", marginLeft: 5 }}>
              {/* <Clock style={styles.icon} /> */}
              <Text style={styles.blockBottomTextBold}>
                {dateToDay(tournee.end_hour.date)}{" "}
              </Text>
              <Text style={styles.blockBottomTextBold}>
                {hour_to_string(tournee.end_hour)}
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
      <Overlay
        isVisible={showNewTourneeOverlay}
        // onBackdropPress={() => routergoBack()}
        onBackdropPress={() => setShowNewTourneeOverlay(false)}
        overlayStyle={styles.overlayBackground}
      >
        <AcceptTourneOverlay
          onClose={() => {
            setShowNewTourneeOverlay(false);
          }}
        />
      </Overlay>

      {/* <Overlay
        isVisible={showAcceptedTourneeOverlay}
        //onBackdropPress={() => navigation.goBack()}
        onBackdropPress={() => setShowAcceptedTourneeOverlay(false)}
        //fullScreen
      >
        {/* <TourneeDetail
          onClose={() => {
            setShowAcceptedTourneeOverlay(false);
            onClose();
          }}
        /> */}
      
      {/* </Overlay> */}
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    backgroundColor: "rgba(231, 231, 231, 0.4)",
    justifyContent: "center",
    alignSelf: "center",
    width: width * 1,
    //height: 120,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: "transparent",
    //borderColor: "#C4C4C4",
    flexDirection: "column",
    marginBottom: 34,
  },
  blockTop: {
    height: 30,
    borderBottomWidth: 0.5,
    //borderBottomColor: "#C4C4C4",
    borderColor: "transparent",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "rgba(231, 231, 231, 0.4)",
  },
  blockTopLeft: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
    paddingHorizontal: 21,
  },
  blockTopRight: {
    width: "35%",
    height: 32,
    backgroundColor: "#ECC53B",
    alignItems: "center",
    borderTopStartRadius: 13.5,
    borderBottomStartRadius: 13.5,
    flexDirection: "row",
    paddingLeft: 11.8,
  },
  blockTopRightGreen: {
    width: "40%",
    height: 32,
    backgroundColor: "#40ADA2",
    alignItems: "center",
    borderTopStartRadius: 13.5,
    borderBottomStartRadius: 13.5,
    flexDirection: "row",
    paddingLeft: 9.5,
  },
  blockTopTimerTextGreen: {
    fontFamily: fontFamily.regular,
    color: "#fff",
    fontSize: 10,
  },
  blockTopTimerSubTextGreen: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 14,
  },
  blockTopTitle: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 16,
  },
  blockTopSubTitle: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 16,
  },
  blockTopSubTitleBold: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 16,
  },
  blockTimer: {
    flexDirection: "column",
    justifyContent: "center",
    height: 32,
    paddingVertical: 8,
  },
  blockTopTimerText: {
    fontFamily: fontFamily.Bold,
    color: "#fff",
    fontSize: 14,
  },
  blockTopTimerSubText: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 10,
  },
  blockBottom: {
    height: 60,
    flexDirection: "row",
    backgroundColor: "#FCFAFA",
  },
  blockBottomLeft: {
    width: "50%",
    //borderRightWidth: 0.5,
    //borderRightColor: "#C4C4C4",
    borderColor: "#transparent",
    justifyContent: "center",
    paddingLeft: 21,
  },
  blockBottomRight: {
    width: "50%",
    justifyContent: "center",
    flexDirection: "column",
    alignItems: "center",
  },
  blockBottomText: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 22,
  },
  icon: {
    marginRight: 1,
  },
  blockBottomTextBold: {
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    fontSize: 16,
    lineHeight: 17.07,
  },
  overlayBackground: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
});
