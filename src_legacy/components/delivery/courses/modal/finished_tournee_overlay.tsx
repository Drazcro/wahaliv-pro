import React from "react";
import { Text, View, TouchableOpacity } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets/image_svg/X.svg";
import Success from "assets/image_svg/success.svg";
import { StyleSheet } from "react-native";

import { fontFamily } from "src/theme/typography";
import { DEVICE } from "src/constants/default_values";
export function FinishedTourneeOverlay(props: {
  visible: boolean;
  onClose: () => void;
  onPress: () => void;
}) {
  return (
    <Overlay
      isVisible={props.visible}
      onBackdropPress={props.onClose}
      overlayStyle={{
        backgroundColor: "rgba(16, 16, 16, 0.4)",
      }}
    >
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <View style={{ alignSelf: "center", paddingTop: 10 }}>
            <Success />
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onClose()}
          >
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
          <View style={styles.wrapper}>
            <Text style={styles.courseRefus}>Tournée finalisée</Text>
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                Maintenant, prends le temps de te reposer un peu avant ta
                nouvelle tournée
              </Text>
            </View>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity style={styles.btnAcpt} onPress={props.onPress}>
              <Text style={styles.btnTextAcpt}>Revenir à mes courses</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Overlay>
  );
}




const styles = StyleSheet.create({
  backgroundModal: {
    width: DEVICE.width,
    height: DEVICE.height,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  modalContainer: {
    width: DEVICE.width - 42,
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  courseRefus: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 16,
    lineHeight: 24,
    textAlign: "center",
    marginBottom: 25,
  },

  btnAcpt: {
    backgroundColor: "#FF5C85",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginBottom: 20,
  },
  btnTextAcpt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
  closeButton: {
    position: "absolute",
    top: 20,
    right: 20,
  },
  text: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
  },
  wrapper: {
    marginBottom: 50,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper1: {
    alignItems: "center",
  },
  textContainer: {
    width: 265,
    justifyContent: "center",
    alignItems: "center",
  },
});
