import I18n from "i18n-js";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import Croix from "assets/image_svg/croix.svg";
import { fontFamily } from "src/theme/typography";
import { parse_to_float_number } from "src_legacy/services/Utils";
import { color } from "src/theme";

interface ReverseEarningsModalProps {
  onClose: () => void;
  onAccept: () => void;
  price: number;
}

export function ReverseEarningsModal(props: ReverseEarningsModalProps) {
  return (
    <View style={styles.main}>
      <View style={styles.container}>
        <TouchableOpacity
          style={{ position: "absolute", top: 15, right: 15 }}
          onPress={props.onClose}
        >
          <Croix width={16} height={16} />
        </TouchableOpacity>
        <View style={styles.text_container}>
          <Text style={styles.text_label}>
            {I18n.t("profile.reversEarnings.modalMessageStart")}
            <Text style={styles.price_label}>
              {" "}
              {parse_to_float_number(props.price)} €
            </Text>
          </Text>
          <Text style={styles.text_label}>
            {I18n.t("profile.reversEarnings.modalMessageEnd")}
          </Text>
        </View>
        <Button
          type="solid"
          title={I18n.t("profile.reversEarnings.buttonModal")}
          buttonStyle={styles.button}
          titleStyle={styles.text_button}
          onPress={props.onAccept}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  container: {
    width: 318,
    height: 186,
    backgroundColor: "white",
    borderRadius: 20,
    position: "relative",
  },
  text_container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  text_label: {
    fontFamily: fontFamily.regular,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  price_label: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  button: {
    justifyContent: "center",
    alignSelf: "center",
    width: 270.81,
    height: 44,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
    marginBottom: 32,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
});
