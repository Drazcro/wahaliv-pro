import { StyleSheet } from "react-native";

import { fontFamily } from "src/theme/typography";
import { DEVICE } from "src/constants/default_values";

export const styles = StyleSheet.create({
  backgroundModal: {
    width: DEVICE.width,
    height: DEVICE.height,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  modalContainer: {
    width: DEVICE.width - 42,
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  courseRefus: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 16,
    lineHeight: 24,
    textAlign: "center",
    marginBottom: 25,
  },

  btnAcpt: {
    backgroundColor: "#FF5C85",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginBottom: 20,
  },
  btnTextAcpt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
  closeButton: {
    position: "absolute",
    top: 20,
    right: 20,
  },
  text: {
    fontFamily: fontFamily.regular,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
  },
  wrapper: {
    marginBottom: 50,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper1: {
    alignItems: "center",
  },
  textContainer: {
    width: 265,
    justifyContent: "center",
    alignItems: "center",
  },
});
