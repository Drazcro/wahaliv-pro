import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Dimensions,
  Image,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { Divider } from "react-native-elements";
import { images } from "src/theme";
import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

import Path from "assets//image_svg/Path.svg";
import Money from "assets//image_svg/Money.svg";
import Timer from "assets//image_svg/ClockBlack.svg";
import StorefrontRed from "assets//image_svg/StorefrontRed.svg";
import UserCircle from "assets//image_svg/UserCircle.svg";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import {
  AccepteTournee,
  GetDetailTournee,
  RefuseTournee,
} from "src_legacy/services/Loaders";
import { ITourneeDetail } from "src/interfaces/Entities";
import { TOURNEE_STEP_TYPE } from "src/constants/tournee";
import { hour_to_string } from "src_legacy/services/Utils";
import Euro from "assets/image_svg/Euro.svg";
import { soundStore } from "src_legacy/zustore/sound_store";
import MapPreviewTournee from "../../maps/map_preview_tournee";

const { width: screenWidth } = Dimensions.get("window");

export function AcceptTourneOverlay({ onClose }: { onClose: () => void }) {
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [loaderRefus, setLoaderRefus] = useState(false);
  const [loaderAccept, setLoaderAccept] = useState(false);
  const [currentTournee, setCurrentTournee] = useState<ITourneeDetail>(
    {} as ITourneeDetail,
  );
  const [, setContent] = useState({});

  const { selectedTournee, setShouldReloadTournee } = tourneeStore();

  const onAcceptTournee = () => {
    const idTournee = selectedTournee.id;
    setLoaderAccept(true);
    AccepteTournee(idTournee).then(() => {
      setLoaderAccept(false);
      setShouldReloadTournee(true);
      onClose();
    });
  };

  const onRefusTournee = () => {
    const idTournee = selectedTournee.id;
    setLoaderRefus(true);
    RefuseTournee(idTournee).then(() => {
      setLoaderRefus(false);
      setShouldReloadTournee(true);
      onClose();
    });
  };

  const loadData = () => {
    const idTournee = selectedTournee.id;
    GetDetailTournee(idTournee)
      .then((t: ITourneeDetail) => {
        if (t && t.id) {
          setCurrentTournee(t);
        } else {
          setError(true);
        }
      })
      .catch(() => setError(true))
      .finally(() => {
        //setError(false);
        setIsLoading(false);
      });
  };
  const stopSound = soundStore((v) => v.stopSound);
  const stopAlertSound = async () => {
    const stop_status = await stopSound();
    console.log("sound unloaded: ", stop_status);
  };

  useEffect(() => {
    console.log("useEffect");
    loadData();
    return () => {
      stopAlertSound();
    };
  }, []);

  console.log("modal", selectedTournee);

  if (error) {
    onClose();
  }

  if (isLoading === true) {
    return (
      <View style={styles.loadingContainer}>
        <View style={styles.loading}>
          <Text>Chargement en cours ...</Text>
        </View>
      </View>
    );
  }

  return (
    <View>
      <ScrollView
        contentContainerStyle={styles.scrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.container}>
          <View style={styles.header}>
            <Text style={styles.title}>
              Nouvelle tournée #{currentTournee?.id}
            </Text>
            <Text style={styles.subTitle}>
              {currentTournee?.orders_count} commandes -{" "}
              {currentTournee?.steps_count} arrêts
            </Text>
            <View style={styles.map}>
              <MapPreviewTournee tournee={currentTournee} isOnOverlay />
            </View>
          </View>
          <View style={styles.hour}>
            <View
              style={{
                ...styles.timeContainer,
                borderRightWidth: 1,
                borderRightColor: "#C4C4C4",
              }}
            >
              <View style={styles.timeTitle}>
                <Timer />
                <Text style={styles.timeText}>Heure de debut </Text>
              </View>
              <Text style={styles.timeHour}>
                {hour_to_string(currentTournee?.begin_hour)}
              </Text>
            </View>
            <View style={styles.timeContainer}>
              <View style={styles.timeTitle}>
                <Timer />
                <Text style={styles.timeText}>Heure de fin</Text>
              </View>
              <Text style={styles.timeHour}>
                {hour_to_string(currentTournee?.end_hour)}
              </Text>
            </View>
          </View>
          <Divider />
          <View style={styles.courses}>
            <View style={styles.distance}>
              <Path />
              <Text style={styles.distanceText}>
                Distance de la tournée{" "}
                <Text style={styles.distanceTextBold}>
                  {currentTournee?.distance} Km
                </Text>
              </Text>
            </View>
            <View style={styles.distance}>
              <Money />
              <Text style={{ ...styles.distanceText, marginLeft: 15 }}>
                Gains de la tournée{" "}
                <Text style={styles.distanceTextBold}>
                  {currentTournee?.earnings} €
                </Text>
              </Text>
            </View>
            {currentTournee.tips && parseFloat(currentTournee.tips) > 0 && (
              <View style={styles.distance}>
                <Euro style={{ marginLeft: 4 }} />
                <View
                  style={{
                    backgroundColor: "#ECC53B",
                    marginLeft: 18,
                    borderRadius: 5,
                  }}
                >
                  <Text style={{ ...styles.distanceText, marginLeft: 0 }}>
                    Pourboires de la tournée{" "}
                    <Text style={styles.distanceTextBold}>
                      {currentTournee.tips} €
                    </Text>
                  </Text>
                </View>
              </View>
            )}
          </View>
          <View style={styles.tourneContainer}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              onTouchStart={() => {
                setContent({ flex: 1 });
              }}
              onMomentumScrollEnd={() => {
                setContent({});
              }}
              onScrollEndDrag={() => {
                setContent({});
              }}
              style={{ maxHeight: 200 }}
            >
              {currentTournee &&
                currentTournee?.steps &&
                currentTournee?.steps.map((step, index) => (
                  <View key={index}>
                    <Tournee
                      number={index + 1}
                      name={step.target.name}
                      adress={step.target.adress}
                      hour={step.hour}
                      isLivrer={
                        step.type === TOURNEE_STEP_TYPE.DELIVERY ? true : false
                      }
                      isLast={
                        currentTournee?.steps.length === index + 1
                          ? true
                          : false
                      }
                    />
                  </View>
                ))}
            </ScrollView>
          </View>
          <View style={styles.btnContainer}>
            <TouchableOpacity
              onPress={() => onRefusTournee()}
              style={styles.btnLeft}
            >
              {loaderRefus ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.btnLeftText}>Refuser</Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => onAcceptTournee()}
              style={styles.btnRight}
            >
              {loaderAccept ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.btnRightText}>Accepter</Text>
              )}
            </TouchableOpacity>
          </View>
          <TouchableOpacity style={styles.btnClose} onPress={onClose}>
            <Image style={styles.btnIconClose} source={images.Icone.Close} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
    //</Overlay>
  );
}

interface ITourneeProps {
  isLivrer?: boolean;
  isLast?: boolean;
  number: number;
  name: string;
  adress: string;
  hour: string;
}

const Tournee = ({
  isLivrer,
  isLast,
  number,
  name,
  adress,
  hour,
}: ITourneeProps) => {
  return (
    <View style={styles.tourneeMain}>
      {isLast ? <View /> : <View style={styles.lineDash} />}
      <View style={styles.indexContent}>
        <Text style={{ color: "#fff" }}>{number}</Text>
      </View>
      <View style={{ flexDirection: "column", width: "80%" }}>
        <View
          style={{
            flexDirection: "row",
            alignItems: "center",
          }}
        >
          <Text style={styles.tourneeHourBold}>{hour_to_string(hour)}</Text>
          <View style={styles.tourneeIconName}>
            {isLivrer ? <UserCircle /> : <StorefrontRed />}
            <Text style={styles.tourneeName}>{name}</Text>
          </View>
        </View>
        <Text style={styles.tourneeAdress}>{adress}</Text>
      </View>
    </View>
  );
};

export const styles = StyleSheet.create({
  background: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  scrollView: {
    paddingTop: 25,
  },
  btnClose: {
    position: "absolute",
    top: 8,
    right: 8,
  },
  btnIconClose: {
    width: 35,
    height: 35,
  },
  container: {
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  header: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 18,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
    marginTop: 15,
  },
  subTitle: {
    fontFamily: fontFamily.regular,
    textAlign: "center",
    fontSize: 14,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
  },
  map: {
    position: "relative",
    width: "100%",
    height: 220,
  },
  hour: {
    flexDirection: "row",
    justifyContent: "space-around",
    borderBottomWidth: 1,
    borderColor: "##4C4C4",
    height: 68,
  },
  timeContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    paddingVertical: 10,
  },
  timeTitle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  timeText: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
  timeHour: {
    fontFamily: fontFamily.Bold,
    color: "#1D2A40",
    fontSize: 18,
    lineHeight: 21.97,
  },
  courses: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 40,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
  },
  distance: {
    flexDirection: "row",
    marginBottom: 5,
    alignItems: "center",
  },
  distanceText: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 14.63,
    color: "#112842",
    marginLeft: 20,
  },
  distanceTextBold: {
    fontFamily: fontFamily.Bold,
    color: "#112842",
    fontSize: 12,
    lineHeight: 17.07,
    marginLeft: 5,
    marginRight: 5,
  },
  btnContainer: {
    flexDirection: "row",
    justifyContent: "center",
    marginBottom: 39,
  },
  btnLeft: {
    backgroundColor: "#ED7178",
    width: 67,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginRight: 5,
  },
  btnLeftText: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 11,
    lineHeight: 22,
  },
  btnRight: {
    backgroundColor: "#40ADA2",
    width: 258,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
  },
  btnRightText: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 18,
    lineHeight: 22,
  },
  tourneContainer: {
    marginBottom: 15,
    paddingLeft: 26,
  },
  tourneeMain: {
    flexDirection: "row",
    marginBottom: 25,
    position: "relative",
  },
  lineDash: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 10,
    height: 60,
  },
  indexContent: {
    width: 22,
    height: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginRight: 10,
    backgroundColor: "#FF5C85",
    marginTop: 5,
  },
  tourneeHourBold: {
    fontFamily: fontFamily.Bold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 18,
    marginRight: 10,
  },
  tourneeIconName: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "rgba(255, 92, 133, 0.1)",
    paddingHorizontal: 10,
    paddingVertical: 3,
  },
  tourneeName: {
    fontFamily: fontFamily.Bold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 18,
    marginLeft: 5,
  },
  tourneeAdress: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 18,
  },
  loadingContainer: {
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
    paddingTop: 41,
    marginTop: 30,
    marginBottom: 20,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: screenWidth,
    height: 100,
    backgroundColor: "white",
  },
});
