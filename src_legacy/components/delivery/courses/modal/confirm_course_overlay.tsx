import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets//image_svg/X.svg";
import { styles } from "./reject_course.styles";

export function ConfirmCourseOverlay(props: {
  visible: boolean;
  onClose: () => void;
  onAccept: () => void;
  loadingAccept: boolean;
}) {
  return (
    <Overlay
      isVisible={props.visible === true ? true : false}
      onBackdropPress={props.onClose}
      overlayStyle={{
        backgroundColor: "rgba(16, 16, 16, 0.4)",
      }}
    >
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <View style={styles.wrapper0}>
            <Text style={styles.courseRefus}>Confirmation de la course</Text>
            <View style={styles.textContainer0}>
              <Text style={styles.text}>Souhaites-tu accepter la course ?</Text>
            </View>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity
              style={styles.btnGreenFull}
              onPress={props.onAccept}
            >
              {props.loadingAccept ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.textAcceptGreen}>Accepter la course</Text>
              )}
            </TouchableOpacity>
          </View>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onClose()}
          >
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
        </View>
      </View>
    </Overlay>
  );
}
