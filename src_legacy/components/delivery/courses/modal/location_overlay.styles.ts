import { DEVICE } from "src/constants/default_values";
import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

const width = DEVICE.width;

export const styles = StyleSheet.create({
  container: {
    borderRadius: 30,
  },
  backgroundModal: {
    width: DEVICE.width * 0.8,
    height: DEVICE.height * 0.6,
    alignItems: "center",
    justifyContent: "space-evenly",
    marginTop: 0,
    paddingVertical: 20,
    borderRadius: 30,
  },

  acceptButton: {
    backgroundColor: "#FF5C85",
    width: 258,
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
  },
  text: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 21.94,
    color: "#1D2A40",
    textAlign: "center",
  },

  buttonText: {
    fontFamily: fontFamily.Bold,
    color: "white",
    fontSize: 16,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
});
