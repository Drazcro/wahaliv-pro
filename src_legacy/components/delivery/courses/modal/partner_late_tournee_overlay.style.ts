import { StyleSheet } from "react-native";

import { fontFamily } from "src/theme/typography";
import { DEVICE } from "src/constants/default_values";

export const styles = StyleSheet.create({
  backgroundModal: {
    width: DEVICE.width,
    height: DEVICE.height,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  modalContainer: {
    width: DEVICE.width - 42,
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  refus: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "center",
  },
  courseRefus: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 18,
    lineHeight: 21.94,
    textAlign: "center",
    marginBottom: 25,
  },

  btnAcpt: {
    backgroundColor: "#FF5C85",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginBottom: 20,
  },
  btnTextAcpt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
  btnRefus: {
    backgroundColor: "#fff",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    borderWidth: 1,
    borderColor: "#FF5C85",
    marginBottom: 20,
  },
  btnTextRefus: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#FF5C85",
  },
  closeButton: {
    position: "absolute",
    top: 20,
    right: 20,
  },
  text: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "center",
  },
  wrapper: {
    marginBottom: 30,
    marginTop: 35,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper1: {
    alignItems: "center",
  },
  textContainer: {
    width: 265,
    justifyContent: "center",
    alignItems: "center",
  },
});
