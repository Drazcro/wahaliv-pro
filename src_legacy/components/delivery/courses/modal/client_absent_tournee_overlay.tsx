import React from "react";
import { Text, View, TouchableOpacity, ActivityIndicator } from "react-native";
import { Overlay } from "react-native-elements";
import X from "assets/image_svg/X.svg";

import { StyleSheet } from "react-native";

import { fontFamily } from "src/theme/typography";
import { DEVICE } from "src/constants/default_values";

export function ClientAbsentTourneeOverlay(props: {
  visible: boolean;
  loaderConfirm: boolean;
  Resto?: string;
  onClose: () => void;
  onAccept: () => void;
}) {
  return (
    <Overlay isVisible={props.visible} onBackdropPress={props.onClose}>
      <View style={styles.backgroundModal}>
        <View style={styles.modalContainer}>
          <TouchableOpacity
            style={styles.closeButton}
            onPress={() => props.onClose()}
          >
            <X onPress={props.onClose} width={20} height={20} />
          </TouchableOpacity>
          <View style={styles.wrapper}>
            <Text style={styles.courseRefus}>Client absent ?</Text>
            <View style={styles.textContainer}>
              <Text style={styles.text}>
                Je confirme l’absence du client et je retournerai la commande
                dans le resto {props.Resto} à la fin de ma tournée.
              </Text>
            </View>
          </View>
          <View style={styles.wrapper1}>
            <TouchableOpacity style={styles.btnAcpt} onPress={props.onAccept}>
              {props.loaderConfirm ? (
                <ActivityIndicator color="#fff" />
              ) : (
                <Text style={styles.btnTextAcpt}>Confirmer</Text>
              )}
            </TouchableOpacity>
            <TouchableOpacity style={styles.btnRefus} onPress={props.onClose}>
              <Text style={styles.btnTextRefus}>Annuler</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Overlay>
  );
}




const styles = StyleSheet.create({
  backgroundModal: {
    width: DEVICE.width,
    height: DEVICE.height,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 0,
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  modalContainer: {
    width: DEVICE.width - 42,
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
  },
  courseRefus: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 18,
    lineHeight: 21.94,
    textAlign: "center",
    marginBottom: 25,
  },

  btnAcpt: {
    backgroundColor: "#FF5C85",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginBottom: 20,
  },
  btnTextAcpt: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#fff",
  },
  btnRefus: {
    backgroundColor: "#fff",
    width: 270.81,
    height: 44,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    borderWidth: 1,
    borderColor: "#FF5C85",
    marginBottom: 20,
  },
  btnTextRefus: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: "#FF5C85",
  },
  closeButton: {
    position: "absolute",
    top: 20,
    right: 20,
  },
  text: {
    fontFamily: fontFamily.regular,
    color: "#1D2A40",
    fontSize: 14,
    lineHeight: 17.07,
    textAlign: "center",
  },
  wrapper: {
    marginBottom: 50,
    marginTop: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  wrapper1: {
    alignItems: "center",
  },
  textContainer: {
    width: 265,
    justifyContent: "center",
    alignItems: "center",
  },
});
