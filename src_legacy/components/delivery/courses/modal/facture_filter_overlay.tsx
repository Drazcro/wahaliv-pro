import { Calendar, ThemeType } from "src/libs/calendario";
import moment, { Moment } from "moment";
import React, { useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Overlay, Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import Close from "assets/image_svg_partner/X.svg";
import I18n from "i18n-js";
import CalendarSVG from "assets/image_svg/Calendar.svg";
import RNPickerSelect from "react-native-picker-select";
import CaretDown from "assets/image_svg/CaretDown.svg";
import { styles, theme } from "./facture_filter_overlay.styles";

const FactureFilter = (props: {
  startDate: Moment;
  endDate: Moment;
  setStartDate: (date: Moment) => void;
  setEndDate: (date: Moment) => void;
  onClose: () => void;
  onApply: () => void;
}) => {
  const [periode, setPeriode] = useState("2");

  const onValueChange = (value: string) => {
    setPeriode(value);
    switch (value) {
      case "2": //now
        props.setStartDate(moment(new Date()));
        props.setEndDate(moment(new Date()));
        break;
      case "3": //hier
        props.setStartDate(moment(new Date()).subtract(1, "days"));
        props.setEndDate(moment(new Date()).subtract(1, "days"));
        break;
      case "4": // la semaine derniere
        {
          const weekDay = new Date().getDay();
          props.setStartDate(
            moment(new Date()).subtract(7 + weekDay - 1, "days"),
          );
          props.setEndDate(moment(new Date()).subtract(weekDay, "days"));
        }
        break;
      case "5": // les 7 jours derniers
        props.setStartDate(moment(new Date()).subtract(7, "days"));
        props.setEndDate(moment(new Date()));
        break;
      case "6": // les 30 jours precds
        props.setStartDate(moment(new Date()).subtract(30, "days"));
        props.setEndDate(moment(new Date()));
        break;

      default:
        break;
    }
  };

  const setRange = (selectedDate: Date) => {
    setPeriode("1");
    props.endDate.hours(0).seconds(0).minutes(0).milliseconds(0);
    props.startDate.hours(0).seconds(0).minutes(0).milliseconds(0);
    const diff_endDate: number = moment(selectedDate).diff(props.endDate);
    const diff_startDate: number = moment(selectedDate).diff(props.startDate);
    const diff_startAndEnd: number = props.startDate.diff(props.endDate);
    const diff_threeDays: number = moment(new Date(2022, 1, 3)).diff(
      moment(new Date(2022, 1, 1)),
    );

    if (diff_startDate == 0) {
      props.setEndDate(moment(selectedDate));
      return;
    }
    if (diff_endDate == 0) {
      props.setStartDate(moment(selectedDate));
      return;
    }
    if (diff_endDate > 0 && diff_threeDays <= Math.abs(diff_startAndEnd)) {
      props.setEndDate(moment(selectedDate));
      return;
    }
    if (diff_endDate > 0) {
      props.setEndDate(moment(selectedDate));
      return;
    } else {
      props.setStartDate(moment(selectedDate));
    }
  };

  return (
    <View style={{ width: "100%", height: "100%" }}>
      <Text style={styles.title}>
        {" "}
        {I18n.t("partner.order.old.filter.title")}
      </Text>
      <TouchableOpacity style={styles.closeBtn}>
        <Close onPress={props.onClose} />
      </TouchableOpacity>
      <View style={styles.selectContainer}>
        <Text style={styles.text_label}>
          {I18n.t("partner.order.old.filter.periode")}
        </Text>
        <RNPickerSelect
          useNativeAndroidPickerStyle={false}
          placeholder={{}}
          onValueChange={onValueChange}
          value={periode}
          Icon={() => (
            <View style={{ right: 15 }}>
              <CaretDown />
            </View>
          )}
          style={{
            inputAndroidContainer: styles.picker_input_container,
            inputIOSContainer: styles.picker_input_container,
            inputIOS: styles.picker_input,
            inputAndroid: styles.picker_input,
          }}
          items={items}
        />
      </View>
      <View style={styles.dateContainer}>
        <CalendarSVG style={styles.calendar} />
        <View style={styles.dateInpuContainer}>
          <Text style={styles.dateInput}>
            {props.startDate.format("DD")} {props.startDate.format("MMM")}{" "}
            {props.startDate.format("YYYY")}
          </Text>
        </View>
        <Text style={styles.separator}>-</Text>
        <View style={styles.dateInpuContainer}>
          <Text style={styles.dateInput}>
            {props.endDate.format("DD")} {props.endDate.format("MMM")}{" "}
            {props.endDate.format("YYYY")}
          </Text>
        </View>
      </View>
      <View style={{ flex: 1 }}>
        <View style={{ width: "90%", alignSelf: "center", height: "90%" }}>
          <Calendar
            locale="fr"
            isFilter={true}
            onPress={(date) => setRange(date)}
            minDate={moment(new Date()).subtract(6, "M").toDate()}
            startDate={props.startDate.toDate()}
            endDate={props.endDate.toDate()}
            maxDate={moment(new Date()).add(6, "M").toDate()}
            startingMonth={moment(new Date())
              .subtract(6, "M")
              .format("YYYY-MM-DD")}
            theme={theme}
            firstDayMonday={true}
            numberOfMonths={12}
            dayNames={["L", "M", "X", "J", "V", "S", "D"]}
            monthHeight={370}
          />
        </View>
        <View style={styles.buttonContainer}>
          <Button
            title={I18n.t("partner.order.old.filter.btn.cancel")}
            buttonStyle={styles.button}
            titleStyle={styles.text_button}
            onPress={props.onClose}
          />
          <View style={styles.btnSeparator} />
          <Button
            title={I18n.t("partner.order.old.filter.btn.save")}
            buttonStyle={{ ...styles.button, backgroundColor: "#FF5C85" }}
            titleStyle={{ ...styles.text_button, color: "#fff" }}
            onPress={props.onApply}
          />
        </View>
      </View>
    </View>
  );
};

const items = [
  {
    label: "Personalisée",
    value: "1",
  },
  {
    label: "Aujourd’hui",
    value: "2",
  },
  {
    label: "Hier",
    value: "3",
  },
  {
    label: "La semaine dernier",
    value: "4",
  },
  {
    label: "Les 7 dernieres jours",
    value: "5",
  },
  {
    label: "Les 30 dernieres jours",
    value: "6",
  },
];

export default FactureFilter;
