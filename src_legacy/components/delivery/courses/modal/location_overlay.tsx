import React from "react";
import { Text, TouchableOpacity, View } from "react-native";
import { Overlay } from "react-native-elements";

import { styles } from "./location_overlay.styles";

import { useGPS } from "src/hooks/useLocalisation";
import { ActivityIndicator } from "react-native-paper";
import usePermissionLevel from "src_legacy/hooks/usePermissionLevel";
import {
  PermissionType,
  getSVGByPermissionType,
} from "src_legacy/components/permissions/generic_permission";

export const LocationOverlay = () => {
  const { isLoading, isGPSOn, isEnabled } = useGPS();

  const { onGrantCallback, enableLocationService } = usePermissionLevel();

  const onAccept = () => {
    if (!isGPSOn) {
      enableLocationService();
      onGrantCallback();
    } else {
      onGrantCallback();
    }
  };

  const msg = React.useMemo(() => {
    if (isLoading) {
      return "Verification des permissions GPS";
    } else if (!isGPSOn) {
      return "Veuillez activer le service GPS sur votre Smartphone";
    } else {
      return "Merci de partager ta position PRECISE pour acceder aux courses";
    }
  }, [isGPSOn, isLoading, isEnabled]);

  return (
    <Overlay isVisible={!isEnabled} overlayStyle={styles.container}>
      <View style={styles.backgroundModal}>
        {getSVGByPermissionType(PermissionType.LOCATION)}
        <Text style={styles.text}>{msg}</Text>
        {!isLoading ? (
          <TouchableOpacity onPress={onAccept} style={styles.acceptButton}>
            <Text style={styles.buttonText}>Partager</Text>
          </TouchableOpacity>
        ) : null}
        {isLoading ? <ActivityIndicator /> : null}
      </View>
    </Overlay>
  );
};
