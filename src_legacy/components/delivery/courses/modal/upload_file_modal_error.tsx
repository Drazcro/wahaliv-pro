import I18n from "i18n-js";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { Button } from "react-native-elements";
import Error from "assets/image_svg/Error.svg";
import Croix from "assets/image_svg/croix.svg";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export function UploadFileModalError(props: {
  isSizeError: boolean;
  isFileError: boolean;
  onClose: (arg0: boolean) => void;
}) {
  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <View style={styles.wrapper}>
          <TouchableOpacity
            style={{ position: "absolute", top: 15, right: 15 }}
            onPress={() => props.onClose(false)}
          >
            <Croix width={16} height={16} />
          </TouchableOpacity>
          <Error width={41} height={41} style={{ marginBottom: 13 }} />
          <Text style={styles.text_success}>
            {I18n.t("profile.supportingDocument.failedDownload")}
          </Text>
          {props.isSizeError && !props.isFileError && (
            <Text style={styles.text_document}>
              La taille du fichier est limitée à{" "}
              <Text style={styles.text_bold}>6 Mo</Text>. {"\n"} Veuillez
              réessayer avec un fichier {"\n"} moins volumineux.
            </Text>
          )}
          {props.isFileError && (
            <Text style={styles.text_document}>
              Le type du fichier est incorrect. {"\n"} Veuillez réessayer avec
              un format {"\n"} <Text style={styles.text_bold}>jpg, png</Text> ou{" "}
              <Text style={styles.text_bold}>pdf</Text>.
            </Text>
          )}
          <Button
            type="solid"
            title={I18n.t("profile.supportingDocument.buttonTried")}
            buttonStyle={styles.button}
            titleStyle={styles.text_button}
            onPress={() => props.onClose(false)}
          />
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
  content: {
    width: 318,
    height: 275,
    backgroundColor: "white",
    borderRadius: 20,
  },
  button: {
    width: 270.81,
    height: 44,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
    marginTop: 37.54,
  },
  wrapper: {
    flex: 1,
    position: "relative",
    justifyContent: "center",
    alignItems: "center",
  },
  text_success: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 16,
    lineHeight: 22,
    marginBottom: 6,
  },
  text_document: {
    fontFamily: fontFamily.regular,
    textAlign: "center",
    color: "rgba(0, 0, 0, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 16,
    lineHeight: 22,
  },
  text_bold: {
    fontFamily: fontFamily.semiBold,
    color: "#000000",
    fontSize: 14,
    lineHeight: 22,
  },
});
