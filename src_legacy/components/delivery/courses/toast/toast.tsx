import React, { useRef } from "react";
import {
  Image,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import ValidateImage from "assets/image_svg/validateImg.svg";
import NewTournee from "assets/image_svg/newTournee.svg";
import ErrorIcon from "assets/image_svg/errorIcon.svg";
import { fontFamily } from "src/theme/typography";
import { toastStore } from "src/zustore/toast_store";
import CloseBtn from "assets/image_svg/X.svg";
import ChatNotification from "assets/image_svg/ChatNotificationIcon.svg";

// export function TourneeToast(type: any, title: string, text1: string) {
export function TourneeToast() {
  const { title, text1, type, hideToast } = toastStore();

  return (
    <View
      style={[
        styles.container,
        Platform.OS === "android" ? styles.androidShadow : styles.iosShadow,
      ]}
    >
      {type === "valide" && (
        <View style={styles.imgPosition}>
          <ValidateImage />
        </View>
      )}
      {type === "newTournee" && (
        <View style={styles.imgPosition}>
          <NewTournee />
        </View>
      )}
      {type === "error" && (
        <View style={styles.imgPosition}>
          <ErrorIcon />
        </View>
      )}
      {type === "newMessage" && (
        <View style={styles.imgPosition}>
          <ChatNotification />
        </View>
      )}
      {type === "loginError" && (
        <View style={styles.imgPosition}>
          <ErrorIcon />
        </View>
      )}
      <View style={styles.fontPosition}>
        <Text style={styles.titleStyle}>{title}</Text>
        <Text
          style={[
            styles.textStyle,
            {
              width:
                title ===
                "Bonne route vers le restaurant pour récupérer la commande !"
                  ? "50%"
                  : "70%",
            },
          ]}
        >
          {text1}
        </Text>
      </View>
      <TouchableOpacity
        onPressIn={hideToast}
        style={{
          position: "absolute",
          right: 10,
          top: 10,
        }}
      >
        <CloseBtn />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    display: "flex",
    flexDirection: "row",
    maxWidth: 359,
    height: 105,
    marginBottom: 26,
    marginRight: "10%",
    backgroundColor: "white",
    marginVertical: "auto",
    marginTop: "15%",
    alignItems: "center",
    alignSelf: "center",
    borderRadius: 15,
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.25,
    position: "absolute",
    zIndex: 10000,
    borderColor: "white",
    // shadowColor: "red",
    borderWidth: 1,
  },

  androidShadow: {
    shadowColor: "rgba(0, 0, 0, 0.8)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    elevation: 5,
    // shadowOpacity: 0.25,
    // shadowRadius: 7.49,
  },
  iosShadow: {
    shadowColor: "rgba(0, 0, 0, 0.8)",
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 5,
    borderWidth: 1,
    borderColor: "#e7e7e7",
  },

  imgPosition: {
    marginLeft: "5%",
  },

  fontPosition: {
    height: "100%",
    justifyContent: "center",
    marginLeft: "5%",
    marginHorizontal: "10%",
  },

  titleStyle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "rgba(29, 42, 64, 1)",
    lineHeight: 22,
  },

  textStyle: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    // width: text ,
    lineHeight: 20,
    marginTop: 5,
    color: "rgba(29, 42, 64, 1)",
  },
});
