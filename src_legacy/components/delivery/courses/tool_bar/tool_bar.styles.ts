import { Platform, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    justifyContent: "space-around",
    paddingTop: Platform.OS === "ios" ? 45 : 50,
    flexDirection: "row",
    alignItems: "center",
    gap: 20,
  },
  icon: {
    width: 34,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
  },
  caption: {
    color: "#8D98A0",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    // marginRight: 20,
  },
  text: {
    fontFamily: fontFamily.semiBold,
    fontSize: 18.3,
    lineHeight: 24.38,
    color: "#1D2A40",
  },
});

export default styles;
