import React from "react";
import { Text, View, TouchableOpacity, Platform } from "react-native";
import CaretLeft from "assets//image_svg/CaretLeft.svg";
import styles from "./tool_bar.styles";
import { fontFamily } from "src/theme/typography";
import { Button, Divider, Icon } from "react-native-elements";
import { router } from "expo-router";

export const Toolbar = ({
  title,
  withBack = true,
  state,
  avatar,
  showReload = false,
  username,
  tourneeId,
}: {
  title: string;
  withBack?: boolean;
  state?: string;
  avatar?: React.ReactNode;
  showReload?: boolean;
  username?: string;
  tourneeId: number;
}) => {
  // const actionGoBack = () => {
  //   router.back();
  // };

  return (
    <React.Fragment>
      <View
        style={{
          backgroundColor: "white",
          justifyContent: "center",
          paddingTop: Platform.OS === "ios" ? 45 : 40,
          flexDirection: "row",
          width: "100%",
          alignItems: "center", // marginHorizontal: 10,
        }}
      >
        {withBack ? (
          <TouchableOpacity onPress={() => router.back()} style={styles.icon}>
            <CaretLeft />
          </TouchableOpacity>
        ) : null}
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <Text style={{ ...styles.text, marginHorizontal: 50 }}>{title}</Text>
          {tourneeId ? (
            <Text
              style={{
                fontFamily: fontFamily.Medium,
                fontSize: 16,
                color: "#8D98A0",
                marginBottom: 5,
              }}
            >
              {tourneeId}
            </Text>
          ) : null}
        </View>
        {showReload ? (
          <Button icon={<Icon name="refresh" size={25} />} type="clear" />
        ) : null}
        {state ? (
          <Text
            style={{
              fontFamily: fontFamily.Bold,
              color: "rgba(64, 173, 162, 1)",
              fontSize: 14,
            }}
          >
            {state}
          </Text>
        ) : null}
      </View>
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "row",
          marginTop: -11,
        }}
      >
        {avatar}
        <Text
          style={{
            marginLeft: 6,
            fontSize: 16,
            color: "rgba(29, 42, 64, 1)",
            fontFamily: fontFamily.Medium,
          }}
        >
          {username}
        </Text>
      </View>
      <Divider />
    </React.Fragment>
  );
};
