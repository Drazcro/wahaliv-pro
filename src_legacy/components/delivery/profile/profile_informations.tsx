import React from "react";
import { StyleSheet, View } from "react-native";
import { parse_to_float_number } from "src_legacy/services/Utils";
import { fontFamily } from "src/theme/typography";
import { Text } from "react-native-paper";

interface IProfileInformationsProps {
  title: string;
  icon?: SVGElement;
  value: string | number;
  unit?: string;
  icon_plus?: HTMLImageElement;
  startText?: string;
  textValue?: string | number;
  endText?: string;
  tipTextStart?: string;
  tipTextValue: string | number;
  tipTextEnd?: string;
}

export const ProfileInformations = (props: IProfileInformationsProps) => {
  return (
    <View style={styles.container}>
      <View style={styles.content_item_one}>
        {props.icon && <View style={styles.icon}>{props.icon}</View>}
        <View style={styles.content_value}>
          <Text style={styles.text_value}>{props.value}</Text>
          <Text style={styles.text_unity}> {props.unit}</Text>
        </View>
        <View style={styles.content_title}>
          <Text style={styles.text_title}>{props.title}</Text>
        </View>
        <View style={styles.content_message}>
          <View
            style={{
              flexDirection: "row",
            }}
          >
            <Text style={styles.text_message}>{props.startText}</Text>
            <Text style={styles.text_message_value}> {props.textValue} </Text>
            <Text style={styles.text_message}>{props.endText}</Text>
          </View>
          {parseInt(props.tipTextValue.toString()) > 0 ? (
            <View style={{ flexDirection: "row" }}>
              <Text style={styles.text_message}>{props.tipTextStart}</Text>
              <Text style={styles.text_message_value}>
                {" "}
                {parse_to_float_number(props.tipTextValue)} €{" "}
              </Text>
              <Text
                allowFontScaling
                adjustsFontSizeToFit
                style={styles.text_message}
              >
                {props.tipTextEnd}
              </Text>
            </View>
          ) : null}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    // width: "45%",
    marginBottom: 26,
    // marginRight: 10,
    borderWidth: 3,
    borderColor: "white",
    backgroundColor: "rgba(217, 217, 217, 0.2)",
    borderRadius: 11,
    // padding: 10,
    height: 127,
    paddingBottom: 14,
    flex: 1,
    alignItems: "center",
    justifyContent: "space-between",
  },
  content_title: {
    marginBottom: 10,
  },
  text_title: {
    fontFamily: fontFamily.Medium,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 13,
    lineHeight: 15.85,
    width: "100%",
    letterSpacing: -0.3,
    textAlign: "center",
  },
  content_item_one: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    paddingRight: 5,
    paddingTop: 19,
    // paddingBottom: -10,
  },
  content_value: {
    flexDirection: "row",
    alignItems: "center",
    // justifyContent: "center",
    marginTop: 10,
  },
  text_value: {
    fontFamily: fontFamily.Bold,
    color: "rgba(17, 40, 66, 1)",
    fontWeight: "700",
    fontSize: 18,
  },
  text_unity: {
    fontFamily: fontFamily.Bold,
    color: "rgba(17, 40, 66, 1)",
    // fontWeight: "700",
    fontSize: 18,
  },
  text_message: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 10,
    // fontWeight: "400",
    lineHeight: 14.6,
    fontFamily: fontFamily.Bold,
    fontStyle: "italic",
    // marginTop: 0,
  },
  text_message_value: {
    color: "rgba(17, 40, 66, 1)",
    fontSize: 10,
    // fontWeight: "400",
    lineHeight: 14.6,
    fontFamily: fontFamily.Bold,
    fontStyle: "italic",
  },
  content_message: {
    alignSelf: "flex-end",
    position: "relative",
  },
});
