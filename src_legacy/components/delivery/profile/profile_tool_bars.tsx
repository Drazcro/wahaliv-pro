import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import CaretLeft from "assets/image_svg/CaretLeft.svg";
import { fontFamily } from "src/theme/typography";
import { router } from "expo-router";

interface ProfileToolbarsProps {
  title: string;
  dontHaveGoBack?: boolean;
}

export function ProfileToolbar(props: ProfileToolbarsProps) {
  return (
    <View
      style={{
        ...styles.container,
        justifyContent: props.dontHaveGoBack ? "center" : "space-between",
      }}
    >
      {!props.dontHaveGoBack && (
        <TouchableOpacity style={styles.icon} onPress={() => router.back()}>
          <CaretLeft />
        </TouchableOpacity>
      )}
      <Text
        style={{
          ...styles.text_title,
          marginLeft: props.dontHaveGoBack ? 18 : 0,
        }}
      >
        {props.title}
      </Text>
      <Text></Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: 50,
    paddingBottom: 20,
    paddingHorizontal: 25,
    backgroundColor: "#fff",
  },

  icon: {},
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    lineHeight: 24.38,
    marginRight: 20,
    textAlign: "center",
  },
});
