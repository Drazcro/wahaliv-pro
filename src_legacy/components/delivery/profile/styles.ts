import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

export const ProfileStyle = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  header: {
    flex: 1,
    justifyContent: "center",
  },
  group_content: {
    flex: 3,
    marginTop: 25,
    marginHorizontal: 10,
  },
  wrapper: {
    flex: 1,
    flexDirection: "row",
    padding: 8,
    justifyContent: "space-between",
  },
  background_image: {
    position: "relative",
    justifyContent: "center",
    width: "100%",
    height: 210,
  },
  text_group: {
    position: "absolute",
    bottom: 45,
    marginHorizontal: 30,
    flexDirection: "column",
    justifyContent: "center",
  },
  text_name: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 20,
    lineHeight: 26,
  },
  text_email: {
    color: "#fff",
    fontSize: 16,
    lineHeight: 14.63,
    marginVertical: 8,
    fontFamily: fontFamily.regular,
  },
  contact: {
    flex: 1,
    justifyContent: "center",
    paddingVertical: 10,
    marginHorizontal: 10,
  },
  text_rappel: {
    fontSize: 16,
    color: "rgba(17, 40, 66, 1)",
    marginLeft: 5,
    lineHeight: 20,
    marginBottom: 5,
    fontFamily: fontFamily.regular,
  },
  phone_number: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 92, 133, 1)",
    marginRight: 5,
    fontSize: 16,
    lineHeight: 22,
  },
  group_accept_auto: {
    marginHorizontal: 15,
    justifyContent: "space-around",
    marginTop: 20,
  },
  accept_button: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
  },
  text_accept: {
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 19.5,
    color: "rgba(17, 40, 66, 1)",
  },
  text_auto: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(140, 140, 140, 1)",
    fontSize: 12,
    lineHeight: 14.6,
  },
  line: { marginVertical: 12 },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  progress_container: {
    flexDirection: "column",
    marginLeft: 20,
    marginRight: 25,
  },
  min_garanti: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(140, 140, 140, 1)",
    fontSize: 12,
    lineHeight: 14.6,
  },
  more: {
    fontFamily: fontFamily.regular,
    textDecorationLine: "underline",
  },
  groupe_value: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginLeft: 0,
    marginTop: 15,
    width: "100%",
  },
  text_service: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    lineHeight: 20,
  },
  text_min_garanti: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 18,
    lineHeight: 21.94,
  },
  text_rappelle: {
    fontFamily: fontFamily.regular,
    color: "rgba(17, 40, 66, 1)",
    fontSize: 14,
    lineHeight: 18,
  },
  progress_bar: {
    height: 18,
    width: "100%",
    borderRadius: 50,
    backgroundColor: "#E5E5E5",
    borderColor: "#E5E5E5",
    marginBottom: 10,
    marginTop: 2,
  },
  appVersionLabel: {
    width: "100%",
    textAlign: "center",
    color: "rgba(17, 40, 66, 1)",
    marginBottom: 15,
  },
  missingDocs: {
    backgroundColor: "#FEF1F1",
    marginHorizontal: 5,
    marginBottom: 5,
    borderRadius: 10,
    height: 110,
  },
  missingDocsTitle: {
    fontFamily: fontFamily.semiBold,
    marginLeft: 12,
    fontSize: 16,
    color: "rgba(29, 42, 64, 1)",
  },
  missingDocsText: {
    marginTop: 10,
    marginBottom: 20,
    marginHorizontal: 13,
  },
  missingDocsTextBold: {
    fontFamily: fontFamily.Bold,
    textDecorationStyle: "solid",
    textDecorationColor: "rgba(29, 42, 64, 1)",
    textDecorationLine: "underline",
    color: "rgba(29, 42, 64, 1)",
    lineHeight: 20,
    fontSize: 14,
  },
  NotePosition: {
    display: "flex",
    flexDirection: "row",
    marginHorizontal: 16,
    marginTop: 20,
  },
  missingDocsAction: {
    marginTop: -3,
  },
  informationsTitle: {
    fontFamily: fontFamily.Medium,
    fontWeight: "600",
    fontSize: 12,
    lineHeight: 15,
    color: "#8D98A0",
    paddingHorizontal: 7,
    marginVertical: 20,
    marginHorizontal: 5,
  },
});
