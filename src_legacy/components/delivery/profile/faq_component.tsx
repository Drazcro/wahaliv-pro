import React, { useEffect, useState } from "react";
import {
  Linking,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import FaqItemsCaretRight from "assets/image_svg/FaqItemsCaretRight.svg";
import { ScrollView } from "react-native-gesture-handler";
import { Divider } from "react-native-elements";
import Mail from "assets/image_svg/MailButton.svg";
import Phone from "assets/image_svg/PhoneButton.svg";
import i18n from "i18n-js";
import { IContact } from "src/interfaces/Entities";
import {
  get_partenaire_help_information,
} from "src_legacy/services/Loaders";
import { ActivityIndicator } from "react-native-paper";
import { router } from "expo-router";
import { authStore } from "src/services/auth_store";
import { ROLE_TYPE } from "src/services/types";
import { get_deliverymen_help_information } from "src/api/delivery/profile";

interface itemProps {
  text: string;
  onPress: (id: number, text: string) => void;
}

const FaqComponent = () => {
  const { base_user: user } = authStore();

  const [contactInformations, setContactInformations] = useState<IContact>();
  const [loading, setLoading] = useState<boolean>(false);


  const role = React.useMemo(() => {
    return user?.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0];
  }, [user]);

  const items: itemProps[] = [
    {
      text: "Je n’arrive pas à me positionner, je ne sais pas comment faire.",
      onPress: (id, text) => {
        router.push({ pathname: "/delivery/screens/help_screen", params: { id: "LOCATION" } });
      },
    },
    {
      text: "Je voudrais prévenir que je serai en retard",
      onPress: () => {
        router.push("/delivery/screens/chat_screen");
      },
    },
    {
      text: "Je ne pourrai pas assurer le service (en panne, problème familial,etc)",
      onPress: () => {
        router.push("/delivery/screens/chat_screen");
      },
    },
    {
      text: "Je suis sur place mais je n’arrive pas à joindre le client",
      onPress: () => {
        router.push("/delivery/screens/chat_screen");
      },
    },
    {
      text: "Je ne vois plus mes bonus ",
      onPress: (id, text) => {
        router.push({ pathname: "/delivery/screens/help_screen", params: { id: "BONUS" } });
      },
    },
    {
      text: "Je n’ai pas reçu mon/mes virement(s)",
      onPress: (id, text) => {
        router.push({ pathname: "/delivery/screens/help_screen", params: { id: "WIRE" } });
      },
    },
    {
      text: "J’ai un problème avec le nombre de kilomètres et la rémunération.",
      onPress: (id, text) => {
        router.push({ pathname: "/delivery/screens/help_screen", params: { id: "KM" } });
      },
    },
    {
      text: "Remontée/info client mécontent",
      onPress: () => {
        router.push("/delivery/screens/chat_screen");
      },
    },
    {
      text: "Remontée/infos sur restaurants mécontent",
      onPress: () => {
        router.push("/delivery/screens/chat_screen");
      },
    },
  ];

  function getDeliveryContactInformations() {
    if (user?.id) {
      setLoading(true);
      get_deliverymen_help_information()
        .then((response) => {
          setContactInformations(response);
        })
        .catch((err) => console.error(err))
        .finally(() => setLoading(false));
    }
  }

  function getPartenaireContactInformations() {
    if (user?.id) {
      setLoading(true);
      get_partenaire_help_information(user?.id)
        .then((response) => {
          setContactInformations(response);
        })
        .catch((err) => console.error(err))
        .finally(() => setLoading(false));
    }
  }

  useEffect(() => {
    if (role === ROLE_TYPE.ROLE_COURSIER) {
      console.log(contactInformations);
      getDeliveryContactInformations();
    } else {
      getPartenaireContactInformations();
    }
  }, []);

  const phone = React.useMemo(() => {
    const num = contactInformations?.mobile;
    return num?.replace(/\s/g, "") ?? "";
  }, []);

  return (
    <View style={styles.faqItems}>
      <View
        style={{
          ...styles.contactContainer,
          position: role !== ROLE_TYPE.ROLE_COURSIER ? "absolute" : "relative",
        }}
      >
        {loading ? (
          <View
            style={{ justifyContent: "center", alignItems: "center", flex: 1 }}
          >
            <ActivityIndicator size={"small"} />
          </View>
        ) : (
          <React.Fragment>
            <Text style={styles.nameTitle}>
              {contactInformations?.first_name}
            </Text>
            <Text style={styles.prod_text}>
              {role === ROLE_TYPE.ROLE_COURSIER
                ? i18n.t("profile.problem")
                : "Si vous avez une question, n'hésite pas à nous contacter."}
            </Text>
            <View style={styles.servicesContainer}>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(`mailto:${contactInformations?.email}`)
                }
              >
                <Mail />
              </TouchableOpacity>
              <TouchableOpacity onPress={() => Linking.openURL(`tel:${phone}`)}>
                <Phone />
              </TouchableOpacity>
            </View>
          </React.Fragment>
        )}
      </View>

      {role === ROLE_TYPE.ROLE_COURSIER ? (
        <SafeAreaView style={styles.container}>
          <ScrollView style={{ flexShrink: 1 }}>
            {items.map((item, index) => (
              <View key={index}>
                <View>
                  <View>
                    <Divider color="rgba(196, 196, 196, 1)" />
                  </View>
                  {/* <View style={styles.linksSeparators}></View> */}
                  <TouchableOpacity
                    style={styles.links}
                    onPress={() => item.onPress(index, item.text)}
                  >
                    <View style={styles.itemContainer}>
                      <Text style={styles.text}>{item.text}</Text>
                      <Text>
                        <FaqItemsCaretRight />
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
            ))}
          </ScrollView>
        </SafeAreaView>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "relative",
  },
  faqItems: {
    justifyContent: "space-evenly",
    alignItems: "flex-start",
    height: "85%",
    marginTop: 20,
    position: "relative",
  },
  text: {
    fontFamily: fontFamily.Medium,
    paddingRight: 20,
    marginRight: 10,
    //backgroundColor: "blue",
    fontSize: 14,
    lineHeight: 26,
  },
  itemContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    width: "100%",
  },
  links: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "flex-start",
    width: "90%",
    // backgroundColor: "red",
    margin: 20,
  },
  linksSeparators: {
    borderWidth: 1,
    borderBottomColor: "transparent",
    borderColor: "#E7E7E7",
    width: 380,
  },
  contactContainer: {
    width: "90%",
    height: 162,
    marginHorizontal: 20,
    marginBottom: 30,
    margin: 10,
    backgroundColor: "#FFFFFF",
    borderRadius: 10,
    top: 0,
    bottom: 0,
    elevation: 2,
  },

  androidShadow: {
    shadowColor: "rgba(0, 0, 0, 0.9)",
    shadowOffset: {
      width: 0,
      height: 0,
    },

    elevation: 2,
  },

  iosShadow: {
    shadowOffset: { width: 0, height: 0 },
    shadowColor: "rgba(0, 0, 0, 0.9)",
    shadowOpacity: 0.2,
    shadowRadius: 3,
  },

  nameTitle: {
    textAlign: "center",
    marginTop: 18,
    fontSize: 18,
    lineHeight: 22,
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
  },
  prod_text: {
    fontSize: 14,
    lineHeight: 20,
    textAlign: "center",
    color: "#8D98A0",
    marginTop: 6,
    marginBottom: 16,
    fontFamily: fontFamily.regular,
  },
  servicesContainer: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    // gap: 20,
    marginTop: 5,
    marginHorizontal: 20,
  },
});

export default FaqComponent;
