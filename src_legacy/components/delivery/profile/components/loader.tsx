import React from "react";
import { StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import { ActivityIndicator, MD2Colors } from "react-native-paper";

const Loader = () => {
  return (
    <View style={styles.loaderContainer}>
      <ActivityIndicator animating={true} color={MD2Colors.pink200} />
      <Text
        style={{
          textAlign: "center",
          marginTop: 20,
        }}
      >
        Chargement en cours
      </Text>
    </View>
  );
};

export default Loader;

const styles = StyleSheet.create({
  loaderContainer: {
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
});
