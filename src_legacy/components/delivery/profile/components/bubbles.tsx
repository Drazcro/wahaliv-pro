/* eslint-disable react/jsx-key */
import moment from "moment";
import React from "react";
import { StyleSheet, View } from "react-native";
import { Text } from "react-native";
import { IMessage } from "src_legacy/session/types";
import { fontFamily } from "src/theme/typography";

type BubblesProps = {
  msg: IMessage;
};

const Bubbles = ({ msg }: BubblesProps) => {
  return (
    <View style={styles.chatBody}>
      {msg.sent_by_client === false || msg.sent_by_user === true ? (
        <View
          style={[
            {
              backgroundColor: "#FF5C85",
              padding: 10,
              marginLeft: "45%",
              borderRadius: 5,
              marginTop: 25,
              marginRight: "5%",
              maxWidth: "70%",
              flex: 1,
              alignSelf: "flex-end",
            },
          ]}
        >
          <View style={{ flex: 1 }}>
            <Text style={styles.msgText}>{msg.content}</Text>
            <Text style={styles.date}>
              {moment(msg.sent_at).format("HH:mm:ss")}{" "}
              {moment(msg.sent_at).format("DD/MM/YYYY")}
            </Text>
          </View>

          <View style={styles.rightArrow}></View>
          <View style={styles.rightArrowOverlap}></View>
        </View>
      ) : (
        <View
          style={{
            backgroundColor: "rgba(217, 217, 217, 1)",
            padding: 10,
            borderRadius: 5,
            marginTop: 5,
            marginLeft: "5%",
            maxWidth: "70%",

            alignSelf: "flex-start",
          }}
        >
          <View>
            <Text style={styles.receivedMsg}>{msg.content}</Text>
            <Text style={[styles.date, { color: "rgba(141, 152, 160, 1)" }]}>
              {moment(msg.sent_at).format("HH:mm:ss")}{" "}
              {moment(msg.sent_at).format("DD/MM/YYYY")}
            </Text>
          </View>
          <View style={styles.leftArrow}></View>
          <View style={styles.leftArrowOverlap}></View>
        </View>
      )}
    </View>
  );
};

export default Bubbles;

const styles = StyleSheet.create({
  chatBody: {
    flex: 1,
    height: "100%",
    marginTop: 10,
  },
  rightArrow: {
    position: "absolute",
    backgroundColor: "#FF5C85",
    width: 20,
    height: 25,
    bottom: 0,
    borderBottomLeftRadius: 25,
    right: -11,
  },

  receivedMsg: {
    fontSize: 14,
    color: "rgba(29, 42, 64, 1)",
    justifyContent: "center",
    alignSelf: "flex-start",
    fontFamily: fontFamily.Medium,
    lineHeight: 16,
  },

  msgText: {
    fontSize: 14,
    fontFamily: fontFamily.Medium,
    color: "#fff",
    lineHeight: 16,
  },

  rightArrowOverlap: {
    position: "absolute",
    backgroundColor: "#fff",
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomLeftRadius: 18,
    right: -20,
  },

  /*Arrow head for recevied messages*/
  leftArrow: {
    position: "absolute",
    backgroundColor: "#dedede",
    width: 20,
    height: 25,
    bottom: 0,
    borderBottomRightRadius: 25,
    left: -11,
  },

  leftArrowOverlap: {
    position: "absolute",
    backgroundColor: "#fff",
    width: 20,
    height: 35,
    bottom: -6,
    borderBottomRightRadius: 18,
    left: -20,
  },

  date: {
    color: "#fff",
    fontSize: 12,
    fontStyle: "italic",
    lineHeight: 18,
    fontFamily: fontFamily.regular,
    marginTop: 1,
  },
});
