import React, { useMemo } from "react";
import { Text, View, StyleSheet } from "react-native";

import { Divider } from "react-native-elements";

import MapPinRed from "assets/image_svg/ChatMapPin.svg";
import ScooterRed from "assets/image_svg/ScooterArmand.svg";
import { fontFamily } from "src/theme/typography";

type Props = {
  client_location: string;
  command_time: string;
  command_id: string | undefined;
};

const ChatClientToolBar = ({
  client_location,
  command_time,
  command_id,
}: Props) => {
  
  const target_location = useMemo(() => {
    return client_location?.split(" ")[client_location?.split(" ").length - 1];
  }, [client_location]);

  return (
    <React.Fragment>
      <View style={styles.container}>

        <View
          style={{
            justifyContent: "center",
            alignItems: "flex-start",
          }}
        >
          {/* <Text style={styles.text}>Client: {client_name}</Text> */}
          <Text style={styles.caption}>Commande Nº {command_id}</Text>
        </View>

        
      </View>

      <View
        style={{
          flexDirection: "column-reverse",
          justifyContent: "flex-start",
          alignItems: "flex-start",
          marginHorizontal: 27,
          gap: 10,
          marginTop: 24,
          marginBottom: 9,
        }}
      >
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            gap: 11,
          }}
        >
          <MapPinRed height={24} width={24} />
          <Text
            style={{
              fontSize: 14,
              color: "#1D2A40",
              fontFamily: fontFamily.Medium,
              lineHeight: 17.07,
            }}
          >
            {target_location}
          </Text>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
            gap: 5.23,
          }}
        >
          <ScooterRed />
          <Text
            style={{
              lineHeight: 18,
              fontSize: 14,
              color: "#1D2A40",
              fontFamily: fontFamily.Medium,
            }}
          >
            Livraison: {command_time.replace(":", "h")}
          </Text>
        </View>
      </View>
      <Divider />
    </React.Fragment>
  );
};

export default ChatClientToolBar;


const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    justifyContent: "space-around",
    flexDirection: "row",
    alignItems: "center",
    gap: 20,
  },
  icon: {
    width: 34,
    height: 34,
    justifyContent: "center",
    alignItems: "center",
  },
  caption: {
    color: "#8D98A0",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    // marginRight: 20,
  },
  text: {
    fontFamily: fontFamily.semiBold,
    fontSize: 18.3,
    lineHeight: 24.38,
    color: "#1D2A40",
  },
});