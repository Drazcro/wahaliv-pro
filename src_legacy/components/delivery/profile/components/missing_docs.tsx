import React, { useState } from "react";
import { View, Text, TouchableOpacity } from "react-native";

import { ProfileStyle as styles } from "../styles";
import Note from "assets/image_svg/Note.svg";
import { get_deliveryman_docs } from "src_legacy/services/Loaders";
import { fontFamily } from "src/theme/typography";
import { router } from "expo-router";
//import * as Sentry from "@sentry/react-native";

export const MissingDocs = ({ setDoc }: { setDoc: (v: boolean) => void }) => {
  const [isDocOk, setIsDocOk] = useState(true);

  const load_or_reload_from_backend = async () => {
    try {
      const response = await get_deliveryman_docs();
      console.log(response);
      const status =
        response.contract === true &&
        response.identityRecto === true &&
        response.identityVerso === true &&
        response.registration_proof === true &&
        response.rib === true;
      setIsDocOk(status);
    } catch (e) {
      console.error(e);
      //Sentry.captureException(e);
    }
  };

  React.useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  React.useEffect(() => {
    setDoc(isDocOk);
  }, [isDocOk]);

  if (!isDocOk) {
    return (
      <View style={styles.missingDocs}>
        <View style={styles.NotePosition}>
          <Note />
          <Text style={styles.missingDocsTitle}>Documents manquants</Text>
        </View>
        <View style={styles.missingDocsText}>
          <Text
            style={{
              fontFamily: fontFamily.regular,
              color: "rgba(29, 42, 64, 1)",
              lineHeight: 20,
              fontSize: 14,
            }}
          >
            Des documents sont manquants
          </Text>

          <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <Text
              style={{
                fontFamily: fontFamily.regular,
                color: "rgba(29, 42, 64, 1)",
                lineHeight: 20,
                fontSize: 14,
              }}
            >
              Pour déclencher tes paiement
            </Text>
            <TouchableOpacity
              style={{ marginTop: 3 }}
              onPress={() => {
                router.push("/supporting_documents_screen");
              }}
            >
              <Text style={styles.missingDocsTextBold}> cliques ici</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
  return <></>;
};
