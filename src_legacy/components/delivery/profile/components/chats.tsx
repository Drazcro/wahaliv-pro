/* eslint-disable @typescript-eslint/ban-types */
import React from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Divider } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import { useChatStore } from "src_legacy/zustore/chatStore";

import useChat from "src_legacy/hooks/useChat";
import UserAvatar from "assets/image_svg/UserAvatar.svg";
import { ROLE_TYPE, getBaseUser } from "src_legacy/services/Utils";
import { encode } from "base-64";
import { ref, update } from "firebase/database";
import { db } from "firebase";
import moment from "moment";
import { router } from "expo-router";
import { authStore } from "src_legacy/services/v2/auth_store";

const DiscussionSPCard = () => {
  const { auth } = authStore();

  const { lastMessage, unreadCount } = useChat();

  const user = React.useMemo(() => {
    if (auth?.access_token) {
      return getBaseUser(auth);
    } else {
      return undefined;
    }
  }, [auth]);

  const getChatPath = React.useMemo(() => {
    const formattedString = `${user?.id}|-|;${user?.email}|-|;${
      user?.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0]
    }`;

    return encode(formattedString);
  }, [user]);

  const emptyCounter = () => {
    const dbRef = ref(db, `chat/${getChatPath}/chat_metadata/unread_for`);

    const updates = {
      user: 0,
    };

    update(dbRef, updates);
  };

  return (
    <View>
      <TouchableOpacity
        style={styles.conversationContainer}
        onPress={() => {
          router.push("/chat_screen");
          useChatStore.getState().emptyArray();
          emptyCounter();
        }}
      >
        <View style={{ position: "relative" }}>
          <UserAvatar />
          <View
            style={{
              width: 15,
              height: 15,
              backgroundColor: "rgba(245, 160, 82, 1)",
              borderRadius: 50,
              position: "absolute",
              right: 0,
              bottom: 0,
            }}
          ></View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
            width: "80%",
          }}
        >
          <View style={{ marginLeft: 20 }}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: fontFamily.semiBold,
                lineHeight: 19.5,
                color: "rgba(29, 42, 64, 1)",
              }}
            >
              Service client
            </Text>
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 14,
                lineHeight: 18,
                fontStyle: "italic",
                color: "rgba(141, 152, 160, 1)",
                marginTop: 3,
              }}
            >
              {lastMessage?.content}
            </Text>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Text
              style={{
                color: "rgba(141, 152, 160, 1)",
                fontFamily: fontFamily.regular,
                fontStyle: "italic",
                fontSize: 14,
                marginBottom: 4,
              }}
            >
              {moment(lastMessage?.sent_at).format("HH:mm").replace(":", "h")}
            </Text>
            {unreadCount > 0 ? (
              <View style={styles.counter}>
                <Text
                  style={{
                    textAlign: "center",
                    color: "#fff",
                    fontSize: 16.43,
                    fontFamily: fontFamily.Medium,
                  }}
                >
                  {unreadCount}
                </Text>
              </View>
            ) : null}
          </View>
        </View>
      </TouchableOpacity>
      <Divider width={1} />
    </View>
  );
};

export default DiscussionSPCard;

const styles = StyleSheet.create({
  conversationContainer: {
    paddingVertical: 10,
    marginLeft: 22,
    flexDirection: "row",
    alignItems: "center",
  },
  counter: {
    backgroundColor: "rgba(255, 92, 133, 1)",
    width: 23,
    height: 23,
    borderRadius: 50,
  },
});
