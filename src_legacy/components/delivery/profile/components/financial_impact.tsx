import { StyleSheet, Text, View, ScrollView } from "react-native";
import { Calendar } from "./calendar";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-elements";

import React, { useState } from "react";
import { getBaseUser } from "src_legacy/services/Utils";
import { IFinancialImpact, IHistory } from "src/interfaces/Entities";
import moment from "moment";
import { ActivityIndicator } from "react-native-paper";
//import * as Sentry from "@sentry/react-native";
import { vanillaAuthState } from "src_legacy/services/v2/auth_store";
import { get_financial_history } from "src/api/delivery/profile";

type HistoryProps = {
  history: IHistory;
  index: number;
};

const Historique = ({ history, index }: HistoryProps) => {
  const getInterval = React.useMemo(() => {
    if (!history.description.includes("(")) {
      return "";
    }
    const opening_parenthesis_index = history.description.indexOf("(");
    const closing_parenthesis_index = history.description.indexOf(")");
    return history.description
      .substring(opening_parenthesis_index, closing_parenthesis_index + 1)
      .replace(" ", "")
      .replace(" - ", "-")
      .toLowerCase();
  }, [history.description]);
  return (
    <View style={styles.historiqueContainer}>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: 10,
          marginTop: 18,
        }}
      >
        <View
          style={{
            flexDirection: "column",
            justifyContent: "space-between",
            alignItems: "flex-start",
          }}
        >
          <Text
            adjustsFontSizeToFit
            style={{
              fontFamily: fontFamily.Medium,
              lineHeight: 22,
              fontSize: 16,
              color: "rgba(29, 42, 64, 1)",
            }}
          >
            {history.description.split("(")[0]}
          </Text>
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text
              style={{
                color: "rgba(29, 42, 64, 1)",
                fontSize: 14,
                fontFamily: fontFamily.Medium,
                lineHeight: 22,
                marginTop: 8,
              }}
            >
              {getInterval}{" "}
            </Text>
            <Text
              style={{
                color: "rgba(141, 152, 160, 1)",
                fontSize: 14,
                fontFamily: fontFamily.Medium,
                lineHeight: 22,
                marginTop: 8,
              }}
            >
              {moment(history.datetime.date).format("DD/MM/YY")} à{" "}
              {history.hour.replace(":", "h")} {history.commande_id}
            </Text>
          </View>
        </View>
        <Text
          style={{
            color: "rgba(29, 42, 64, 1)",
            fontSize: 16,
            fontFamily: fontFamily.Medium,
            lineHeight: 22,
          }}
        >
          -{history.penalty}% bonus
        </Text>
      </View>
      <View>
        <Divider color="rgba(196, 196, 196, 1)" />
      </View>
    </View>
  );
};

const DEFAULT_FINANCIAL_IMPACT = {
  base_bonus: 0,
  total_bonus: 0,
  month_penalty: 0,
  historique: [],
} as IFinancialImpact;
const FinancialImpact = () => {
  const [month, setMonth] = useState(moment().month()); // 5 represents June (months are zero-based)
  const [year, setYear] = useState(moment().year());
  const [selectedMonth, setSelectedMonth] = useState("");
  const { getState: getAuth } = vanillaAuthState;
  const [data, setData] = useState<IFinancialImpact>(DEFAULT_FINANCIAL_IMPACT);
  const [isLoading, setIsLoading] = useState(false);

  const { auth } = getAuth();

  const base_user = getBaseUser(auth);
  let monthNumberValue: string;

  function handleHistoryFetching() {
    setIsLoading(true);
    if (month < 9) {
      monthNumberValue = `0${(month + 1).toString()}`;
    } else {
      monthNumberValue = month.toString();
    }
    get_financial_history(year.toString(), monthNumberValue)
      .then((data) => {
        setData(data);
        setTimeout(() => {
          setIsLoading(false);
        }, 1000);
      })
      .catch((err) => {
        console.error(err);
        //Sentry.captureException(err);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }
  if (isLoading) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <ActivityIndicator animating />
      </View>
    );
  }

  return (
    <View
      style={{
        marginTop: 50,
        backgroundColor: "white",
        flex: 1,
      }}
    >
      <Calendar
        {...{
          month,
          setMonth,
          year,
          setYear,
          setSelectedMonth,
          selectedMonth,
          handleHistoryFetching,
        }}
      />
      <View style={{ flexDirection: "row", width: "100%" }}>
        <View style={styles.bonusContainer}>
          <View style={styles.informationContainer}>
            <Text style={styles.text}>Bonus de base</Text>
            <Text style={styles.amount}>{data.base_bonus} €</Text>
          </View>
          <View style={styles.informationContainer}>
            <Text style={styles.text}>Pénalités du mois</Text>
            <Text style={styles.amount}>{data.month_penalty} %</Text>
          </View>
          <View style={styles.informationContainer}>
            <Text style={styles.totalTitle}>Total bonus du mois</Text>
            <Text
              style={{
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
                lineHeight: 22,
              }}
            >
              {data?.total_bonus} €
            </Text>
          </View>
        </View>
      </View>
      <Text style={styles.historiqueTitle}>HISTORIQUE</Text>
      <View style={{ flex: 1, width: "100%" }}>
        <ScrollView
          style={{
            marginHorizontal: 18,
          }}
        >
          {data.historique.length > 0
            ? data.historique.map((item, index) => (
                <Historique history={item} index={index} key={index} />
              ))
            : null}
        </ScrollView>
      </View>
    </View>
  );
};

export default FinancialImpact;

const styles = StyleSheet.create({
  bonusContainer: {
    backgroundColor: "rgba(247, 247, 247, 1)",
    borderRadius: 15,
    height: 150,
    marginHorizontal: 16,
    marginVertical: 30,
    justifyContent: "center",
    alignItems: "center",
    color: "rgba(29, 42, 64, 1)",
    padding: 18,
    flex: 1,
  },
  informationContainer: {
    flexDirection: "row",
    width: "100%",
    justifyContent: "space-between",
    alignItems: "center",
  },
  text: {
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 22,
    marginVertical: 12,
  },
  amount: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 19.5,
    color: "rgba(17, 40, 66, 1)",
  },
  totalTitle: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 16,
    lineHeight: 22,
    marginVertical: 12,
  },
  historiqueContainer: {},
  historiqueTitle: {
    margin: 19,
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
    lineHeight: 22,
    marginBottom: 20,
  },
});
