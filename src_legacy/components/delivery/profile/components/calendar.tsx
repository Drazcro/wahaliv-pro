import React, { useState, useMemo, useEffect } from "react";
import { Text, TouchableOpacity, View, SafeAreaView } from "react-native";
import CaretLeft from "assets/image_svg/CaretLeftBlue.svg";
import CaretRight from "assets/image_svg/CaretRightBlue.svg";
import { fontFamily } from "src/theme/typography";
import { Divider } from "react-native-elements";
import { Overlay } from "react-native-elements";
import { SelectList } from "react-native-dropdown-select-list";
import moment from "moment";
import CloseBtn from "assets/image_svg/X.svg";

type CalendarProps = {
  month: number;
  year: number;
  selectedMonth: string;

  setMonth: React.Dispatch<React.SetStateAction<number>>;
  setYear: React.Dispatch<React.SetStateAction<number>>;
  setSelectedMonth: React.Dispatch<React.SetStateAction<string>>;
  handleHistoryFetching: () => void;
};

export const Calendar = (props: CalendarProps) => {
  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const years = useMemo(() => {
    const currentYear = new Date().getFullYear();
    const years = [];
    for (let i = 2015; i <= currentYear; i++) {
      years.push(i);
    }
    return years;
  }, []);

  const monthNames: string[] = [
    "Janvier",
    "Février",
    "Mars",
    "Avril",
    "Mai",
    "Juin",
    "Juillet",
    "Aout",
    "Septembre",
    "Octobre",
    "Novembre",
    "Décembre",
  ];

  const prevMonth = () => {
    if (props.month === 0) {
      props.setMonth(11);
      props.setYear(props.year - 1);
    } else {
      props.setMonth(props.month - 1);
    }
  };

  const nextMonth = () => {
    if (props.month === 11) {
      props.setMonth(0);
      props.setYear(props.year + 1);
    } else {
      props.setMonth(props.month + 1);
    }
  };

  useEffect(() => {
    console.log(moment(props.month).month());
  }, [props.month]);

  return (
    <SafeAreaView>
      <View>
        <Overlay
          isVisible={visible}
          onBackdropPress={toggleOverlay}
          overlayStyle={{
            padding: 30,
            width: "80%",
            borderRadius: 15,
            position: "relative",
          }}
        >
          <View
            style={{
              marginBottom: 10,
              position: "relative",
            }}
          >
            <TouchableOpacity
              style={{ position: "absolute", top: -15, right: -15 }}
              onPress={() => setVisible(false)}
            >
              <CloseBtn />
            </TouchableOpacity>
            <Text
              style={{
                color: "rgba(29, 42, 64, 1)",
                fontFamily: fontFamily.semiBold,
                textAlign: "center",
                fontSize: 16,
                lineHeight: 22,
              }}
            >
              Sélectionnez une date
            </Text>
          </View>

          {/* DropDown Select for Months */}
          <SelectList
            setSelected={(val: string) => {
              const index = monthNames.indexOf(val);
              props.setMonth(index);
              if (index < 10) {
                props.setSelectedMonth(`0${(index + 1).toString()}`);
              } else {
                props.setSelectedMonth((index + 1).toString());
              }
            }}
            data={monthNames}
            save="value"
            defaultOption={{
              key: monthNames[props.month],
              value: monthNames[props.month],
            }}
            search={false}
            boxStyles={{
              marginTop: 10,
              borderColor: "1px solid rgba(196, 196, 196, 1)",
              height: 44,
            }}
            dropdownStyles={{
              marginVertical: 20,
            }}
          />

          {/* DropDown Select for Years */}

          <SelectList
            setSelected={(val: string) => props.setYear(Number(val))}
            data={years}
            save="value"
            search={false}
            defaultOption={{ key: props.year.toString(), value: props.year }} //default selected option
            boxStyles={{
              marginTop: 25,
              borderColor: "1px solid rgba(196, 196, 196, 1)",
              height: 44,
            }}
          />

          <TouchableOpacity
            style={{
              backgroundColor: "rgba(255, 92, 133, 1)",
              width: "100%",
              padding: 10,
              marginTop: 37,
              borderRadius: 50,
            }}
            onPress={() => {
              props.handleHistoryFetching();
              setVisible(false);
            }}
          >
            <Text
              style={{
                textAlign: "center",
                color: "white",
                fontFamily: fontFamily.Medium,
              }}
            >
              Appliquer
            </Text>
          </TouchableOpacity>
        </Overlay>
      </View>
      <TouchableOpacity
        style={{
          marginTop: -20,
          flexDirection: "row",
          justifyContent: "space-evenly",
          alignItems: "center",
          marginHorizontal: 40,
          marginBottom: 15,
        }}
        onPress={toggleOverlay}
      >
        <TouchableOpacity onPress={prevMonth}>
          <CaretLeft />
        </TouchableOpacity>
        <Text
          style={{
            fontFamily: fontFamily.semiBold,
            color: "rgba(29, 42, 64, 1)",
            fontSize: 18,
          }}
        >{`${monthNames[props.month]} ${props.year}`}</Text>
        <TouchableOpacity onPress={nextMonth}>
          <CaretRight />
        </TouchableOpacity>
      </TouchableOpacity>
      <View>
        <Divider color="rgba(196, 196, 196, 1)" width={1} />
      </View>
    </SafeAreaView>
  );
};
