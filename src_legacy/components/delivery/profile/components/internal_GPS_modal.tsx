import React from "react";
import {
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import { Overlay } from "react-native-elements";

type Props = {
  open: boolean;
  setOpen: (open: boolean) => void;
};

const InternalGPSModal = ({ open, setOpen }: Props) => {
  return (
    <Overlay
      visible={open}
      overlayStyle={{
        width: "100%",
        height: "100%",
      }}
      onRequestClose={() => setOpen(false)}
      isVisible={false}
    >
      <TouchableWithoutFeedback onPress={() => setOpen(false)}>
        <View style={{ flex: 1 }}>
          <Text
            style={{ textAlign: "center", flex: 1, top: 300, fontSize: 16 }}
          >
            Fonctionnalité non encore intégrée !
          </Text>
          <TouchableOpacity
            onPress={() => setOpen(false)}
            style={{
              backgroundColor: "#CD5C5C",
              padding: 10,
            }}
          >
            <Text
              style={{
                color: "#FFFFFF",
                textAlign: "center",
              }}
            >
              Close
            </Text>
          </TouchableOpacity>
        </View>
      </TouchableWithoutFeedback>
    </Overlay>
  );
};

export default InternalGPSModal;
