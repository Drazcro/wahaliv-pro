import React, { useEffect, useState } from "react";
import {
  Linking,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import CaretRightLink from "assets/image_svg/CaretRightLink.svg";
import { profileStore } from "src_legacy/zustore/profilestore";

export const FAQLocation = ({ title }: { title: string }) => {
  const instructions = React.useMemo(() => {
    return [
      "1- Sur l’app aller sur l’onglet Planning",
      "2- Sélectionner les jours et créneaux sur lesquels je souhaite travailler",
      "3-Enregistrer les modifications",
    ];
  }, []);
  return (
    <React.Fragment>
      <View style={styles.container}>
        <Text
          style={{
            fontFamily: fontFamily.semiBold,
            fontSize: 16,
            lineHeight: 22,
            marginTop: 20,
          }}
        >
          {title}
        </Text>
        <View style={styles.instructions}>
          {instructions.map((instruction, index) => (
            <View key={index}>
              <Text
                style={{
                  fontFamily: fontFamily.regular,
                  paddingHorizontal: 2,
                }}
              >
                {instruction}
              </Text>
            </View>
          ))}
        </View>
        <TouchableOpacity style={styles.linkBtn}>
          <Text style={styles.link}>Regarder la vidéo tutorielle</Text>
          <CaretRightLink />
        </TouchableOpacity>
      </View>
    </React.Fragment>
  );
};

type FAQWithEmailProps = {
  title: string;
  requirements?: string[];
  object: string;
  body: string;
};

const FAQWithEmail = ({
  title,
  requirements = [],
  object,
  body,
}: FAQWithEmailProps) => {
  const profile = profileStore((v) => v.profile);

  const emailLink = React.useMemo(() => {
    return `mailto:prod@sacrearmand.com?&subject=${object}&body=${body}`;
  }, [profile]);

  useEffect(() => {
    console.log(profile);
  }, [profile]);

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      {/* <View style={styles.instructions}>
        <Text
          style={{
            lineHeight: 20,
            fontFamily: fontFamily.regular,
          }}
        >
          Envoyer un mail à{" "}
          <Text style={{ fontWeight: "bold" }}>prod@sacrearmand.com</Text> avec
          objet:{" "}
          <Text style={{ fontWeight: "500" }}>
            « Je ne vois plus mes bonus »
          </Text>{" "}
          et dans corps du mail pre remplir ces informations:
        </Text>
        <View style={styles.requirement}>
          {["Nom et prénom: ", "Ville: ", ...requirements].map(
            (requirement, index) => (
              <Text style={styles.requirementText}>{`· ${requirement}`}</Text>
            ),
          )}
        </View>
      </View> */}
      <View
        style={{
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          marginTop: 300,
        }}
      >
        <TouchableOpacity
          style={styles.emailBtn}
          // disabled={disabled}
          onPress={() => {
            Linking.openURL(emailLink);
          }}
        >
          <Text style={styles.emailBtnText}>Envoyer un mail</Text>
        </TouchableOpacity>
        <Text
          style={{
            marginTop: 35,
            fontFamily: fontFamily.regular,
            textAlign: "center",
            color: "#000",
            fontSize: 16,
            width: "85%",
            lineHeight: 24,
          }}
        >
          Tu recevras une réponse par email sous 2 jours ouvrés
        </Text>
      </View>
    </View>
  );
};

export const FAQBonus = ({
  title,
  object,
  body,
}: {
  title: string;
  object: string;
  body: string;
}) => {
  return <FAQWithEmail title={title} object={object} body={body} />;
};

export const FAQWire = ({
  title,
  object,
  body,
}: {
  title: string;
  object: string;
  body: string;
}) => {
  return (
    <FAQWithEmail
      title={title}
      requirements={["Paiement montant attendu"]}
      object={object}
      body={body}
    />
  );
};

export const FAQKilometrage = ({
  title,
  object,
  body,
}: {
  title: string;
  object: string;
  body: string;
}) => {
  return (
    <FAQWithEmail
      title={title}
      requirements={["Km réel (Google Maps)", "N° de la commande concernée"]}
      object={object}
      body={body}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: "5%",
    paddingHorizontal: 20,
  },
  title: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 16,
    lineHeight: 26,
    marginTop: 20,
  },
  instructions: {
    marginTop: "5%",
    justifyContent: "space-between",
    alignContent: "center",
    height: "35%",
  },
  linkBtn: {
    marginTop: "10%",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  link: {
    color: "#FF5C85",
    textDecorationLine: "underline",
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    marginRight: 10,
  },
  requirement: {
    justifyContent: "space-between",
    alignItems: "flex-start",
    marginLeft: 10,
    marginTop: 10,
  },
  requirementText: {
    fontFamily: fontFamily.Medium,
    marginTop: 5,
  },
  emailBtn: {
    alignSelf: "center",

    backgroundColor: "#FF5C85",
    width: "100%",
    height: 52,
    borderRadius: 30,
  },
  emailBtnText: {
    color: "#ffff",
    textAlign: "center",
    paddingTop: 15,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
  },
  informationContainer: {
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
});
