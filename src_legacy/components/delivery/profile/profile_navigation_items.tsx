import React from "react";
import i18n from "i18n-js";
import * as Linking from "expo-linking";

import User from "assets/image_svg/UserThin.svg";
import CaretRight from "assets/image_svg/CaretRight.svg";
import Lock from "assets/image_svg/Lock.svg";
import CreditCard from "assets/image_svg/CreditCard.svg";
import Receipt from "assets/image_svg/Receipt.svg";
import Reverses from "assets/image_svg/Reverses.svg";
import ClipboardText from "assets/image_svg/ClipboardText.svg";
import SignOut from "assets/image_svg/SignOut.svg";
import Bug from "assets/image_svg/Bug.svg";
import FAQ from "assets/image_svg/FAQ.svg";
import Politique from "assets/image_svg/Politique.svg";
import Chat from "assets/image_svg/ChatIcon.svg";

import { clearAuth } from "src_legacy/session/store";

import { ProfileNavigation } from "./profile_navigation";
import { unregisterForServiceStatusCheck } from "src/cron/delivery_men_service_check";
import { unregisterBackgroundFetchAsync } from "src/cron/foreground_localisation_task";
import { Platform, Text, View } from "react-native";
import NoteAttention from "assets/image_svg/NoteAttention.svg";
import { fontFamily } from "src/theme/typography";
import useChat from "src_legacy/hooks/useChat";
import { router } from "expo-router";
import { authStore } from "src_legacy/services/v2/auth_store";

function ChatIcon() {
  const { unreadCount } = useChat();

  return (
    <View style={{ position: "relative" }}>
      <Chat />
      {unreadCount > 0 ? (
        <View
          style={{
            position: "absolute",
            right: 0,
            top: 0,
            backgroundColor: "rgba(255, 92, 133, 1)",
            width: 14,
            height: 14,
            borderRadius: 20,
          }}
        >
          <Text
            style={{
              textAlign: "center",
              color: "#fff",
              fontSize: 10,
              fontFamily: fontFamily.Medium,
            }}
          >
            {unreadCount}
          </Text>
        </View>
      ) : null}
    </View>
  );
}

export function ProfileNavigationItems(props: {
  isSalarie: boolean;
  docIsOk: boolean;
}) {
  const { removeAuth } = authStore();

  const navigation_options = [
    {
      iconLeft: <FAQ />,
      title: i18n.t("profile.Help"),
      onPress: () => {
        router.push("/faq_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <ChatIcon />,
      title: "Chat",
      onPress: () => {
        router.push("/inbox_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <Bug />,
      title: i18n.t("profile.bug"),
      onPress: () => {
        router.push("/bug_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <User />,
      title: i18n.t("profile.userInfo"),
      onPress: () => {
        router.push("/information_profile_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <Lock />,
      title: i18n.t("profile.changePassword"),
      onPress: () => {
        router.push("/update_password_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <Reverses />,
      title: i18n.t("profile.Receipt"),
      onPress: () => {
        router.push("/reverse_earnings_screen");
      },
      iconRight: <CaretRight />,
    },
    props.docIsOk
      ? {
          iconLeft: <ClipboardText />,
          title: i18n.t("profile.supportingDocuments"),
          onPress: () => {
            router.push("/supporting_document_screen");
          },
          iconRight: <CaretRight />,
        }
      : {
          iconLeft: <NoteAttention />,
          title: i18n.t("profile.supportingDocuments"),
          onPress: () => {
            router.push("/supporting_documents_screen");
          },
          iconRight: <CaretRight />,
        },
    {
      iconLeft: <CreditCard />,
      title: i18n.t("profile.coordonneBank"),
      onPress: () => {
        router.push("/bank_details_screen");
      },
      iconRight: <CaretRight />,
    },
    {
      iconLeft: <Receipt />,
      title: i18n.t("profile.bill"),
      iconRight: <CaretRight />,
      onPress: () => {
        router.push("/invoice_screen");
      },
    },
    {
      iconLeft: <Politique />,
      title: i18n.t("signIn.privacyPolicy"),
      iconRight: <CaretRight />,
      onPress: () => {
        Linking.openURL(
          "https://sacrearmand.com/sacre-armand-pro/privacy-policy",
        );
      },
    },
    {
      iconLeft: <SignOut />,
      title: i18n.t("profile.logout"),
      onPress: () => {
        if (Platform.OS !== "web") {
          unregisterForServiceStatusCheck();
          unregisterBackgroundFetchAsync();
        }
        removeAuth();
        clearAuth();
      },
    },
  ];

  return (
    <>
      {navigation_options.map((item, index) => (
        <ProfileNavigation
          key={index}
          iconLeft={item.iconLeft}
          iconRight={item.iconRight}
          title={item.title}
          onPress={() => item.onPress()}
          isSalarie={props.isSalarie}
          index={index}
        />
      ))}
    </>
  );
}
