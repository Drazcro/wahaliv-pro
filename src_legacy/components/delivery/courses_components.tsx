import React from "react";
import { Text, View } from "react-native";
import { COURSE_STATUS } from "src/constants/course";
import { ICourse } from "src/interfaces/Entities";

import { Divider } from "react-native-elements/dist/divider/Divider";


import { Dimensions, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

import ClockDark from "assets//image_svg/ClockDark.svg";
import Money from "assets/image_svg/Money.svg";
import Path from "assets/image_svg/Path.svg";
import Scooter from "assets/image_svg/ScooterBlack.svg";
import CurrencyEur from "assets/image_svg/CurrencyEur.svg";
import { IBaseUser } from "src/services/types";
import { convert_point_to_comma, dateToDay, dateToTime, point_and_comma } from "src/services/helpers";

const { width } = Dimensions.get("window");
///////////////////////////////// Address /////////////////////////////////

export const CheckDeliveryComplementAddress = ({
  complementadresse,
}: {
  complementadresse: string;
}) => {
  if (complementadresse != null) {
    return <Text style={styles.title}>{complementadresse}</Text>;
  } else return null;
};

export const CheckHowToDisplayInfoCourse = ({
  course: _course,
  baseUser,
}: {
  course: ICourse;
  baseUser: IBaseUser | undefined;
}) => {
  if (_course.delivery_status == COURSE_STATUS.RECOVERED) {
    return <DeliveredDisplay course={_course} tips={_course.tip} />;
  } else if (_course.delivery_status == COURSE_STATUS.ACCEPTED) {
    return (
      <RecoveredDisplay
        course={_course}
        tips={_course.tip}
        baseUser={baseUser}
      />
    );
  } else if (_course.delivery_status == COURSE_STATUS.FINISHED) {
    return <FinishedDisplay course={_course} />;
  }
  return <></>;
};

export const displayTips = (tips: number) => {
  const newPrice = tips.toString().replace(",", "");
  if (parseInt(newPrice) !== 0) {
    return (
      <Text style={styles.tipText}>+ Pourboire {point_and_comma(tips)} €</Text>
    );
  } else {
    return <View />;
  }
};

export const DeliveredDisplay = ({
  course: _course,
  tips,
}: {
  course: ICourse;
  tips: number;
}) => {
  return (
    <View>
      <View style={styles.wrapperOne}>
        <View style={styles.columnCenter}>
          <View style={styles.pic}>
            <Path />
            <Text style={styles.textOne}>Distance</Text>
          </View>
          <View>
            <Text style={styles.textTwo}>
              {convert_point_to_comma(_course?.delivery_distance)} km
            </Text>
          </View>
        </View>
        <View style={styles.columnCenter}>
          <View style={styles.pic}>
            <Scooter height={19} />
            <Text style={styles.textOne}>Livrer à</Text>
          </View>
          <View>
            <Text style={styles.textTwo}>
              {dateToTime(_course?.delivery_date?.date)}
            </Text>
          </View>
        </View>
        <View>
          <View style={styles.pic}>
            <CurrencyEur />
            <Text style={styles.textOne}>Gains</Text>
          </View>
          <View>
            <Text style={styles.textTwo}>
              {point_and_comma(_course?.delivery_amount)} €
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.tipPosition}>
        <View style={styles.tip}>{displayTips(tips)}</View>
      </View>
    </View>
  );
};

export const RecoveredDisplay = ({
  course: _course,
  tips,
  baseUser,
}: {
  course: ICourse;
  tips: number;
  baseUser: IBaseUser | undefined;
}) => {
  return (
    <View>
      <View style={styles.wrapperOne}>
        <View style={styles.columnCenter}>
          <View style={styles.wrapper}>
            <ClockDark />
            <Text style={styles.textOne}>Collecter</Text>
          </View>
          <View>
            <Text style={styles.textTwo}>
              <Text style={styles.dateNow}>{dateToDay(_course.pickup_date.date)} à</Text>{" "}
              {dateToTime(_course?.pickup_date?.date)}
            </Text>
          </View>
        </View>
        <View style={styles.columnCenter}>
          <View style={styles.pic}>
            <Scooter height={19} />
            <Text style={styles.textOne}>Livrer</Text>
          </View>
          <View>
            <Text style={styles.textTwo}>
              <Text style={styles.dateNow}>{dateToDay(_course.pickup_date.date)} à </Text>
              {dateToTime(_course?.delivery_date?.date)}
            </Text>
          </View>
        </View>
      </View>
      <Divider style={styles.divider} />
      <View style={styles.wrapperRecovered}>
        <View style={styles.subWrapper}>
          <Path />
          <Text style={styles.distance}>
            Distance de la course{" "}
            <Text style={styles.text1}>
              {convert_point_to_comma(_course.delivery_distance)} Km
            </Text>
          </Text>
        </View>
        {baseUser?.isSalarie && (
          <>
            <View style={styles.subWrapper}>
              <Money />
              <Text style={styles.distance}>
                Gains de la course{" "}
                <Text style={styles.text2}>
                  {point_and_comma(_course.delivery_amount)} €
                </Text>
              </Text>
            </View>
            <View style={styles.tip}>{displayTips(tips)}</View>
          </>
        )}
        {!baseUser?.isSalarie && (
          <>
            <View style={styles.subWrapper}>
              <Money />
              <Text style={styles.distance}>
                Gains de la course{" "}
                <Text style={styles.text2}>
                  {point_and_comma(_course.delivery_amount)} €
                </Text>
              </Text>
            </View>
            <View style={styles.tip}>{displayTips(tips)}</View>
          </>
        )}
      </View>
    </View>
  );
};

export const FinishedDisplay = ({ course: _course }: { course: ICourse }) => {
  return (
    <View style={styles.wrapperOne}>
      <View style={styles.columnCenter}>
        <View style={styles.pic}>
          <Text style={styles.textOne}>DISTANCE</Text>
        </View>
        <View>
          <Text style={styles.textTwo}>
            {convert_point_to_comma(_course?.delivery_distance)} km
          </Text>
        </View>
      </View>
      <View style={styles.columnCenter}>
        <View style={styles.pic}>
          <Text style={styles.textOne}>GAIN</Text>
        </View>
        <View>
          <Text style={styles.textTwo}>
            {point_and_comma(_course?.delivery_amount)} €
          </Text>
        </View>
      </View>
      <View style={styles.grid}>
        <Text style={styles.textOne}>POURBOIRE</Text>
        <View>
          <Text style={styles.textTwo}>2,00 €</Text>
        </View>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  wrapperOne: {
    flexDirection: "row",
    justifyContent: "space-around",
    marginTop: 20,
    paddingVertical: 10,
  },

  textOne: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 5,
  },

  textTwo: {
    fontFamily: fontFamily.Bold,
    fontSize: 18,
    lineHeight: 21.94,
    color: "#1D2A40",
  },

  dateNow: {
    textAlign: "justify",
    fontFamily: fontFamily.Medium,
    color: "#000000",
    fontSize: 13,
    lineHeight: 22,
  },

  divider: {
    borderWidth: 1,
    borderColor: "#C4C4C4",
  },

  pic: {
    flexDirection: "row",
  },

  grid: {
    alignItems: "center",
    lineHeight: 0,
  },

  wrapperRecovered: {
    backgroundColor: "#F4F4F4",
    padding: 15,
    position: "relative",
  },

  subWrapperOne: {
    marginBottom: 35,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    paddingRight: 30,
    left: width * 0.01,
  },

  columnCenter: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },

  title: {
    //textAlign: 'justify',
    fontFamily: fontFamily.Medium,
    color: "#000000",
    fontSize: 14,
    lineHeight: 22,
  },

  tip: {
    backgroundColor: "#E9C852",
    width: 125,
    borderRadius: 5,
    paddingHorizontal: 5,
  },
  tipText: {
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 14.63,
  },

  tipPosition: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginRight: 33,
    marginTop: -5,
    marginBottom: 5,
  },
  wrapper: {
    flexDirection: "row",
  },

  subWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5,
    marginLeft: 10,
  },

  distance: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    paddingLeft: 10,
  },

  text1: {
    fontFamily: fontFamily.Bold,
    marginLeft: 20,
    color: "#112842",
    fontSize: 14,
  },

  text2: {
    fontFamily: fontFamily.Bold,
    marginLeft: 10,
    color: "#112842",
    fontSize: 14,
  },
});
