import {
  IPlanningSummaryData,
  IPlanningSummary,
} from "src/interfaces/Entities";
import moment from "moment";
import React from "react";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import { capitalize_first_letter } from "src_legacy/services/Utils";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export function DaysItems(props: {
  item: IPlanningSummary;
  setDate: (arg0: Date) => void;
  setIndex: (arg0: number) => void;
}) {
  const update = () => {
    props.setDate(new Date(props.item.day));
    props.setIndex(0);
  };
  return (
    <View style={styles.container}>
      <View style={styles.space_day}>
        <View
          style={{ width: "75%", display: "flex", flexDirection: "column" }}
        >
          <Text style={styles.text_day}>
            {capitalize_first_letter(
              moment(props?.item?.day).format("dddd D MMMM"),
            )}
          </Text>
          <Text style={styles.text_day}>({props.item.total_hour} heures)</Text>
        </View>
        <TouchableOpacity style={styles.button} onPress={update}>
          <Text style={styles.text_modify}>Modifier</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.space_periode}>
        <Text style={styles.text_period}>PÉRIODE D’ACTIVITÉ</Text>
      </View>
      {props?.item?.data?.map((item: IPlanningSummaryData, index: number) => (
        <View style={styles.space_hours_one} key={index}>
          <View style={{ width: "75%" }}>
            <Text style={styles.text_hours}>
              {item.start_time} à {item.end_time}
            </Text>
          </View>
          <View style={{ width: "25%", alignItems: "flex-start" }}>
            <Text style={styles.text_hours}>{item.hour} heures</Text>
          </View>
        </View>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
  },
  text_day: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 16,
    lineHeight: 19.5,
  },
  text_period: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(105, 109, 117, 1)",
    fontSize: 12,
    lineHeight: 14.63,
  },
  text_hours: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 17.07,
  },
  text_modify: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 92, 133, 1)",
    fontSize: 12,
    lineHeight: 20,
  },
  space_day: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    paddingRight: 24,
    paddingLeft: 25,
  },
  space_periode: {
    justifyContent: "flex-start",
    marginTop: 16,
    marginBottom: 13,
    paddingRight: 24,
    paddingLeft: 25,
  },
  space_hours_one: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 14,
    paddingRight: 24,
    paddingLeft: 25,
  },
  space_hours_two: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingRight: 24,
    paddingLeft: 25,
    marginBottom: 18,
  },
  button: {
    borderWidth: 1,
    alignItems: "center",
    width: "25%",
    height: 27,
    borderColor: color.ACCENT,
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderRadius: 30,
  },
});
