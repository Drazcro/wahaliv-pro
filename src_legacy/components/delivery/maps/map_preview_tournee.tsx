import * as React from "react";
import MapView, { Marker, Region } from "react-native-maps";
import { StyleSheet, View, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import { ITourneeDetail } from "src/interfaces/Entities";
import { TOURNEE_STEP_STATE, TOURNEE_STEP_STATUS } from "src/constants/tournee";

const { width, height } = Dimensions.get("window");

const LATITUDE_DELTA = 0.0125;
const LONGITUDE_DELTA = 0.0125 * (height / width);

export interface ILatLong {
  latitude: number;
  longitude: number;
}

export default function MapPreviewTournee(props: {
  coords?: ILatLong;
  tournee?: ITourneeDetail;
  isOnOverlay?: boolean;
}) {
  const tournee = props.tournee;

  const initialRegion = {
    latitude: tournee ? tournee?.steps[0]?.target.lat : 0,
    longitude: tournee ? tournee?.steps[0]?.target.lng : 0,
    latitudeDelta: LATITUDE_DELTA,
    longitudeDelta: LONGITUDE_DELTA,
  } as Region;

  return (
    <View style={styles.container}>
      <MapView style={styles.map} region={initialRegion}>
        {tournee &&
          tournee?.steps.map((step, index) => {
            const active = step.state === TOURNEE_STEP_STATE.ENABLE;
            const isPreview =
              step.step_status === TOURNEE_STEP_STATUS.FINISHED ||
              step.state === TOURNEE_STEP_STATE.CROSSED;
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude: step.target.lat,
                  longitude: step.target.lng,
                }}
              >
                <View style={styles.parent}>
                  <View
                    style={[
                      styles.circle,
                      {
                        opacity: isPreview ? 0.6 : 1,
                        backgroundColor: props.isOnOverlay
                          ? "#FF5C85"
                          : active
                          ? "#FF5C85"
                          : isPreview
                          ? "#FFB6C8"
                          : "#C4C4C4",
                      },
                    ]}
                  >
                    <Text style={styles.numberText}>{step.position + 1}</Text>
                  </View>
                </View>
              </Marker>
            );
          })}
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 20,
  },
  map: {
    width: "100%",
    height: 220,
  },
  parent: {
    width: 35,
    height: 35,
    alignItems: "center",
    justifyContent: "center",
  },
  circle: {
    width: 22,
    height: 22,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
  },
  numberText: {
    fontFamily: fontFamily.semiBold,
    color: "white",
  },
});
