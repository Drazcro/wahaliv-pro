import * as React from "react";
import MapView, { Marker, Region } from "react-native-maps";
import { StyleSheet, View, Dimensions } from "react-native";
import { COURSE_STATUS } from "src/constants/course";
import { courseStore } from "src_legacy/zustore/coursestore";
import MapPinRed from "assets/image_svg/MapPinRed.svg";

const { width, height } = Dimensions.get("window");

const LATITUDE_DELTA = 0.0125;
const LONGITUDE_DELTA = 0.0125 * (height / width);

export interface ILatLong {
  latitude: number;
  longitude: number;
}

export default function MapPreview(props: { coords?: ILatLong }) {
  let initialRegion = {} as Region;
  if (props.coords) {
    initialRegion = {
      latitude: props.coords.latitude,
      longitude: props.coords.longitude,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    };
  } else {
    const { courseEnCours } = courseStore();

    const isRecovered =
      courseEnCours.delivery_status == COURSE_STATUS.RECOVERED;
    initialRegion = {
      latitude: isRecovered
        ? courseEnCours.customer_lat
        : courseEnCours.partner_lat,
      longitude: isRecovered
        ? courseEnCours.customer_lng
        : courseEnCours.partner_lng,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA,
    };
  }

  return (
    <View style={styles.container}>
      <MapView style={styles.map} region={initialRegion}>
        <Marker
          coordinate={{
            latitude: initialRegion.latitude,
            longitude: initialRegion.longitude,
          }}
        >
          <MapPinRed width={30} height={30} />
        </Marker>
      </MapView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  map: {
    width: "100%",
    height: 220,
  },
});
