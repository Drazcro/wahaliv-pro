import React from "react";
import { Image, Platform, StyleSheet, View } from "react-native";
import { get_new_access_token_with_refresh } from "src_legacy/services/Loaders";

import { color, images } from "src/theme";
import { getAuth } from "src_legacy/session/store";
import { IAuth } from "src_legacy/session/types";
import { getBaseUser, ROLE_TYPE } from "src_legacy/services/Utils";
import { setApiEnv } from "src_legacy/services/AuthService";
import { authStore, vanillaAuthState } from "src_legacy/services/v2/auth_store";

export default function LoadingScreen() {
  const { setAuth, setAccountTypeDelivery, setAccountTypePartner } =
    authStore();

  function dispatchDefaultAuth() {
    setAuth({ is_loading: false } as IAuth);
  }

  React.useEffect(() => {
    const renew_token = async () => {
      try {
        const old_iauth = await getAuth();
        if (!old_iauth.refresh_token) {
          dispatchDefaultAuth();
          return;
        }
        const old_base_user = getBaseUser(old_iauth);
        setApiEnv(old_base_user.email);
        const local_iauth = await get_new_access_token_with_refresh(
          old_iauth.refresh_token,
        );
        console.log("retrieved new token");

        const local_user = getBaseUser(local_iauth);
        console.log(local_user);

        if (local_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
          setAccountTypeDelivery();
        } else if (local_user.roles.includes(ROLE_TYPE.ROLE_PARTENAIRE)) {
          setAccountTypePartner();
        } else {
          dispatchDefaultAuth();
          return Promise.reject();
        }
        // HttpClient.setAuthorization(local_iauth.access_token);
        vanillaAuthState.getState().auth.access_token =
          local_iauth.access_token;

        local_iauth.is_loading = false;
        console.log("************************");
        setAuth(local_iauth);
      } catch {
        dispatchDefaultAuth();
      }
    };
    renew_token();
  }, []);

  return (
    <View style={style.main}>
      <Image
        source={images.Landing.Background}
        resizeMode="cover"
        style={style.container}
      />
    </View>
  );
}

const style = StyleSheet.create({
  main: {
    height: "100%",
    backgroundColor: color.ACCENT,
  },
  container: {
    width: "100%",
    height: "100%",
    marginTop: Platform.OS === "ios" ? 5.2 : 0,
  },
});
