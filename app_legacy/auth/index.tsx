import React, { useEffect, useState } from "react";
import {
  Keyboard,
  KeyboardAvoidingView,
  Platform,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import * as Linking from "expo-linking";

import Lock from "assets/image_svg/Lock.svg";
import EnvelopeSimple from "assets/image_svg/EnvelopeSimple.svg";

import { Input } from "@rneui/themed";
import { Button, Text } from "react-native-elements";
import Eye from "assets/image_svg/Eye.svg";
import EyeClosed from "assets/image_svg/EyeClosed.svg";
import { color } from "../../src/theme/color";
import { styles } from "./sign_in.style";
import { login } from "../../src_legacy/services/AuthService";
import * as Notifications from "expo-notifications";
import { unregisterBackgroundFetchAsync } from "../../src/cron/foreground_localisation_task";

import { ActivityIndicator } from "react-native-paper";
import { clearSession } from "../../src_legacy/session/store";
import useDeviceType from "../../src/hooks/useDeviceType";
import * as Device from "expo-device";

import { router, usePathname } from "expo-router";
import { toastStore } from "src/zustore/toast_store";
import { TourneeToast } from "src_legacy/components/delivery/courses/toast/toast";
import { authStore } from "src_legacy/services/v2/auth_store";
// import { showToast } from "../../src/services/Loaders";

export default function SignIn() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [enableButton, setEnableButton] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [expoToken, setExpoToken] = useState("");
  const path = usePathname();
  const {showToast} = toastStore()
  const toastState = toastStore(v => v.isVisible)

  console.log(path);

  const { auth } = authStore();

  const isTablet = useDeviceType();

  const registerForPushNotificationsAsync = async () => {
    const token = (
      await Notifications.getExpoPushTokenAsync({
        //applicationId: "@helodev/sacrearmand_pro_app",
        projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
      })
    ).data;
    setExpoToken(token);

    if (Platform.OS === "android") {
      Notifications.setNotificationChannelAsync("default", {
        name: "default",
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: "#FF231F7C",
      });
    }
  };
  const handleSubmit = () => {
    const device_brand = Device.brand;
    const device_model_name = Device.modelName;
    const device_os_version = Device.osVersion;
    const device_year = Device.deviceYearClass;
    clearSession();
    setIsLoading(true);
    login(
      username,
      password,
      expoToken,
      device_brand,
      device_model_name,
      device_os_version,
      device_year,
    ).finally(() => {
      setIsLoading(false);
    })
  
  };

  useEffect(() => {
    if (username.trim() !== "" && password.trim() !== "") {
      setEnableButton(true);
      setUsername(username.replace(/\s+/g, ""));
    } else {
      setEnableButton(false);
    }
  }, [username, password]);
  console.log(username);
  useEffect(() => {
    registerForPushNotificationsAsync();
    if (Platform.OS !== "web") {
      unregisterBackgroundFetchAsync();
    }
  }, []);

  if (isLoading === false && auth.access_token) {
    return <ActivityIndicator size={"large"} />;
  }

  return (
    <KeyboardAvoidingView style={styles.main}>

      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View>
          <View>
            <Text
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.TabletHeaderTitle
                  : styles.title
              }
            >
              Connexion
            </Text>
          </View>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.TabletInputContainer
                : styles.inputContainer
            }
          >
            <Input
              autoCapitalize={"none"}
              // placeholder={i18n.t("signIn.email")}
              placeholder="Email"
              onChangeText={setUsername}
              value={username}
              style={styles.inputText}
              textContentType="emailAddress"
              leftIcon={<EnvelopeSimple />}
            />
            <Input
              autoCapitalize={"none"}
              // placeholder={i18n.t("signIn.password")}
              placeholder="Mot de passe"
              onChangeText={setPassword}
              value={password}
              style={styles.inputText}
              textContentType="password"
              leftIcon={<Lock />}
              rightIcon={
                password !== "" ? (
                  showPassword === true ? (
                    <TouchableOpacity
                      style={styles.iconEyes}
                      onPress={() => setShowPassword(false)}
                    >
                      <Eye />
                    </TouchableOpacity>
                  ) : (
                    <TouchableOpacity
                      style={styles.iconEyes}
                      onPress={() => setShowPassword(true)}
                    >
                      <EyeClosed />
                    </TouchableOpacity>
                  )
                ) : undefined
              }
              secureTextEntry={showPassword === false}
            />
          </View>
          <Button
            type="clear"
            buttonStyle={{
              alignSelf: "flex-end",
            }}
            // title={i18n.t("signIn.forgetPassword")}
            title={"Mot de passe oublié ?"}
            onPress={() => router.push("/auth/password_reset")}
            titleStyle={
              isTablet && Device.brand != "SUNMI"
                ? styles.TabletForgotPasswordText
                : styles.forgotPasswordText
            }
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.TabletForgotPasswordContainer
                : styles.forgotPasswordContainer
            }
          ></Button>
          <TouchableOpacity
            onPress={() => {
              Linking.openURL(
                "https://sacrearmand.com/sacre-armand-pro/privacy-policy",
              );
            }}
          >
            <Text
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.TabletPrivacyPolicyText
                  : styles.privacyPolicyText
              }
            >
              {/* {i18n.t("signIn.privacyPolicy")} */}
              Politique de confidentialité
            </Text>
          </TouchableOpacity>
          <View style={{ paddingTop: !isTablet ? "55%" : 135 }}>
            <Button
              onPress={handleSubmit}
              type="solid"
              title={
                isLoading === true ? (
                  <ActivityIndicator color={color.RED} />
                ) : (
                  "Se connecter"
                )
              }
              disabled={isLoading || !(password && username) ? true : false}
              titleStyle={{
                ...styles.buttonTitle,
                fontSize: isTablet ? 18 : 16,
              }}
              buttonStyle={
                isTablet && Device.brand != "SUNMI"
                  ? styles.TabletButton
                  : styles.button
              }
              disabledStyle={{
                backgroundColor: "#C4C4C4",
              }}
              disabledTitleStyle={{
                color: "#FDFDFD",
              }}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
}
