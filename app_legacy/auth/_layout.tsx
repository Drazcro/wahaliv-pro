import { Stack } from "expo-router";
import React from "react";
import useCachedResources from "src/hooks/useCachedResources";
export default function AuthLayout() {
  const isLoadingComplete = useCachedResources();
  if (!isLoadingComplete) {
    return null;
  }

  return (
    <React.Fragment>
      <Stack screenOptions={{ headerShown: false }} />
    </React.Fragment>
  );
}
