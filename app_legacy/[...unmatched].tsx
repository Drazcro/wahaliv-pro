import { Redirect } from "expo-router";

export default function () {
  return <Redirect href={"/auth/sign_in"} />;
}
