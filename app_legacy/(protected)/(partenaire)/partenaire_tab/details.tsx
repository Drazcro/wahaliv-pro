import { StatusBar } from "expo-status-bar";
import I18n from "i18n-js";
import React from "react";
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ImageBackground,
  TouchableOpacity,
  Platform,
  Linking,
  ActivityIndicator,
} from "react-native";
import Constants from "expo-constants";
import { images } from "src/theme";
import { fontFamily } from "src/theme/typography";
import ThumbsUp from "assets/image_svg_partner/ThumbsUp.svg";
import Timer from "assets/image_svg_partner/Timer.svg";
import IconeC from "assets/image_svg_partner/orders.svg";
import IconeCMobile from "assets/image_svg_partner/IconeC.svg";
import CurrencyEur from "assets/image_svg_partner/CurrencyEur.svg";
import { Divider } from "react-native-elements";
import Info from "assets/image_svg/Info.svg";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import useDeviceType from "src/hooks/useDeviceType";
import { useKeepAwake } from "expo-keep-awake";
import * as Device from "expo-device";
import { PartnerProfileNavigationItems } from "src_legacy/components/partenaire/Profile/MenuItems";
import { PartnerProfileBtnHeader } from "src_legacy/components/partenaire/Profile/HeaderBtn";
import { PartnerProfileInformations } from "src_legacy/components/partenaire/Profile/Informations";

const PartnerProfileScreen = () => {
  useKeepAwake();
  const { partnerProfile } = partnerStore();

  const isTablet = useDeviceType();

  // React.useEffect(() => {
  //   const user_id = authToUserId(auth);
  //   console.log(auth.access_token);
  //   partner_profile_information_and_state(user_id);
  // }, [auth]);

  if (!partnerProfile.partner) {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" />
      </View>
    );
  }

  return (
    <View style={styles.main}>
      <StatusBar style="dark" />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        automaticallyAdjustContentInsets={false}
        overScrollMode={"never"}
      >
        <View style={styles.header}>
          <ImageBackground
            source={images.Landing.Ellipse}
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tablet_background_image
                : styles.background_image
            }
            resizeMode={
              isTablet && Device.brand != "SUNMI" ? "stretch" : "cover"
            }
          ></ImageBackground>
          {isTablet && Device.brand != "SUNMI" ? (
            <View style={styles.tablet_text_group}>
              <Text style={styles.tablet_text_name}>
                {I18n.t("partner.profile.header.welcome")}{" "}
                {partnerProfile.partner.name}
              </Text>
              <Text style={styles.tablet_text_name}>
                {I18n.t("partner.profile.header.space")}
              </Text>
              <Text
                style={{
                  ...styles.tablet_text_email,
                  fontFamily: fontFamily.Medium,
                }}
              >
                {partnerProfile.partner.address}
              </Text>
            </View>
          ) : (
            <View style={styles.text_group}>
              <Text style={styles.text_name}>
                {I18n.t("partner.profile.header.welcome")}{" "}
                {partnerProfile.partner.name}
              </Text>
              <Text style={styles.text_name}>
                {I18n.t("partner.profile.header.space")}
              </Text>
              <Text
                style={{ ...styles.text_email, fontFamily: fontFamily.Medium }}
              >
                {partnerProfile.partner.address}
              </Text>
            </View>
          )}
          <PartnerProfileBtnHeader isOpen={partnerProfile.partner.is_open} />
        </View>
        {isTablet && Device.brand != "SUNMI" ? (
          <View style={styles.tablet_info_content}>
            <PartnerProfileInformations
              unit="%"
              title={"AVIS CLIENT"}
              value={partnerProfile.stats.rate_client}
              startText="Sur"
              textValue={partnerProfile.stats.rate_count}
              endText="notations"
              tipTextValue={0}
              icon={<ThumbsUp height={32} width={32} />}
            />
            <PartnerProfileInformations
              unit="min"
              title={"RETARD MOYEN"}
              value={partnerProfile.stats.average_delay}
              startText="Sur"
              textValue={partnerProfile.stats.nb_transactions}
              endText="transactions"
              tipTextValue={0}
              icon={<Timer height={32} width={32} />}
            />
            <PartnerProfileInformations
              unit="€"
              title={"TOTAL CA"}
              value={partnerProfile.stats.ca}
              startText=""
              textValue={undefined}
              endText="Mensuel"
              tipTextValue={0}
              icon={<CurrencyEur height={32} width={32} />}
            />
            <PartnerProfileInformations
              unit=""
              title={"COMMANDE"}
              value={partnerProfile.stats.order_count}
              startText=""
              textValue={undefined}
              endText="Mensuel"
              tipTextValue={0}
              icon={<IconeC height={32} width={32} />}
            />
          </View>
        ) : (
          <View style={styles.infoMain}>
            <View style={styles.infoContent}>
              <PartnerProfileInformations
                unit="%"
                title={"AVIS CLIENT"}
                value={partnerProfile.stats.rate_client}
                startText="Sur"
                textValue={partnerProfile.stats.rate_count}
                endText="notations"
                tipTextValue={0}
                icon={<ThumbsUp />}
              />
              <PartnerProfileInformations
                unit="min"
                title={"RETARD MOYEN"}
                value={partnerProfile.stats.average_delay}
                startText="Sur"
                textValue={partnerProfile.stats.nb_transactions}
                endText="transactions"
                tipTextValue={0}
                icon={<Timer />}
              />
            </View>
            <View style={styles.infoContent}>
              <PartnerProfileInformations
                unit="€"
                title={"TOTAL CA"}
                value={partnerProfile.stats.ca}
                startText=""
                textValue={undefined}
                endText="Mensuel"
                tipTextValue={0}
                icon={<CurrencyEur />}
              />
              <PartnerProfileInformations
                unit=""
                title={"COMMANDE"}
                value={partnerProfile.stats.order_count}
                startText=""
                textValue={undefined}
                endText="Mensuel"
                tipTextValue={0}
                icon={<IconeCMobile />}
              />
            </View>
          </View>
        )}
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_line
              : styles.line
          }
        >
          <Divider color="#C4C4C4" />
        </View>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_contact
              : styles.contact
          }
        >
          <View style={{ flexDirection: "row" }}>
            <Info />
            <View>
              <Text style={styles.text_rappel}>
                {I18n.t("partner.profile.contact.text")}{" "}
                {isTablet && Device.brand != "SUNMI"
                  ? I18n.t("partner.profile.contact.subtext")
                  : ""}
              </Text>
              {!isTablet && Device.brand != "SUNMI" ? (
                <Text style={styles.text_rappel}>
                  {I18n.t("partner.profile.contact.subtext")}
                </Text>
              ) : null}
            </View>
          </View>
          <TouchableOpacity
            style={{ marginLeft: 25 }}
            onPress={() => {
              openApp(`${I18n.t("partner.profile.contact.number")}`);
            }}
          >
            <Text style={{ ...styles.phone_number, marginLeft: 5 }}>
              {I18n.t("partner.profile.contact.number")}
            </Text>
          </TouchableOpacity>
        </View>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_line
              : styles.line
          }
        >
          <Divider color="rgba(196, 196, 196, 1)" />
        </View>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_navigation_items
              : styles.navigationItems
          }
        >
          <PartnerProfileNavigationItems />
        </View>
        {!isTablet &&
          Device.brand != "SUNMI" &&
          Constants.expoConfig?.version && (
            <Text style={styles.appVersionLabel}>
              V{Constants.expoConfig?.version}
            </Text>
          )}
      </ScrollView>
      {isTablet && Device.brand != "SUNMI" && Constants.expoConfig?.version && (
        <Text style={styles.appVersionLabel}>
          V{Constants.expoConfig?.version}
        </Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fff",
    flex: 1,
  },
  header: {
    justifyContent: "center",
    alignItems: "center",
    position: "relative",
    marginBottom: 35,
  },
  background_image: {
    justifyContent: "center",
    width: "100%",
    height: 190,
  },
  text_group: {
    position: "absolute",
    bottom: 45,
    flexDirection: "column",
    justifyContent: "center",
  },
  text_name: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 18,
    lineHeight: 21.94,
  },
  text_email: {
    color: "#fff",
    fontSize: 14,
    lineHeight: 14.63,
    marginVertical: 8,
    fontFamily: fontFamily.semiBold,
  },
  infoMain: {
    flexDirection: "column",
    justifyContent: "center",
    gap: 19,
    marginRight: 12,
    marginTop: 6,
    marginBottom: 37,
  },
  infoContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
    marginBottom: -20,
    gap: 10,
  },
  line: {
    marginHorizontal: 10,
  },
  contact: {
    flex: 1,
    justifyContent: "center",
    paddingVertical: 15,
    marginHorizontal: 35,
  },
  text_rappel: {
    fontSize: 16,
    color: "#1D2A40",
    marginLeft: 5,
    lineHeight: 20,
    marginBottom: 5,
    fontFamily: fontFamily.regular,
  },
  phone_number: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 92, 133, 1)",
    marginRight: 5,
    fontSize: 16,
    lineHeight: 22,
  },
  navigationItems: {
    marginTop: 38,
    marginBottom: 5,
    marginHorizontal: 30,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  /** Tablet screen */
  tablet_background_image: {
    justifyContent: "center",
    width: "100%",
    height: 226,
  },
  tablet_text_group: {
    position: "absolute",
    bottom: 45,
    left: 80,
    width: 466,
  },
  tablet_text_name: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 24,
    lineHeight: 32,
  },
  tablet_text_email: {
    color: "#fff",
    fontSize: 16,
    lineHeight: 19.05,
    fontFamily: fontFamily.Medium,
    marginTop: 7,
  },
  tablet_info_content: {
    flexDirection: "row",
    alignContent: "center",
    alignItems: "center",
    paddingHorizontal: 60,
    justifyContent: "space-between",
  },
  /** tablet screen */
  tablet_contact: {
    paddingVertical: 15,
    width: "100%",
    paddingHorizontal: 80,
    justifyContent: "center",
  },
  tablet_navigation_items: {
    marginTop: 38,
    marginBottom: 5,
    width: "100%",
    alignSelf: "center",
    paddingHorizontal: 80,
    justifyContent: "space-between",
  },
  appVersionLabel: {
    width: "100%",
    textAlign: "center",
    color: "rgba(17, 40, 66, 1)",
    marginBottom: 15,
  },
  tablet_line: {
    marginLeft: 80,
    marginRight: 70,
  },
});

const factoringPhoneNumber = (num: string) => {
  return num.replace(/\s/g, "");
};

const openApp = (number: string) => {
  if (Platform.OS !== "android") {
    const phone = factoringPhoneNumber(`telprompt:${number}`);
    return Linking.openURL(phone);
  } else {
    const phone = factoringPhoneNumber(`tel:${number}`);
    return Linking.openURL(phone);
  }
};

export default PartnerProfileScreen;
