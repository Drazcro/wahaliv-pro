// import { ProfileToolbar } from "delivery/components/Profile/ProfileToolbars";
import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  Platform,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
  TouchableOpacity,
} from "react-native";
import { StatusBar } from "expo-status-bar";
import I18n from "i18n-js";
import { fontFamily } from "src/theme/typography";
import { Button } from "react-native-elements";
import Calendar from "assets/image_svg_partner/Calendar.svg";
import Clock from "assets/image_svg_partner/Clock.svg";
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { profileStore } from "src_legacy/zustore/profilestore";
import {
  PartnerProfileEarlyClosingDelete,
  PartnerProfileEarlyClosingUpdate,
} from "src_legacy/services/Loaders";
import { IPartnerProfileUpdateDate } from "src/interfaces/Entities";
import { authToUserId } from "src_legacy/services/Utils";
import Toast from "react-native-expo-popup-notifications";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { useColorScheme } from "react-native";
import { EventRegister } from "react-native-event-listeners";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { router } from "expo-router";
import { authStore } from "src_legacy/services/v2/auth_store";
import { color } from "src/theme";

const to_formatted_date = (date: Date | string) => {
  return moment(date).format("DD/MM/YYYY");
};

const to_formatted_time = (date: Date | string) => {
  return moment(date).format("HH:mm");
};

const postFormatDate = (d: Date | string, h: Date | string) => {
  return moment(d).format("YYYY-MM-DD") + " " + moment(h).format("HH:mm:ss");
};

export default function PartnerProfileEarlyClosingTimerScreen() {
  const layout = useWindowDimensions();
  const isTablet = useDeviceType();
  const colorScheme = useColorScheme();
  console.log(colorScheme);

  const { auth } = authStore();
  const { earlyClosing } = profileStore();

  const [disableBtn, setDisableBtn] = useState(true);
  const [loading, setLoading] = useState(false);
  const [loadingDelete, setLoadingDelete] = useState(false);
  const [startDate, setStartDate] = useState(
    earlyClosing ? earlyClosing.begin_date.date : new Date(),
  );
  const [startTime, setStartTime] = useState(
    earlyClosing ? earlyClosing.begin_date.date : new Date(),
  );
  const [endDate, setEndDate] = useState(
    earlyClosing ? earlyClosing.end_date.date : new Date(),
  );
  const [endTime, setEndTime] = useState(
    earlyClosing ? earlyClosing.end_date.date : new Date(),
  );

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
  const [isStartDate, setIsStartDate] = useState(false);
  const [isStartTime, setIsStartTime] = useState(false);

  const user_id: number = authToUserId(auth);

  const showDatePicker = (isStart: boolean) => {
    setDatePickerVisibility(true);
    setIsStartDate(isStart);
  };

  const showTimePicker = (isStart: boolean) => {
    setTimePickerVisibility(true);
    setIsStartTime(isStart);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };

  const handleDateConfirm = (date: Date) => {
    hideDatePicker();
    isStartDate ? setStartDate(date) : setEndDate(date);
    setDisableBtn(false);
  };

  const handleTimeConfirm = (date: Date) => {
    hideTimePicker();
    isStartTime ? setStartTime(date) : setEndTime(date);
    setDisableBtn(false);
  };

  const onValidate = () => {
    const payload: IPartnerProfileUpdateDate = {
      begin_date: postFormatDate(startDate, startTime),
      end_date: postFormatDate(endDate, endTime),
      id: earlyClosing ? parseInt(earlyClosing.id) : null,
    };
    setLoading(true);
    PartnerProfileEarlyClosingUpdate(user_id, payload)
      .then((res) => {
        if (res && res.status === 200) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: I18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: I18n.t("profile.notification.error.message2"),
          });
        }
      })
      .catch()
      .finally(() => {
        setLoading(false);
        EventRegister.emitEvent("EARLY_CLOSING_SHOULD_RELOAD");
      });
  };

  const onDelete = () => {
    setLoadingDelete(true);
    PartnerProfileEarlyClosingDelete(user_id, earlyClosing?.id)
      .then((res) => {
        if (res && res.status === 200) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: I18n.t("profile.notification.success.message"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: I18n.t("profile.notification.error.message3"),
          });
        }
      })
      .catch()
      .finally(() => {
        setLoadingDelete(false);
        EventRegister.emitEvent("EARLY_CLOSING_SHOULD_RELOAD");
      });
  };

  useEffect(() => {
    if (earlyClosing) {
      setDisableBtn(false);
    }
  }, [isStartDate, isStartTime]);

  return (
    <View style={styles.main}>
      <StatusBar style="dark" />
      <ProfileToolbar
        title={isTablet ? "" : I18n.t("partner.profile.earlyClosing.title")}
      />
      <View style={{ height: layout.height - 170, ...styles.body }}>
        <View style={styles.closingocntent}>
          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tabletDateItem
                : styles.dateItem
            }
          >
            <View style={{ marginBottom: 14 }}>
              <Text style={styles.dateItemText}>
                {I18n.t("partner.profile.earlyClosingTimer.startDate")}
              </Text>
            </View>
            <View
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tabletInput
                  : styles.input
              }
            >
              <TouchableOpacity
                onPress={() => showDatePicker(true)}
                style={
                  isTablet && Device.brand != "SUNMI"
                    ? styles.tabletInputContent
                    : styles.inputContent
                }
              >
                <Calendar />
                <Text
                  style={{
                    ...styles.inputDate,
                    color: disableBtn ? "#C4C4C4" : "#1D2A40",
                  }}
                >
                  {disableBtn ? "JJ/MM/AA" : to_formatted_date(startDate)}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => showTimePicker(true)}
                style={styles.inputContent}
              >
                <Clock />
                <Text
                  style={{
                    ...styles.inputDate,
                    color: disableBtn ? "#C4C4C4" : "#1D2A40",
                  }}
                >
                  {disableBtn ? "00:00" : to_formatted_time(startTime)}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View
            style={
              isTablet && Device.brand != "SUNMI"
                ? styles.tabletDateItem
                : styles.dateItem
            }
          >
            <View style={{ marginBottom: 14 }}>
              <Text style={styles.dateItemText}>
                {I18n.t("partner.profile.earlyClosingTimer.endDate")}
              </Text>
            </View>
            <View
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tabletInput
                  : styles.input
              }
            >
              <TouchableOpacity
                onPress={() => showDatePicker(false)}
                style={
                  isTablet && Device.brand != "SUNMI"
                    ? styles.tabletInputContent
                    : styles.inputContent
                }
              >
                <Calendar />
                <Text
                  style={{
                    ...styles.inputDate,
                    color: disableBtn ? "#C4C4C4" : "#1D2A40",
                  }}
                >
                  {disableBtn ? "JJ/MM/AA" : to_formatted_date(endDate)}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => showTimePicker(false)}
                style={styles.inputContent}
              >
                <Clock />
                <Text
                  style={{
                    ...styles.inputDate,
                    color: disableBtn ? "#C4C4C4" : "#1D2A40",
                  }}
                >
                  {disableBtn ? "00:00" : to_formatted_time(endTime)}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View>
          {loading ? (
            <View
              style={{
                ...styles.button,
                marginBottom: isTablet && Device.brand != "SUNMI" ? 40 : 25,
                backgroundColor: color.ACCENT,
                alignItems: "center",
                justifyContent: "center",
              }}
            >
              <ActivityIndicator color="white" />
            </View>
          ) : (
            <Button
              type="solid"
              title={I18n.t("partner.profile.earlyClosingTimer.validateBtn")}
              titleStyle={{ ...styles.buttonTitle, color: "#fff" }}
              buttonStyle={{
                ...styles.button,
                width: isTablet && Device.brand != "SUNMI" ? 355 : 308,
                backgroundColor: color.ACCENT,
                marginBottom:
                  isTablet && Device.brand != "SUNMI"
                    ? disableBtn
                      ? 130
                      : 40
                    : 25,
                borderColor: color.ACCENT,
              }}
              onPress={onValidate}
              disabled={disableBtn}
              disabledStyle={{
                backgroundColor: "#C4C4C4",
                borderColor: "#C4C4C4",
              }}
              disabledTitleStyle={{
                color: "#FDFDFD",
                fontFamily: fontFamily.semiBold,
                fontSize: isTablet && Device.brand != "SUNMI" ? 18 : 16,
                lineHeight: 21,
              }}
            />
          )}
          {!disableBtn && loadingDelete && (
            <View
              style={{
                ...styles.button,
                marginBottom:
                  isTablet && Device.brand != "SUNMI"
                    ? loadingDelete
                      ? 130
                      : 40
                    : 25,
                backgroundColor: "#fff",
                alignItems: "center",
                justifyContent: "center",
                width: isTablet && Device.brand != "SUNMI" ? 355 : 308,
              }}
            >
              <ActivityIndicator color={color.ACCENT} />
            </View>
          )}
          {!disableBtn && !loadingDelete ? (
            <Button
              type="solid"
              title={I18n.t("partner.profile.earlyClosingTimer.closeBtn")}
              titleStyle={styles.buttonTitle}
              buttonStyle={{
                ...styles.button,
                width: isTablet && Device.brand != "SUNMI" ? 355 : 308,
                marginBottom: isTablet && Device.brand != "SUNMI" ? 130 : 0,
              }}
              onPress={onDelete}
            />
          ) : null}
        </View>
      </View>
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        display="spinner"
        mode="date"
        onConfirm={handleDateConfirm}
        onCancel={hideDatePicker}
        locale="fr-FR"
        cancelTextIOS="Annuler"
        confirmTextIOS="Valider"
      />
      <DateTimePickerModal
        isVisible={isTimePickerVisible}
        mode="time"
        onConfirm={handleTimeConfirm}
        onCancel={hideTimePicker}
        locale="fr-FR"
        cancelTextIOS="Annuler"
        confirmTextIOS="Valider"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
  },
  body: {
    justifyContent: "space-between",
    paddingBottom: 10,
  },
  title: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 22,
    color: "#112842",
  },
  closingocntent: {
    height: Platform.OS === "ios" ? "65%" : "55%",
    marginHorizontal: 10,
  },
  button: {
    height: 52,
    backgroundColor: "#fff",
    borderColor: color.ACCENT,
    borderWidth: 1,
    borderRadius: 30,
    alignSelf: "center",
  },
  buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 18,
    lineHeight: 22,
    color: color.ACCENT,
  },
  dateItem: {
    flexDirection: "column",
    marginHorizontal: 30,
    marginTop: 30,
  },
  dateItemText: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 22,
    color: "#112842",
  },
  inputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 40,
    color: "black",
  },
  input: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#6C6C6C",
    borderWidth: 1,
    borderRadius: 30,
    height: 46,
    color: "black",
  },
  inputDate: {
    fontFamily: fontFamily.regular,
    fontSize: 16,
    lineHeight: 19.5,
    marginLeft: 5,
  },
  /** tablet screen */
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    lineHeight: 24.38,
    textAlign: "center",
  },
  tabletDateItem: {
    flexDirection: "column",
    width: 670,
    marginTop: 40,
    alignSelf: "center",
  },
  tabletInput: {
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    borderColor: "#6C6C6C",
    borderWidth: 1,
    borderRadius: 30,
    height: 46,
  },
  tabletInputContent: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 20,
    marginRight: 200,
  },
});
