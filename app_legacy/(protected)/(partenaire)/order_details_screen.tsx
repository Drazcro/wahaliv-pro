import React, { useEffect, useState } from "react";
import { StatusBar, StyleSheet, TouchableOpacity, View } from "react-native";
import { Text } from "react-native-elements";
import User from "assets/image_svg/User.svg";
import ThumbsUpGreen from "assets/image_svg/ThumbsUpGreen.svg";
import Scooter from "assets/image_svg/ScooterBlack.svg";
import DarkClock from "assets/image_svg/ClockDark.svg";
import ClockWhite from "assets/image_svg/ClockWhite.svg";
import { fontFamily } from "src/theme/typography";
import { ScrollView } from "react-native-gesture-handler";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import {
  to_formatted_date,
  to_formatted_time,
  parse_to_float_number,
} from "src_legacy/services/Utils";
import {
  IPartnerOrderDetails,
  IPayementDetail,
  IProductDetails,
  ORDER_STATUS,
} from "src/interfaces/Entities";
import moment from "moment";
import {
  GetPartnerOrderDetails,
  PartnerOrderCanceled,
  PartnerOrderFinished,
  PartnerOrderPrint,
  reloadOrders,
} from "src_legacy/services/Loaders";
import Toast from "react-native-expo-popup-notifications";
import I18n from "i18n-js";
import { Input } from "@rneui/themed";
import useDeviceType from "src/hooks/useDeviceType";
import { useKeepAwake } from "expo-keep-awake";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { SliderButton } from "src_legacy/components/slider_button/slider_button";
import * as Device from "expo-device";
import { EventRegister } from "react-native-event-listeners";
import { TimeOverlay } from "src_legacy/components/partenaire/overlay/timeOverlay/TimeOverlay";
import { PartnerOrderDetailsSettingOverlay } from "src_legacy/components/partenaire/overlay/order/OrderDetailsSettingOverlay";
import { PartnerCancelOrderOverlay } from "src_legacy/components/partenaire/overlay/order/CancelOrderOverlay";
import { PartnerOrderPrintTcketOverlay } from "src_legacy/components/partenaire/overlay/order/PrintTicketOverlay";
import { PartnerOrderTradeDiscountOverlay } from "src_legacy/components/partenaire/overlay/order/TradeDiscountOverly";
import { PartnerOrderRefundsOverlay } from "src_legacy/components/partenaire/overlay/order/RefundsOverlay";
import { PartnerToolbar } from "src_legacy/components/partenaire/PartnerToolbars";
import { router } from "expo-router";

const DATE_NOW = moment(new Date());

export default function PartnerOrderDetailsScreen() {
  useKeepAwake();
  const { orderEnCours, partnerProfile, setOrderEnCours } = partnerStore();
  const { setCurrentTabIndex } = tourneeStore();
  const [openSetting, setOpenSetting] = useState(false);
  const [openRefunds, setOpenRefunds] = useState(false);
  const [openTrade, setOpenTrade] = useState(false);
  const [openPrintTicket, setOpenTicket] = useState(false);
  const [openCancelOrder, setOpenCancelOrder] = useState(false);
  const [btnLoading, setBtnLoading] = useState(false);
  const [orderData, setOrderData] = useState<IPartnerOrderDetails>();
  const { setOrderInModal } = partnerStore();

  const [timeOverlayVisible, setTimeOverlayVisible] = useState(false);

  // Order Status
  const IS_ACCEPTED = orderEnCours.commande.status === ORDER_STATUS.ACCEPTED;
  const IS_DELIVERED = orderEnCours.commande.status === ORDER_STATUS.DELIVERED;
  const IS_IN_DELIVERY =
    orderEnCours.commande.service_type === ORDER_STATUS.IN_DELIVERY;
  const IS_IN_TAKE_OUT =
    orderEnCours.commande.service_type === ORDER_STATUS.IN_TAKE_OUT;
  const IS_ORDER_ACCEPTED_LATE =
    IS_ACCEPTED &&
    moment(orderEnCours.commande.delivery_date.date).isBefore(DATE_NOW);

  // Action Order
  const actionAcceptedOrder = () => {
    PartnerOrderFinished(orderEnCours).then((res) => {
      if (res && res.status === 200) {
        Toast.show({
          type: "success",
          text1: "Success",
          text2: I18n.t("partner.notification.success.messageOrderFinished"),
        });
        setCurrentTabIndex(1);
        router.back();
      } else {
        Toast.show({
          type: "error",
          text1: "Erreur",
          text2: I18n.t("partner.notification.error.messageOrderFinished"),
        });
      }
    });
  };
  const actionPrintTicket = () => {
    setBtnLoading(true);
    PartnerOrderPrint(orderEnCours.commande.id)
      .then((res) => {
        if (res && res.status === 200) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: I18n.t("partner.notification.success.messageOrderPrint"),
          });
          router.back();
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: I18n.t("partner.notification.error.messageOrderPrint"),
          });
        }
      })
      .finally(() => {
        setBtnLoading(false);
      });
  };
  const actionCancelOrder = () => {
    setBtnLoading(true);
    PartnerOrderCanceled(orderEnCours.commande.id)
      .then((res) => {
        if (res && res.status === 200) {
          Toast.show({
            type: "success",
            text1: "Success",
            text2: I18n.t("partner.notification.success.messageOrderCanceled"),
          });
          reloadOrders().then(() => {
            setBtnLoading(false);
            router.back();
          });
        } else {
          Toast.show({
            type: "error",
            text1: "Erreur",
            text2: I18n.t("partner.notification.error.messageOrderCanceled"),
          });
          setBtnLoading(false);
        }
      })
      .finally(() => {
        //setBtnLoading(false);
      });
  };
  const LateTime = () => {
    const realTime = moment(orderEnCours.commande.delivery_date.date).from(
      DATE_NOW,
    );
    return (
      <View style={[{ position: "absolute", right: isTablet ? 0 : -30 }]}>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.timerContainerTablet
              : styles.timerContainer
          }
        >
          <ClockWhite />
          <Text style={styles.timerText}>
            {realTime
              .replace("il y a", "En retard")
              .replace("il y a", "En retard")
              .replace("une", "1")
              .replace("minutes", "min")}
          </Text>
        </View>
      </View>
    );
  };
  const onChangePrice = (price: string) => {
    setOrderEnCours({ ...orderEnCours, newPrice: price });
  };

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "COMMAND_SHOULD_RELOAD",
      () => {
        //
      },
    );
    //@ts-ignore
    return () => {
      EventRegister.removeEventListener(listener);
    };
  }, []);

  const isTablet = useDeviceType();
  const toCrossed = (value: number) => value === 0;
  const checkNewHour = () => {
    GetPartnerOrderDetails(orderEnCours.commande.id).then((orderData) => {
      setOrderData(orderData);
    });
  };
  useEffect(() => {
    console.log(orderData?.commande.pickup_date.date);
    checkNewHour();
  }, [timeOverlayVisible]);
  return (
    <View style={styles.main}>
      <StatusBar />
      <PartnerToolbar
        title={`Commande N°${orderEnCours.commande.id}`}
        openSetting={() => setOpenSetting(true)}
        isInDelivery={IS_IN_DELIVERY}
        isInTakeOout={IS_IN_TAKE_OUT}
        isOrderlate={IS_ORDER_ACCEPTED_LATE}
      />
      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? {
                height: 1,
                backgroundColor: "#C4C4C4",
              }
            : {}
        }
      />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            paddingHorizontal:
              isTablet && Device.brand != "SUNMI" ? 60 : undefined,
            marginTop: isTablet && Device.brand != "SUNMI" ? 60 : undefined,
          }}
        >
          <View style={styles.clientMain}>
            <Text style={styles.subTitles}>CLIENT</Text>
            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View style={styles.clientContainer}>
                <User />
                <Text style={{ ...styles.clientTex, marginLeft: 13 }}>
                  {orderEnCours.customer.name}
                </Text>
              </View>
              {IS_DELIVERED && (
                <View style={{ ...styles.clientContainer, marginRight: 20 }}>
                  <Text style={{ ...styles.clientTex, marginRight: 13 }}>
                    Note
                  </Text>
                  <ThumbsUpGreen />
                </View>
              )}
            </View>
          </View>
        </View>
        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_horizontalLine
              : styles.horizontalLine
          }
        />

        {IS_ACCEPTED && (
          <View
            style={{
              paddingHorizontal:
                isTablet && Device.brand != "SUNMI" ? 60 : undefined,
              paddingRight: !isTablet ? 30 : undefined,
            }}
          >
            <View style={styles.clientMain}>
              <View
                style={{
                  position: "relative",
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text style={styles.subTitles}>COLLECTE</Text>
                {IS_ORDER_ACCEPTED_LATE && <LateTime />}
              </View>
              <View
                style={{
                  ...styles.deliveryContainer,
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "flex-end",
                }}
              >
                <View style={{ ...styles.textContainer, flexDirection: "row" }}>
                  {orderData === undefined ? (
                    <View style={styles.hourPosition}>
                      <DarkClock />
                      <Text style={{ ...styles.deliveryText, marginLeft: 13 }}>
                        Aujourd'hui à{" "}
                        <Text style={styles.hourBold}>
                          {to_formatted_time(orderEnCours.commande.pickup_date.date)}
                        </Text>
                      </Text>
                    </View>
                  ) : (
                    <View style={styles.hourPosition}>
                      <DarkClock />
                      <Text style={{ ...styles.deliveryText, marginLeft: 13 }}>
                        Aujourd'hui à{" "}
                        <Text style={styles.hourBold}>
                          {to_formatted_time(orderData.commande.pickup_date.date)}
                        </Text>
                      </Text>
                    </View>
                  )}
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      setOrderInModal(orderEnCours);
                      setTimeOverlayVisible(true);
                    }}
                  >
                    <Text style={styles.editText}>Modifier</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        )}
        {IS_DELIVERED ? (
          <View
            style={{
              paddingHorizontal:
                isTablet && Device.brand != "SUNMI" ? 60 : undefined,
            }}
          >
            <View style={styles.clientMain}>
              <Text style={styles.subTitles}>COLLECTE</Text>
              <View style={styles.deliveryContainer}>
                <Text style={styles.deliveryText}>
                  Collecté le{" "}
                  {to_formatted_date(orderEnCours.commande.pickup_date.date)} à{" "}
                  {to_formatted_time(orderEnCours.commande.pickup_date.date)}
                </Text>
                <Text style={{ ...styles.deliveryText, marginTop: 5 }}>
                  {IS_IN_DELIVERY ? "Livrée " : "Retirée "}
                  {to_formatted_date(
                    orderEnCours.commande.delivery_date.date,
                  )} à {to_formatted_time(orderEnCours.commande.delivery_date.date)}
                </Text>
              </View>
            </View>
          </View>
        ) : null}

        <View
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_horizontalLine
              : styles.horizontalLine
          }
        />

        {IS_IN_DELIVERY ? (
          <>
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 60 : undefined,
              }}
            >
              <View style={styles.clientMain}>
                <Text style={styles.subTitles}>LIVREUR</Text>
                <View style={styles.clientContainer}>
                  <Scooter />
                  <Text style={{ ...styles.clientTex, marginLeft: 13 }}>
                    {orderEnCours.commande.deliveryman}
                  </Text>
                </View>
              </View>
            </View>
            <View
              style={
                isTablet && Device.brand != "SUNMI"
                  ? styles.tablet_horizontalLine
                  : styles.horizontalLine
              }
            />
          </>
        ) : null}

        <View
          style={{
            paddingHorizontal:
              isTablet && Device.brand != "SUNMI" ? 60 : undefined,
            paddingRight: !isTablet ? 25 : undefined,
          }}
        >
          <View style={styles.clientMain}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                marginRight: 7,
              }}
            >
              <Text style={styles.subTitles}>DÉTAIL DE LA COMMANDE</Text>
              {(IS_ACCEPTED || IS_DELIVERED) && (
                <TouchableOpacity onPress={() => setOpenRefunds(true)}>
                  <Text style={styles.editText}>Modifier</Text>
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.clientContainer}>
              <View style={{ flexDirection: "column", width: "100%" }}>
                <>
                  {orderEnCours.product_details.map(
                    (product: IProductDetails, index: number) => (
                      <React.Fragment key={index}>
                        <View style={styles.products} key={product.id}>
                          <Text
                            style={{
                              ...styles.productTitle,
                              textDecorationLine:
                                product.isRefund || toCrossed(product.qte)
                                  ? "line-through"
                                  : "none",
                            }}
                          >
                            {product.qte} {product.name}
                          </Text>
                          <Text
                            style={{
                              ...styles.productPrice,
                              textDecorationLine:
                                product.isRefund || toCrossed(product.qte)
                                  ? "line-through"
                                  : "none",
                            }}
                          >
                            {parse_to_float_number(product.price)}€
                          </Text>
                        </View>
                        {product.sub_products?.map(
                          (subProduct: IProductDetails, index: number) => (
                            <View style={styles.subproducts} key={index}>
                              <Text
                                style={{
                                  ...styles.productTitle,
                                  textDecorationLine:
                                    subProduct.isRefund ||
                                    toCrossed(subProduct.quantity)
                                      ? "line-through"
                                      : "none",
                                }}
                              >
                                {subProduct.quantity} {subProduct.name}
                              </Text>
                              <Text
                                style={{
                                  ...styles.productPrice,
                                  textDecorationLine:
                                    subProduct.isRefund ||
                                    toCrossed(subProduct.quantity)
                                      ? "line-through"
                                      : "none",
                                }}
                              >
                                {parse_to_float_number(subProduct.price)}€
                              </Text>
                            </View>
                          ),
                        )}
                      </React.Fragment>
                    ),
                  )}
                </>
                <View style={styles.totalProducts}>
                  <Text style={styles.productTotalText}>Total payé</Text>
                  <Text style={styles.productTotalText}>
                    {parse_to_float_number(orderEnCours.total_amount)}€
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>

        <View
          style={
            isTablet ? styles.tablet_horizontalLine : styles.horizontalLine
          }
        />

        <View
          style={{
            paddingHorizontal:
              isTablet && Device.brand != "SUNMI" ? 60 : undefined,
            paddingRight: !isTablet ? 25 : undefined,
          }}
        >
          <View style={styles.clientMain}>
            <Text style={styles.subTitles}>MOYEN DE PAYEMENT</Text>
            <View style={styles.clientContainer}>
              <View style={{ flexDirection: "column", width: "100%" }}>
                {orderEnCours.paiement_details?.map(
                  (payment: IPayementDetail, index: number) => (
                    <View style={styles.products} key={index}>
                      <Text style={styles.paymentText}>{payment.type}</Text>
                      <Text style={styles.paymentText}>
                        {parse_to_float_number(payment.amount)}€
                      </Text>
                    </View>
                  ),
                )}
              </View>
            </View>
          </View>
        </View>

        <View
          style={
            isTablet ? styles.tablet_horizontalLine : styles.horizontalLine
          }
        />

        {orderEnCours.commande.comment && (
          <View
            style={{
              paddingHorizontal:
                isTablet && Device.brand != "SUNMI" ? 60 : undefined,
              paddingRight: !isTablet ? 25 : undefined,
            }}
          >
            <View style={styles.clientMain}>
              <Text style={styles.subTitles}>COMMENTAIRE CLIENT</Text>
              <View style={styles.comment}>
                <Text>{orderEnCours.commande.comment}</Text>
              </View>
            </View>
          </View>
        )}

        {IS_ACCEPTED && IS_IN_TAKE_OUT && (
          <React.Fragment>
            <View
              style={
                isTablet ? styles.tablet_horizontalLine : styles.horizontalLine
              }
            />
            <View
              style={{
                paddingHorizontal:
                  isTablet && Device.brand != "SUNMI" ? 60 : undefined,
              }}
            >
              <View style={styles.realPriceContent}>
                <Text style={styles.textThree}>PRIX AU KILO</Text>
                <Text style={styles.realPriceText}>
                  Veuillez saisir le prix réel au client
                </Text>
              </View>
              <View style={styles.realPriceInputMain}>
                <Input
                  value={orderEnCours.newPrice}
                  onChangeText={(text) => onChangePrice(text)}
                  containerStyle={
                    isTablet && Device.brand != "SUNMI"
                      ? styles.amountInputTablet
                      : styles.amountInput
                  }
                  inputStyle={styles.price}
                  inputContainerStyle={styles.input}
                  keyboardType="decimal-pad"
                />
                <Text style={styles.realPriceCurrency}>€</Text>
              </View>
            </View>
          </React.Fragment>
        )}

        {IS_ACCEPTED && IS_IN_TAKE_OUT && (
          <SliderButton
            title="Commande récupérée"
            subTitle={
              "Faites glisser le bouton dès que le client à récupéré la commande"
            }
            railBgColor="rgba(64, 173, 162, 0.12)"
            railStylesBgColor="#40ADA2"
            onSuccess={actionAcceptedOrder}
            subTitleColor={"#000"}
          />
        )}
      </ScrollView>
      <PartnerOrderDetailsSettingOverlay
        visible={openSetting}
        onClose={() => setOpenSetting(false)}
        onOpenRefunds={() => {
          setOpenRefunds(true);
          setOpenSetting(false);
        }}
        onOpenTrade={() => {
          console.log("opening");
          setOpenTrade(true);
          setOpenSetting(false);
        }}
        onOpenprintTicket={() => {
          setOpenTicket(true);
          setOpenSetting(false);
        }}
        onOpencancelOrder={() => {
          setOpenCancelOrder(true);
          setOpenSetting(false);
        }}
        canPrint={partnerProfile.partner?.can_print}
      />
      <PartnerOrderRefundsOverlay
        visible={openRefunds}
        onClose={() => setOpenRefunds(false)}
      />
      <PartnerOrderTradeDiscountOverlay
        visible={openTrade}
        onClose={() => setOpenTrade(false)}
      />
      <PartnerOrderPrintTcketOverlay
        visible={openPrintTicket}
        onClose={() => setOpenTicket(false)}
        onPrint={actionPrintTicket}
        loading={btnLoading}
      />
      <PartnerCancelOrderOverlay
        visible={openCancelOrder}
        onClose={() => setOpenCancelOrder(false)}
        onCancel={actionCancelOrder}
        loading={btnLoading}
      />
      <TimeOverlay
        style={styles.TimeOverlaySize}
        onClose={() => setTimeOverlayVisible(false)}
        visible={timeOverlayVisible}
        id={orderEnCours?.commande?.id}
        toggleOverlay={() => setTimeOverlayVisible(false)}
        orderType={orderEnCours.commande.service_type}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
    position: "relative",
  },
  TimeOverlaySize: {
    marginHorizontal: 27,
    borderRadius: 20,
    marginVertical: 60,
  },
  clientMain: {
    marginLeft: 30,
  },
  subTitles: {
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
  modification: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "#FF8189",
  },
  modificationBox: {
    position: "absolute",
    right: 0,
    marginRight: 32,
  },
  editText: {
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.semiBold,
    color: "#FF5C85",
  },
  clientContainer: {
    marginTop: 10,
    marginRight: 7,
    flexDirection: "row",
    alignItems: "center",
  },
  clientTex: {
    fontSize: 16,
    fontFamily: fontFamily.Medium,
  },
  horizontalLine: {
    height: 1,
    backgroundColor: "#C4C4C4",
    marginVertical: 19,
    marginHorizontal: 10,
  },
  deliveryContainer: {
    marginTop: 10,
    flexDirection: "column",
  },
  deliveryText: {
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    lineHeight: 22,
  },
  hourBold: {
    fontFamily: fontFamily.Bold,
  },
  products: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10,
  },
  subproducts: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginLeft: 15,
    marginBottom: 5,
  },
  productTitle: {
    flex: 9,
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 22,
    paddingRight: 8,
  },
  productPrice: {
    flex: 3,
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 22,
    textAlign: "right",
    paddingLeft: 5,
  },
  totalProducts: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
  },
  productTotalText: {
    fontFamily: fontFamily.Bold,
    fontSize: 16,
    lineHeight: 22,
  },
  paymentText: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 22,
  },
  comment: {
    marginTop: 10,
    width: "100%",
    height: 66,
    borderRadius: 5,
    justifyContent: "center",
    backgroundColor: "rgba(252, 247, 231, 1)",
    alignSelf: "center",
    alignItems: "center",
    marginBottom: 30,
  },
  timerContainer: {
    width: 165,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
    borderBottomLeftRadius: 13.5,
    borderTopLeftRadius: 13.5,
    backgroundColor: "#ED7178",
    flexDirection: "row",
  },
  timerContainerTablet: {
    //width: 165,
    height: 27,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 13.5,
    backgroundColor: "#ED7178",
    flexDirection: "row",
    paddingRight: 15,
    paddingLeft: 15,
  },

  timerText: {
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    color: "#FFFFFF",
    marginLeft: 5,
  },
  realPriceContent: {
    marginTop: -10,
    marginHorizontal: 20,
    paddingBottom: 20,
    flexDirection: "column",
  },
  realPriceText: {
    fontFamily: fontFamily.regular,
    color: "#112842",
    fontSize: 14,
  },
  realPriceInputMain: {
    paddingBottom: 30,
    marginHorizontal: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  realPriceCurrency: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 20,
    marginLeft: 5,
  },
  amountInput: {
    backgroundColor: "#fff",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    width: 100,
    height: 35,
    borderRadius: 10,
  },
  amountInputTablet: {
    backgroundColor: "#fff",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    width: 132,
    height: 35,
    borderRadius: 10,
  },
  input: {
    borderBottomWidth: 0,
    bottom: 3,
  },
  price: {
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    textAlign: "center",
    color: "#112842",
  },
  textThree: {
    fontFamily: fontFamily.Medium,
    color: "#777777",
    fontSize: 12,
    lineHeight: 22,
  },
  /** tablet screen */
  tablet_horizontalLine: {
    height: 1,
    backgroundColor: "#C4C4C4",
    marginVertical: 14,
  },
  textContainer: {
    width: "60%",
  },
  hourPosition: {
    display: "flex",
    flexDirection: "row",
  },
});
