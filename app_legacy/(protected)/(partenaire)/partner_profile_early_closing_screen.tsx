import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { StatusBar } from "expo-status-bar";
import I18n from "i18n-js";
import { fontFamily } from "src/theme/typography";
import { Button } from "react-native-elements";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { PartnerProfileEarlyClosing } from "src_legacy/services/Loaders";
import { authToUserId, to_formatted_date, to_formatted_time } from "src_legacy/services/Utils";
import {
  IPartnerProfileEarlyClosing,
  IPartnerProfileEarlyClosings,
} from "src/interfaces/Entities";
import { profileStore } from "src_legacy/zustore/profilestore";
import useDeviceType from "src/hooks/useDeviceType";
import * as Device from "expo-device";
import { EventRegister } from "react-native-event-listeners";
import { router } from "expo-router";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { authStore } from "src_legacy/services/v2/auth_store";
import { color } from "src/theme";

export default function PartnerProfileEarlyClosingScreen() {
  const { auth } = authStore();
  const { setEarlyClosing } = profileStore();

  const user_id: number = authToUserId(auth);

  const [data, setData] = useState([] as IPartnerProfileEarlyClosings);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);

  const isTablet = useDeviceType();

  const load_or_reload_from_backend = async () => {
    PartnerProfileEarlyClosing(user_id)
      .then((items: IPartnerProfileEarlyClosings) => {
        if (items !== undefined) {
          setData(items);
        } else {
          setError(true);
        }
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setIsLoading(false);
        setError(false);
      });
  };
  useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "EARLY_CLOSING_SHOULD_RELOAD",
      () => load_or_reload_from_backend(),
    );
    //@ts-ignore
    return () => {
      EventRegister.removeEventListener(listener);
    };
  }, []);

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  return (
    <View style={styles.main}>
      <StatusBar style="dark" />
      <ProfileToolbar
        title={
          isTablet && Device.brand != "SUNMI"
            ? ""
            : I18n.t("partner.profile.earlyClosing.title")
        }
      />

      <View
        style={
          isTablet && Device.brand != "SUNMI"
            ? styles.tablet_container
            : styles.container
        }
      >
        <Text
          style={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_title
              : styles.title
          }
        >
          {I18n.t("partner.profile.earlyClosing.text")}
        </Text>
      </View>
      <View style={styles.ClosingContentView}>
        {data.length === 0 ? (
          <View style={styles.noCLosingContent}>
            <Text
              style={{
                fontFamily: fontFamily.Medium,
                fontSize: 16,
                lineHeight: 30,
              }}
            >
              {I18n.t("partner.profile.earlyClosing.noClosing")}
            </Text>
          </View>
        ) : (
          <ScrollView
            style={styles.closingocntent}
            showsVerticalScrollIndicator={false}
          >
            {data &&
              data.map((item: IPartnerProfileEarlyClosing) => (
                <View
                  style={
                    isTablet && Device.brand != "SUNMI"
                      ? styles.tabletItemsContent
                      : styles.itemsContent
                  }
                  key={item.id}
                >
                  <View style={styles.itemLeft}>
                    <Text style={styles.itemTextLeft}>
                      Du {to_formatted_date(item.begin_date.date)} à{" "}
                      {to_formatted_time(item.begin_date.date)}
                    </Text>
                    <Text style={styles.itemTextLeft}>
                      Au {to_formatted_date(item.end_date.date)} à{" "}
                      {to_formatted_time(item.end_date.date)}
                    </Text>
                  </View>
                  <View style={styles.itemRight}>
                    <TouchableOpacity
                      onPress={() => {
                        setEarlyClosing(item);
                        router.push(
                          "/partner_profile_early_closing_timer_screen",
                        );
                      }}
                    >
                      <Text style={styles.itemTextRight}>Modifier</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              ))}
          </ScrollView>
        )}
      </View>
      <View
        style={
          isTablet && Device.brand != "SUNMI" ? styles.tablet_btn : styles.btn
        }
      >
        <Button
          title={I18n.t("partner.profile.earlyClosing.btn")}
          titleStyle={
            isTablet && Device.brand != "SUNMI"
              ? styles.tablet_buttonTitle
              : styles.buttonTitle
          }
          buttonStyle={{
            ...styles.button,
            width: isTablet && Device.brand != "SUNMI" ? 355 : 308,
          }}
          onPress={() => {
            setEarlyClosing(null);
            router.push("/partner_profile_early_closing_timer_screen");
          }}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
  },
  container: {
    paddingLeft: 34,
    paddingRight: 55,
    marginHorizontal: 10,
    paddingTop: 20,
    paddingBottom: 5,
    borderBottomColor: "#C4C4C4",
    borderBottomWidth: 1,
  },
  title: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 22,
    color: "#112842",
  },
  ClosingContentView: {
    flexBasis: "60%",
    flexGrow: 2,
  },
  noCLosingContent: {
    height: "100%",
    justifyContent: "center",
    alignSelf: "center",
  },
  closingocntent: {
    marginHorizontal: 10,
  },
  button: {
    height: 52,
    backgroundColor: "#fff",
    borderColor: color.ACCENT,
    borderWidth: 1,
    borderRadius: 30,
    marginBottom: 30,
  },
  buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
    color: color.ACCENT,
  },
  itemsContent: {
    height: 96,
    width: "100%",
    flexDirection: "row",
    paddingTop: 15,
    borderBottomColor: "#C4C4C4",
    borderBottomWidth: 1,
  },
  itemLeft: {
    width: "80%",
    flexDirection: "column",
    paddingLeft: 15,
  },
  itemRight: {
    width: "20%",
  },
  itemTextLeft: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 30,
    color: "#112842",
  },
  itemTextRight: {
    alignSelf: "flex-end",
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: color.ACCENT,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  btn: {
    alignSelf: "center",
    paddingVertical: 10,
  },
  /** tablet screen */
  text_title: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 20,
    lineHeight: 24.38,
    textAlign: "center",
  },
  tablet_container: {
    width: 704,
    alignSelf: "center",
    paddingTop: 95,
    paddingBottom: 5,
    borderBottomColor: "#C4C4C4",
    borderBottomWidth: 1,
  },
  tablet_btn: {
    alignSelf: "center",
    paddingBottom: 200,
  },
  tabletItemsContent: {
    height: 96,
    width: 704,
    alignSelf: "center",
    flexDirection: "row",
    paddingTop: 15,
    borderBottomColor: "#C4C4C4",
    borderBottomWidth: 1,
  },
  /** tablet screen */
  tablet_title: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 22,
    color: "#112842",
  },
  tablet_buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 18,
    lineHeight: 22,
    color: color.ACCENT,
  },
});
