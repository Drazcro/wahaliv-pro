import {
  Platform,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from "react-native";

import React, { useState } from "react";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { fontFamily } from "src/theme/typography";

import NewUpdateAvailableOverlay from "src_legacy/components/modal/NewUpdateAvailableOverlay/NewUpdateAvailableOverlay";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { authToUserId } from "src_legacy/services/Utils";
import SacreArmandInvoiceTabView from "./sacre_armand_invoice";
import { ClientInvoiceTabView } from "./client_invoice";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
import { authStore } from "src_legacy/services/v2/auth_store";
import { color } from "src/theme";

const TOP_TABS = [
  { key: "first", title: "Sacré Armand" },
  { key: "second", title: "Clients" },
];

const FirstRoute = React.memo(() => {
  const { auth } = authStore();
  const user_id = authToUserId(auth);
  return <SacreArmandInvoiceTabView userId={user_id} />;
});
const SecondRoute = React.memo(() => {
  const { auth } = authStore();
  const user_id = authToUserId(auth);
  return <ClientInvoiceTabView userId={user_id} />;
});
const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

export default function PartnerInvoiceScreen() {
  const { currentTabIndex, setCurrentTabIndex } = tourneeStore();
  const [isNewUpdateAvailable, setIsNewUpdateAvailable] = useState(false);
  const layout = useWindowDimensions();
  const { auth } = authStore();
  const userid = authToUserId(auth);

  return (
    <View style={styles.container}>
      <TabView
        style={styles.tabView}
        navigationState={{ index: currentTabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <Toolbar title={"Mes factures"} />
            <View>
              <TabBar
                {...props}
                indicatorStyle={styles.tabBarIndicatorStyle}
                style={styles.tabBarStyle}
                renderLabel={({ route, focused }) => (
                  <Text
                    style={
                      focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                    }
                  >
                    {route.title}
                  </Text>
                )}
              />
            </View>
            <View style={styles.rappelBg}>
              <View style={styles.rappel}>
                <Text>
                  <Text style={styles.rappelBold}>Rappel: </Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.Medium,
                      fontSize: 14,
                      color: "#1D2A40",
                    }}
                  >
                    Les factures sont payées que{" "}
                  </Text>
                  <Text style={styles.rappelBold}>7 jours ouvrés </Text>
                  <Text
                    style={{
                      fontFamily: fontFamily.Medium,
                      fontSize: 14,
                      color: "#1D2A40",
                    }}
                  >
                    après leur date d'édition.
                  </Text>
                </Text>
              </View>
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setCurrentTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
      <NewUpdateAvailableOverlay
        isVisible={isNewUpdateAvailable}
        onClose={() => setIsNewUpdateAvailable(false)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 50 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  rappel: {
    marginLeft: "5%",
    marginRight: "5%",
    textAlign: "center",
  },
  rappelBold: {
    fontFamily: fontFamily.Bold,
  },
  rappelBg: {
    backgroundColor: "#FFF7F9",
    paddingVertical: "2%",
    marginTop: "5%",
  },
});
