import React, { useEffect, useState } from "react";
import { ScrollView, StyleSheet, View } from "react-native";
import CommandsQuantityController from "src_legacy/components/partenaire/PartenaireInformation/parametres/CommandsQuantityController";
import GeneralInformation from "src_legacy/components/partenaire/PartenaireInformation/parametres/GeneralInformation";
import { Tva } from "src_legacy/components/partenaire/PartenaireInformation/parametres/Tva";
import UsersManager from "src_legacy/components/partenaire/PartenaireInformation/parametres/UsersManager";
import { ParametersDetails } from "src/interfaces/Entities";
import { getBaseUser } from "src_legacy/services";
import { get_parameters_details } from "src_legacy/services/Loaders";
import { authStore } from "src_legacy/services/v2/auth_store";

type Props = {};

const Parameters = (props: Props) => {
  const [ParamsData, setParamsData] = useState({} as ParametersDetails);
  const { auth } = authStore();
  const base_user = getBaseUser(auth);

  useEffect(() => {
    if (!base_user) {
      return;
    }

    get_parameters_details(base_user?.id)
      .then((res) => {
        console.log(res);
        setParamsData(res as unknown as ParametersDetails);
      })
      .catch((err) => console.error(err));
  }, [base_user?.id]);

  return (
    <View style={styles.container}>
      <ScrollView>
        <GeneralInformation generalInfo={ParamsData.general_informations} />
        <Tva tvaInfo={ParamsData.tva_informations} />
        <CommandsQuantityController />
        <UsersManager collaborators={ParamsData?.collab_informations} />
      </ScrollView>
    </View>
  );
};

export default Parameters;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
});
