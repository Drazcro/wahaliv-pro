import React, { useEffect, useState } from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import BandDetails from "src_legacy/components/partenaire/PartenaireInformation/administratif/BandDetails";
import JustitifcationDocuments from "src_legacy/components/partenaire/PartenaireInformation/administratif/JustitifcationDocuments";
import AdminitratifOverlay from "src_legacy/components/partenaire/overlay/AdminitratifOverlay";
import { AdminJusti, BankingInformations } from "src/interfaces/Entities";
import { getBaseUser } from "src_legacy/services";
import {
  get_profile_admin_and_justification,
  update_profile_admin_and_justification,
} from "src_legacy/services/Loaders";
import { authStore } from "src_legacy/services/v2/auth_store";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

const Administratif = () => {
  const [open, setOpen] = useState(false);
  const { auth } = authStore();
  const base_user = getBaseUser(auth);
  const [adminData, setAdminData] = useState({} as AdminJusti);

  function handleOpen() {
    setOpen(true);
  }

  function handleClose() {
    setOpen(false);
  }

  function updateProfileAdminAndJusti() {
    update_profile_admin_and_justification(
      adminData.banking_informations,
      base_user?.id,
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.error(err));
  }

  useEffect(() => {
    if (!base_user) {
      return;
    }

    get_profile_admin_and_justification(base_user.id)
      .then((res) => {
        console.log(res);
        setAdminData(res);
      })
      .catch((err) => console.error(err));
  }, [base_user?.id]);

  return (
    <View style={styles.container}>
      <AdminitratifOverlay open={open} closeOverlay={handleClose} />
      <ScrollView>
        <BandDetails
          bankData={adminData.banking_informations}
          setBankData={setAdminData}
        />
        <JustitifcationDocuments openOverlay={handleOpen} />
        <View
          style={{
            marginTop: 40,
          }}
        >
          <TouchableOpacity
            style={styles.submitBtn}
            onPress={updateProfileAdminAndJusti}
          >
            <Text
              style={{
                color: "#fff",
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
              }}
            >
              Enregistrer les modifications
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Administratif;

const styles = StyleSheet.create({
  container: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    borderRadius: 30,
  },
});
