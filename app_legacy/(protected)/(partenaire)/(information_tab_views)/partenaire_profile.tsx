import React, { useEffect, useState } from "react";
import { TouchableOpacity } from "react-native";
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import InformationJuridique from "src_legacy/components/partenaire/PartenaireInformation/profile/InformationJuridique";
import Interlocuteur from "src_legacy/components/partenaire/PartenaireInformation/profile/Interlocuteur";
import { ProfilePicture } from "src_legacy/components/partenaire/PartenaireInformation/profile/ProfilePicture";
import { AdminJusti, ProfileInformations } from "src/interfaces/Entities";
import { getBaseUser } from "src_legacy/services";
import {
  get_partenaire_profile_information,
  update_partenaire_profile_information,
} from "src_legacy/services/Loaders";
import { authStore } from "src_legacy/services/v2/auth_store";
import { fontFamily } from "src/theme/typography";
import { color } from "src/theme";

export const PartenaireProfile = () => {
  const [profileInfo, setProfileInfo] = useState({} as ProfileInformations);
  const [loading, setLoading] = useState(false);
  const { auth } = authStore();
  const base_user = getBaseUser(auth);

  function flattenProfileData(profileData: any) {
    const flattenedData = {
      ...profileData.legal_informations,
      ...profileData.personal_informations,
    };
    return flattenedData;
  }

  const load_profile_data = () => {
    try {
      setLoading(true);
      get_partenaire_profile_information(base_user?.id)
        .then((res) => {
          console.log(res);
          setProfileInfo(res as ProfileInformations);
        })
        .catch((err) => console.error(err))
        .finally(() => {
          setLoading(false);
        });
    } catch (error) {
      console.error(error);
    }
  };

  const updateProfileInfo = () => {
    const profileBody = flattenProfileData(profileInfo);

    update_partenaire_profile_information(base_user.id, profileBody)
      .then((res) => {
        console.log(res);
      })
      .catch((err) => console.error(err));
  };

  useEffect(() => {
    if (!base_user) {
      return;
    }
    load_profile_data();
  }, [base_user?.id]);

  if (loading || profileInfo === null) {
    return (
      <View
        style={{
          width: "100%",
          height: "100%",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>Chargement des données !</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <ProfilePicture
          pageTitle={profileInfo?.legal_informations?.homepage_title}
        />
        <InformationJuridique
          legalInfo={profileInfo.legal_informations}
          setLegalInfo={setProfileInfo}
        />
        <Interlocuteur
          personalInfo={profileInfo.personal_informations}
          setPersonalInfo={setProfileInfo}
        />
        <View
          style={{
            marginTop: 40,
          }}
        >
          <TouchableOpacity
            style={styles.submitBtn}
            onPress={updateProfileInfo}
          >
            <Text
              style={{
                color: "#fff",
                fontFamily: fontFamily.semiBold,
                fontSize: 16,
              }}
            >
              Enregistrer les modifications
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: "100%",
    gap: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  submitBtn: {
    backgroundColor: color.ACCENT,
    width: "100%",
    height: 52,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30,
    borderRadius: 30,
  },
});
