import React, { useEffect, useState } from "react";

import { StyleSheet, View } from "react-native";


import { TextInput, Text, Divider } from "react-native-paper";
import User from "assets/image_svg/UserRed.svg";
import Euro from "assets/image_svg/EuroRed.svg";
import Commande from "assets/image_svg/Commande.svg";
import Filter from "assets/image_svg/ArrowsDownUpSmall.svg";

import { fontFamily } from "src/theme/typography";
import { ScrollView, TouchableOpacity } from "react-native-gesture-handler";
import { load_partner_customer } from "src_legacy/services/Loaders";
import { authToUserId } from "src_legacy/services/Utils";
import { IPartnerCustomerData } from "src/interfaces/Entities";
import { ClientFilterType } from "src/interfaces/types";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
import { authStore } from "src_legacy/services/v2/auth_store";

export const CLIENT_FILTERS_ARRAY = [
  {
    label: "Ville",
    value: "city",
  },
  {
    label: "Pannier moyen",
    value: "avg_kart",
  },
  {
    label: "Nombre de commnades",
    value: "nb_command",
  },
  {
    label: "Nombre d'annulation",
    value: "nb_cancel",
  },
  {
    label: "Derniere commande",
    value: "last_command",
  },
  {
    label: "CA Total",
    value: "turnover",
  },
] as {
  label: string;
  value: ClientFilterType;
}[];

export default function MyClientsScreen() {
  const { auth } = authStore();

  const [searchQuery, setSearchQuery] = React.useState("");

  const [partnerCustomer, setPartnerCustomer] = useState<
    IPartnerCustomerData[]
  >([]);
  const user_id = authToUserId(auth);
  const [nameFilter, setNameFilter] = useState<boolean>(false);
  const [cmdFilter, setCmdFilter] = useState<boolean>(false);
  const [caFilter, setCAFilter] = useState<boolean>(false);

  useEffect(() => {
    load_partner_customer(user_id).then((data) => {
      setPartnerCustomer(data.data);
    });
  }, []);

  function SortByCommande(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setCmdFilter: ((arg0: boolean) => void) | undefined,
    cmdFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && cmdFilter === false)
      partnerCustomer.sort((a, b) => a.order.nb_orders - b.order.nb_orders);
    setCmdFilter(true);
  }
  function UnSortByCommande(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setCmdFilter: ((arg0: boolean) => void) | undefined,
    cmdFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && cmdFilter === true)
      partnerCustomer.sort((a, b) => b.order.nb_orders - a.order.nb_orders);
    setCmdFilter(false);
  }

  function SortByName(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setNameFilter: ((arg0: boolean) => void) | undefined,
    nameFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && nameFilter === false) {
      partnerCustomer.sort((a, b) => {
        const nameA = a.last_name.toUpperCase();
        const nameB = b.last_name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
    }
    setNameFilter(true);
  }

  function UnSortByName(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setNameFilter: ((arg0: boolean) => void) | undefined,
    nameFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && nameFilter === true) {
      partnerCustomer.sort((a, b) => {
        const nameA = a.last_name.toUpperCase();
        const nameB = b.last_name.toUpperCase();
        if (nameB < nameA) {
          return -1;
        }
        if (nameB > nameA) {
          return 1;
        }

        return 0;
      });
    }
    setNameFilter(false);
  }

  function SortByCA(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setCAFilter: ((arg0: boolean) => void) | undefined,
    caFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && caFilter === false)
      partnerCustomer.sort((a, b) => a.order.ca - b.order.ca);
    setCAFilter(true);
  }
  function UnSortByCA(
    partnerCustomer: IPartnerCustomerData[] | undefined,
    setCAFilter: ((arg0: boolean) => void) | undefined,
    caFilter: boolean,
  ) {
    if (partnerCustomer !== undefined && caFilter === true)
      partnerCustomer.sort((a, b) => b.order.ca - a.order.ca);
    setCAFilter(false);
  }
  const data = React.useMemo(() => {
    if (searchQuery !== "") {
      return (
        partnerCustomer?.filter((item) =>
          Object.values(item).toString().includes(searchQuery),
        ) || []
      );
    }

    return partnerCustomer;
  }, [searchQuery, partnerCustomer]);

  return (
    <React.Fragment>
      <Toolbar title={"Mes clients"} tourneeId={0} />
      <View style={styles.container}>
        <View style={styles.top}>
          <TextInput
            style={styles.textInputStyle}
            contentStyle={styles.textInputContentStyle}
            outlineStyle={styles.textInputOutlineStyle}
            mode="outlined"
            placeholder="Recherche"
            value={searchQuery}
            onChangeText={(text) => setSearchQuery(text)}
            left={
              <TextInput.Icon
                style={{ marginTop: 17 }}
                iconColor="#C4C4C4"
                icon={"magnify"}
              />
            }
          />
        </View>
        <View style={styles.filterHeader}>
          {nameFilter === false ? (
            <TouchableOpacity
              onPress={() =>
                SortByName(partnerCustomer, setNameFilter, nameFilter)
              }
            >
              <View style={styles.displayrowTitleAndLogo}>
                <User />
                <Text style={styles.headerText}>Client</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() =>
                UnSortByName(partnerCustomer, setNameFilter, nameFilter)
              }
            >
              <View style={styles.displayrowTitleAndLogo}>
                <User />
                <Text style={styles.headerText}>Client</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          )}
          {cmdFilter === false ? (
            <TouchableOpacity
              onPress={() =>
                SortByCommande(partnerCustomer, setCmdFilter, cmdFilter)
              }
            >
              <View style={styles.displayrowTitleAndLogo}>
                <Commande />
                <Text style={styles.headerText}>Commandes</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() =>
                UnSortByCommande(partnerCustomer, setCmdFilter, cmdFilter)
              }
            >
              <View style={styles.displayrowTitleAndLogo}>
                <Commande />
                <Text style={styles.headerText}>Commandes</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          )}
          {caFilter === false ? (
            <TouchableOpacity
              onPress={() => SortByCA(partnerCustomer, setCAFilter, caFilter)}
            >
              <View style={styles.displayrowTitleAndLogo}>
                <Euro />
                <Text style={styles.headerText}>CA</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => UnSortByCA(partnerCustomer, setCAFilter, caFilter)}
            >
              <View style={styles.displayrowTitleAndLogo}>
                <Euro />
                <Text style={styles.headerText}>CA</Text>
                <Filter />
              </View>
            </TouchableOpacity>
          )}
        </View>

        <ScrollView style={styles.PartnerCustomerMap}>
          {data?.map((item) => (
            <CustomerDataMap data={item} key={item.id} />
          ))}
        </ScrollView>
      </View>
    </React.Fragment>
  );
}

function CustomerDataMap({ data }: { data: IPartnerCustomerData }) {
  return (
    <View>
      <View style={styles.customerDataMapBody}>
        <View style={styles.customerDataMapTextOnLeft}>
          <Text style={styles.customerDataMapFont}>
            {data.last_name} {data.first_name}
          </Text>
        </View>
        <View style={styles.customerDataMapTextCenter}>
          <Text style={styles.customerDataMapFont}>{data.order.nb_orders}</Text>
        </View>
        <View style={styles.customerDataMapTextOnRight}>
          <Text style={styles.customerDataMapFont}>{data.order.ca}</Text>
        </View>
      </View>
      <View style={styles.divider}>
        <Divider />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    backgroundColor: "white",
    alignItems: "center",
    marginTop: -12,
  },
  top: {
    width: "100%",
    padding: 15,
    paddingHorizontal: 25,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
  },
  textInputOutlineStyle: {
    borderColor: "#C4C4C4",
    borderRadius: 30,
    borderWidth: 1,
  },
  textInputStyle: {
    backgroundColor: "transparent",
    width: "100%",
    height: 34,
  },
  textInputContentStyle: {
    alignItems: "center",
  },
  iconArrow: {
    transform: [{ rotateZ: "270deg" }, { rotateY: "180deg" }],
    marginTop: 5,
  },
  filterHeader: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-evenly",
    backgroundColor: "rgba(237, 237, 238, 0.3)",
  },
  headerText: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    paddingVertical: 10,
    marginLeft: "2%",
    marginRight: "2%",
  },
  displayrowTitleAndLogo: {
    flexDirection: "row",
    alignItems: "center",
  },
  customerDataMapBody: {
    flexDirection: "row",
    marginLeft: "7%",
    marginRight: "15%",
  },
  customerDataMapFont: {
    fontFamily: fontFamily.regular,
    marginVertical: "1%",
    color: "#1D2A40",
  },
  PartnerCustomerMap: {
    width: "100%",
    marginTop: "4%",
  },
  divider: {
    marginHorizontal: "3%",
    marginVertical: "3%",
  },
  customerDataMapTextOnLeft: {
    width: "33.33%",
  },
  customerDataMapTextCenter: {
    width: "33.33%",
    justifyContent: "center",
    alignItems: "center",
    marginLeft: "5%",
  },
  customerDataMapTextOnRight: {
    width: "33.33%",
    justifyContent: "center",
    alignItems: "flex-end",
  },
});
