import { IPartnerData, ORDER_STATUS } from "src/interfaces/Entities";
import React, { useState } from "react";
import {
  RefreshControl,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  View,
} from "react-native";
import { Text } from "react-native-elements";
import {
  get_accepted_orders_for_partner,
  get_new_orders_for_partner,
  reloadOrders,
} from "src_legacy/services/Loaders";
import { fontFamily } from "src/theme/typography";
import { partnerStore } from "src_legacy/zustore/partnerstore";

import { Input } from "@rneui/themed";
import { Entypo } from "@expo/vector-icons";
import * as Notifications from "expo-notifications";

import Search from "assets/image_svg_partner/Search.svg";
import Sac from "assets/image_svg_partner/sac.svg";
import I18n from "i18n-js";
import { authToUserId } from "src_legacy/services/Utils";
import moment from "moment";
import { DefaultTheme } from "react-native-paper";
import { INITIAL_PAGE, INTERVAL_PAGE } from "src/constants/default_values";
import { List } from "react-native-paper";
import { useFocusEffect } from "@react-navigation/native";
import Layout from "src/constants/layout";
import { EventRegister } from "react-native-event-listeners";

import {
  DatabaseReference,
  off,
  onValue,
  ref,
  remove,
} from "firebase/database";
import { db } from "firebase";

import useDeviceType from "src/hooks/useDeviceType";
import { useKeepAwake } from "expo-keep-awake";
import * as Device from "expo-device";
import { soundStore } from "src_legacy/zustore/sound_store";
import { PartnerCards } from "src_legacy/components/partenaire/Cards/partnerCards";
import { PartnerNewCourseOverlay } from "src_legacy/components/partenaire/overlay/PartnerNewCourseOverlay";
import { router } from "expo-router";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src_legacy/services/v2/auth_store";

DefaultTheme.colors.primary = "#1D2A40";

export const NewCoursesTabView = () => {
  useKeepAwake();
  moment.locale("fr");

  const isTablet = useDeviceType();
  const newOrders = partnerStore((state) => state.newOrders);
  const acceptedOrders = partnerStore((state) => state.acceptedOrders);
  const setOrderEnCours = partnerStore((state) => state.setOrderEnCours);

  const { playSound, stopSound } = soundStore();

  const auth = authStore((state) => state.auth);
  const push_token = authStore((state) => state.push_token);
  const user_id = authToUserId(auth);
  console.log(user_id);

  const [filter, setFilter] = useState("");
  const [refreshing, setRefreshing] = React.useState(false);

  const [cardValidatedLoader, setCardValidatedLoader] = useState(true);

  const [expandedNew, setExpandedNew] = React.useState(true);
  const [expandedAccepted, setExpandedAccepted] = React.useState(true);

  const [visible, setVisible] = useState(false);

  const [focusSearch, setFocusSearch] = useState(false);

  const [orderInModal, setOrderInModal] = useState<IPartnerData>();

  const hasOrders = React.useMemo(() => {
    return newOrders.length + acceptedOrders.length > 0;
  }, [newOrders, acceptedOrders]);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const filterNewOrders = React.useMemo(() => {
    if (filter === "" || !filter) {
      return newOrders;
    }
    const filtered_data_set = newOrders.filter((order) => {
      const pickupDate = moment(order.commande.pickup_date.date).format("L");
      return (
        order.customer.name.toLowerCase().includes(filter.toLowerCase()) ||
        order.commande.id.toString().includes(filter.toLowerCase()) ||
        pickupDate.includes(filter.toLowerCase())
      );
    });
    return filtered_data_set;
  }, [filter, newOrders]);

  const filterAcceptedOrders = React.useMemo(() => {
    if (filter === "" || !filter) {
      return acceptedOrders;
    }
    const filtered_data_set = acceptedOrders.filter((order) => {
      const pickupDate = moment(order.commande.pickup_date.date).format("L");
      return (
        order.customer.name.toLowerCase().includes(filter.toLowerCase()) ||
        order.commande.id.toString().includes(filter.toLowerCase()) ||
        pickupDate.includes(filter.toLowerCase())
      );
    });
    return filtered_data_set;
  }, [filter, acceptedOrders]);

  const playAlertSound = async () => {
    console.log("play Alert call");
    const play_status = await playSound();
    console.log("sound played: ", play_status);
    return Promise.resolve();
  };

  const stopAlertSound = async () => {
    const stop_status = await stopSound();
    console.log("sound unloaded: ", stop_status);
    return Promise.resolve();
  };

  const reload_new_and_accepted_order_from_backend = async () => {
    setCardValidatedLoader(true);
    const user_id_ = authToUserId(auth);
    try {
      const data = await get_new_orders_for_partner(
        user_id_,
        INITIAL_PAGE,
        INTERVAL_PAGE,
      );
      console.log(
        "orders loaded: ",
        data.data.length,
        data.total_data,
        data.total,
      );
    } finally {
      //setCardValidatedLoader(false);
      // setCardAcceptedLoader(true);
    }

    try {
      await get_accepted_orders_for_partner(
        user_id_,
        INITIAL_PAGE,
        INTERVAL_PAGE,
      );
    } finally {
      setCardValidatedLoader(false);
      //setCardAcceptedLoader(false);
    }
  };

  const onRefresh = () => {
    console.log("refreshing");
    setRefreshing(true);
    reload_new_and_accepted_order_from_backend().finally(() => {
      setRefreshing(false);
      //setLoadingNewOrder(0);
    });
  };

  const onEvent = (e) => {
    reloadOrders(user_id);
  };

  React.useEffect(() => {
    reload_new_and_accepted_order_from_backend();
    const listener = EventRegister.addEventListener(
      "COMMAND_SHOULD_RELOAD",
      onEvent,
    );
    //@ts-ignore
    return () => {
      EventRegister.removeEventListener(listener);
    };
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      onRefresh();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  React.useEffect(() => {
    const subscription = Notifications.addNotificationReceivedListener(
      (notification) => {
        console.log(notification.request.content.data);
        if (
          notification.request.content.data.alarm &&
          notification.request.content.data.alarm == "yes"
        )
          playAlertSound();
      },
    );
    return () => {
      subscription.remove();
      stopAlertSound();
    };
  }, []);

  useFocusEffect(() => {
    const idUser = authToUserId(auth);
    if (idUser == undefined) return;
    const reference = ref(db, `actualisationCommande/${idUser}/${push_token}`);
    onValue(reference, async (snapshot) => {
      console.log("firebase commnade update");
      if (snapshot.exists()) {
        console.log("firebase -----------");

        try {
          await reloadOrders();
          remove(reference as DatabaseReference);
          //stopAlertSound();
        } catch (error) {
          console.error(error);
          //Sentry.captureException(error);

        }
      }
    });

    return () => {
      off(reference);
    };
  });

  function onPressCard(command: IPartnerData) {
    if (command.commande.status === ORDER_STATUS.NEW) {
      setOrderInModal(command);
      toggleOverlay();
    } else if (
      [ORDER_STATUS.DELIVERED, ORDER_STATUS.ACCEPTED].includes(
        command.commande.status,
      )
    ) {
      setOrderEnCours(command);
      router.push("/order_details_screen");
    }
  }

  return (
    <View style={{ flex: 1 }}>
      {hasOrders && !refreshing ? (
        <Input
          value={filter}
          onChangeText={setFilter}
          containerStyle={styles.searchInputMain}
          inputStyle={styles.searchTextInput}
          inputContainerStyle={styles.searchInput}
          placeholder={I18n.t("partner.order.old.search.label")}
          leftIcon={<Search />}
          onFocus={() => setFocusSearch(true)}
          rightIcon={
            focusSearch ? (
              <TouchableOpacity
                style={styles.icon_right}
                onPress={() => setFilter("")}
              >
                <Entypo name="cross" size={12} color="white" />
              </TouchableOpacity>
            ) : undefined
          }
        />
      ) : null}

      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
        contentContainerStyle={
          !hasOrders ? styles.noOrderScrollView : undefined
        }
      >
        {hasOrders && (
          <List.Section>
            {filterNewOrders.length > 0 && (
              <List.Accordion
                theme={DefaultTheme}
                style={{
                  paddingBottom: 0,
                  paddingTop: 0,
                  backgroundColor: "rgba(245, 245, 245, 1)",
                  marginTop: 20,
                }}
                title="À valider"
                titleStyle={styles.cardCategory}
                expanded={expandedNew}
                onPress={() => setExpandedNew(!expandedNew)}
              >
                <View style={styles.spaceBetweenCategory}>
                  {filterNewOrders.map((item: IPartnerData) => (
                    <PartnerCards
                      course={item}
                      key={item.commande.id}
                      onPress={() => onPressCard(item)}
                    />
                  ))}
                </View>
              </List.Accordion>
            )}

            {acceptedOrders.length > 0 || refreshing ? (
              <List.Accordion
                style={{
                  paddingBottom: 0,
                  paddingTop: 0,
                  backgroundColor: "rgba(245, 245, 245, 1)",
                }}
                title="Acceptées"
                titleStyle={styles.cardCategory}
                expanded={expandedAccepted}
                onPress={() => setExpandedAccepted(!expandedAccepted)}
              >
                <View style={styles.spaceBetweenCategory}>
                  {filterAcceptedOrders.map((item: IPartnerData) => (
                    <PartnerCards
                      course={item}
                      key={item.commande.id}
                      onPress={() => onPressCard(item)}
                    />
                  ))}
                </View>
              </List.Accordion>
            ) : null}
          </List.Section>
        )}

        {!hasOrders && (
          <View style={styles.notOrderContainer}>
            <Sac />
            <Text style={styles.notOrderText}>
              Vous n'avez pas de nouvelles commandes à traiter.
            </Text>
          </View>
        )}
      </ScrollView>

      {orderInModal ? (
        <PartnerNewCourseOverlay
          course={orderInModal}
          toggleOverlay={toggleOverlay}
          visible={visible}
          style={{
            width: "100%",
            height: "95%",
            position: "absolute",
            bottom: isTablet && Device.brand != "SUNMI" ? undefined : 0,
            top: isTablet && Device.brand != "SUNMI" ? 0 : undefined,
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
          }}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  main: {
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: 35,
  },
  cards: {
    marginTop: 15,
  },
  searchInputMain: {
    marginTop: 21,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "#C4C4C4",
    height: 38,
    width: Layout.window.width - 35,
    alignSelf: "center",
  },
  icon_right: {
    backgroundColor: "#ccc",
    width: 20,
    height: 20,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 30,
  },
  searchInput: {
    borderBottomWidth: 0,
    bottom: 5,
  },
  searchTextInput: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    textAlign: "left",
  },
  cardCategory: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
  spaceBetweenCategory: {
    marginBottom: 20,
  },
  noOrderScrollView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  notOrderText: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    color: "#B0B0B0",
    marginTop: 13.78,
    width: "80%",
    textAlign: "center",
  },
  notOrderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    width: "100%",
    minHeight: 600,
  },
});
