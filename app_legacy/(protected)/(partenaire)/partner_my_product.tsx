import React, { useEffect, useState } from "react";
import { WebView } from "react-native-webview";
import { ActivityIndicator, Text, View } from "react-native";
import { authToUserId } from "src_legacy/services/Utils";
import { DEVICE } from "src/constants/default_values";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
import { router } from "expo-router";
import { authStore, vanillaAuthState } from "src_legacy/services/v2/auth_store";
import { http } from "src/services/http";

export default function PartnerMyProductScreen() {
  const { auth } = authStore();
  const URL = vanillaAuthState.getState().base_url;

  const [loading, setLoading] = useState(true);
  const [loader, setLoader] = useState(true);
  const [url, setUrl] = useState();

  useEffect(() => {
    const user_id = authToUserId(auth);
    const _url = `${URL}partenaire/${user_id}/link/produit`;
    http.get(_url).then((r) => {
      if (r?.status !== 200) {
        router.back();
      } else {
        setUrl(r.data.url);
        setLoading(false);
      }
    });
  }, [router]);

  if (loading || url == undefined) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Text>Chargement en cours</Text>
      </View>
    );
  }

  return (
    <React.Fragment>
      <Toolbar title={"Mes produits"} />
      <WebView he source={{ uri: url }} onLoad={() => setLoader(false)} />
      {loader && (
        <ActivityIndicator
          color="#1D2A40"
          style={{
            position: "absolute",
            top: DEVICE.height / 2,
            left: DEVICE.width / 2,
          }}
        />
      )}
    </React.Fragment>
  );
}
