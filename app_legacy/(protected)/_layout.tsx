import { Stack } from "expo-router";
import React from "react";
import useCachedResources from "src/hooks/useCachedResources";
import { PermissionProvider } from "src_legacy/session/context";
import { authStore } from "src_legacy/services/v2/auth_store";

export default function RootStack() {
  const isLoadingComplete = useCachedResources();
  const { auth } = authStore();

  if (!isLoadingComplete) {
    return null;
  }

  return (
    <PermissionProvider>
      {auth.access_token ? (
        <Stack screenOptions={{ headerShown: false, animation: "none" }} />
      ) : null}
    </PermissionProvider>
  );
}
