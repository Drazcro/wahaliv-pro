import React from "react";
import { StyleSheet, View } from "react-native";
import { StatusBar } from "expo-status-bar";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import FaqComponent from "src_legacy/components/delivery/profile/faq_component";

export default function FaqScreen() {
  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ProfileToolbar title="Aide et contact" />
      <FaqComponent />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    height: "100%",
  },
});
