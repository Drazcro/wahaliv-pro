import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { BugComponent } from "src_legacy/components/delivery/profile/bug_component";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { KeyboardAvoidingView, Platform, StyleSheet, View } from "react-native";

export default function BugScreen() {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <StatusBar style="dark" />
      <ProfileToolbar title="Informer un bug" />
      <View style={styles.bugComponentPosition}>
        <BugComponent />
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  bugComponentPosition: {
    height: "100%",
    backgroundColor: "white",
    marginTop: -10,
  },
});
