import {
  FAQBonus,
  FAQKilometrage,
  FAQLocation,
  FAQWire,
} from "src_legacy/components/delivery/profile/help_component";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { useLocalSearchParams } from "expo-router";
import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, View } from "react-native";
import { profileStore } from "src_legacy/zustore/profilestore";

export default function HelpScreen() {
  const { id } = useLocalSearchParams();

  const profile = profileStore((v) => v.profile);

  const getComponentByID = React.useMemo(() => {
    switch (id) {
      case "BONUS":
        return (
          <FAQBonus
            title={"Je ne vois plus mes bonus "}
            object={"Je ne vois plus mes bonus"}
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
            `}
          />
        );
      case "WIRE":
        return (
          <FAQWire
            title={"Je n’ai pas reçu mon/mes virement(s)"}
            object={"Je n’ai pas reçu mon/mes virement(s) "}
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
              Paiement montant attendu: 
            `}
          />
        );
      case "KM":
        return (
          <FAQKilometrage
            title={
              "J’ai un problème avec le nombre de kilomètres et la rémunération."
            }
            object={
              "Il y a un problème avec le nombre de kilomètres et la rémunération"
            }
            body={`
              Nom et prénom: ${profile.firstName} ${profile.lastName}
              Ville: ${profile.idCity}
              Km réel (Google Maps): 
              N° de la commande concernée: 
            `}
          />
        );
      case "LOCATION":
        return (
          <FAQLocation title="Je n’arrive pas à me positionner, je ne sais pas comment faire." />
        );
      default:
        return;
    }
  }, [id]);

  return (
    <>
      <StatusBar style="dark" />
      <ProfileToolbar title="Aide" />
      <View style={styles.helpComponentPosition} key={getComponentByID.key}>
        {getComponentByID}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  helpComponentPosition: {
    height: "100%",
    backgroundColor: "white",
  },
});
