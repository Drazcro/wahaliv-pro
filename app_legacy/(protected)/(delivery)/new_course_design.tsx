import {
  RefreshControl,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import React, { useState } from "react";

import ScooterWaitBlack from "assets/ScooterWaitBlack.svg";

import { ICourseV2, ICourseV2Data } from "src/interfaces/Entities";
import { ActivityIndicator } from "react-native-paper";
import moment from "moment";

import { authToUserId } from "src_legacy/services/Utils";
import { get_new_course_v2 } from "src_legacy/services/Loaders";

import { EventRegister } from "react-native-event-listeners";
import { soundStore } from "src_legacy/zustore/sound_store";
import { NewCourseCard } from "src_legacy/components/delivery/cards/courses/new_course_card";
import { router, usePathname } from "expo-router";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src_legacy/services/v2/auth_store";

const EMPTY_COURSE_V2 = {
  total_data: 0,
  total: 0,
  data: [],
  hasTournee: false,
  tournee_begin_at: moment().format(),
} as ICourseV2Data;

export function NewCourseDesign({
  newCoursesData,
  refreshing,
  refreshCourses,
  isLoading,
  onTouchStart,
}: {
  newCoursesData: ICourseV2Data;
  refreshing: boolean;
  refreshCourses: () => void;
  isLoading: boolean;
  onTouchStart: () => void;
}) {
  const newCourses = newCoursesData.data;

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading} onTouchStart={onTouchStart}>
        <ScrollView contentContainerStyle={styles.loading_scroll}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }
  return (
    <React.Fragment>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={refreshCourses} />
        }
        contentContainerStyle={
          newCourses.length == 0
            ? { flexGrow: 1, justifyContent: "center" }
            : {}
        }
      >
        {newCourses.length != 0 ? (
          newCourses.map((course: ICourseV2) => (
            <NewCourseCard
              course={course}
              key={course.id_commande + Math.random() * 1000}
            />
          ))
        ) : (
          <NoCourse />
        )}
      </ScrollView>
    </React.Fragment>
  );
}

function NoCourse() {
  return (
    <View style={styles.no_course_main}>
      <ScooterWaitBlack />
      <Text style={styles.no_course_title}>Pas encore de course</Text>
      <Text style={styles.no_course_description}>
        Attend quelques minutes et une
      </Text>
      <Text style={styles.no_course_description}>
        nouvelle course apparaitra
      </Text>
    </View>
  );
}

export const CourseBottomTabV2 = () => {
  const [refreshing, setRefreshing] = React.useState(true);
  const [newCoursesV2Data, setNewCoursesV2Data] = useState(EMPTY_COURSE_V2);

  const { playSound, stopSound } = soundStore();

  const playAlertSound = async () => {
    console.log("play Alert call");

    const play_status = await playSound();
    console.log("sound played: ", play_status);
  };

  const stopAlertSound = async () => {
    const stop_status = await stopSound();
    console.log("sound unloaded: ", stop_status);
  };

  const auth = authStore((v) => v.auth);

  const updateCourses = async () => {
    setRefreshing(true);
    const userId = authToUserId(auth);
    if (userId == undefined) {
      console.log("user undefined");
      return;
    }

    try {
      const fetched_data = await get_new_course_v2(userId);
      //console.log(fetched_data);
      const new_data = fetched_data.data.filter(
        (item) => !newCoursesV2Data.data.includes(item),
      );
      setNewCoursesV2Data(fetched_data);
      if (new_data.length > 0) {
        router.push("/delivery_tab/courses");
        console.log("we have new courses");
        playAlertSound();
      } else {
        console.log("no new courses found");
        stopAlertSound();
      }

      return Promise.resolve(new_data.length > 0);
    } catch (e) {
      console.error(e);
      //Sentry.captureException(e);
      stopAlertSound();
      return Promise.resolve(false);
    } finally {
      setRefreshing(false);
    }
  };

  const onRefresh = () => {
    updateCourses();
  };

  React.useEffect(() => {
    updateCourses();
  }, []);

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "COURSE_SHOULD_RELOAD",
      onRefresh,
    );

    return () => {
      //@ts-ignore
      EventRegister.removeEventListener(listener);
    };
  }, []);

  React.useEffect(() => {
    const interval = setInterval(() => {
      onRefresh();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  return (
    <NewCourseDesign
      onTouchStart={() => stopAlertSound()}
      newCoursesData={newCoursesV2Data}
      refreshing={refreshing}
      refreshCourses={onRefresh}
      isLoading={refreshing}
    />
  );
};

const styles = StyleSheet.create({
  /** no course styles */
  no_course_main: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  no_course_title: {
    fontFamily: fontFamily.Medium,
    fontSize: 16,
    lineHeight: 20,
    marginTop: 18,
    marginBottom: 11,
    color: "#1D2A40",
  },
  no_course_description: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 17,
    color: "#1D2A40",
  },
  /** message tournee styles */
  message_tournee_main: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  message_tournee_text_regular: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  message_tournee_text_bold: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: "#1D2A40",
  },
  message_tournee_text_bold_pink: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 22,
    color: "#FF5C85",
  },
  /** loading render styles */
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loading_scroll: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
});
