import React, { useEffect, useState } from "react";
import { db } from "firebase";
import { off, onValue, ref, remove } from "firebase/database";
import { get_course_details, showToast } from "src_legacy/services/Loaders";
import { CoursesBody } from "../../../src_legacy/components/delivery/course_body";
import { getBaseUser, IBaseUser } from "src_legacy/services/Utils";
import { COURSE_STATUS } from "src/constants/course";
import { courseStore } from "src_legacy/zustore/coursestore";
import { router, usePathname } from "expo-router";
import { authStore } from "src_legacy/services/v2/auth_store";

function CoursesEncoursDeliveryContent() {
  //hooks
  const { courseEnCours } = courseStore();

  const { auth, push_token } = authStore();
  // selector
  const [, setError] = useState(false);
  const [baseUser, setBaseUser] = useState<IBaseUser>();
  console.log("ID_DELIVERY", courseEnCours.id_delivery);
  useEffect(() => {
    if (auth.access_token && auth.access_token !== "") {
      setBaseUser(getBaseUser(auth));
    }
  }, [auth]);

  const actionGoBack = () => {
    router?.back();
  };
  const checkOrderIsRetrievable = () => {
    const canceled = "La course a été annulée";

    get_course_details(courseEnCours.id_delivery)
      .then((newCourse) => {
        if (
          newCourse.delivery_status === COURSE_STATUS.CANCELED_BY_PARTNER ||
          newCourse.delivery_status === COURSE_STATUS.CANCELED_BY_SACRE
        ) {
          showToast("error", canceled);
          actionGoBack();
          return;
        }
      })
      .catch((e) => setError(true));
  };

  useEffect(() => {
    const reference = ref(db, `actualisations/${baseUser?.id}/${push_token}`);
    onValue(reference, (snapshot) => {
      if (snapshot.exists()) {
        checkOrderIsRetrievable();
        remove(reference);
      }
    });
    return () => {
      off(reference);
    };
  }, [push_token]);
  return <CoursesBody />;
}

export default function CoursesEncoursDelivery() {
  return <CoursesEncoursDeliveryContent />;
}
