import React, { useEffect, useState } from "react";
import { RefreshControl, Text, TouchableOpacity, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";
import { ICourse } from "src/interfaces/Entities";
import { authToUserId, NumberOfSkeleton } from "src_legacy/services/Utils";
import { get_finished_course } from "src_legacy/services/Loaders";
import { useIsFocused } from "@react-navigation/native";
import { EventRegister } from "react-native-event-listeners";
import { Input } from "@rneui/themed";
import Search from "assets/image_svg/SearchIcon.svg";
import { fontFamily } from "src/theme/typography";
import DeliveryCardV2 from "src_legacy/components/delivery/cards/delivery_card/delivery_card_v2";
import ClearSearch from "assets/image_svg/ClearCourseSearch.svg";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src_legacy/services/v2/auth_store";

export function OldCoursesTabView({ isOnTournee }: { isOnTournee?: boolean }) {
  const [Finished, setFinished] = useState([] as ICourse[]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const [searchedValue, setSearchedValue] = useState("");

  const { auth } = authStore();
  const isFocused = useIsFocused();

  const user_id = authToUserId(auth);

  const onRefresh = () => {
    setRefreshing(true);
    setIsLoading(true);
    get_finished_course(user_id)
      .then((courses) => {
        setFinished(courses);
      })
      .finally(() => {
        setRefreshing(false);
        setIsLoading(false);
      });
  };

  React.useEffect(() => {
    const listener = EventRegister.addEventListener(
      "COURSE_SHOULD_RELOAD",
      onRefresh,
    );
    return () => {
      //@ts-ignore
      EventRegister.removeEventListener(listener);
    };
  }, []);

  useEffect(() => {
    if (!auth.access_token) {
      return;
    }
    get_finished_course(user_id)
      .then((courses) => setFinished(courses))
      .catch((e) => {
        setError(true);
        console.error(e);
        //Sentry.captureException(e);
      })
      .finally(() => setIsLoading(false));
  }, [auth, isFocused]);

  const filteredData = React.useMemo(() => {
    if (searchedValue === "" || !searchedValue) {
      return Finished;
    }
    return Finished.filter((item) => {
      return (
        item.id_commande.toString().includes(searchedValue) ||
        item.commande_at.date.includes(searchedValue) ||
        item.delivery_date.date.includes(searchedValue) ||
        item.customer.includes(searchedValue) ||
        item.partner_telephone.includes(searchedValue) ||
        item.customer_telephone.includes(searchedValue) ||
        item.delivery_address.address.includes(searchedValue)
      );
    });
  }, [searchedValue, Finished]);

  if (error === true) {
    return <Text>erreur de chargement</Text>;
  }

  return (
    <>
      <View>
        <Input
          inputContainerStyle={{
            borderWidth: 1,
            borderRadius: 20,
            paddingLeft: 15,
            marginTop: 20,
            marginBottom: -10,
            height: 37,
            borderColor: "#C4C4C4",
          }}
          inputStyle={{
            fontSize: 14,
            fontFamily: fontFamily.Medium,
            marginLeft: 5,
            color: "rgba(119, 119, 119, 0.79)",
          }}
          placeholder="Rechercher par numéro, nom ou date"
          leftIcon={<Search />}
          rightIcon={
            <TouchableOpacity
              style={{
                marginRight: 15,
              }}
              onPress={() => setSearchedValue("")}
            >
              {searchedValue !== "" && <ClearSearch />}
            </TouchableOpacity>
          }
          defaultValue={searchedValue}
          onChangeText={(newValue) => setSearchedValue(newValue)}
        />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {isLoading
          ? NumberOfSkeleton.map((i: number) => (
              <React.Fragment key={i}></React.Fragment>
            ))
          : filteredData.map((item: ICourse) => (
              <DeliveryCardV2
                isOnTournee={isOnTournee}
                course={item}
                key={item.id_commande * Math.random() * 10}
                onAcceptPrompt={() => console.log("on accept")}
              />
            ))}
      </ScrollView>
    </>
  );
}
