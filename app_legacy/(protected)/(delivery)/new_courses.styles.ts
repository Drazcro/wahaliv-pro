import { StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

export const newCourseStyles = StyleSheet.create({
  emptyListContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  titleText: {
    paddingTop: 25,
    paddingBottom: 5,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "#8D98A0",
  },
  descriptionText: {
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 12,
    paddingHorizontal: 60,
    color: "#8D98A0",
  },
  CourseAccepted: {
    fontSize: 16,
    marginLeft: 29,
    marginTop: 31,
    marginBottom: 12,
    fontFamily: fontFamily.semiBold,
  },
  CourseState: {
    fontSize: 16,
    marginLeft: 29,
    marginTop: 12,
    marginBottom: 12,
    fontFamily: fontFamily.semiBold,
  },
});
