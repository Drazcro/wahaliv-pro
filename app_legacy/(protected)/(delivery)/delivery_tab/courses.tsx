import {
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  useWindowDimensions,
  View,
} from "react-native";

import React, { useEffect, useState } from "react";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { fontFamily } from "src/theme/typography";
import { LocationOverlay } from "src_legacy/components/delivery/courses/modal/location_overlay";
import { Button, Icon, Overlay } from "react-native-elements";
import Rappel from "assets/image_svg/rappel.svg";
import Close from "assets/image_svg/X.svg";
import { CourseBottomTabV2 } from "../new_course_design";
import { OldCoursesTabView } from "../old_courses";
import { color } from "src/theme";

const TOP_TABS = [
  { key: "first", title: "Nouvelles" },
  { key: "second", title: "Toutes" },
];

const FirstRoute = React.memo(() => {
  return <CourseBottomTabV2 />;
});

const SecondRoute = React.memo(() => {
  return <OldCoursesTabView isOnTournee={false} />;
});
const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

export default function CourseV2TopTab() {
  const [tabIndex, setTabIndex] = React.useState(0);
  const layout = useWindowDimensions();

  const [showReminder, setShowReminder] = useState(false);

  useEffect(() => {
    // Function to update showReminder to true
    const updateReminder = () => {
      setShowReminder(true);
    };

    const interval = setInterval(updateReminder, 7 * 24 * 60 * 60 * 1000); 

    return () => clearInterval(interval);
  }, []);

  return (
    <View style={styles.container}>
      <Overlay
        isVisible={showReminder}
        overlayStyle={{
          justifyContent: "center",
          alignItems: "center",
          gap: 10,
          width: 350,
          height: 341,
          borderRadius: 10,
          position: "relative",
          paddingHorizontal: 25,
          paddingTop: 34,
          paddingBottom: 21,
        }}
      >
        <TouchableOpacity
          style={{
            position: "absolute",
            right: 11,
            top: 12,
          }}
          onPress={() => setShowReminder(false)}
        >
          <Close />
        </TouchableOpacity>
        <Rappel />
        <Text
          style={{
            fontFamily: fontFamily.semiBold,
            fontSize: 16,
            color: "#1D2A40",
            marginTop: 19.32,
          }}
        >
          RAPPEL: Sac isotherme obligatoire !
        </Text>
        <Text
          style={{
            textAlign: "center",
            fontFamily: fontFamily.Medium,
            color: "#1D2A40",
            lineHeight: 18,
          }}
        >
          Tu dois obligatoirement te présenter{" "}
          <Text
            style={{
              fontFamily: fontFamily.Bold,
              lineHeight: 18,
            }}
          >
            chez le resto et chez le client{" "}
          </Text>
          avec le sac isotherme.
        </Text>
        <Text
          style={{
            textAlign: "center",
            fontFamily: fontFamily.Medium,
            color: "#1D2A40",
            fontSize: 14,
            lineHeight: 18,
          }}
        >
          C'est indispensable pour maintenir la chaleur des repas. Merci de ta
          coopération.
        </Text>
        <TouchableOpacity
          style={{
            width: "100%",
            height: 45,
            backgroundColor: color.ACCENT,
            justifyContent: "center",
            marginTop: 28,
            borderRadius: 21,
            paddingHorizontal: 81.61,
            paddingVertical: 11.25,
          }}
          onPress={() => setShowReminder(false)}
        >
          <Text
            style={{
              textAlign: "center",
              color: "#fff",
              fontSize: 14,
              fontFamily: fontFamily.semiBold,
            }}
          >
            D’accord{" "}
          </Text>
        </TouchableOpacity>
      </Overlay>
      <TabView
        style={styles.tabView}
        navigationState={{ index: tabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <View style={styles.tabViewHeader}>
              <Text style={styles.tabViewHeaderLabel}>Mes Courses</Text>
            </View>
            <View>
              <TabBar
                {...props}
                indicatorStyle={styles.tabBarIndicatorStyle}
                style={styles.tabBarStyle}
                renderLabel={({ route, focused }) => (
                  <Text
                    style={
                      focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                    }
                  >
                    {route.title}
                  </Text>
                )}
              />
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
      <LocationOverlay />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 50 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  reminderContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
    width: 318,
    height: 341,
  },
});
