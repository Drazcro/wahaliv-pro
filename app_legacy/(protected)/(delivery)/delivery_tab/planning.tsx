// import React from "react";
// import {
//   Platform,
//   StyleSheet,
//   Text,
//   useWindowDimensions,
//   View,
// } from "react-native";

// import { TabView, SceneMap, TabBar } from "react-native-tab-view";
// import { fontFamily } from "src/theme/typography";
// import { Calendars } from "./calendar/calendars/calendars";
// import { Summary } from "./calendar/summary/summary";

// const TOP_TABS = [
//   { key: "first", title: "Calendrier" },
//   { key: "second", title: "Résumé" },
// ];

// const renderScene = SceneMap({
//   first: Calendars,
//   second: Summary,
// });

// export default function Calendar() {
//   const layout = useWindowDimensions();
//   const [tabIndex, setTabIndex] = React.useState(0);

//   return (
//     <View style={styles.container}>
//       <TabView
//         style={styles.tabView}
//         navigationState={{ index: tabIndex, routes: TOP_TABS }}
//         renderTabBar={(props) => (
//           <View>
//             <View style={styles.tabViewHeader}>
//               <Text style={styles.tabViewHeaderLabel}>Calendrier</Text>
//             </View>
//             <View>
//               <TabBar
//                 {...props}
//                 indicatorStyle={styles.tabBarIndicatorStyle}
//                 style={styles.tabBarStyle}
//                 renderLabel={({ route, focused }) => (
//                   <Text
//                     style={
//                       focused ? styles.tabBarLabelFocused : styles.tabBarLabel
//                     }
//                   >
//                     {route.title}
//                   </Text>
//                 )}
//               />
//             </View>
//           </View>
//         )}
//         renderScene={renderScene}
//         onIndexChange={setTabIndex}
//         initialLayout={{ width: layout.width }}
//         keyboardDismissMode="none"
//       />
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: "#fff",
//     alignItems: "center",
//     justifyContent: "center",
//   },
//   tabBarStyle: {
//     shadowOffset: { height: 0, width: 0 },
//     shadowColor: "transparent",
//     shadowOpacity: 0,
//     elevation: 0,
//     backgroundColor: "#fff",
//     borderBottomWidth: 1,
//     borderColor: "#C4C4C4",
//     Bottom: 10,
//   },
//   tabBarIndicatorStyle: {
//     backgroundColor: "#FF5C85",
//     height: 4,
//     bottom: -2,
//     borderRadius: 2,
//   },
//   tabViewHeader: {
//     flexDirection: "row",
//     paddingTop: Platform.OS === "android" ? 30 : 60,
//     backgroundColor: "#FFF",
//   },
//   tabViewHeaderLabel: {
//     flexGrow: 1,
//     fontFamily: fontFamily.Medium,
//     fontSize: 20,
//     color: "#000",
//     textAlign: "center",
//   },
//   tabView: {
//     width: "100%",
//   },
//   tabBarLabel: {
//     fontFamily: fontFamily.semiBold,
//     color: "#112842",
//     fontSize: 14,
//     textAlign: "center",
//     marginBottom: -10,
//   },
//   tabBarLabelFocused: {
//     fontFamily: fontFamily.semiBold,
//     color: "#FF5C85",
//     fontSize: 14,
//     textAlign: "center",
//     marginBottom: -10,
//   },
// });

import React from "react";
import {
  Platform,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from "react-native";

import { TabView, TabBar } from "react-native-tab-view";
import { fontFamily } from "src/theme/typography";
import { StatusBar } from "expo-status-bar";
import * as Location from "expo-location";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { Calendars } from "../calendars";
import { Summary } from "../summary";
const TOP_TABS = [
  { key: "first", title: "Calendrier" },
  { key: "second", title: "Résumé" },
];

export default function PlanningScreen() {
  const layout = useWindowDimensions();
  const [tabIndex, setTabIndex] = React.useState(0);
  const [date, setDate] = React.useState(new Date());

  React.useEffect(() => {
    Location.getCurrentPositionAsync().catch((e) => {
      console.warn(e);
    });
  }, []);

  const renderScene = ({ route }: { route: any }) => {
    switch (route.key) {
      case "first":
        return <Calendars date={date} setDate={setDate} />;
      case "second":
        return (
          <Summary
            setDate={setDate}
            setTabIndex={setTabIndex}
            tabIndex={tabIndex}
          />
        );
    }
  };

  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ProfileToolbar title="Calendrier" dontHaveGoBack />
      <TabView
        style={styles.tabView}
        navigationState={{ index: tabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <View>
              <TabBar
                {...props}
                indicatorStyle={styles.tabBarIndicatorStyle}
                style={styles.tabBarStyle}
                renderLabel={({ route, focused }) => (
                  <Text
                    style={
                      focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                    }
                  >
                    {route.title}
                  </Text>
                )}
              />
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    position: "relative",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: "#FF5C85",
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 30 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.Medium,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: "#FF5C85",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  icon: {
    position: "relative",
    alignSelf: "flex-start",
    top: Platform.OS === "ios" ? 83 : 54,
    left: 25.25,
    zIndex: 2,
    width: 50,
  },
});
