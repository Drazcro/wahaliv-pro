import {
  Platform,
  StyleSheet,
  Text,
  useWindowDimensions,
  View,
} from "react-native";

import React from "react";
import { TabView, SceneMap } from "react-native-tab-view";
import { fontFamily } from "src/theme/typography";
import NewUpdateAvailableOverlay from "src_legacy/components/modal/NewUpdateAvailableOverlay/NewUpdateAvailableOverlay";
import { NewTourneesTabView } from "../new_tournees";
import { OldCoursesTabView } from "../old_courses";
import { color } from "src/theme";

const TOP_TABS = [
  { key: "first", title: "Nouvelles Tournées" },
  { key: "second", title: "Courses passées" },
];

const FirstRoute = React.memo(() => {
  return <NewTourneesTabView />;
});
const SecondRoute = React.memo(() => {
  return <OldCoursesTabView isOnTournee />;
});

const renderScene = SceneMap({
  first: FirstRoute,
  second: SecondRoute,
});

export default function HomeScreen() {
  const [tabIndex, setTabIndex] = React.useState(0);
  const [isNewUpdateAvailable, setIsNewUpdateAvailable] = React.useState(false);
  const layout = useWindowDimensions();

  return (
    <View style={styles.container}>
      <TabView
        style={styles.tabView}
        navigationState={{ index: tabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <View style={styles.tabViewHeader}>
              <Text style={styles.tabViewHeaderLabel}>Mes tournées</Text>
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
      <NewUpdateAvailableOverlay
        isVisible={isNewUpdateAvailable}
        onClose={() => setIsNewUpdateAvailable(false)}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 50 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    color: "#000",
    textAlign: "center",
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
});
