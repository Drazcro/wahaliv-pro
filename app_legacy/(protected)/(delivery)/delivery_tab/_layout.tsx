import {
  Stack,
  Tabs,
  usePathname,
  useRootNavigation,
  useSegments,
} from "expo-router";
import { fontFamily } from "src/theme/typography";
import ScooterIcon from "assets/image_svg/ScooterGrey.svg";
import ScooterRed from "assets/image_svg/ScooterArmand.svg";

import TourneeIcon from "assets/icons/TourneesGray.svg";
import TourneeIconRed from "assets/icons/Tournees.svg";

import CalendarIcon from "assets/image_svg/CalendarTab.svg";
import CalendarRed from "assets/image_svg/CalendarTabRose.svg";

import UserIcon from "assets/image_svg/UserLite.svg";
import UserRed from "assets/image_svg/UserRed.svg";
import React, { useEffect, useMemo, useState } from "react";
import { StatusBar } from "expo-status-bar";
import { useFocusEffect } from "@react-navigation/native";
import { authToUserId } from "src_legacy/services/Utils";
import {
  DatabaseReference,
  Query,
  Unsubscribe,
  off,
  onValue,
  ref,
  remove,
  serverTimestamp,
  set,
} from "firebase/database";
import { db } from "firebase";
import { EventRegister } from "react-native-event-listeners";
import { soundStore } from "src_legacy/zustore/sound_store";

import * as Notifications from "expo-notifications";
import { router } from "expo-router";
import { LocationOverlay } from "src_legacy/components/delivery/courses/modal/location_overlay";
import { useTrackLocation } from "src_legacy/hooks/useTrackLocation";
//import * as Sentry from "@sentry/react-native";
// import * as Sentry from "sentry-expo";
import Constants from "expo-constants";
import useChat from "src_legacy/hooks/useChat";
import {
  ROLE_TYPE,
  getBaseUser,
  shouldUpdateFromStore,
} from "src_legacy/services/Utils";
import { profileStore } from "src_legacy/zustore/profilestore";
import { encode } from "base-64";
import {
  recover_delivery_information_and_state_for_profile,
  save_device_info,
  set_notification_as_read,
} from "src_legacy/services/Loaders";
import { Platform } from "react-native";
import moment from "moment";
import * as Device from "expo-device";
import { useKeepAwake } from "expo-keep-awake";
import { useAppState } from "src_legacy/hooks/useAppState";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { PermissionProvider } from "src_legacy/session/context";
import { authStore } from "src_legacy/services/v2/auth_store";

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: false,
  }),
});

const getPushToken = () => {
  if (!Constants.isDevice) {
    return Promise.reject("Must use physical device for Push Notifications");
  }

  try {
    return Notifications.getPermissionsAsync()
      .then((statusResult) => {
        return statusResult.status !== "granted"
          ? Notifications.requestPermissionsAsync()
          : statusResult;
      })
      .then((statusResult) => {
        if (statusResult.status !== "granted") {
          throw "Failed to get push token for push notification!";
        }
        return Notifications.getExpoPushTokenAsync({
          //applicationId: "@helodev/sacrearmand_pro_app",
          projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
        });
      })
      .then((tokenData) => tokenData.data);
  } catch (error) {
    return Promise.reject("Couldn't check notifications permissions");
  }
};

const PATHS = ["/courses_en_cours_delivery", "/tournee_detail"];

export default function DeliveryLayout() {
  const path = usePathname();
  const { playSound, stopSound } = soundStore();
  const { auth, push_token } = authStore();


  const [block, setBlock] = useState(false);

  // const isReady = React.useMemo(() => {
  //   const navigatorReady = rootNavigationState?.key != null;
  //   return navigation?.isReady && navigatorReady;
  // }, [auth, router, navigation, rootNavigationState]);

  const { unreadCount } = useChat();

  const base_user = React.useMemo(() => {
    return getBaseUser(auth);
  }, [auth]);

  const [out_of_date, set_out_of_date] = React.useState(false);
  const profile = profileStore((v) => v.profile);

  const getChatPath = React.useMemo(() => {
    if (!base_user.roles) {
      return "";
    }

    const formattedString = `${base_user.id}|-|;${base_user.email}|-|;${
      base_user.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0]
    }`;

    return encode(formattedString);
  }, [base_user]);

  let reference: Query;

  React.useEffect(() => {
    if (unreadCount > 0) {

    //   vanillaToastStore
    //     .getState()
    //     .showToast(
    //       "newMessage",
    //       "Consulte ta boîte de réception pour le découvrir.",
    //       "Nouveau message reçu !",
    //     );
    }
  }, [unreadCount]);

  React.useEffect(() => {
    save_device_info()
      .then((response) => {
        //console.log(response);
        //console.log(expo_token);
      })
      .catch((error) => {
        console.error(error);
        //Sentry.captureException(error);
      });
  }, []);

  React.useEffect(() => {
    const OS = Platform.OS;

    reference = ref(db, `proAppVersions/${OS}`);
    onValue(reference, async (snapshot) => {
      console.log("checking remote app version");
      const user = getBaseUser(auth);
      if (snapshot.exists() && user) {
        const value: string = snapshot.val();
        console.log("comparing remote app version", value);
        const should_update = shouldUpdateFromStore(user.email, value);
        console.log(should_update);
        set_out_of_date(should_update || base_user.is_latest_version === false);
      }
    });

    console.log("|||||||||||||||| ", base_user.is_latest_version);
    return () => {
      off(reference);
    };
  }, []);

  const getFullName = React.useMemo(() => {
    if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
      const account = profile;
      return `${account.firstName ?? ""} ${account.lastName ?? ""}`;
    }
  }, [profile, base_user]);

  const decodedPushToken = React.useMemo(() => {
    return encode(push_token);
  }, [push_token]);

  React.useEffect(() => {
    if (!base_user.roles) {
      return;
    }

    if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER) && !profile.email) {
      recover_delivery_information_and_state_for_profile(base_user.id);
    }

    const chat_ref = ref(db, "chat/" + getChatPath);

    onValue(chat_ref, async (snapshot) => {
      if (!snapshot.exists()) {
        const metaDataMessage = {
          email: base_user.email,
          full_name: `${getFullName}`,
          role: base_user.roles.filter(
            (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
          )[0],
          id: base_user.id,
          sent_by_user: false,
          content: "",
          sent_at: moment().format(),
          timestamp: serverTimestamp(),
          meta: "meta",
        };
        set(chat_ref, {
          initial: {
            content: "Bonjour",
            read: false,
            sent_by_user: false,
            sent_at: moment().format(),
            timestamp: serverTimestamp(),
          },
          chat_metadata: {
            email: base_user.email,
            full_name: `${getFullName}`,
            role: base_user.roles.filter(
              (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
            )[0],
            id: base_user.id,

            lastMessage: metaDataMessage,
            unread_for: {
              supervision: 0,
              user: 0,
            },
          },
        });
      }
    });

    return () => {
      off(chat_ref);
    };
  }, [base_user, profile]);

  React.useEffect(() => {
    let unsubscribe: Unsubscribe;
    if (!base_user || !base_user.id) {
      console.log("BASE USER MISSING");
    }

    let outer_decoded_token = "";
    let outer_token = "";
    try {
      const role =
        base_user.roles.filter(
          (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
        )[0] ?? "NO_ROLE";
      const decoded_token = (
        Device.isDevice || push_token.length > 1
          ? push_token
          : Device.designName ?? Device.deviceName ?? Device.osName ?? "noname"
      ).replace(" ", "");

      const token = encode(decoded_token);

      console.log("TOKEN: ", token);

      outer_decoded_token = decoded_token;
      outer_token = token;

      if (token === "" || decoded_token === "") {
        // Sentry.captureException(
        //   "EXPO_PUSH_TOKEN empty " +
        //     JSON.stringify(base_user) +
        //     token +
        //     decoded_token,
        // );
        return;
      }

      const db_ref = ref(db, `multi_device/${role}/${base_user.id}/${token}`);

      unsubscribe = onValue(db_ref, async (result) => {
        if (!result.exists()) {
          try {
            save_device_info().then((v) => console.log(v));
          } catch (error) {
            console.error(error);
            //Sentry.captureException(error);
          }

          console.log("Device case 1");
          set(db_ref, decoded_token);
        }
        // else {
        //   const scalar = result.val();
        //   console.log(scalar, "scalar");

        //   const registered_devices = Object.values(scalar) ?? [];
        //   console.log(
        //     registered_devices,
        //     "registered",
        //     `multi_device/${role}/${base_user.id}`,
        //   );

        //   if (!(registered_devices.includes(token)) && token.length > 1) {
        //     const new_device_objects = {
        //       ...Object.values(scalar),
        //       [token]: token,
        //     };
        //     console.log("Device case 2");
        //     set(db_ref, new_device_objects);
        //   }
        // }
      });
    } catch (error) {
      // Sentry.captureException(
      //   "sentry error: " + error + outer_token + outer_decoded_token,
      // );
    }

    return () => {
      if (unsubscribe) {
        unsubscribe();
      }
    };
  }, [push_token, base_user]);

  React.useEffect(() => {
    getPushToken().then((token) => setPushToken(token));

    notificationListener.current =
      Notifications.addNotificationReceivedListener((notification) => {
        console.log(notification);
      });

    responseListener.current =
      Notifications.addNotificationResponseReceivedListener((response) => {
        console.log(response);
        const notif_id = response.notification.request.content.data[
          "id"
        ] as string;
        if (notif_id && notif_id != undefined && notif_id != "undefined") {
          set_notification_as_read(notif_id);
        } else {
          console.log("Notification id is not available");
        }
      });

    return () => {
      Notifications.removeNotificationSubscription(
        notificationListener.current,
      );
      Notifications.removeNotificationSubscription(responseListener.current);
    };
  }, []);

  useKeepAwake();
  const appStateVisible = useAppState();

  React.useEffect(() => {
    appStateVisible;
  }, []);

  const fb_auth = getAuth();
  const { setPushToken } = authStore();

  // const [expoPushToken, setExpoPushToken] = useState("");
  const notificationListener = React.useRef<any>();
  const responseListener = React.useRef<any>();

  // ----------- Sound Player -------------- //
  React.useEffect(() => {
    if (!fb_auth.currentUser || fb_auth.currentUser.isAnonymous) {
      signInWithEmailAndPassword(
        fb_auth,
        "cedric@sacrearmand.com",
        "wq6JrGgXhjhV5Lu",
      );
    }

    Notifications.getExpoPushTokenAsync()
      .then((pushToken) => {
        console.log("[TOKEN]: ", pushToken);
        if (pushToken.data !== "") {
          setPushToken(pushToken.data);
        }
      })
      .catch((e) => {
        console.log("CANT FETCH TOKEN");
        console.error(e);
        //Sentry.captureException(e);
      });

    Notifications.getExpoPushTokenAsync({
      //applicationId: "@helodev/sacrearmand_pro_app",
      projectId: "b0904124-db05-4663-91ba-a8a38929ec06",
    })
      .then((pushToken) => {
        console.log("[TOKEN]: ", pushToken);
        if (pushToken.data !== "") {
          setPushToken(pushToken.data);
        }
      })
      .catch((e) => {
        console.log("CANT FETCH TOKEN");
        console.error(e);

        //Sentry.captureException(e);
      });
  }, [fb_auth, setPushToken]);

  const playAlertSound = async () => {
    console.log("play Alert call");

    const play_status = await playSound();
    console.log("sound played: ", play_status);
  };

  const stopAlertSound = async () => {
    const stop_status = await stopSound();
    console.log("sound unloaded: ", stop_status);
  };

  React.useEffect(() => {
    const subscription = Notifications.addNotificationReceivedListener(
      (notification) => {
        console.log(notification.request.content.data);
        if (
          notification.request.content.data.alarm &&
          notification.request.content.data.alarm == "yes"
        )
          playAlertSound();
      },
    );
    return () => {
      subscription.remove();
      stopAlertSound();
    };
  }, []);

  function moveFocus() {
    router.push("/delivery_tab/courses");
  }

  useFocusEffect(() => {
    const idUser = authToUserId(auth);
    if (idUser == undefined) return;
    const reference = ref(db, `actualisations/${idUser}/${push_token}`);
    onValue(reference, async (snapshot) => {
      console.log("firebase course update.", snapshot.val());
      if (snapshot.exists()) {
        try {
          EventRegister.emitEvent("COURSE_SHOULD_RELOAD", { firebase: true });
          remove(reference as DatabaseReference);
          moveFocus();
          //playAlertSound();
        } catch (e) {
          console.error(e);
          //Sentry.captureException(e);
        }
      }
    });
    return () => off(reference);
  });


  React.useEffect(() => {
    return () => {
      stopAlertSound();
    };
  }, []);

  useEffect(() => {
    console.log("user: ", auth.access_token);
  }, [base_user]);

  const tabDisplay = useMemo(() => {
    if (PATHS.includes(path)) {
      return "none";
    } else {
      return "flex";
    }
  }, [path]);

  return (
    <PermissionProvider>
      <Stack.Screen />
      <StatusBar style="dark" />
      <Tabs
        screenOptions={{
          headerShown: false,
          tabBarStyle: {
            display: tabDisplay,
          },
        }}
      >
        <Tabs.Screen
          name="courses"
          options={{
            tabBarLabel: "Courses",
            title: "Courses",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              lineHeight: 20,
              marginTop: -6,
            },
            tabBarActiveTintColor: "rgba(255, 92, 133, 1)",
            headerShown: false,
            tabBarIcon: ({ color }) => {
              if (color === "rgba(255, 92, 133, 1)") {
                return <ScooterRed />;
              } else {
                return <ScooterIcon />;
              }
            },
          }}
        />

        <Tabs.Screen
          name="tournee"
          options={{
            tabBarLabel: "Tournee",
            title: "Tournee",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              marginTop: -6,
              lineHeight: 20,
            },
            tabBarActiveTintColor: "rgba(255, 92, 133, 1)",

            headerShown: false,
            tabBarIcon: ({ color }) => {
              if (color === "rgba(255, 92, 133, 1)") {
                return <TourneeIconRed />;
              } else {
                return <TourneeIcon />;
              }
            },
          }}
        />
        <Tabs.Screen
          name="planning"
          options={{
            tabBarLabel: "Planning",
            title: "Planning",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              marginTop: -6,
              lineHeight: 20,
            },
            tabBarActiveTintColor: "rgba(255, 92, 133, 1)",

            headerShown: false,
            tabBarIcon: ({ color }) => {
              if (color === "rgba(255, 92, 133, 1)") {
                return <CalendarRed />;
              } else {
                return <CalendarIcon />;
              }
            },
          }}
        />
        <Tabs.Screen
          name="profile"
          options={{
            tabBarLabel: "Profile",
            title: "Profile",
            tabBarLabelStyle: {
              fontSize: 11,
              fontFamily: fontFamily.Medium,
              marginTop: -6,
              lineHeight: 20,
            },
            tabBarActiveTintColor: "rgba(255, 92, 133, 1)",

            headerShown: false,
            tabBarIcon: ({ color }) => {
              if (color === "rgba(255, 92, 133, 1)") {
                return <UserRed />;
              } else {
                return <UserIcon />;
              }
            },
          }}
        />
      </Tabs>
      {block ? <LocationOverlay /> : null}
    </PermissionProvider>
  );
}
