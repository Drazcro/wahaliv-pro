import React, { useEffect, useState } from "react";
import {
  ImageBackground,
  Platform,
  ScrollView,
  Text,
  View,
} from "react-native";
import i18n from "i18n-js";
import { StatusBar } from "expo-status-bar";
import { Button, Divider, Switch } from "react-native-elements";

import { images } from "src/theme";
import Constants from "expo-constants";

import ThumbsUp from "assets/image_svg/ThumbsUpRed.svg";
import ScooterArmand from "assets/image_svg/ScooterArmand.svg";
import TimerRed from "assets/image_svg/TimerRed.svg";
import { authToUserId, parse_to_float_number } from "src_legacy/services/Utils";
import {
  get_is_on_service,
  recover_delivery_information_and_state_for_profile,
  update_delivery_auto_valid_status,
  recover_delivery_information_for_profile,
} from "src_legacy/services/Loaders";

import { IProfile } from "src/interfaces/Entities";
import Toast from "react-native-expo-popup-notifications";
import { clearAuth } from "src_legacy/session/store";
import { unregisterForServiceStatusCheck } from "src/cron/delivery_men_service_check";
import {
  getTrackingTaskStatus,
  isTrackingTaskRegistered,
  registerBackgroundFetchAsync,
  unregisterBackgroundFetchAsync,
} from "src/cron/foreground_localisation_task";
import * as Location from "expo-location";

import { ProfileStyle as styles } from "src_legacy/components/delivery/profile/styles";
import { ProfileInformations } from "src_legacy/components/delivery/profile/profile_informations";
import { ProfileNavigationItems } from "src_legacy/components/delivery/profile/profile_navigation_items";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { useTrackLocation } from "src_legacy/hooks/useTrackLocation";
import { GainsInformations } from "src_legacy/components/delivery/profile/components/gains_informations";
import { ServiceThreshold } from "src_legacy/components/delivery/profile/components/service_threshold";
import { MissingDocs } from "src_legacy/components/delivery/profile/components/missing_docs";
import { authStore } from "src_legacy/services/v2/auth_store";
import { saveTokenToFirebase } from "src_legacy/services";

export default function ProfileScreen() {
  const steps = tourneeStore((v) => v.steps);
  const [isEnable, setIsEnabled] = useState<boolean>();
  const [profile, setProfil] = useState<IProfile>({} as IProfile);
  const [error, setError] = useState(false);
  const { auth, removeAuth } = authStore();
  const [isLoading, setIsLoading] = useState(true);
  const [isDocOk, setIsDocOk] = useState(true);
  const [userId, setUserID] = useState<number>();

  const { isRegistered, toggleFetchTask } = useTrackLocation();

  React.useEffect(() => {
    toggleFetchTask();
  }, []);

  useEffect(() => {
    if (auth.access_token && auth.access_token !== "") {
      console.log("access_token:", auth.access_token);
      saveTokenToFirebase(auth);
      setUserID(authToUserId(auth));
      recover_delivery_information_for_profile(authToUserId(auth));
    }
  }, [auth]);

  const onChangeSwitch = () => {
    setIsEnabled((previousState) => !previousState);
    if (userId === undefined) {
      return false;
    }
    update_delivery_auto_valid_status(userId, {
      "is-auto-valid": isEnable,
    })
      .then(() => {
        Toast.show({
          type: "success",
          text1: "Success",
          text2: i18n.t("profile.notification.success.message"),
        });
      })
      .catch(() => setError(true))
      .finally(() => {
        setIsLoading(false);
        setError(false);
      });
  };

  useEffect(() => {
    if (!auth.access_token) {
      return;
    }
    const user_id: number = authToUserId(auth);
    recover_delivery_information_and_state_for_profile(user_id)
      .then((items: IProfile) => {
        console.log(items);
        if (items !== undefined) {
          setIsEnabled(items.autoValid);
          setProfil(items);
          console.log(items);
        } else {
          setError(true);
        }
      })
      .catch(() => setError(true))
      .finally(() => {
        setError(false);
        setIsLoading(false);
      });
    Location.getCurrentPositionAsync().catch((e) => {
      console.warn(e);
    });
    get_is_on_service(user_id);
  }, [auth.access_token]);

  if (!auth.access_token) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
        <Button
          title={"logout"}
          onPress={() => {
            if (Platform.OS !== "web") {
              unregisterForServiceStatusCheck();
              unregisterBackgroundFetchAsync();
            }
            removeAuth();
            clearAuth();
          }}
        >
          Logout
        </Button>
      </View>
    );
  }

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ScrollView
        bounces={false}
        showsVerticalScrollIndicator={false}
        automaticallyAdjustContentInsets={false}
        overScrollMode={"never"}
      >
        <View style={styles.header}>
          <ImageBackground
            source={images.Landing.Ellipse}
            style={styles.background_image}
            resizeMode="cover"
          ></ImageBackground>
          <View style={styles.text_group}>
            <Text style={styles.text_name}>
              {i18n.t("profile.welcome")} {profile?.firstName || profile?.name}
            </Text>
            <Text style={styles.text_name}>{i18n.t("profile.espace")}</Text>
            <Text style={styles.text_email}>{profile?.email}</Text>
          </View>
        </View>
        <View style={styles.group_content}>
          <Text style={styles.informationsTitle}>MES INFORMATIONS DU MOIS</Text>
          <GainsInformations
            unit="€"
            title={i18n.t("profile.Earnings")}
            value={parse_to_float_number(profile.earnings)}
            startText="Dont"
            textValue={`${parse_to_float_number(profile.bonus)} €`}
            endText=" de bonus"
            tipTextStart="Dont"
            tipTextValue={parse_to_float_number(profile?.tipEarning)}
            tipTextEnd="de pourboires"
          />
          <View style={styles.wrapper}>
            <ProfileInformations
              unit="min"
              title={i18n.t("profile.averageDelay")}
              value={parse_to_float_number(profile.averageDelay)}
              icon={<TimerRed />}
              startText="Sur"
              textValue={parse_to_float_number(profile.nbTransactions)}
              endText="transactions"
              tipTextValue={0}
            />
            <ProfileInformations
              unit="%"
              title={i18n.t("profile.deliveryNote")}
              value={parse_to_float_number(profile.ratePercentage)}
              icon={<ThumbsUp />}
              startText="Sur"
              textValue={parse_to_float_number(profile.rateCount)}
              endText="notations"
              tipTextValue={0}
            />
            <ProfileInformations
              title={i18n.t("profile.deliveries")}
              value={Number(profile.coursesCount)}
              icon={<ScooterArmand />}
              tipTextValue={0}
            />
          </View>
          <ServiceThreshold data={profile.minimum_garanti} />

          <View style={styles.line}>
            <Divider color="rgba(196, 196, 196, 1)" />
          </View>
          <View style={{ flex: 1, marginHorizontal: 5 }}>
            <View style={styles.group_accept_auto}>
              <Text style={styles.text_auto}>
                {i18n.t("profile.positionSharingLabel")}
              </Text>
            </View>
            <View style={styles.accept_button}>
              <Text style={styles.text_accept}>
                {i18n.t("profile.positionSharingText")}
              </Text>
              <Text></Text>

              <Switch
                ios_backgroundColor="red"
                trackColor={{
                  true: "rgba(64, 173, 162, 1)",
                  false: "rgba(243, 106, 114, 1)", // Green color for false
                }}
                color={
                  isRegistered === true
                    ? "rgba(64, 173, 162, 1)" // Green color for false
                    : "rgba(243, 106, 114, 1)"
                }
                onValueChange={toggleFetchTask}
                value={isRegistered}
              />
            </View>
          </View>

          <View style={styles.line}>
            <Divider color="rgba(196, 196, 196, 1)" />
          </View>
          {profile.isSalarie !== true ? (
            <React.Fragment>
              <View style={{ flex: 1, marginHorizontal: 5 }}>
                <View style={styles.group_accept_auto}>
                  <Text style={styles.text_auto}>
                    {i18n.t("profile.automaticAssignment")} {profile.isSalarie}
                  </Text>
                </View>
                <View style={styles.accept_button}>
                  <Text style={styles.text_accept}>
                    {i18n.t("profile.automaticAcceptance")}
                  </Text>
                  <Text></Text>
                  <Switch
                    ios_backgroundColor="red"
                    trackColor={{
                      true: "rgba(64, 173, 162, 1)",
                      false: "rgba(243, 106, 114, 1)",
                    }}
                    color={
                      isEnable == true
                        ? "rgba(64, 173, 162, 1)"
                        : "rgba(243, 106, 114, 1)"
                    }
                    onValueChange={onChangeSwitch}
                    value={isEnable}
                  />
                </View>
              </View>

              <View style={styles.line}>
                <Divider color="rgba(196, 196, 196, 1)" />
              </View>
            </React.Fragment>
          ) : null}

          <MissingDocs setDoc={(v) => setIsDocOk(v)} />
          <View style={{ marginTop: 10 }}>
            <ProfileNavigationItems
              isSalarie={profile.isSalarie}
              docIsOk={isDocOk}
            />
          </View>
        </View>
        {Constants.expoConfig?.version && (
          <Text style={styles.appVersionLabel}>
            V{Constants.expoConfig?.version}
          </Text>
        )}
      </ScrollView>
    </View>
  );
}
