import React, { useEffect, useState } from "react";
import { Overlay } from "react-native-elements";

import { ITournee, ITourneeAPIResponse } from "src/interfaces/Entities";

import { authToUserId } from "src_legacy/services/Utils";
import { load_tournee_data } from "src_legacy/services/Loaders";
import {
  RefreshControl,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import { ScrollView } from "react-native-gesture-handler";
import { TOURNEE_STATUS } from "src/constants/tournee";

import ScooterWait from "assets/ScooterWait.svg";

import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { ActivityIndicator } from "react-native-paper";
import { useFocusEffect } from "@react-navigation/native";
import {
  DatabaseReference,
  off,
  onValue,
  ref,
  remove,
} from "firebase/database";
import { db } from "firebase";
import { CardTournee } from "src_legacy/components/delivery/cards/tournee";
import { AcceptTourneOverlay } from "src_legacy/components/delivery/courses/modal/accept_tournee_overlay";
import {
  registerBackgroundFetchAsync,
  unregisterBackgroundFetchAsync,
} from "src/cron/foreground_localisation_task";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src_legacy/services/v2/auth_store";

export const NewTourneesTabView = () => {
  const { auth, push_token } = authStore();
  const [tournees, setTournees] = useState<ITourneeAPIResponse>({
    total_data: 0,
    total: "0",
    data: [],
  });
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [refreshing, setRefreshing] = useState(false);
  const [openModalSignal, setOpenModalSignal] = useState(false);

  const { shouldReloadTournee, setShouldReloadTournee, setSelectedTournee } =
    tourneeStore();

  useEffect(() => {
    load_or_reload_from_backend();
  }, []);

  useEffect(() => {
    if (shouldReloadTournee === true) {
      load_or_reload_from_backend();
    }
  }, [shouldReloadTournee]);

  const load_or_reload_from_backend = async () => {
    const idUser = authToUserId(auth);
    try {
      const tournee_data_from_backend = await load_tournee_data(idUser);

      const news =
        tournees.data && tournee_data_from_backend.data
          ? tournee_data_from_backend.data
              .filter((item) => !tournees.data.some((j) => j.id === item.id))
              .map((item) => item.id)
          : [];
      const shouldOpenPopup =
        tournees.total_data < tournee_data_from_backend.total_data ||
        news.length > 0;
      setTournees(tournee_data_from_backend);
      console.log(`loaded ${tournee_data_from_backend.total} tournee`);
      setSelectedTournee(tournee_data_from_backend.data[0]); // set initial selectedTournee
      setShouldReloadTournee(false);
      return Promise.resolve(shouldOpenPopup);
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error)
      setError(true);
      return Promise.resolve(false);
    } finally {
      setError(false);
      setIsLoading(false);
    }
  };

  const newTournee = React.useMemo(() => {
    if (!tournees.data) return [];
    return tournees.data.filter((item) => item.status == TOURNEE_STATUS.SENDED);
  }, [tournees]);

  const acceptedTournee = React.useMemo(() => {
    if (!tournees.data) return [];
    return tournees.data.filter(
      (item) => item.status == TOURNEE_STATUS.ACCEPTED,
    );
  }, [tournees]);

  const onRefresh = () => {
    setRefreshing(true);
    load_or_reload_from_backend().finally(() => {
      setRefreshing(false);
    });
  };

  useFocusEffect(() => {
    const idUser = authToUserId(auth);
    if (idUser == undefined) return;
    const reference = ref(db, `actualisationTournee/${idUser}/${push_token}`);
    onValue(reference, async (snapshot) => {
      console.log("firebase tournee update.", snapshot.val(), tournees);
      if (snapshot.exists()) {
        try {
          await load_or_reload_from_backend();
          remove(reference as DatabaseReference);
        } catch (e) {
          console.error(e);
          //Sentry.captureException(e);
        }
      }
    });
    return () => off(reference);
  });

  React.useEffect(() => {
    try {
      if (acceptedTournee.length > 0) {
        registerBackgroundFetchAsync();
      } else {
        unregisterBackgroundFetchAsync();
      }
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error);
    }
  }, [acceptedTournee]);
  console.log("re render");

  const DataBlock = React.useCallback(() => {
    if (acceptedTournee.length > 0) {
      if (refreshing) {
        return <View style={{ marginTop: 30 }}></View>;
      } else {
        return (
          <View style={{ marginTop: 30 }}>
            <TourneeItem
              title="Acceptées"
              show={acceptedTournee.length > 0}
              items={acceptedTournee}
              shouldRefresh={() => setShouldReloadTournee(true)}
            />
          </View>
        );
      }
    } else if (acceptedTournee.length === 0) {
      return (
        <View style={styles.emptyListContainer}>
          <ScooterWait />
          <Text style={styles.titleText}>Pas encore de tournée</Text>
          <Text style={styles.descriptionText}>
            Attend quelques minutes et une nouvelle tournée apparaitra
          </Text>
        </View>
      );
    } else {
      return <React.Fragment />;
    }
  }, [
    tournees.total_data,
    refreshing,
    acceptedTournee.length,
    newTournee.length,
    newTournee,
    tournees,
  ]);

  // ----------------  Render ---------------- //
  if (error) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView
          contentContainerStyle={styles.scrollView}
          refreshControl={
            <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
          }
        >
          <View>
            <Text>Erreur de chargement ... </Text>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  if (isLoading === true) {
    return (
      <SafeAreaView style={styles.loading}>
        <ScrollView contentContainerStyle={styles.scrollView}>
          <Text>Chargement en cours ...</Text>
          <ActivityIndicator />
        </ScrollView>
      </SafeAreaView>
    );
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView
        contentContainerStyle={
          tournees.total_data === 0 ? styles.scrollView : undefined
        }
        onTouchStart={() => {
          //stopAlertSound();
        }}
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
        }
      >
        {<DataBlock />}
      </ScrollView>

      {/** Modal open by signal firebase */}
      {openModalSignal && (
        <Overlay
          isVisible={openModalSignal}
          overlayStyle={styles.overlayBackground}
          onBackdropPress={() => {
            //stopAlertSound();
            setOpenModalSignal(false);
          }}
        >
          <AcceptTourneOverlay
            onClose={() => {
              setOpenModalSignal(false);
            }}
          />
        </Overlay>
      )}
    </SafeAreaView>
  );
};

const TourneeItem = ({
  show,
  items,
  title,
  shouldRefresh,
}: {
  show: boolean;
  items: Array<ITournee>;
  title: string;
  shouldRefresh: () => void;
}) => {
  return (
    <View>
      {show &&
        items.map((t: ITournee, index) => (
          <View key={index}>
            <CardTournee tournee={t} onClose={shouldRefresh} />
          </View>
        ))}
    </View>
  );
};

const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  scrollView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  emptyListContainer: {
    marginTop: 200,
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  titleText: {
    paddingTop: 25,
    paddingBottom: 5,
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "#8D98A0",
  },
  descriptionText: {
    textAlign: "center",
    fontFamily: fontFamily.regular,
    fontSize: 12,
    paddingHorizontal: 60,
    color: "#8D98A0",
  },
  typeText: {
    fontSize: 16,
    marginLeft: 29,
    marginBottom: 11,
    fontFamily: fontFamily.semiBold,
  },
  overlayBackground: {
    backgroundColor: "rgba(0, 0, 0, 0.7)",
  },
});
