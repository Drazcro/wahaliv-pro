import { Text, useWindowDimensions, View } from "react-native";

import React from "react";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { styles } from "./invoice.style";
import { FuturInvoiceTabView, InvoiceTabView } from "./invoice_tab_view";
import { StatusBar } from "expo-status-bar";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";

const TOP_TABS = [
  { key: "first", title: "Prochainement facturées" },
  { key: "second", title: "Facturées" },
];

const renderScene = SceneMap({
  first: FuturInvoiceTabView,
  second: InvoiceTabView,
});

export default function InvoiceScreen() {
  const [tabIndex, setTabIndex] = React.useState(0);
  const layout = useWindowDimensions();

  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ProfileToolbar title="Mes Factures" />
      <TabView
        style={styles.tabView}
        navigationState={{ index: tabIndex, routes: TOP_TABS }}
        renderTabBar={(props) => (
          <View>
            <View>
              <TabBar
                {...props}
                indicatorStyle={styles.tabBarIndicatorStyle}
                style={styles.tabBarStyle}
                renderLabel={({ route, focused }) => (
                  <Text
                    style={
                      focused ? styles.tabBarLabelFocused : styles.tabBarLabel
                    }
                  >
                    {route.title}
                  </Text>
                )}
              />
            </View>
          </View>
        )}
        renderScene={renderScene}
        onIndexChange={setTabIndex}
        initialLayout={{ width: layout.width }}
        keyboardDismissMode="none"
      />
    </View>
  );
}
