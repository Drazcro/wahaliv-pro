import React, { useEffect, useState } from "react";
import {
  Dimensions,
  Platform,
  SafeAreaView,
  ScrollView,
  Text,
  View,
} from "react-native";
import { Divider } from "react-native-elements";
import { StyleSheet, TouchableOpacity } from "react-native";
import { fontFamily } from "src/theme/typography";
import { showLocation } from "react-native-map-link";

import Euro from "assets//image_svg/Euro.svg";
import EuroGris from "assets//image_svg/EuroGris.svg";
import StorefrontRed from "assets//image_svg/StorefrontRed.svg";
import UserCircle from "assets//image_svg/UserCircle.svg";
import StorefrontGrey from "assets//image_svg/StorefrontGrey.svg";
import UserCircleGrey from "assets//image_svg/UserCircleGrey.svg";
import CaretDowns from "assets/image_svg/CaretDowns.svg";
import CaretUp from "assets/image_svg/CaretUp.svg";
import CaretLeft from "assets/image_svg/CaretLeft.svg";
import Phone from "assets/image_svg/Phone.svg";
import EmporterSmall from "assets/image_svg/EmporterSmall.svg";
import Phone_Disabled from "assets/image_svg/Phone_Disabled.svg";
import * as Location from "expo-location";
import { useFocusEffect } from "@react-navigation/native";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import {
  GetDetailTournee,
  ready_to_go_to_pick_up,
  complete_step,
} from "src_legacy/services/Loaders";
import { IStep, ITourneeDetail } from "src/interfaces/Entities";
import {
  TOURNEE_STEP_STATE,
  TOURNEE_STEP_STATUS,
  TOURNEE_STEP_TYPE,
} from "src/constants/tournee";
import {
  authToUserId,
  callHiddenNumber,
  getBaseUser,
  hour_to_string,
} from "src_legacy/services/Utils";

import {
  DatabaseReference,
  off,
  onValue,
  ref,
  remove,
  update,
} from "firebase/database";
import { db } from "firebase";
import { IsBusyOverlay } from "src_legacy/components/overlays/is_busy_overlay";
import { busyStore } from "src/zustore/busy_store";
import { SliderButton } from "src_legacy/components/slider_button/slider_button";

import { EventRegister } from "react-native-event-listeners";
import ScooterGrey from "assets/image_svg/ScooterGrey.svg";
import CarryGrey from "assets/image_svg/CarryGrey.svg";

import PAST_2 from "assets/image_svg/PAST_2.svg";
import PAST_3 from "assets/image_svg/PAST_3.svg";
import PAST_4 from "assets/image_svg/PAST_4.svg";
import PAST_5 from "assets/image_svg/PAST_5.svg";
import PAST_6 from "assets/image_svg/PAST_6.svg";

import ACTUAL_2 from "assets/image_svg/ACTUAL_2.svg";
import ACTUAL_3 from "assets/image_svg/ACTUAL_3.svg";
import ACTUAL_4 from "assets/image_svg/ACTUAL_4.svg";
import ACTUAL_5 from "assets/image_svg/ACTUAL_5.svg";
import ACTUAL_6 from "assets/image_svg/ACTUAL_6.svg";

import FUTURE_2 from "assets/image_svg/FUTURE_2.svg";
import FUTURE_3 from "assets/image_svg/FUTURE_3.svg";
import FUTURE_4 from "assets/image_svg/FUTURE_4.svg";
import FUTURE_5 from "assets/image_svg/FUTURE_5.svg";
import FUTURE_6 from "assets/image_svg/FUTURE_6.svg";
import MapPreviewTournee from "src_legacy/components/delivery/maps/map_preview_tournee";

import { router } from "expo-router";
import ActiveChatBtn from "assets/image_svg/ActiveChatBtn.svg";
import InactiveChatBtn from "assets/image_svg/InactiveChatBtn.svg";
import { PartnerLateTourneeOverlay } from "src_legacy/components/delivery/courses/modal/partner_late_tournee_overlay";
import { ClientAbsentTourneeOverlay } from "src_legacy/components/delivery/courses/modal/client_absent_tournee_overlay";
import { FinishedTourneeOverlay } from "src_legacy/components/delivery/courses/modal/finished_tournee_overlay";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
//import * as Sentry from "@sentry/react-native";
import { authStore } from "src_legacy/services/v2/auth_store";

const { width: screenWidth } = Dimensions.get("window");

const ICONS = {
  PAST: {
    2: PAST_2,
    3: PAST_3,
    4: PAST_4,
    5: PAST_5,
    6: PAST_6,
  },
  ACTUAL: {
    2: ACTUAL_2,
    3: ACTUAL_3,
    4: ACTUAL_4,
    5: ACTUAL_5,
    6: ACTUAL_6,
  },
  FUTURE: {
    2: FUTURE_2,
    3: FUTURE_3,
    4: FUTURE_4,
    5: FUTURE_5,
    6: FUTURE_6,
  },
};

export default function TourneeDetail() {
  const { auth, push_token } = authStore();
  const user = getBaseUser(auth);
  const setSteps = tourneeStore((v) => v.setSteps);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [showFinishedPopup, setShowFinishedPopup] = useState(false);
  const [isOptionsVisible, setIsOptionsVisible] = useState(false);
  const [isInternalMapOpen, setIsInternalMapOpen] = useState(false);

  const { selectedTournee, setShouldReloadTournee } = tourneeStore();

  const [tourneeDetails, setTourneeDetails] = React.useState<ITourneeDetail>();
  const [allSteps, setAllSteps] = React.useState(tourneeDetails?.steps || []);
  const [showLatePopup, setShowLatePopup] = useState(false);
  const [loaderConfirm, setLoaderConfirm] = useState(false);

  const [usableSteps, setUsableSteps] = React.useState(
    allSteps.filter(
      (s) =>
        s.state === TOURNEE_STEP_STATE.ENABLE &&
        s.step_status === TOURNEE_STEP_STATUS.SENDED,
    ),
  );

  const setIsBusy = busyStore((v) => v.setBusyState);

  const [currentStep, setCurrentStep] = React.useState<IStep | null>(null);

  React.useEffect(() => {
    console.log("updating all steps");
    setAllSteps(tourneeDetails?.steps || []);
  }, [tourneeDetails]);

  React.useEffect(() => {
    setUsableSteps(
      allSteps.filter(
        (s) =>
          s.state === TOURNEE_STEP_STATE.ENABLE &&
          s.step_status === TOURNEE_STEP_STATUS.SENDED,
      ),
    );
  }, [allSteps]);

  React.useEffect(() => {
    if (usableSteps.length > 0) {
      setCurrentStep(usableSteps[0]);
    } else {
      setCurrentStep(null);
    }
  }, [usableSteps]);

  //const currentStep = React.useMemo(()=>{},[])

  useEffect(() => {
    if (allSteps.length > 0 && currentStep === null) {
      setShowFinishedPopup(true);
      setTimeout(() => {
        goBackToCourse();
      }, 2000);
    }
  }, [currentStep]);

  useEffect(() => {
    setIsBusy(true);
    loadData();
  }, [selectedTournee]);

  React.useEffect(() => {
    setSteps(allSteps);
  }, [tourneeDetails?.steps]);

  async function oncomplete_step() {
    if (currentStep == null || tourneeDetails === undefined) {
      goBackToCourse();
      // setShowFinishedPopup(true);
      return;
    }
    const idStep = currentStep.step_id;
    if (
      currentStep.order_is_last_moment &&
      currentStep.order_is_last_moment === true
    ) {
      try {
        const { latitude, longitude } = (
          await Location.getCurrentPositionAsync()
        ).coords;

        setIsBusy(true);
        await ready_to_go_to_pick_up(
          currentStep.course_id,
          latitude,
          longitude,
        );
        loadData();
      } catch (e) {
        console.error(e);
        setIsBusy(false);
        //Sentry.captureException(e);
      }
    } else {
      const msg =
        currentStep?.type === TOURNEE_STEP_TYPE.PICK
          ? "Bravo ! Tu as bien récupéré la commande ! "
          : "Excellent travail! Tu as livré la commande avec succès!";

      const title =
        currentStep?.type === TOURNEE_STEP_TYPE.PICK
          ? "Commande récupérée !"
          : "Client livré !";

      setIsBusy(true);
      complete_step(idStep, msg, title).finally(() => {
        try {
          if (currentStep.type === TOURNEE_STEP_TYPE.DELIVERY) {
            const closed_ref = ref(
              db,
              `chat_all_My8xMS8yMDIz/${user?.id}/${currentStep.order_id}/chat_metadata/`,
            );

            update(closed_ref, {
              closed: true,
            });
          }
        } catch (error) {
          console.error(error);
          //Sentry.captureException(error);
        }

        loadData();
      });
    }
  }

  const goBackToCourse = () => {
    setShowFinishedPopup(false);
    setShouldReloadTournee(true);
    router.push("/delivery_tab/courses");
  };

  const loadData = () => {
    const idTournee = selectedTournee.id;
    GetDetailTournee(idTournee)
      .then((t: ITourneeDetail) => {
        if (t && t.id) {
          setTourneeDetails(t);
        } else {
          setError(true);
        }
      })
      .catch((e) => {
        setError(true);
        console.log("error", e);
      })
      .finally(() => {
        //setError(false);
        setIsLoading(false);
        setIsBusy(false);
      });
  };

  useFocusEffect(() => {
    const idUser = authToUserId(auth);
    if (idUser == undefined) return;
    const reference = ref(db, `actualisationTournee/${idUser}/${push_token}`);
    onValue(reference, (snapshot) => {
      console.log("firebase tournee update");
      if (snapshot.exists()) {
        setIsBusy(true);
        loadData();
        remove(reference as DatabaseReference);
        EventRegister.emitEvent("COURSE_SHOULD_RELOAD");
      }
    });
    return () => off(reference);
  });

  const openMap = (destLat: number, destLong: number) => {
    return showLocation({
      latitude: destLat,
      longitude: destLong,
      directionsMode: "car",
      alwaysIncludeGoogle: true,
      dialogTitle: "Ouvrir dans Maps",
      dialogMessage: "Quelle application GPS voudriez-vous utiliser ?",
      cancelText: "Annuler",
      appsWhiteList:
        Platform.OS === "android" ? ["google-maps", "waze"] : undefined,
    });
  };

  function onMapButtonClick() {
    const latitude = currentStep ? currentStep.target.lat : 0;
    const longitude = currentStep ? currentStep.target.lng : 0;
    openMap(latitude, longitude);
  }

  // function OpenAndroidOptionsModal() {
  //   setIsBusy(false);
  //   setIsOptionsVisible(true);
  // }

  // function openInternalGPS() {
  //   setIsBusy(false);
  //   setIsOptionsVisible(false);
  //   setIsInternalMapOpen(true);
  // }

  useEffect(
    () => {
      loadData();
    },
    //[currentStep, currentTournee]
    [],
  );

  React.useEffect(() => {
    const interval = setInterval(() => {
      loadData();
    }, 60000);
    return () => clearInterval(interval);
  }, []);

  const currentStepIndex = React.useMemo(() => {
    return allSteps.findIndex(
      (item) => item.step_id === currentStep?.step_id ?? -1,
    );
  }, [currentStep, allSteps]);

  const isTwoSteps = React.useMemo(() => {
    console.log(currentStep?.order_is_last_moment);
    allSteps.map((item) => console.log(item.order_is_last_moment));
    if (currentStepIndex >= 0 && currentStepIndex + 1 < allSteps.length) {
      console.log(allSteps[currentStepIndex + 1].order_is_last_moment);
      return (
        currentStep?.order_is_last_moment ||
        allSteps[currentStepIndex + 1].order_is_last_moment
      );
    }
    return currentStep?.order_is_last_moment ?? false;
  }, [currentStep, allSteps, currentStepIndex]);

  if (error) {
    router.push("/delivery_tab/tournee");
  }

  if (
    isLoading === true ||
    tourneeDetails === undefined ||
    currentStep === null
  ) {
    return (
      <View style={styles.loadingContainer}>
        <View style={styles.loading}>
          <Text>Chargement en cours ...</Text>
        </View>
      </View>
    );
  }

  // if (currentStep === null) {
  //   return (
  //     <View
  //       style={{
  //         width: "100%",
  //         height: "100%",
  //         flex: 1,
  //         justifyContent: "center",
  //         alignItems: "center",
  //       }}
  //     >
  //       <Text>Chargement des données...</Text>
  //     </View>
  //   );
  // }

  return (
    <SafeAreaView style={{ height: "100%" }}>
      <Toolbar title={`Détail de la tournée`} tourneeId={tourneeDetails.id} />
      <View style={styles.main}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{
            height: "75%",
          }}
        >
          <View style={styles.container}>
            <View style={styles.map}>
              <View style={styles.mapGps}>
                <TouchableOpacity
                  style={styles.mapButton}
                  onPress={() => {
                    // if (Platform.OS === "android") {
                    //   OpenAndroidOptionsModal();
                    // } else {
                    onMapButtonClick();
                  }}
                >
                  <Text
                    style={{
                      fontFamily: fontFamily.semiBold,
                      fontSize: 16,
                      lineHeight: 22,
                      color: "#fff",
                    }}
                  >
                    GPS
                  </Text>
                </TouchableOpacity>
              </View>
              <MapPreviewTournee tournee={tourneeDetails} />
            </View>

            <View style={styles.tourneContainer}>
              {(tourneeDetails.steps ?? []).map((step, index) => (
                <View key={index}>
                  <StepLine
                    number={step.position + 1}
                    step={step}
                    price={step.tip}
                    isLast={
                      tourneeDetails.steps.length === index + 1 ? true : false
                    }
                    currentIndex={index}
                    activeIndex={currentStepIndex}
                  />
                </View>
              ))}
            </View>

            {currentStep !== null ? (
              <DetailCommande step={currentStep} />
            ) : null}
          </View>
        </ScrollView>
      </View>
      {currentStep !== null ? (
        <ButtonTournee step={currentStep} doAction={oncomplete_step} />
      ) : null}
      {currentStep?.type === TOURNEE_STEP_TYPE.PICK ? (
        <PartnerLateTourneeOverlay
          visible={showLatePopup}
          loaderConfirm={loaderConfirm}
          onClose={() => setShowLatePopup(false)}
          onAccept={() => {
            setShowLatePopup(false);
          }}
        />
      ) : (
        <ClientAbsentTourneeOverlay
          visible={showLatePopup}
          loaderConfirm={loaderConfirm}
          Resto={currentStep?.target.name}
          onClose={() => setShowLatePopup(false)}
          onAccept={() => {
            //
          }}
        />
      )}

      {/* {isOptionsVisible ? (
        <View
          style={{
            position: "absolute",

            width: "100%",
            height: "100%",
            backgroundColor: "rgba(0, 0, 0, 0.4)",
            zIndex: 10000,
          }}
        >
          <View
            style={{
              flex: 1,
              justifyContent: "flex-end", // Center vertically
              alignItems: "center", // Center horizontally
            }}
          >
            <View
              style={{
                width: "90%",
                borderRadius: 20,
                backgroundColor: "#fff",
              }}
            >
              <TouchableOpacity
                style={{ width: "100%" }}
                // onPress={() => openInternalGPS()}
              >
                <Text style={styles.navigationOptions}>GPS Interne</Text>
              </TouchableOpacity>
              <Divider />
              <TouchableOpacity
                style={{ width: "100%" }}
                onPress={() => onMapButtonClick()}
              >
                <Text style={styles.navigationOptions}>
                  Ouvrir avec Google Maps
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.cancelBtn}>
              <TouchableOpacity onPress={() => setIsOptionsVisible(false)}>
                <Text style={styles.cancelBtnText}>Annuler</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      ) : null} */}

      <FinishedTourneeOverlay
        visible={showFinishedPopup}
        onClose={() => goBackToCourse()}
        onPress={() => goBackToCourse()}
      />

      <IsBusyOverlay />

      {isTwoSteps ? (
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-evenly",
            marginHorizontal: 30,
            alignItems: "center",
            marginTop: -15,
            position: "relative",
            top: -15,
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginTop: 12,
              marginLeft: 39,
            }}
          >
            <ScooterGrey />
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 10,
                lineHeight: 12,
                marginTop: 1,
                color: "rgba(196, 196, 196, 1)",
              }}
            >
              Départ vers restaurant
            </Text>
          </View>
          <View
            style={{
              backgroundColor: "#E7E7E7",
              height: 5,
              width: "40%",
              borderRadius: 5,
            }}
          >
            <View style={{}}></View>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              marginRight: 30,
            }}
          >
            <CarryGrey />
            <Text
              style={{
                fontFamily: fontFamily.regular,
                fontSize: 10,
                lineHeight: 12,
                color: "rgba(196, 196, 196, 1)",
                marginTop: 1,
              }}
            >
              Recupérer la commande
            </Text>
          </View>
        </View>
      ) : null}

      {/* <InternalGPSModal
        open={isInternalMapOpen}
        setOpen={setIsInternalMapOpen}
      /> */}
    </SafeAreaView>
  );
}

interface ITourneeProps {
  number: number;
  isLast: boolean;
  step: IStep;
  price: string;
  currentIndex: number;
  activeIndex: number;
}
const ADRESS_DEFAULT = {
  address: "",
  appartement: "",
  batiment: "",
  code_porte: "",
  complementadresse: "",
  etage: "",
  numerorue: "",
  postalCode: "",
};
const StepLine = ({
  number,
  isLast,
  step,
  price,
  currentIndex,
  activeIndex,
}: ITourneeProps) => {
  const active = step.state === TOURNEE_STEP_STATE.ENABLE;
  const isPreview = step.step_status === TOURNEE_STEP_STATUS.FINISHED;
  const isCrossed = step.state === TOURNEE_STEP_STATE.CROSSED;
  const isLivrer = step.type === TOURNEE_STEP_TYPE.DELIVERY;
  const newPrice = parseFloat(price);

  const NBIcon = React.useCallback(() => {
    const total = step.order_details.length;
    if (total === 1 || total > 6) {
      return <EmporterSmall />;
    }
    const key1 = active
      ? "ACTUAL"
      : currentIndex < activeIndex
      ? "PAST"
      : "FUTURE";
    const COMPONENT = ICONS[key1][total];
    return <COMPONENT />;
  }, [activeIndex, currentIndex, active, step.order_details.length]);

  const getAdress = React.useMemo(() => {
    if (step.target.delivery_adress === null) return ADRESS_DEFAULT;
    else {
      return step.target.delivery_adress;
    }
  }, [step.target.delivery_adress]);

  useEffect(() => {
    console.log("step: ", step);
  }, []);

  return (
    <View style={styles.tourneeMain}>
      {!isLast ? <View style={styles.lineDash} /> : <View />}
      <View
        style={{
          ...styles.indexContent,
          backgroundColor: active
            ? "rgba(255, 92, 133, 1)"
            : isPreview || isCrossed
            ? "rgba(141, 152, 160, 1)"
            : "rgba(196, 196, 196, 1)",
        }}
      >
        <Text style={{ color: "#fff" }}>{number}</Text>
      </View>
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{ flexDirection: "column", width: active ? "80%" : "80%" }}
        >
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
              flexWrap: "wrap",
            }}
          >
            <Text
              style={{
                ...styles.tourneeHourBold,
                marginRight: active ? 10 : 5,
                color: active ? "#FF5C85" : "rgba(141, 152, 160, 1)",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {hour_to_string(step.hour)}
            </Text>
            <View
              style={{
                ...styles.tourneeIconName,
                paddingHorizontal: active ? 10 : 0,
                backgroundColor: active ? "rgba(255, 92, 133, 0.1)" : "#fff",
              }}
            >
              {isLivrer ? (
                active ? (
                  <UserCircle />
                ) : (
                  <UserCircleGrey />
                )
              ) : active ? (
                <StorefrontRed />
              ) : (
                <StorefrontGrey />
              )}
              <Text
                style={{
                  ...styles.tourneeName,
                  color: active ? "#1D2A40" : "rgba(141, 152, 160, 1)",
                  textDecorationLine: isCrossed ? "line-through" : "none",
                }}
              >
                {step.target.name}
              </Text>
            </View>
            {step.order_details.length > 1 ? (
              <View
                style={{
                  alignItems: "flex-end",
                  justifyContent: "space-evenly",
                  borderRadius: 10,
                  flexDirection: "row",
                  paddingBottom: 4,
                  paddingHorizontal: 5,
                  marginHorizontal: 5,
                }}
              >
                {NBIcon()}
              </View>
            ) : null}
          </View>
          <Text
            style={{
              ...styles.tourneeAdress,
              color: active ? "#1D2A40" : "#C4C4C4",
              textDecorationLine: isCrossed ? "line-through" : "none",
            }}
          >
            {step.target.adress}
          </Text>
          {getAdress.numerorue && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"N° " + getAdress.numerorue + ", "}
            </Text>
          )}

          {getAdress.code_porte && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"porte " + getAdress.code_porte + ", "}
            </Text>
          )}

          {getAdress.appartement && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"appartement " + getAdress.appartement + ", "}
            </Text>
          )}
          {getAdress.etage && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"étage " + getAdress.etage + ", "}
            </Text>
          )}
          {getAdress.batiment && (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {"batiment " + getAdress.batiment}
            </Text>
          )}
          {getAdress.complementadresse ? (
            <Text
              style={{
                ...styles.tourneeAdress,
                color: active ? "#1D2A40" : "#C4C4C4",
                textDecorationLine: isCrossed ? "line-through" : "none",
              }}
            >
              {getAdress.complementadresse}
            </Text>
          ) : null}
          <View
            style={{
              flexDirection: "row",
              alignItems: "center",
            }}
          >
            <View
              style={{
                flexDirection: "column",
                justifyContent: "flex-start",
                alignItems: "flex-start",
              }}
            >
              <Text
                style={{
                  ...styles.tourneeAdress,
                  color: active ? "#1D2A40" : "#C4C4C4",
                  textDecorationLine: isCrossed ? "line-through" : "none",
                  marginBottom: 5,
                }}
              >
                Commande ID {step.order_id}
              </Text>
            </View>
            {newPrice > 0 && isLivrer && (
              <View
                style={{
                  backgroundColor: active ? "#ECC53B" : "#fff",
                  borderRadius: 5,
                  marginLeft: 5,
                  paddingHorizontal: 5,
                  flexDirection: "row",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {active ? <Euro /> : <EuroGris />}
                <Text
                  style={{
                    fontFamily: fontFamily.semiBold,
                    fontSize: 12,
                    lineHeight: 18,
                    color: active ? "#1D2A40" : "#C4C4C4",
                    marginLeft: 3,
                    textDecorationLine: isCrossed ? "line-through" : "none",
                  }}
                >
                  {price} €
                </Text>
              </View>
            )}
          </View>
          {step.pickup_indication && (
            <View style={styles.recuperationBox}>
              <Text style={styles.recuperationTextBold}>
                Récuperation de la commande :{" "}
              </Text>
              <Text style={styles.recuperationTextLite}>
                {step.pickup_indication}
              </Text>
            </View>
          )}
        </View>
        {isLivrer ? (
          <View
            style={{
              flexDirection: "row",
              gap: 15.66,
              marginRight: -80,
            }}
          >
            <TouchableOpacity
              onPress={() => {
                try {
                  callHiddenNumber(step.target.telephone);
                } catch (error) {
                  console.error(error);
                  //Sentry.captureException(error);
                }
              }}
            >
              {active ? <Phone /> : <Phone_Disabled />}
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                router.push({
                  pathname: "/chat_client_screen",
                  params: { command_id: step.order_id },
                });
              }}
            >
              {active ? <ActiveChatBtn /> : <InactiveChatBtn />}
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    </View>
  );
};

const DetailCommande = ({ step }: { step: IStep }) => {
  const [toggle, setToggle] = useState(true);
  return (
    <SafeAreaView>
      <View style={styles.clientMain}>
        <TouchableOpacity
          onPress={() => setToggle(!toggle)}
          style={{
            flexDirection: "row",
            justifyContent: "space-between",

            width: "100%",
            height: 30,
          }}
        >
          <Text style={styles.itemTitle}>COMMANDE ID {step.order_id}</Text>
          {toggle ? <CaretUp /> : <CaretDowns />}
        </TouchableOpacity>
        <View style={styles.horizontalLine} />
        {toggle ? (
          <View style={{ ...styles.clientContainer, marginHorizontal: 15 }}>
            <View style={{ flexDirection: "column", width: "100%" }}>
              {step.order_details.map((o, index) => (
                <React.Fragment key={index}>
                  <View style={styles.products} key={index}>
                    <Text style={styles.productTitle}>
                      {o.qte} {o.name}
                    </Text>
                    <Text style={styles.productPrice}>{o.price} €</Text>
                  </View>
                  {o?.sub_products?.map((p, index) => (
                    <React.Fragment key={index}>
                      <View style={styles.products} key={index}>
                        <Text style={styles.productTitle}>
                          {p.quantity} {p.name}
                        </Text>
                        <Text style={styles.productPrice}>{p.price} €</Text>
                      </View>
                    </React.Fragment>
                  ))}
                </React.Fragment>
              ))}
            </View>
          </View>
        ) : null}
      </View>
      {toggle && step.order_comment ? (
        <View style={styles.clientMain}>
          <View style={styles.comment}>
            <Text style={styles.subTitles}>COMMENTAIRE CLIENT</Text>
            <Text style={styles.commentText}>{step.order_comment}</Text>
          </View>
        </View>
      ) : null}
    </SafeAreaView>
  );
};

const ButtonTournee = ({
  step,
  doAction,
}: {
  step: IStep;
  doAction: () => void;
}) => {
  const subTitle = React.useMemo(() => {
    if (step.order_is_last_moment && step.order_is_last_moment === true) {
      return "Fais glisser le bouton dès que tu pars vers le restaurant";
    }
    if (step.type === TOURNEE_STEP_TYPE.PICK) {
      return "Fais glisser le bouton dès que tu as récupéré la commande";
    } else {
      return "Fais glisser le bouton dès que tu as livré la commande";
    }
  }, [step]);

  const title = React.useMemo(() => {
    if (step.order_is_last_moment && step.order_is_last_moment === true) {
      return "Départ vers restaurant";
    }
    if (step.type === TOURNEE_STEP_TYPE.PICK) {
      return "Commande récupérée";
    } else {
      return "Client livré !";
    }
  }, [step]);
  console.log("updating button");

  return (
    <View
      style={{
        backgroundColor: "#fff",
      }}
    >
      <SliderButton
        title={title}
        name={step.target.name}
        subTitle={subTitle}
        railBgColor="rgba(255, 92, 133, 0.1);"
        railStylesBgColor="#FF5C85"
        onSuccess={doAction}
        subTitleColor={"#000"}
        isDisabled={false}
      />
    </View>
  );
};

export const styles = StyleSheet.create({
  main: {
    // flex: 5,
    backgroundColor: "#fff",
    // paddingTop: 40,
    marginTop: -10,
  },
  container: {
    backgroundColor: "#fff",
    paddingHorizontal: 0,
  },
  header: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    width: "75%",
  },
  title: {
    fontFamily: fontFamily.semiBold,
    textAlign: "center",
    fontSize: 18,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
    marginTop: 15,
  },
  subTitle: {
    fontFamily: fontFamily.regular,
    textAlign: "center",
    fontSize: 14,
    lineHeight: 20,
    color: "#1D2A40",
    marginBottom: 10,
  },
  map: {
    position: "relative",
    width: "100%",
    height: 220,
  },
  mapGps: {
    position: "absolute",
    top: 10,
    right: 20,
    zIndex: 1,
    overflow: "hidden",
  },
  hour: {
    flexDirection: "row",
    justifyContent: "space-around",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    height: 68,
    marginBottom: 20,
  },
  timeContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    paddingVertical: 10,
  },
  timeTitle: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  timeText: {
    fontFamily: fontFamily.semiBold,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
  timeHour: {
    fontFamily: fontFamily.Bold,
    color: "#1D2A40",
    fontSize: 18,
    lineHeight: 21.97,
  },
  courses: {
    backgroundColor: "#F4F4F4",
    paddingLeft: 40,
    paddingTop: 10,
    paddingBottom: 10,
    marginBottom: 20,
  },
  distance: {
    flexDirection: "row",
    marginBottom: 5,
    alignItems: "center",
  },
  distanceText: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 14.63,
    color: "#112842",
    marginLeft: 20,
  },
  distanceTextBold: {
    fontFamily: fontFamily.Bold,
    color: "#112842",
    fontSize: 12,
    lineHeight: 17.07,
    marginLeft: 5,
    marginRight: 5,
  },
  tourneContainer: {
    marginTop: 10,
    marginBottom: 15,
    paddingLeft: 26,
  },
  tourneeMain: {
    flexDirection: "row",
    marginBottom: 10,
    position: "relative",
  },
  mapButton: {
    backgroundColor: "#40ADA2",
    width: 98,
    height: 45,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 26,
    marginRight: 15,
    shadowColor: "rgba(0, 0, 0, 1)",
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 5,
    elevation: 5,
  },
  lineDash: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: 30,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 10,
    borderColor: "#C4C4C4",
    height: "100%",
  },
  indexContent: {
    width: 22,
    height: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    marginRight: 10,
    marginTop: 5,
  },
  tourneeHourBold: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 18,
  },
  tourneeIconName: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
    paddingVertical: 3,
  },
  tourneeName: {
    fontFamily: fontFamily.Bold,
    fontSize: 14,
    lineHeight: 18,
    marginLeft: 5,
  },
  tourneeAdress: {
    fontFamily: fontFamily.regular,
    fontSize: 12,
    lineHeight: 18,
  },
  clientMain: {
    marginHorizontal: 23,
    marginVertical: 5,
    height: "50%",
  },
  subTitles: {
    position: "absolute",
    top: 9,
    left: 13,
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
  comment: {
    position: "relative",
    marginTop: 10,
    marginBottom: 20,
    width: "100%",
    height: 87,
    borderRadius: 5,
    justifyContent: "center",
    backgroundColor: "rgba(252, 247, 231, 1)",
    alignSelf: "center",
  },
  commentText: {
    fontFamily: fontFamily.regular,
    fontSize: 13,
    lineHeight: 18,
    color: "#5C5B5B",
    marginLeft: 13,
    marginTop: 20,
  },
  clientContainer: {
    marginTop: 10,
    flexDirection: "row",
    alignItems: "center",
  },
  editText: {
    fontSize: 16,
    lineHeight: 22,
    fontFamily: fontFamily.semiBold,
    color: "#FF5C85",
  },
  products: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 15,
  },
  productTitle: {
    flex: 9,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    paddingRight: 8,
  },
  productPrice: {
    flex: 3,
    fontFamily: fontFamily.Medium,
    fontSize: 14,
    lineHeight: 22,
    textAlign: "right",
    paddingLeft: 5,
  },
  itemTitle: {
    fontSize: 12,
    lineHeight: 22,
    fontFamily: fontFamily.regular,
    color: "#5C5B5B",
  },
  horizontalLine: {
    height: 1,
    backgroundColor: "rgba(196, 196, 196, 0.31)",
    marginVertical: 5,
    marginHorizontal: -20,
  },
  loadingContainer: {
    backgroundColor: "white",
    paddingHorizontal: 0,
    paddingVertical: 16,
    borderRadius: 20,
    paddingTop: 41,
    marginTop: 30,
    marginBottom: 20,
    width: "100%",
    height: "100%",
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: screenWidth,
    height: 100,
    backgroundColor: "white",
  },
  recuperationBox: {
    width: 270,
    height: 47,
    backgroundColor: "#FCF7E7",
    borderRadius: 5,
    flexDirection: "column",
    justifyContent: "center",
    paddingLeft: 9,
  },
  recuperationTextLite: {
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 13,
    lineHeight: 18,
  },
  recuperationTextBold: {
    fontFamily: fontFamily.semiBold,
    color: "#000000",
    fontSize: 13,
    lineHeight: 18,
  },

  navigationOptions: {
    textAlign: "center",
    fontSize: 16,
    fontFamily: fontFamily.Medium,
    marginTop: 17,
    marginBottom: 22,
  },

  cancelBtn: {
    width: "90%",
    height: 57,
    marginTop: 10,
    borderRadius: 20,
    backgroundColor: "#fff",
    marginBottom: 12,
    justifyContent: "center",
  },
  cancelBtnText: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    color: "rgba(243, 106, 114, 1)",
    textAlign: "center",
  },
});
