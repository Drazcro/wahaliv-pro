import { Dimensions, Platform, StyleSheet } from "react-native";
import { fontFamily } from "src/theme/typography";

const { width } = Dimensions.get("window");

export const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: "#fff",
  },
  container: {
    backgroundColor: "#fff",
  },
  header: {
    left: 0,
    right: 0,
    zIndex: 1,
  },
  wrapperOne: {
    flexDirection: "row",
    //justifyContent:"space-evenly",
    paddingVertical: 10,
  },
  wrapperThreeSub: {
    position: "relative",
    bottom: 15,
  },
  textOne: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
  textTwo: {
    fontFamily: fontFamily.Bold,
    fontSize: 18,
    lineHeight: 21.94,
    color: "#1D2A40",
  },
  title: {
    //textAlign: 'justify',
    fontFamily: fontFamily.Medium,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  dateNow: {
    textAlign: "justify",
    fontFamily: fontFamily.Medium,
    color: "#000000",
    fontSize: 13,
    lineHeight: 22,
  },
  line: {
    borderWidth: 1,
    borderColor: "#C4C4C4",
  },
  divider: {
    borderWidth: 1,
    borderColor: "#C4C4C4",
  },
  dividerOne: {
    mmarginRight: 10,
    marginLeft: 10,
    marginBottom: 20,
  },
  pic: {
    backgroundColor: "red",
    flexDirection: "row",
  },
  grid: {
    alignItems: "center",
    lineHeight: 0,
  },
  wrapperTwo: {
    marginTop: 20,
    position: "relative",
  },
  wrapperRecovered: {
    backgroundColor: "#F4F4F4",
    padding: 15,
    position: "relative",
  },
  picOne: {
    position: "absolute",
    top: 23,
    left: 24,
  },
  wrapperData: {
    flexDirection: "row",
    marginLeft: 15,
  },
  subWrapperOne: {
    marginBottom: 35,
    flexDirection: "row",
    justifyContent: "space-around",
    width: "100%",
    paddingRight: 30,
    left: width * 0.01,
  },
  wrapperFour: {
    alignSelf: "center",
    width: "90%",
    marginBottom: Platform.OS === "ios" ? -20 : -35,
  },
  textThree: {
    fontFamily: fontFamily.Medium,
    color: "rgba(141, 152, 160, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  section: {
    marginHorizontal: width * 0.02,
    marginBottom: -25,
  },
  linePath: {
    position: "absolute",
    top: 32,
    left: 24,
  },
  client: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  client1: {
    fontFamily: fontFamily.Medium,
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 22,
  },
  maps: {
    width: "100%",
    height: 220,
    marginBottom: 15,
    backgroundColor: "red",
  },
  mapgps: {
    position: "absolute",
    top: 10,
    right: 20,
    zIndex: 1,
    overflow: "hidden",
  },
  globalMap: {
    position: "relative",
    width: "100%",
    height: 220,
  },
  map: {
    top: 5,
  },
  width: {
    width: "80%",
  },
  width1: {
    width: "80%",
  },
  columnCenter: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  lineDash: {
    position: "absolute",
    top: -10,
    left: 23.5,
    marginTop: 35,
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 5,
    height: "70%",
    borderColor: "#1D2A40",
  },
  downContent: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    paddingHorizontal: 30,
  },
  dropDownStyle: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    marginLeft: 20,
    marginRight: 20,
  },
  dropDown: {
    width: "100%",
    marginTop: 10,
    marginBottom: 20,
    marginRight: 100,
  },
  textOrderFinish: {
    fontFamily: fontFamily.regular,
    color: "#000000",
    fontSize: 14,
    lineHeight: 22,
  },
  content: {
    backgroundColor: "#ED7178",
    height: 31,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 8,
  },
  textOne1: {
    fontFamily: fontFamily.semiBold,
    color: "#fff",
    fontSize: 14,
    lineHeight: 22,
    marginLeft: 5,
    marginRight: 5,
  },
  tip: {
    backgroundColor: "#E9C852",
    width: 125,
    borderRadius: 5,
    paddingHorizontal: 5,
  },
  tipText: {
    fontFamily: fontFamily.Medium,
    color: "#1D2A40",
    fontSize: 12,
    lineHeight: 14.63,
  },
  wrapper: {
    flexDirection: "row",
  },
  subWrapper: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 5,
    marginLeft: 10,
  },
  distance: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    paddingLeft: 10,
  },
  text1: {
    fontFamily: fontFamily.Bold,
    marginLeft: 20,
    color: "#112842",
    fontSize: 14,
  },
  text2: {
    fontFamily: fontFamily.Bold,
    marginLeft: 10,
    color: "#112842",
    fontSize: 14,
  },
  // need_real_price
  realPriceContent: {
    marginTop: -10,
    marginHorizontal: 20,
    paddingBottom: 20,
    flexDirection: "column",
  },
  realPriceText: {
    fontFamily: fontFamily.regular,
    color: "#112842",
    fontSize: 14,
  },
  realPriceInputMain: {
    paddingBottom: 30,
    marginHorizontal: 20,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  realPriceCurrency: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 20,
    marginLeft: 5,
  },
  amountInput: {
    backgroundColor: "#fff",
    borderColor: "#C4C4C4",
    borderWidth: 1,
    width: 100,
    height: 35,
    borderRadius: 10,
  },
  input: {
    borderBottomWidth: 0,
    bottom: 3,
  },
  price: {
    fontFamily: fontFamily.semiBold,
    fontSize: 20,
    textAlign: "center",
    color: "#112842",
  },
});
