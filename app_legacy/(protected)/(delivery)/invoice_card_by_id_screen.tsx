import { View } from "react-native";

import React from "react";
import { styles } from "./invoice.style";
import { ScrollView } from "react-native-gesture-handler";
import moment from "moment";
import { InvoiceCardByID } from "src_legacy/components/delivery/cards/invoice_card/invoice_card_by_id";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
import { courseStore } from "src_legacy/zustore/coursestore";
import { useLocalSearchParams } from "expo-router";
import { IInvoiceDataCourse } from "src/interfaces/Entities";

export default function InvoiceCardByIDScreen() {
  const courses = courseStore((v) => v.courses);
  const { date } = useLocalSearchParams<{ date: string }>();
  //const {courses} = useLocalSearchParams<>

  const charAtOne = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  return (
    <View style={styles.bgColor}>
      <Toolbar title={charAtOne(moment(date).format("dddd D MMMM YYYY"))} />
      <ScrollView style={styles.bgColor}>
        <View style={styles.invoiceCard}>
          {
            courses?.map((item: any, index: number) => (
              <InvoiceCardByID key={index} invoice_data_course={item} />
            ))}
        </View>
      </ScrollView>
    </View>
  );
}
