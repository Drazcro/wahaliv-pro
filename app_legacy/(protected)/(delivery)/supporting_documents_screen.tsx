import React, { useEffect, useState } from "react";
import { StatusBar } from "expo-status-bar";
import I18n from "i18n-js";
import { View, StyleSheet, Text, ActivityIndicator, Modal } from "react-native";
import { Button } from "react-native-elements";

import { authToUserId } from "src_legacy/services/Utils";
import { get_deliveryman_docs } from "src_legacy/services/Loaders";
import * as DocumentPicker from "expo-document-picker";
import { fontFamily } from "src/theme/typography";
import { ScrollView } from "react-native-gesture-handler";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";
import { ProfileSupportingDocumentItem } from "src_legacy/components/delivery/profile/profile_supporting_document_item";
import { UploadFileModalSuccess } from "src_legacy/components/delivery/courses/modal/upload_file_modal_success";
import { UploadFileModalError } from "src_legacy/components/delivery/courses/modal/upload_file_modal_error";
import { authStore, vanillaAuthState } from "src_legacy/services/v2/auth_store";
import { color } from "src/theme";

interface IDocument {
  identityRecto: boolean;
  identityVerso: boolean;
  rib: boolean;
  registration_proof: boolean;
  contract: boolean;
}

export default function SupportingDocumentsScreen() {
  const { auth } = authStore();
  const user_id: number = authToUserId(auth);
  const token: string = auth.access_token;

  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [errorRequest, setErrorRequest] = useState(false);

  const [documents, setDocuments] = useState({} as IDocument);
  const [rectoFile, setRectoFile] = useState(
    {} as DocumentPicker.DocumentResult,
  );
  const [versoFile, setVersoFile] = useState(
    {} as DocumentPicker.DocumentResult,
  );
  const [ribFile, setRibFile] = useState({} as DocumentPicker.DocumentResult);
  const [kbisFile, setKbisFile] = useState({} as DocumentPicker.DocumentResult);
  const [contractFile, setContractFile] = useState(
    {} as DocumentPicker.DocumentResult,
  );

  const [showModalSucess, setShowModalSuccess] = useState(false);
  const [showModalError, setShowModalError] = useState(false);
  const [errorSize, setErrorSize] = useState(false);
  const [errorFile, setErrorFile] = useState(false);

  const isObject = (obj: object) =>
    typeof obj === "object" &&
    obj !== null &&
    obj !== undefined &&
    Object.keys(obj).length > 0;

  const uploadFile = async (
    file: DocumentPicker.DocumentResult,
    id: number,
    type: string,
  ) => {
    const formdata = new FormData();
    formdata.append("file", {
      uri: file.uri,
      type: file.mimeType,
      name: file.name,
      size: file.size,
    });
    const res = await fetch(
      `${
        vanillaAuthState.getState().base_url
      }deliverymen/${id}/document/${type}`,
      {
        method: "POST",
        body: formdata,
        headers: {
          "Content-Type": "multipart/form-data",
          Authorization: `Bearer ${token}`,
        },
      },
    );

    const response = await res.json();
    if (response.status === 200) {
      console.log("File uploaded: ", response);
    }
  };

  const handleSubmit = async () => {
    setShowLoadingButton(true);
    isObject(rectoFile) && (await uploadFile(rectoFile, user_id, "recto"));
    isObject(versoFile) && (await uploadFile(versoFile, user_id, "verso"));
    isObject(ribFile) && (await uploadFile(ribFile, user_id, "rib"));
    isObject(kbisFile) && (await uploadFile(kbisFile, user_id, "kbis"));
    isObject(contractFile) &&
      (await uploadFile(contractFile, user_id, "contract"));
    setShowLoadingButton(false);
    setShowModalSuccess(true);
  };

  useEffect(() => {
    get_deliveryman_docs();
    get_deliveryman_docs()
      .then((response) => {
        setDocuments(response);
      })
      .catch(() => {
        setErrorRequest(true);
      })
      .finally(() => setIsLoading(false));
  }, []);

  if (isLoading === true) {
    return (
      <View style={styles.loading}>
        <Text>Chargement en cours ...</Text>
      </View>
    );
  }

  if (errorRequest === true) {
    return (
      <View style={styles.loading}>
        <Text>erreur de chargement ...</Text>
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <StatusBar style="dark" />
      <ProfileToolbar title={I18n.t("profile.supportingDocument.title")} />
      <ScrollView
        style={{ marginTop: -8 }}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.body}>
          <Text style={styles.text_title}>
            {I18n.t("profile.supportingDocument.text")}
            <Text style={styles.sub_title}>
              {I18n.t("profile.supportingDocument.myInformation")}
            </Text>
          </Text>
        </View>
        <View style={styles.fileBody}>
          <ProfileSupportingDocumentItem
            hasFile={documents.identityRecto}
            fileTitle={I18n.t("profile.supportingDocument.recto")}
            setFile={setRectoFile}
            setErrorSize={setErrorSize}
            setErrorFile={setErrorFile}
            setShowModalError={setShowModalError}
          />
          <ProfileSupportingDocumentItem
            hasFile={documents.identityVerso}
            fileTitle={I18n.t("profile.supportingDocument.verso")}
            setFile={setVersoFile}
            setErrorSize={setErrorSize}
            setErrorFile={setErrorFile}
            setShowModalError={setShowModalError}
          />
          <ProfileSupportingDocumentItem
            hasFile={documents.contract}
            fileTitle={I18n.t("profile.supportingDocument.contract")}
            setFile={setContractFile}
            setErrorSize={setErrorSize}
            setErrorFile={setErrorFile}
            setShowModalError={setShowModalError}
          />
          <ProfileSupportingDocumentItem
            hasFile={documents.rib}
            fileTitle={I18n.t("profile.supportingDocument.rib")}
            setFile={setRibFile}
            setErrorSize={setErrorSize}
            setErrorFile={setErrorFile}
            setShowModalError={setShowModalError}
          />
          <ProfileSupportingDocumentItem
            hasFile={documents.registration_proof}
            fileTitle={I18n.t("profile.supportingDocument.kbis")}
            setFile={setKbisFile}
            setErrorSize={setErrorSize}
            setErrorFile={setErrorFile}
            setShowModalError={setShowModalError}
          />
        </View>
        <View style={styles.container_button}>
          {showLoadingButton ? (
            <View style={styles.loader_button}>
              <ActivityIndicator
                style={{ alignSelf: "center" }}
                color="white"
              />
            </View>
          ) : (
            <Button
              type="solid"
              title={I18n.t("form.button.saveEdit")}
              titleStyle={styles.text_button}
              buttonStyle={styles.button}
              onPress={handleSubmit}
            />
          )}
        </View>
      </ScrollView>
      <Modal visible={showModalSucess} transparent={true}>
        <UploadFileModalSuccess onClose={setShowModalSuccess} />
      </Modal>
      <Modal visible={showModalError} transparent={true}>
        <UploadFileModalError
          onClose={setShowModalError}
          isSizeError={errorSize}
          isFileError={errorFile}
        />
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  body: {
    flexDirection: "row",
    justifyContent: "center",
    zIndex: 3,
  },
  fileBody: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 15,
  },
  text_title: {
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 18,
    textAlign: "center",
    fontFamily: fontFamily.regular,
  },
  sub_title: {
    color: "rgba(29, 42, 64, 1)",
    fontSize: 14,
    lineHeight: 18,
    fontFamily: fontFamily.semiBold,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  container_button: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 50,
    marginBottom: 30,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  text_button: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
    lineHeight: 22,
  },
});
