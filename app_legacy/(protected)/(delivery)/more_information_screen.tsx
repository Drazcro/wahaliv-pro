import { StatusBar } from "expo-status-bar";
import React from "react";
import { KeyboardAvoidingView, Platform, StyleSheet, View } from "react-native";
import { Text } from "react-native-elements";
import { fontFamily } from "src/theme/typography";
import Check from "assets/Check.svg";
import { ProfileToolbar } from "src_legacy/components/delivery/profile/profile_tool_bars";

const CONDITIONS = [
  "Respecter le nombre de services prévus au MG",
  "Se positionner 1 semaine à l’avance impérativement (ou perte du MG)",
  "Possibilité de se dépositionner jusqu’à 6h avant le service, sans perte du MG",
  "La course doit être acceptée dans les 10 min sinon sera considérée comme Annulée",
  "Course annulée = perte du MG et de bonus",
];

export default function MoreInformationScreen() {
  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <StatusBar style="dark" />
      <ProfileToolbar title="Minimum garanti" />
      <View style={styles.bugComponentPosition}>
        <Text style={styles.title}>CONDITIONS</Text>
        <View style={styles.list}>
          {CONDITIONS.map((item, index) => (
            <View style={styles.line} key={index}>
              <Check style={styles.icon} />
              <Text style={styles.condition}>{item}</Text>
            </View>
          ))}
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    height: "100%",
  },
  bugComponentPosition: {
    height: "100%",
    backgroundColor: "white",
    paddingTop: 50,
    paddingHorizontal: 20,
  },
  title: {
    color: "#8C8C8C",
    fontFamily: fontFamily.semiBold,
    fontSize: 12,
    marginLeft: 14,
  },
  list: {
    marginTop: 15,
    width: "100%",
  },
  line: {
    flexDirection: "row",
  },
  condition: {
    fontFamily: fontFamily.regular,
    fontSize: 14,
    lineHeight: 26,
  },
  icon: {
    marginTop: 8,
  },
});
