import React, { useEffect, useState } from "react";
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from "react-native";

import IconsLeft from "assets/image_svg/IconsLeft.svg";
import IconsRight from "assets/image_svg/IconsRight.svg";
import { fontFamily } from "src/theme/typography";
import { Button } from "react-native-elements";
import I18n from "i18n-js";
import Moment from "moment";
import { extendMoment } from "moment-range";
import {
  get_is_on_service,
  recover_delivery_planning,
  recover_delivery_planning_summary_disengage_status,
} from "src_legacy/services/Loaders";
import { STATUS } from "src/constants/status";
import { authToUserId, capitalize_first_letter } from "src_legacy/services/Utils";
import {
  IPlanning,
  IPlanningDisengageStatus,
  IPlannings,
} from "src/interfaces/Entities";
import Toast from "react-native-expo-popup-notifications";
import PlanningDesengageOverlay from "src_legacy/components/modal/PlanningDesengageOverlay";
import PlanningInProgressOverlay from "src_legacy/components/modal/PlanningInProgressOverlay";
import { authStore, vanillaAuthState } from "src_legacy/services/v2/auth_store";
import { color } from "src/theme";

const moment = extendMoment(Moment);

export function Calendars(props: {
  date: Date;
  setDate: (arg0: Date) => void;
}) {
  const [showLoadingButton, setShowLoadingButton] = useState(false);
  const date = props.date;
  const setDate = props.setDate;
  const [planning, setPlanning] = useState({} as IPlannings);
  const [error, setError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const [openModalDesengage, setModalDesengage] = useState(false);
  const [openModalInProgress, setModalInProgress] = useState(false);

  const [planningToAdd, setPlanningToAdd] = useState(
    [] as Array<number | undefined>,
  );
  const [planningToDelete, setPlanningToDelete] = useState(
    [] as Array<number | undefined>,
  );

  const { auth } = authStore();
  const user_id = authToUserId(auth);
  const token: string = auth.access_token;

  const startDate = moment().startOf("week");
  const EndDate = moment(startDate).add(2, "weeks");

  const checkRangeWeeks = (day: unknown) => {
    const current = moment(day);
    const isInRangeWeeks = current.isBetween(startDate, EndDate);
    return !isInRangeWeeks;
  };

  const checkRangeWeeksChevronDown = (day: unknown) => {
    const current = moment(day).subtract(1, "days");
    const isInRangeWeeks = current.isBetween(startDate, EndDate);
    return !isInRangeWeeks;
  };

  const checkRangeWeeksChevronUp = (day: unknown) => {
    const current = moment(day).add(1, "days");
    const isInRangeWeeks = current.isBetween(startDate, EndDate);
    return !isInRangeWeeks;
  };

  const increment_week = () => {
    const _date = moment(date).add(1, "days");
    setDate(_date);
    get_planning_by_date(user_id, _date);
  };

  const decrement_week = () => {
    const _date = moment(date).subtract(1, "days");
    setDate(_date);
    get_planning_by_date(user_id, _date);
  };

  const get_planning_by_date = async (id: number, _date: Date) => {
    // reset array for planngin
    setPlanningToAdd([]);
    setPlanningToDelete([]);

    // get planning by date selected
    setIsLoading(true);
    recover_delivery_planning(
      id,
      STATUS.MARKETPLACE,
      moment(_date).format("YYYY-MM-DD"),
    )
      .then((planning: IPlannings) => {
        setPlanningSelected(planning);
      })
      .catch(() => {
        setError(true);
      })
      .finally(() => {
        setError(false);
        setIsLoading(false);
        get_is_on_service(id);
      });
  };

  const setSelectedColor = (day: string | number | Date) => {
    const selecteDay = new Date(date).getTime();
    const currentDay = new Date(day).getTime();
    if (currentDay === selecteDay) {
      return {
        primary: "#FF5C85",
        secondary: "#FFFFFF",
      };
    } else {
      return {
        primary: "#FFFFFF",
        secondary: "#000000",
      };
    }
  };

  const Days = () => {
    const range = moment.range(
      moment(date).subtract(1, "days"),
      moment(date).add(1, "days"),
    );

    const daysArr = Array.from(range.by("day"));
    return (
      <View style={{ flexDirection: "row" }}>
        {daysArr.map((day: any) => (
          <TouchableOpacity
            style={{
              ...styles.selectedDay,
              backgroundColor: setSelectedColor(day).primary,
            }}
            key={day.format("YYYYMMDD")}
            onPress={() => setDate(day)}
            disabled={checkRangeWeeks(day)}
          >
            <Text
              style={{
                ...styles.text_day_one,
                color: setSelectedColor(day).secondary,
              }}
            >
              {capitalize_first_letter(day.format("ddd")).replace(".", " ")}
            </Text>
            <Text
              style={{
                ...styles.text_day_one,
                color: setSelectedColor(day).secondary,
              }}
            >
              {day.format("D")}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  const selectedSlots = (type: string) => {
    switch (type) {
      case "blocked":
        return "#DDDDDD";
      case "complete":
        return "#FFA2A8";
      case "selected":
        return "#9ED5D0";
      default:
        return "#FFFFFF";
    }
  };

  const deleteOrAddArray = (
    tabId: Array<number | undefined>,
    id: number | undefined,
  ) => {
    const index = tabId.indexOf(id);
    if (index > -1) {
      tabId.splice(index, 1);
    } else {
      tabId.push(id);
    }
    return tabId;
  };

  const addOrDeletePlanning = (i: IPlanning, r: IPlanningDisengageStatus) => {
    const id_creneaux: number | undefined = i.id_creneaux;
    const id_planning: number | undefined = i.id_planning;
    if (id_planning) {
      if (r.is_able_to_disengage && !r.is_course_in_progress) {
        const resultToDelete: Array<number | undefined> = deleteOrAddArray(
          planningToDelete,
          id_planning,
        );
        setPlanningToDelete(resultToDelete);
      } else if (!r.is_able_to_disengage && r.is_course_in_progress) {
        setModalInProgress(true);
        // regenerate liste planning: can't deselect course in progress
        const newPlanning = planning.map((item: IPlanning) => {
          if (i.id === item.id) {
            return {
              ...item,
              state: "selected",
            };
          }
          return item;
        });
        setPlanning(newPlanning);
      } else if (!r.is_able_to_disengage) {
        // can deselect with reduction 25%
        if (i.state !== "dispo") {
          setModalDesengage(true);
        }
        const resultToDelete: Array<number | undefined> = deleteOrAddArray(
          planningToDelete,
          id_planning,
        );
        setPlanningToDelete(resultToDelete);
      }
    } else {
      const resultToAdd: Array<number | undefined> = deleteOrAddArray(
        planningToAdd,
        id_creneaux,
      );
      setPlanningToAdd(resultToAdd);
    }
  };

  const setPlanningSelected = (item: IPlannings) => {
    const newPlanning = item.map((i: IPlanning) => {
      if (i.id_planning) {
        return {
          ...i,
          state: "selected",
        };
      }
      return i;
    });
    setPlanning(newPlanning);
  };

  const editPlanning = (item: IPlanning) => {
    const newPlanning = planning.map((i: IPlanning) => {
      if (item.id === i.id) {
        return {
          ...i,
          state: i.state === "dispo" ? "selected" : "dispo",
        };
      }
      return i;
    });
    setPlanning(newPlanning);

    // check if disengage or have a course in progress
    recover_delivery_planning_summary_disengage_status(
      user_id,
      item.id_creneaux,
    ).then((response: IPlanningDisengageStatus) => {
      addOrDeletePlanning(item, response);
    });
  };

  const updatePlanning = async () => {
    setShowLoadingButton(true);
    const _planningToAdd = [...new Set(planningToAdd)];
    const _planningToDelete = [...new Set(planningToDelete)];

    // fetch form data
    const FData = new FormData();
    const url = `${
      vanillaAuthState.getState().base_url
    }deliverymen/${user_id}/update-planning`;

    // fetch array
    FData.append("marketplace", STATUS.MARKETPLACE);

    _planningToAdd.forEach((addNumber: number | undefined, index: number) => {
      const selectedDate = moment(date).format("YYYY-MM-DD");
      const name = `planning_to_add[${selectedDate}][${index}]`;
      FData.append(name, addNumber);
    });

    _planningToDelete.forEach(
      (deleteNumber: number | undefined, index: number) => {
        const name = `planning_to_delete[${index}]`;
        FData.append(name, deleteNumber);
      },
    );

    // sendPlanning
    const res = await fetch(url, {
      method: "POST",
      body: FData,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Bearer ${token}`,
      },
    });

    const response = await res.json();
    if (response.code === 200) {
      console.log(response.message);
      Toast.show({
        type: "success",
        text1: "Success",
        text2: I18n.t("profile.notification.success.message"),
      });
      await get_planning_by_date(user_id, date);
      setShowLoadingButton(false);
    } else {
      setShowLoadingButton(false);
    }
  };

  const charAtOne = (str: string) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
  };

  const checkValidateDate = (date: unknown) => {
    const dateNow = moment().format("YYYY-MM-DD");
    const selectedDate = moment(date).format("YYYY-MM-DD");
    const dateSameOrAfter = moment(selectedDate).isSameOrAfter(dateNow);

    if (dateSameOrAfter) {
      return false;
    }
    return true;
  };

  useEffect(() => {
    get_planning_by_date(user_id, date);
  }, [date]);

  if (error) {
    return (
      <View style={styles.loading}>
        <Text>Erreur de chargement ... </Text>
      </View>
    );
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.button_day}>
        <Text style={styles.text_day}>
          {charAtOne(moment(date).format("dddd D MMMM YYYY"))}
        </Text>
        <Text style={styles.text_select}>
          Sélectionne tes horaires de travail
        </Text>
      </View>
      <View style={{ flexDirection: "row", marginLeft: 35 }}>
        <View style={styles.month}>
          <Text style={styles.text_month}>
            {moment(date).format("MMMM").toLocaleUpperCase()}
          </Text>
          <Text style={styles.text_month}>{moment(date).format("YYYY")}</Text>
        </View>
        <View style={styles.scroll_day}>
          <TouchableOpacity
            onPress={decrement_week}
            style={styles.icon_chevron}
            disabled={checkRangeWeeksChevronDown(date)}
          >
            <IconsLeft />
          </TouchableOpacity>
          <Days />
          <TouchableOpacity
            onPress={increment_week}
            style={styles.icon_chevron}
            disabled={checkRangeWeeksChevronUp(date)}
          >
            <IconsRight />
          </TouchableOpacity>
        </View>
      </View>
      <View style={{ paddingBottom: 100, marginTop: 7, height: "80%" }}>
        {!isLoading ? (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={planning}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() =>
                  checkValidateDate(date) ? {} : editPlanning(item)
                }
                disabled={
                  item.state === "blocked" ||
                  item.state === "complete" ||
                  checkValidateDate(date)
                }
                style={{
                  flexDirection: "row",
                  marginHorizontal: 30,
                  height: 54,
                  marginTop: index == 0 ? 7 : 0,
                }}
              >
                <View style={{ width: "20%", position: "relative" }}>
                  <Text style={{ position: "absolute", top: -10 }}>
                    {item.time}
                  </Text>
                  {planning.length === index + 1 && (
                    <Text style={{ position: "absolute", bottom: -4 }}>
                      00:00
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    width: "80%",
                    borderTopWidth: index === 0 ? 0.5 : 0,
                    borderBottomWidth: 1,
                    backgroundColor: selectedSlots(item.state),
                    borderColor: "#C4C4C4",
                  }}
                />
              </TouchableOpacity>
            )}
          />
        ) : (
          <View style={styles.loading}>
            <Text>Chargement en cours ... </Text>
          </View>
        )}
      </View>
      <View style={styles.container_button}>
        {showLoadingButton ? (
          <View style={styles.loader_button}>
            <ActivityIndicator style={{ alignSelf: "center" }} color="white" />
          </View>
        ) : (
          <Button
            onPress={updatePlanning}
            type="solid"
            title={I18n.t("form.button.saveEdit")}
            titleStyle={styles.buttonTitle}
            buttonStyle={styles.button}
            disabled={checkValidateDate(date)}
          />
        )}
      </View>
      <PlanningDesengageOverlay
        visible={openModalDesengage}
        onClose={() => setModalDesengage(false)}
        onAccept={() => setModalDesengage(false)}
      />
      <PlanningInProgressOverlay
        visible={openModalInProgress}
        onClose={() => setModalInProgress(false)}
        onAccept={() => setModalInProgress(false)}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  button_day: {
    backgroundColor: "rgba(221, 221, 221, 0.46)",
    borderRadius: 21.5,
    marginLeft: 56,
    marginRight: 32,
    marginTop: 21,
    marginBottom: 15,
  },
  scroll_day: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-around",
    width: 250,
  },
  text_day: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 14,
    lineHeight: 17.07,
    paddingTop: 4,
    paddingBottom: 2,
    paddingLeft: 20,
  },
  text_select: {
    fontFamily: fontFamily.regular,
    color: "rgba(0, 0, 0, 1)",
    fontSize: 12,
    lineHeight: 14.63,
    paddingLeft: 21,
    paddingBottom: 5,
  },
  text_day_one: {
    fontFamily: fontFamily.semiBold,
    fontSize: 14,
    lineHeight: 17.07,
  },
  active: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(255, 92, 133, 1)",
    borderRadius: 16,
    width: 102,
    height: 32,
  },
  text_day_active: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(255, 255, 255, 1)",
    fontSize: 14,
    lineHeight: 17.07,
  },
  container_button: {
    display: "flex",
    position: "absolute",
    alignSelf: "center",
    bottom: 20,
  },
  loader_button: {
    justifyContent: "center",
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  button: {
    width: 308,
    height: 52,
    backgroundColor: color.ACCENT,
    borderRadius: 30,
  },
  line: {
    marginLeft: 50,
    marginRight: 30,
    paddingTop: 7,
  },
  month: {
    alignItems: "flex-start",
    justifyContent: "flex-end",
  },
  text_month: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(105, 109, 117, 1)",
    fontSize: 12,
    lineHeight: 17.07,
  },
  list_item: {
    marginHorizontal: 40,
    height: 50,
    borderBottomWidth: 1,
  },
  text_time: {
    fontFamily: fontFamily.semiBold,
    color: "rgba(105, 109, 117, 1)",
    fontSize: 12,
    lineHeight: 14.63,
  },
  change_date: {
    flexDirection: "row",
    borderRadius: 16,
    paddingLeft: 15,
    marginLeft: 5,
  },
  icon_chevron: {
    width: 25,
    height: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  selectedDay: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    width: 75,
    height: 25,
    borderRadius: 16,
  },
  loading: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",
    width: "100%",
  },
  buttonTitle: {
    fontFamily: fontFamily.semiBold,
    fontSize: 16,
  },
});
