import { Platform, StyleSheet } from "react-native";
import { color } from "src/theme/color";
import { fontFamily } from "src/theme/typography";

export const styles = StyleSheet.create({
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  loadMoreButton: {
    width: "80%",
    height: 45,
    backgroundColor: color.ACCENT,
    borderRadius: 10,
    marginStart: "10%",
    justifyContent: "center",
    alignItems: "center",
  },
  loadMoreButtonText: {
    color: "white",
    fontSize: 15,
    fontWeight: "500",
  },
  scrollView: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  RappelBgColor: {
    marginTop: 17,
    backgroundColor: "rgba(218, 218, 218, 0.31)",
  },
  alignement: {
    marginTop: 7,
    marginBottom: 7,
    marginLeft: 26,
    marginRight: 58,
    fontSize: 12,
    fontFamily: fontFamily.semiBold,
  },

  Mediumtxt: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
  },
  boldtxt: {
    fontSize: 12,
    fontFamily: fontFamily.Bold,
  },
  resume: {
    fontSize: 14,
    fontFamily: fontFamily.semiBold,
  },
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  tabBarStyle: {
    shadowOffset: { height: 0, width: 0 },
    shadowColor: "transparent",
    shadowOpacity: 0,
    elevation: 0,
    backgroundColor: "#fff",
    borderBottomWidth: 1,
    borderColor: "#C4C4C4",
    Bottom: 10,
  },
  tabBarIndicatorStyle: {
    backgroundColor: color.ACCENT,
    height: 4,
    bottom: -2,
    borderRadius: 2,
  },
  tabViewHeader: {
    flexDirection: "row",
    paddingTop: Platform.OS === "android" ? 50 : 60,
    backgroundColor: "#FFF",
  },
  tabViewHeaderLabel: {
    flexGrow: 1,
    fontFamily: fontFamily.Medium,
    fontSize: 20,
    color: "#000",
    marginLeft: 60,
  },
  tabView: {
    width: "100%",
  },
  tabBarLabel: {
    fontFamily: fontFamily.semiBold,
    color: "#112842",
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tabBarLabelFocused: {
    fontFamily: fontFamily.semiBold,
    color: color.ACCENT,
    fontSize: 14,
    textAlign: "center",
    marginBottom: -10,
  },
  tips: {
    flexDirection: "row",
  },
  bonus: {
    flexDirection: "row",
  },
  tipsandBonus: {
    justifyContent: "space-evenly",
    alignItems: "center",
    flexDirection: "row",
    marginTop: 14,
    marginBottom: 10,
  },
  BonusAndTips: {
    marginTop: 18,
    marginLeft: 24,
  },
  tipsandBonusBg: {
    marginTop: 7,
    backgroundColor: "rgba(255, 203, 61, 0.21)",
    height: 80,
  },
  textPosition: {
    marginLeft: 25,
    marginRight: 18,
    fontFamily: fontFamily.regular,
    fontSize: 10,
    paddingBottom: 8,
  },
  gainTextPosition: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 24,
    paddingHorizontal: 20,
  },
  invoiceCard: {
    marginTop: 10,
  },
  filterContainer: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "#F2F2F2",
    paddingStart: 5,
  },
  filterText: {
    fontSize: 12,
    fontFamily: fontFamily.Medium,
    paddingHorizontal: 7,
    paddingVertical: 3,
  },
  GoBackPosition: {
    marginLeft: 25,
    marginTop: 5,
    width: 50,
    height: 34,
    alignItems: "center",
  },
  bgColor: {
    backgroundColor: "#FFF",
    height: "100%",
  },
});
