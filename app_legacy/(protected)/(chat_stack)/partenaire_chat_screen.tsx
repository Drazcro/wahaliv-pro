import React, { useEffect, useMemo, useState } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
} from "react-native";
import { ROLE_TYPE, getBaseUser } from "src_legacy/services/Utils";
import { IMessage } from "src_legacy/session/types";
import { db } from "firebase";
import {
  onValue,
  push,
  ref,
  set,
  serverTimestamp,
  update,
  increment,
  off,
} from "firebase/database";
import moment from "moment";
import SendIcon from "assets/sendIcon.svg";
import { fontFamily } from "src/theme/typography";
import { useAppState } from "src_legacy/hooks/useAppState";
import { TextInput } from "react-native-paper";
import UserAvatar from "assets/image_svg/UserAvatar.svg";
import { profileStore } from "src_legacy/zustore/profilestore";
import { encode } from "base-64";
import { partnerStore } from "src_legacy/zustore/partnerstore";
import { Toolbar } from "src_legacy/components/delivery/courses/tool_bar/tool_bar";
//import * as Sentry from "@sentry/react-native";
import MessageBone from "src_legacy/components/delivery/profile/components/message_body";
import { authStore } from "src_legacy/services/v2/auth_store";

const ChatScreen = () => {
  const [text, setText] = useState<string>("");
  const { auth } = authStore();
  const [messages, setMessages] = useState<IMessage[]>([]);
  const base_user = getBaseUser(auth);
  const AppState = useAppState();

  const profile = profileStore((v) => v.profile);
  const partnerProfile = partnerStore((v) => v.partnerProfile);

  const message = {
    sent_by_user: true,
    content: text,
    read: false,
    sent_at: moment().format(),
    timestamp: serverTimestamp(),
  };

  const getChatPath = useMemo(() => {
    const formattedString = `${base_user.id}|-|;${base_user.email}|-|;${
      base_user.roles.filter((item) => item !== ROLE_TYPE.ROLE_PARTICULIER)[0]
    }`;

    return encode(formattedString);
  }, [base_user]);

  const updateCounter = () => {
    const dbRef = ref(db, `chat/${getChatPath}/chat_metadata/unread_for`);

    const updates = {
      supervision: increment(1),
    };

    update(dbRef, updates);
  };

  const handleMessageSend = () => {
    const messageListRef = ref(db, "chat/" + getChatPath);

    if (messageListRef.key === getChatPath) {
      const newMessageRef = push(messageListRef);
      set(newMessageRef, message);

      updateCounter();
    }
    setText("");
  };

  const getFullName = React.useMemo(() => {
    if (base_user.roles.includes(ROLE_TYPE.ROLE_COURSIER)) {
      const account = profile;
      return `${account.firstName ?? ""} ${account.lastName ?? ""}`;
    } else {
      const account = partnerProfile;
      return `${account?.partner?.name ?? ""}`;
    }
  }, [partnerProfile, profile, base_user]);

  useEffect(() => {
    const dbRef = ref(db, "/chat/" + getChatPath);
    const unsubscribe = onValue(dbRef, (snapshot) => {
      if (!snapshot.exists()) {
        return;
      }

      const newMessages: IMessage[] = [];
      const data = snapshot.val();
      const messages = { ...data };
      try {
        delete messages.chat_metadata;
      } catch (e) {
        console.error(e);
        //Sentry.captureException(e)
        
      }

      for (const key in messages) {
        //console.log("keys => ", messages[key]);
        const msg: IMessage = messages[key];
        newMessages.push(msg);
      }

      setMessages(newMessages);
    });

    return () => {
      unsubscribe();
    };
  }, [getChatPath]);

  useEffect(() => {
    console.log("encoded => ", getChatPath);
  }, []);

  // Update latest
  useEffect(() => {
    const dbRef = ref(db, `/chat/${getChatPath}/`);

    onValue(dbRef, (snapshot) => {
      const data = snapshot.val();
      const discussion = { ...data };

      // remove chat_metadata and read to get the last discussion data
      const old_metadata = { ...(discussion.chat_metadata ?? {}) };
      delete discussion.chat_metadata;
      //delete discussion.read;

      const messages: IMessage[] = Object.values(discussion);

      if (messages && messages.length > 0) {
        const latest_message: IMessage = messages[messages.length - 1];
        const metadataRef = ref(db, `/chat/${getChatPath}/chat_metadata`);

        const updates = {
          ...old_metadata,
          lastMessage: {
            ...latest_message,
            content: latest_message.content ?? "",
            sent_at: latest_message.sent_at ?? moment().format(),
            sent_by_user:
              latest_message.sent_by_user !== undefined
                ? latest_message.sent_by_user
                : false,
            timestamp: latest_message.timestamp ?? serverTimestamp(),
          },

          full_name: `${getFullName}`,
          email: base_user.email,
          id: base_user.id,
          role: base_user.roles.filter(
            (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
          )[0],
        };

        update(metadataRef, updates);
      }
    });

    return () => {
      off(dbRef);
    };
  }, [getChatPath]);

  useEffect(() => {
    const dbRef = ref(db, `chat/${getChatPath}/chat_metadata/unread_for`);

    const updates = {
      user: 0,
    };
    update(dbRef, updates);
  }, []);

  useEffect(() => {
    if (
      !base_user ||
      !base_user?.id ||
      !base_user.roles ||
      base_user.roles.length < 1
    ) {
      console.log("no user");
    }
    const role = base_user?.roles.filter(
      (item) => item !== ROLE_TYPE.ROLE_PARTICULIER,
    )[0];
    const dbRef = ref(db, `/status/${role}/` + base_user?.id);

    if (AppState === "inactive") {
      const updates = {
        email: base_user.email,
        last_seen: moment().format(),
        online: false,
        full_name: `${getFullName}`,
      };

      update(dbRef, updates);
    } else {
      const updates = {
        email: base_user.email,
        last_seen: moment().format(),
        online: true,
        full_name: `${getFullName}`,
      };

      update(dbRef, updates);
    }
  }, [AppState]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <View style={{ flex: 1 }}>
        <Toolbar
          title={"Chat service client "}
          avatar={<UserAvatar width={26} />}
          username={`${profile.firstName ?? ""}`}
          showRaload={true}
        />
        <View style={{ ...styles.container, backgroundColor: "transparent" }}>
          <MessageBone messages={messages} />
        </View>
        <View style={{ ...styles.inputView }}>
          <TextInput
            outlineStyle={{ borderWidth: 0, borderRadius: 15 }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(_text: string) => setText(_text)}
            value={text}
            maxLength={200}
            placeholder="Votre message"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
          <TouchableOpacity
            onPress={handleMessageSend}
            style={styles.sendButton}
            disabled={!text}
          >
            <SendIcon />
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
};

export default ChatScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    backgroundColor: "#fff",
  },
  chatHeader: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    gap: 1,
  },
  userInformation: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  messageInput: {
    flex: 1,
    borderWidth: 1,
    paddingHorizontal: 10,
    backgroundColor: "#fff",
  },
  InputContainer: {
    backgroundColor: "rgba(231, 231, 231, 1)",
    flexDirection: "row",
    width: "90%",
  },
  inputView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 10,
    height: 68,
    backgroundColor: "rgba(231, 231, 231, 1)",
  },
  screenHeader: {
    paddingTop: 50,
    paddingBottom: 9,
    paddingHorizontal: 25,
    borderBottomWidth: 1,
    borderBottomColor: "#D3D3D3",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  headerTitle: {
    textAlign: "center",
    fontSize: 20,
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
  },
  sendButton: {
    position: "relative",
    right: 28,
  },
  msgInput: {
    width: "85%",
    backgroundColor: "#fff",
    marginRight: 41,
    paddingLeft: 10,
    marginLeft: 10,
    marginBottom: 6,
    fontFamily: fontFamily.Medium,
  },
});
