import React, { useEffect, useState } from "react";
import {
  KeyboardAvoidingView,
  Platform,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import { fontFamily } from "src/theme/typography";
import SendIcon from "assets/sendIcon.svg";
import DisabledSendBtn from "assets/image_svg/disabled_send_btn.svg";
import { TextInput, useTheme } from "react-native-paper";
import { IMessage } from "src_legacy/session/types";
import {
  get,
  increment,
  off,
  onValue,
  push,
  ref,
  set,
  update,
} from "firebase/database";
import { db } from "firebase";
import moment from "moment";
import { tourneeStore } from "src_legacy/zustore/tourneestore";
import { useLocalSearchParams } from "expo-router";
import useChat from "src_legacy/hooks/useChat";
import { getBaseUser } from "src_legacy/services/Utils";
//import * as Sentry from "@sentry/react-native";
import ChatClientToolBar from "src_legacy/components/delivery/profile/components/chat_client_toolbar";
import MessageBone from "src_legacy/components/delivery/profile/components/message_body";
import { authStore } from "src_legacy/services/v2/auth_store";

type TopBarDataType = {
  client_name: string;
  location: string;
  delivery_hour: string;
};

export default function ChatClientScreen() {
  const { command_id } = useLocalSearchParams<{ command_id: string }>();
  const { auth } = authStore();
  const user = getBaseUser(auth);

  const steps = tourneeStore((v) => v.steps);
  const { setUnreadClientCount } = useChat();
  const [text, setText] = useState<string>("");
  const [messages, setMessages] = useState<IMessage[]>([]);

  const [topBarData, setTopBarData] = useState<TopBarDataType>({
    client_name: "",
    location: "",
    delivery_hour: "",
  });

  const [closed, setClose] = useState<boolean>(false);

  const theme = useTheme();

  const handleMessageSend = () => {
    try {
      const messageListRef = ref(
        db,
        `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/client_livreur`,
      );

      if (messageListRef.key === "client_livreur") {
        const newMessageRef = push(messageListRef);
        set(newMessageRef, {
          content: text,
          sent_at: moment().format(),
          sent_by_client: false,
        });
      }

      const counter_ref = ref(
        db,
        `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/unread_for`,
      );

      update(counter_ref, {
        client: increment(1),
      });

      const lastContact_ref = ref(
        db,
        `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/chat_metadata`,
      );
      update(lastContact_ref, {
        last_message_at: moment().format(),
      });

      setText("");
    } catch (error) {
      //Sentry.captureException(error)
      console.error(error);
    }
  };

  function fetchChatMessages() {
    const db_ref = ref(db, `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/client_livreur`);

    try {
      onValue(db_ref, (snaphost) => {
        const data = snaphost.val();

        delete data.initial;

        const messages: IMessage[] = Object.values(data);

        setMessages(
          messages.sort((a, b) => {
            return moment(a.sent_at).diff(b.sent_at);
          }),
        );
      });
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    try {
      const chat_metadata_ref = ref(
        db,
        `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/chat_metadata/`,
      );

      get(chat_metadata_ref).then((snapshot) => {
        if (snapshot.exists()) {
          const data = snapshot.val();

          setTopBarData({
            client_name: data.nom_client,
            location: data.location,
            delivery_hour: data.delivery_time,
          });

          setClose(data.closed);
        }
      });
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error)
    }
  }, [command_id]);

  useEffect(() => {
    fetchChatMessages();
  }, [command_id]);

  useEffect(() => {
    const counter_ref = ref(
      db,
      `chat_all_My8xMS8yMDIz/${user?.id}/${command_id}/unread_for/`,
    );
    try {
      onValue(counter_ref, (snapshot) => {
        if (!snapshot.exists()) {
          return;
        }

        const count = snapshot.val().livreur;

        if (count > 0) {
          update(counter_ref, {
            livreur: 0,
          });
        }
      });
    } catch (error) {
      console.error(error);
      //Sentry.captureException(error)
    }
    setUnreadClientCount(0);

    return () => {
      off(counter_ref);
    };
  }, [steps]);

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <View style={{ flex: 1 }}>
        <ChatClientToolBar
          command_id={command_id}
          client_name={topBarData.client_name ?? ""}
          client_location={topBarData.location ?? ""}
          command_time={topBarData.delivery_hour ?? ""}
          reFetchMessages={fetchChatMessages}
        />
        <View style={{ ...styles.container, backgroundColor: "transparent" }}>
          <MessageBone messages={messages} />
        </View>
        {closed === true ? (
          <Text
            style={{
              fontSize: 16,
              color: "rgba(141, 152, 160, 1)",
              fontFamily: fontFamily.Medium,
              width: 263,
              justifyContent: "center",
              alignSelf: "center",
              textAlign: "center",
              flex: 1,
              lineHeight: 19.5,
            }}
          >
            La commande a été livrée. Il n’est plus possible d’échanger avec ce
            client
          </Text>
        ) : null}
        <View style={{ ...styles.inputView }}>
          <TextInput
            theme={{
              fonts: {
                bodyLarge: {
                  ...theme.fonts.bodyLarge,
                  fontFamily: fontFamily.Medium,
                },
              },
            }}
            outlineStyle={{ borderWidth: 0, borderRadius: 15 }}
            mode="outlined"
            style={styles.msgInput}
            onChangeText={(_text: string) => setText(_text)}
            value={text}
            maxLength={200}
            placeholder="Votre message"
            placeholderTextColor={"rgba(105, 109, 117, 0.7)"}
          />
          <TouchableOpacity
            onPress={handleMessageSend}
            style={styles.sendButton}
            disabled={!text || closed === true}
          >
            {closed === true ? <DisabledSendBtn /> : <SendIcon />}
          </TouchableOpacity>
        </View>
      </View>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 10,
    backgroundColor: "#fff",
  },
  chatHeader: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
    gap: 1,
  },
  userInformation: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  messageInput: {
    flex: 1,
    borderWidth: 1,
    paddingHorizontal: 10,
    backgroundColor: "#fff",
  },
  InputContainer: {
    backgroundColor: "rgba(231, 231, 231, 1)",
    flexDirection: "row",
    width: "90%",
  },
  inputView: {
    width: "100%",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 10,
    height: 68,
    backgroundColor: "rgba(231, 231, 231, 1)",

    // position: "absolute",
    // bottom: 0,
  },
  screenHeader: {
    paddingTop: 50,
    paddingBottom: 9,
    paddingHorizontal: 25,
    borderBottomWidth: 1,
    borderBottomColor: "#D3D3D3",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  headerTitle: {
    textAlign: "center",
    fontSize: 20,
    fontFamily: fontFamily.semiBold,
    color: "rgba(29, 42, 64, 1)",
  },
  sendButton: {
    position: "relative",
    right: 28,
  },
  msgInput: {
    width: "85%",
    backgroundColor: "#fff",
    marginRight: 41,
    paddingLeft: 10,
    marginLeft: 10,
    marginBottom: 6,
    fontFamily: fontFamily.Medium,
  },
});
