import { Slot, usePathname, useRootNavigation, useRouter } from "expo-router";
import React from "react";
import I18n from "i18n-js";
import { fr } from "src/localization/fr";
import * as Localization from "expo-localization";
import { ACCOUNT_TYPE, authStore } from "src_legacy/services/v2/auth_store";

I18n.translations = {
  fr: fr,
  en: fr,
};
// Set the locale once at the beginning of your app.
I18n.locale = Localization.locale;
// When a value is missing from a language it'll fallback to another language with the key present.
I18n.fallbacks = true;

export default function RootLayout() {
  const { auth, account_type } = authStore();
  const navigation = useRootNavigation();
  const router = useRouter();
  const path = usePathname();

  // useEffect(() => {
  //   const unsubscribe = rootNavigation?.addListener('state', () => {
  //     setNavigationReady(true);
  //   });
  //   return function cleanup() {
  //     if (unsubscribe) {
  //       unsubscribe();
  //     }
  //   };
  // }, [rootNavigation]);

  React.useEffect(() => {
    if (!navigation?.isReady()) return;
    console.log("auth:", auth.access_token, auth, account_type, auth);
    if (auth && auth.is_loading === true) {
      router.replace("/loading");
    } else {
      console.log("check");
      if (!auth.access_token && !path.startsWith("/auth")) {
        router.replace("/auth/");
      } else if (auth.access_token && path.startsWith("/auth")) {
        console.log("ok");
        if (account_type === ACCOUNT_TYPE.DELIVERY) {
          router.replace("/delivery_tab");
        } else {
          router.replace("/partenaire_tab");
        }
      } else {
        console.log("path: ", path, auth.access_token);
      }
    }
  }, [auth, account_type, auth.access_token, auth.is_loading, path, router]);

  return <Slot />;
}
